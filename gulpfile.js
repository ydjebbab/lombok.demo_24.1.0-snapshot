'use strict';

const process = require('process');
const path = require('path');
const fs = require('fs');
const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const csso = require('gulp-csso');

const srcSass = process.env.SRC_SASS;
const srcSassToFilter = process.env.SRC_SASS_TOFILTER;
const srcSassFiltered = process.env.SRC_SASS_FILTERED;
const ineaSass = process.env.INEA_SASS;
const deploy = process.env.DEPLOY_DIR;
const dist = process.env.DIST_DIR;

// plugin postcss pour regrouper ensemble les mêmes sélecteurs afin de la fusionner avec csso
const groupRules = require('postcss').plugin('postcss-group-rules', function (opts) {
    opts = opts || {};
    return function (css, result) {
        let rule = css.first;
        while (rule) {
            if (rule.type === 'rule') {
                // regrouper les selecteurs identiques
                let sames = [];
                let same = rule.next();
                while (same) {
                    if (same.type === 'rule' && same.selector === rule.selector) {
                        sames.push(same);
                    }
                    same = same.next();
                }
                if (sames.length > 0) {
                    rule.after(sames);
                    rule = sames.pop();
                }
            } else if (rule.type === 'atrule' && rule.name === 'media') {
                let mrule = rule.first;
                while (mrule) {
                    if (mrule.type === 'rule') {
                        // regrouper les selecteurs identiques
                        let sames = [];
                        let same = mrule.next();
                        while (same) {
                            if (same.type === 'rule' && same.selector === mrule.selector) {
                                sames.push(same);
                            }
                            same = same.next();
                        }
                        if (sames.length > 0) {
                            mrule.after(sames);
                            mrule = sames.pop();
                        }
                    }
                    mrule = mrule.next();
                }
            }
            rule = rule.next();
        }
    };
});

gulp.task('css:deploy', function () {
    return gulp.src(`${srcSass}/application.scss`)
        .pipe(sourcemaps.init())
        .pipe(sass({
            errLogToConsole: true,
            includePaths: [`${srcSassFiltered}/`,`${ineaSass}/`],
            precision: 8,
        }))
        .pipe(postcss([require('autoprefixer')]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(`${deploy}/css`));
});

gulp.task('css:dist', function () {
    return gulp.src(`${srcSass}/application.scss`)
        .pipe(sass({
            errLogToConsole: true,
            includePaths: [`${srcSassToFilter}/`,`${ineaSass}/`],
            precision: 8,
            outputStyle: 'compressed'
        }))
        .pipe(postcss([require('autoprefixer'), require('css-mqpacker')({ sort: true }), groupRules]))
        .pipe(csso())
        .pipe(gulp.dest(`${dist}/css`));
});

gulp.task('css', ['css:deploy','css:dist']);