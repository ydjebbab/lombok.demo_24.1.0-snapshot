#!/bin/bash

# Fonction de description des parametres
fonct_usage ()
{
  echo "Usage: $0 (configuration|exploitation) version_a_amender [local]" 1>&2
  echo "Exemple: $0 configuration 1.2.3-2" 1>&2
}


# Nombre d'arguments
if [ $# -ne 2 ] && [ $# -ne 3 ]
then
  fonct_usage
  exit 1
fi

if [[ !( $2 =~ ^[0-9][0-9]?\.[0-9][0-9]?(\.[0-9][0-9]?)?-[1-9][0-9]?$ ) ]]
then
  echo "Version invalide: $2" 1>&2
  echo "La version doit être de la forme MM.mm[.pp]-nn, par exemple 1.2.3-2" 1>&2
  exit 1
fi

PROPERTIES="-DamendementVersion=$2 "
if [[ $1 == 'configuration' ]]
then
  PROPERTIES="${PROPERTIES} -DamendementType=configuration"
elif [[ $1 == 'exploitation' ]]
then
  PROPERTIES="${PROPERTIES} -DamendementType=exploitation"
else
  usage
  exit 1
fi

GOALS='clean deploy clean'
if [[ $3 == 'local' ]]
then
  GOALS='clean package'
fi

cd ..
mvn ${GOALS} ${PROPERTIES} -Pamendement ${EXTRA_ARGS}

if [[ $3 == 'local' ]]
then
  mvn fr.gouv.finances.canada:canada-rpm-plugin:supprime-pom-tmp-amendement
  echo
  echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
  echo "Les RPMs générés se trouvent dans le répertoire ../../target/rpm/"
  echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
  echo "Amendement effectué en local !"
  echo "L'amendement ne sera effectif que si vous enlevez l'argument 'local'."
  echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
fi
