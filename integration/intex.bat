@echo off

if "%1" == "" GOTO Err

call mvn -Plivraison_intex -Drelease=%1 clean verify %EXTRA_ARGS%

GOTO Fin

:Err
echo Syntaxe: %0 version_a_livrer
echo Exemple: %0 1.2.3
:Fin
