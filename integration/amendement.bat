@echo off

REM encoding: cp850

if "%2" == "" GOTO Err

if "%1" == "configuration" GOTO paramConfiguration
if "%1" == "exploitation" GOTO paramExploitation
GOTO Err

:paramConfiguration
SET PROPERTIES=-DamendementType=configuration
GOTO maven

:paramExploitation
SET PROPERTIES=-DamendementType=exploitation
GOTO maven

:maven

if "%3" == "local" (SET GOALS=clean package) else (SET GOALS=clean deploy clean)

SET REPERTOIRE_COURANT=%cd%

cd ..
call mvn %GOALS% %PROPERTIES% -Pamendement -DamendementVersion=%2 %EXTRA_ARGS%

if "%3" == "local" (
  call mvn fr.gouv.finances.canada:canada-rpm-plugin:supprime-pom-tmp-amendement
  echo:
  echo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  echo Les RPMs g�n�r�s se trouvent dans le r�pertoire ..\..\target\rpm\
  echo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  echo Amendement effectu� en local !
  echo L'amendement ne sera effectif que si vous enlevez l'argument 'local'.
  echo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
)

cd %REPERTOIRE_COURANT%

GOTO Fin

:Err
echo Syntaxe: %0 (configuration ou exploitation) version_a_amender [local]
echo Exemple: %0 configuration 1.2.3-2
:Fin
