<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page afficherlisteavis.jsp -->
<c:set var="responsive" value="true" />

<app:page   titreecran="Satelit bienvenue"
            titrecontainer="Choisissez un avis d'imposition dans la liste"
            menu="true" responsive="${responsive}">

    <app:chemins action="zf1/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contrats" responsive="${responsive}">Contrats</app:chemin>
        <app:chemin title="Adhérer" responsive="${responsive}">Adhérer</app:chemin>
        <app:chemin title="Prélèvement mensuel" responsive="${responsive}">Prélèvement mensuel</app:chemin>
        <app:chemin title="Immédiat" responsive="${responsive}">Immédiat</app:chemin>
        <app:chemin title="Sélection d'un avis d'imposition" responsive="${responsive}">Sélection d'un avis d'imposition</app:chemin>
    </app:chemins>

    <app:form action="zf1/flux.ex" formboxclass="eXtremeTable" responsive="${responsive}">
        <%--
<ec:table 
  items="listeavisimposition" 
  title="Choisissez un avis d'imposition dans la liste" 
  autoIncludeParameters="false"
  showPagination="false"
  showStatusBar="false"
  filterable="false"
  sortable="false"
  locale="fr_FR"
  width="50%"
  form="test"
  theme="eXtremeTable centerTable"
  var="row"
  styleClass="table">
--%>
        <ec:table  items="listeavisimposition" var="row" autoIncludeParameters="false"
            form="test" locale="fr_FR" filterable="true" sortable="true" showPagination="true" showNewPagination="true" showExports="true"
            newPaginationNbPage="5" showStatusBar="true" showPaginationBottom="false" showExportsBottom="false" showNewFilter="true"
            onInvokeAction="ecupdatectra('tableaunavigation')" positionnoresultsfound="body">

            <ec:row>
                <%--
                <ec:column property="reference" title="Référence">
                <app:link action="zf1/flux.ex"
                          transition="selection"
                          attribute="refavis"
                          value="${row.reference}"
                          label="${row.reference}"
                />
                </ec:column>
                <ec:column property="montant" alias="Montant" title="Montant (€)" cell="currency" format="# ##0,00" style="text-align: right;"/>
                <ec:column property="montant" alias="Montant du" title="Montant (€)" style="text-align: right;">
                  <fmt:formatNumber pattern="#,##0.###">${row.montant}</fmt:formatNumber>
                </ec:column>
                <ec:column property="dateRole" title="Date d'émission du rôle" cell="date" format="dd/MM/yyyy"/>
  --%>
                <ec:column property="reference" title="Référence" filterable="true" sortable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col">
                    <app:link action="zf1/flux.ex" transition="selection" attribute="refavis" value="${row.reference}" attributesupp="${_csrf.parameterName}" valuesupp="${_csrf.token}"
                        label="${row.reference}" />
                </ec:column>
                <ec:column property="montant" alias="Montant" title="Montant (€)" cell="currency" format="# ##0,00"
                    style="text-align: right;" filterable="true" sortable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=1 scope=col" />
                <ec:column property="montant" alias="Montant du" title="Montant (€)" style="text-align: right;" filterable="true"
                    sortable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col">
                    <fmt:formatNumber pattern="#,##0.###">${row.montant}</fmt:formatNumber>
                </ec:column>
                <ec:column property="dateRole" title="Date d'émission du rôle" cell="date" format="dd/MM/yyyy" filterable="true"
                    sortable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />
            </ec:row>
        </ec:table>
    </app:form>
</app:page>
