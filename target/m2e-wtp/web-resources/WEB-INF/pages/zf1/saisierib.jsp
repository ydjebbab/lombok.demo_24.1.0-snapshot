<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisierib.jsp -->
<c:set var="responsive" value="true" />

<app:page   titreecran="Adhésion à la mensualisation - Références bancaires"
            titrecontainer="Adhésion à la mensualisation - Références bancaires" 
            menu="true" responsive="${responsive}">

    <app:chemins action="zf1/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contrats" responsive="${responsive}">Contrats</app:chemin>
        <app:chemin title="Adhérer" responsive="${responsive}">Adhérer</app:chemin>
        <app:chemin title="Prélèvement mensuel" responsive="${responsive}">Prélèvement mensuel</app:chemin>
        <app:chemin title="Immédiat" responsive="${responsive}">Immédiat</app:chemin>
    </app:chemins>

    <app:onglets cleactive="2" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Type d'impôt" bouton="Precedent" responsive="${responsive}" ongletclass="btmodel_inv_third" />
        <app:onglet cle="2" libelle="Références bancaires" responsive="${responsive}" ongletclass="btmodel_inv_third" />
        <app:onglet cle="3" libelle="Récapitulatif" bouton="Suivant" responsive="${responsive}" ongletclass="btmodel_inv_third" />
    </app:onglets>

    <app:form method="post" action="zf1/flux.ex" formid="saisiecontribuable" formobjectname="demandeadhesionmensualisationform"
        onsubmit="return submit_form_noenter(500)" formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}">
        <app:input attribut="nouveauContrat.rib.codeBanque" libelle="Code banque :" maxlength="5" size="5" responsive="${responsive}" />
        <app:input attribut="nouveauContrat.rib.codeGuichet" libelle="Code guichet :" maxlength="5" size="5" responsive="${responsive}" />
        <app:input attribut="nouveauContrat.rib.numeroCompte" libelle="Numéro de compte :" maxlength="11" size="11"
            responsive="${responsive}" />
        <app:input attribut="nouveauContrat.rib.cleRib" libelle="Clé RIB:" maxlength="2" size="2" responsive="${responsive}" />
        <app:input attribut="titulaireCompte" libelle="Titulaire du compte (à saisir si compte d\'une tierce personne) :" maxlength="38"
            size="38" responsive="${responsive}" />
        <app:input attribut="nouveauContrat.contribuablePar.adresseMail" libelle="Adresse mél : " maxlength="60" size="60"
            responsive="${responsive}" />


        <app:boxboutons>
            <app:submit label="Précédent" transition="Precedent" responsive="${responsive}"  />
            <app:submit label="Suivant" transition="Suivant" id="enterButton" responsive="${responsive}" 
                class="primary" />
        </app:boxboutons>
    </app:form>
</app:page>