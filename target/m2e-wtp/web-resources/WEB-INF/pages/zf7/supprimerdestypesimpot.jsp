<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page supprimerdestypesimpot.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Gérer les types d'impôt" menu="true" responsive="${responsive}">

    <app:chemins action="zf7/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Gestion des types d'impôts" transition="listeimpots" responsive="${responsive}">Gestion des types d'impôts</app:chemin>
        <app:chemin title="Supprimer des types d'impôts" responsive="${responsive}">Supprimer des types d'impôt</app:chemin>
    </app:chemins>

    <app:form action="zf7/flux.ex" formboxclass="eXtremeTable" formobjectname="referenceform" responsive="${responsive}">


        <ec:table items="typesimpotsasupprimer" action="${pageContext.request.contextPath}/zf7/flux.ex" tableId="typesimpotsasupprimer"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml" filterable="true" showPagination="false"
            showExports="true" var="row" sortRowsCallback="cp" filterRowsCallback="cp">
            <ec:row highlightRow="true">
                <ec:column property="codeImpot" title="Code impôt" width="5%" sortable="true" filterable="false">
                </ec:column>
                <ec:column property="nomImpot" title="Libellé de l'impôt" width="40%" sortable="true" />
                <ec:column property="estOuvertALaMensualisation" title="Ouvert à la mensualisation" width="10%" sortable="true">
                </ec:column>
                <ec:column property="estDestineAuxParticuliers" title="Destiné aux particuliers" width="10%" sortable="true">
                </ec:column>
                <ec:column property="jourOuvertureAdhesion" title="Jour d'ouverture de l'adhésion" width="10%" cell="date" sortable="true">
                </ec:column>
                <ec:column property="jourLimiteAdhesion" title="Date limite de l'adhésion" width="10%" cell="date" sortable="true">
                </ec:column>
            </ec:row>
        </ec:table>
        <p>
            <br>
        </p>

        <app:boxboutons>
            <app:submit label="Retourner à la liste des types d'impôts" transition="listeimpots" responsive="${responsive}" />
            <app:submit label="Confirmer la suppression" transition="confirmersuppression" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>