<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page testformatnombre.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Test des formats nombre" menu="true" responsive="${responsive}">

    <app:chemins action="zf7/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Gestion des types d'impôt" transition="listeimpots" responsive="${responsive}">Gestion des types d'impôt</app:chemin>
        <app:chemin title="Pages de test" transition="pagesdetest" responsive="${responsive}">Pages de test</app:chemin>
        <app:chemin title="Format de nombres" responsive="${responsive}">Format de nombres</app:chemin>
    </app:chemins>

    <app:form action="zf7/flux.ex" formboxclass="eXtremeTable" responsive="${responsive}">

        <ec:table items="referencetypesimpots" action="${pageContext.request.contextPath}/zf7/flux.ex" tableId="testfmt1"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml" filterable="false" showPagination="false"
            showExports="true" var="row" sortRowsCallback="cp" filterRowsCallback="cp" title="Formats nombres">

            <ec:exportPdf fileName="listeDesTypesImpots.pdf" tooltip="Export PDF" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="test de formats booleans" imageName="pdf" view="cppdf" />

            <ec:exportXls encoding="UTF" fileName="listeDesTypesImpots.xls" imageName="xls" tooltip="Export Excel" view="cpxls" />

            <ec:exportCsv fileName="listeDesTypesImpots.csv" tooltip="Export CSV" delimiter="|" viewResolver="csv" />

            <ecjrxml:exportPdf titreedition="Test formatage de nombres" fileName="testfmt1.pdf" view="cpjrxml" viewResolver="cpjrxml"
                tooltip="Export PDF (jrxml)" imageName="pdf" text="Export PDF (jrxml)" nomapplication="DMO Test" modele="portrait"
                textepieddepage="Copyright DGCP " pagedegarde="true" conf="documentinfo.xml" />

            <ec:row highlightRow="true">
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
                    cell="nombrecellzerodecimale" title="nombre zero decimale" alias="nombrezerodecimale" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
                    cell="nombrecellunedecimale" title="nombre une decimale" alias="nombreunedecimale" calc="total,decomptenonnulls"
                    calcExport="all,last" calcTitle="Total,DecompteNonNulls" calcStyle="text-align: right;"
                    calcTitleStyle="text-align: right;" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
                    cell="nombrecelldeuxdecimales" title="nombre deux decimales" alias="nombredeuxdecimales" calc="total,decompte"
                    calcTitle="Total,Decompte" calcStyle="text-align: right;" calcTitleStyle="text-align: right;" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
                    cell="nombrecelltroisdecimales" title="nombre trois decimales" alias="nombretroisdecimales" calc="total,decompte"
                    calcTitle="Total,Decompte" calcStyle="text-align: right;" calcTitleStyle="text-align: right;" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
                    cell="nombrecellquatredecimales" title="nombre quatre decimales" alias="nombrequatredecimales" calc="total,decompte"
                    calcTitle="Total,Decompte" calcStyle="text-align: right;" calcTitleStyle="text-align: right;" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
                    cell="nombrecellcinqdecimales" title="nombre cinq decimales" alias="nombresixdecimales" calc="total,decompte"
                    calcTitle="Total,Decompte" calcStyle="text-align: right;" calcTitleStyle="text-align: right;" />
            </ec:row>
        </ec:table>


        <ec:table items="referencetypesimpots" action="${pageContext.request.contextPath}/zf7/flux.ex" tableId="testfmt2"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml" filterable="false" showPagination="false"
            showExports="true" var="row" sortRowsCallback="cp" filterRowsCallback="cp">

            <ec:exportPdf fileName="listeDesTypesImpots.pdf" tooltip="Export PDF" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="test de formats booleans" imageName="pdf" view="cppdf" />

            <ec:exportXls encoding="UTF" fileName="listeDesTypesImpots.xls" imageName="xls" tooltip="Export Excel" view="cpxls" />

            <ec:exportCsv fileName="listeDesTypesImpots.csv" tooltip="Export CSV" delimiter="|" viewResolver="csv" />

            <ecjrxml:exportPdf fileName="testfmt2.pdf" view="cpjrxml" viewResolver="cpjrxml" tooltip="Export PDF (jrxml)" imageName="pdf"
                text="Export PDF (jrxml)" nomapplication="DMO Test" modele="portrait" textepieddepage="Copyright DGCP " />

            <ec:row highlightRow="true">
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
                    cell="nombrecellsixdecimales" title="nombre six decimales" alias="nombresixdecimales" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
                    cell="nombrecellseptdecimales" title="nombre sept decimales" alias="nombreseptdecimale" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
                    cell="nombrecellhuitdecimales" title="nombre huit decimales" alias="nombrehuitdecimales" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
                    cell="nombrecellneufdecimales" title="nombre neuf decimales" alias="nombreneufdecimales" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
                    cell="nombrecelldixdecimales" title="nombre dix decimales" alias="nombredicdecimales" />
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" />
                    <app:submit label="Retourner à l'accueil" transition="retour" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" responsive="${responsive}"
                         />
                    <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
</app:page>