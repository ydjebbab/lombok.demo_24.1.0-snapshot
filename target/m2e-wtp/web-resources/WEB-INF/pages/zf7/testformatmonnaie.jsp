<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page testformatmonnaie.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Test des formats monnaie" menu="true" responsive="${responsive}">

    <app:chemins action="zf7/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Gestion des types d'impôt" transition="listeimpots" responsive="${responsive}">Gestion des types d'impôt</app:chemin>
        <app:chemin title="Pages de test" transition="pagesdetest" responsive="${responsive}">Pages de test</app:chemin>
        <app:chemin title="Format de monnaie" responsive="${responsive}">Format de monnaie</app:chemin>
    </app:chemins>

    <app:form action="zf7/flux.ex" formboxclass="eXtremeTable" responsive="${responsive}">



        <ec:table items="referencetypesimpots" action="${pageContext.request.contextPath}/zf7/flux.ex" tableId="testfmt1"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" filterable="true" showPagination="true" showExports="true"
            var="row" sortRowsCallback="cp" filterRowsCallback="cp">
            <ec:exportPdf fileName="listeDesTypesImpots.pdf" tooltip="Export PDF" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="Test de formats monétaires" imageName="pdf" view="cppdf" />

            <ec:exportXls encoding="UTF" fileName="listeDesTypesImpots.xls" imageName="xls" tooltip="Export Excel" view="cpxls" />

            <ec:exportCsv fileName="listeDesTypesImpots.csv" tooltip="Export CSV" delimiter="|" viewResolver="csv" />

            <ec:exportPdfJrxml fileName="testfmt2.pdf" tooltip="Export PDF (jrxml)" imageName="pdf" text="Export PDF (jrxml)"
                nomapplication="DMO Test" modele="portrait" textepieddepage="Copyright DGCP " />

            <ec:row highlightRow="true">
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
                    title="monnaie une decimale" alias="monnaiezerodecimale">
			  ${ROWCOUNT} - ${COROWCOUNT} - Page : ${PAGECOUNT} 
			  </ec:column>
                <ec:column property="seuil" title="monnaie une decimale" sortable="true" headerStyle="text-align: center"
                    style="text-align: right;" width="16%" cell="monnaiecellunedecimale" alias="monnaieunedecimale"
                    calc="pagetotal,subtotal,decomptenonnulls" calcTitle="Total par page,Sous-total,Total" calcDisplay="first,first,last"
                    calcExport="all,allbutlast,last" calcClass="" calcStyle="text-align: right;" calcTitleClass="" calcTitleStyle="" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
                    cell="monnaiecelldeuxdecimales" title="monnaie deux decimales" alias="monnaiedeuxdecimales"
                    calc="pagetotal,subtotal,total" calcTitle="Total,Decompte" calcStyle="text-align: right;" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
                    cell="monnaiecelltroisdecimales" title="monnaie trois decimales" alias="monnaietroisdecimales"
                    calc="pagetotal,subtotal,total" calcTitle="Total,Decompte" calcStyle="text-align: right;" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
                    cell="monnaiecellquatredecimales" title="monnaie quatre decimales" alias="monnaiequatredecimales"
                    calc="pagetotal,subtotal,total" calcTitle="Total,Decompte" calcStyle="text-align: right;" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
                    cell="monnaiecellcinqdecimales" title="monnaie cinq decimales" alias="monnaiecinqdecimales"
                    calc="pagetotal,subtotal,total" calcTitle="Total,Decompte" calcStyle="text-align: right;" />
            </ec:row>
        </ec:table>

        <ec:table items="referencetypesimpots" action="${pageContext.request.contextPath}/zf7/flux.ex" tableId="testfmt2"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml" filterable="true" showPagination="false"
            showExports="true" var="row" sortRowsCallback="cp" filterRowsCallback="cp">

            <ec:exportPdf fileName="listeDesTypesImpots.pdf" tooltip="Export PDF" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="Test de formats monétaires" imageName="pdf" view="cppdf" />

            <ec:exportXls encoding="UTF" fileName="listeDesTypesImpots.xls" imageName="xls" tooltip="Export Excel" view="cpxls" />

            <ec:exportCsv fileName="listeDesTypesImpots.csv" tooltip="Export CSV" delimiter="|" viewResolver="csv" />

            <ecjrxml:exportPdf fileName="testfmt2.pdf" view="cpjrxml" viewResolver="cpjrxml" tooltip="Export PDF (jrxml)" imageName="pdf"
                text="Export PDF (jrxml)" nomapplication="DMO Test" modele="portrait" textepieddepage="Copyright DGCP " />

            <ec:row highlightRow="true">
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
                    cell="monnaiecellsixdecimales" title="monnaie six decimales" alias="monnaiesixdecimales" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
                    cell="monnaiecellseptdecimales" title="monnaie sept decimales" alias="monnaiedeuxdecimales" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
                    cell="monnaiecellhuitdecimales" title="monnaie huit decimales" alias="monnaiehuitdecimales" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
                    cell="monnaiecellneufdecimales" title="monnaie neuf decimales" alias="monnaieneufdecimales" />
                <ec:column property="seuil" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
                    cell="monnaiecelldixdecimales" title="monnaie dix decimales" alias="monnaiedixdecimales" />
            </ec:row>
        </ec:table>


        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" />
                    <app:submit label="Retourner à l'accueil" transition="retour" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" responsive="${responsive}"
                         />
                    <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
</app:page>