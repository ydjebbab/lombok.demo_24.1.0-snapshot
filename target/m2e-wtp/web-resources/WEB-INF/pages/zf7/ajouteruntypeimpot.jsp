<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page ajouteruntypeimpot.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Ajouter un type d'impôt" menu="true" responsive="${responsive}">

    <app:chemins action="zf7/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Gestion des types d'impôt" transition="listeimpots" responsive="${responsive}">Gestion des types d'impôt</app:chemin>
        <app:chemin title="Ajouter un type d'impôt" responsive="${responsive}">Ajouter un type d'impôt</app:chemin>
    </app:chemins>

    <app:form action="zf7/flux.ex" formobjectname="referenceform" responsive="${responsive}">

        <c:set var="readonly" value="false" />
        <p>
            <br />
        </p>
        <app:input attribut="typeImpot.codeImpot" maxlength="32" size="32" readonly="${readonly}" requis="true" responsive="${responsive}" />
        <app:input attribut="typeImpot.nomImpot" maxlength="32" size="32" readonly="${readonly}" requis="true" responsive="${responsive}" />
        <app:checkbox attribut="typeImpot.estOuvertALaMensualisation" requis="false" readonly="${readonly}" responsive="${responsive}" />

        <app:input attribut="typeImpot.jourOuvertureAdhesion" maxlength="10" size="10" readonly="${readonly}" inputboxwidth="10%"
            placeholder="JJ/MM/AAAA" requis="false" responsive="${responsive}">
            <app:calendar />
        </app:input>

        <app:dateheure attribut="typeImpot.jourLimiteAdhesion" readonly="${readonly}" requis="false" dateouverture="01/01/1995"
            responsive="${responsive}" />
        <app:checkbox attribut="typeImpot.estDestineAuxParticuliers" requis="false" readonly="${readonly}" responsive="${responsive}" />
        <app:input attribut="typeImpot.taux" maxlength="32" size="32" readonly="${readonly}" requis="true" responsive="${responsive}" />
        <app:input attribut="typeImpot.seuil" maxlength="32" size="32" readonly="${readonly}" requis="true" responsive="${responsive}" />
        <app:boxboutons>
            <app:submit label="Retourner à la liste des types d'impôts" transition="listeimpots" responsive="${responsive}" />
            <app:submit label="Enregistrer" transition="enregistrer" responsive="${responsive}" />
        </app:boxboutons>

    </app:form>
</app:page>