<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page affichertypesimpots.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Gestion des types d'impôt" menu="true" responsive="${responsive}">

    <app:chemins action="zf7/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Gestion des types d'impôt" responsive="${responsive}">Gestion des types d'impôt</app:chemin>
    </app:chemins>

    <app:form action="zf7/flux.ex" formboxclass="eXtremeTable" formobjectname="referenceform" responsive="${responsive}">

       
        <ec:table title="table.title.referencetypesimpots" items="referencetypesimpots" tableId="typesimpots" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/zf7/flux.ex" state="persist" stateAttr="gotofirstpage" autoIncludeParameters="false"
            form="formulaire" locale="fr_FR" filterable="true" sortable="true" sortRowsCallback="cp" filterRowsCallback="cpfiltreinfsup"
            rowsDisplayed="10" showPagination="true" showNewPagination="true" showExports="true" newPaginationNbPage="5"
            showStatusBar="true" showPaginationBottom="false" showExportsBottom="false" showNewFilter="true"
            onInvokeAction="ecupdatectra('tableaunavigation')" libellenoresultsfound="Aucun type d'impôt n'a été trouvé"
            positionnoresultsfound="body">

            <ec:exportPdfJrxml fileName="listeDesTypesImpots.pdf" tooltip="Export PDF (jrxml)" imageName="pdf" text="Export PDF (jrxml)"
                nomapplication="DMO Test" modele="portrait" textepieddepage="Copyright DGCP " />

            <ec:exportXls encoding="UTF" fileName="listeDesTypesImpots.xls" imageName="xls" tooltip="Export Excel" view="cpxls" />

            <ec:exportOds encoding="UTF" fileName="listeDesTypesImpots.ods" imageName="ods" tooltip="Export Excel" view="cpods" />

            <ec:exportCsv fileName="listeDesTypesImpots.csv" tooltip="Export CSV" delimiter="|" viewResolver="csv" />

            <ec:parameter name="_eventId_tableaunavigation" value="tableaunavigation" />
            <ec:row highlightRow="true" interceptor="fr.gouv.finances.cp.dmo.interceptor.TypeImpotRowInterceptor">               
                
                <ec:column property="codeImpot" title="Code impôt" width="20%" sortable="true" filterable="false" style="text-align: center"
                    headerStyle="text-align: center" cell="nombrecellzerodecimale" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col" />
                    
<%--               <ec:column property="codeImpot" title="Code impôt" width="20%" sortable="true" filterable="true" style="text-align: center" 
                     headerStyle="text-align: center" cell="nombrecellzerodecimale" showResponsive="${responsive}" 
                     dataAttributes="data-tablesaw-priority=persist scope=col" /> --%>
                     
                <ec:column property="nomImpot" title="Nom impôt" width="20%" sortable="true" style="text-align: right"
                    headerStyle="text-align: center" cell="" alias="nomImpot" calc="decompte,decomptenonnulls "
                    calcTitle="decompte,decomptenonnulls" calcDisplay="last,last" calcExport="last,last" calcClass=","
                    calcStyle="text-align: right;" calcTitleClass="" calcTitleStyle="" filterable="false" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col" escapeXml="false">
                    <app:link action="zf7/flux.ex" transition="modifier" attribute="id" value="${row.id}" label="${row.nomImpot}"
                        active="true" title="Editer le type d'impôt" />
                </ec:column>
                
<%--                 <ec:column property="nomImpot" title="Nom impôt" width="20%" sortable="true" style="text-align: right" 
                     headerStyle="text-align: center" cell="" alias="nomImpot" calc="decompte,decomptenonnulls " 
                    calcTitle="decompte,decomptenonnulls" calcDisplay="last,last" calcExport="last,last" calcClass="," 
                     calcStyle="text-align: right;" calcTitleClass="" calcTitleStyle="" filterable="true" showResponsive="${responsive}" 
                    dataAttributes="data-tablesaw-priority=persist scope=col" escapeXml="false"> 
                    <app:link action="zf7/flux.ex" transition="modifier" attribute="id" value="${row.id}" label="${row.nomImpot}" 
                         active="true" title="Editer le type d'impôt" /> 
                 </ec:column> --%>
                 
                <ec:column property="estOuvertALaMensualisation" filterCell="droplist" title="Ouvert à la mensualisation" width="10%"
                    sortable="true" filterable="true" cell="booleanvraifaux" style="text-align: center" headerStyle="text-align: center"
                    viewsDenied="cpjrxml" filterNullOption="Tous" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-sortable-col data-tablesaw-priority=1 scope=col" />
                <ec:column property="estOuvertALaMensualisationString" filterCell="droplist" title="Ouvert à la mensualisation" width="10%"
                    sortable="true" filterable="true" style="text-align: center" headerStyle="text-align: center" viewsDenied="cphtml"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-sortable-col data-tablesaw-priority=2 scope=col" />
                <ec:column alias="test" cell="image" property="estOuvertALaMensualisation" title="test" width="10%" sortable="true"
                    filterable="true" style="text-align: center" headerStyle="text-align: center"
                    interceptor="fr.gouv.finances.cp.dmo.interceptor.TypeImpotColumnInterceptor" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-sortable-col data-tablesaw-priority=3 scope=col" />
                <ec:column property="estDestineAuxParticuliers" title="Destiné aux particuliers" width="10%" sortable="true"
                    filterable="true" cell="booleanouinon" style="text-align: center" headerStyle="text-align: center" viewsDenied="cpjrxml"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-sortable-col data-tablesaw-priority=4 scope=col" />
                <ec:column property="estDestineAuxParticuliersString" title="Destiné aux particuliers" width="10%" sortable="true"
                    filterable="true" style="text-align: center" headerStyle="text-align: center" viewsDenied="cphtml"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-sortable-col data-tablesaw-priority=5 scope=col" />
                <ec:column property="jourOuvertureAdhesion" title="Jour d'ouverture de l'adhésion" width="10%" sortable="true"
                    filterable="true" style="text-align: center" headerStyle="text-align: center" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-sortable-col data-tablesaw-priority=6 scope=col" />
                <ec:column property="jourLimiteAdhesion" title="Date limite de l'adhésion" width="10%" cell="dateheureminute"
                    sortable="true" filterable="true" style="text-align: center" headerStyle="text-align: center"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-sortable-col data-tablesaw-priority=7 scope=col" />
                <ec:column property="seuil" title="Seuil" width="15%" cell="nombrecelldeuxdecimales" sortable="true" filterable="true"
                    style="text-align: center;" headerStyle="text-align: center" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-sortable-col data-tablesaw-priority=8 scope=col" />
                <ec:column property="taux" title="Taux" width="10%" cell="nombrecelldeuxdecimales" sortable="true" filterable="true"
                    style="text-align: center;" headerStyle="text-align: center" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-sortable-col data-tablesaw-priority=9 scope=col" />
                  <ech:column property="chkbx" cell="cpcheckboxcell" rowid="id" viewsAllowed="cphtml" headerCell="cpselectAll" width="5%"
                    filterable="false" sortable="false" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col" />
            </ec:row>
        </ec:table>
        <p>
            <br />
        </p>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Ajouter un type d'impôt" transition="ajouter" />
                    <app:submit label="Editer le type d'impôt sélectionné" transition="editer" />
                    <app:submit label="Supprimer les types d'impôts sélectionnés" transition="supprimer" />
                    <app:submit label="Retourner à l'accueil" transition="retour" />
                    <app:submit label="Pages de test" transition="pagesdetest" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Ajouter un type d'impôt" transition="ajouter" responsive="${responsive}"  />
                    <app:submit label="Editer le type d'impôt sélectionné" transition="editer" responsive="${responsive}"
                         />
                    <app:submit label="Supprimer les types d'impôts sélectionnés" transition="supprimer" responsive="${responsive}"
                         />
                    <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}"  />
                    <app:submit label="Pages de test" transition="pagesdetest" responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>