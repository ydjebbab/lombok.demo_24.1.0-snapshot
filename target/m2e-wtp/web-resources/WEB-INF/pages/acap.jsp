<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>
<!--  Page acap.jsp -->
<c:set var="responsive" value="true"/>

 
<app:page titreecran="DMO" 
          titrecontainer="Bienvenue sur la page affichant le cookie acap"
          menu="true" 
          donneescontainerboxclass="informationcontainer"
          responsive="${responsive}">
			   
<div>
<p>Nom du cookie acap  : ${cookieACAP.name}</p>
<p>Value du cookie acap:${cookieACAP.value}</p>
</div>
   
</app:page>
