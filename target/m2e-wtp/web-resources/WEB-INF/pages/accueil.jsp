<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page accueil.jsp -->

<spring:eval
    expression="@environment.getProperty('appli.inea.version')!=null?!@environment.getProperty('appli.inea.version').isEmpty():false"
    var="isIneaStyle" />


<c:choose>
    <c:when test="${isIneaStyle eq true}">
        <c:redirect url="/accueil.inea" />
    </c:when>
    <c:otherwise>
        <!--  Page accueil.jsp -->
        <c:set var="responsive" value="true" />

        <app:page titreecran="DMO" titrecontainer="Bienvenue sur le projet démo lombok" menu="true"
            donneescontainerboxclass="informationcontainer" responsive="${responsive}">

            <p>
               <img src="application/img/profile.png"> Profils activés:
                <spring:eval expression="@environment.getProperty('spring.profiles.active')" var="profilsActives" />
                <c:out value="${profilsActives}"></c:out>
            </p>
            <p>
               <img src="application/img/module.png"> Modules activés:
                <spring:eval expression="@environment.getProperty('lombok.composant.sireme.inclus')" var="sireme" />
                <c:if test="${sireme eq true }">sireme - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.journal.inclus')" var="journal" />
                <c:if test="${journal eq true }">journal - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.journalws.inclus')" var="journalws" />
                <c:if test="${journalws eq true }">journalws - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.bancaire.inclus')" var="bancaire" />
                <c:if test="${bancaire eq true }">bancaire - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.atlas.inclus')" var="atlas" />
                <c:if test="${atlas eq true }">atlas - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.upload.inclus')" var="upload" />
                <c:if test="${upload eq true }">upload - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.adresse.inclus')" var="adresse" />
                <c:if test="${adresse eq true }">adresse - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.structure.inclus')" var="structure" />
                <c:if test="${structure eq true }">structure - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.clamav.inclus')" var="clamav" />
                <c:if test="${clamav eq true }">clamav - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.clamav.simulation.inclus')" var="clamavsimulation" />
                <c:if test="${clamavsimulation eq true }">clamav.simulation - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.clamav.simulation.offline.inclus')" var="clamavsimulationoffline" />
                <c:if test="${clamavsimulationoffline eq true }">clamav.simulation.offline - </c:if>                
                <spring:eval expression="@environment.getProperty('lombok.composant.itm.inclus')" var="itm" />
                <c:if test="${itm eq true }">itm - </c:if>                
                <spring:eval expression="@environment.getProperty('lombok.composant.ws.inclus')" var="ws" />
                <c:if test="${ws eq true }">ws - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.edition.inclus')" var="edition" />
                <c:if test="${edition eq true }">edition - </c:if>
                <spring:eval expression="@environment.getProperty('lombok.composant.editionsansbase.inclus')" var="editionsansbase" />
                <c:if test="${editionsansbase eq true }">editionsansbase </c:if>                
            </p>
            
            <c:choose>
                <c:when test="${fn:contains(profilsActives, 'postgre')}">
                    <p>
                        <img src="application/img/database.png"> Base principale :<br /> URL :
                        <spring:eval expression="@environment.getProperty('db.url')" />
                        <br /> Login :
                        <spring:eval expression="@environment.getProperty('db.username')" />
                    </p>
                    <p>
                        <img src="application/img/database.png"> Base des éditions :<br /> URL :
                        <spring:eval expression="@environment.getProperty('jasperdb.url')" />
                        <br /> Login :
                        <spring:eval expression="@environment.getProperty('jasperdb.username')" />
                    </p>
                </c:when>
                <c:otherwise>
                    Base embedded HSQL
                </c:otherwise>
            </c:choose>


            <%-- 
  A ajouter pour forcer l'accès à un flux sans passer par la page d'accueil
   <c:if test="${!empty FLUX_ACCUEIL}">
       <c:redirect url="/${pageContext.request.contextPath}/zf/flux.ex?_flowId=${FLUX_ACCUEIL}"/>
   </c:if>
   --%>
        </app:page>
    </c:otherwise>
</c:choose>