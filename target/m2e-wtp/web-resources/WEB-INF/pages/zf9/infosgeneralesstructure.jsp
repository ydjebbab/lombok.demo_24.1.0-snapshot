<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page infosgeneralesstructure.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Recherche d'une structure par codique" titrecontainer="Saisie du codique de la structure" menu="true">

    <app:onglets cleactive="1">
        <app:onglet cle="1" libelle="Informations générales" />
        <app:onglet cle="2" libelle="Références bancaires" transition="coordonneesbancairesstructure" />
    </app:onglets>

    <app:form action="zf9/flux.ex" formobjectname="recherchelibellestructureform" formboxclass="donnees sousonglets">

        <div class="eXtremeTable">
            <table class="tableRegion">
                <tr>
                    <th class="tableHeader" rowspan="1">Catégorie</th>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.laCategorieStructureCP.libellecategorie}</td>
                </tr>
                <tr>
                    <th class="tableHeader" rowspan="5">Adresse</th>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.ladresseGeographique.ligne2ServiceAppartEtgEsc}&nbsp;</td>
                </tr>
                <tr>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.ladresseGeographique.ligne3BatimentImmeubleResid}&nbsp;</td>
                </tr>
                <tr>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.ladresseGeographique.ligne4NumeroLibelleVoie}&nbsp;</td>
                </tr>
                <tr>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.ladresseGeographique.ligne5LieuDitMentionSpeciale}&nbsp;</td>
                </tr>
                <tr>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.ladresseGeographique.codePostalEtCedex}&nbsp;
                        ${recherchelibellestructureform.uneStructureSelectionnee.ladresseGeographique.ville}</td>
                </tr>
                <tr>
                    <th class="tableHeader" rowspan="5">Adresse Postale</th>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.ladressePostale.ligne2ServiceAppartEtgEsc}&nbsp;</td>
                </tr>
                <tr>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.ladressePostale.ligne3BatimentImmeubleResid}&nbsp;</td>
                </tr>
                <tr>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.ladressePostale.ligne4NumeroLibelleVoie}&nbsp;</td>
                </tr>
                <tr>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.ladressePostale.ligne5LieuDitMentionSpeciale}&nbsp;</td>
                </tr>
                <tr>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.ladressePostale.codePostalEtCedex}&nbsp;
                        ${recherchelibellestructureform.uneStructureSelectionnee.ladressePostale.ville}</td>
                </tr>
                <tr>
                    <th class="tableHeader">Téléphone</th>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.telStandard}</td>
                </tr>
                <tr>
                    <th class="tableHeader">Télécopie</th>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.telecopie}</td>
                </tr>
                <tr>
                    <th class="tableHeader">Mail</th>
                    <td>${recherchelibellestructureform.uneStructureSelectionnee.adresseMelGenerique}</td>
                </tr>
            </table>
        </div>
        <%-- Si les horaires sont disponibles, on les affiches --%>
        <c:if test="${!empty recherchelibellestructureform.uneStructureSelectionnee.lesHorairesCP}">

            <p>Horaires d'ouverture</p>
            <ec:table items="recherchelibellestructureform.uneStructureSelectionnee.lesHorairesCP"
                action="${pageContext.request.contextPath}/zf9/flux.ex" tableId="detailstructurescp" autoIncludeParameters="false"
                form="formulaire" locale="fr_FR" view="cpgroup" showStatusBar="false" filterable="false" showPagination="false"
                showExports="false" var="row" sortRowsCallback="cp" filterRowsCallback="cpfiltreinfsup">
                <ec:row>
                    <ech:column group="&nbsp;" property="joursemaine" title="&nbsp;" sortable="true" style="text-align: center"
                        headerStyle="text-align: center" />
                    <ech:column group="Matin" property="matindebut" title="De" sortable="true" style="text-align: center"
                        headerStyle="text-align: center" />
                    <ech:column group="Matin" property="matinfin" title="A" sortable="true" style="text-align: center"
                        headerStyle="text-align: center" />
                    <ech:column group="Après midi" property="apremdebut" title="De" sortable="true" style="text-align: center"
                        headerStyle="text-align: center" />
                    <ech:column group="Après midi" property="apremfin" title="A" sortable="true" style="text-align: center"
                        headerStyle="text-align: center" />
                </ec:row>
            </ec:table>
        </c:if>

        <app:boxboutons>
            <app:submit label="Retourner à la liste" transition="RetourListe" />
        </app:boxboutons>
    </app:form>
</app:page>