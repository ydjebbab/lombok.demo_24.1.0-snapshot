<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisierib.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Recherche de guichet bancaire" titrecontainer="Saisie du codique de la structure" menu="true"
    responsive="${responsive}">

    <app:chemins action="zf9/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Recherche de guichet bancaire" responsive="${responsive}">Recherche de guichet bancaire</app:chemin>
    </app:chemins>

    <!-- Problème au niveau du responsive pour les tag app:inputmultiple -->

    <p>Code banque (demo=10037):Code guichet (demo=00323):Numéro de compte (demo=01234567890):Clé RIB (demo=76):</p>

    <app:onglets cleactive="1" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Saisie rib" responsive="${responsive}" ongletclass="btmodel_inv_half" />
        <app:onglet cle="2" libelle="Détail bancaire" responsive="${responsive}" ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form action="zf9/flux.ex" formobjectname="rechercheguichetbancaireform" formboxclass="donnees sousonglets"
        responsive="${responsive}">

        <app:inputmultiple theme="H" libelle="RIB" nbfield="4" f1attribut="rib.codeBanque" f1maxlength="5" f1size="5"
            f1libelle="Code banque" f1consigne="Le code banque doit être composé de 5 caractères" f2attribut="rib.codeGuichet"
            f2maxlength="5" f2size="5" f2libelle="Code guichet" f2consigne="Le code guichet doit être composé de 5 caractères"
            f3attribut="rib.numeroCompte" f3maxlength="11" f3size="11" f3libelle="Numéro de compte"
            f3consigne="Le numéro de compte doit être composé de 11 caractères" f4attribut="rib.cleRib" f4maxlength="2" f4size="2"
            f4libelle="Clé" f4consigne="La clé doit être composée de 2 caractères">
        </app:inputmultiple>

        <app:boxboutons>
            <app:submit label="Retourner à l'accueil" transition="Annuler" responsive="${responsive}" />
            <app:submit label="Rechercher les caractéristiques du guichet bancaire" transition="Rechercher" id="enterButton"
                inputclass="primary" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>