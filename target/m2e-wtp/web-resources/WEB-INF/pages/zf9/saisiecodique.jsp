<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisiecodique.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Recherche d'une structure par codique" menu="true" responsive="${responsive}">

    <app:chemins action="zf9/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Recherche de structures" transition="AutreRecherche" responsive="${responsive}">Recherche de structures</app:chemin>
        <app:chemin title="Recherche par codique et code annexe" responsive="${responsive}">Recherche par codique et code annexe</app:chemin>
    </app:chemins>

    <app:onglets cleactive="1" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Saisie codique" responsive="${responsive}" ongletclass="btmodel_inv_half" />
        <app:onglet cle="2" libelle="Détail structure" responsive="${responsive}" ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form action="zf9/flux.ex" formobjectname="recherchelibellestructureform" formboxclass="donnees sousonglets"
        responsive="${responsive}">

        <app:input attribut="unestructurearechercher.codique" libelle="codique structure (demo=005016,005000,005023) :" maxlength="6"
            size="6" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.codeAnnexe" libelle="code annexe structure (demo=0) :" maxlength="1" size="1"
            responsive="${responsive}" />

        <app:boxboutons>
            <app:submit label="Autre recherche" transition="AutreRecherche" responsive="${responsive}" />
            <app:submit label="Rechercher" transition="Rechercher" id="enterButton" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>