<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page choixtypederecherche.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Recherche de structures" menu="true" responsive="${responsive}">

    <app:chemins action="zf9/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Recherche de structures" responsive="${responsive}">Recherche de structures</app:chemin>
    </app:chemins>

    <app:form method="post" action="zf9/flux.ex" formid="recherchelibellestructure" formobjectname="recherchelibellestructureform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees" responsive="${responsive}">

        <div class="table-responsive wrapped">
            <table class="table" border="1">
                <thead>
                    <tr>
                        <td>Type de recherche</td>
                    </tr>
                </thead>
                <tr>
                    <td><app:button action="zf9/flux.ex" transition="saisiecodique">Recherche par codique et code annexe</app:button></td>
                </tr>
                <tr>
                    <td><app:button action="zf9/flux.ex" transition="saisiecategories">Recherche par catégories</app:button></td>
                </tr>
                <tr>
                    <td><app:button action="zf9/flux.ex" transition="saisiedepartement">Recherche par département</app:button></td>
                </tr>
                <tr>
                    <td><app:button action="zf9/flux.ex" transition="saisiecodesannexes">Recherche par code annexe</app:button></td>
                </tr>
                <tr>
                    <td><app:button action="zf9/flux.ex" transition="saisiedepartementsetcategories">Recherche par catégories et département</app:button></td>
                </tr>
                <tr>
                    <td><app:button action="zf9/flux.ex" transition="saisiecodesannexesetcategories">Recherche par catégories et codes annexes</app:button></td>
                </tr>
                <tr>
                    <td><app:button action="zf9/flux.ex" transition="saisiecodesannexesetcategoriesetdepartement">Recherche par catégories, par codes annexes et par département</app:button></td>
                </tr>
            </table>
        </div>

        <app:boxboutons>
            <app:submit label="Retour à l'accueil" transition="RetourAccueil" />
        </app:boxboutons>

    </app:form>
</app:page>