<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisiedepartement.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Recherche d'une structure par département" titrecontainer="Sélection un département" menu="true"
    responsive="${responsive}">

    <app:chemins action="zf9/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Recherche de structures" transition="AutreRecherche" responsive="${responsive}">Recherche de structures</app:chemin>
        <app:chemin title="Recherche par départements" responsive="${responsive}">Recherche par départements</app:chemin>
    </app:chemins>

    <app:form action="zf9/flux.ex" formobjectname="recherchelibellestructureform" formboxclass="donnees">

        <app:select attribut="departement" itemslist="${departements}" libelle="Département :" label="codePlusLibelleDepartement"
            value="codedepartementINSEE" theme="V" boxwidth="100%" requis="false" defautitem="false" labelboxwidth="18%" inputboxwidth="40%"
            compboxwidth="27%" consigne="Sélectionner département">
        </app:select>

        <app:boxboutons>
            <app:submit transition="recherche_par_departement" label="Rechercher par département"
                title="Recherche toutes les structures qui appartiennent à un département." responsive="${responsive}" />
            <app:submit label="Autre recherche" transition="AutreRecherche" responsive="${responsive}" />
            <app:submit label="Retour à l'accueil" transition="RetourAccueil" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>