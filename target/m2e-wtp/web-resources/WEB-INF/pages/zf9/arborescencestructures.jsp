<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page arborescencestructures.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Consultation des structures" menu="true" responsive="${responsive}">

    <app:chemins action="zf9/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Consultation des structures" responsive="${responsive}">Consultation des structures</app:chemin>
    </app:chemins>


    <app:form method="post" action="zf9/flux.ex" formid="consultationstructuresform" formobjectname="consultationstructuresform"
        formboxid="donnees" formboxclass="donnees" responsive="${responsive}">

        <div style="width: 30%; border: thin; border-style: solid; border-color: gray; float: left; display: block;">
            <app:button action="zf9/flux.ex" transition="collapseAll" responsive="${responsive}">Tout réduire</app:button>
            <app:button action="zf9/flux.ex" transition="expandAll" responsive="${responsive}">Tout développer</app:button>
            <app:button action="zf9/flux.ex" transition="unSelectAll" responsive="${responsive}">Tout dé-sélectionner</app:button>

            <app:input attribut="text" libelle="" size="15" maxlength="40" labelboxwidth="0%" inputboxwidth="50%" compboxwidth="45%"
                requis="false" responsive="${responsive}">
                <app:submit transition="findNodesByName" label="Rechercher" responsive="${responsive}" />
            </app:input>

            <div style="clear: both;">&nbsp;</div>

            <div id="treescroll" style="overflow: scroll; height: 600px;"
                onscroll="javascript:document.getElementById('scroll').value =this.scrollTop;">

                <lb:tree tree="tree.model" includeRootNode="false" action="zf9/flux.ex">
                    <lb:nodes>
                        <lb:nodenavigation />
                        <%-- Noeuds de premier niveau--%>
                        <lb:nodeMatch type="POSTES_EN_FRANCE">
                            <lb:nodetext selectable="false" inline="true" navigable="true">
                                <lb:nodeName />
                            </lb:nodetext>
                        </lb:nodeMatch>
                        <lb:nodeMatch type="POSTES_A_LETRANGER">
                            <lb:nodetext selectable="true" inline="true" navigable="false">
                                <lb:nodeName />
                            </lb:nodetext>
                        </lb:nodeMatch>
                        <lb:nodeMatch type="STR_ATTR_SPEC">
                            <lb:nodetext selectable="true" inline="true" navigable="false">
                                <lb:nodeName />
                            </lb:nodetext>
                        </lb:nodeMatch>
                        <lb:nodeMatch type="SER_CENT_DGCP">
                            <lb:nodetext selectable="true" inline="true" navigable="false">
                                <lb:nodeName />
                            </lb:nodetext>
                        </lb:nodeMatch>
                        <lb:nodeMatch type="STRUC_FORMATION">
                            <lb:nodetext selectable="true" inline="true" navigable="false">
                                <lb:nodeName />
                            </lb:nodetext>
                        </lb:nodeMatch>

                        <%-- Noeuds de deuxième niveau--%>
                        <lb:nodeMatch type="REGION">
                            <lb:nodetext selectable="false" inline="true" navigable="true">
                                <lb:nodeName />
                            </lb:nodetext>
                        </lb:nodeMatch>
                        <lb:nodeMatch type="DEPARTEMENT">
                            <lb:nodetext selectable="true" inline="false" navigable="false">
                                <lb:nodeName />
                            </lb:nodetext>
                        </lb:nodeMatch>
                    </lb:nodes>
                </lb:tree>
            </div>
        </div>
        <!-- </body> -->

        <c:if test="${!empty consultationstructuresform.structuresSelectionnees}">

            <div style="width: 68%; float: right; border: thin; border-style: solid; border-color: gray;">

                <c:set var="structuresSelectionnees" value="${consultationstructuresform.structuresSelectionnees}" />

                <ec:table items="structuresSelectionnees" form="consultationstructuresform" tableId="consarbre" view="cphtml"
                    action="zf9/flux.ex" filterable="false" sortable="false" showTitle="false" showExports="false" showPagination="true"
                    var="row" autoIncludeParameters="false" rowsDisplayed="30">
                    <ec:parameter name="_eventId_tableaunavigation" value="tableaunavigation" />
                    <ec:row>
                        <ec:column title="Ville" property="ladresseGeographique.ville" width="20%"></ec:column>
                        <ec:column title="Codique" property="codique" width="10%"></ec:column>
                        <ec:column property="Nom du poste" width="50%">
                            <lb:nodelink action="zf9/flux.ex" transition="consulterDetail" attribute="identitenominoe"
                                value="${row.identiteNOMINOE}">${row.liDenominationStandard}</lb:nodelink>
                        </ec:column>
                        <ec:column title="Téléphone" property="telStandard" width="20%"></ec:column>
                    </ec:row>
                </ec:table>
            </div>

        </c:if>
        <%--
Fix pour firefox et ie7
Ce div est ajouté pour forcer le div englobant (donnees) à s'étendre autour
des deux div flottant.
En utilisant clear:both dans le style de ce div, cet élément définit se positionne
sous les div flottants précédents et comme lui-même n'est pas flottant, il force
le div parent (donnees) à s'étendre pour l'englober
//TODO voir s'il faut ajouter ce div dans le tag app:form
 --%>
        <div style="clear: both;"></div>

    </app:form>

</app:page>
