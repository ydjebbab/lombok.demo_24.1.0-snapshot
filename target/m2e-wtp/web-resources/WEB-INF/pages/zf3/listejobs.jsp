<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page listejobs.jsp -->
<app:page titreecran="Liste des jobs">

    <app:chemins action="zf2/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Tableaux et éditions statistiques" transition="NouvelleRecherche">Tableaux et éditions statistiques</app:chemin>
        <app:chemin title="Liste des jobs">Liste des jobs</app:chemin>
    </app:chemins>


    <app:form action="zf3/flux.ex" formid="formulaire" formboxclass="eXtremeTable">
        <ec:table items="listjobHistory" tableId="listejobs" view="cphtml" action="flux.ex" state="persist" autoIncludeParameters="false"
            form="formulaire" locale="fr_FR" filterable="false" sortRowsCallback="cp" filterRowsCallback="cp" rowsDisplayed="10"
            showPagination="true" var="row">
            <ec:parameter name="_eventId_tableaunavigation" value="tableaunavigation" />
            <ec:row highlightRow="true">
                <ec:column property="editionUuid" title="editionUuid">
                </ec:column>
                <ec:column property="startTime" title="startTime"></ec:column>
                <ec:column property="endTime" title="endTime">
                </ec:column>
                <ec:column property="status" title="status">
                </ec:column>
                <ec:column property="result" title="result">
                    <app:link uri="${pageContext.request.contextPath}/${row.result}" openwindow="true" label="${row.description}"
                        active="true" />
                </ec:column>
                <ec:column property="cheminComplet" title="cheminComplet">
                </ec:column>
                <ec:column property="nomFicStockage" title="nomFicStockage">
                </ec:column>
                <ec:column property="nomFicDownload" title="nomFicDownload">
                </ec:column>
                <ec:column property="message" title="message">
                </ec:column>
                <ec:column property="uidProprietaire" title="uidProprietaire">
                </ec:column>
                <ec:column property="applicationOrigine" title="applicationOrigine">
                </ec:column>
            </ec:row>
        </ec:table>

        <%-- 
${pageContext.request.contextPath}
--%>
        <app:boxboutons>
            <app:submit label="Retourner à la page précédente" transition="retour" />
        </app:boxboutons>
    </app:form>
</app:page>