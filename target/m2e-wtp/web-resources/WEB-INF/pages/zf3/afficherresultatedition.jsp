<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page afficherresultatedition.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Edition produite" menu="true" responsive="${responsive}">
    <app:form method="post" action="zf3/flux.ex" formid="resultatscreation" formobjectname="statistiquesform"
        onsubmit="submit_form_sans_attente()">
        <p>
            L'édition demandée est accessible à l'adresse suivante : (
            <app:lienedition jobhistory="${jobHistory}" openwindow="false" />
            ) .
        </p>

        <app:boxboutons>
            <app:submit label="Retourner à la page précédente" transition="retour" responsive="${responsive}" />
        </app:boxboutons>

    </app:form>
</app:page>