<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page agregcontrpartypeimpot.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Agrégation par type d'impôt des contribuables" menu="true" responsive="${responsive}">

    <app:chemins action='zf3/flux.ex' responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Tableaux et éditions statistiques" responsive="${responsive}" transition="retour">Tableaux et éditions statistiques</app:chemin>
        <app:chemin title="Agrégation par type d'impôt des contribuables" responsive="${responsive}">Agrégation par type d'impôt des contribuables</app:chemin>
    </app:chemins>

    <app:onglets cleactive="2" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Tableaux et éditions statistiques" responsive="${responsive}" transition="retour"
            ongletclass="btmodel_inv_half" />
        <app:onglet cle="2" libelle="Avis d'imposition" responsive="${responsive}" ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form method="post" action="zf3/flux.ex" formid="statistiques" formobjectname="statistiquesform" onsubmit="return submit_form(500)"
        formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}">

        <ec:table items="statistiques" action="flux.ex" showTitle="true" title="Répartition par type d'impôt" form="statistiques"
            view="cpgroup" autoIncludeParameters="false" var="row" showPagination="true" showStatusBar="false" filterable="true"
            sortable="false" locale="fr_FR" width="100%" theme="eXtremeTable centerTable">

            <ec:exportPdf fileName="zf3.agregcontripartypeimpot.edition" text="zf3.agregcontripartypeimpot.parametres" view="jasperhtml"
                viewResolver="jasperhtml" tooltip="Export HTML" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="Agregation des contribuables par avis type d'impôt" imageName="html" />
            <ec:row>
                <ech:column group="Impôts" property="codeImpot" title="Code" width="12%" sortable="true" />
                <ech:column group="Impôts" property="nomImpot" title="Libellé" width="28%" filterable="true" />
                <ech:column group="Avis d'imposition" property="nombreContribuables"
                    title="Nombre de contribuables possédant au moins un avis" width="36%" />
                <ech:column group="Avis d'imposition" property="montantMoyen" title="Montant moyen" cell="currency" format="### ### ###0,00"
                    width="12%" />
                <ech:column group="Avis d'imposition" property="montantTotal" title="Montant total" cell="currency" format="### ### ###0,00"
                    width="12%" />
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à la page précédente" transition="retour" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à la page précédente" transition="retour" responsive="${responsive}"
                         />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>