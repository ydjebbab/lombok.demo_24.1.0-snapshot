<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page agregavispartypeimpotseuil.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Agrégation par type d'impôt des avis d'imposition dont le montant est supérieur au 'seuil d'agrégation'" menu="true"
    responsive="${responsive}">

    <app:chemins action='zf3/flux.ex' responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Tableaux et éditions statistiques" responsive="${responsive}" transition="retour">Tableaux et éditions statistiques</app:chemin>
        <app:chemin title="Agrégation par type d'impôt des avis d'imposition dont le montant est supérieur au 'seuil d'agrégation'"
            responsive="${responsive}">Agrégation par type d'impôt des avis d'imposition dont le montant est supérieur au 'seuil d'agrégation'</app:chemin>
    </app:chemins>

    <app:onglets cleactive="2" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Tableaux et éditions statistiques" responsive="${responsive}" transition="retour"
            ongletclass="btmodel_inv_half" />
        <app:onglet cle="2" libelle="Avis d'imposition" responsive="${responsive}" ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form method="post" action="zf3/flux.ex" formid="statistiques" formobjectname="statistiquesform" onsubmit="return submit_form(500)"
        formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}">

        <ec:table action="flux.ex" items="statistiques" title="Répartition par civilité" form="statistiques" view="cpgroup"
            autoIncludeParameters="false" var="row" showPagination="false" showStatusBar="false" filterable="false" sortable="false"
            locale="fr_FR" width="100%" theme="eXtremeTable centerTable">

            <ec:exportPdf fileName="zf3.agregavispartypeimpotseuil.edition" text="zf3.agregavispartypeimpotseuil.parametres" view="jasper"
                viewResolver="jasper" tooltip="Export PDF" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="Agregation des avis type d'impôt supérieurs à un seuil donné" imageName="pdf" />

            <ec:row>
                <ec:colTwoOrThreeHeader group="Impôts" property="nomImpot" title="Libellé" width="28%" />
                <ec:colTwoOrThreeHeader group="Annee de naissance" property="annee" title="Annee" width="12%" />
                <ec:colTwoOrThreeHeader group="Avis d'impositions" property="nombreAvis" title="Nombre d'avis" width="12%"
                    calc="total,pagetotal" />
                <ec:colTwoOrThreeHeader group="Avis d'impositions" property="montantTotal" title="Montant total"
                    cell="monnaiecelldeuxdecimales" width="12%" />
                <ec:colTwoOrThreeHeader group="Avis d'impositions" property="montantMoyen" title="Montant moyen"
                    cell="monnaiecellzerodecimale" style="text-align: right" width="12%" />
                <ec:colTwoOrThreeHeader group="Avis d'impositions" property="dateRoleMin" title="Date d'émission du premier rôle"
                    cell="date" format="dd/MM/yyyy" width="12%" />
                <ec:colTwoOrThreeHeader group="Avis d'impositions" property="dateRoleMax" title="Date d'émission du dernier rôle"
                    cell="date" format="dd/MM/yyyy" width="12%" />
            </ec:row>
        </ec:table>


        <app:boxboutons>
            <app:submit label="Retourner à la page précédente" transition="retour" responsive="${responsive}" />
        </app:boxboutons>

    </app:form>
</app:page>