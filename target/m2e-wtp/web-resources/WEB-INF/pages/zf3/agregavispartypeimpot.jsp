<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page agregavispartypeimpot.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Agrégation par type d'impôt des avis d'imposition" menu="true" responsive="${responsive}">

    <app:chemins action='zf3/flux.ex' responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Tableaux et éditions statistiques" responsive="${responsive}" transition="retour">Tableaux et éditions statistiques</app:chemin>
        <app:chemin title="Agrégation par type d'impôt des avis d'imposition" responsive="${responsive}">Agrégation par type d'impôt des avis d'imposition</app:chemin>
    </app:chemins>

    <app:onglets cleactive="2" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Tableaux et éditions statistiques" responsive="${responsive}" transition="retour"
            ongletclass="btmodel_inv_half">Tableaux et éditions statistiques</app:onglet>
        <app:onglet cle="2" libelle="Avis d'imposition" responsive="${responsive}" ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form method="post" action="zf3/flux.ex" formid="statistiques" formobjectname="statistiquesform" onsubmit="return submit_form(500)"
        formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}">

        <ec:table items="statistiques" title="Agrégation des avis par type d'impôt" form="statistiques" action="flux.ex" view="cpgroup"
            autoIncludeParameters="false" var="row" showPagination="false" showStatusBar="false" filterable="false" sortable="false"
            locale="fr_FR" width="100%" theme="eXtremeTable centerTable">

            <ecjrxml:exportPdf conf="documentinfo.xml" fileName="zf3.agregcontripartypeimpot.edition.pdf" view="cpjrxml"
                viewResolver="cpjrxml" tooltip="Export PDF" imageName="pdf" text="Export PDF" nomapplication="DMO Test" modele="paysage"
                texteentete="Annee : 2005" textepieddepage="Copyright DGCP " graphique1="${graphiquecontripartypeimpot}" zoomratio1="3"
                graphique2="${graphiquerepartavispartypeimpot}" zoomratio2="3" />

            <ec:row>
                <ech:column group="Impôt" property="codeImpot" title="Code" width="10%" />
                <ech:column group="Impôt" property="nomImpot" title="Libellé" width="50%" />
                <ech:column group="Avis d'imposition" property="nombreAvis" title="Nombre d'avis" cell="pourcentcelldeuxdecimales"
                    width="10%" />
                <ech:column group="Avis d'imposition" property="montantTotal" title="Montant total" width="10%"
                    cell="monnaiecelldeuxdecimales" />
                <ech:column group="Avis d'imposition" property="montantMoyen" title="Montant moyen" width="10%"
                    cell="nombrecelldeuxdecimales" />
                <ech:column group="Avis d'imposition" property="dateRoleMin" title="Date d'émission du premier rôle" cell="date" width="10%" />
                <ech:column group="Avis d'imposition" property="dateRoleMax" title="Date d'émission du dernier rôle" cell="date" width="10%" />
            </ec:row>
        </ec:table>
        <table>
            <tr>
                <td><app:image fichierjoint="graphiquecontripartypeimpot" width="285" height="368" /></td>
                <td><app:image fichierjoint="graphiquerepartavispartypeimpot" width="285" height="200" border="0" /></td>
            </tr>
        </table>
        <app:boxboutons>
            <app:submit label="Retourner à la page précédente" transition="retour" responsive="${responsive}" />
        </app:boxboutons>

    </app:form>
</app:page>