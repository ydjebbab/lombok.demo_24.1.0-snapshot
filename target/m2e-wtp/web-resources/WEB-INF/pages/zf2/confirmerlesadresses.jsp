<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page confirmerlesadresses.jsp -->
<!-- PAS ENCORE RESPONSIVE -->

<c:set var="responsive" value="true" />

<app:page titreecran="Gérer les adresses">

    <app:chemins action="zf2/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Rechercher un contribuable" active="false" transition="NouvelleRecherche">Rechercher un contribuable</app:chemin>
        <app:chemin title="Modifier un contribuable" transition="retour">Modifier un contribuable</app:chemin>
        <app:chemin title="Gérer les adresses">Gérer les adresses</app:chemin>
    </app:chemins>

    <app:form action="zf2/flux.ex" formboxclass="eXtremeTable" formobjectname="creationmodificationcontribuableform">

        <ec:table items="creationmodificationcontribuableform.contribuable.listeAdresses"
            action="${pageContext.request.contextPath}/zf2/flux.ex" tableId="tableadresses" autoIncludeParameters="false" form="formulaire"
            locale="fr_FR" view="cphtml" filterable="false" sortable="false" showPagination="false" showExports="false" var="row">
            <ec:row highlightRow="true">
                <ech:column property="chkbx" cell="cpcheckboxcell" rowid="ROW" viewsAllowed="cphtml" headerCell="cpselectAll" width="5%"
                    filterable="false" sortable="false" />
                <ec:column property="ligne1" alias="vide" title="Adresse n°" width="5%" sortable="true">${ROWCOUNT} </ec:column>
                <ec:column property="ligne1" alias="ligne1" title="ligne1" width="30%" sortable="true">
                    <app:link action="zf2/flux.ex" transition="editeruneadresse" attribute="hashcode" value="${lb:hashcode(row)}"
                        label="${row.ligne1}" active="true" title="Modifier l'adresse" />

                </ec:column>
                <ec:column property="ligne2" title="ligne2" width="10%" sortable="true">
                </ec:column>
                <ec:column property="ligne3" alias="ligne3" title="ligne3" width="10%" sortable="true">
                </ec:column>
                <ec:column property="ligne4" alias="ligne4" title="ligne4" width="10%" sortable="true">
                </ec:column>
                <ec:column property="pays" alias="pays" title="pays" width="25%" sortable="true">
                </ec:column>
                <ec:column property="ville" alias="ville" title="ville" width="20%" sortable="true">
                </ec:column>
                <ec:column property="codePostal" alias="codepostal" title="Code postal" width="20%" sortable="true">
                </ec:column>
            </ec:row>
        </ec:table>
        <p>
            <br>
        </p>

        <%--         <app:boxboutons> --%>
        <%--             <app:submit label="Page précédente" transition="pageprecedente" /> --%>
        <%--             <app:submit label="Enregistrer les modification" transition="enregistrermodification" /> --%>
        <%--         </app:boxboutons> --%>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="retour" title="Retourner à l'accueil" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="retour"
                        responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
        <p>
            <br>
        </p>

    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>