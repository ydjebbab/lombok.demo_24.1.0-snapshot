<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page afficherincidentsuppression.jsp -->

<app:page titreecran="Satelit bienvenue" 
		  titrecontainer="Resultats >> Contribuables >> Suppression >> Incidents" 
		  menu="true" >
		  
<app:disperrorfix path="suppressioncontribuableform.*" style="left:10% ;top: 5%; width: 80%;height: 15%"/>

<%@ include file="/WEB-INF/pages/errors/rapporterreur.jspf" %>
</app:page>
