<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!--  Page resultatsrecherchecontribuableRD.jsp -->
<app:page titreecran="Résultats de la recherche">

    <app:chemins action="zf2/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Rechercher un contribuable" transition="NouvelleRecherche">Rechercher un contribuable</app:chemin>
        <app:chemin title="Résultats de la recherche">Résultats de la recherche</app:chemin>
    </app:chemins>


    <!-- css indispensables  -->
    <link href="<c:url value="/webjars/datatables/1.9.4/media/css/jquery.dataTables.css"/>" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/webjars/bootstrap/2.3.2/css/bootstrap.css"/>" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/webjars/datatables/1.9.4/media/css/demo_table.css"/>" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/webjars/datatables/1.9.4/media/css/demo_page.css"/>" type="text/css" rel="stylesheet" />


    <link href="<c:url value="/application/css/datatables.responsive.css"/>" type="text/css" rel="stylesheet" />
    <!-- css si twitter /bootstrap -->
    <link href="<c:url value="/webjars/datatables-bootstrap/2-20120201/DT_bootstrap.css"/>" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/webjars/bootstrap/2.3.2/css/bootstrap-responsive.css"/>" type="text/css" rel="stylesheet" />
    <!--css pour exports-->
    <link href="<c:url value="/webjars/datatables/1.9.4/extras/TableTools/media/css/TableTools.css"/>" type="text/css" rel="stylesheet" />
    <link href="<c:url value="/webjars/datatables/1.9.4/extras/TableTools/media/css/TableTools_JUI.css"/>" type="text/css" rel="stylesheet" />
    <app:form action="zf2/flux.ex" formid="formulaire" formboxclass="eXtremeTable">
        <!-- affichage de nombres  -->
        <script type="text/javascript" src="<c:url value="/webjars/numeral-js/1.4.5/min/numeral.min.js"/>"></script>
        <!-- affichage de dates -->
        <script type="text/javascript" src="<c:url value="/webjars/momentjs/2.5.0/min/moment.min.js"/>"></script>


        <!--  
 <script type="text/javascript" src="<c:url value="/application/js/jquery.min.js"/>"></script> 
 <script type="text/javascript" src="<c:url value="/application/js/jquery-ui.min.js"/>"></script> 
 -->
        <!-- pour affichage nombre d epage et cclick exemple 1 2 3 4 5  -->
        <script type="text/javascript" src="<c:url value="/webjars/jquery/1.8.2/jquery.js"/>"></script>

        <!-- pour filtre par colonnes -->
        <script type="text/javascript" src="<c:url value="/webjars/datatables/1.9.4/media/js/jquery.dataTables.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/application/js/jquery.dataTables.columnFilter.js"/>"></script>
        <script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharttable.org/master/jquery.highchartTable-min.js"></script>
        <script src="http://code.highcharts.com/highcharts.js"></script>
        <script src="http://code.highcharttable.org/master/jquery.highchartTable-min.js"></script>



        <!-- pour exports -->
        <script type="text/javascript" src="<c:url value="/webjars/datatables/1.9.4/extras/TableTools/media/js/TableTools.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/webjars/datatables/1.9.4/extras/TableTools/media/js/ZeroClipboard.js"/>"></script>
        <!--  librairies pour le responsive  -->
        <script type="text/javascript" src="<c:url value="/application/js/datatables.responsive.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/webjars/datatables-bootstrap/2-20120201/DT_bootstrap.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/webjars/underscorejs/1.5.2/underscore-min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/webjars/lodash/2.4.1/lodash.min.js"/>"></script>
        <script type="text/javascript">
									var asInitVals = new Array();
									/* Get the rows which are currently selected */
									function fnGetSelected(oTableLocal) {
										var aReturn = new Array();
										var aTrs = oTableLocal.fnGetNodes();

										for (var i = 0; i < aTrs.length; i++) {
											if ($(aTrs[i]).hasClass(
													'row_selected')) {
												aReturn.push(aTrs[i]);
											}
										}
										return aReturn;
									}
									$(document)
											.ready(
													function() {

														$("#resultats tbody")
																.click(
																		function(
																				event) {
																			var aData = oTable
																					.fnGetData(this);
																			$(
																					oTable
																							.fnSettings().aoData)
																					.each(
																							function() {
																								$(
																										this.nTr)
																										.removeClass(
																												'row_selected');
																							});
																			$(
																					event.target.parentNode)
																					.addClass(
																							'row_selected');
																			var aPos = oTable
																					.fnGetPosition(this);

																			// Get the data array for this row
																			var aData = oTable
																					.fnGetData(aPos[0]);
																			alert(aData);
																		});

														$('#delete')
																.click(
																		function() {

																			var anSelected = fnGetSelected(oTable);
																			var rowData = oTable
																					.fnGetData(anSelected[0]);
																			alert(rowData);

																			//suppression client 
																			var anSelected = fnGetSelected(oTable);
																			if (anSelected == []) {
																				alert('aucun élément sélectionné');
																			}
																			oTable
																					.fnDeleteRow(anSelected[0]);

																			//suppression serveur 
																			$
																					.ajax({
																						url : "${pageContext.request.contextPath}/async/controller/delete?uid="
																								+ rowData[0],
																						dataType : 'json',

																					});
																		});

														$('#modify')
																.click(
																		function() {

																			var anSelected = fnGetSelected(oTable);
																			var rowData = oTable
																					.fnGetData(anSelected[0]);
																			alert(rowData);

																			//modification, client 
																			var anSelected = fnGetSelected(oTable);
																			if (anSelected == []) {
																				alert(anSelected);
																			}
																			alert(rowData);
																			//oTable.fnDeleteRow( anSelected[0] );

																			//modification serveur 
																			$
																					.ajax({
																						type : "GET",
																						url : "${pageContext.request.contextPath}/async/controller/modify?uid="
																								+ rowData[0],
																						dataType : 'json',
																						success : function() {

																							// data.redirect contains the string URL to redirect to
																							window.location.href = "${pageContext.request.contextPath}/zf2/flux.ex?_flowId=modificationcontribuableRD-flow&uid="
																									+ rowData[0];
																						}

																					});
																		});

														numeral
																.language(
																		'fr',
																		{
																			delimiters : {
																				thousands : ' ',
																				decimal : ','
																			},
																			// abbreviations: {
																			//    thousand: 'k',
																			//   million: 'm',
																			//   billion: 'b',
																			//   trillion: 't'
																			//},
																			ordinal : function(
																					number) {
																				return number === 1 ? 'er'
																						: 'ème';
																			},
																			currency : {
																				symbol : '€'
																			}
																		});

														// switch between languages
														numeral.language('fr');
														moment.lang("fr");

														var responsiveHelper;
														var breakpointDefinition = {
															tablet : 1024,
															phone : 480,
															desktop : 1024
														};
														var tableContainer = $('#resultats');

														oTable = tableContainer
																.dataTable({
																	"oLanguage" : {
																		"sSearch" : "recherche",
																		"sLengthMenu" : "afficher _MENU_ enregistrements par page",
																		"sZeroRecords" : "Aucun résultat n a été trouvé",
																		"sInfo" : " _TOTAL_enregistrements trouvés.affichage de _START_ à _END_",
																		"sInfoEmpty" : "affichage 0 to 0 of 0 enregistrements",
																		"sInfoFiltered" : "(fitre a partir de _MAX_ total enregistrements)",
																		"oPaginate" : {
																			"sNext" : "page suivante ",
																			"sPrevious" : "page précédente ",
																			"sFirst" : "premiére page ",
																			"sLast" : "derniére page ",

																		},
																		"sProcessing" : "Requête en cours de traitement",
																	},
																	//"sDom": 'T<"clear">lfrtip',
																	//bootstap '<"row"<"span6"l><"span6"f>r>t<"row"<"span6"i><"span6"p>>',
																	"sDom" : 'T<"clear">lfrtip',

																	//exports pds csv...
																	"oTableTools" : {
																		"sSwfPath" : "http://datatables.net/release-datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf",
																		//ci desssous exemple si on ne veut exporter que certaines colonnes
																		"aButtons" : [
																				{
																					"sExtends" : "xls",
																					"sButtonText" : "excel",
																					"sFileName" : "*.xls",
																				//"mColumns": [ 0, 1, 4 ]
																				},
																				{
																					"sExtends" : "pdf",
																					"sButtonText" : "pdf"
																				},

																		]
																	},

																	//1ere colonne qui sera triée
																	"aaSorting" : [ [
																			4,
																			"desc" ] ],
																	'bProcessing' : true,
																	'bJQueryUI' : true,
																	//"sPaginationType": 
																	//bootstrap  "bootstrap",full_numbers
																	'sPaginationType' : "full_numbers",
																	'sAjaxSource' : "${pageContext.request.contextPath}/async/controller/search?uid=${uid}",
																	'bAutoWidth' : false,
																	//pour responsive design
																	// 'fnPreDrawCallback': function () {
																	// Initialize the responsive datatables helper once.
																	//   if (!responsiveHelper) {
																	//     responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
																	// }
																	// },
																	//  'fnRowCallback'  : function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
																	//      responsiveHelper.createExpandIcon(nRow);
																	// },
																	// 'fnDrawCallback' : function (oSettings) {
																	//    responsiveHelper.respond();
																	//  },
																	//exemple d'interceptrur si madame , on colorie ligne en rouge
																	"fnRowCallback" : function(
																			nRow,
																			aaData,
																			iDisplayIndex) {
																		if (aaData[1] == "Madame") {
																			// testrow IS THE CSS CLASS WITH THE CORRECT background-color
																			$(
																					'td',
																					nRow)
																					.css(
																							'background-color',
																							'red');
																		}
																		return nRow;
																	},
																	"fnDrawCallback" : function() {
																		//  alert( 'DataTables has redrawn the table' );
																		$(
																				"#delete")
																				.click(
																						function() {
																							// alert('blah');
																							var anSelected = fnGetSelected(oTable);
																							// alert (anSelected);
																							var anSelected1 = fnGetSelected(this);
																							// alert (anSelected1);
																							oTable
																									.fnDeleteRow(anSelected[0]);
																						});
																	},

																	"fnFooterCallback" : function(
																			nRow,
																			aaData,
																			iStart,
																			iEnd,
																			aiDisplay) {
																		/*
																		 * Calculate the total market share for all browsers in this table (ie inc. outside
																		 * the pagination)
																		 */
																		var iTotalMarket = 0;
																		for (var i = 0; i < aaData.length; i++) {
																			iTotalMarket += parseFloat(aaData[i][6]);
																		}

																		/* Calculate the market share for browsers on this page */
																		// var iPageMarket = 0;
																		//for ( var i=iStart ; i<iEnd ; i++ )
																		//{
																		//   iPageMarket += aaData[ aiDisplay[i] ][4]*1;
																		//}
																		/* Modify the footer row to match what we want */
																		var nCells = nRow
																				.getElementsByTagName('th');
																		nCells[6].innerHTML = parseFloat(iTotalMarket);
																	},

																	"aoColumnDefs" : [

																			{
																				"sWidth" : "10%",
																				"aTargets" : [ 0 ]
																			},
																			{
																				"sWidth" : "10%",
																				"aTargets" : [ 1 ]
																			},
																			{
																				"sWidth" : "10%",
																				"aTargets" : [ 2 ]
																			},
																			{
																				"sWidth" : "10%",
																				"aTargets" : [ 3 ]
																			},
																			{
																				"sWidth" : "10%",
																				"aTargets" : [ 4 ]
																			},
																			{
																				"sWidth" : "20%",
																				"aTargets" : [ 5 ]
																			},
																			{
																				"sWidth" : "30%",
																				"aTargets" : [ 6 ]
																			} ],

																	"aoColumns" : [
																			{
																				"aaData" : "identifiant",
																				"fnRender" : function(
																						oObj) {
																					alert("oObj.aData[0]");
																					alert(oObj.aData[0]);
																					return ('<a href="${pageContext.request.contextPath}/async/controller/modify1?uid='
																							+ oObj.aData[0]
																							+ '">'
																							+ oObj.aData[0] + '</a>');
																				}
																			},
																			{
																				"aaData" : "libelle"
																			},
																			{
																				"aaData" : "nom"
																			},
																			//sexmple pour centrer
																			{
																				"aaData" : "prenom",
																				"sClass" : "center"
																			},
																			{
																				"aaData" : "adresseMail"
																			},
																			{
																				"aaData" : "dateDeNaissance",
																				"fnRender" : function(
																						obj,
																						val) {
																					var dx = new Date(
																							parseInt(val
																									.substr(6)));

																					var now = moment(
																							val)
																							.format(
																									'DD/MM/YYYY');
																					return now;
																				}
																			},
																			{
																				"aaData" : "solde"
																			},
																	//,"fnRender" : function(obj, val)
																	//  {
																	// var number = numeral(val).format('0,0');

																	// return number;
																	//}  }  

																	],

																});

														oTable.columnFilter({
															aoColumns : [ {
																type : "text"
															}, {
																type : "text"
															}, {
																type : "text"
															}, {
																type : "text"
															}, {
																type : "text"
															}, {
																type : "date"
															}, {
																type : "number"
															} ]

														});

													});
								</script>


        <div class="span12">
            <p>
                <a href="javascript:void(0)" id="delete">Supprimer la ligne sélectionnée </a>
            </p>

            <p>
                <a href="javascript:void(0)" id="modify">Modifier la ligne sélectionnée </a>
            </p>

            <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped display" id="resultats" width="100%">
                <caption>Liste des articles</caption>
                <thead>
                    <tr>

                        <th width="20%" rowspan="3">Identifiant</th>

                        <th colspan="3">Coordonnées</th>
                        <th width="25%" rowspan="3">Adresse de messagerie</th>
                        <th width="25%" data-hide="phone,tablet" width="50%" rowspan="3">Date de naissance</th>
                        <th width="30%" data-hide="phone,tablet" rowspan="3">Solde</th>

                    </tr>

                    <tr>

                        <th width="50%" colspan="2">Nom-libelle</th>
                        <th width="15%" colspan="1">Prenom</th>
                    </tr>

                    <tr>
                        <th width="50%" rowspan="1">libellé</th>
                        <th width="50%" rowspan="1">Nom</th>
                        <th data-hide="phone,tablet" width="15%" rowspan="1">Prénom</th>
                    </tr>


                </thead>

                <tbody>
                    <!-- Contenu géré par dataTable -->
                </tbody>
                <tfoot>



                    <tr>
                        <th>Identifiant</th>
                        <th>Libellé</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>adresseMail</th>
                        <th>dateDeNaissance</th>
                        <th>solde</th>
                    </tr>

                    <tr>
                        <th></th>
                        <th style="text-align: right" colspan="6">Total:</th>
                    </tr>

                </tfoot>
            </table>
        </div>



    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>