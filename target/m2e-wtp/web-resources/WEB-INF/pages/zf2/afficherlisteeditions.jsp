<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page afficherlisteeditions.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Édition liste des contribuables" titrecontainer="Édition liste des contribuables" menu="true" responsive="${responsive}">

    <app:chemins action="zf2/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Gestion des contribuables" responsive="${responsive}">Gestion des contribuables</app:chemin>
        <app:chemin title="Édition liste des contribuables" responsive="${responsive}">Édition liste des contribuables</app:chemin>
    </app:chemins>

    <app:form action="/zf2/flux.ex">

        <table border="1">
            <caption>Édition liste des contribuables</caption>
            <thead>
                <tr>
                    <th>Contribuables - Editions</th>
                    <th>Caractéristiques</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>

            <tr>
                <td>zf2.listecontribuablescsv.edition</td>
                <td>Éditer la liste des contribuables au format TXT sous forme CSV (Utilisation de La classe Csvprinter au lieu de jasper. Compression
                    au format ZIP de l'édition.</td>
                <td><app:button action="zf2/flux.ex" transition="editionlistecontribuablescsv">Éditer les contribuables</app:button></td>
            </tr>
            <tr>
                <td>zf2.adressescontribuables.edition</td>
                <td>Éditer adresses (Jasper - cpcsv)</td>
                <td><app:button action="zf2/flux.ex" transition="editionadressescontribuables">Éditer adresses</app:button></td>
            </tr>
            <tr>
                <td>zf2.exportcodespostaux.jasperview</td>
                <td>Éditer la liste des codes postaux au format PDF (Jasper - csv)</td>
                <td><app:button action="zf2/flux.ex" transition="editionlistecodepostauxjaspercsv">Éditer les codes postaux</app:button></td>
            </tr>
        </table>

        <%--         <app:boxboutons> --%>
        <%--             <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="retour" --%>
        <%--                 responsive="${responsive}" /> --%>
        <%--         </app:boxboutons> --%>


        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="retour" title="Retourner à l'accueil" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="retour"
                        responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
</app:page>