<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page confirmercontribuableamodifier.jsp -->
<app:page titreecran="Confirmer la modification">

    <app:chemins action="zf2/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Rechercher un contribuable" active="false" transition="NouvelleRecherche">Rechercher un contribuable</app:chemin>
        <app:chemin title="Résultats de la recherche" transition="annuler">Résultats de la recherche</app:chemin>
        <app:chemin title="Modifier un contribuable" transition="pageprecedente">Modifier un contribuable</app:chemin>
        <app:chemin title="Confirmer la modification">Confirmer la modification</app:chemin>
    </app:chemins>

    <app:onglets cleactive="1">
        <app:onglet cle="1" libelle="Informations sur le contribuable" />
    </app:onglets>

    <app:form method="post" action="zf2/flux.ex" formid="saisiecontribuable" formobjectname="creationmodificationcontribuableform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets">

        <c:set var="readonly" value="false" />

        <fieldset>
            <legend>Identité</legend>
            <app:input attribut="contribuable.identifiant" libelle="Identifiant" maxlength="50" size="50" readonly="${readonly}" />
            <app:select attribut="contribuable.civilite" itemslist="${civiliteliste}" label="libelle" value="code" readonly="${readonly}"
                defautitem="false" />
            <app:input attribut="contribuable.nom" libelle="Nom" maxlength="36" size="36" readonly="${readonly}" />
            <app:input attribut="contribuable.prenom" libelle="Prénom" maxlength="32" size="32" readonly="${readonly}" />
            <app:input attribut="contribuable.adresseMail" libelle="Adresse mail" maxlength="50" size="50" readonly="${readonly}" />
            <app:input attribut="contribuable.dateDeNaissance" libelle="Date de naissance" maxlength="10" size="10" readonly="${readonly}"
                requis="false" inputboxwidth="10%">
                <app:calendar />
            </app:input>
            <app:input attribut="contribuable.solde" maxlength="50" size="50" readonly="${readonly}" requis="false" />
        </fieldset>
        <p>
            <br />
        </p>
        <fieldset>
            <legend>Adresse postale</legend>
            <div>
                <app:textarea attribut="contribuable.adressePrincipale.ligne1" libelle="adresse Etage - escalier - appartement" cols="32"
                    rows="2" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.ligne2" libelle="Immeuble - bâtiment - résidence" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.ligne3" libelle="adresse N° et libellé de la voie" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.ligne4" maxlength="50" size="50" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.codePostal" libelle="Code postal" maxlength="8" size="8"
                    readonly="${readonly}" />
                <app:input attribut="contribuable.adressePrincipale.ville" libelle="Localité" maxlength="32" size="32"
                    readonly="${readonly}" />
                <app:select attribut="contribuable.adressePrincipale.pays" libelle="Pays" itemsmap="${paysmap}" readonly="${readonly}" />
            </div>
        </fieldset>

        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                    <app:submit label="Retourner à la page précédente" transition="pageprecedente" title="Retourner à la page précédente" />
                    <app:submit label="Confirmer les modifications" transition="enregistrer" id="enterButton"
                        title="Confirmer les modifications" />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" responsive="${responsive}" />
                    <app:submit label="Retourner à la page précédente" transition="pageprecedente" title="Retourner à la page précédente"
                        responsive="${responsive}" />
                    <app:submit label="Confirmer les modifications" transition="enregistrer" id="enterButton" responsive="${responsive}"
                        inputclass="primary btn btn-default" title="Confirmer les modifications" />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>
    </app:form>
</app:page>