<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisiecontribuableacreer.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Création d'un contribuable" menu="true" responsive="${responsive}">

    <app:chemins action="zf2/flux.ex" responsive="${responsive}">
        <app:chemin transition="annuler" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Gestion des contribuables" responsive="${responsive}">Gestion des contribuables</app:chemin>
        <app:chemin title="Ajouter un contribuable" transition="choixtypecontribuable" responsive="${responsive}">Ajouter un contribuable</app:chemin>
        <app:chemin title="Créer un contribuable" responsive="${responsive}">Créer un contribuable</app:chemin>
    </app:chemins>

    <c:if test="${!empty errorMessage}">
        <div class="one_column_center">
            <div class="message_box">
                <p>
                    <c:out value="${errorMessage}"></c:out>
                </p>
            </div>
        </div>
    </c:if>

    <c:if test="${! empty warning}">
        <app:dispwarning path="${warning}" />
    </c:if>

    <app:onglets cleactive="2" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" transition="choixtypecontribuable" libelle="Ajouter un contribuable" responsive="${responsive}"
            ongletclass="btmodel_inv_half" />
        <app:onglet cle="2" transition="choixtypecontribuable" libelle="Informations sur le contribuable" responsive="${responsive}"
            ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form method="post" action="zf2/flux.ex" formid="creationmodificationcontribuable"
        formobjectname="creationmodificationcontribuableform" onsubmit="return submit_form(500)" formboxid="donnees"
        formboxclass="donnees sousonglets" responsive="${responsive}">

        <c:set var="readonly" value="false" />

        <fieldset>
            <legend>Identité</legend>

            <app:input attribut="contribuable.identifiant" label="Identifiant" maxlength="50" size="50" accesskey="I" readonly="${readonly}"
                responsive="${responsive}" />

            <app:select attribut="contribuable.civilite" itemslist="${civiliteliste}" label="libelle" value="code" readonly="${readonly}"
                defautitem="false" responsive="${responsive}" />
            <!-- attention rajout id obligatoire pour que le springjs derriére s'exécute -->
            <app:input id="nom" attribut="contribuable.nom" libelle="Nom" maxlength="36" size="36" readonly="${readonly}"
                consigne="Veuillez saisir votre nom."
                onblur="addValidation( 'nom', 'dijit.form.ValidationTextBox','nom incorrect','saisie obligatoire du nom','true','[A-Za-z]{1,50}')"
                responsive="${responsive}" />


            <app:input attribut="contribuable.prenom" libelle="Prénom" maxlength="32" size="32" readonly="${readonly}"
                responsive="${responsive}" />
            <app:input attribut="contribuable.adresseMail" id="adresseMail" libelle="Adresse mail" maxlength="50" size="50"
                readonly="${readonly}"
                onblur="addValidation( 'adresseMail', 'dijit.form.ValidationTextBox','email incorrect','saisie obligatoire du email','true','[a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}')"
                responsive="${responsive}" />


            <div>
                <app:input attribut="contribuable.dateDeNaissance" id="dateDeNaissance" maxlength="10" size="10" readonly="false"
                    requis="false" placeholder="JJ/MM/AAAA" inputboxwidth="10%" consigne="Veuillez saisir une date de la forme jj/mm/aaaa"
                    onchange="addValidation( 'dateDeNaissance', 'dijit.form.DateTextBox','date incorrect','saisie obligatoire de la date','true','dd/MM/yyyy')"
                    responsive="${responsive}">
                </app:input>
                <app:calendar dateouverture="01/01/1995" />
            </div>

            <app:input attribut="contribuable.solde" maxlength="50" size="50" readonly="${readonly}" requis="false" compboxclass="add-on"
                inputclass="input-append" inputboxwidth="auto" responsive="${responsive}">€</app:input>
        </fieldset>

        <fieldset>
            <legend>Adresse postale</legend>
            <div>
                <app:textarea attribut="contribuable.adressePrincipale.ligne1" libelle="adresse Etage - escalier - appartement" cols="32"
                    rows="2" readonly="${readonly}" requis="false" responsive="${responsive}" />
                <app:input attribut="contribuable.adressePrincipale.ligne2" libelle="Immeuble - bâtiment - résidence" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" responsive="${responsive}" />
                <app:input attribut="contribuable.adressePrincipale.ligne3" libelle="adresse N° et libellé de la voie" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" responsive="${responsive}" />
                <app:input attribut="contribuable.adressePrincipale.ligne4" libelle="Lieu-dit ou boîte postale" maxlength="50" size="50"
                    readonly="${readonly}" requis="false" responsive="${responsive}" />
                <app:input attribut="contribuable.adressePrincipale.codePostal" libelle="Code postal" maxlength="8" size="8"
                    readonly="${readonly}" inputboxwidth="30%" responsive="${responsive}">
                    <app:submit label="Rechercher un code postal" transition="_eventId_recherchercodepostal" responsive="${responsive}" />
                </app:input>
                <app:input attribut="contribuable.adressePrincipale.ville" libelle="Localité" maxlength="32" size="32"
                    readonly="${readonly}" responsive="${responsive}" />
                <app:select attribut="contribuable.adressePrincipale.pays" libelle="Pays" itemsmap="${paysmap}" readonly="${readonly}"
                    responsive="${responsive}" />

            </div>
        </fieldset>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                    <app:submit label="Retouner à la page précédente" title="Retouner à la page précédente"
                        transition="choixtypecontribuable" />
                    <app:submit label="Valider la création" title="Valider la création" transition="valider" id="enterButton"
                        class="primary" onclick="addAllValidation('enterButton','onclick')" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="annuler" responsive="${responsive}" />
                    <app:submit label="Retouner à la page précédente" title="Retouner à la page précédente"
                        transition="choixtypecontribuable" responsive="${responsive}" />
                    <app:submit label="Valider la création" title="Valider la création" transition="valider" id="enterButton"
                        class="primary" onclick="addAllValidation('enterButton','onclick')" responsive="${responsive}" />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
</app:page>
