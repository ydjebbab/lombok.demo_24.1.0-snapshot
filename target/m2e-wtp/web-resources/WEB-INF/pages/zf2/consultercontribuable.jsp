<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page consultercontribuable.jsp -->
<!-- PAS ENCORE RESPONSIVE -->

<c:set var="responsive" value="true" />

<app:page titreecran="Nouvelles caractéristiques">

    <app:chemins action="zf2/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Rechercher un contribuable" active="false" transition="NouvelleRecherche">Rechercher un contribuable</app:chemin>
        <app:chemin title="Résultats de la recherche" transition="retour">Résultats de la recherche</app:chemin>
        <app:chemin title="Informations sur un contribuable">Informations sur un contribuable</app:chemin>
    </app:chemins>

    <app:onglets cleactive="1">
        <app:onglet cle="1" libelle="Informations sur le contribuable" />
    </app:onglets>

    <app:form method="post" action="zf2/flux.ex" formid="infocontribuable" formobjectname="recherchecontribuableform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets">

        <c:set var="readonly" value="true" />

        <fieldset>
            <legend>Identité</legend>
            <app:input attribut="contribuable.identifiant" libelle="Identifiant" maxlength="50" size="50" readonly="${readonly}" />
            <app:select attribut="contribuable.civilite" itemslist="${civiliteliste}" label="libelle" value="code" readonly="${readonly}"
                defautitem="false" />
            <app:input attribut="contribuable.nom" libelle="Nom" maxlength="36" size="36" readonly="${readonly}" />
            <app:input attribut="contribuable.prenom" libelle="Prénom" maxlength="32" size="32" readonly="${readonly}" />
            <app:input attribut="contribuable.adresseMail" libelle="Adresse mail" maxlength="50" size="50" readonly="${readonly}" />
            <app:input attribut="contribuable.dateDeNaissance" libelle="Date de naissance" maxlength="10" size="10" readonly="${readonly}"
                requis="false" inputboxwidth="10%">
                <app:calendar />
            </app:input>
            <app:input attribut="contribuable.solde" maxlength="50" size="50" readonly="${readonly}" requis="false" />
        </fieldset>
        <br />
        <fieldset>
            <legend>Adresse postale</legend>
            <div>
                <app:textarea attribut="contribuable.listeAdresses[0].ligne1" libelle="adresse Etage - escalier - appartement" cols="32"
                    rows="2" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].ligne2" libelle="Immeuble - bâtiment - résidence" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].ligne3" libelle="adresse N° et libellé de la voie" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].ligne4" libelle="Lieu-dit ou boîte postale" maxlength="50" size="50"
                    readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].codePostal" libelle="Code postal" maxlength="8" size="8"
                    readonly="${readonly}" />
                <app:input attribut="contribuable.listeAdresses[0].ville" libelle="Localité" maxlength="32" size="32" readonly="${readonly}" />
                <app:input attribut="contribuable.listeAdresses[0].pays" libelle="Pays" maxlength="100" size="50" readonly="${readonly}" />
            </div>
        </fieldset>

        <%--         <app:boxboutons> --%>
        <%--             <app:submit label="Retour" transition="retour" id="enterButton" /> --%>
        <%--         </app:boxboutons> --%>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="retour" title="Retourner à l'accueil" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="retour"
                        responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
</app:page>
