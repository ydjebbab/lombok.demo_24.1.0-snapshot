<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page resultatsrecherchecontribuable.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Résultats de la recherche" titrecontainer="Résultats de la recherche" responsive="${responsive}">

    <app:chemins action="zf2/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Gestion des contribuables" responsive="${responsive}">Gestion des contribuables</app:chemin>
        <app:chemin title="Rechercher un contribuable" transition="NouvelleRecherche" responsive="${responsive}">Rechercher un contribuable</app:chemin>
        <app:chemin title="Résultats de la recherche" responsive="${responsive}">Résultats de la recherche</app:chemin>
    </app:chemins>


    <app:form action="zf2/flux.ex" formid="formulaire" formboxclass="eXtremeTable" responsive="${responsive}">
    
    <app:disperrortop var="${erreurentreflux}" />

        <c:if test="${responsive eq 'true'}">
            <h1>tableau en mode columntoggle</h1>
        </c:if>

        <ec:table items="listecontribuable" tableId="rechcont" view="cphtml" var="cont" action="flux.ex" state="notifyToDefault"
            stateAttr="gotofirstpage" autoIncludeParameters="false" form="formulaire" locale="fr_FR" filterable="true" sortable="true"
            sortRowsCallback="cp" filterRowsCallback="cpfiltreinfsup" rowsDisplayed="10" showPagination="true" showNewPagination="true"
            showExports="true" newPaginationNbPage="5" showStatusBar="true" showPaginationBottom="true" showExportsBottom="false"
            showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')"
            libellenoresultsfound="Aucune personne ne correspond à vos criteres de recherche" positionnoresultsfound="body">


            <ec:exportPdfJrxml fileName="listeDesTypesImpots.pdf" tooltip="Export PDF (jrxml)" imageName="new_pdf" text="Export PDF"
                nomapplication="DMO Test" modele="portrait" textepieddepage="Copyright DGCP " />

            <ec:exportXls view="cpxls" encoding="UTF" fileName="listecontribuables.xls" imageName="new_xls" tooltip="Export Excel"
                text="Export XLS" />

            <ec:exportCsv fileName="listecontribuables.csv" tooltip="Export CSV" text="Export CSV" delimiter="|" imageName="new_csv"
                viewResolver="csv" />

            <%--         <ec:parameter name="_eventId_tableaunavigation" value="tableaunavigation"/>  --%>
    
            <ec:row highlightRow="true" namePropertyShowIfEqualsLast="nom">

                <ec:column property="identifiant" title="Identifiant" width="10%" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=persist scope=col" />
                <ec:column property="nom" title="Nom" width="15%" sortable="true" filterable="true" cell="" alias="nom"
                    calc="decompte,decomptenonnulls " calcTitle="decompte,decomptenonnulls" calcDisplay="last,last" calcExport="last,last"
                    calcClass="alignRight" calcStyle="" calcTitleClass="" calcTitleStyle="" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=1 scope=col" />

                <ec:column property="prenom" title="Prénom" width="15%" sortable="true" filterable="true" cell="" alias="prenom"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />
                <ec:column property="adresseMail" title="Adresse de messagerie" width="30%" sortable="true" filterable="true" cell=""
                    alias="adresseMail" calc="decompte,decomptenonnulls " calcTitle="decompte,decomptenonnulls" calcDisplay="last,last"
                    calcExport="last,last" calcClass="alignRight" calcStyle="" calcTitleClass="" calcTitleStyle=""
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-sortable-col data-tablesaw-priority=3 scope=col" />
                <ec:column property="dateDeNaissance" title="Date de naissance" cell="date" width="10%" sortable="true" filterable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=4 scope=col" />
                <ec:column property="solde" title="solde (€)" cell="monnaiecelldeuxdecimales" width="15%" styleClass="alignRight"
                    filterable="true" writeOrBlank="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=5 scope=col" />
                <ech:column property="chkbx" cell="cpcheckboxcell" viewsAllowed="cphtml" headerCell="cpselectAll" filterable="false"
                    sortable="false" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=persist scope=col" />
                    
            </ec:row>
        </ec:table>
        
        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retouner à l'accueil" title="Retourner à l'accueil" transition="Annuler" />
                    <app:submit label="Ajouter un nouveau contribuable" title="Ajouter un nouveau contribuable" transition="Ajouter" />
                    <app:submit label="Supprimer les contribuables sélectionnés" title="Supprimer les contribuables sélectionnés"
                        transition="Supprimer" />
                    <app:submit label="Modifier le contribuable sélectionné" title="Modifier le contribuable sélectionné"
                        transition="Modifier" />
                    <app:submit label="Voir les avis d'imposition" transition="VoirAvis" disabled="true" />
                    <app:submit label="Documents associés au contribuable sélectionné"
                        title="Documents associés au contribuable sélectionné" transition="Documents" />
                    <app:submit label="Nouvelle recherche" title="Nouvelle recherche" transition="NouvelleRecherche" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retouner à l'accueil" title="Retourner à l'accueil" transition="Annuler" responsive="${responsive}" />
                    <app:submit label="Ajouter un nouveau contribuable" title="Ajouter un nouveau contribuable" transition="Ajouter"
                        responsive="${responsive}" />
                    <app:submit label="Supprimer les contribuables sélectionnés" title="Supprimer les contribuables sélectionnés"
                        transition="Supprimer" responsive="${responsive}" />
                    <app:submit label="Modifier le contribuable sélectionné" title="Modifier le contribuable sélectionné"
                        transition="Modifier" responsive="${responsive}" />
                    <app:submit label="Voir les avis d'imposition" transition="VoirAvis" disabled="true" responsive="${responsive}" />
                    <app:submit label="Documents associés au contribuable sélectionné"
                        title="Documents associés au contribuable sélectionné" transition="Documents" responsive="${responsive}" />
                    <app:submit label="Nouvelle recherche" title="Nouvelle recherche" transition="NouvelleRecherche"
                        responsive="${responsive}" />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
        </br>

    </app:form>
</app:page>