<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisiecontribuableamodifierRD.jsp -->
<app:page titreecran="Modifier un contribuable">

    <app:chemins action="zf2/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Rechercher un contribuable" active="false" transition="NouvelleRecherche">Rechercher un contribuable</app:chemin>
        <app:chemin title="Résultats de la recherche" transition="annuler">Résultats de la recherche</app:chemin>
        <app:chemin title="Modifier un contribuable">Modifier un contribuable</app:chemin>
    </app:chemins>

    <app:onglets cleactive="1">
        <app:onglet cle="1" libelle="Informations sur le contribuable" />
    </app:onglets>


    <app:form method="post" action="zf2/flux.ex" formid="creationmodificationcontribuable"
        formobjectname="creationmodificationcontribuableform" onsubmit="return submit_form(500)" formboxid="donnees"
        formboxclass="donnees sousonglets">

        <c:set var="readonly" value="false" />

        <fieldset>
            <legend>Identité</legend>
            <app:input attribut="contribuable.identifiant" maxlength="50" size="50" readonly="${readonly}" />
            <app:select attribut="contribuable.civilite" itemslist="${civiliteliste}" label="libelle" value="code" readonly="${readonly}"
                defautitem="false" />
            <app:input attribut="contribuable.nom" maxlength="36" size="36" readonly="${readonly}" />
            <app:input attribut="contribuable.prenom" maxlength="32" size="32" readonly="${readonly}" />
            <app:input attribut="contribuable.adresseMail" maxlength="50" size="50" readonly="${readonly}" />
            <app:input attribut="contribuable.dateDeNaissance" maxlength="10" size="10" readonly="false" requis="false" inputboxwidth="10%"
                consigne="Veuillez saisir une date de la forme jj/mm/aaaa">
                <app:calendar />
            </app:input>
            <app:input attribut="contribuable.solde" maxlength="50" size="50" readonly="${readonly}" requis="false" />
        </fieldset>
        <p>
            <br />
        </p>
        <fieldset>
            <legend>Adresse postale</legend>
            <div>
                <app:textarea attribut="contribuable.adressePrincipale.ligne1" cols="32" rows="2" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.ligne2" maxlength="32" size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.ligne3" maxlength="32" size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.ligne4" maxlength="50" size="50" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.codePostal" maxlength="8" size="8" readonly="${readonly}"
                    inputboxwidth="30%">
                    <app:submit label="Rechercher un code postal" transition="_eventId_recherchercodepostal" disabled="${readonly}" />
                </app:input>
                <app:input attribut="contribuable.listeAdresses[0].ville" maxlength="32" size="32" readonly="${readonly}" />
                <app:select attribut="contribuable.listeAdresses[0].pays" itemsmap="${paysmap}" readonly="${readonly}" />
            </div>
        </fieldset>

        <app:boxboutons>
            <app:submit label="Retouner à l'accueil" transition="annuler" />
            <app:submit label="Editer les adresses" transition="editerlesadresses" />
            <app:submit label="Editer les avis d'impositions" transition="gestionavisimposition" />
            <app:submit label="Valider" transition="valider" id="enterButton" />
        </app:boxboutons>

        <app:dispwarning path="${warning}" transition="continuer" transitionannulation="annuler" libelletransition="Continuer" />
    </app:form>

</app:page>