<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page recapitulatif.jsp -->
<!-- PAS ENCORE RESPONSIVE -->

<c:set var="responsive" value="true" />

<app:page titreecran="Créer d'un contribuable">

    <app:chemins action="zf2/flux.ex">
        <app:chemin transition="annuler" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Gestion des contribuables" responsive="${responsive}">Gestion des contribuables</app:chemin>
        <app:chemin title="Ajouter un contribuable" transition="choixtypecontribuable">Ajouter un contribuable</app:chemin>
        <app:chemin title="Créer un contribuable">Créer un contribuable</app:chemin>
    </app:chemins>

    <app:onglets cleactive="2">
        <app:onglet cle="1" transition="choixtypecontribuable" libelle="Ajouter un contribuable" />
        <app:onglet cle="2" transition="choixtypecontribuable" libelle="Informations sur le contribuable" />
    </app:onglets>

    <app:form method="post" action="zf2/flux.ex" formid="creationmodificationcontribuable"
        formobjectname="creationmodificationcontribuableform" onsubmit="return submit_form(500)" formboxid="donnees"
        formboxclass="donnees sousonglets">

        <c:set var="readonly" value="false" />

        <fieldset>
            <legend>Identité</legend>
            <app:input attribut="contribuable.identifiant" label="Identifiant" maxlength="50" size="50" accesskey="I" readonly="true" />
            <app:select attribut="contribuable.civilite" itemslist="${civiliteliste}" label="libelle" value="code" readonly="true"
                defautitem="false" />
            <app:input attribut="contribuable.nom" libelle="Nom" maxlength="36" size="36" readonly="true"
                consigne="Veuillez saisir votre nom." />
            <app:input attribut="contribuable.prenom" libelle="Prénom" maxlength="32" size="32" readonly="true" />
            <app:input attribut="contribuable.adresseMail" libelle="Adresse mail" maxlength="50" size="50" readonly="true" />
            <app:input attribut="contribuable.dateDeNaissance" maxlength="10" size="10" readonly="false" requis="false" inputboxwidth="10%"
                consigne="Veuillez saisir une date de la forme jj/mm/aaaa">
                <app:calendar dateouverture="01/01/1995" />
            </app:input>
            <app:input attribut="contribuable.solde" maxlength="50" size="50" readonly="true" requis="false" inputboxwidth="30%">€</app:input>
        </fieldset>
        <p></p>
        <fieldset>
            <legend>Adresse postale</legend>
            <div>
                <app:textarea attribut="contribuable.adressePrincipale.ligne1" libelle="adresse Etage - escalier - appartement" cols="32"
                    rows="2" readonly="true" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.ligne2" libelle="Immeuble - bâtiment - résidence" maxlength="32"
                    size="32" readonly="true" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.ligne3" libelle="adresse N° et libellé de la voie" maxlength="32"
                    size="32" readonly="true" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.ligne4" libelle="Lieu-dit ou boîte postale" maxlength="50" size="50"
                    readonly="true" requis="false" />
                <app:input attribut="contribuable.adressePrincipale.codePostal" libelle="Code postal" maxlength="8" size="8"
                    readonly="$true" inputboxwidth="30%" />
                <app:input attribut="contribuable.adressePrincipale.ville" libelle="Localité" maxlength="32" size="32" readonly="true" />
                <app:select attribut="contribuable.adressePrincipale.pays" libelle="Pays" itemsmap="${paysmap}" readonly="true" />

            </div>
        </fieldset>
        <%-- <app:boxboutons> --%>
        <%--     <app:submit label="Retour à l'accueil" transition="retour"/> --%>
        <%-- </app:boxboutons> --%>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="retour" title="Retourner à l'accueil" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="retour"
                        responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

        <c:if test="${! empty warning}">
            <app:dispwarning path="${warning}" />
        </c:if>


    </app:form>
</app:page>