<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="false"/>

<app:page titreecran="Test des sélections multiples">

    <app:chemins action="zf8/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test des tags" transition="pagesdetest" responsive="${responsive}">Test des tags</app:chemin>
        <app:chemin title="Test du tag app:selectmultiple" responsive="${responsive}">Test du tag app:selectmultiple</app:chemin>
        <app:chemin title="Validation" responsive="${responsive}">Validation</app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="testtagsform" formboxclass="donnees">

        <fieldset>
            <app:selectmultiple attribut="multipleListFromArrayList" label="nomImpot" value="codeImpot"
                itemslist="${testtagsform.multipleListFromArrayList}" requis="false" size="6" inputboxwidth="20%" compboxwidth="40%"
                readonly="${lectureseule}" disabled="${inactif}" disabledwhenreadonly="${inactifquandlectureseule}">
            </app:selectmultiple>
            <app:selectmultiple attribut="multipleListFromHashSet" label="nomImpot" value="codeImpot"
                itemsset="${testtagsform.multipleListFromHashSet}" defautitem="false" defautitemlabel="Aucune sélection"
                defautitemvalue="-999999" requis="false" size="4" readonly="${lectureseule}" disabled="${inactif}"
                disabledwhenreadonly="${inactifquandlectureseule}" />

            <app:selectmultiple attribut="multipleListFromLinkedHashSet" label="nomImpot" value="codeImpot"
                itemsset="${linkedhashsetelementsdelalistemultiple}" defautitem="true" defautitemlabel="Aucune sélection"
                defautitemvalue="-999999" requis="false" size="4" readonly="${lectureseule}" disabled="${inactif}"
                disabledwhenreadonly="${inactifquandlectureseule}" />

            <app:selectmultiple attribut="multipleListFromHashMap" itemsmap="${mapelementsdelalistemultiple}" defautitem="true"
                defautitemlabel="Aucune sélection" defautitemvalue="-999999" requis="false" size="4" readonly="${lectureseule}"
                disabled="${inactif}" disabledwhenreadonly="${inactifquandlectureseule}" />
            <app:selectmultiple attribut="multipleListFromLinkedHashMap" itemsmap="${linkedhashmapelementsdelalistemultiple}"
                defautitem="true" defautitemlabel="Aucune sélection" defautitemvalue="-999999" requis="false" size="4"
                readonly="${lectureseule}" disabled="${inactif}" disabledwhenreadonly="${inactifquandlectureseule}" />
        </fieldset>

        <app:boxboutons>
            <app:submit label="Page précédente" transition="pageprecedente" />
            <app:submit label="Retourner à la liste des tests" transition="pagesdetest" responsive="${responsive}" />
            <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>