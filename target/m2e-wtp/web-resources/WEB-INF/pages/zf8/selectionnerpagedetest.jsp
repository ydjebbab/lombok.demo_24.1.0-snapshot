<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="true" />

<app:initvar attrname="_imagesrepertoire" attrvalue="${imagesrepertoire}" bundlekey="app.page.imagesrepertoire"
    defaultvalue="/composants/tagsapp/images/" />

<app:page titreecran="Test des tags" menu="true" responsive="${responsive}">

    <app:chemins action="zf8/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test des tags" responsive="${responsive}">Test des tags</app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="testtagsform" formboxclass="donnees" responsive="${responsive}">

        <ul>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:input" transition="testinput" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:textarea" transition="testtextarea" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:radio" transition="testradio" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:checkbox" transition="testcheckbox" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:date" transition="testdate" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:dateheure" transition="testdateheure" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:select" transition="testselect" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:selectmultiple" transition="testselectmultiple" /></li>

            <li><app:link action="zf8/flux.ex" label="Test du tag app:details" transition="testdetails" /></li>

            <li><app:link action="zf8/flux.ex" label="Test du tag app:page" transition="testpage" /></li>

            <li><app:link action="zf8/flux.ex" label="Test du tag app:form" transition="testform" /></li>

            <li><app:link action="zf8/flux.ex" label="Test des tags app:input et app:button" transition="testinputetbouton" /></li>
        </ul>

        <p>RESPONSIVE</p>
        <ul>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:input responsive" transition="testinputresp" /></li>
            <li><app:link action="zf8/flux.ex" label="[BUG] Test du tag app:textarea responsive [BUG]" transition="testtextarearesp" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:radio responsive" transition="testradioresp" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:checkbox responsive" transition="testcheckboxresp" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:date responsive" transition="testdateresp" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:dateheure responsive" transition="testdateheureresp" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:select responsive" transition="testselectresp" /></li>
            <li><app:link action="zf8/flux.ex" label="Test du tag app:selectmultiple responsive" transition="testselectmultipleresp" /></li>

            <li><app:link action="zf8/flux.ex" label="[BUG] Test du tag app:details responsive [BUG]" transition="testdetailsresp" /></li>

            <li><app:link action="zf8/flux.ex" label="Test du tag app:page responsive" transition="testpageresp" /></li>

            <li><app:link action="zf8/flux.ex" label="Test du tag app:form responsive" transition="testformresp" /></li>

            <li><app:link action="zf8/flux.ex" label="Test des tags app:input et app:button responsive"
                    transition="testinputetboutonresp" /></li>
        </ul>

        <img data-selector="true" title="Captcha test pour png." alt="Captcha test pour png."
            src="<c:url value="${_imagesrepertoire}/imageCaptcha.png"/>" />

        <app:boxboutons>
            <app:submit label="Retourner à l'accueil" transition="annuler" responsive="${responsive}" />
        </app:boxboutons>

    </app:form>
</app:page>