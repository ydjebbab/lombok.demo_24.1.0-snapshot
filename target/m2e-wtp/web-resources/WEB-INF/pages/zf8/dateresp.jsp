<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="true" />

<app:page titreecran="Test des sélections multiples" menu="true" responsive="${responsive}">

    <app:chemins action="zf8/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test des tags" transition="pagesdetest" responsive="${responsive}">Test des tags</app:chemin>
        <app:chemin title="Test du tag app:date responsive" responsive="${responsive}">Test du tag app:date responsive</app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="testtagsform" formboxclass="donnees" responsive="${responsive}">
        <c:set var="lectureseule" value="false" />
        <c:set var="inactif" value="false" />
        <c:set var="inactifquandlectureseule" value="true" />

        <fieldset>
            <%--             <app:date attribut="dateField" maxlength="30" size="30" requis="false" inputboxwidth="20%" compboxwidth="40%" --%>
            <%--                 readonly="${lectureseule}" disabled="${inactif}" disabledwhenreadonly="${inactifquandlectureseule}" --%>
            <%--                 responsive="${responsive}" /> --%>
            <div>
                <app:input size="10" maxlength="10" attribut="dateField" libelle="Date :" requis="false" inputboxwidth="10%"
                    readonly="${lectureseule}" disabled="${inactif}" disabledwhenreadonly="${inactifquandlectureseule}"
                    responsive="${responsive}" placeholder="JJ/MM/AAAA">
                </app:input>
                <app:calendar />
            </div>
        </fieldset>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Valider (readonly)" transition="validerreadonly" />
                    <app:submit label="Valider (disabled)" transition="validerdisabled" />
                    <app:submit label="Valider (disabledwhenreadonly)" transition="validerdisabledreadonly" />
                    <app:submit label="Valider (not disabledwhenreadonly)" transition="validernotdisabledreadonly" />
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" />
                    <app:submit label="Retourner à l'accueil" transition="retour" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Valider (readonly)" transition="validerreadonly" responsive="${responsive}"
                         />
                    <app:submit label="Valider (disabled)" transition="validerdisabled" responsive="${responsive}"
                         />
                    <app:submit label="Valider (disabledwhenreadonly)" transition="validerdisabledreadonly" responsive="${responsive}"
                         />
                    <app:submit label="Valider (not disabledwhenreadonly)" transition="validernotdisabledreadonly"
                        responsive="${responsive}"  />
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" responsive="${responsive}"
                         />
                    <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>