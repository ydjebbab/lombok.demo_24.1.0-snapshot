<?xml version="1.0" encoding="UTF-8" ?>

<taglib xmlns="http://java.sun.com/xml/ns/j2ee"
	      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	      xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee/web-jsptaglibrary_2_0.xsd"
	      version="2.0">
	      
    <tlib-version>2.0</tlib-version>
    <jsp-version>2.0</jsp-version>
    <short-name>tag lombok - DGFIP</short-name>
    <uri>http://www.cp.finances.gouv.fr/lombok</uri>

<!-- Tag tree :  -->
    <tag>
        <name>tree</name>
        <tag-class>fr.gouv.finances.lombok.apptags.tree.tag.TreeTag</tag-class>
        <body-content>JSP</body-content>
		<description>Insère un arbre</description>
		
        <attribute>
            <name>tree</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
            <description>Nom du bean stocké dans le contexte qui contient l'objet de type Tree à afficher sous forme d'arbre</description>
        </attribute>

        <attribute>
            <name>action</name>
            <required>true</required>
            <rtexprvalue>true</rtexprvalue>
            <description>URI qui pointe vers le contrôleur de flux utilisé</description>
        </attribute>

        <attribute>
            <name>scope</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
            <description>Contexte dans lequel le bean 'tree' est stocké. (request, session, page, application).  Par défaut : le bean est recherché dans tous les scopes</description>
        </attribute>

        <attribute>
            <name>includeRootNode</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
            <description>'true' pour indiquer que le noeud racine doit être affiché, sinon 'false'</description>
        </attribute>

        <attribute>
            <name>imagesDir</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
            <description>Répertoire dans lequel les images utilisées par le tag sont stockées. Par défaut : /composants/tree/images</description>
        </attribute>
        
         <attribute>
            <name>cssClass</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
            <description>Classe css utilisée par le tag. Par défaut : 'tree'</description>
        </attribute>

         <attribute>
            <name>nodeIdParameter</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
            <description>Paramètre utilisé pour transférer au serveur le noeud qui fait l'objet d'une action d'ouverture, de fermeture, de sélection, ...Par défaut : nodeID</description>
        </attribute>        

         <attribute>
            <name>cssClass</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
            <description>Classe css utilisée par le tag. Par défaut : 'tree'</description>
        </attribute>

         <attribute>
            <name>eventIdFieldName</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
            <description>Paramètre utilisé pour transférer au serveur le nom de la transition du webflow. Par défaut : _eventId</description>
        </attribute>        

         <attribute>
            <name>flowExecutionKeyFieldName</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
            <description>Paramètre utilisé pour transférer au serveur la clé flowExecutionkey utilisée par le webflow. Par défaut : _flowExecutionKey</description>
        </attribute> 

         <attribute>
            <name>flowExecutionKeyBeanName</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
            <description>Nom du bean utilisé pour stocker dans le contexte la clé flowExecutionKey. Par défaut : flowExecutionKey</description>
        </attribute>

    </tag>

<!-- Tag nodes :  -->
<tag>
	<name>nodes</name>
	<tag-class>fr.gouv.finances.lombok.apptags.tree.tag.NodesTag</tag-class>
	<body-content>JSP</body-content>
	<description>Insertion des noeuds de l'arbre</description>
</tag>

<!-- Tag nodetext :  -->
<tag>
	<name>nodetext</name>
	<tag-class>fr.gouv.finances.lombok.apptags.tree.tag.NodeTextTag</tag-class>
	<body-content>JSP</body-content>
	<description>Insère la cellule destinée à contenir le texte du noeud dans un lien de sélection ou de dé-selection du noeud ou de navigation</description>
	
	<attribute>
		<name>selectable</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Indique si un clic sur le noeud provoque une action de sélection / dé-selection. Par défaut : false</description>
	</attribute>
	
	<attribute>
		<name>navigable</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Indique si un clic sur le noeud provoque une action de développement / réduction de l'arbre. Par défaut : false</description>
	</attribute>

	<attribute>
		<name>inline</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Indique si le contenu du noeud est de type 'bloc' ou 'inline' au sens html. Par défaut : false</description>
	</attribute>
	
	<attribute>
		<name>selectNodeTransition</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Nom de la transition qui est associée à l'action de sélection d'un noeud. Par défaut : selectNode</description>
	</attribute>
	
	<attribute>
		<name>unSelectNodeTransition</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Nom de la transition qui est associée à l'action de dé-sélection d'un noeud. Par défaut : unSelectNode</description>
	</attribute>
	
		<attribute>
		<name>classNodeSelected</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Class css appliquée à un noeud sélectionné</description>
	</attribute>
	
	<attribute>
		<name>classNodeUnSelected</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Class css appliquée à un noeud non  sélectionné</description>
	</attribute>
	
		<attribute>
		<name>selectionable</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Indique si le texte du noeud porte un lien qui permet une sélection ou une désélection du noeud.</description>
	</attribute>
</tag>

<!-- Tag nodenavigation :  -->
<tag>
	<name>nodenavigation</name>
	<tag-class>fr.gouv.finances.lombok.apptags.tree.tag.NodeNavigationTag</tag-class>
	<body-content>JSP</body-content>
	<description>Insère les éléments graphique de navigation dans l'arbre (icones permettant d'ouvrir et de fermer les noeuds,
	 indentation entre les différents niveaux de l'arbre, ...</description>
	
	<attribute>
		<name>expandNodeTransition</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Nom de la transition qui est associée à l'action d'ouverture d'un noeud. Par défaut : expandNode</description>
	</attribute>

	<attribute>
		<name>collapseNodeTransition</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Nom de la transition qui est associée à l'action de fermeture d'un noeud. Par défaut : collapseNode</description>
	</attribute>

	<attribute>
		<name>blankImage</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Nom de l'image "blanche" qui permet d'identer les noeuds par niveau. Par défaut : blankSpace.gif</description>
	</attribute>

	<attribute>
		<name>verticalLineImage</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Nom de l'image qui représente une ligne vertivale entre  les noeuds. Par défaut : verticalLine.gif</description>
	</attribute>

	<attribute>
		<name>collapsedMidNode</name>
		<required>false</required>
		<rtexprvalue>true</rtexprvalue>
		<description>Nom de l’image qui représente un noeud réduit. Par défaut : collapsedMidNode.gif</description>		 
	</attribute>

	<attribute>
		<name>collapsedLastNode</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Nom de l’image qui représente le dernier noeud réduit d’une liste de noeuds. Par défaut : collapsedLastNode.gif</description>
	</attribute>

	<attribute>
		<name>expandedMidNode</name>
		<required>false</required>
		<rtexprvalue>true</rtexprvalue>
		<description>Nom de l’image qui représente un noeud développé.Par défaut : expandedMidNode.gif</description>
	</attribute>

	<attribute>
		<name>expandedLastNode</name>
		<required>false</required>
		<rtexprvalue>true</rtexprvalue>
		<description>Nom de l’image qui représente le dernier noeud développé d’une liste de noeuds. Par défaut : expandedLastNode.gif</description> 
	</attribute>

	<attribute>
		<name>noChildrenMidNode</name>
		<required>false</required>
		<rtexprvalue>true</rtexprvalue>
		<description>Nom de l’image qui représente un noeud sans enfants. Par défaut : noChildrenMidNode.gif </description> 
	</attribute>

	<attribute>
		<name>noChildrenLastNode</name>
		<required>false</required>
		<rtexprvalue>true</rtexprvalue>
		<description>Nom de l’image qui représente le dernier noeud sans enfants d’une liste. Par défaut : noChildrenLastNode.gif</description>
	</attribute>	
</tag>

<!-- Tag nodeId  -->
<tag>
        <name>nodeId</name>
        <tag-class>fr.gouv.finances.lombok.apptags.tree.tag.NodeIdTag</tag-class>
        <body-content>empty</body-content>
		<description>Retourne l'id du noeud courant</description>
        <attribute>
            <name>node</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>

<!-- Tag nodeName -->
    <tag>
        <name>nodeName</name>
        <tag-class>fr.gouv.finances.lombok.apptags.tree.tag.NodeNameTag</tag-class>
        <body-content>empty</body-content>
		<description>Retourne la propriété 'name' du noeud courant</description>
    </tag>

<!-- Tag nodeType -->
<tag>
        <name>nodeType</name>
        <tag-class>fr.gouv.finances.lombok.apptags.tree.tag.NodeTypeTag</tag-class>
        <body-content>empty</body-content>
		<description>Retourne la propriété 'type' du noeud courant.</description>
    </tag>

<!-- Tag nodeToolTip -->
    <tag>
        <name>nodeToolTip</name>
        <tag-class>fr.gouv.finances.lombok.apptags.tree.tag.NodeToolTipTag</tag-class>
        <body-content>empty</body-content>
		<description>Retourne la propriété toolTip du noeud courant</description>
    </tag>

<!-- Tag nodeMatch -->
    <tag>
        <name>nodeMatch</name>
        <tag-class>fr.gouv.finances.lombok.apptags.tree.tag.NodeMatchTag</tag-class>
        <body-content>JSP</body-content>
		<description>Teste si le noeud courant respecte les propriétés spécifiées par les paramètres de ce tag. Si c'est le cas, le corps du tag est évalué</description>
        <attribute>
            <name>type</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
            <description>Compare avec la propriété 'type' du TreeNode</description>
        </attribute>
        <attribute>
            <name>name</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
			<description>Compare avec la propriété 'name' du TreeNode</description>            
        </attribute>
        <attribute>
            <name>id</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
			<description>Compare avec la propriété 'id' du TreeNode</description>            
        </attribute>
        <attribute>
            <name>expanded</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
			<description>Noeud développé</description>            
        </attribute>
        <attribute>
            <name>selected</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
            <description>Noeud sélectionné</description>
        </attribute>
        <attribute>
            <name>hasChildren</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
			<description>Le noeud possède des enfants</description>            
        </attribute>
        <attribute>
            <name>isFirstChild</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
			<description>Le noeud est le premier fils</description>            
        </attribute>
        <attribute>
            <name>isLastChild</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
			<description>Le noeud est le dernier fils</description>            
        </attribute>
    </tag>
    
    <!-- Tag nodeNoMatch -->
    <tag>
        <name>nodeNoMatch</name>
        <tag-class>fr.gouv.finances.lombok.apptags.tree.tag.NodeNoMatchTag</tag-class>
        <body-content>JSP</body-content>
		<description>Teste si le noeud courant ne respecte pas les propriétés spécidiées par les paramètres de ce tag. Si c'est le cas, le corps du tag est évalué</description>
        <attribute>
            <name>node</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>type</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>name</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>id</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>expanded</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>selected</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>hasChildren</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>isFirstChild</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
        <attribute>
            <name>isLastChild</name>
            <required>false</required>
            <rtexprvalue>true</rtexprvalue>
        </attribute>
    </tag>


<!-- Tag nodelink -->
<tag>
	<name>nodelink</name>
	<tag-class>fr.gouv.finances.lombok.apptags.tree.tag.NodeLinkTag</tag-class>
	<body-content>JSP</body-content>
	<description>Insère un lien</description>

	<attribute>
		<name>action</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Nom de l'action - URL qui permet d'accéder au contrôleur.</description>
	</attribute>
	
	<attribute>
		<name>transition</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Nom de la transition définie dans le webflow</description>
	</attribute>
	
	<attribute>
		<name></name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description></description>
	</attribute>
	
   <attribute>
		<name></name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description></description>
	</attribute>
	
	<attribute>
		<name>uri</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Uri vers laquelle pointe le lien. Utilisé si les paramètres action et transition ne sont pas utilisés.</description>
	</attribute>
	
	<attribute>
		<name>active</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Attribut qui indique si le lien est actif.</description>
	</attribute>
	
	<attribute>
		<name>attribute</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Nom de l'attribut passé en paramètre</description>
	</attribute>
	
	<attribute>
		<name>value</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Valeur de l'attribut passé en paramètre</description>
	</attribute>
	
	<attribute>
		<name>aclass</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Classe css de l'élément a</description>
	</attribute>
	
	<attribute>
		<name>image</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Nom de l'image qui sert pour matérialiser le lien</description>
	</attribute>
	
	<attribute>
		<name>imagesrepertoire</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Répertoire de stockage des images</description>
	</attribute>
	
	<attribute>
		<name>title</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Titre de l'élément href et de l'image</description>
	</attribute>	
	
	<attribute>
		<name>alt</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Texte affiché si le navigateur n'affiche pas les images</description>
	</attribute>	
		
	<attribute>
		<name>label</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>Libellé du lien</description>
	</attribute>	
	
	<attribute>
		<name>openwindow</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>true : ouvre une nouvelle fenêtre du navigateur. Par défaut : false</description>
	</attribute>	
	
	<attribute>
		<name>waitingmsg</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>true : affiche un message d'attente. Par défaut : false</description>
	</attribute>	
		
	<attribute>
		<name>displayed</name>
		<required>false</required>
		 <rtexprvalue>true</rtexprvalue>
		 <description>true: le tag est évalué. Par défaut : true</description>
	</attribute>	
														
</tag>

<function>
    <name>hashcode</name>
       <function-class>fr.gouv.finances.lombok.apptags.Functions</function-class>
       <function-signature>
          java.lang.String hashCode(java.lang.Object)
       </function-signature>
</function>

<!-- Utilisé par le tag file inputmultiplearray -->
<function>
    <name>buildInputMultipleArray</name>
       <function-class>fr.gouv.finances.lombok.apptags.Functions</function-class>
       <function-signature>
			java.util.List buildInputMultipleArray(
				java.lang.String ,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,
				java.lang.String ,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,
				java.lang.String ,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,
				java.lang.String ,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,
				java.lang.String ,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,
				java.lang.String ,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,
				java.lang.String ,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,
				java.lang.String ,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,
				java.lang.String ,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,
				java.lang.String ,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String,java.lang.String)				
       </function-signature>
</function>

</taglib>