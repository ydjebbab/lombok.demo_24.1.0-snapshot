---
--- CREATION DES TABLESPACES DU SCHEMA DU COMPOSANT EDITION
--- VERSION : V13R05
CREATE TABLESPACE Jdmo_DATA OWNER jdmo location '/u02/pgsql/tbs/Jdmo_data01';
CREATE TABLESPACE Jdmo_INDX OWNER jdmo location '/u02/pgsql/tbs/Jdmo_indx01';
alter user jdmo set default_tablespace=Jdmo_DATA;
