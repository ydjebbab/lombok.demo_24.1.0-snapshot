---
--- CREATION DES TABLESPACES DU SCHEMA DU COMPOSANT EDITION
--- VERSION : V13R05
--- 
--- Modifications 
--- wpetit        03/05/2007    creation      
---
CREATE TABLESPACE Jdmo_DATA location '/u02/pgdata/tbs/Jdmo_data01';
CREATE TABLESPACE Jdmo_INDX location '/u02/pgdata/tbs/Jdmo_indx01';
CREATE TABLESPACE Jdmo_BLOB location '/u02/pgdata/tbs/Jdmo_blob01';