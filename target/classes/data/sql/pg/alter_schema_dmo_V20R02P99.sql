    ALTER TABLE ZOPERATIONJOURNAL_ZOPE add STRUCTUREUTILISATEUR varchar(255);

    CREATE INDEX IDX_ZCOF_ID_LECONTENUDUFICHIER 
        ON ZFICHIERJOINT_ZFIJ(ZCOF_ID_LECONTENUDUFICHIER) 
        ;
    
    CREATE INDEX IDX_ZFIJ_ID_LEFICHIER 
        ON ZDOCUMENTATLAS_ZDOA(ZFIJ_ID_LEFICHIER) 
        ;

    create table ZTYPEDOCUMENTATLAS_ZTDA (
        ZTDA_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        code varchar(80) not null,
        libelle varchar(80) not null,
        CONSTRAINT PK_ZTDA PRIMARY KEY(ZTDA_ID)
            
            ,
        CONSTRAINT UK_ZTDA_1 UNIQUE(code)
            
            
    )
       ; 
    alter table ZDOCUMENTATLAS_ZDOA 
        add ZTDA_ID_LETYPEDOCUMENT numeric(19,0);
        
    alter table ZDOCUMENTATLAS_ZDOA 
        add constraint FK_ZDOA_ZTDA_1
        foreign key (ZTDA_ID_LETYPEDOCUMENT) 
        references ZTYPEDOCUMENTATLAS_ZTDA;

    create sequence ZTDA_ID_SEQUENCE;
