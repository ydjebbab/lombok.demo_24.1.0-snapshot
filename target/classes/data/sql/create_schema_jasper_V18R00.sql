---
--- CREATION DU SCHEMA DU COMPOSANT EDITION
--- 

CREATE TABLE QRTZ_JOB_DETAILS
  (
    JOB_NAME  VARCHAR2(80) NOT NULL,
    JOB_GROUP VARCHAR2(80) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    JOB_CLASS_NAME   VARCHAR2(128) NOT NULL,
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    IS_STATEFUL VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    CONSTRAINT PK_QRTZ_JOB_DETAILS PRIMARY KEY(JOB_NAME,JOB_GROUP)
    USING INDEX
    TABLESPACE JDMO_INDX
   )
       LOB (JOB_DATA) STORE AS
        (TABLESPACE JDMO_BLOB
           STORAGE (INITIAL 6144 NEXT 6144)
           CHUNK 8K
           NOCACHE LOGGING
           INDEX (TABLESPACE JDMO_BLOB)
        )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE QRTZ_JOB_LISTENERS
  (
    JOB_NAME  VARCHAR2(80) NOT NULL,
    JOB_GROUP VARCHAR2(80) NOT NULL,
    JOB_LISTENER VARCHAR2(80) NOT NULL,
    CONSTRAINT PK_QRTZ_JOB_LISTENERS PRIMARY KEY(JOB_NAME,JOB_GROUP,JOB_LISTENER)
    USING INDEX
    TABLESPACE JDMO_INDX,
    FOREIGN KEY (JOB_NAME,JOB_GROUP)
    REFERENCES QRTZ_JOB_DETAILS(JOB_NAME,JOB_GROUP)
   )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE QRTZ_TRIGGERS
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    JOB_NAME  VARCHAR2(80) NOT NULL,
    JOB_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    DESCRIPTION VARCHAR2(120) NULL,
    NEXT_FIRE_TIME NUMBER(13) NULL,
    PREV_FIRE_TIME NUMBER(13) NULL,
    PRIORITY NUMBER(13) NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(13) NOT NULL,
    END_TIME NUMBER(13) NULL,
    CALENDAR_NAME VARCHAR2(80) NULL,
    MISFIRE_INSTR NUMBER(2) NULL,
    JOB_DATA BLOB NULL,
    CONSTRAINT PK_QRTZ_TRIGGERS PRIMARY KEY(TRIGGER_NAME,TRIGGER_GROUP)
    USING INDEX
    TABLESPACE JDMO_INDX,
    FOREIGN KEY (JOB_NAME,JOB_GROUP)
    REFERENCES QRTZ_JOB_DETAILS(JOB_NAME,JOB_GROUP)
    )
    LOB (JOB_DATA) STORE AS
        (TABLESPACE JDMO_BLOB
           STORAGE (INITIAL 6144 NEXT 6144)
           CHUNK 8K
           NOCACHE LOGGING
           INDEX (TABLESPACE JDMO_BLOB)
        )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE QRTZ_SIMPLE_TRIGGERS
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    REPEAT_COUNT NUMBER(7) NOT NULL,
    REPEAT_INTERVAL NUMBER(12) NOT NULL,
    TIMES_TRIGGERED NUMBER(7) NOT NULL,
    CONSTRAINT PK_QRTZ_SIMPLE_TRIGGERS PRIMARY KEY(TRIGGER_NAME,TRIGGER_GROUP)
    USING INDEX
    TABLESPACE JDMO_INDX,
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
    REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
    )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE QRTZ_CRON_TRIGGERS
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    CRON_EXPRESSION VARCHAR2(80) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    CONSTRAINT PK_QRTZ_CRON_TRIGGERS PRIMARY KEY(TRIGGER_NAME,TRIGGER_GROUP)
    USING INDEX
    TABLESPACE JDMO_INDX,
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
    REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
    )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE QRTZ_BLOB_TRIGGERS
  (
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    BLOB_DATA BLOB NULL,
    CONSTRAINT PK_QRTZ_BLOB_TRIGGERS PRIMARY KEY(TRIGGER_NAME,TRIGGER_GROUP)
    USING INDEX
    TABLESPACE JDMO_INDX,
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
    REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
    )
    LOB (BLOB_DATA) STORE AS
        (TABLESPACE JDMO_BLOB
           STORAGE (INITIAL 6144 NEXT 6144)
           CHUNK 8K
           NOCACHE LOGGING
           INDEX (TABLESPACE JDMO_BLOB)
        )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE QRTZ_TRIGGER_LISTENERS
  (
    TRIGGER_NAME  VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    TRIGGER_LISTENER VARCHAR2(80) NOT NULL,
    CONSTRAINT PK_QRTZ_TRIGGER_LISTENERS PRIMARY KEY(TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_LISTENER)
    USING INDEX
    TABLESPACE JDMO_INDX,
    FOREIGN KEY (TRIGGER_NAME,TRIGGER_GROUP)
    REFERENCES QRTZ_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
    )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE QRTZ_CALENDARS
  (
    CALENDAR_NAME  VARCHAR2(80) NOT NULL,
    CALENDAR BLOB NOT NULL,
    CONSTRAINT PK_QRTZ_CALENDARS PRIMARY KEY(CALENDAR_NAME)
    USING INDEX
    TABLESPACE JDMO_INDX
    )
    LOB (CALENDAR) STORE AS
        (TABLESPACE JDMO_BLOB
           STORAGE (INITIAL 6144 NEXT 6144)
           CHUNK 8K
           NOCACHE LOGGING
           INDEX (TABLESPACE JDMO_BLOB)
        )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE QRTZ_PAUSED_TRIGGER_GRPS
  (
    TRIGGER_GROUP  VARCHAR2(80) NOT NULL,
    CONSTRAINT PK_QRTZ_PAUSED_TRIGGER_GRPS PRIMARY KEY(TRIGGER_GROUP)
    USING INDEX
    TABLESPACE JDMO_INDX
    )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE QRTZ_FIRED_TRIGGERS
  (
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(80) NOT NULL,
    TRIGGER_GROUP VARCHAR2(80) NOT NULL,
    IS_VOLATILE VARCHAR2(1) NOT NULL,
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    FIRED_TIME NUMBER(13) NOT NULL,
    PRIORITY NUMBER(13) NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(80) NULL,
    JOB_GROUP VARCHAR2(80) NULL,
    IS_STATEFUL VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    CONSTRAINT PK_QRTZ_FIRED_TRIGGERS PRIMARY KEY(ENTRY_ID)
    USING INDEX
    TABLESPACE JDMO_INDX
    )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE QRTZ_SCHEDULER_STATE
  (
    INSTANCE_NAME VARCHAR2(80) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
    CHECKIN_INTERVAL NUMBER(13) NOT NULL,
    CONSTRAINT PK_QRTZ_SCHEDULER_STATE PRIMARY KEY(INSTANCE_NAME)
    USING INDEX
    TABLESPACE JDMO_INDX
    )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;


CREATE TABLE QRTZ_LOCKS
  (
    LOCK_NAME  VARCHAR2(40) NOT NULL,
    CONSTRAINT PK_QRTZ_LOCKS PRIMARY KEY(LOCK_NAME)
    USING INDEX
    TABLESPACE JDMO_INDX
);

INSERT INTO qrtz_locks values('TRIGGER_ACCESS');
INSERT INTO qrtz_locks values('JOB_ACCESS');
INSERT INTO qrtz_locks values('CALENDAR_ACCESS');
INSERT INTO qrtz_locks values('STATE_ACCESS');
INSERT INTO qrtz_locks values('MISFIRE_ACCESS');
COMMIT;

CREATE INDEX IDX_QRTZ_J_REQ_RECOVERY ON QRTZ_JOB_DETAILS(REQUESTS_RECOVERY)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_T_NEXT_FIRE_TIME ON QRTZ_TRIGGERS(NEXT_FIRE_TIME)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_T_STATE ON QRTZ_TRIGGERS(TRIGGER_STATE)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_T_NFT_ST ON QRTZ_TRIGGERS(NEXT_FIRE_TIME,TRIGGER_STATE)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_T_VOLATILE ON QRTZ_TRIGGERS(IS_VOLATILE)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_FT_TRIG_NAME ON QRTZ_FIRED_TRIGGERS(TRIGGER_NAME)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_FT_TRIG_GROUP ON QRTZ_FIRED_TRIGGERS(TRIGGER_GROUP)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_FT_TRIG_NM_GP ON QRTZ_FIRED_TRIGGERS(TRIGGER_NAME,TRIGGER_GROUP)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_FT_TRIG_VOLATILE ON QRTZ_FIRED_TRIGGERS(IS_VOLATILE)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_FT_TRIG_INST_NAME ON QRTZ_FIRED_TRIGGERS(INSTANCE_NAME)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_FT_JOB_NAME ON QRTZ_FIRED_TRIGGERS(JOB_NAME)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_FT_JOB_GROUP ON QRTZ_FIRED_TRIGGERS(JOB_GROUP)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_FT_JOB_STATEFUL ON QRTZ_FIRED_TRIGGERS(IS_STATEFUL)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IDX_QRTZ_FT_JOB_REQ_RECOVERY ON QRTZ_FIRED_TRIGGERS(REQUESTS_RECOVERY)
    TABLESPACE JDMO_INDX
;

CREATE TABLE ZCONTENUEDITION_ZCOE (
   ZCOE_ID NUMBER(19,0) NOT NULL,
   VERSION NUMBER(10,0) NOT NULL,
   DATA BLOB,
   CONSTRAINT PK_ZCOE PRIMARY KEY(ZCOE_ID)
   USING INDEX
   TABLESPACE JDMO_INDX
    )
    LOB (DATA) STORE AS
        (TABLESPACE JDMO_BLOB
           STORAGE (INITIAL 6144 NEXT 6144)
           CHUNK 8K
           NOCACHE LOGGING
           INDEX (TABLESPACE JDMO_BLOB)
        )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE ZFILTREEDITION_ZFIE (
        ZFIE_ID NUMBER(19,0) NOT NULL,
        VERSION NUMBER(10,0) NOT NULL,
        NOMDUFILTRE VARCHAR2(255 CHAR),
        ALLOW NUMBER(1,0),
        DENY NUMBER(1,0),
        ZPRD_ID_LISTEFILTRES NUMBER(19,0),
        CONSTRAINT PK_ZFIE PRIMARY KEY(ZFIE_ID)
        USING INDEX
        TABLESPACE JDMO_INDX
   )
   TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE ZFILTREVALUE_ZFIV (
       ZFIV_ID NUMBER(19,0) NOT NULL,
       VERSION NUMBER(10,0) NOT NULL,
       VALUE VARCHAR2(255 CHAR),
       ZFIE_ID_VALSFILTRE NUMBER(19,0),
       CONSTRAINT PK_ZFIV PRIMARY KEY(ZFIV_ID)
       USING INDEX
       TABLESPACE JDMO_INDX
        )
       TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

    CREATE TABLE ZJOBHISTORY_ZJOH (
        ZJOH_ID NUMBER(19,0) NOT NULL,
        VERSION NUMBER(10,0) NOT NULL,
        EDITIONUUID VARCHAR2(255 CHAR),
        BEANEDITIONID VARCHAR2(255 CHAR),
        DATEPURGE TIMESTAMP,
        NBJSTOCK NUMBER(10,0),
        DESCRIPTION VARCHAR2(255 CHAR),
        URLEDITION VARCHAR2(2000 CHAR),
        TYPEMIME VARCHAR2(255 CHAR),
        CHARACTERENCODING VARCHAR2(255 CHAR),
        EXTENSION VARCHAR2(255 CHAR),
        NOMFICDOWNLOAD VARCHAR2(255 CHAR),
        STOCKAGEENBASE NUMBER(1,0),
        APPLIEDITIONROOT VARCHAR2(255 CHAR),
        INDEXREPERTOIRE NUMBER(10,0),
        CHEMINCOMPLET VARCHAR2(255 CHAR),
        NOMFICSTOCKAGE VARCHAR2(255 CHAR),
        ZCOE_ID_LECONTENUEDITION NUMBER(19,0),
        DATEDEMANDE TIMESTAMP,
        STARTTIME TIMESTAMP,
        ENDTIME TIMESTAMP,
        STATUS VARCHAR2(255 CHAR),
        MODESOUMISSION VARCHAR2(255 CHAR),
        TRIGGERNAME VARCHAR2(255 CHAR),
        TRIGGERGROUP VARCHAR2(255 CHAR),
        RESULTAT VARCHAR2(255 CHAR),
        MESSAGE VARCHAR2(4000 CHAR),
        DELAILANCEMENT NUMBER(10,0),
        NOTIFICATIONMAIL NUMBER(1,0),
        MAILA VARCHAR2(255 CHAR),
        MAILDE VARCHAR2(255 CHAR),
        MAILOBJET VARCHAR2(255 CHAR),
        MAILMESSAGE VARCHAR2(4000 CHAR),
        ENVOIPIECEJOINTE NUMBER(1,0),
        APPLICATIONORIGINE VARCHAR2(255 CHAR),
        UIDPROPRIETAIRE VARCHAR2(255 CHAR),
        MONODESTINATAIRE NUMBER(1,0),
        NOUVELLEEDITION NUMBER(1,0),
        TAILLEFICHIER NUMBER(19,0),
        URLPINTRANET VARCHAR2(2000 CHAR),
        URLPINTERNET VARCHAR2(2000 CHAR),        
        CONSTRAINT PK_ZJOH PRIMARY KEY(ZJOH_ID)
        USING INDEX
        TABLESPACE JDMO_INDX
         )
        TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;


CREATE UNIQUE INDEX UK_ZJOH_1
        ON ZJOBHISTORY_ZJOH (EDITIONUUID)
        TABLESPACE JDMO_INDX
;

CREATE TABLE ZPROFILDESTI_ZPRD (
        ZPRD_ID NUMBER(19,0) NOT NULL,
        VERSION NUMBER(10,0) NOT NULL,
        NOMPROFIL VARCHAR2(255 CHAR),
        ZJOH_ID_PROFILSDESTI NUMBER(19,0),
        CONSTRAINT PK_ZPRD PRIMARY KEY(ZPRD_ID)
        USING INDEX
        TABLESPACE JDMO_INDX
        )
        TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

    CREATE TABLE ZTRCPURGEEDITION_ZTPE (
        ZTPE_ID NUMBER(19,0) NOT NULL,
        VERSION NUMBER(10,0) NOT NULL,
        DATEDEBUTPURGE TIMESTAMP,
        DATEFINPURGE TIMESTAMP,
        ETATPURGE VARCHAR2(255 CHAR),
        MESSAGEERREUR VARCHAR2(4000 CHAR),
        NBEDITIONSSUPPRIMEES NUMBER(19,0),
        DUREEEXECUTION NUMBER(19,0),
        APPLICATIONORIGINE VARCHAR2(255 CHAR),
        CONSTRAINT PK_ZTPE PRIMARY KEY(ZTPE_ID)
        USING INDEX
        TABLESPACE JDMO_INDX
        )
        TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE TABLE ZUIDCONSULT_ZUCO (
        ZUCO_ID NUMBER(19,0) NOT NULL,
        VERSION NUMBER(10,0) NOT NULL,
        UIDUTILISATEUR VARCHAR2(255 CHAR),
        ZJOH_ID_ZUIDCONSULT NUMBER(19,0),
        CONSTRAINT PK_ZUCO PRIMARY KEY(ZUCO_ID)
        USING INDEX
        TABLESPACE JDMO_INDX
        )
        TABLESPACE JDMO_DATA PCTFREE 10 PCTUSED 80
;

CREATE INDEX IN_ZFIE_ZPRD_1 ON ZFILTREEDITION_ZFIE (ZPRD_ID_LISTEFILTRES)
       TABLESPACE JDMO_INDX
;

ALTER TABLE ZFILTREEDITION_ZFIE
       ADD CONSTRAINT FK_ZFIE_ZPRD_1
       FOREIGN KEY (ZPRD_ID_LISTEFILTRES)
       REFERENCES ZPROFILDESTI_ZPRD
;

CREATE INDEX IN_ZFIV_ZFIE_1 ON ZFILTREVALUE_ZFIV (ZFIE_ID_VALSFILTRE)
    TABLESPACE JDMO_INDX
;

ALTER TABLE ZFILTREVALUE_ZFIV
        ADD CONSTRAINT FK_ZFIV_ZFIE_1
        FOREIGN KEY (ZFIE_ID_VALSFILTRE)
        REFERENCES ZFILTREEDITION_ZFIE;

CREATE INDEX IN_ZJOH_2 ON ZJOBHISTORY_ZJOH (DATEDEMANDE)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IN_ZJOH_3 ON ZJOBHISTORY_ZJOH (UIDPROPRIETAIRE)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IN_ZJOH_1 ON ZJOBHISTORY_ZJOH (BEANEDITIONID)
    TABLESPACE JDMO_INDX
;

ALTER TABLE ZJOBHISTORY_ZJOH
        ADD CONSTRAINT FK_ZJOH_ZCOE_1
        FOREIGN KEY (ZCOE_ID_LECONTENUEDITION)
        REFERENCES ZCONTENUEDITION_ZCOE
;

CREATE INDEX IN_ZPRD_ZJOH_1 ON ZPROFILDESTI_ZPRD (ZJOH_ID_PROFILSDESTI)
    TABLESPACE JDMO_INDX
;

CREATE INDEX IN_ZPRD_1 ON ZPROFILDESTI_ZPRD (NOMPROFIL)
    TABLESPACE JDMO_INDX
;

ALTER TABLE ZPROFILDESTI_ZPRD
        ADD CONSTRAINT FK_ZPRD_ZJOH_1
        FOREIGN KEY (ZJOH_ID_PROFILSDESTI)
        REFERENCES ZJOBHISTORY_ZJOH
;

CREATE INDEX IN_ZUCO_ZJOH_1 ON ZUIDCONSULT_ZUCO (ZJOH_ID_ZUIDCONSULT)
    TABLESPACE JDMO_INDX
;


ALTER TABLE ZUIDCONSULT_ZUCO
        ADD CONSTRAINT FK_ZUCO_ZJOH_1
        FOREIGN KEY (ZJOH_ID_ZUIDCONSULT)
        REFERENCES ZJOBHISTORY_ZJOH
;

CREATE UNIQUE INDEX IN_FK_ZJOH_ZCOE_1
        ON ZJOBHISTORY_ZJOH (ZCOE_ID_LECONTENUEDITION)
        TABLESPACE JDMO_INDX
;

CREATE INDEX IN_ZJOH_4
        ON ZJOBHISTORY_ZJOH (DATEPURGE)
        TABLESPACE JDMO_INDX
;

CREATE INDEX IN_ZJOH_5
        ON ZJOBHISTORY_ZJOH (APPLICATIONORIGINE)
        TABLESPACE JDMO_INDX
;

CREATE SEQUENCE ZCONTENUEDITION_ID_SEQUENCE
;

CREATE SEQUENCE ZFILTREEDITION_ID_SEQUENCE
;

CREATE SEQUENCE ZFILTREVALUE_ID_SEQUENCE
;

CREATE SEQUENCE ZJOBHISTORY_ID_SEQUENCE
;

CREATE SEQUENCE ZPROFILDESTI_ID_SEQUENCE
;

CREATE SEQUENCE ZTRCPURGEEDITION_ID_SEQUENCE
;

CREATE SEQUENCE ZUIDCONSULT_ID_SEQUENCE;
