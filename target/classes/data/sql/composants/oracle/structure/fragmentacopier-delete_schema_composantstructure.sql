    drop table TJ_ZECL_ZSTR_TJ cascade constraints;

    drop table TJ_ZSTR_ZATT_TJ cascade constraints;

    drop table TJ_ZSTR_ZFON_TJ cascade constraints;

    drop table ZADRESSE_ZADR cascade constraints;

    drop table ZATTRIBUTIONTGCP_ZATT cascade constraints;

    drop table ZCATEGORIESTRUCTURECP_ZCAS cascade constraints;

    drop table ZCODEPOSTAL_ZCPO cascade constraints;

    drop table ZCOMMUNE_ZCOM cascade constraints;

    drop table ZCOORDBANCAIRECP_ZCOB cascade constraints;

    drop table ZDEPARTEMENT_ZDEP cascade constraints;

    drop table ZDOMAINECP_ZDOM cascade constraints;

    drop table ZECLATEMENTCP_ZECL cascade constraints;

    drop table ZFONCTIONCP_ZFON cascade constraints;

    drop table ZGUICHETBANC_ZGUI cascade constraints;

    drop table ZMODEGESTIONCP_ZMGE cascade constraints;

    drop table ZPAYSINSEE_ZPIN cascade constraints;

    drop table ZPAYSISO_ZPIS cascade constraints;

    drop table ZRATTACHEMENTCP_ZRAT cascade constraints;

    drop table ZREGION_ZREG cascade constraints;

    drop table ZREROUTAGECOORDBANCAIRECP_ZRCB cascade constraints;

    drop table ZRIB_RIB cascade constraints;

    drop table ZSTRUCTURECP_ZSTR cascade constraints;

    drop table ZSTR_LESHORAIRESCP cascade constraints;

    drop table ZSTR_LESPOLESACTIV cascade constraints;

    drop table ZSTR_LESSERVICESCP cascade constraints;

    drop table ZTYPEPOLEACTIVITECP_ZTYP cascade constraints;

    drop table ZTYPESERVICECP_ZTYS cascade constraints;

    drop table ZTYPESERVICEDIRECTION_ZTSD cascade constraints;

    drop table ZTYPESTRUCTURECP_ZTSC cascade constraints;

    drop table ZPROPIMMEUBLE_ZPIM cascade constraints;

	drop table ZORGAACCUEIL_ZOAC cascade constraints;

	drop table ZLIENSDEP_ZLID  cascade constraints;

	drop table ZTYPELIEN_ZTLI  cascade constraints;

	drop table ZLID_QSTRUCTLIES  cascade constraints;

	drop sequence ZADR_ID_SEQUENCE;

    drop sequence ZATT_ID_SEQUENCE;

    drop sequence ZCAS_ID_SEQUENCE;

    drop sequence ZCOB_ID_SEQUENCE;

    drop sequence ZCOM_ID_SEQUENCE;

    drop sequence ZCPO_ID_SEQUENCE;

    drop sequence ZDEP_ID_SEQUENCE;

    drop sequence ZDOM_ID_SEQUENCE;

    drop sequence ZECL_ID_SEQUENCE;

    drop sequence ZFON_ID_SEQUENCE;

    drop sequence ZGUI_ID_SEQUENCE;

    drop sequence ZMGE_ID_SEQUENCE;

    drop sequence ZPAY_ID_SEQUENCE;

    drop sequence ZPIN_ID_SEQUENCE;

    drop sequence ZRAT_ID_SEQUENCE;

    drop sequence ZRCB_ID_SEQUENCE;

    drop sequence ZRIB_ID_SEQUENCE;

    drop sequence ZSTR_ID_SEQUENCE;

    drop sequence ZTSC_ID_SEQUENCE;

    drop sequence ZTSD_ID_SEQUENCE;

    drop sequence ZTYP_ID_SEQUENCE;

    drop sequence ZTYS_ID_SEQUENCE;

	drop sequence ZPIM_ID_SEQUENCE;

    drop sequence ZOAC_ID_SEQUENCE;

	drop  sequence ZLID_ID_SEQUENCE;

     drop  sequence ZTLI_ID_SEQUENCE;
