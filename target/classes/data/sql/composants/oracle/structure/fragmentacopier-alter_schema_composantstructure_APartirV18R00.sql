   
--rajout des index sur les champs utilisés pour faire des recherches dans structure


 create index IN_ZSTR_5 on ZSTRUCTURECP_ZSTR (CODEANNEXE)
                TABLESPACE @appli@_INDX;


create index IN_ZSTR_7  on ZSTRUCTURECP_ZSTR (ZPIN_ID_LEPAYS)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_8  on ZSTRUCTURECP_ZSTR (DATEDERNIEREMAJ)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_9  on ZSTRUCTURECP_ZSTR (ETAT)
                TABLESPACE @appli@_INDX;



create index IN_ZCAS_2 on ZCATEGORIESTRUCTURECP_ZCAS (LIBELLECATEGORIE)
                TABLESPACE @appli@_INDX;


create index IN_ZTSC_2 on ZTYPESTRUCTURECP_ZTSC (LIBELLETYPESTRUCTURE)
                TABLESPACE @appli@_INDX;


create index IN_ZRAT_1  on ZRATTACHEMENTCP_ZRAT (DATEDERNIEREMAJ)
                TABLESPACE @appli@_INDX;


create index IN_ZECL_2  on ZECLATEMENTCP_ZECL (DATEDERNIEREMAJ)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_10  on ZSTRUCTURECP_ZSTR (ZSTR_ID_LACENTRALCOMPTABLE)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_11  on ZSTRUCTURECP_ZSTR (ZTSC_ID_LETYPESTRUCTURECP)
                TABLESPACE @appli@_INDX;





 

