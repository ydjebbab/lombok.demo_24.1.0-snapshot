--creation schema jouirnal à partir V18R00

create table ZOPERATIONJOURNAL_ZOPE (
       ZOPE_ID number(19,0) not null,
        version number(10,0) not null,
        DATEHEUREOPERATION timestamp not null,
        IDENTIFIANTUTILISATEUROUBATCH varchar2(255 char) not null,
        natureOperation varchar2(255 char),
        identifiantStructure varchar2(255 char),
        typeDureeDeConservation varchar2(255 char),
        CONSTRAINT PK_ZOPERATIONJOURNAL_ZOPE primary key (ZOPE_ID)
                USING INDEX TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZOPERATIONJOURNAL_ZOPE unique (DATEHEUREOPERATION, IDENTIFIANTUTILISATEUROUBATCH)
                USING INDEX TABLESPACE @appli@_INDX
    ) TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80;

    create table ZPARAMETREOPERATION_ZPAO (
        ZPAO_ID number(19,0) not null,
        version number(10,0) not null,
        NOM varchar2(255 char) not null,
        VALEUR varchar2(255 char),
        CONSTRAINT PK_ZPARAMETREOPERATION_ZPAO primary key (ZPAO_ID)
               USING INDEX TABLESPACE @appli@_INDX
    ) TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80;

    create table TJ_ZOPE_ZPAO_TJ (
        ZOPE_ID number(19,0) not null,
        ZPAO_ID number(19,0) not null,
        CONSTRAINT PK_TJ_ZOPE_ZPAO_TJ primary key (ZOPE_ID,ZPAO_ID)
             USING INDEX TABLESPACE @appli@_INDX
    ) TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80;

    alter table TJ_ZOPE_ZPAO_TJ 
        add constraint FK_TJ_ZOPE_ZPAO_TJ_ZPAO_1 
        foreign key (ZPAO_ID)
        references ZPARAMETREOPERATION_ZPAO;

    alter table TJ_ZOPE_ZPAO_TJ 
        add constraint FK_TJ_ZOPE_ZPAO_TJ_ZOPE_1 
        foreign key (ZOPE_ID) 
        references ZOPERATIONJOURNAL_ZOPE;

CREATE SEQUENCE ZOPE_ID_SEQUENCE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  CACHE 20;

CREATE SEQUENCE ZPAO_ID_SEQUENCE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  CACHE 20;

CREATE INDEX IN_ZOPE_1 ON ZOPERATIONJOURNAL_ZOPE (NATUREOPERATION)
        TABLESPACE @appli@_INDX;

CREATE INDEX IN_ZOPE_2 ON ZOPERATIONJOURNAL_ZOPE (IDENTIFIANTSTRUCTURE)
        TABLESPACE @appli@_INDX;

CREATE INDEX IN_ZOPE_3 ON ZOPERATIONJOURNAL_ZOPE (IDENTIFIANTUTILISATEUROUBATCH)
        TABLESPACE @appli@_INDX;        
        
CREATE INDEX IN_TJ_ZOPE_ZPAO_TJ_1  ON TJ_ZOPE_ZPAO_TJ(ZPAO_ID)
        TABLESPACE @appli@_INDX;        




 alter table ZOPERATIONJOURNAL_ZOPE
		add  apurementPossible  number(1,0);

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  codeOperation  varchar2(255 char);


alter table  ZOPERATIONJOURNAL_ZOPE
		add  valeurIdentifiantMetier1  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  valeurIdentifiantMetier2  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE
		add  valeurIdentifiantMetier3  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  valeurIdentifiantMetier4  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  valeurIdentifiantMetier5  varchar2(255 char) ;

alter table ZOPERATIONJOURNAL_ZOPE 
		add  libelleIdentifiantMetier1  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE
		add  libelleIdentifiantMetier2  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  libelleIdentifiantMetier3  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  libelleIdentifiantMetier4  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE
		add  libelleIdentifiantMetier5  varchar2(255 char) ;
 

create index IN_ZOPE_4 on  ZOPERATIONJOURNAL_ZOPE (libelleIdentifiantMetier1)
                TABLESPACE @appli@_INDX ;   
create index IN_ZOPE_5 on  ZOPERATIONJOURNAL_ZOPE (libelleIdentifiantMetier2)
                TABLESPACE @appli@_INDX ;   
create index IN_ZOPE_6 on  ZOPERATIONJOURNAL_ZOPE (libelleIdentifiantMetier3)
                TABLESPACE @appli@_INDX ;   
create index IN_ZOPE_7 on  ZOPERATIONJOURNAL_ZOPE (libelleIdentifiantMetier4)
                TABLESPACE @appli@_INDX ;   
create index IN_ZOPE_8 on  ZOPERATIONJOURNAL_ZOPE (libelleIdentifiantMetier5)
                TABLESPACE @appli@_INDX ;
create index IN_ZOPE_9 on ZOPERATIONJOURNAL_ZOPE (valeurIdentifiantMetier1)
                TABLESPACE @appli@_INDX ;   
create index IN_ZOPE_10 on ZOPERATIONJOURNAL_ZOPE (valeurIdentifiantMetier2)
                TABLESPACE @appli@_INDX ;   
create index IN_ZOPE_11 on ZOPERATIONJOURNAL_ZOPE (valeurIdentifiantMetier3)
                TABLESPACE @appli@_INDX ;   
create index IN_ZOPE_12 on ZOPERATIONJOURNAL_ZOPE (valeurIdentifiantMetier4)
                TABLESPACE @appli@_INDX ;   
create index IN_ZOPE_13 on ZOPERATIONJOURNAL_ZOPE (valeurIdentifiantMetier5)
                TABLESPACE @appli@_INDX ;   

alter table ZPARAMETREOPERATION_ZPAO 
		add  valeurPrecedente  varchar2(255 char) ;

