---
--- suppression séquences
---
DROP SEQUENCE MESSAGE_ID_SEQUENCE 
;
DROP SEQUENCE MESSAGETYPE_ID_SEQUENCE 
;
DROP SEQUENCE PROFIL_ID_SEQUENCE 
;
DROP SEQUENCE CODIQUE_ID_SEQUENCE; 
 
---
--- suppression index
---

ALTER TABLE MESSAGE_MESS 
    DROP INDEX IN_PROF_MESS_1 
;
---
--- suppression clés étrangères
 

alter table  TJ_MESS_CODI_TJ         
        drop  constraint FK_TJ_MESS_CODI_TJ_MESS_1  

alter table  TJ_MESS_CODI_TJ         
        drop  constraint FK_TJ_MESS_CODI_TJ_CODI_1 



alter  table  TJ_MESS_PROF_TJ        
        drop constraint   FK_TJ_MESS_PROF_TJ_MESS_1  ;

alter  table  TJ_MESS_PROF_TJ        
        drop  constraint FK_TJ_MESS_PROF_TJ_PROF_1   ;

alter table PROFIL_PROF   
     drop    constraint FK_PROF_MESS__1 ; 


 

 
 
---
--- suppression tables
---

drop  table  TJ_MESS_PROF_TJ  ;

drop table  TJ_MESS_CODI_TJ 

 drop table MESSAGE_MESS  ;

 drop table  MESSAGETYPE_METY  ;

 drop table PROFIL_PROF  ;

drop table CODIQUE_CODI