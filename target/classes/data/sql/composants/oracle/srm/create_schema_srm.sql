
    drop table MESSAGETYPE_METY cascade constraints;

    drop table MESSAGE_MESS cascade constraints;

    drop table PROFIL_PROF cascade constraints;

    drop sequence MESSAGETYPE_ID_SEQUENCE;

    drop sequence MESSAGE_ID_SEQUENCE;

    drop sequence PROFIL_ID_SEQUENCE;

    drop table CODIQUE_CODI cascade constraints;    
    
    drop table TJ_MESS_CODI_TJ cascade constraints;

    drop table TJ_MESS_PROF_TJ cascade constraints;       

    drop sequence CODIQUE_ID_SEQUENCE;
    
    create table MESSAGETYPE_METY (
        MESSAGETYPE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        LIBELLEMESSAGETYPE varchar2(2000 char) not null,
        primary key (MESSAGETYPE_ID),
        unique (LIBELLEMESSAGETYPE)
    );

    create table MESSAGE_MESS (
        MESSAGE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        LIBELLEMESSAGE varchar2(2000 char) not null,
        DATECREATIONMESSAGE timestamp not null,
        DATEDEBMESSAGE timestamp,
        DATEFINMESSAGE timestamp,
        DATEDEBINACPROF timestamp,
        DATEFININACPROF timestamp,
        TYPEMESSAGE varchar2(255 char),
        primary key (MESSAGE_ID),
        unique (LIBELLEMESSAGE, DATECREATIONMESSAGE)
    );

    create table PROFIL_PROF (
        PROFIL_ID number(19,0) not null,
        VERSION number(10,0) not null,
        CODEPROFILCONCATCODEAPPLI varchar2(255 char) not null,
        LIBELLEPROFIL varchar2(255 char) not null,
        CODEPROFIL varchar2(255 char) not null,
        MESS_ID_LISTPROFDESACT number(19,0),
        primary key (PROFIL_ID),
        unique (CODEPROFILCONCATCODEAPPLI)
    );


    create index IN_PROF_MESS_1 on PROFIL_PROF (MESS_ID_LISTPROFDESACT);

    alter table PROFIL_PROF 
        add constraint FK_PROF_MESS__1 
        foreign key (MESS_ID_LISTPROFDESACT) 
        references MESSAGE_MESS;

    create sequence MESSAGETYPE_ID_SEQUENCE;

    create sequence MESSAGE_ID_SEQUENCE;

    create sequence PROFIL_ID_SEQUENCE;

    create table CODIQUE_CODI (
        CODIQUE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        LIBELLECODIQUE varchar2(255 char) not null,
        primary key (CODIQUE_ID),
        unique (LIBELLECODIQUE)
    ); 
    
    create table  TJ_MESS_CODI_TJ (
        MESS_ID number(19,0) not null,
        CODI_ID number(19,0) not null,
        primary key (MESS_ID, CODI_ID)
    );

    create table  TJ_MESS_PROF_TJ (
        MESS_ID number(19,0) not null,
        PROF_ID number(19,0) not null,
        primary key (MESS_ID, PROF_ID)
    );

    

    alter table MESSAGE_MESS 
        add UIDCREATEURMESSAGE varchar2(255 char) ;   

   
    alter table TJ_MESS_CODI_TJ 
        add constraint FK_TJ_MESS_CODI_TJ_MESS_1 
        foreign key (MESS_ID) 
        references MESSAGE_MESS;

    alter table TJ_MESS_CODI_TJ 
        add constraint FK_TJ_MESS_CODI_TJ_CODI_1 
        foreign key (CODI_ID) 
        references CODIQUE_CODI;

    alter table TJ_MESS_PROF_TJ 
        add constraint FK_TJ_MESS_PROF_TJ_MESS_1 
        foreign key (MESS_ID) 
        references MESSAGE_MESS;

    alter table TJ_MESS_PROF_TJ 
        add constraint FK_TJ_MESS_PROF_TJ_PROF_1 
        foreign key (PROF_ID) 
        references PROFIL_PROF;

   
    create sequence CODIQUE_ID_SEQUENCE;
    