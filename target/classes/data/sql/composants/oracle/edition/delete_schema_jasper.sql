---
--- SUPPRESSION DU SCHEMA DU COMPOSANT EDITION
--- VERSION : V13R08
--- 
--- Modifications 
--- wpetit        03/05/2007    creation      
---

    delete from qrtz_job_listeners;

    delete from qrtz_trigger_listeners;

    delete from qrtz_fired_triggers;

    delete from qrtz_simple_triggers;

    delete from qrtz_cron_triggers;

    delete from qrtz_blob_triggers;

    delete from qrtz_triggers;

    delete from qrtz_job_details;

    delete from qrtz_calendars;

    delete from qrtz_paused_trigger_grps;

    delete from qrtz_locks;

    delete from qrtz_scheduler_state;

    drop table qrtz_calendars;

    drop table qrtz_fired_triggers;

    drop table qrtz_trigger_listeners;

    drop table qrtz_blob_triggers;

    drop table qrtz_cron_triggers;

    drop table qrtz_simple_triggers;

    drop table qrtz_triggers;

    drop table qrtz_job_listeners;

    drop table qrtz_job_details;

    drop table qrtz_paused_trigger_grps;

    drop table qrtz_locks;

    drop table qrtz_scheduler_state;


    drop table CONTENUEDITION_COED cascade constraints;

    drop table FILTREEDITION_FIED cascade constraints;

    drop table FILTREVALUE_FIVA cascade constraints;

    drop table JOBHISTORY_JOHI cascade constraints;

    drop table PROFILDESTI_PRDE cascade constraints;

    drop table TRCPURGEEDITION_TRPE cascade constraints;

    drop table UIDCONSULT_UICO cascade constraints;

    drop sequence CONTENUEDITION_ID_SEQUENCE;

    drop sequence FILTREEDITION_ID_SEQUENCE;

    drop sequence FILTREVALUE_ID_SEQUENCE;

    drop sequence JOBHISTORY_ID_SEQUENCE;

    drop sequence PROFILDESTI_ID_SEQUENCE;

    drop sequence TRCPURGEEDITION_ID_SEQUENCE;

    drop sequence UIDCONSULT_ID_SEQUENCE;
