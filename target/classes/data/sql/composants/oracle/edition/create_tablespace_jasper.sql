---
--- CREATION DES TABLESPACES DU SCHEMA DU COMPOSANT EDITION
--- VERSION : V13R05
--- 
--- Modifications 
--- wpetit        03/05/2007    creation      
---

CREATE TABLESPACE J@appli@_DATA DATAFILE '/oradata/J@appli@_data01.dbf' SIZE 100M
 AUTOEXTEND ON NEXT 1280K
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 128K
;
CREATE TABLESPACE J@appli@_INDX DATAFILE '/oradata/J@appli@_indx01.dbf' SIZE 50M
 AUTOEXTEND ON NEXT 1280K
EXTENT MANAGEMENT LOCAL UNIFORM SIZE 128K
;
CREATE TABLESPACE J@appli@_BLOB DATAFILE '/oradata/J@appli@_blob01.dbf' SIZE 100M
 AUTOEXTEND ON NEXT 1280K
 EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO
;
