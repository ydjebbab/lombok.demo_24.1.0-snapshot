--- Création du schéma du composant journal, à partir de la V15R03

    create table OPERATIONJOURNAL_OPER (
        OPER_ID numeric(19,0) not null,
        version numeric(10,0) not null,
        DATEHEUREOPERATION timestamp not null,
        IDENTIFIANTUTILISATEUROUBATCH varchar(255) not null,
        natureOperation varchar(255),
        identifiantStructure varchar(255),
        typeDureeDeConservation varchar(255),
        CONSTRAINT PK_OPERATIONJOURNAL_OPER primary key (OPER_ID)
                USING INDEX TABLESPACE @appli@_INDX,
        CONSTRAINT UK_OPERATIONJOURNAL_OPER unique (DATEHEUREOPERATION, IDENTIFIANTUTILISATEUROUBATCH)
                USING INDEX TABLESPACE @appli@_INDX
    ) TABLESPACE @appli@_DATA ;

    create table PARAMETREOPERATION_PAOP (
        PAOP_ID numeric(19,0) not null,
        version numeric(10,0) not null,
        NOM varchar(255) not null,
        VALEUR varchar(255),
        CONSTRAINT PK_PARAMETREOPERATION_PAOP primary key (PAOP_ID)
               USING INDEX TABLESPACE @appli@_INDX
    ) TABLESPACE @appli@_DATA ;

    create table TJ_OPER_PAOP_TJ (
        OPER_ID numeric(19,0) not null,
        PAOP_ID numeric(19,0) not null,
        CONSTRAINT PK_TJ_OPER_PAOP_TJ primary key (OPER_ID, PAOP_ID)
             USING INDEX TABLESPACE @appli@_INDX
    ) TABLESPACE @appli@_DATA ;

    alter table TJ_OPER_PAOP_TJ 
        add constraint FK_TJ_OPER_PAOP_TJ_PAOP_1 
        foreign key (PAOP_ID) 
        references PARAMETREOPERATION_PAOP;

    alter table TJ_OPER_PAOP_TJ 
        add constraint FK_TJ_OPER_PAOP_TJ_OPER_1 
        foreign key (OPER_ID) 
        references OPERATIONJOURNAL_OPER;

CREATE SEQUENCE OPER_ID_SEQUENCE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  CACHE 20;

CREATE SEQUENCE PAOP_ID_SEQUENCE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  CACHE 20;

CREATE INDEX IN_OPER_1 ON OPERATIONJOURNAL_OPER (NATUREOPERATION)
        TABLESPACE @appli@_INDX;

CREATE INDEX IN_OPER_2 ON OPERATIONJOURNAL_OPER (IDENTIFIANTSTRUCTURE)
        TABLESPACE @appli@_INDX;

CREATE INDEX IN_OPER_3 ON OPERATIONJOURNAL_OPER (IDENTIFIANTUTILISATEUROUBATCH)
        TABLESPACE @appli@_INDX;        
        
CREATE INDEX IN_TJ_OPER_PAOP_TJ_1  ON TJ_OPER_PAOP_TJ(PAOP_ID)
        TABLESPACE @appli@_INDX;        
