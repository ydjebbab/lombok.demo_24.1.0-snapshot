    CREATE INDEX IDX_ZCOF_ID_LECONTENUDUFICHIER 
        ON ZFICHIERJOINT_ZFIJ(ZCOF_ID_LECONTENUDUFICHIER) 
        TABLESPACE @appli@_INDX;
    
    CREATE INDEX IDX_ZFIJ_ID_LEFICHIER 
        ON ZDOCUMENTATLAS_ZDOA(ZFIJ_ID_LEFICHIER) 
        TABLESPACE @appli@_INDX;