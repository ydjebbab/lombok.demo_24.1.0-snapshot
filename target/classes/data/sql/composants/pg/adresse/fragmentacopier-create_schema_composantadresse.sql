create table ZCODEPOSTAL_ZCPO (
        ZCPO_ID numeric(19,0) not null,
        version numeric(10,0) not null,
        code varchar(255) not null,
        ville varchar(255) not null,
        CONSTRAINT PK_ZCODEPOSTAL_ZCPO primary key (ZCPO_ID)
            USING INDEX TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZCODEPOSTAL_ZCPO unique (code, ville)
            USING INDEX TABLESPACE @appli@_INDX
    ) TABLESPACE @appli@_DATA;
    
create index IN_ZCPO_1 on ZCODEPOSTAL_ZCPO (code)
            TABLESPACE @appli@_INDX;
create index IN_ZCPO_2 on ZCODEPOSTAL_ZCPO (ville)
            TABLESPACE @appli@_INDX;

create sequence ZCPO_ID_SEQUENCE;
