    create table CONTENUFICHIER_COFIC (
        id numeric(19,0) not null,
        version numeric(10,0) not null,
        DATA blob,
        CONSTRAINT PK_COFIC primary key (id) USING INDEX TABLESPACE DMO_INDX
    )
    TABLESPACE @appli@_DATA 
    ;

    create table FICHIERJOINT_FIJO (
        id numeric(19,0) not null,
        version numeric(10,0) not null,
        dateHeureSoumission timestamp,
        nomFichierOriginal varchar(255),
        tailleFichier numeric(19,0),
        typeMimeFichier varchar(255) not null,
        COFI_ID_LECONTENUDUFICHIER numeric(19,0),
        CONSTRAINT PK_FIJO primary key (id) USING INDEX TABLESPACE DMO_INDX
    )
    TABLESPACE @appli@_DATA 
    ;

    alter table FICHIERJOINT_FIJO 
        add constraint FK_FIJO_COFI_1 
        foreign key (COFI_ID_LECONTENUDUFICHIER) 
        references CONTENUFICHIER_COFIC;

	create sequence COFIC_ID_SEQUENCE;

	create sequence FIJO_ID_SEQUENCE;