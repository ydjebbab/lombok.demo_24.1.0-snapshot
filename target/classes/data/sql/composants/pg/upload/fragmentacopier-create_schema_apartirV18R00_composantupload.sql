    create table ZCONTENUFICHIER_ZCOF (
        id numeric(19,0) not null,
        version numeric(10,0) not null,
        DATA blob,
        CONSTRAINT PK_ZCOF primary key (id) USING INDEX TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA 
    ;

    create table ZFICHIERJOINT_ZFIJ (
        id numeric(19,0) not null,
        version numeric(10,0) not null,
        dateHeureSoumission timestamp,
        nomFichierOriginal varchar(255),
        tailleFichier numeric(19,0),
        typeMimeFichier varchar(255) not null,
        ZCOF_ID_LECONTENUDUFICHIER numeric(19,0),
        CONSTRAINT PK_ZFIJ primary key (id) USING INDEX TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA 
    ;

    alter table ZFICHIERJOINT_ZFIJ 
        add constraint FK_ZFIJ_ZCOF_1 
        foreign key (ZCOF_ID_LECONTENUDUFICHIER) 
        references ZCONTENUFICHIER_ZCOF;

	create sequence ZCOF_ID_SEQUENCE;

	create sequence ZFIJ_ID_SEQUENCE;