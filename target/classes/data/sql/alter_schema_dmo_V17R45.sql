	create table DOCUMENTATLAS_DOAT (
        DOAT_ID number(19,0) not null,
        VERSION number(10,0) not null,
        identifiantAtlas varchar2(255 char),
        nom varchar2(255 char) not null,
        ETAT number(10,0) not null,
        dateDePeremption timestamp,
        dateDEnvoiAtlas timestamp,
        dateAccuseReception timestamp,
        datePurgeContenu timestamp,
		FIJO_ID_LEFICHIER number(19,0),
        CONSTRAINT PK_DOAT PRIMARY KEY(DOAT_ID)
            USING INDEX
            TABLESPACE dmo_INDX,
        CONSTRAINT UK_DOAT_1 UNIQUE(identifiantAtlas)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;

	alter table DOCUMENTATLAS_DOAT 
        add constraint FK_DOAT_FIJO_1
        foreign key (FIJO_ID_LEFICHIER) 
        references FICHIERJOINT_FIJO;

	create sequence DOAT_ID_SEQUENCE;