---
--- MODIFICATION DU SCHEMA DU COMPOSANT EDITION
--- VERSION : V14R06
--- 
---
CREATE UNIQUE INDEX IN_FK_JOHI_COED_1
        ON JOBHISTORY_JOHI (COED_ID_LECONTENUEDITION)
        TABLESPACE J@appli@_INDX
;

CREATE INDEX IN_JOHI_4
        ON JOBHISTORY_JOHI (DATEPURGE)
        TABLESPACE J@appli@_INDX
;

CREATE INDEX IN_JOHI_5
        ON JOBHISTORY_JOHI (APPLICATIONORIGINE)
        TABLESPACE J@appli@_INDX
;
