 alter table CONTRATMENSUALISATION_COME 
        drop constraint FK_COME_RIB_1;

 alter table CONTRATMENSUALISATION_COME 
        add constraint FK_COME_RIB_1 
        foreign key (RIB_ID_RIB) 
        references ZRIB_RIB;
  
 create table ZGUICHETBANC_ZGUI (
        ZGUI_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeEtablissement varchar2(255 char),
        codeGuichet varchar2(255 char),
        nomGuichet varchar2(255 char),
        adresseGuichet varchar2(255 char),
        codePostal varchar2(255 char),
        villeCodePostal varchar2(255 char),
        denominationComplete varchar2(255 char),
        denominationAbregee varchar2(255 char),
        libelleAbrevDeDomiciliation varchar2(255 char),
        codeReglementDesCredits varchar2(255 char),
        moisDeModification varchar2(255 char),
        anneeDeModification varchar2(255 char),
        codeEnregistrement varchar2(255 char),
        nouveauCodeEtablissement varchar2(255 char),
        nouveauCodeGuichet varchar2(255 char),
        estEnVoieDePeremption number(1,0),
        estPerime number(1,0),
        dateDerniereMiseAJour timestamp,
        CONSTRAINT PK_ZGUI primary key (ZGUI_ID) USING INDEX TABLESPACE DMO_INDX
    );
 create index IN_ZGUI_2 on ZGUICHETBANC_ZGUI (codeGuichet)
         TABLESPACE DMO_INDX;

 create index IN_ZGUI_3 on ZGUICHETBANC_ZGUI (dateDerniereMiseAJour)
         TABLESPACE DMO_INDX;

 create index IN_ZGUI_1 on ZGUICHETBANC_ZGUI (codeEtablissement)
          TABLESPACE DMO_INDX;
 
 create sequence ZGUI_ID_SEQUENCE;
 
 create sequence ZPAY_ID_SEQUENCE;
 
 drop sequence ZRIB_ID_SEQUENCE;
 create sequence ZRIB_ID_SEQUENCE
              START WITH   100000
              INCREMENT BY 1
              MINVALUE     1
              NOMAXVALUE
              CACHE        20
              NOCYCLE
              NOORDER;
 
 drop sequence ZCOB_ID_SEQUENCE;
 create sequence ZCOB_ID_SEQUENCE
 START WITH   100000
                    INCREMENT BY 1
                    MINVALUE     1
                    NOMAXVALUE
                    CACHE        20
                    NOCYCLE
                    NOORDER;
 
 alter table ZCOORDBANCAIRECP_ZCOB 
        drop constraint FK_ZCOB_ZRIB_1;
