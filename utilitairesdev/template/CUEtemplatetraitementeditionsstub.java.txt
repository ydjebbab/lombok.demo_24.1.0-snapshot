package @PREFIXEPACKAGE@.edition;

import java.util.ArrayList;
import java.util.Collection;

import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunStubImpl;

public class TraitementEditions@CUE@Stub extends
		AbstractServiceEditionCommunStubImpl {


	public TraitementEditions@CUE@Stub()
		{
		super();
		}

 	public static Collection createBeanCollection()
 	{
 		Collection list = new ArrayList();
 		//TODO construire des objets servant de base à l'edition avec des valeurs d'exemple et les mettre dans la liste
 		return list;
 	}

}
