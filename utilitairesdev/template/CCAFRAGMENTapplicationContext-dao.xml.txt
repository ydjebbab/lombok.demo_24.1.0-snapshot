<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:context="http://www.springframework.org/schema/context"
  xsi:schemaLocation="http://www.springframework.org/schema/beans
    http://www.springframework.org/schema/beans/spring-beans.xsd">

<!-- elements ci-dessous a recopier dans applicationContext-dao.xml entre les balises <beans> </beans>-->

  	<bean id="@CCAMINUS@dao" parent="daoproxyabstract">
		<property name="target">
			<ref bean="@CCAMINUS@daoimpl"/> 
		</property>
	</bean>
	<bean id="@CCAMINUS@daoimpl" class="@PREFIXEPACKAGE@.dao.impl.@CCA@DaoImpl" parent="targetdaoabstract"/>

<!-- fin elements a copier -->

</beans>
	