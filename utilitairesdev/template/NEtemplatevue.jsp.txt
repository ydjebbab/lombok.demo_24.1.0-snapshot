<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>
<app:page titreecran="mon titre ecran" 
          titrecontainer="mon titre container" 
          menu="true"> 

<app:onglets cleactive="1">
  	<app:onglet cle="1" libelle="vu1"/>
  	<app:onglet cle="2" libelle="vue2"/>
</app:onglets>

<app:form 
	action="@ZFNE@flux.ex" 
	formobjectname="@CUMINUS@form" 
	formboxclass="donnees sousonglets">
	<p>hello</p>
	<app:boxboutons>
		<app:submit label="bouton1" transition="bouton1"/>
	</app:boxboutons>
</app:form>
</app:page>