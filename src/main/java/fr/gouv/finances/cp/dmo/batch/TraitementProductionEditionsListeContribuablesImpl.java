/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.batch;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.tar.ArchiveTar;

/**
 * Batch TraitementProductionEditionsListeContribuablesImpl
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Service("traitementproductioneditionslistecontribuables")
@Profile("batch")
@Lazy(true)
public class TraitementProductionEditionsListeContribuablesImpl extends ServiceBatchCommunImpl
{

    // Exemple de batch qui contient 2 archives
    @Autowired()
    @Qualifier("editiondemandeserviceimpl")
    private EditionDemandeService editiondemandeserviceso;

    private String fichierArchive;

    public TraitementProductionEditionsListeContribuablesImpl()
    {
        super();
    }

    @Value("${traitementproductioneditionslistecontribuables.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch)
    {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementproductioneditionslistecontribuables.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }

    public EditionDemandeService getEditiondemandeserviceso()
    {
        return editiondemandeserviceso;
    }

    public String getFichierArchive()
    {
        return fichierArchive;
    }

    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso)
    {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    @Value("${traitementproductioneditionslistecontribuables.fichierarchive}")
    public void setFichierArchive(String fichierArchive)
    {
        this.fichierArchive = fichierArchive;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        // initialisation de l'édition
        EditionSynchroneNonNotifieeParMail editionSynchroneNonNotifieeParMail =
            editiondemandeserviceso.initEditionSynchroneNonNotifieeParMail("zf2.listecontribuables.edition", "amleplatinec-cp");
        Map<?, ?> lesParameditions = new HashMap<>();
        /*
         * on passe par un fichier joint afin de récupérer directement l'édition ( si declencherEditionxxx est utilisée
         * , on peut avoir un soucis quand on cherche à récupérer l'edition : celle ci peut ne pas être créée
         */
        FichierJoint unFichierJoint = editiondemandeserviceso
            .declencherProductionEditionCompresseeNonStockee(editionSynchroneNonNotifieeParMail.getBeanEditionId(), lesParameditions);
        EditionSynchroneNonNotifieeParMail editionSynchroneNonNotifieeParMail1 =
            editiondemandeserviceso.initEditionSynchroneNonNotifieeParMail("zf8.lancertachelongue.edition", "amleplatinec-cp");
        FichierJoint unFichierJoint1 = editiondemandeserviceso
            .declencherProductionEditionCompresseeNonStockee(editionSynchroneNonNotifieeParMail1.getBeanEditionId(), lesParameditions);
        // création d'une archice tar qui contiendra les editions créées
        ArchiveTar archiveTar;
        try
        {
            /*
             * nom de l'archive à externaliser dans le batch + dans le cas application metier nom comportant uid
             * utilisateur codique poste ... pour la retrouver plus facilement
             */
            archiveTar = new ArchiveTar(fichierArchive);
            archiveTar.creerEntree(unFichierJoint.getNomFichierOriginal(), unFichierJoint.getLeContenuDuFichier().getData());
            archiveTar.creerEntree(unFichierJoint1.getNomFichierOriginal(), unFichierJoint1.getLeContenuDuFichier().getData());
            // Clôture l'archive TAR
            archiveTar.fermerArchive();
        }
        catch (IOException exc)
        {
            log.error("Erreur lors de la création de l'archive au format tar", exc);
        }
    }

}
