/*
 * Copyright (c) 2018 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.config;

/**
 * Enum Authorite : liste des rôles/authorités de l'application DMO.
 */
public enum Authorite
{
    /** exploitant. */
    EXPLOITANT,
    /** chefdeprojetapplicatif. */
    CHEFDEPROJETAPPLICATIF,
    /** particulier. */
    PARTICULIER,
    /** administrateur. */
    ADMINISTRATEUR,
    /** notaires. */
    NOTAIRES,

    /** rolls. Par défaut nous sommes tous habilités */
    ROLLS
    {
        @Override
        public String toString()
        {
            return "0";
        }
    },

    /** Rôle qui n'existe pas. */
    ROLE_QUI_N_EXISTE_PAS
}
