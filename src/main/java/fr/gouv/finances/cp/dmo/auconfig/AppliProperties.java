package fr.gouv.finances.cp.dmo.auconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Classe de configuration pour les propriétés présentes dans application.properties et spécifiques à l'application
 * @author celinio fernandes
 * Date: Feb 25, 2020
 */
@ConfigurationProperties(prefix = "appli")
public class AppliProperties
{

    private String name;

    private String buildVersion;

    private String lombokVersion;

    private String version;

    private String libelleCourt;

    private String libelleLong;
    
    private String test;
    
    private Integer profiltest;
    
    private String dntest;
    
    private String typeAnnuairetest;
    
    private String meltest;
    
    private String notenvoimel;
        
    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getLibelleCourt()
    {
        return libelleCourt;
    }

    public void setLibelleCourt(String libelleCourt)
    {
        this.libelleCourt = libelleCourt;
    }

    public String getLibelleLong()
    {
        return libelleLong;
    }

    public void setLibelleLong(String libelleLong)
    {
        this.libelleLong = libelleLong;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getBuildVersion()
    {
        return buildVersion;
    }

    public void setBuildVersion(String buildVersion)
    {
        this.buildVersion = buildVersion;
    }

    public String getLombokVersion()
    {
        return lombokVersion;
    }

    public void setLombokVersion(String lombokVersion)
    {
        this.lombokVersion = lombokVersion;
    }
    
    public String getTest()
    {
        return test;
    }

    public void setTest(String test)
    {
        this.test = test;
    }

    public Integer getProfiltest()
    {
        return profiltest;
    }

    public void setProfiltest(Integer profiltest)
    {
        this.profiltest = profiltest;
    }

    public String getdntest()
    {
        return dntest;
    }

    public void setdntest(String dntest)
    {
        this.dntest = dntest;
    }

    public String getTypeAnnuairetest()
    {
        return typeAnnuairetest;
    }

    public void setTypeAnnuairetest(String typeAnnuairetest)
    {
        this.typeAnnuairetest = typeAnnuairetest;
    }

    public String getMeltest()
    {
        return meltest;
    }

    public void setMeltest(String meltest)
    {
        this.meltest = meltest;
    }

    public String getNotenvoimel()
    {
        return notenvoimel;
    }

    public void setNotenvoimel(String notenvoimel)
    {
        this.notenvoimel = notenvoimel;
    }

    @ConfigurationProperties(prefix = "appli.inea")
    public static class Inea
    {
        private String version;

        private String jqueryVersion;

        private String cssUrl;

        public String getVersion()
        {
            return version;
        }

        public void setVersion(String version)
        {
            this.version = version;
        }

        public String getJqueryVersion()
        {
            return jqueryVersion;
        }

        public void setJqueryVersion(String jqueryVersion)
        {
            this.jqueryVersion = jqueryVersion;
        }

        public String getCssUrl()
        {
            return cssUrl;
        }

        public void setCssUrl(String cssUrl)
        {
            this.cssUrl = cssUrl;
        }
    }


}
