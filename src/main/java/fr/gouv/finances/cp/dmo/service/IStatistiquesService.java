/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : StatistiquesService.java
 *
 */
package fr.gouv.finances.cp.dmo.service;

import java.util.List;

import fr.gouv.finances.cp.dmo.techbean.StatistiquesAvisImpositions;
import fr.gouv.finances.cp.dmo.techbean.StatistiquesContribuables;

/**
 * Interface IStatistiquesService
 * 
 * @author chouard-cp
 * @author CF
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public interface IStatistiquesService
{

    /**
     * methode Calculer agregation avis imposition par type impot : DGFiP.
     * 
     * @return list
     */
    public List<StatistiquesAvisImpositions> calculerAgregationAvisImpositionParTypeImpot();

    /**
     * methode Calculer agregation avis imposition par type impot seuil : DGFiP.
     * 
     * @param seuil param
     * @return list
     */
    public List<StatistiquesAvisImpositions>  calculerAgregationAvisImpositionParTypeImpotSeuil(Double seuil);

    /**
     * methode Calculer agregation contribuable par type impot : DGFiP.
     * 
     * @return list
     */
    public List<StatistiquesContribuables> calculerAgregationContribuableParTypeImpot();

    /**
     * methode Calculer agregation par contribuable : DGFiP.
     * 
     * @return list
     */
    public List<StatistiquesContribuables> calculerAgregationParContribuable();
}
