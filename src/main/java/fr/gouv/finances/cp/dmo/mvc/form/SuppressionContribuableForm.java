/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : SuppressionContribuableForm.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;
import java.util.List;

/**
 * Class SuppressionContribuableForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class SuppressionContribuableForm implements Serializable
{

    private static final long serialVersionUID = 470410828253049636L;

    /** liste des contribuables a supprimer. */
    private List listeDesContribuablesASupprimer;

    public SuppressionContribuableForm()
    {
        super();
    }

    /**
     * Gets the liste des contribuables a supprimer.
     *
     * @return the liste des contribuables a supprimer
     */
    public List getListeDesContribuablesASupprimer()
    {
        return listeDesContribuablesASupprimer;
    }

    /**
     * Sets the liste des contribuables a supprimer.
     *
     * @param listedescontribuablesasupprimer the new liste des contribuables a supprimer
     */
    public void setListeDesContribuablesASupprimer(List listedescontribuablesasupprimer)
    {
        this.listeDesContribuablesASupprimer = listedescontribuablesasupprimer;
    }

}
