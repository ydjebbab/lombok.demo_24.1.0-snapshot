/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : ReferenceTypesImpotsValidator.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.validator;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.dmo.mvc.form.ReferenceTypesImpotsForm;

/**
 * Class ReferenceTypesImpotsValidator DGFiP.
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class ReferenceTypesImpotsValidator implements Validator
{

    /**
     * Instanciation de reference types impots validator.
     */
    public ReferenceTypesImpotsValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param arg0
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    public boolean supports(Class arg0)
    {
        return arg0.equals(ReferenceTypesImpotsForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param arg0
     * @param arg1
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    public void validate(Object arg0, Errors arg1)
    {

        this.validateSurfaceGestionTypesImpots((ReferenceTypesImpotsForm) arg0, arg1);
    }

    /**
     * Vérification que les champs obligatoires sont saisis.
     *
     * @param command param
     * @param errors param
     */
    public void validateSurfaceGestionTypesImpots(ReferenceTypesImpotsForm command, Errors errors)
    {

        if (command.getTypeImpot().getCodeImpot() == null
            || GenericValidator.isBlankOrNull(command.getTypeImpot().getCodeImpot().toString()))
        {
            errors.rejectValue("typeImpot.codeImpot", "rejected.typeImpot.codeImpot",
                "La saisie du code impot est obligatoire.");

        }

        if (command.getTypeImpot().getSeuil() == null
            || GenericValidator.isBlankOrNull(command.getTypeImpot().getSeuil().toString()))
        {
            errors.rejectValue("typeImpot.seuil", "rejected.typeImpot.seuil", "La saisie du seuil est obligatoire.");

        }

        if (GenericValidator.isBlankOrNull(command.getTypeImpot().getNomImpot()))
        {
            errors.rejectValue("typeImpot.nomImpot", "rejected.typeImpot.nomImpot",
                "La saisie du nom  impot est obligatoire.");

        }

        if (command.getTypeImpot().getTaux() == null
            || GenericValidator.isBlankOrNull(command.getTypeImpot().getTaux().toString()))
        {
            errors.rejectValue("typeImpot.taux", "rejected.typeImpot.taux", "La saisie du taux est obligatoire.");

        }

    }

}
