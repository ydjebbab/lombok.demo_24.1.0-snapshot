/*
 * Copyright (c) 2014 DGFiP - Tous droits réservés
 */

package fr.gouv.finances.cp.dmo.entite;

import java.math.BigInteger;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;
import fr.gouv.finances.lombok.structure.bean.StructureRegleGestionException;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Entité Rib. Classe RIB aux normes de la banque de france
 * 
 * @author lcontinsouzas-cp
 * @author CF : migration vers JPA
 */

@Entity
@Table(name = "ZRIB_RIB")
@SequenceGenerator(name = "ZRIB_RIB_ID_GENERATOR", sequenceName = "SEQ_ZRIB_RIB", allocationSize = 1)
public class Rib extends BaseEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ZRIB_RIB_ID_GENERATOR")
    private long id;

    @Column(nullable = false, name = "CODEBANQUE")
    private String codeBanque = null;

    @Column(nullable = false, name = "CODEGUICHET")
    private String codeGuichet = null;

    @Column(nullable = false, name = "NUMEROCOMPTE")
    private String numeroCompte = null;

    @Column(name = "CLERIB")
    private String cleRib = null;

    @Column(name = "ESTAUTOMATISE")
    private boolean estAutomatise;

    private static final String CODEBANQUE_REGEXP = "\\d{5}"; // $NON-NLS-1$

    private static final String CODEGUICHET_REGEXP = "\\d{5}"; // $NON-NLS-1$

    private static final String NUMEROCOMPTE_REGEXP = "\\w{11}"; // $NON-NLS-1$

    private static final String CLERIB_REGEXP = "\\d{2}"; // $NON-NLS-1$

    private final static long serialVersionUID = 1L;

    public Rib()
    {
        super();
    }

    public String getCleRib()
    {
        return cleRib;
    }

    public String getCodeBanque()
    {
        return codeBanque;
    }

    public String getCodeGuichet()
    {
        return codeGuichet;
    }

    public String getNumeroCompte()
    {
        return numeroCompte;
    }

    public void setCleRib(String clerib)
    {
        this.cleRib = clerib;
    }

    public void setCodeBanque(String codebanque)
    {
        this.codeBanque = codebanque;
    }

    public void setCodeGuichet(String codeguichet)
    {
        this.codeGuichet = codeguichet;
    }

    public void setNumeroCompte(String numerocompte)
    {
        this.numeroCompte = numerocompte;
    }

    /**
     * Verifie forme codebanque
     * 
     * @throws StructureRegleGestionException the structure regle gestion exception
     */
    public void verifierFormeCodebanque() throws StructureRegleGestionException
    {
        if ((this.getCodeBanque() == null)
            || !this.getCodeBanque().matches(CODEBANQUE_REGEXP))
        {
            throw new StructureRegleGestionException(Messages
                .getString("exception.formecodebanque")); // $NON-NLS-1$
        }
    }

    /**
     * Verifie forme codeguichet
     * 
     * @throws StructureRegleGestionException the structure regle gestion exception
     */
    public void verifierFormeCodeguichet()
        throws StructureRegleGestionException
    {
        if ((this.getCodeGuichet() == null)
            || !this.getCodeGuichet().matches(CODEGUICHET_REGEXP))
        {
            throw new StructureRegleGestionException(Messages
                .getString("exception.formecodeguichet")); // $NON-NLS-1$
        }
    }

    /**
     * Verifie forme numerocompte
     * 
     * @throws StructureRegleGestionException the structure regle gestion exception
     */
    public void verifierFormeNumerocompte()
        throws StructureRegleGestionException
    {
        if ((this.getNumeroCompte() == null)
            || !this.getNumeroCompte().matches(NUMEROCOMPTE_REGEXP))
        {
            throw new StructureRegleGestionException(Messages
                .getString("exception.formenumerocompte")); // $NON-NLS-1$
        }
    }

    /**
     * Verifie forme clerib
     * 
     * @throws StructureRegleGestionException the structure regle gestion exception
     */
    public void verifierFormeClerib() throws StructureRegleGestionException
    {
        if ((this.getCleRib() == null)
            || !this.getCleRib().matches(CLERIB_REGEXP))
        {
            throw new StructureRegleGestionException(Messages
                .getString("exception.formeclerib")); // $NON-NLS-1$
        }
    }

    /**
     * permet le remplacement de caractères par d'autres dans une chaîne.
     * 
     * @param str String
     * @param from char[
     * @param tabo String[]
     * @return string
     */
    public static String strtr(String str, char[] from, String[] tabo)
    {
        StringBuffer sbuf = new StringBuffer(str);
        int intn;

        int intm;

        for (intn = 0; intn < sbuf.length(); intn++)
        {
            for (intm = 0; intm < from.length; intm++)
            {
                if (from[intm] == sbuf.charAt(intn))
                {
                    sbuf.replace(intn, intn + 1, tabo[intm]);
                    intn = intn + (tabo[intm].length() - 1);
                    break;
                }
            }
        }
        return sbuf.toString();

    }

    /**
     * Permet de générer une clé rib pour vérification de celle saisie ou transmise Renvoie une exception si les champs
     * de base du RIB sont malformés.
     * 
     * @return Cle sur 2 caractères
     * @throws StructureRegleGestionException the structure regle gestion exception
     */
    public String calculerCleRib() throws StructureRegleGestionException
    {
        String clerib = null;
        StringBuffer rib = new StringBuffer();

        this.verifierFormeCodebanque();
        this.verifierFormeCodeguichet();
        this.verifierFormeNumerocompte();

        rib.append(getCodeBanque()).append(getCodeGuichet()).append(
            getNumeroCompte());

        char[] alpha = new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
                'x', 'y', 'z'};
        String[] numeri = new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "1", "2", "3",
                "4", "5", "6", "7", "8", "9", "2", "3", "4", "5", "6",
                "7", "8", "9"};

        String ribstr = rib.toString();
        String ribnumerique = strtr(ribstr.toLowerCase(Locale.FRANCE),
            alpha, numeri);
        int intB = Integer.parseInt(ribnumerique.substring(0, 5));
        int intG = Integer.parseInt(ribnumerique.substring(5, 10));
        int intD = Integer.parseInt(ribnumerique.substring(10, 16));
        int intC = Integer.parseInt(ribnumerique.substring(16, 21));
        int cleribint =
            Integer.parseInt("97")
                - (Integer.parseInt("89") * intB + Integer.parseInt("15") * intG + Integer.parseInt("76") * intD + Integer.parseInt("3")
                    * intC) % Integer.parseInt("97");

        clerib = String.valueOf(cleribint);
        if (cleribint < Integer.parseInt("10"))
        {
            clerib = "0" + clerib;
        }
        return clerib;
    }

    /**
     * Permet de verifier que l'attribut cleRIB est bien correspond à la clé calculee a partie des champs codebanque,
     * codeguicher et numerodecompte Si ce n'est pas le cas une BancaireRegleGestionException est levee.
     * 
     * @throws StructureRegleGestionException the structure regle gestion exception
     */
    public void verifierCleRib() throws StructureRegleGestionException
    {
        String calc = this.calculerCleRib();
        if (!calc.equalsIgnoreCase(this.getCleRib()))
        {
            throw new StructureRegleGestionException(Messages
                .getString("exception.clerib")); // $NON-NLS-1$
        }
    }

    /**
     * Permet de générer un iban pour les comptes bancaires français uniquement Renvoie une exception si les champs de
     * base du RIB sont malformés.
     * 
     * @return IBAN
     */
    public String calulerIBAN()
    {

        String iBAN = null;

        String cleIBAN = null;

        this.verifierCleRib();

        String artificialIBAN = this.getCodeBanque()
            + this.getCodeGuichet() + this.getNumeroCompte()
            + this.getCleRib() + "FR00";

        char[] alpha = new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
                'x', 'y', 'z'};
        String[] numeri = new String[] {"10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
                "20", "21", "22", "23", "24", "25", "26", "27", "28",
                "29", "30", "31", "32", "33", "34", "35"};

        String ibannumerique = strtr(artificialIBAN
            .toLowerCase(Locale.FRANCE), alpha, numeri);

        BigInteger ibanint = new BigInteger(ibannumerique);
        BigInteger ibanmod97 = ibanint.mod(new BigInteger("97"));

        // Iban mod 97
        int cleibanint = Integer.parseInt("98") - ibanmod97.intValue();
        // log.debug("Cle IBAN : " + cleibanint);

        cleIBAN = String.valueOf(cleibanint);
        if (cleibanint < Integer.parseInt("10"))
        {
            cleIBAN = "0" + cleIBAN;
        }

        iBAN = "FR" + cleIBAN + this.getCodeBanque()
            + this.getCodeGuichet() + this.getNumeroCompte()
            + this.getCleRib();

        return iBAN;
    }

    /**
     * Verifie si est automatise.
     * 
     * @return true, si c'est est automatise
     */
    public boolean isEstAutomatise()
    {
        return estAutomatise;
    }

    public void setEstAutomatise(boolean estAutomatise)
    {
        this.estAutomatise = estAutomatise;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return codeBanque + codeGuichet + numeroCompte + cleRib;
    }

    // La clé métier est constituée par l'attribut reference
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }

        if ((object == null) || (object.getClass() != this.getClass()))
        {
            return false;
        }

        Rib myObj = (Rib) object;

        return ((codeBanque == null ? myObj.codeBanque == null : codeBanque
            .equals(myObj.codeBanque))
            && (codeGuichet == null ? myObj.codeGuichet == null
                : codeGuichet.equals(myObj.codeGuichet))
            && (numeroCompte == null ? myObj.numeroCompte == null
                : numeroCompte.equals(myObj.numeroCompte)));

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int hash = hashCODEHASHINIT;

        int varCode = 0;

        varCode = (null == codeBanque ? 0 : codeBanque.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        varCode = (null == codeGuichet ? 0 : codeGuichet.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        varCode = (null == numeroCompte ? 0 : numeroCompte.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        return hash;

    }
}
