/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Contrôleur FileUploadController
 */
@Controller
@RequestMapping("/controller")
public class FileUploadController implements HandlerExceptionResolver
{
    private static Logger log = LoggerFactory.getLogger(FileUploadController.class);

    FichierJoint ufile;

    List<FichierJoint> files = new ArrayList<>();

    public FileUploadController()
    {
        ufile = new FichierJoint();
    }

    /**
     * methode Get
     *
     * @param response
     * @param value
     */
    @RequestMapping(value = "/get/{value}", method = RequestMethod.GET)
    public void get(HttpServletResponse response, @PathVariable String value)
    {
        try
        {
            response.setContentType(ufile.getTypeMimeFichier());
            response.setContentLength(org.springframework.util.NumberUtils.convertNumberToTargetClass(ufile.getTailleFichier(),
                Integer.class));
            FileCopyUtils.copy(ufile.getLeContenuDuFichier().getData(), response.getOutputStream());

        }
        catch (IOException exc)
        {
            log.error("Erreur : ", exc.getStackTrace(), exc);
        }
    }

    /**
     * methode Upload
     *
     * @param request
     * @param response
     * @return fichier joint
     */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public FichierJoint upload(MultipartHttpServletRequest request, HttpServletResponse response)
    {
        request.getSession().removeAttribute("MaxUploadSizeExceededException");
        Iterator<String> itr = request.getFileNames();
        MultipartFile mpf = request.getFile(itr.next());
        try
        {
            ufile.setTailleFichier(mpf.getBytes().length);
            ufile.getLeContenuDuFichier().setData(mpf.getBytes());
            ufile.setTypeMimeFichier(mpf.getContentType());
            ufile.setNomFichierOriginal(mpf.getOriginalFilename());
            request.getSession().setAttribute("fileupload", ufile);
        }
        catch (IOException ioExc)
        {
            log.error(ioExc.getMessage(), ioExc);
        }
        return ufile;
    }

    // @Override
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.web.servlet.HandlerExceptionResolver#resolveException(javax.servlet.http.HttpServletRequest,
     *      javax.servlet.http.HttpServletResponse, java.lang.Object, java.lang.Exception)
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
    {
        if (ex instanceof MaxUploadSizeExceededException)
        {
            // version jvascript compatible ie , ff chrome
            request
                .getSession()
                .putValue("MaxUploadSizeExceededException",
                    "<p>Le transfert du fichier a été interrompu.</p>    <p>Les fichiers de taille supérieure à 10 Mo ne peuvent pas être transférés.</p>");
            // version jquery
            return new ModelAndView("404");
        }
        log.error("Erreur : ", ex);
        return new ModelAndView("404");
    }

}
