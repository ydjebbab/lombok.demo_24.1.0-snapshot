/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementEditionsCasinosParDepartementStub.java
 *
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import java.util.Collection;
import java.util.Vector;

import fr.gouv.finances.cp.dmo.editionbean.Casino;
import fr.gouv.finances.cp.dmo.editionbean.Departement;

/**
 * Class TraitementEditionsCasinosParDepartementStub DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementEditionsCasinosParDepartementStub
{
    /**
     * Constructeur
     */
    public TraitementEditionsCasinosParDepartementStub()
    {
        super();
    }

    /**
     * methode Creates the bean collection : DGFiP.
     * 
     * @return collection< departement>
     */
    public static Collection<Departement> createBeanCollection()
    {

        Casino casino1 = new Casino();
        Casino casino2 = new Casino();
        Casino casino4 = new Casino();

        casino1.setLibelle("montantdu");
        casino1.setModedeprelevement("prelevementfixe");
        casino1.setMontant(Double.valueOf(30000.05));
        casino1.setName("Cancale");
        casino2.setLibelle("montantpercu");
        casino2.setModedeprelevement("prelevementfixe");
        casino2.setMontant(Double.valueOf(748510000.05));
        casino2.setName("Cancale");

        casino4.setLibelle("montantdu2");
        casino4.setModedeprelevement("prelevementprogressif");
        casino4.setMontant(Double.valueOf(30001.05));
        casino4.setName("Vannes");

        Casino casino5 = new Casino();
        casino5.setLibelle("montantdu");
        casino5.setModedeprelevement("prelevementfixe");
        casino5.setMontant(Double.valueOf(30000.05));
        casino5.setName("Saint-Quay-POrtrieux");
        // Casino casino6 = new Casino("Saint-Quay-POrtrieux" , new
        // Double(4158.05) ,"montantpercu","prelevementfixe" ) ;
        Casino casino6 = new Casino();
        casino6.setLibelle("montantpercu");
        casino6.setModedeprelevement("prelevementfixe");
        casino6.setMontant(Double.valueOf(4158.05));
        casino6.setName("Saint-Quay-POrtrieux");
        Casino casino7 = new Casino();
        casino7.setLibelle("montantdu2");
        casino7.setModedeprelevement("prelevementprogressif");
        casino7.setMontant(Double.valueOf(30001.05));
        casino7.setName("Saint-Quay-POrtrieux");
        Casino casino8 = new Casino();
        casino8.setLibelle("montantdu2");
        casino8.setModedeprelevement("prelevementprogressif");
        casino8.setMontant(Double.valueOf(30001.05));
        casino8.setName("Saint-Quay-POrtrieux");
        Casino casino9 = new Casino();
        casino9.setLibelle("montantdu");
        casino9.setModedeprelevement("prelevementfixe");
        casino9.setMontant(Double.valueOf(160000.05));
        casino9.setName("Perros-Guirec");
        Casino casino10 = new Casino();
        casino10.setLibelle("montantpercu");
        casino10.setModedeprelevement("prelevementfixe");
        casino10.setMontant(Double.valueOf(160000.05));
        casino10.setName("Perros-Guirec");

        Casino casino11 = new Casino("StMalo", new Double(150000), "montant du", "prelevementfixe");

        Vector<Departement> collection = new java.util.Vector<Departement>();
        Departement dep1 = new Departement(Double.valueOf(35));

        dep1.getListCasino().add(casino1);
        dep1.getListCasino().add(casino2);
        dep1.getListCasino().add(casino11);

        Departement dep2 = new Departement(Double.valueOf(56));
        dep2.getListCasino().add(casino4);
        Departement dep3 = new Departement(Double.valueOf(22));
        dep3.getListCasino().add(casino5);
        dep3.getListCasino().add(casino6);
        dep3.getListCasino().add(casino7);
        dep3.getListCasino().add(casino8);
        dep3.getListCasino().add(casino10);
        dep3.getListCasino().add(casino9);

        collection.add(dep1);
        collection.add(dep2);
        collection.add(dep3);
        return collection;
    }

}
