/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - DI 14
 *
 * Projet NOM_PROJET - cas
 *
 * fichier : TypeCodeNatureMois.java
 *
 */
package fr.gouv.finances.cp.dmo.editionbean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

/**
 * Enum TypeCodeNatureMois
 * <p>
 * Type Nature du Mois de Prélévement
 * </p>
 * .
 * 
 * @author DI 14
 * @version $Revision: 1.4 $ Date: 16 nov. 2009
 */
public enum TypeCodeNatureMois
{

    /** Dernier mois de la saison. */
    DERNIER_MOIS_SAISON("D", "Dernier mois de la saison"),

    /** Mois de la saison courante. */
    MOIS_SAISON_COURANTE("S", "Mois de la saison courante"),

    /** Mise en oeuvre de la clause de sauvegarde. */
    CLAUSE_SAUVEGARDE("X", "Mise en oeuvre de la clause de sauvegarde");

    /** code. */
    private String code;

    /** libelle. */
    private String libelle;

    /**
     * Instanciation de TypeCodeNatureMois.
     * 
     * @param code Code représentant l'énumération
     * @param libelle Libellé associé à l'énumération
     */
    TypeCodeNatureMois(String code, String libelle)
    {
        this.code = code;
        this.libelle = libelle;
    }

    /**
     * Accesseur de libelle.
     * 
     * @return libelle
     */
    public String getLibelle()
    {
        return libelle;
    }

    /**
     * Accesseur de l attribut name.
     * 
     * @return name
     */
    public String getName()
    {
        return this.name();
    }

    /**
     * Retourne le code représentant l'énumération.
     * 
     * @return string
     */
    public String getCode()
    {
        return code;
    }

    /**
     * Construit un objet TypeCodeNatureMois à partir de son code.
     * 
     * @param code Code représentant l'énumération
     * @return TypeCodeNatureMois
     */
    public static TypeCodeNatureMois fromCode(String code)
    {
        TypeCodeNatureMois result = null;

        TypeCodeNatureMois[] lesTypeCodeNatureMois = TypeCodeNatureMois.values();
        for (int i = 0; i < lesTypeCodeNatureMois.length; i++)

        {
            if (lesTypeCodeNatureMois[i] != null && lesTypeCodeNatureMois[i].code.equals(code))
            {
                result = lesTypeCodeNatureMois[i];
                break;
            }
        }
        return result;

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Enum#toString()
     */
    public String toString()
    {
        return this.libelle;
    }

    /**
     * <p>
     * Retourne toutes les valeurs de l'énumération dans une map qui a
     * <p>
     * <ul>
     * <li>pour clé un élément de l'énumération</li>
     * <li>comme valeur une chaîne de caractères qui représente cette énumération.</li>
     * </ul>
     * 
     * @return Map contenant l'ensemble des valeurs de l'énumération
     */
    public static Map<TypeCodeNatureMois, String> getMapTypeCodeNatureMois()
    {

        EnumMap<TypeCodeNatureMois, String> map = new EnumMap<TypeCodeNatureMois, String>(TypeCodeNatureMois.class);
        for (TypeCodeNatureMois instanceClasse : TypeCodeNatureMois.values())
        {
            map.put(instanceClasse, instanceClasse.toString());
        }
        return map;
    }

    /**
     * Retourne toutes les valeurs de l'énumération dans une liste.
     * 
     * @return List
     */
    public static List<TypeCodeNatureMois> getListeTypeCodeNatureMois()
    {
        return Arrays.asList(TypeCodeNatureMois.values());
    }

    /**
     * Retourne toutes les valeurs de l'énumération dans une liste, sans CLAUSE_SAUVEGARDE.
     * 
     * @return List
     */
    public static List<TypeCodeNatureMois> getListeTypeCodeNatureMoisSansClauseSauvegarde()
    {
        List<TypeCodeNatureMois> listeTypeCodeNatureMoisSsClauseSauv = new ArrayList<TypeCodeNatureMois>();
        List<TypeCodeNatureMois> listeTypeCodeNatureMois = Arrays.asList(TypeCodeNatureMois.values());

        for (TypeCodeNatureMois unTypeCodeNatureMois : listeTypeCodeNatureMois)
        {
            if (!unTypeCodeNatureMois.equals(TypeCodeNatureMois.CLAUSE_SAUVEGARDE))
            {
                listeTypeCodeNatureMoisSsClauseSauv.add(unTypeCodeNatureMois);
            }
        }

        return listeTypeCodeNatureMoisSsClauseSauv;
    }

}
