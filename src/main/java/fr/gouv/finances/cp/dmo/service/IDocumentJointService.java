/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : DocumentJointService.java
 *
 */
package fr.gouv.finances.cp.dmo.service;

import java.util.Collection;

/**
 * Interface IDocumentJointService
 * 
 * @author chouard-cp
 * @author CF
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public interface IDocumentJointService
{

    /**
     * methode Supprimer les documents joints
     * 
     * @param documentsASupprimer
     */
    public void supprimerLesDocumentsJoints(Collection documentsASupprimer);
}
