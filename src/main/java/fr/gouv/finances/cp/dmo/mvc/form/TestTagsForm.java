/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TestTagsForm.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import fr.gouv.finances.cp.dmo.entite.TypeImpot;

/**
 * Class TestTagsForm serialisable.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
public class TestTagsForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** multiple list from array list. */
    private List multipleListFromArrayList ;

    /** multiple list from hash set. */
    private  Set<TypeImpot> multipleListFromHashSet ;

    /** multiple list from linked hash set. */
    private List multipleListFromLinkedHashSet  ;

    /** multiple list from hash map. */
    private List multipleListFromHashMap  ;

    /** multiple list from linked hash map. */
    private List multipleListFromLinkedHashMap  ;

    /** input field. */
    private String inputField;

    /** textarea field. */
    private String textareaField;

    /** radio field from array list. */
    private String radioFieldFromArrayList;

    /** radio field from hash set. */
    private String radioFieldFromHashSet;

    /** radio field from linked hash set. */
    private String radioFieldFromLinkedHashSet;

    /** radio field from hash map. */
    private String radioFieldFromHashMap;

    /** radio field from linked hash map. */
    private String radioFieldFromLinkedHashMap;

    /** checkbox list from array list. */
    private List checkboxListFromArrayList = new ArrayList();

    /** checkbox list from hash set. */
    private List checkboxListFromHashSet = new ArrayList();

    /** checkbox list from linked hash set. */
    private List checkboxListFromLinkedHashSet = new ArrayList();

    /** checkbox list from hash map. */
    private List checkboxListFromHashMap = new ArrayList();

    /** checkbox list from linked hash map. */
    private List checkboxListFromLinkedHashMap = new ArrayList();

    /** select field from array list. */
    private String selectFieldFromArrayList;

    /** select field from hash set. */
    private String selectFieldFromHashSet;

    /** select field from linked hash set. */
    private String selectFieldFromLinkedHashSet;

    /** select field from hash map. */
    private String selectFieldFromHashMap;

    /** select field from linked hash map. */
    private String selectFieldFromLinkedHashMap;

    /** date heure field. */
    private Date dateHeureField;

    /** date field. */
    private Date dateField;
    
    /** set selected. */
    private List setSelected;

    /**
     * Accesseur de l attribut sets the selected.
     *
     * @return sets the selected
     */
    public List getSetSelected()
    {
        return setSelected;
    }

    /**
     * Modificateur de l attribut sets the selected.
     *
     * @param setSelected le nouveau sets the selected
     */
    public void setSetSelected(List setSelected)
    {
        this.setSelected = setSelected;
    }

    /**
     * Instanciation de test tags form.
     */
    public TestTagsForm()
    {
        super();
    }

    /**
     * Gets the checkbox list from array list.
     *
     * @return the checkbox list from array list
     */
    public List getCheckboxListFromArrayList()
    {
        return checkboxListFromArrayList;
    }

    /**
     * Gets the checkbox list from hash map.
     *
     * @return the checkbox list from hash map
     */
    public List getCheckboxListFromHashMap()
    {
        return checkboxListFromHashMap;
    }

    /**
     * Gets the checkbox list from hash set.
     *
     * @return the checkbox list from hash set
     */
    public List getCheckboxListFromHashSet()
    {
        return checkboxListFromHashSet;
    }

    /**
     * Gets the checkbox list from linked hash map.
     *
     * @return the checkbox list from linked hash map
     */
    public List getCheckboxListFromLinkedHashMap()
    {
        return checkboxListFromLinkedHashMap;
    }

    /**
     * Gets the checkbox list from linked hash set.
     *
     * @return the checkbox list from linked hash set
     */
    public List getCheckboxListFromLinkedHashSet()
    {
        return checkboxListFromLinkedHashSet;
    }

    /**
     * Gets the date field.
     *
     * @return the date field
     */
    public Date getDateField()
    {
        return dateField;
    }

    /**
     * Gets the date heure field.
     *
     * @return the date heure field
     */
    public Date getDateHeureField()
    {
        return dateHeureField;
    }

    /**
     * Gets the input field.
     *
     * @return the input field
     */
    public String getInputField()
    {
        return inputField;
    }

    /**
     * Gets the multiple list from array list.
     *
     * @return the multiple list from array list
     */
    public List getMultipleListFromArrayList()
    {
        return multipleListFromArrayList;
    }

    /**
     * Gets the multiple list from hash map.
     *
     * @return the multiple list from hash map
     */
    public List getMultipleListFromHashMap()
    {
        return multipleListFromHashMap;
    }

    /**
     * Gets the multiple list from hash set.
     *
     * @return the multiple list from hash set
     */
    public  Set getMultipleListFromHashSet()
    {
        return multipleListFromHashSet;
    }

    /**
     * Gets the multiple list from linked hash map.
     *
     * @return the multiple list from linked hash map
     */
    public List getMultipleListFromLinkedHashMap()
    {
        return multipleListFromLinkedHashMap;
    }

    /**
     * Gets the multiple list from linked hash set.
     *
     * @return the multiple list from linked hash set
     */
    public List getMultipleListFromLinkedHashSet()
    {
        return multipleListFromLinkedHashSet;
    }

    /**
     * Gets the radio field from array list.
     *
     * @return the radio field from array list
     */
    public String getRadioFieldFromArrayList()
    {
        return radioFieldFromArrayList;
    }

    /**
     * Gets the radio field from hash map.
     *
     * @return the radio field from hash map
     */
    public String getRadioFieldFromHashMap()
    {
        return radioFieldFromHashMap;
    }

    /**
     * Gets the radio field from hash set.
     *
     * @return the radio field from hash set
     */
    public String getRadioFieldFromHashSet()
    {
        return radioFieldFromHashSet;
    }

    /**
     * Gets the radio field from linked hash map.
     *
     * @return the radio field from linked hash map
     */
    public String getRadioFieldFromLinkedHashMap()
    {
        return radioFieldFromLinkedHashMap;
    }

    /**
     * Gets the radio field from linked hash set.
     *
     * @return the radio field from linked hash set
     */
    public String getRadioFieldFromLinkedHashSet()
    {
        return radioFieldFromLinkedHashSet;
    }

    /**
     * Gets the select field from array list.
     *
     * @return the select field from array list
     */
    public String getSelectFieldFromArrayList()
    {
        return selectFieldFromArrayList;
    }

    /**
     * Gets the select field from hash map.
     *
     * @return the select field from hash map
     */
    public String getSelectFieldFromHashMap()
    {
        return selectFieldFromHashMap;
    }

    /**
     * Gets the select field from hash set.
     *
     * @return the select field from hash set
     */
    public String getSelectFieldFromHashSet()
    {
        return selectFieldFromHashSet;
    }

    /**
     * Gets the select field from linked hash map.
     *
     * @return the select field from linked hash map
     */
    public String getSelectFieldFromLinkedHashMap()
    {
        return selectFieldFromLinkedHashMap;
    }

    /**
     * Gets the select field from linked hash set.
     *
     * @return the select field from linked hash set
     */
    public String getSelectFieldFromLinkedHashSet()
    {
        return selectFieldFromLinkedHashSet;
    }

    /**
     * Gets the textarea field.
     *
     * @return the textarea field
     */
    public String getTextareaField()
    {
        return textareaField;
    }

    /**
     * Sets the checkbox list from array list.
     *
     * @param checkboxListFromArrayList the new checkbox list from array list
     */
    public void setCheckboxListFromArrayList(List checkboxListFromArrayList)
    {
        this.checkboxListFromArrayList = checkboxListFromArrayList;
    }

    /**
     * Sets the checkbox list from hash map.
     *
     * @param checkboxListFromHashMap the new checkbox list from hash map
     */
    public void setCheckboxListFromHashMap(List checkboxListFromHashMap)
    {
        this.checkboxListFromHashMap = checkboxListFromHashMap;
    }

    /**
     * Sets the checkbox list from hash set.
     *
     * @param checkboxListFromHashSet the new checkbox list from hash set
     */
    public void setCheckboxListFromHashSet(List checkboxListFromHashSet)
    {
        this.checkboxListFromHashSet = checkboxListFromHashSet;
    }

    /**
     * Sets the checkbox list from linked hash map.
     *
     * @param checkboxListFromLinkedHashMap the new checkbox list from linked hash map
     */
    public void setCheckboxListFromLinkedHashMap(List checkboxListFromLinkedHashMap)
    {
        this.checkboxListFromLinkedHashMap = checkboxListFromLinkedHashMap;
    }

    /**
     * Sets the checkbox list from linked hash set.
     *
     * @param checkboxListFromLinkedHashSet the new checkbox list from linked hash set
     */
    public void setCheckboxListFromLinkedHashSet(List checkboxListFromLinkedHashSet)
    {
        this.checkboxListFromLinkedHashSet = checkboxListFromLinkedHashSet;
    }

    /**
     * Sets the date field.
     *
     * @param dateField the new date field
     */
    public void setDateField(Date dateField)
    {
        this.dateField = dateField;
    }

    /**
     * Sets the date heure field.
     *
     * @param dateHeureField the new date heure field
     */
    public void setDateHeureField(Date dateHeureField)
    {
        this.dateHeureField = dateHeureField;
    }

    /**
     * Sets the input field.
     *
     * @param inputField the new input field
     */
    public void setInputField(String inputField)
    {
        this.inputField = inputField;
    }

    /**
     * Sets the multiple list from array list.
     *
     * @param multipleListFromArrayList the new multiple list from array list
     */
    public void setMultipleListFromArrayList(List multipleListFromArrayList)
    {
        this.multipleListFromArrayList = multipleListFromArrayList;
    }

    /**
     * Sets the multiple list from hash map.
     *
     * @param multipleListFromHashMap the new multiple list from hash map
     */
    public void setMultipleListFromHashMap(List multipleListFromHashMap)
    {
        this.multipleListFromHashMap = multipleListFromHashMap;
    }

    /**
     * Sets the multiple list from hash set.
     *
     * @param multipleListFromHashSet the new multiple list from hash set
     */
    public void setMultipleListFromHashSet ( Set<TypeImpot> multipleListFromHashSet)
    {
        this.multipleListFromHashSet = multipleListFromHashSet;
    }

    /**
     * Sets the multiple list from linked hash map.
     *
     * @param multipleListFromLinkedHashMap the new multiple list from linked hash map
     */
    public void setMultipleListFromLinkedHashMap(List multipleListFromLinkedHashMap)
    {
        this.multipleListFromLinkedHashMap = multipleListFromLinkedHashMap;
    }

    /**
     * Sets the multiple list from linked hash set.
     *
     * @param multipleListFromLinkedHashSet the new multiple list from linked hash set
     */
    public void setMultipleListFromLinkedHashSet(List multipleListFromLinkedHashSet)
    {
        this.multipleListFromLinkedHashSet = multipleListFromLinkedHashSet;
    }

    /**
     * Sets the radio field from array list.
     *
     * @param radioFieldFromArrayList the new radio field from array list
     */
    public void setRadioFieldFromArrayList(String radioFieldFromArrayList)
    {
        this.radioFieldFromArrayList = radioFieldFromArrayList;
    }

    /**
     * Sets the radio field from hash map.
     *
     * @param radioFieldFromHashMap the new radio field from hash map
     */
    public void setRadioFieldFromHashMap(String radioFieldFromHashMap)
    {
        this.radioFieldFromHashMap = radioFieldFromHashMap;
    }

    /**
     * Sets the radio field from hash set.
     *
     * @param radioFieldFromHashSet the new radio field from hash set
     */
    public void setRadioFieldFromHashSet(String radioFieldFromHashSet)
    {
        this.radioFieldFromHashSet = radioFieldFromHashSet;
    }

    /**
     * Sets the radio field from linked hash map.
     *
     * @param radioFieldFromLinkedHashMap the new radio field from linked hash map
     */
    public void setRadioFieldFromLinkedHashMap(String radioFieldFromLinkedHashMap)
    {
        this.radioFieldFromLinkedHashMap = radioFieldFromLinkedHashMap;
    }

    /**
     * Sets the radio field from linked hash set.
     *
     * @param radioFieldFromLinkedHashSet the new radio field from linked hash set
     */
    public void setRadioFieldFromLinkedHashSet(String radioFieldFromLinkedHashSet)
    {
        this.radioFieldFromLinkedHashSet = radioFieldFromLinkedHashSet;
    }

    /**
     * Sets the select field from array list.
     *
     * @param selectFieldFromArrayList the new select field from array list
     */
    public void setSelectFieldFromArrayList(String selectFieldFromArrayList)
    {
        this.selectFieldFromArrayList = selectFieldFromArrayList;
    }

    /**
     * Sets the select field from hash map.
     *
     * @param selectFieldFromHashMap the new select field from hash map
     */
    public void setSelectFieldFromHashMap(String selectFieldFromHashMap)
    {
        this.selectFieldFromHashMap = selectFieldFromHashMap;
    }

    /**
     * Sets the select field from hash set.
     *
     * @param selectFieldFromHashSet the new select field from hash set
     */
    public void setSelectFieldFromHashSet(String selectFieldFromHashSet)
    {
        this.selectFieldFromHashSet = selectFieldFromHashSet;
    }

    /**
     * Sets the select field from linked hash map.
     *
     * @param selectFieldFromLinkedHashMap the new select field from linked hash map
     */
    public void setSelectFieldFromLinkedHashMap(String selectFieldFromLinkedHashMap)
    {
        this.selectFieldFromLinkedHashMap = selectFieldFromLinkedHashMap;
    }

    /**
     * Sets the select field from linked hash set.
     *
     * @param selectFieldFromLinkedHashSet the new select field from linked hash set
     */
    public void setSelectFieldFromLinkedHashSet(String selectFieldFromLinkedHashSet)
    {
        this.selectFieldFromLinkedHashSet = selectFieldFromLinkedHashSet;
    }

    /**
     * Sets the textarea field.
     *
     * @param textareaField the new textarea field
     */
    public void setTextareaField(String textareaField)
    {
        this.textareaField = textareaField;
    }

}
