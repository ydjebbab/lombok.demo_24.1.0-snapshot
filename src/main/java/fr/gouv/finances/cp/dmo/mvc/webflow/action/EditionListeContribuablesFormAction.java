/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.atlas.service.DocumentAtlasService;
import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Action EditionListeContribuablesFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class EditionListeContribuablesFormAction extends FormAction
{
    private static final Logger log = LoggerFactory.getLogger(EditionListeContribuablesFormAction.class);

    private String editionPostProcUrl;

    private EditionDemandeService editiondemandeserviceso;

    private DocumentAtlasService documentatlasserviceso;

    public EditionListeContribuablesFormAction()
    {
        super();
    }

    public EditionListeContribuablesFormAction(Class arg0)
    {
        super(arg0);
    }

    public EditionDemandeService getEditiondemandeserviceso()
    {
        return editiondemandeserviceso;
    }

    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso)
    {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    public DocumentAtlasService getDocumentatlasserviceso()
    {
        return documentatlasserviceso;
    }

    public void setDocumentatlasserviceso(DocumentAtlasService documentatlasserviceso)
    {
        this.documentatlasserviceso = documentatlasserviceso;
    }

    public String getEditionPostProcUrl()
    {
        return editionPostProcUrl;
    }

    public void setEditionPostProcUrl(String editionPostProcUrl)
    {
        this.editionPostProcUrl = editionPostProcUrl;
    }

    /**
     * methode Initialiser formulaire
     * 
     * @param request param
     * @return event
     */
    public Event initialiserFormulaire(RequestContext request)
    {

        return success();
    }

    /**
     * methode Produire edition adresses contribuables
     * 
     * @param rc param
     * @return event
     */
    public Event produireEditionAdressesContribuables(RequestContext rc)
    {
        Map parametresEdition = new HashMap();

        FichierJoint fichier = documentatlasserviceso.recupererDocumentImageSurAtlas(editionPostProcUrl);
        byte[] monImagebyte = fichier.getLeContenuDuFichier().getData();
        File tempFile = null;
        try
        {
            tempFile = File.createTempFile("tempfilename", ".jpg");
            // Put the database media field content in this temporary file
            FileUtils.writeByteArrayToFile(tempFile, monImagebyte);
        }
        catch (IOException exc)
        {
            log.error(exc.getMessage(), exc);

        }

        parametresEdition.put("logo", tempFile);

        // Récupération des caractéristiques de la personne connectée
        // dans le contexte sécurisé pour retrouver l'uid

        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        EditionAsynchroneNotifieeParMail editionAsynchroneNotifieeParMail =
            editiondemandeserviceso.initEditionAsynchroneNotifieeParMail("zf2.adressescontribuables.edition", personne
                .getUid(), personne.getMail());
        editionAsynchroneNotifieeParMail =
            editiondemandeserviceso.declencherEdition(editionAsynchroneNotifieeParMail, parametresEdition);

        rc.getFlowScope().put("jobHistory", editionAsynchroneNotifieeParMail);

        return success();
    }

    /**
     * methode Produire edition liste codes postaux jasper csv
     * 
     * @param rc param
     * @return event
     */
    public Event produireEditionListeCodesPostauxJasperCsv(RequestContext rc)
    {
        // Récupération des caractéristiques de la personne connectée
        // dans le contexte sécurisé pour retrouver l'uid

        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        EditionAsynchroneNotifieeParMail editionAsynchroneNotifieeParMail =
            editiondemandeserviceso.initEditionAsynchroneNotifieeParMail("zf2.exportcodespostaux.edition", personne
                .getUid(), personne.getMail());

        editionAsynchroneNotifieeParMail =
            editiondemandeserviceso.declencherEdition(editionAsynchroneNotifieeParMail, new HashMap());

        rc.getFlowScope().put("jobHistory", editionAsynchroneNotifieeParMail);
        return success();
    }

    /**
     * methode Produire edition liste contribuables csv
     * 
     * @param requestContext param
     * @return event
     */
    public Event produireEditionListeContribuablesCsv(RequestContext requestContext)
    {
        // Récupération des caractéristiques de la personne connectée
        // dans le contexte sécurisé pour retrouver l'uid

        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        EditionAsynchroneNotifieeParMail editionAsynchroneNotifieeParMail =
            editiondemandeserviceso.initEditionAsynchroneNotifieeParMail("zf2.listecontribuablescsv.edition", personne
                .getUid(), personne.getMail());
        editiondemandeserviceso
            .initMessageMailPourAccesViaPortailIntranetOuInternet((JobHistory) editionAsynchroneNotifieeParMail);

        // Production d'une édition asynchrone compressée
        editionAsynchroneNotifieeParMail =
            editiondemandeserviceso.declencherEditionCompressee(editionAsynchroneNotifieeParMail, new HashMap());

        requestContext.getFlowScope().put("jobHistory", editionAsynchroneNotifieeParMail);

        return success();
    }

    public class FichierObject
    {
        Object value = new Object();

        public FichierObject(FichierJoint value)
        {
        }
    }

}
