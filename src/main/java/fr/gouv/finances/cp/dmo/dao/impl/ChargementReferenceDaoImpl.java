/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 *
 *
 * fichier : ChargementReferenceDaoImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.dao.impl;

import java.io.StringReader;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.io.DocumentResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IChargementReferenceDao;
import fr.gouv.finances.cp.dmo.entite.Civilite;
import fr.gouv.finances.cp.dmo.entite.SituationFami;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.techbean.ListeCivilites;
import fr.gouv.finances.cp.dmo.techbean.ListePays;
import fr.gouv.finances.cp.dmo.techbean.ListeSituationsFami;
import fr.gouv.finances.cp.dmo.techbean.ListeTypesImpot;
import fr.gouv.finances.lombok.adresse.bean.PaysIso;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;

/**
 * DAO ChargementReferenceDaoImpl
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Repository("chargementReferenceDao")
@Transactional(transactionManager="transactionManager")
public class ChargementReferenceDaoImpl extends BaseDaoJpaImpl implements IChargementReferenceDao
{
    // TODO A remonter dans un service commun AdresseContribuableDao

    private static final Logger log = LoggerFactory.getLogger(ChargementReferenceDaoImpl.class);

    public ChargementReferenceDaoImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param document
     * @see fr.gouv.finances.cp.dmo.dao.IChargementReferenceDao#loadCiviliteFromXml(org.dom4j.Document)
     */
    @Override
    public void loadCiviliteFromXml(final Document document)
    {
        log.debug(">>> Debut methode loadCiviliteFromXml");

        if (document == null)
        {
            throw new IllegalArgumentException();
        }
        try
        {
            JAXBContext context = JAXBContext.newInstance(ListeCivilites.class);

            Unmarshaller unmarshaller = context.createUnmarshaller();
            StringReader reader = new StringReader(document.asXML());
            ListeCivilites listeCivilites = (ListeCivilites) unmarshaller.unmarshal(reader);

            List<Civilite> listeCiv = listeCivilites.getListeCivilites();
            log.debug(" listeCiv.size() : " + listeCiv.size());
            listeCiv.forEach(entityManager::merge);

            flush();
            clearSession();
        }
        catch (JAXBException jAXBException)
        {
            LOGGER.error("Erreur JAXB", jAXBException);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param document
     * @see fr.gouv.finances.cp.dmo.dao.IChargementReferenceDao#loadPaysFromXml(org.dom4j.Document)
     */
    @Override
    public void loadPaysFromXml(final Document document)
    {
        log.debug(">>> Debut methode loadPaysFromXml");
        if (document == null)
        {
            throw new IllegalArgumentException();
        }

        try
        {
            JAXBContext context = JAXBContext.newInstance(ListePays.class);

            Unmarshaller unmarshaller = context.createUnmarshaller();
            StringReader reader = new StringReader(document.asXML());
            ListePays listePays = (ListePays) unmarshaller.unmarshal(reader);

            List<PaysIso> listePaysISO = listePays.getListePays();
            log.debug(" listePaysISO.size() : " + listePaysISO.size());
            listePaysISO.forEach(entityManager::merge);

            flush();
            clearSession();
        }
        catch (JAXBException jAXBException)
        {
            LOGGER.error("Erreur JAXB", jAXBException);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param document
     * @see fr.gouv.finances.cp.dmo.dao.IChargementReferenceDao#loadSituationFamiFromXml(org.dom4j.Document)
     */
    @Override
    public void loadSituationFamiFromXml(final Document document)
    {
        log.debug(">>> Debut methode loadSituationFamiFromXml");

        if (document == null)
        {
            throw new IllegalArgumentException();
        }

        try
        {
            JAXBContext context = JAXBContext.newInstance(ListeSituationsFami.class);

            Unmarshaller unmarshaller = context.createUnmarshaller();
            StringReader reader = new StringReader(document.asXML());
            ListeSituationsFami listeSituationsFami = (ListeSituationsFami) unmarshaller.unmarshal(reader);

            List<SituationFami> listeSituationFami = listeSituationsFami.getListeSituationsFami();
            log.debug(" listeSituationFami.size() : " + listeSituationFami.size());
            listeSituationFami.forEach(entityManager::merge);

            flush();
            clearSession();
        }
        catch (JAXBException jAXBException)
        {
            LOGGER.error("Erreur JAXB", jAXBException);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param document
     * @see fr.gouv.finances.cp.dmo.dao.IChargementReferenceDao#loadTypeImpotFromXml(org.dom4j.Document)
     */
    @Override
    public void loadTypeImpotFromXml(final Document document)
    {
        log.debug(">>> Debut methode loadTypeImpotFromXml");
        if (document == null)
        {
            throw new IllegalArgumentException();
        }

        try
        {
            JAXBContext context = JAXBContext.newInstance(ListeTypesImpot.class);

            Unmarshaller unmarshaller = context.createUnmarshaller();
            StringReader reader = new StringReader(document.asXML());
            ListeTypesImpot listeTypesImpot = (ListeTypesImpot) unmarshaller.unmarshal(reader);

            List<TypeImpot> listeTypeImpots = listeTypesImpot.getListeTypesImpot();
            log.debug(" listeTypeImpots.size() : " + listeTypeImpots.size());
            listeTypeImpots.forEach(entityManager::merge);

            flush();
            clearSession();
        }
        catch (JAXBException jAXBException)
        {
            LOGGER.error("Erreur JAXB", jAXBException);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return document
     * @see fr.gouv.finances.cp.dmo.dao.IChargementReferenceDao#unloadPaysToXml()
     */
    @Override
    public Document unloadPaysToXml()
    {
        log.debug(">>> Debut methode unloadPaysToXml");
        try
        {
            Document document = DocumentFactory.getInstance().createDocument();
            JAXBContext context = JAXBContext.newInstance(ListePays.class);
            DocumentResult documentResult = new DocumentResult();
            Marshaller marshaller = context.createMarshaller();

            CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<PaysIso> criteriaQuery = criteriabuilder.createQuery(PaysIso.class);
            Root<PaysIso> paysIsoRoot = criteriaQuery.from(PaysIso.class);

            criteriaQuery.select(paysIsoRoot);

            // Pour debug seulement
            TypedQuery<PaysIso> paysIsoTypedQuery = entityManager.createQuery(criteriaQuery);
            log.debug("paysIsoTypedQuery : " + paysIsoTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

            List<PaysIso> paysIsoListeResultat = paysIsoTypedQuery.getResultList();

            log.debug("paysIsoListeResultat.size() : " + paysIsoListeResultat.size());

            ListePays listePays = new ListePays();
            listePays.setListePays(paysIsoListeResultat);

            marshaller.marshal(listePays, documentResult);
            document = documentResult.getDocument();
            clearSession();
            return document;
        }
        catch (JAXBException e)
        {
            LOGGER.error("Erreur JAXB", e);
            return null;
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return document
     * @see fr.gouv.finances.cp.dmo.dao.IChargementReferenceDao#unloadTypeImpotToXml()
     */
    @Override
    public Document unloadTypeImpotToXml()
    {
        log.debug(">>> Debut methode unloadTypeImpotToXml");        

        try
        {
            Document document = DocumentFactory.getInstance().createDocument();
            JAXBContext context = JAXBContext.newInstance(ListeTypesImpot.class);
            DocumentResult documentResult = new DocumentResult();
            Marshaller marshaller = context.createMarshaller();

            CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<TypeImpot> criteriaQuery = criteriabuilder.createQuery(TypeImpot.class);
            Root<TypeImpot> typeImpotRoot = criteriaQuery.from(TypeImpot.class);

            criteriaQuery.select(typeImpotRoot);

            // Pour debug seulement
            TypedQuery<TypeImpot> typeImpotTypedQuery = entityManager.createQuery(criteriaQuery);
            log.debug("typeImpotTypedQuery : " + typeImpotTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

            List<TypeImpot> typeImpotListeResultat = typeImpotTypedQuery.getResultList();
            log.debug("typeImpotListeResultat.size() : " + typeImpotListeResultat.size());

            ListeTypesImpot listeTypesImpot = new ListeTypesImpot();
            listeTypesImpot.setListeTypesImpot(typeImpotListeResultat);

            marshaller.marshal(listeTypesImpot, documentResult);
            document = documentResult.getDocument();
            clearSession();
            return document;
        }
        catch (JAXBException e)
        {
            LOGGER.error("Erreur JAXB", e);
            return null;
        }
    }
}
