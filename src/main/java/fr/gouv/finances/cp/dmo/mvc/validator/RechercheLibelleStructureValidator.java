/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.dmo.mvc.form.RechercheLibelleStructureForm;
import fr.gouv.finances.lombok.structure.bean.StructureCP;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Class RechercheLibelleStructureValidator
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class RechercheLibelleStructureValidator implements Validator
{

    private static final Logger log = LoggerFactory.getLogger(RechercheLibelleStructureValidator.class);

    public RechercheLibelleStructureValidator()
    {
        super(); 
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(RechercheLibelleStructureForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param command
     * @param errors
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object command, Errors errors)
    {
        // renvoi rien
    }

    /**
     * methode Validate surface saisie codique
     *
     * @param command param
     * @param errors param
     */
    public void validateSurfaceSaisieCodique(RechercheLibelleStructureForm command, Errors errors)
    {
        StructureCP uneStructureCP = command.getUnestructurearechercher();
        try
        {
            uneStructureCP.verifierFormeCodique();
            uneStructureCP.verifierFormeCodeAnnexe();
        }
        catch (RegleGestionException exc)
        {
            log.error(exc.getMessage(), exc);
            errors.reject(null, exc.getMessage());
        }

    }
}
