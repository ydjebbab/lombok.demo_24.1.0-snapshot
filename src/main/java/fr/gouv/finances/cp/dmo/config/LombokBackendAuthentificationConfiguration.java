package fr.gouv.finances.cp.dmo.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.security.web.csrf.MissingCsrfTokenException;
import org.springframework.stereotype.Component;

import fr.gouv.finances.lombok.securite.springsecurity.PortailAuthenticationProcessingFilter;
import fr.gouv.finances.lombok.securite.springsecurity.PortailAuthentificationSecurityLombok;

@Component
public class LombokBackendAuthentificationConfiguration
{
    @Value(value = "${annuaire.typeannuaire}")
    private String typeAnnuaire;

    @Value(value = "${authentification.adressesip}")
    private String adressesIP;

    @Value(value = "${authentification.separateurdnmotdepasse}")
    private String separateurdnmotdepasse;

    @Value(value = "${authentification.motdepasseportail}")
    private String motDePassePortail;
   
    @Autowired
    @Named("authenticationPortailProvider")
    public PortailAuthentificationSecurityLombok authenticationPortailProvider;
    
    @Bean
    AuthenticationSuccessHandler authenticationSuccessBackendHandler()
    {
        SimpleUrlAuthenticationSuccessHandler successHandler = new SimpleUrlAuthenticationSuccessHandler();
        successHandler.setDefaultTargetUrl("/rest/authentificationsucces");
        return successHandler;
    }

    @Bean
    AuthenticationFailureHandler authenticationFailureBackendHandler()
    {
        SimpleUrlAuthenticationFailureHandler failureHandler = new SimpleUrlAuthenticationFailureHandler();
        failureHandler.setDefaultFailureUrl("/rest/authentificationechec");
        return failureHandler;
    }
    
    @Bean
    AccessDeniedHandler accessDeniedBackendHandler()
    {
        return new AccessDeniedHandlerImpl() 
        {
            @Override
            public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
                throws IOException, ServletException 
            {
                if (accessDeniedException instanceof MissingCsrfTokenException
                    || accessDeniedException instanceof InvalidCsrfTokenException) 
                {
                    response.sendRedirect("/rest/accesinterditcsrf");                                        
                }
                else 
                {
                    response.sendRedirect("/rest/accesinterdit");
                }

                super.handle(request, response, accessDeniedException);
            }
        };
    }
        
    @Bean
    public PortailAuthenticationProcessingFilter authenticationBackendProcessingFilterPortail()
    {
        PortailAuthenticationProcessingFilter portailFilter = new PortailAuthenticationProcessingFilter();
        portailFilter.setFilterProcessesUrl("/j_appelportailbackend");
        List<AuthenticationProvider> listeAuthenticationProvider = new ArrayList<>();        
        listeAuthenticationProvider.add(authenticationPortailProvider);
        // ProviderManager implements AuthenticationManager
        ProviderManager providerManager = new ProviderManager(listeAuthenticationProvider);
        portailFilter.setAuthenticationManager(providerManager);
        portailFilter.setAuthenticationSuccessHandler(authenticationSuccessBackendHandler());
        portailFilter.setAuthenticationFailureHandler(authenticationFailureBackendHandler());              
        portailFilter.setMotDePassePortail(motDePassePortail);
        portailFilter.setAdressesIP(adressesIP);
        portailFilter.setSeparateurdnmotdepasse(separateurdnmotdepasse);
        portailFilter.setPostOnly(false);
        portailFilter.setTypeAnnuaire(typeAnnuaire);

        return portailFilter;
    }
}
