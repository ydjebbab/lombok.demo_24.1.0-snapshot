package fr.gouv.finances.cp.dmo.entite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;

/**
 * Entite Centre des impôts (Centre des Finances publiques)
 * 
 * @author celinio fernandes Date: Mar 9, 2020
 */
@Entity
@Table(name = "CENTRE_IMPOTS")
@SequenceGenerator(name = "CENTRE_IMPOTS_ID_GENERATOR", sequenceName = "SEQ_CENTRE_IMPOTS", allocationSize = 1)
public class CentreImpot extends BaseEntity
{

    private static final long serialVersionUID = 135487L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CENTRE_IMPOTS_ID_GENERATOR")
    private Long id;

    @Column(nullable = false, name = "COMMUNE")
    private String commune;

    @Column(nullable = false, name = "NOMBRE_AGENTS")
    private int nombreAgents;

    @Column(nullable = false, name = "OUVERT")
    private boolean ouvert;

    public CentreImpot()
    {
        super();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCommune()
    {
        return commune;
    }

    public void setCommune(String commune)
    {
        this.commune = commune;
    }

    public int getNombreAgents()
    {
        return nombreAgents;
    }

    public void setNombreAgents(int nombreAgents)
    {
        this.nombreAgents = nombreAgents;
    }

    public boolean isOuvert()
    {
        return ouvert;
    }

    public void setOuvert(boolean ouvert)
    {
        this.ouvert = ouvert;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((commune == null) ? 0 : commune.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + nombreAgents;
        result = prime * result + (ouvert ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CentreImpot other = (CentreImpot) obj;
        if (commune == null)
        {
            if (other.commune != null)
                return false;
        }
        else if (!commune.equals(other.commune))
            return false;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (nombreAgents != other.nombreAgents)
            return false;
        if (ouvert != other.ouvert)
            return false;
        return true;
    }

}
