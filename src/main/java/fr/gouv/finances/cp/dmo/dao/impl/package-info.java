/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
*/
// Projet : lombok.demo
/**
 * Documentation du paquet fr.gouv.finances.cp.dmo.dao.impl
 * @author chouard
 * @version 1.0
 */
package fr.gouv.finances.cp.dmo.dao.impl;