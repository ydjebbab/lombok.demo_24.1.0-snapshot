/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementEditionsAgregContriParAvisImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import java.util.Collection;
import java.util.Map;

import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;

/**
 * Class TraitementEditionsAgregContriParAvisImpl DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementEditionsAgregContriParAvisImpl extends AbstractServiceEditionCommunImpl
{

    /**
     * Constructeur
     */
    public TraitementEditionsAgregContriParAvisImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return collection
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerDatasource(java.util.Map)
     */
    @Override
    public Collection creerDatasource(Map parametresEdition)
    {

        return TraitementEditionsAgregContriParAvisStub.createBeanCollection();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {
        return parametresEdition;
    }

    /**
     * méthode à surcharger pour ajouter des éléments dans la description affichée à l'utilisateur.
     * 
     * @param ParametresEdition param
     * @return the description
     */
    @Override
    public String getDescription(Map ParametresEdition)
    {

        StringBuffer desc = new StringBuffer();
        desc.append(this.getNomEdition());
        // pour test car pas de parametre passés

        desc.append("  test parametre ");

        return desc.toString();
    }

}
