/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : Civilite.java
 *
 */
package fr.gouv.finances.cp.dmo.entite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;

/**
 * Entité Civilite .
 *
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 **/
// @XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "CIVILITE_CIVI")
@SequenceGenerator(name = "CIVILITE_ID_GENERATOR", sequenceName = "SEQ_CIVILITE_CIVI", allocationSize = 1)
public class Civilite extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @XmlAttribute(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CIVILITE_ID_GENERATOR")
    private Long id;

    // @XmlElement(name = "version")
    // @Version
    // @Column(name = "version")
    // private int version;

    @XmlElement(name = "code")
    @Column(nullable = false, name = "code")
    private Long code;

    @XmlElement(name = "libelle")
    @Column(nullable = false, name = "LIBELLE")
    private String libelle;

    public Civilite()
    {
        super();
    }

    public Long getCode()
    {
        return code;
    }

    public Long getId()
    {
        return id;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setCode(Long code)
    {
        this.code = code;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param obj
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Civilite other = (Civilite) obj;
        if (code == null)
        {
            if (other.code != null)
            {
                return false;
            }
        }
        else if (!code.equals(other.code))
        {
            return false;
        }
        return true;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @return int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int PRIME = hashCODEHASHMULT;
        int result = 1;

        result = PRIME * result + ((code == null) ? 0 : code.hashCode());
        return result;
    }

}
