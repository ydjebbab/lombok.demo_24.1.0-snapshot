/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.editionbean;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Classe Technique LettreAuMaire pas d'equivalent en Base de Données
 */
public class LettreAuMaire extends BaseTechBean
{
    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** le rme. */
    private ResultatMensuelExploitation leRme;

    /** le comptable du casino *. */
    private Comptable leComptable;

    /**
     * Constructeur de la classe LettreAuMaire.java
     */
    public LettreAuMaire()
    {
        super();

    }

    /**
     * Accesseur de phraseMoisSaison
     * 
     * @return phraseMoisSaison
     */
    public StringBuilder getPhraseMoisSaison()
    {
        return phraseMoisSaison;
    }

    /**
     * Accesseur de phraseFormulePolitesse
     * 
     * @return phraseFormulePolitesse
     */
    public StringBuilder getPhraseFormulePolitesse()
    {
        return phraseFormulePolitesse;
    }

    /**
     * Mutateur de phraseMoisSaison
     * 
     * @param phraseMoisSaison phraseMoisSaison
     */
    public void setPhraseMoisSaison(StringBuilder phraseMoisSaison)
    {
        this.phraseMoisSaison = phraseMoisSaison;
    }

    /**
     * Mutateur de phraseFormulePolitesse
     * 
     * @param phraseFormulePolitesse phraseFormulePolitesse
     */
    public void setPhraseFormulePolitesse(StringBuilder phraseFormulePolitesse)
    {
        this.phraseFormulePolitesse = phraseFormulePolitesse;
    }

    /** phrase mois saison. */
    private StringBuilder phraseMoisSaison;

    /** phrase formule politesse. */
    private StringBuilder phraseFormulePolitesse;

    /**
     * Accesseur de leRme
     * 
     * @return leRme
     */
    public ResultatMensuelExploitation getLeRme()
    {
        return leRme;
    }

    /**
     * Mutateur de leRme
     * 
     * @param leRme leRme
     */
    public void setLeRme(ResultatMensuelExploitation leRme)
    {
        this.leRme = leRme;
    }

    /**
     * Accesseur de serialversionuid.
     * 
     * @return serialversionuid
     */
    public static long getSerialversionuid()
    {
        return serialVersionUID;
    }

    /**
     * Accesseur de leComptable
     * 
     * @return leComptable
     */
    public Comptable getLeComptable()
    {
        return leComptable;
    }

    /**
     * Mutateur de leComptable
     * 
     * @param leComptable leComptable
     */
    public void setLeComptable(Comptable leComptable)
    {
        this.leComptable = leComptable;
    }

}
