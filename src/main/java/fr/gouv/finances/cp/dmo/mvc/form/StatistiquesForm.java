/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : StatistiquesForm.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Class StatistiquesForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class StatistiquesForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 470410828253049636L;

    /** rapport. */
    private String rapport;

    /** seuil agregation. */
    private Double seuilAgregation;

    /** edition produite. */
    private FichierJoint editionProduite;

    /**
     * Instanciation de statistiques form.
     */
    public StatistiquesForm()
    {
        super();
    }

    /**
     * Gets the edition produite.
     *
     * @return the edition produite
     */
    public FichierJoint getEditionProduite()
    {
        return editionProduite;
    }

    /**
     * Gets the rapport.
     *
     * @return the rapport
     */
    public String getRapport()
    {
        return rapport;
    }

    /**
     * Gets the seuil agregation.
     *
     * @return the seuil agregation
     */
    public Double getSeuilAgregation()
    {
        return seuilAgregation;
    }

    /**
     * Sets the edition produite.
     *
     * @param editionProduite the new edition produite
     */
    public void setEditionProduite(FichierJoint editionProduite)
    {
        this.editionProduite = editionProduite;
    }

    /**
     * Sets the rapport.
     *
     * @param rapport the new rapport
     */
    public void setRapport(String rapport)
    {
        this.rapport = rapport;
    }

    /**
     * Sets the seuil agregation.
     *
     * @param seuilAgregation the new seuil agregation
     */
    public void setSeuilAgregation(Double seuilAgregation)
    {
        this.seuilAgregation = seuilAgregation;
    }

}
