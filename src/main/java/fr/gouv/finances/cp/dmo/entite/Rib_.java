package fr.gouv.finances.cp.dmo.entite;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Rib.class)
public abstract class Rib_ {

	public static volatile SingularAttribute<Rib, String> codeGuichet;
	public static volatile SingularAttribute<Rib, String> numeroCompte;
	public static volatile SingularAttribute<Rib, Boolean> estAutomatise;
	public static volatile SingularAttribute<Rib, String> codeBanque;
	public static volatile SingularAttribute<Rib, Long> id;
	public static volatile SingularAttribute<Rib, Integer> version;
	public static volatile SingularAttribute<Rib, String> cleRib;

}

