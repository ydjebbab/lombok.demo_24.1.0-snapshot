package fr.gouv.finances.cp.dmo.entite;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AvisImposition.class)
public abstract class AvisImposition_ {

	public static volatile SingularAttribute<AvisImposition, String> reference;
	public static volatile SingularAttribute<AvisImposition, ContribuablePar> contribuablePar;
	public static volatile SingularAttribute<AvisImposition, Double> montant;
	public static volatile SingularAttribute<AvisImposition, Long> id;
	public static volatile SingularAttribute<AvisImposition, TypeImpot> typeImpot;
	public static volatile SingularAttribute<AvisImposition, Integer> version;
	public static volatile SingularAttribute<AvisImposition, Date> dateRole;

}

