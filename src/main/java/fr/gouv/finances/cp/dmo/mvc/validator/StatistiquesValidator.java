/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : StatistiquesValidator.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.validator;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.dmo.mvc.form.StatistiquesForm;

/**
 * Class StatistiquesValidator DGFiP.
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class StatistiquesValidator implements Validator
{

    /**
     * Instanciation de statistiques validator.
     */
    public StatistiquesValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    public boolean supports(Class clazz)
    {
        return clazz.equals(StatistiquesForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param arg0
     * @param arg1
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    public void validate(Object arg0, Errors arg1)
    {

    }

    /**
     * methode Validate surface saisie seuil agregation : DGFiP.
     *
     * @param command param
     * @param errors param
     */
    public void validateSurfaceSaisieSeuilAgregation(StatistiquesForm command, Errors errors)
    {
        if (command.getSeuilAgregation() == null
            || GenericValidator.isBlankOrNull(command.getSeuilAgregation().toString()))
        {
            errors.rejectValue("seuilAgregation", "rejected.seuilAgregation",
                "La saisie du seuil d'agrégation est obligatoire.");
        }
    }

    /**
     * methode Validate surface selection type tableau statistique : DGFiP.
     *
     * @param command param
     * @param errors param
     */
    public void validateSurfaceSelectionTypeTableauStatistique(StatistiquesForm command, Errors errors)
    {
        String rapport = command.getRapport();
        if (GenericValidator.isBlankOrNull(rapport))
        {
            errors.rejectValue("rapport", "rejected.rapport", "Saisie obligatoire d'un type de tableau statistique");
        }
    }

}
