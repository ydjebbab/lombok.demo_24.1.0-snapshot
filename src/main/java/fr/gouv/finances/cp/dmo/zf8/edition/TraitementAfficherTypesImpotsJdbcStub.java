/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : TraitementLancerTacheLongueStub.java
 *
 */
package fr.gouv.finances.cp.dmo.zf8.edition;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import fr.gouv.finances.cp.dmo.editionbean.EditionTypeImpot;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunStubImpl;

/**
 * Class TraitementAfficherTypesImpotsStub DGFiP.
 *
 * @author amleplatinec-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class TraitementAfficherTypesImpotsJdbcStub extends AbstractServiceEditionCommunStubImpl
{

    /**
     * Constructeur
     */
    public TraitementAfficherTypesImpotsJdbcStub()
    {
        super();
    }

    /**
     * methode Creates the bean collection : DGFiP.
     *
     * @return collection
     */
    public static Collection createBeanCollection()
    {

        Collection<EditionTypeImpot> lesEditionTypeImpot = new ArrayList<>();
        new ArrayList<TypeImpot>();

        String datedebuts = "01/10/2005";
        SimpleDateFormat sdfdebut = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
        Date datedebut = null;

        try
        {
            datedebut = sdfdebut.parse(datedebuts);
        }
        catch (ParseException e)
        {
            datedebut = null;
        }

        String datefins = "01/10/2006";
        SimpleDateFormat sdffin = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
        Date datefin = null;

        try
        {
            datefin = sdffin.parse(datefins);
        }
        catch (ParseException e)
        {
            datefin = null;
        }

        TypeImpot untypeimpot1 = new TypeImpot();

        untypeimpot1.setCodeImpot(Long.valueOf(1));
        untypeimpot1.setNomImpot("Impôt sur le revenu");
        untypeimpot1.setEstOuvertALaMensualisation(Boolean.TRUE);
        untypeimpot1.setEstDestineAuxParticuliers(Boolean.TRUE);
        untypeimpot1.setJourOuvertureAdhesion(datedebut);
        untypeimpot1.setJourLimiteAdhesion(datefin);
        untypeimpot1.setSeuil(new BigDecimal("300245.12"));
        untypeimpot1.setTaux(new BigDecimal("0.1234567895"));

        TypeImpot untypeimpot2 = new TypeImpot();

        untypeimpot2.setCodeImpot(Long.valueOf(2));
        untypeimpot2.setNomImpot("Taxe foncières");
        untypeimpot2.setEstOuvertALaMensualisation(Boolean.TRUE);
        untypeimpot2.setEstDestineAuxParticuliers(Boolean.TRUE);
        untypeimpot2.setJourOuvertureAdhesion(datedebut);
        untypeimpot2.setJourLimiteAdhesion(datefin);
        untypeimpot2.setSeuil(new BigDecimal("999999.99"));
        untypeimpot2.setTaux(new BigDecimal("0.8888888888"));

        TypeImpot untypeimpot3 = new TypeImpot();

        untypeimpot3.setCodeImpot(Long.valueOf(3));
        untypeimpot3.setNomImpot("Taxe d'habitation");
        untypeimpot3.setEstOuvertALaMensualisation(Boolean.TRUE);
        untypeimpot3.setEstDestineAuxParticuliers(Boolean.TRUE);
        untypeimpot3.setJourOuvertureAdhesion(datedebut);
        untypeimpot3.setJourLimiteAdhesion(datefin);
        untypeimpot3.setSeuil(new BigDecimal("777777.77"));
        untypeimpot3.setTaux(new BigDecimal("0.7777777777"));

        TypeImpot untypeimpot4 = new TypeImpot();

        untypeimpot4.setCodeImpot(Long.valueOf(4));
        untypeimpot4.setNomImpot("Taxe professionnelle");
        untypeimpot4.setEstOuvertALaMensualisation(Boolean.FALSE);
        untypeimpot4.setEstDestineAuxParticuliers(Boolean.TRUE);
        untypeimpot4.setJourOuvertureAdhesion(datedebut);
        untypeimpot4.setJourLimiteAdhesion(datefin);
        untypeimpot4.setSeuil(new BigDecimal("66666666.66"));
        untypeimpot4.setTaux(new BigDecimal("0.6666666666"));

        EditionTypeImpot uneEditionTypeImpot = new EditionTypeImpot();
        uneEditionTypeImpot.setCodeImpot(Long.valueOf(1));
        uneEditionTypeImpot.setNomImpot("Impôt sur le revenu");
        uneEditionTypeImpot.setEstOuvertALaMensualisation(Boolean.TRUE);
        uneEditionTypeImpot.setEstDestineAuxParticuliers(Boolean.TRUE);
        uneEditionTypeImpot.setJourOuvertureAdhesion(datedebut);
        uneEditionTypeImpot.setJourLimiteAdhesion(datefin);
        uneEditionTypeImpot.setSeuil(new BigDecimal("300245.12"));
        uneEditionTypeImpot.setTaux(new BigDecimal("0.1234567895"));
        lesEditionTypeImpot.add(uneEditionTypeImpot);

        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot1);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot2);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot3);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot4);

        uneEditionTypeImpot = new EditionTypeImpot();
        uneEditionTypeImpot.setCodeImpot(Long.valueOf(2));
        uneEditionTypeImpot.setNomImpot("Taxe foncières");
        uneEditionTypeImpot.setEstOuvertALaMensualisation(Boolean.TRUE);
        uneEditionTypeImpot.setEstDestineAuxParticuliers(Boolean.TRUE);
        uneEditionTypeImpot.setJourOuvertureAdhesion(datedebut);
        uneEditionTypeImpot.setJourLimiteAdhesion(datefin);
        uneEditionTypeImpot.setSeuil(new BigDecimal("999999.99"));
        uneEditionTypeImpot.setTaux(new BigDecimal("0.8888888888"));
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot1);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot2);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot3);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot4);

        lesEditionTypeImpot.add(uneEditionTypeImpot);

        uneEditionTypeImpot = new EditionTypeImpot();
        uneEditionTypeImpot.setCodeImpot(Long.valueOf(3));
        uneEditionTypeImpot.setNomImpot("Taxe d'habitation");
        uneEditionTypeImpot.setEstOuvertALaMensualisation(Boolean.TRUE);
        uneEditionTypeImpot.setEstDestineAuxParticuliers(Boolean.TRUE);
        uneEditionTypeImpot.setJourOuvertureAdhesion(datedebut);
        uneEditionTypeImpot.setJourLimiteAdhesion(datefin);
        uneEditionTypeImpot.setSeuil(new BigDecimal("777777.77"));
        uneEditionTypeImpot.setTaux(new BigDecimal("0.7777777777"));
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot1);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot2);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot3);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot4);

        lesEditionTypeImpot.add(uneEditionTypeImpot);

        uneEditionTypeImpot = new EditionTypeImpot();
        uneEditionTypeImpot.setCodeImpot(Long.valueOf(4));
        uneEditionTypeImpot.setNomImpot("Taxe professionnelle");
        uneEditionTypeImpot.setEstOuvertALaMensualisation(Boolean.FALSE);
        uneEditionTypeImpot.setEstDestineAuxParticuliers(Boolean.TRUE);
        uneEditionTypeImpot.setJourOuvertureAdhesion(datedebut);
        uneEditionTypeImpot.setJourLimiteAdhesion(datefin);
        uneEditionTypeImpot.setSeuil(new BigDecimal("66666666.66"));
        uneEditionTypeImpot.setTaux(new BigDecimal("0.6666666666"));
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot1);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot2);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot3);
        uneEditionTypeImpot.getLesTypesImpots().add(untypeimpot4);

        lesEditionTypeImpot.add(uneEditionTypeImpot);
        return lesEditionTypeImpot;

    }

    /**
     * Accesseur de l attribut description.
     *
     * @param parametresEdition param
     * @return description
     */
    public String getDescription(Map parametresEdition)
    {
        return "tot3";

    }
}
