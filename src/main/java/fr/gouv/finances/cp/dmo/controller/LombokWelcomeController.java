/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Classe redirigeant les appels sur la racine de la servlet dispatcher (/) vers la page la vue accueil.ex qui est
 * résolue en accueil.jsp (voir controleur AccueilController).
 */
@Controller
@ApiModel(description = "Redirection vers accueil.jsp ")
public class LombokWelcomeController
{
    /**
     * Redirection vers la page d'accueil.
     *
     * @param model le model
     * @return string
     */
    @ApiOperation(value = "Redirection vers accueil.jsp", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Succès de la redirection"),
            @ApiResponse(code = 401, message = "Accès non autorisé à cette ressource"),
            @ApiResponse(code = 403, message = "Accès interdit à la ressource"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String welcome(Model model)
    {
        return "redirect:/accueil.ex";
    }

}
