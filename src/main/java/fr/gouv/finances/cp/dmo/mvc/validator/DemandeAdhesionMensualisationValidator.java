/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : DemandeAdhesionMensualisationValidator.java
 *
 */

package fr.gouv.finances.cp.dmo.mvc.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.dmo.mvc.form.DemandeAdhesionMensualisationForm;

/**
 * Class DemandeAdhesionMensualisationValidator.
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
public class DemandeAdhesionMensualisationValidator implements Validator
{

    /**
     * Instanciation de demande adhesion mensualisation validator.
     */
    public DemandeAdhesionMensualisationValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    public boolean supports(Class clazz)
    {
        return clazz.equals(DemandeAdhesionMensualisationForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param command
     * @param errors
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    public void validate(Object command, Errors errors)
    {
    }

    /**
     * Methode de validation de surface : Règle de nommage debute par validateSurface NomdeVue.
     *
     * @param command param
     * @param errors param
     */

    public void validateSurfaceSaisieRib(DemandeAdhesionMensualisationForm command, Errors errors)
    {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nouveauContrat.rib.codeBanque",
            "rejected.codebanqueobligatoire");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nouveauContrat.rib.codeGuichet",
            "rejected.codeguichetobligatoire");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nouveauContrat.rib.numeroCompte",
            "rejected.numerocompteobligatoire");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nouveauContrat.rib.cleRib", "rejected.cleribobligatoire");
    }

    /**
     * methode Validate surface saisie type impot : DGFiP.
     *
     * @param command param
     * @param errors param
     */
    public void validateSurfaceSaisieTypeImpot(DemandeAdhesionMensualisationForm command, Errors errors)
    {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "avisImposition.reference",
            "rejected.referenceavisimposition", "Avis vide");

        if (command.getNouveauContrat().getTypeImpot() == null)
        {
            errors.rejectValue("nouveauContrat.typeImpot", null, "Sélectionnez un type d'impôt.");
        }

    }

}
