/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementTestChargeEditionImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.batch;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;

/**
 * Batch TraitementTestChargeEditionImpl
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Service("traitementtestchargeedition")
@Profile("batch")
@Lazy(true)
public class TraitementTestChargeEditionImpl extends ServiceBatchCommunImpl
{

    /** editiondemandeserviceso. */
    @Autowired
    @Qualifier("editiondemandeserviceimpl")
    private EditionDemandeService editiondemandeserviceso;

    /** nombre boucle. */
    private Integer nombreBoucle = Integer.valueOf("1");

    /** uid proprietaire. */
    private String uidProprietaire = "DMO";

    /** mail destinataire. */
    private String mailDestinataire = "";

    /** nofifie par mail. */
    private Boolean nofifieParMail = Boolean.FALSE;

    /** code structure. */
    private String codeStructure = "0750000";

    /**
     * Constructeur 
     */
    public TraitementTestChargeEditionImpl()
    {
        super();
    }

    /**
     * Gets the code structure.
     * 
     * @return the codeStructure
     */
    public String getCodeStructure()
    {
        return codeStructure;
    }

    /**
     * Accesseur de l attribut editiondemandeserviceso.
     * 
     * @return editiondemandeserviceso
     */
    public EditionDemandeService getEditiondemandeserviceso()
    {
        return editiondemandeserviceso;
    }

    /**
     * Gets the mail destinataire.
     * 
     * @return the mailExpediteur
     */
    public String getMailDestinataire()
    {
        return mailDestinataire;
    }

    /**
     * Gets the nofifie par mail.
     * 
     * @return the nofifieParMail
     */
    public Boolean getNofifieParMail()
    {
        return nofifieParMail;
    }

    /**
     * Gets the nombre boucle.
     * 
     * @return the nombreBoucle
     */
    public Integer getNombreBoucle()
    {
        return nombreBoucle;
    }

    /**
     * Gets the uid proprietaire.
     * 
     * @return the mailDemandeur
     */
    public String getUidProprietaire()
    {
        return uidProprietaire;
    }

    /**
     * Sets the code structure.
     * 
     * @param codeStructure the codeStructure to set
     */
    @Value("${traitementtestchargeedition.codeStructure}")
    public void setCodeStructure(String codeStructure)
    {
        this.codeStructure = codeStructure;
    }

    /**
     * Modificateur de l attribut editiondemandeserviceso.
     * 
     * @param editiondemandeserviceso le nouveau editiondemandeserviceso
     */
    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso)
    {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    /**
     * Sets the mail destinataire.
     * 
     * @param mailExpediteur the mailExpediteur to set
     */
    @Value("${traitementtestchargeedition.mailDestinataire}")
    public void setMailDestinataire(String mailExpediteur)
    {
        this.mailDestinataire = mailExpediteur;
    }

    /**
     * Sets the nofifie par mail.
     * 
     * @param nofifieParMail the nofifieParMail to set
     */
    @Value("${traitementtestchargeedition.nofifieParMail}")
    public void setNofifieParMail(Boolean nofifieParMail)
    {
        this.nofifieParMail = nofifieParMail;
    }

    /**
     * Sets the nombre boucle.
     * 
     * @param nombreBoucle the nombreBoucle to set
     */
    @Value("${traitementtestchargeedition.nombreBoucle}")
    public void setNombreBoucle(Integer nombreBoucle)
    {
        this.nombreBoucle = nombreBoucle;
    }

    /**
     * Sets the uid proprietaire.
     * 
     * @param mailDemandeur the mailDemandeur to set
     */
    @Value("${traitementtestchargeedition.uidProprietaire}")
    public void setUidProprietaire(String mailDemandeur)
    {
        this.uidProprietaire = mailDemandeur;
    }

    @Value("${traitementtestchargeedition.nombatch}")
    @Override
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }

    @Value("${traitementtestchargeedition.versionbatch}")
    @Override
    public void setVersionBatch(String versionBatch)
    {
        super.setVersionBatch(versionBatch);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        log.debug("Debut methode traiterBatch()");
        Map<?, ?> lesArgumentsPasses = getArguments();

        // on va regarder pour chaque clé du fichier propriété si clé
        // avec meme nom existe dans les arguments passés
        if (lesArgumentsPasses != null)
        {
            if (lesArgumentsPasses.containsKey("nombreBoucle"))
            {
                setNombreBoucle(Integer.valueOf((String) lesArgumentsPasses.get("nombreBoucle")));

                log.info("Nombre de boucle:" + (String) lesArgumentsPasses.get("nombreBoucle"));
            }

            if (lesArgumentsPasses.containsKey("mailDemandeur"))
            {
                setUidProprietaire((String) lesArgumentsPasses.get("mailDemandeur"));
                log.info("mail demandeur :" + (String) lesArgumentsPasses.get("mailDemandeur"));
            }
            if (lesArgumentsPasses.containsKey("fichier.civilite"))
            {
                setMailDestinataire((String) lesArgumentsPasses.get("mailDestinataire"));
                log.debug("Mail destinataire :" + (String) lesArgumentsPasses.get("mailDestinataire"));

            }
            if (lesArgumentsPasses.containsKey("fichier.situationfami"))
            {
                setNofifieParMail(Boolean.valueOf((String) lesArgumentsPasses.get("nofifieParMail")));
                log.debug("Notifie par mail :" + (String) lesArgumentsPasses.get("nofifieParMail"));

            }
            if (lesArgumentsPasses.containsKey("codeStructure"))
            {
                setCodeStructure((String) lesArgumentsPasses.get("codeStructure"));
                log.debug("Code structure :" + (String) lesArgumentsPasses.get("codeStructure"));

            }
        }

        log.info("nb itération = " + nombreBoucle.intValue());
        int nombreIteration = 0;
        do
        {
            log.debug("nombreIteration : " + nombreIteration);
            log.debug("nofifieParMail : " + nofifieParMail);
            if (nofifieParMail.booleanValue())
            {
                produireEditionsAvecEnvoiDeMails("zf2.adressescontribuables.edition");
                produireEditionsAvecEnvoiDeMails("zf3.agregcontriparavis.edition");
                produireEditionsAvecEnvoiDeMails("zf3.agregcontripartypeimpot.edition");
                produireEditionsAvecEnvoiDeMails("zf3.casinospardepartement.edition");
                produireEditionsAvecEnvoiDeMails("zf3.graphiquecontripartypeimpot.edition");
                produireEditionsAvecEnvoiDeMails("zf3.graphiquerepartavispartypeimpot.edition");
            }
            else
            {
                produireEditionsSansEnvoiDeMails("zf2.adressescontribuables.edition");
                produireEditionsSansEnvoiDeMails("zf3.agregcontriparavis.edition");
                produireEditionsSansEnvoiDeMails("zf3.agregcontripartypeimpot.edition");
                produireEditionsSansEnvoiDeMails("zf3.casinospardepartement.edition");
                produireEditionsSansEnvoiDeMails("zf3.graphiquecontripartypeimpot.edition");
                produireEditionsSansEnvoiDeMails("zf3.graphiquerepartavispartypeimpot.edition");
            }
            nombreIteration++;
        }
        while (nombreIteration < nombreBoucle.intValue());

    }

    /**
     * methode Produit editions avec envoi de mails
     * 
     * @param beanEditionId param
     */
    private void produireEditionsAvecEnvoiDeMails(String beanEditionId)
    {
        log.debug(">>> Debut methode produireEditionsAvecEnvoiDeMails");
        Map<?, ?> parametresEdition = new HashMap<>();

        EditionSynchroneNotifieeParMail editionSynchroneNotifieeParMail =
            editiondemandeserviceso.initEditionSynchroneNotifieeParMail(beanEditionId, uidProprietaire,
                mailDestinataire);

        editionSynchroneNotifieeParMail.getDestinationEdition().ajouterUnProfilDestinataire("ADMINISTRATEUR")
            .ajouterUnFiltre("CODE_STRUCTURE_SUP").affecterLaValeur("0350000").affecterLaValeur(codeStructure);

        editionSynchroneNotifieeParMail =
            editiondemandeserviceso.declencherEdition(editionSynchroneNotifieeParMail, parametresEdition);
    }

    /**
     * methode Produit editions sans envoi de mails 
     * 
     * @param beanEditionId param
     */
    private void produireEditionsSansEnvoiDeMails(String beanEditionId)
    {
        log.debug(">>> Debut methode produireEditionsSansEnvoiDeMails");
        Map<?, ?> parametresEdition = new HashMap<>();

        EditionSynchroneNonNotifieeParMail editionSynchroneNotifieeParMail =
            editiondemandeserviceso.initEditionSynchroneNonNotifieeParMail(beanEditionId, uidProprietaire);
        log.debug(">>> Debut 1");
        editionSynchroneNotifieeParMail.getDestinationEdition().ajouterUnProfilDestinataire("ADMINISTRATEUR")
            .ajouterUnFiltre("CODE_STRUCTURE_SUP").affecterLaValeur("0350000").affecterLaValeur(codeStructure);
        log.debug(">>> Debut 2");
        editionSynchroneNotifieeParMail =
            editiondemandeserviceso.declencherEdition(editionSynchroneNotifieeParMail, parametresEdition);
    }

}
