/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : DocumentJointServiceImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.dao.IDocumentJointDao;
import fr.gouv.finances.cp.dmo.service.IDocumentJointService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Class DocumentJointServiceImpl
 * 
 * @author chouard-cp
 * @author CF: passage XML vers annotations
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Service("documentJointService")
public class DocumentJointServiceImpl extends BaseServiceImpl implements IDocumentJointService
{
    @Autowired
    private IDocumentJointDao documentJointDao;

    public DocumentJointServiceImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param documentsASupprimer
     * @see fr.gouv.finances.cp.dmo.service.IDocumentJointService#supprimerLesDocumentsJoints(java.util.Collection)
     */
    @Override
    public void supprimerLesDocumentsJoints(Collection documentsASupprimer)
    {
        documentJointDao.deleteCollectionDocumentsJoints(documentsASupprimer);
    }

}
