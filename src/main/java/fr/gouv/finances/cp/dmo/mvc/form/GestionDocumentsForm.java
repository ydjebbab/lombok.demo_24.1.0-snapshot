/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : GestionDocumentsForm.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.DocumentJoint;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Class GestionDocumentsForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class GestionDocumentsForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** contribuable. */
    private Contribuable contribuable;

    /** document joint. */
    private DocumentJoint documentJoint = new DocumentJoint();;

    /** fichier joint. */
    private FichierJoint fichierJoint;

    /**
     * Instanciation de gestion documents form.
     */
    public GestionDocumentsForm()
    {
        super();
    }

    /**
     * Gets the contribuable.
     *
     * @return the contribuable
     */
    public Contribuable getContribuable()
    {
        return contribuable;
    }

    /**
     * Gets the document joint.
     *
     * @return the document joint
     */
    public DocumentJoint getDocumentJoint()
    {
        return documentJoint;
    }

    /**
     * Gets the fichier joint.
     *
     * @return the fichier joint
     */
    public FichierJoint getFichierJoint()
    {
        return fichierJoint;
    }

    /**
     * Sets the contribuable.
     *
     * @param contribuable the new contribuable
     */
    public void setContribuable(Contribuable contribuable)
    {
        this.contribuable = contribuable;
    }

    /**
     * Sets the document joint.
     *
     * @param documentJoint the new document joint
     */
    public void setDocumentJoint(DocumentJoint documentJoint)
    {
        this.documentJoint = documentJoint;
    }

    /**
     * Sets the fichier joint.
     *
     * @param fichierJoint the new fichier joint
     */
    public void setFichierJoint(FichierJoint fichierJoint)
    {
        this.fichierJoint = fichierJoint;
    }

}
