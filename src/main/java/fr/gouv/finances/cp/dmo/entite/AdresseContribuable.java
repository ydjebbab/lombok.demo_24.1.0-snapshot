/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.entite;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

/**
 * Entité AdresseContribuable
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Embeddable
public class AdresseContribuable implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Column(name = "ligne1")
    private String ligne1;

    @Column(name = "ligne2")
    private String ligne2;

    @Column(name = "ligne3")
    private String ligne3;

    @Column(name = "ligne4")
    private String ligne4;

    @Column(name = "pays")
    private String pays;

    @Column(name = "ville")
    private String ville;

    @Column(name = "codePostal")
    private String codePostal;

    @Transient
    protected final int hashCODEHASHINIT = 7;

    @Transient
    protected final int hashCODEHASHMULT = 31;

    public AdresseContribuable()
    {
        super();
    }

    public String getCodePostal()
    {
        return codePostal;
    }

    public String getLigne1()
    {
        return ligne1;
    }

    public String getLigne2()
    {
        return ligne2;
    }

    public String getLigne3()
    {
        return ligne3;
    }

    public String getLigne4()
    {
        return ligne4;
    }

    public String getPays()
    {
        return pays;
    }

    public String getVille()
    {
        return ville;
    }

    public void setCodePostal(String codePostal)
    {
        this.codePostal = codePostal;
    }

    public void setLigne1(String ligne1)
    {
        this.ligne1 = ligne1;
    }

    public void setLigne2(String ligne2)
    {
        this.ligne2 = ligne2;
    }

    public void setLigne3(String ligne3)
    {
        this.ligne3 = ligne3;
    }

    public void setLigne4(String ligne4)
    {
        this.ligne4 = ligne4;
    }

    public void setPays(String pays)
    {
        this.pays = pays;
    }

    public void setVille(String ville)
    {
        this.ville = ville;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param object
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }

        if (object == null || (object.getClass() != this.getClass()))
        {
            return false;
        }

        AdresseContribuable myObj = (AdresseContribuable) object;

        return ((ligne1 == null ? myObj.ligne1 == null : ligne1.equals(myObj.ligne1))
            && (ligne2 == null ? myObj.ligne2 == null : ligne2.equals(myObj.ligne2))
            && (ligne3 == null ? myObj.ligne3 == null : ligne3.equals(myObj.ligne3))
            && (ligne4 == null ? myObj.ligne4 == null : ligne4.equals(myObj.ligne4))
            && (pays == null ? myObj.pays == null : pays.equals(myObj.pays))
            && (ville == null ? myObj.ville == null : ville.equals(myObj.ville)) && (codePostal == null
                ? myObj.codePostal == null : codePostal.equals(myObj.codePostal)));

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int hash = hashCODEHASHINIT;
        int varCode = 0;

        varCode = (null == ligne1 ? 0 : ligne1.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        varCode = (null == ligne2 ? 0 : ligne2.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        varCode = (null == ligne3 ? 0 : ligne3.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        varCode = (null == ligne4 ? 0 : ligne4.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        varCode = (null == pays ? 0 : pays.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        varCode = (null == ville ? 0 : ville.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        return hash;

    }
}
