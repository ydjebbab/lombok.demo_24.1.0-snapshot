/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TestAdressesPersonnesService.java
 *
 */
package fr.gouv.finances.cp.dmo.service;

import java.util.List;

import fr.gouv.finances.cp.dmo.entite.AdresseTest;
import fr.gouv.finances.cp.dmo.entite.PersonneTest;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Service utilisé pour les tests Hibernate sur les relations bi-directionnelles n-n.
 * 
 * @author chouard-cp
 * @author CF: migration vers JPA
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
public interface ITestAdressesPersonnesService
{

    /**
     * methode Supprimer une personne.
     * 
     * @param personne personne
     */
    public void supprimerPersonne(PersonneTest personne);

    /**
     * methode Supprimer une adresse.
     * 
     * @param adresseTest adresseTest
     */
    public void supprimerAdresse(AdresseTest adresseTest);

    /**
     * methode Ajouter une adresse a la personne : DGFiP.
     * 
     * @param personne param
     * @param adresse param
     */
    void ajouterAdresseALaPersonne(PersonneTest personne, AdresseTest adresse);

    /**
     * methode Ajouter une personne al adresse : DGFiP.
     * 
     * @param adresse param
     * @param personne param
     */
    void ajouterPersonneALAdresse(AdresseTest adresse, PersonneTest personne);

    /**
     * methode Rechercher les personnes et adresses par ville : DGFiP.
     * 
     * @param ville param
     * @return list< personne test>
     */
    List<PersonneTest> rechercherPersonnesEtAdressesParVille(String ville);

    /**
     * methode Rechercher toutes les personnes : DGFiP.
     * 
     * @return list< personne test>
     */
    List<PersonneTest> rechercherToutesLesPersonnes();

    /**
     * methode Rechercher toutes les personnes.
     * 
     * @param nombreOccurences Nombre d'occurences
     * @return dgcp scroll iterator
     */
    public ScrollIterator rechercherToutesLesPersonnesIterator(int nombreOccurences);

    /**
     * methode Rechercher une adresse et personnes par voie et ville : DGFiP.
     * 
     * @param voie param
     * @param ville param
     * @return adresse test
     */
    AdresseTest rechercherAdresseEtPersonnesParVoieEtVille(String voie, String ville);

    /**
     * methode Rechercher une personne et adresses par nom : DGFiP.
     * 
     * @param nom param
     * @param prenom param
     * @return personne test
     */
    PersonneTest rechercherPersonneEtAdressesParNom(String nom, String prenom);

    /**
     * Sauvegarder une personne Test
     * 
     * @param personneTest personne
     */
    public void sauvegarderPersonne(PersonneTest personneTest);

    /**
     * Sauvegarder une adresse Test
     * 
     * @param adresseTest adresse
     */
    public void sauvegarderAdresse(AdresseTest adresseTest);

}