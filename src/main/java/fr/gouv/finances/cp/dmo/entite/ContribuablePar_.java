package fr.gouv.finances.cp.dmo.entite;

import java.util.Date;
import java.util.TimeZone;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContribuablePar.class)
public abstract class ContribuablePar_ extends fr.gouv.finances.cp.dmo.entite.Contribuable_ {

	public static volatile ListAttribute<ContribuablePar, DocumentJoint> lesDocsJoints;
	public static volatile SingularAttribute<ContribuablePar, String> adresseMail;
	public static volatile SingularAttribute<ContribuablePar, String> telephonePortable;
	public static volatile SingularAttribute<ContribuablePar, String> nom;
	public static volatile SingularAttribute<ContribuablePar, Date> dateDeNaissance;
	public static volatile SetAttribute<ContribuablePar, ContratMensu> listeContratMensu;
	public static volatile SingularAttribute<ContribuablePar, String> anneeDeNaissance;
	public static volatile SetAttribute<ContribuablePar, AvisImposition> listeAvisImposition;
	public static volatile SingularAttribute<ContribuablePar, Double> solde;
	public static volatile SingularAttribute<ContribuablePar, SituationFami> situationFamiliale;
	public static volatile SingularAttribute<ContribuablePar, String> telephoneFixe;
	public static volatile ListAttribute<ContribuablePar, AdresseContribuable> listeAdresses;
	public static volatile SingularAttribute<ContribuablePar, String> prenom;
	public static volatile SingularAttribute<ContribuablePar, String> dateDeMiseAJour;
	public static volatile SingularAttribute<ContribuablePar, TimeZone> fuseauhorairetel;
	public static volatile SingularAttribute<ContribuablePar, Civilite> civilite;

}

