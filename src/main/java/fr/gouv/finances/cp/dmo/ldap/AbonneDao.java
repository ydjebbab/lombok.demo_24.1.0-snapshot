/*
 * 
 */
package fr.gouv.finances.cp.dmo.ldap;

import java.util.List;

import javax.naming.Name;

/**
 * The Interface AbonneDao.
 */
public interface AbonneDao
{

    /**
     * Creates the.
     *
     * @param abonne the abonne
     */
    void create(Abonne abonne);

    /**
     * Delete.
     *
     * @param abonne the abonne
     */
    void delete(Abonne abonne);

    /**
     * Find all.
     *
     * @return the list
     */
    List<Abonne> findAll();

    /**
     * Find by dn.
     *
     * @param dn the dn
     * @return the abonne
     */
    public Abonne findByDn(Name dn);

    /**
     * Update.
     *
     * @param abonne the abonne
     */
    void update(Abonne abonne);

    /**
     * recherche par sn.
     *
     * @param sn 
     * @return abonne
     */
    public Abonne findBySn(String sn);

}
