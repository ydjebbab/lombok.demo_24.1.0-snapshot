/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * 
 *
 * fichier : TraitementEditionsListeContribuablesCsvImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf2.edition;

import java.io.OutputStream;
import java.util.Map;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.cp.dmo.dao.IContribuableDao;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionFichierCommunImpl;
import fr.gouv.finances.lombok.util.csv.Csvprinter;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Class TraitementEditionsListeContribuablesCsvImpl.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
public class TraitementEditionsListeContribuablesCsvImpl extends AbstractServiceEditionFichierCommunImpl
{

    /** transaction template. */
    private TransactionTemplate transactionTemplate;

    /** contribuablebatchdao. */
    private IContribuableDao contribuabledao;

    /**
     * Constructeur 
     */
    public TraitementEditionsListeContribuablesCsvImpl()
    {
        super(); 

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @param out
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionFichierCommunImpl#creerFichierEdition(java.util.Map,
     *      java.io.OutputStream)
     */
    @Override
    public void creerFichierEdition(Map parametresEdition, OutputStream out)
    {

        final Csvprinter csvPrinter = new Csvprinter(out);
        csvPrinter.setToujoursProtegerLeschamps(false);
        String[] ligneEntete = {"Identifiant", "Nom", "Prénom", "Adresse mail"};
        csvPrinter.writeln(ligneEntete);

        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {

                public void doInTransactionWithoutResult(TransactionStatus arg0)
                {
                    ScrollIterator scrollIterator = contribuabledao.findTousContribuableParIterator(100);
                    while (scrollIterator.hasNext())
                    {
                        ContribuablePar unContribuable = (ContribuablePar) scrollIterator.nextObjetMetier();

                        String[] values =
                        {unContribuable.getIdentifiant(), unContribuable.getNom(), unContribuable.getPrenom(),
                                unContribuable.getAdresseMail()};

                        // On vide la session une fois l'objet lu pour éviter une
                        // exception levée
                        // par Hibernate lorsqu'un collection est associée à deux
                        // sessions
                        contribuabledao.clearPersistenceContext();
                        csvPrinter.writeln(values);
                    }
                }
            });
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur lors de la production de l'édition de la liste des contribuables au format CSV",
                e);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionFichierCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        return "fichierdetest";
    }

    /**
     * Modificateur de l attribut contribuabledao.
     * 
     * @param contribuabledao le nouveau contribuabledao
     */
    public void setContribuabledao(IContribuableDao contribuabledao)
    {
        this.contribuabledao = contribuabledao;
    }

    /**
     * Accesseur de l attribut transaction template.
     * 
     * @return transaction template
     */
    public TransactionTemplate getTransactionTemplate()
    {
        return transactionTemplate;
    }

    /**
     * Modificateur de l attribut transaction template.
     * 
     * @param transactionTemplate le nouveau transaction template
     */
    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

}