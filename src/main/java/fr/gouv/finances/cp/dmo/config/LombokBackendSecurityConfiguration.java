/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.config;


import java.util.List;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;

import fr.gouv.finances.lombok.config.LombokWebSharedConfiguration;
import fr.gouv.finances.lombok.config.ServiceWebSecurityConfigurer;
import fr.gouv.finances.lombok.securite.springsecurity.AucunLdapAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.LdapAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.RealAuthentificationSecurityLombok;

/**
 * <pre>
 * Classe de configuration de la sécurité dans l'application démo. 
 * Configuration à compléter pour les URLS.
 * Cette classe remplace la partie servlet déclarée dans le fichier web.xml et elle remplace
 * également les définitions contenues dans les fichiers applicationContext-commun-security.xml et security.xml
 * Normalement un source de propriétés est créée par spring boot 
 * (depuis le fichier application.properties pour permettre de résoudre les annotations Value)
 * </pre>
 */
@Configuration
@EnableWebSecurity(debug=true)
@Order(1)
@ComponentScan(basePackages = {"fr.gouv.finances.cp.dmo.controller", "fr.gouv.finances.cp.dmo.rest.controller"})
/*
 * @ImportResource(value = {"classpath:conf/applicationContext-commun-security.xml",
 * "classpath:conf/application/security.xml"})
 */
public class LombokBackendSecurityConfiguration extends WebSecurityConfigurerAdapter
{
    private static final Logger log = LoggerFactory.getLogger(LombokBackendSecurityConfiguration.class);

    /** code application dans habilitations. */
    @Value(value = "${lombok.aptera.codeappli}")
    String codeApplicationDansHabilitations;

    /** mode authentification. */
    @Value(value = "${authentification.modeauthentification}")
    String modeAuthentification;

    /** type annuaire. */
    @Value(value = "${annuaire.typeannuaire}")
    String typeAnnuaire;
    
    /** lombok sireme config. */
    @Autowired
    LombokWebSharedConfiguration lombokSiremeConfig;
   
    /**
     * lombok access denied handler. ce controlleur est retrouvé grâce à l'annotation de scan
     */
    @Autowired
    @Named("accessDeniedBackendHandler")
    private AccessDeniedHandler accessDeniedBackendHandler;

    @Autowired
    @Named("authenticationSuccessBackendHandler")
    private AuthenticationSuccessHandler authenticationSuccessBackendHandler;

    @Autowired
    @Named("authenticationFailureBackendHandler")
    private AuthenticationFailureHandler authenticationFailureBackendHandler;
    
    /**
     * classes de configuration additionnelles (cumulatives) qui héritent de l'interface ServiceWebSecurityConfigurer 
     */
    @Autowired(required = false)
    List<ServiceWebSecurityConfigurer> additionalWebSecurityConfigurer;
    
    @Autowired
    private RealAuthentificationSecurityLombok authentificationRealProvider;

    @Autowired
    private LdapAuthentificationSecurityLombok authenticationLdapProvider;
    
    @Autowired
    private AucunLdapAuthentificationSecurityLombok authenticationAucunLdapProvider;
    
    /**
     * Constructeur 
     */
    public LombokBackendSecurityConfiguration()
    {
        super();
    }

    /**
     * Constructeur
     *
     * @param disableDefaults
     */
    public LombokBackendSecurityConfiguration(boolean disableDefaults)
    {
        super(disableDefaults);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * 
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        log.debug(">>> Debut methode LombokBackendSecurityConfiguration.configure()");
        
        // configuration uniquement pour les appels rest
        http.requestMatchers().antMatchers("/j_appelportailbackend", "/j_spring_security_check_backend", "/rest/**");
        
        // point d'entrée pour l'authentification / erreurs liées à l'authentification / accès
        http.authorizeRequests().antMatchers("/j_appelportailbackend", 
            "/rest/demandeauthentification", 
            "/rest/authentificationechec", 
            "/rest/accesinterdit", 
            "/rest/accesinterditcsrf").permitAll();

        // redirection par défaut lors d'une authentification réussie
        http.authorizeRequests()
            .regexMatchers("/rest/.*")
            .hasAnyAuthority(
                Authorite.EXPLOITANT.toString(),
                Authorite.CHEFDEPROJETAPPLICATIF.toString(),
                Authorite.PARTICULIER.toString(),
                Authorite.ADMINISTRATEUR.toString(),
                Authorite.ROLLS.toString(),
                Authorite.NOTAIRES.toString());
        
        http
            .formLogin()
            .loginPage("/rest/demandeauthentification")
            .loginProcessingUrl("/j_spring_security_check_backend")
            .successHandler(authenticationSuccessBackendHandler)
            .failureHandler(authenticationFailureBackendHandler);            
        
        // on refuse les requêtes non autorisées
        http.authorizeRequests().anyRequest().denyAll();
        
        // CORS
        http.cors();            
        

        // interdiction d'accès (par protection CSRF ou non)
        http.exceptionHandling().accessDeniedHandler(accessDeniedBackendHandler);
        
        // CSRF (on désactive le csrf pour l'url d'authentification en POST)
//        http.csrf().disable();
        http.csrf().ignoringAntMatchers("/j_spring_security_check_backend").csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }
    
    @Override
    public void configure(WebSecurity security){
        security.ignoring().antMatchers("/rest/demandeauthentification");
    }   
    
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        auth.authenticationProvider(authenticationAucunLdapProvider);
        auth.authenticationProvider(authentificationRealProvider);
        auth.authenticationProvider(authenticationLdapProvider);
    }
}
