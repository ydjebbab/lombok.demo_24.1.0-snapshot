/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.editionbean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import fr.gouv.finances.cp.dmo.editionbean.ResultatMensuelExploitation;
import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Casino.java class des Casinos.
 * 
 * @author rtoutain-cp Date: 8 févr. 2011
 */

public class Casino extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** identifiant. */
    private int identifiant = 0;

    /** name. */
    private String name = "";

    /** montant. */
    private Double montant = new Double(0);

    /** modedeprelevement. */
    private String modedeprelevement = "";

    /** code poste comptable. */
    private String codePosteSie;

    /** code poste comptable. */
    private String codPostCompt;

    /** date creation. */
    private Timestamp dateCreation;

    /** insee commmune. */
    private String inseeCom;

    /** libelle (du casino). */
    private String libelle;

    /** libelle enseigne. */
    private String libelleEnseigne;

    /** liste resultats mensuels exploitation. */
    private List<ResultatMensuelExploitation> listeResultatsMensuelsExploitation = new ArrayList<ResultatMensuelExploitation>();

    /** numéro service public. */
    private Integer noServPub;

    /** specificite globale ctx. */
    private String specificiteGlobaleCtx;

    /**
     * Instanciation de casino.
     */
    public Casino()
    {
        super();
    }

    /**
     * Instanciation de casino.
     * 
     * @param name param
     * @param montant param
     * @param libelle param
     * @param modedeprelevement param
     */
    public Casino(String name, Double montant, String libelle, String modedeprelevement)
    {
        super();
        this.name = name;
        this.montant = montant;
        this.libelle = libelle;
        this.modedeprelevement = modedeprelevement;

    }

    /*
     * cette méthode FORTE est deplacé dans CahierChargesServiceImpl method CASINOClass.EstCahierSupprimable(input
     * pobj_CahierDesCharges: CASINO_BN_BMServices.CAHIERCHARGESClass): Framework.boolean
     */

    /**
     * Accesseur de l attribut cod poste sie.
     * 
     * @return code poste sie
     */
    public String getCodePosteSie()
    {
        return codePosteSie;
    }

    /**
     * Gets the liste resultats mensuels exploitation.
     * 
     * @return the liste resultats mensuels exploitation
     */
    public List<ResultatMensuelExploitation> getListeResultatsMensuelsExploitation()
    {
        return listeResultatsMensuelsExploitation;
    }

    /**
     * Sets the liste resultats mensuels exploitation.
     * 
     * @param listeResultatsMensuelsExploitation the new liste resultats mensuels exploitation
     */
    public void setListeResultatsMensuelsExploitation(List<ResultatMensuelExploitation> listeResultatsMensuelsExploitation)
    {
        this.listeResultatsMensuelsExploitation = listeResultatsMensuelsExploitation;
    }

    /**
     * Accesseur de l attribut cod post compt.
     * 
     * @return cod post compt
     */
    public String getCodPostCompt()
    {
        return codPostCompt;
    }

    /**
     * Accesseur de l attribut date creation.
     * 
     * @return date creation
     */
    public Timestamp getDateCreation()
    {
        return dateCreation;
    }

    /**
     * Accesseur de l attribut insee com.
     * 
     * @return insee com
     */
    public String getInseeCom()
    {
        return inseeCom;
    }

    /**
     * Accesseur de l attribut libelle.
     * 
     * @return libelle
     */
    public String getLibelle()
    {
        return libelle;
    }

    /**
     * Accesseur de l attribut libelle enseigne.
     * 
     * @return libelle enseigne
     */
    public String getLibelleEnseigne()
    {
        return libelleEnseigne;
    }

    /**
     * Accesseur de l attribut no serv pub.
     * 
     * @return no serv pub
     */
    public Integer getNoServPub()
    {
        return noServPub;
    }

    /**
     * Accesseur de l attribut reference casino. la referenceCasino est la concaténation de inseeCom et noServPub
     * 
     * @return reference casino
     */
    public String getReferenceCasino()
    {
        StringBuilder reference = new StringBuilder();
        reference.append(this.inseeCom);
        reference.append(this.noServPub);
        return reference.toString();
    }

    /**
     * Accesseur de l attribut specificite globale ctx.
     * 
     * @return specificite globale ctx
     */
    public String getSpecificiteGlobaleCtx()
    {
        return specificiteGlobaleCtx;
    }

    /**
     * Modificateur de l attribut code poste sie.
     * 
     * @param codePosteSie le nouveau cod poste sie
     */
    public void setCodePosteSie(String codePosteSie)
    {
        this.codePosteSie = codePosteSie;
    }

    /**
     * Modificateur de l attribut cod post compt.
     * 
     * @param codPostCompt le nouveau cod post compt
     */
    public void setCodPostCompt(String codPostCompt)
    {
        this.codPostCompt = codPostCompt;
    }

    /**
     * Modificateur de l attribut date creation.
     * 
     * @param dateCreation le nouveau date creation
     */
    public void setDateCreation(Timestamp dateCreation)
    {
        this.dateCreation = dateCreation;
    }

    /**
     * Modificateur de l attribut insee com.
     * 
     * @param inseeCom le nouveau insee com
     */
    public void setInseeCom(String inseeCom)
    {
        this.inseeCom = inseeCom;
    }

    /**
     * Modificateur de l attribut libelle.
     * 
     * @param libelle le nouveau libelle
     */
    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    /**
     * Modificateur de l attribut libelle enseigne.
     * 
     * @param libelleEnseigne le nouveau libelle enseigne
     */
    public void setLibelleEnseigne(String libelleEnseigne)
    {
        this.libelleEnseigne = libelleEnseigne;
    }

    /**
     * Modificateur de l attribut no serv pub.
     * 
     * @param noServPub le nouveau no serv pub
     */
    public void setNoServPub(Integer noServPub)
    {
        this.noServPub = noServPub;
    }

    /**
     * Modificateur de l attribut specificite globale ctx.
     * 
     * @param specificiteGlobaleCtx le nouveau specificite globale ctx
     */
    public void setSpecificiteGlobaleCtx(String specificiteGlobaleCtx)
    {
        this.specificiteGlobaleCtx = specificiteGlobaleCtx;
    }

    /**
     * Accesseur de l attribut identifiant.
     * 
     * @return identifiant
     */
    public int getIdentifiant()
    {
        return identifiant;
    }

    /**
     * Modificateur de l attribut identifiant.
     * 
     * @param identifiant le nouveau identifiant
     */
    public void setIdentifiant(int identifiant)
    {
        this.identifiant = identifiant;
    }

    /**
     * Accesseur de l attribut name.
     * 
     * @return name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Modificateur de l attribut name.
     * 
     * @param name le nouveau name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gets the montant.
     * 
     * @return the montant
     */
    public Double getMontant()
    {
        return montant;
    }

    /**
     * Sets the montant.
     * 
     * @param montant the new montant
     */
    public void setMontant(Double montant)
    {
        this.montant = montant;
    }

    /**
     * Gets the modedeprelevement.
     * 
     * @return the modedeprelevement
     */
    public String getModedeprelevement()
    {
        return modedeprelevement;
    }

    /**
     * Sets the modedeprelevement.
     * 
     * @param modedeprelevement the new modedeprelevement
     */
    public void setModedeprelevement(String modedeprelevement)
    {
        this.modedeprelevement = modedeprelevement;
    }

}
