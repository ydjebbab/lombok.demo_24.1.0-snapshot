/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : AvantHabilitationServiceImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.securite;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

import fr.gouv.finances.lombok.securite.service.AvantHabilitationService;
import fr.gouv.finances.lombok.securite.techbean.HabilitationAnnuaire;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * Demo d'un intercepteur avant habilitation
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
@Service("avanthabilitationservice")
public class AvantHabilitationServiceImpl implements AvantHabilitationService
{
    private static final Logger log = LoggerFactory.getLogger(AvantHabilitationServiceImpl.class);
    
    public AvantHabilitationServiceImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param unePersonneAnnuaire
     * @throws BadCredentialsException the bad credentials exception
     * @see fr.gouv.finances.lombok.securite.service.AvantHabilitationService#executerAvantHabilitation(fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire)
     */
    @Override
    public void executerAvantHabilitation(PersonneAnnuaire unePersonneAnnuaire) throws BadCredentialsException
    {
        log.debug(">>> Debut methode executerAvantHabilitation()");
        /* on ajoute dynamiquement un profil fictif dans la liste d'habilitations de la personne */
        HabilitationAnnuaire nouvellehabilitation = new HabilitationAnnuaire();
        nouvellehabilitation.setLibelleCourtAppli("dmo");
        nouvellehabilitation.setDateCreation(Calendar.getInstance().getTime());
        nouvellehabilitation.setDateDebut(Calendar.getInstance().getTime());
        nouvellehabilitation.setNomAdministrateur("intercepteur avant habilitation");
        nouvellehabilitation.setNomProfil("EXPERT");
        unePersonneAnnuaire.getListeHabilitations().add(nouvellehabilitation);
    }

}
