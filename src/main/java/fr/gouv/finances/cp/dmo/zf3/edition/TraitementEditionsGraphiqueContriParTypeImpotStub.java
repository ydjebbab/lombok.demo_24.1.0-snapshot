/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import java.util.ArrayList;
import java.util.Collection;

import fr.gouv.finances.cp.dmo.techbean.StatistiquesContribuables;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunStubImpl;

/**
 * Class TraitementEditionsGraphiqueContriParTypeImpotStub DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
public class TraitementEditionsGraphiqueContriParTypeImpotStub extends AbstractServiceEditionCommunStubImpl
{
    private static final int CINQ = 5;

    private static final int TROIS = 3;

    /**
     * Constructeur
     */
    public TraitementEditionsGraphiqueContriParTypeImpotStub()
    {
        super();
    }

    /**
     * Création d'une collection pour brancher ce bean dans jasperstudio
     * par exemple
     * 
     * @return collection
     */
    public static Collection<StatistiquesContribuables> createBeanCollection()
    {
        Collection<StatistiquesContribuables> list = new ArrayList<>();
        int iii = 1;

        list.add(new StatistiquesContribuables("utilisateur" + iii, Long.valueOf(iii), Long.valueOf((long) TROIS * iii), Long
            .valueOf(iii), "Impôt sur le revenu", Double.valueOf(96 * (double) iii), Double.valueOf((double) CINQ * iii)));
        iii++;
        list.add(new StatistiquesContribuables("utilisateur" + iii, Long.valueOf(iii), Long.valueOf((long) TROIS * iii), Long
            .valueOf(iii), "Taxe foncière", Double.valueOf(96 * (double) iii), Double.valueOf((double) CINQ * iii)));
        iii++;
        list.add(new StatistiquesContribuables("utilisateur" + iii, Long.valueOf(iii), Long.valueOf((long) TROIS * iii), Long
            .valueOf(iii), "Taxe d'habitation", Double.valueOf(96 * (double) iii), Double.valueOf((double) CINQ * iii)));
        iii++;
        list.add(new StatistiquesContribuables("utilisateur" + iii, Long.valueOf(iii), Long.valueOf((long) TROIS * iii), Long
            .valueOf(iii), "ISF", Double.valueOf(96 * (double) iii), Double.valueOf(CINQ * (double) iii)));

        return list;
    }

}
