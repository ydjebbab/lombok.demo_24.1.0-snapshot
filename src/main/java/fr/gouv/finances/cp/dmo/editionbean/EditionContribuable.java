/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : EditionContribuable.java
 *
 */
package fr.gouv.finances.cp.dmo.editionbean;

import java.util.ArrayList;
import java.util.List;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class EditionContribuable DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class EditionContribuable extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** identifiant. */
    private String identifiant;

    /** type contribuable. */
    private String typeContribuable;

    /** nom. */
    private String nom;

    /** prenom. */
    private String prenom;

    /** solde. */
    private String solde;

    /** nombre de contri traites. */
    private String nombreDeContriTraites;

    /** lesEltsPourEntete - List<EditionContribuable>, pour remplir entete commune. */
    private List<EditionContribuable> lesEltsPourEntete = new ArrayList<EditionContribuable>();

    /**
     * Instanciation de edition contribuable.
     */
    public EditionContribuable()
    {
        super();
        lesEltsPourEntete = new ArrayList<EditionContribuable>();
        lesEltsPourEntete.add(this);
    }

    /**
     * Accesseur de l attribut identifiant.
     * 
     * @return identifiant
     */
    public String getIdentifiant()
    {
        return identifiant;
    }

    /**
     * Gets the lesEltsPourEntete - List<EditionContribuable>, pour remplir entete commune.
     * 
     * @return the lesEltsPourEntete - List<EditionContribuable>, pour remplir entete commune
     */
    public List<EditionContribuable> getLesEltsPourEntete()
    {
        return lesEltsPourEntete;
    }

    /**
     * Accesseur de l attribut nom.
     * 
     * @return nom
     */
    public String getNom()
    {
        return nom;
    }

    /**
     * Accesseur de l attribut nombre de contri traites.
     * 
     * @return nombre de contri traites
     */
    public String getNombreDeContriTraites()
    {
        return nombreDeContriTraites;
    }

    /**
     * Accesseur de l attribut prenom.
     * 
     * @return prenom
     */
    public String getPrenom()
    {
        return prenom;
    }

    /**
     * Accesseur de l attribut solde.
     * 
     * @return solde
     */
    public String getSolde()
    {
        return solde;
    }

    /**
     * Accesseur de l attribut type contribuable.
     * 
     * @return type contribuable
     */
    public String getTypeContribuable()
    {
        return typeContribuable;
    }

    /**
     * Modificateur de l attribut identifiant.
     * 
     * @param identifiant le nouveau identifiant
     */
    public void setIdentifiant(String identifiant)
    {
        this.identifiant = identifiant;
    }

    /**
     * Sets the lesEltsPourEntete - List<EditionContribuable>, pour remplir entete commune.
     * 
     * @param lesEltsPourEntete the new lesEltsPourEntete - List<EditionContribuable>, pour remplir entete commune
     */
    public void setLesEltsPourEntete(List<EditionContribuable> lesEltsPourEntete)
    {
        this.lesEltsPourEntete = lesEltsPourEntete;
    }

    /**
     * Modificateur de l attribut nom.
     * 
     * @param nom le nouveau nom
     */
    public void setNom(String nom)
    {
        this.nom = nom;
    }

    /**
     * Modificateur de l attribut nombre de contri traites.
     * 
     * @param nombreDeContriTraites le nouveau nombre de contri traites
     */
    public void setNombreDeContriTraites(String nombreDeContriTraites)
    {
        this.nombreDeContriTraites = nombreDeContriTraites;
    }

    /**
     * Modificateur de l attribut prenom.
     * 
     * @param prenom le nouveau prenom
     */
    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    /**
     * Modificateur de l attribut solde.
     * 
     * @param solde le nouveau solde
     */
    public void setSolde(String solde)
    {
        this.solde = solde;
    }

    /**
     * Modificateur de l attribut type contribuable.
     * 
     * @param typeContribuable le nouveau type contribuable
     */
    public void setTypeContribuable(String typeContribuable)
    {
        this.typeContribuable = typeContribuable;
    }

}
