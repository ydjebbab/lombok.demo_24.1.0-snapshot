package fr.gouv.finances.cp.dmo.entite;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OrigineTest.class)
public abstract class OrigineTest_ {

	public static volatile SingularAttribute<OrigineTest, String> libelle;
	public static volatile SingularAttribute<OrigineTest, Long> id;
	public static volatile SingularAttribute<OrigineTest, Integer> version;

}

