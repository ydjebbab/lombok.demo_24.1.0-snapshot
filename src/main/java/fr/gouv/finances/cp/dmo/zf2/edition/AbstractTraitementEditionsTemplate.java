/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.zf2.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;

/**
 * Class AbstractTraitementEditionsTemplate .
 */
public abstract class AbstractTraitementEditionsTemplate extends AbstractServiceEditionCommunImpl
{

    /** subreportdetail dir. */
    private final String SUBREPORTDETAIL_DIR = "SUBREPORTDETAIL_DIR";

    /** mon sous rapport. */
    private final String MON_SOUS_RAPPORT = "MON_SOUS_RAPPORT";

    /** sous rapport. */
    private String sousRapport;

    /** subreportdirdetail. */
    private String subreportdirdetail;

    /**
     * Constructeur
     */
    public AbstractTraitementEditionsTemplate()
    {
        super();
    }

    @Override
    public Collection creerDatasource(Map parametresEdition)
    {
        List<EditionTemplate> listeEditionTemplate = new ArrayList<EditionTemplate>();
        EditionTemplate edition = new EditionTemplate();

        listeEditionTemplate.add(edition);
        renseignerCorpsEditions(parametresEdition, edition);

        parametresEdition.put(SUBREPORTDETAIL_DIR, this.subreportdirdetail);
        parametresEdition.put(MON_SOUS_RAPPORT, this.sousRapport);

        return listeEditionTemplate;
    }

    /**
     * methode Renseigner editions biens :.
     *
     * @param parametresEdition map
     */
    protected abstract void renseignerCorpsEditions(Map parametresEdition, EditionTemplate edition);

    /**
     * Accesseur de l attribut conflit parametres edition.
     * 
     * @return conflit parametres edition
     */
    protected String[] getConflitParametresEdition()
    {
        return new String[] {};
    }

    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {
        if (parametresEdition == null)
        {
            Map parametresEditionTemp = new HashMap();
            parametresEdition = parametresEditionTemp;
        }

        return parametresEdition;
    }

    /**
     * Accesseur de sousRapport.
     *
     * @return sousRapport
     */
    public String getSousRapport()
    {
        return sousRapport;
    }

    /**
     * Mutateur de sousRapport.
     *
     * @param sousRapport sousRapport
     */
    public void setSousRapport(String sousRapport)
    {
        this.sousRapport = sousRapport;
    }

    /**
     * Accesseur de subreportdirdetail.
     *
     * @return subreportdirdetail
     */
    public String getSubreportdirdetail()
    {
        return subreportdirdetail;
    }

    /**
     * Mutateur de subreportdirdetail.
     *
     * @param subreportdirdetail subreportdirdetail
     */
    public void setSubreportdirdetail(String subreportdirdetail)
    {
        this.subreportdirdetail = subreportdirdetail;
    }

}
