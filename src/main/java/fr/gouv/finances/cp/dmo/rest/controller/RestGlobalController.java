package fr.gouv.finances.cp.dmo.rest.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

@Component
@Path("/")
public class RestGlobalController
{
    @GET
    @Path("/authentificationsucces")
    @Produces({MediaType.APPLICATION_JSON})
    public PersonneAnnuaire authentificationsucces() {               
        return UtilisateurSecurityLombokContainer.getPersonneAnnuaire();         
    }
    
    @GET
    @Path("/authentificationechec")
    @Produces({MediaType.APPLICATION_JSON})
    public Response authentificationechec() {               
        return Response.status(Response.Status.UNAUTHORIZED).entity("Echec de l'authentification.").build();       
    }
    
    @GET
    @Path("/demandeauthentification")
    @Produces({MediaType.APPLICATION_JSON})
    public Response demandeauthentification() {               
        return Response.status(Response.Status.UNAUTHORIZED).entity("Authentification nécessaire pour accéder à cette ressource.").build();       
    }
    
    @GET
    @Path("/accesinterdit")
    @Produces({MediaType.APPLICATION_JSON})
    public Response accesinterdit() {               
        return Response.status(Response.Status.UNAUTHORIZED).entity("Acces interdit.").build();       
    }
    
    @GET
    @Path("/accesinterditcsrf")
    @Produces({MediaType.APPLICATION_JSON})
    public Response accesinterditcsrf() {               
        return Response.status(Response.Status.UNAUTHORIZED).entity("Acces interdit (protection CSRF).").build();       
    }
}
