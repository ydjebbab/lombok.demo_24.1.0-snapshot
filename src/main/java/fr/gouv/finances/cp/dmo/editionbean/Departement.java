/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : Departement.java
 *
 */
package fr.gouv.finances.cp.dmo.editionbean;

import java.util.ArrayList;
import java.util.List;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class Departement DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class Departement extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** departement. */
    private Double departement = new Double(0);

    /** list casino. */
    private List<Casino> listCasino = new ArrayList<Casino>();

    /**
     * Instanciation de departement.
     */
    public Departement()
    {
        super();
    }

    /**
     * Instanciation de departement.
     * 
     * @param departement param
     */
    public Departement(Double departement)
    {
        super();
        this.departement = departement;

    }

    /**
     * Accesseur de l attribut departement.
     * 
     * @return departement
     */
    public Double getDepartement()
    {
        return departement;
    }

    /**
     * Accesseur de l attribut list casino.
     * 
     * @return list casino
     */
    public List<Casino> getListCasino()
    {
        return listCasino;
    }

    /**
     * Modificateur de l attribut departement.
     * 
     * @param departement le nouveau departement
     */
    public void setDepartement(Double departement)
    {
        this.departement = departement;
    }

    /**
     * Modificateur de l attribut list casino.
     * 
     * @param listCasino le nouveau list casino
     */
    public void setListCasino(List<Casino> listCasino)
    {
        this.listCasino = listCasino;
    }
}
