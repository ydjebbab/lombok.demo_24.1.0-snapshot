package fr.gouv.finances.cp.dmo.entite;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Civilite.class)
public abstract class Civilite_ {

	public static volatile SingularAttribute<Civilite, Long> code;
	public static volatile SingularAttribute<Civilite, String> libelle;
	public static volatile SingularAttribute<Civilite, Long> id;
	public static volatile SingularAttribute<Civilite, Integer> version;

}

