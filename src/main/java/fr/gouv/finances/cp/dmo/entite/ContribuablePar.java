/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 *
 */

package fr.gouv.finances.cp.dmo.entite;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;

import org.apache.commons.lang.builder.CompareToBuilder;

import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Entité ContribuablePar
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */

@NamedEntityGraphs({
        // Force les chargements des avis d'imposition et des contrats de mensualisation
        @NamedEntityGraph(name = ContribuablePar.AVISIMPOSITIONCONTRATMENSU_GRAPH, attributeNodes = {
                @NamedAttributeNode("listeAvisImposition"),
                @NamedAttributeNode("listeContratMensu")}),
        // Force les chargements des déclarations de revenus
        @NamedEntityGraph(name = ContribuablePar.DECLARATIONREVENU_GRAPH, attributeNodes = @NamedAttributeNode(value = "listeDeclarationRevenu"), subgraphs = {
                // Force les chargements des revenus fonciers
                @NamedSubgraph(name = "declarationsrevenus.revenusfonciers", attributeNodes = @NamedAttributeNode(value = "listeRevenuFoncier")),
                // Force les chargements des revenus mobiliers
                @NamedSubgraph(name = "declarationsrevenus.revenusmobiliers", attributeNodes = @NamedAttributeNode(value = "listeRevenuMobilier"))
        })
        /*
         * // Force les chargements des docs joints
         * @NamedEntityGraph(name = ContribuablePar.TOUT_GRAPH, attributeNodes = {
         * @NamedAttributeNode("lesDocsJoints"),
         * @NamedAttributeNode("listeAvisImposition"),
         * @NamedAttributeNode("listeContratMensu"),
         * @NamedAttributeNode("listeDeclarationRevenu") })
         */
})
@Entity
@DiscriminatorValue("CP")
public class ContribuablePar extends Contribuable implements Comparable<ContribuablePar>
{
    private static final long serialVersionUID = 1L;

    public static final String AVISIMPOSITIONCONTRATMENSU_GRAPH = "AvisImpositionContratMensu";

    public static final String DECLARATIONREVENU_GRAPH = "DeclarationRevenu";

    // public static final String TOUT_GRAPH = "TOUT";

    @Column(name = "ADRESSEMAIL")
    private String adresseMail;

    @Column(name = "NOM")
    private String nom;

    @Column(name = "PRENOM")
    private String prenom;

    @Column(name = "DATEDEMISEAJOUR")
    private String dateDeMiseAJour; /* date de la dernière mise à jour */

    @Column(name = "FUSEAUHORAIRETEL")
    private TimeZone fuseauhorairetel;

    @Column(name = "TELEPHONEFIXE")
    private String telephoneFixe;

    @Column(name = "TELEPHONEPORTABLE")
    private String telephonePortable;

    @Column(name = "DATEDENAISSANCE")
    private Date dateDeNaissance;

    @Column(name = "SOLDE")
    private Double solde;

    @Column(name = "ANNEEDENAISSANCE")
    private String anneeDeNaissance;

    // private Object logo;

    @ManyToOne // Unidirectionnel
    @JoinColumn(name = "CIVI_ID_CIVILITE", foreignKey = @ForeignKey(name = "FK_CONT_CIVI_1"))
    private Civilite civilite;

    @ManyToOne // Unidirectionnel
    @JoinColumn(name = "SIFA_ID_SITUATIONFAMILIALE", foreignKey = @ForeignKey(name = "FK_CONT_SIFA_1"))
    private SituationFami situationFamiliale;

    // unidirectionnel
    @OneToMany
    // @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "CONT_ID_LESDOCSJOINTS", foreignKey = @ForeignKey(name = "FK_FIC_CONT_1"))
    private List<DocumentJoint> lesDocsJoints = new ArrayList<>();

    @OneToMany(mappedBy = "contribuablePar", fetch = FetchType.LAZY) // bidirectionnel
    // @OneToMany(orphanRemoval = true, cascade = {CascadeType.ALL})
    // @JoinColumn(name = "contribuablePar")
    // @Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "collection")
    private Set<AvisImposition> listeAvisImposition = new HashSet<>();

    @OneToMany(mappedBy = "contribuablePar", fetch = FetchType.LAZY) // bidirectionnel
    private List<DeclarationRevenu> listeDeclarationRevenu = new ArrayList<>();

    @OneToMany(mappedBy = "contribuablePar", fetch = FetchType.LAZY) // bidirectionnel
    private Set<ContratMensu> listeContratMensu = new HashSet<>();

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "CONT_LISTEADRESSES", joinColumns = @JoinColumn(name = "CONT_ID_LISTEADRESSES", foreignKey = @ForeignKey(name = "FK_CONT_ID_LISTEADRESSES_1")))
    @OrderColumn(name = "CONT_ID_LISTEADRESSES_INDEX")
    private List<AdresseContribuable> listeAdresses = new ArrayList<>();

    public ContribuablePar()
    {
        super();
    }

    public ContribuablePar(Long id)
    {
        super(id);
    }

    public ContribuablePar(String identifiant)
    {
        super(identifiant);
    }

    public ContribuablePar(String nom, String prenom, String adresseMail, String telephoneFixe, String telephonePortable)
    {
        this.nom = nom;
        this.prenom = prenom;
        this.adresseMail = adresseMail;
        this.telephoneFixe = telephoneFixe;
        this.telephonePortable = telephonePortable;
    }

    public String getAdresseMail()
    {
        return adresseMail;
    }

    public String getAnneeDeNaissance()
    {
        return anneeDeNaissance;
    }

    public Civilite getCivilite()
    {
        return civilite;
    }

    public String getDateDeMiseAJour()
    {
        return dateDeMiseAJour;
    }

    public Date getDateDeNaissance()
    {
        return this.dateDeNaissance;
    }

    public TimeZone getFuseauhorairetel()
    {
        return fuseauhorairetel;
    }

    public List<DocumentJoint> getLesDocsJoints()
    {
        return lesDocsJoints;
    }

    public List<AdresseContribuable> getListeAdresses()
    {

        return listeAdresses;
    }

    public Set<AvisImposition> getListeAvisImposition()
    {
        return listeAvisImposition;
    }

    public Set<ContratMensu> getListeContratMensu()
    {
        return listeContratMensu;
    }

    public String getNom()
    {
        return nom;
    }

    public String getPrenom()
    {
        return prenom;
    }

    public SituationFami getSituationFamiliale()
    {
        return situationFamiliale;
    }

    public Double getSolde()
    {
        return solde;
    }

    public String getTelephoneFixe()
    {
        return telephoneFixe;
    }

    public String getTelephonePortable()
    {
        return telephonePortable;
    }

    public void setAdresseMail(String mail)
    {
        this.adresseMail = mail;
    }

    public void setAnneeDeNaissance(String anneeDeNaissance)
    {
        this.anneeDeNaissance = anneeDeNaissance;
    }

    public void setCivilite(Civilite civilite)
    {
        this.civilite = civilite;
    }

    public void setDateDeMiseAJour(String datedemiseajour)
    {
        this.dateDeMiseAJour = datedemiseajour;
    }

    public void setDateDeNaissance(Date dateDeNaissance)
    {
        this.dateDeNaissance = dateDeNaissance;
    }

    public void setFuseauhorairetel(TimeZone fuseauhorairetel)
    {
        this.fuseauhorairetel = fuseauhorairetel;
    }

    public void setLesDocsJoints(List<DocumentJoint> lesDocsJoints)
    {
        this.lesDocsJoints = lesDocsJoints;
    }

    public void setListeAdresses(List<AdresseContribuable> listeAdresses)
    {
        this.listeAdresses = listeAdresses;
    }

    public void setListeAvisImposition(Set<AvisImposition> listeavisimposition)
    {
        this.listeAvisImposition = listeavisimposition;
        this.adresseMail = "tutu";
    }

    public void setListeContratMensu(Set<ContratMensu> listecontratmensu)
    {
        this.listeContratMensu = listecontratmensu;
        this.adresseMail = "tutu";
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    public void setSituationFamiliale(SituationFami situationFamiliale)
    {
        this.situationFamiliale = situationFamiliale;
    }

    public void setSolde(Double solde)
    {
        this.solde = solde;
    }

    public void setTelephoneFixe(String telephonefixe)
    {
        this.telephoneFixe = telephonefixe;
    }

    public void setTelephonePortable(String telephoneportable)
    {
        this.telephonePortable = telephoneportable;
    }

    public List<DeclarationRevenu> getListeDeclarationRevenu()
    {
        return listeDeclarationRevenu;
    }

    public void setListeDeclarationRevenu(List<DeclarationRevenu> listeDeclarationRevenu)
    {
        this.listeDeclarationRevenu = listeDeclarationRevenu;
    }

    /**
     * methode Rattacher une nouvelle adresse
     * 
     * @param adresseContribuable param
     */
    public void rattacherUneNouvelleAdresse(AdresseContribuable adresseContribuable)
    {
        if (adresseContribuable != null)
        {
            this.getListeAdresses().add(adresseContribuable);
        }
        else
        {
            throw new ProgrammationException("erreur.programmation");
        }
    }

    /**
     * methode Rattacher un nouveau avis imposition.
     * 
     * @param avisimposition param
     */
    public void rattacherUnNouveauAvisImposition(AvisImposition avisimposition)
    {
        if (avisimposition != null)
        {
            this.getListeAvisImposition().add(avisimposition);
        }
        else
        {
            throw new ProgrammationException("erreur.programmation");
        }
    }

    /**
     * methode Rattacher un nouveau contrat mensu
     * 
     * @param contrat param
     */
    public void rattacherUnNouveauContratMensu(ContratMensu contrat)
    {
        if (contrat != null)
        {
            this.getListeContratMensu().add(contrat);
        }
        else
        {
            throw new ProgrammationException("erreur.programmation");
        }
    }

    /**
     * methode Rechercher avis imposition attache
     * 
     * @param reference param
     * @return avis imposition
     */
    public AvisImposition rechercherAvisImpositionAttache(String reference)
    {
        AvisImposition compavis = new AvisImposition();
        compavis.setReference(reference);
        for (AvisImposition avisImposition : getListeAvisImposition())
        {
            if (avisImposition.equals(compavis))
            {
                return avisImposition;
            }
        }

        return null;
    }

    // renvoie la premiere adresse **dans un premier temps , dans le proto,
    // un contribuable n'a qu'une seule adresse
    /**
     * Accesseur de l attribut adresse principale.
     * 
     * @return adresse principale
     */
    public AdresseContribuable getAdressePrincipale()
    {
        AdresseContribuable uneAdresse = new AdresseContribuable();
        if (getListeAdresses().isEmpty())
        {
            AdresseContribuable uneAdresse1 = new AdresseContribuable();
            listeAdresses.add(uneAdresse1);
        }
        for (AdresseContribuable adresseContribuable : getListeAdresses())
        {
            uneAdresse = adresseContribuable;
            break;
        }
        return uneAdresse;
    }

    /**
     * methode Verifier avis imposition est attache au contribuable r g14
     * 
     * @param referenceavisimposition param
     */
    public void verifierAvisImpositionEstAttacheAuContribuableRG14(String referenceavisimposition)
    {

        Set<String> ensembleReferenceAvisAssocies = new HashSet<>();

        for (AvisImposition avisImposition : getListeAvisImposition())
        {
            ensembleReferenceAvisAssocies.add(avisImposition.getReference());

        }

        if (!ensembleReferenceAvisAssocies.contains(referenceavisimposition))
        {
            // Avis imposition n est pas attache au contribuable
            throw new RegleGestionException(Messages.getString("exception.avisnonrattache") + this.getNom());
        }
    }

    /**
     * methode Verifier pas d adhesion en cours sur ce type d impot pour ce contribuable r g12 : DGFiP.
     * 
     * @param idimpot param
     */
    public void verifierPasDAdhesionEnCoursSurCeTypeDImpotPourCeContribuableRG12(Long idimpot)
    {
        Set<ContratMensu> listeContratMensuDuContribuable = getListeContratMensu();

        if (!listeContratMensuDuContribuable.isEmpty())
        {
            for (ContratMensu contratMensu : listeContratMensuDuContribuable)
            {
                if (contratMensu.getTypeImpot().getCodeImpot().equals(idimpot))
                {
                    // le contribuable a déja un contrat d'adhesion en cours sur ce type impot
                    throw new RegleGestionException(Messages.getString("exception.adhesionencours.part1")
                        + this.getNom() + Messages.getString("exception.adhesionencours.part2")
                        + contratMensu.getTypeImpot().getNomImpot());
                }
            }
        }
        // le contribuable n a pas de contrat d'adhesion en cours sur ce type impot
    }

    /**
     * methode Detacher avis imposition
     * 
     * @param avisimposition param
     */
    public void detacherAvisImposition(AvisImposition avisimposition)
    {
        this.getListeAvisImposition().remove(avisimposition);
    }

    /**
     * methode Detacher contrat mensu
     * 
     * @param contratmensu param
     */
    public void detacherContratMensu(ContratMensu contratmensu)
    {
        this.getListeContratMensu().remove(contratmensu);
    }

    @Override
    public String toString()
    {
        return "ContribuablePar [adresseMail=" + adresseMail + ", nom=" + nom + ", prenom=" + prenom + ", dateDeMiseAJour="
            + dateDeMiseAJour + ", fuseauhorairetel=" + fuseauhorairetel + ", telephoneFixe=" + telephoneFixe + ", telephonePortable="
            + telephonePortable + ", dateDeNaissance=" + dateDeNaissance + ", solde=" + solde + ", civilite=" + civilite
            + ", situationFamiliale=" + situationFamiliale + ", lesDocsJoints=" + lesDocsJoints + ", listeAvisImposition="
            + listeAvisImposition + ", listeContratMensu=" + listeContratMensu + ", id=" + id + ", identifiant=" + identifiant
            + "]";
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param contribuablePar
     * @return int
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(ContribuablePar contribuablePar)
    {
        return new CompareToBuilder().append(identifiant, contribuablePar.getIdentifiant()).append(nom, contribuablePar.getNom())
            .append(prenom, contribuablePar.getPrenom()).append(adresseMail, contribuablePar.getAdresseMail()).toComparison();
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((adresseMail == null) ? 0 : adresseMail.hashCode());
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ContribuablePar other = (ContribuablePar) obj;
        if (adresseMail == null)
        {
            if (other.adresseMail != null)
                return false;
        }
        else if (!adresseMail.equals(other.adresseMail))
            return false;
        if (nom == null)
        {
            if (other.nom != null)
                return false;
        }
        else if (!nom.equals(other.nom))
            return false;
        if (prenom == null)
        {
            if (other.prenom != null)
                return false;
        }
        else if (!prenom.equals(other.prenom))
            return false;
        return true;
    }

    /*
     * public Object getLogo() { return logo; } public void setLogo(Object logo) { this.logo = logo; }
     */

}
