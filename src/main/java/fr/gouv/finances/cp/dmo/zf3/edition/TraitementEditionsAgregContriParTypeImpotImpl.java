/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementEditionsAgregContriParTypeImpotImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import java.util.Collection;
import java.util.Map;

import fr.gouv.finances.cp.dmo.service.IStatistiquesService;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;

/**
 * Class TraitementEditionsAgregContriParTypeImpotImpl DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementEditionsAgregContriParTypeImpotImpl extends AbstractServiceEditionCommunImpl
{

    /** statistiquesserviceso. */
    private IStatistiquesService statistiquesserviceso;

    /**
     * Constructeur
     */
    public TraitementEditionsAgregContriParTypeImpotImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return collection
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerDatasource(java.util.Map)
     */
    @Override
    public Collection creerDatasource(Map parametresEdition)
    {
        // Exécution de la requête qui produit la source de données
        Collection data = statistiquesserviceso.calculerAgregationContribuableParTypeImpot();
        return data;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        String result = "AgregationContribuablesParTypeImpot";
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {
        return null;
    }

    /**
     * Accesseur de l attribut statistiquesserviceso.
     * 
     * @return statistiquesserviceso
     */
    public IStatistiquesService getStatistiquesserviceso()
    {
        return statistiquesserviceso;
    }

    /**
     * Modificateur de l attribut statistiquesserviceso.
     * 
     * @param statistiquesserviceso le nouveau statistiquesserviceso
     */
    public void setStatistiquesserviceso(IStatistiquesService statistiquesserviceso)
    {
        this.statistiquesserviceso = statistiquesserviceso;
    }
}
