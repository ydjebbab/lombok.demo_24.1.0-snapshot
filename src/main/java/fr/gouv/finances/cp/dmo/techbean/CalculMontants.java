/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.techbean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Exemple de calculs précalculés. Pourrait être plus générique en appellant une méthode pourcentage générique,idem
 * decompte...
 * 
 * @author amleplatinec
 * @version $Revision: 1.5 $
 */
public class CalculMontants extends BaseTechBean
{
    private static final long serialVersionUID = 1L;

    public CalculMontants()
    {
        super();
    }

    /**
     * renvoie le decompte des types impot ouverts à la mensu.
     * 
     * @param lesTypesImpots
     * @return double
     */
    public Double calculerDecompteEstOuvertALaMensualisation(List lesTypesImpots)
    {
        Double result = new Double(0);
        int ouvertmesutrue = 0;

        if (lesTypesImpots != null && !lesTypesImpots.isEmpty())
        {
            for (int i = 0; i < lesTypesImpots.size(); i++)            
            {
                TypeImpot unTypeImpot = (TypeImpot) lesTypesImpots.get(i);
                if (unTypeImpot != null)
                {
                    Boolean lebooleenRes = unTypeImpot.getEstOuvertALaMensualisation();
                    if (lebooleenRes.compareTo(true) == 0)
                    {
                        ouvertmesutrue++;
                    }
                }
            }
        }
        result = new Double(ouvertmesutrue);
        return result;
    }

    /**
     * renvoie le decompte non null des jours limite adhesion.
     * 
     * @param lesTypesImpots
     * @return double
     */
    public Double calculerDecompteJourLimiteAdhesion(List lesTypesImpots)
    {
        Double result = new Double(0);
        int nbejourlim = 0;

        if (lesTypesImpots != null && !lesTypesImpots.isEmpty())
        {
            for (int i = 0; i < lesTypesImpots.size(); i++)
            {
                TypeImpot unTypeImpot = (TypeImpot) lesTypesImpots.get(i);
                if (unTypeImpot != null)
                {
                    Date ladateRes = unTypeImpot.getJourLimiteAdhesion();
                    if (ladateRes != null)
                    {
                        nbejourlim++;
                    }
                }
            }
        }

        result = new Double(nbejourlim);
        return result;
    }

    /**
     * renvoie la moyenne des codes impots non nulls.
     * 
     * @param lesTypesImpots
     * @return big decimal
     */
    public BigDecimal calculerMoyenneNonNullCodeImpot(List lesTypesImpots)
    {
        double resultDef = 0.00;
        BigDecimal resultinter = BigDecimal.valueOf(resultDef);
        double nbecodeimpot = 0;
        double size = lesTypesImpots.size();

        if (lesTypesImpots != null && !lesTypesImpots.isEmpty())
        {
            for (int i = 0; i < lesTypesImpots.size(); i++)
            {
                TypeImpot unTypeImpot = (TypeImpot) lesTypesImpots.get(i);
                if (unTypeImpot != null)
                {
                    Long longRes = unTypeImpot.getCodeImpot();
                    if (longRes != null)
                    {
                        nbecodeimpot = nbecodeimpot + longRes.doubleValue();
                    }
                }
            }
        }
        resultinter = BigDecimal.valueOf(nbecodeimpot / size);
        return resultinter;
    }

    /**
     * renvoie les pourcentages des types impot ouverts à la mensu non nulls.
     * 
     * @param lesTypesImpots
     * @return big decimal
     */
    public BigDecimal calculerPourcentageEstOuvertALaMensualisation(List lesTypesImpots)
    {
        double resDef = 0.00;
        BigDecimal result = BigDecimal.valueOf(resDef);
        double ouvertmesutrue = 0;
        double size = new Double(lesTypesImpots.size());

        if (lesTypesImpots != null && !lesTypesImpots.isEmpty())
        {
            for (int i = 0; i < lesTypesImpots.size(); i++)
            {
                TypeImpot unTypeImpot = (TypeImpot) lesTypesImpots.get(i);
                if (unTypeImpot != null)
                {
                    Boolean lebooleenRes = unTypeImpot.getEstOuvertALaMensualisation();
                    if (lebooleenRes.compareTo(true) == 0)
                    {
                        ouvertmesutrue++;
                    }
                }
            }
        }

        result = BigDecimal.valueOf(ouvertmesutrue / size * 100);
        return result;
    }

    /**
     * renvoie les pourcentages des jours limites adhesion non nulls.
     * 
     * @param lesTypesImpots
     * @return big decimal
     */
    public BigDecimal calculerPourcentageJourLimiteAdhesion(List lesTypesImpots)
    {
        BigDecimal result = new BigDecimal("0.0");
        int nbejourlim = 0;

        if (lesTypesImpots != null && !lesTypesImpots.isEmpty())
        {
            for (int i = 0; i < lesTypesImpots.size(); i++)
            {
                TypeImpot unTypeImpot = (TypeImpot) lesTypesImpots.get(i);
                if (unTypeImpot != null)
                {
                    Date ladateRes = unTypeImpot.getJourLimiteAdhesion();
                    if (ladateRes != null)
                    {
                        nbejourlim++;
                    }
                }
            }
            result = BigDecimal.valueOf((long) (nbejourlim / lesTypesImpots.size()) * 100);
        }
        return result;
    }

}
