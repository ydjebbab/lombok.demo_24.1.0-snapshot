package fr.gouv.finances.cp.dmo.entite;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DocumentJoint.class)
public abstract class DocumentJoint_ {

	public static volatile SingularAttribute<DocumentJoint, FichierJoint> leFichierJoint;
	public static volatile SingularAttribute<DocumentJoint, String> identifiant;
	public static volatile SingularAttribute<DocumentJoint, Long> id;
	public static volatile SingularAttribute<DocumentJoint, Integer> version;

}

