/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;

import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.webflow.FormMemento;
import fr.gouv.finances.lombok.webflow.Memento;
import fr.gouv.finances.lombok.webflow.MementoOriginator;

/**
 * Class GestionAvisImpositionForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
public class GestionAvisImpositionForm implements Serializable, MementoOriginator
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** contribuable. */
    private ContribuablePar contribuable;

    /**
     * Constructeur de la classe GestionAvisImpositionForm.java
     */
    public GestionAvisImpositionForm()
    {
        super(); // Raccord de constructeur auto-généré

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return memento
     * @see fr.gouv.finances.lombok.webflow.MementoOriginator#createMemento()
     */
    public Memento createMemento()
    {
        FormMemento formMemento = new FormMemento(this);
        return formMemento;
    }

    /**
     * Gets the contribuable.
     *
     * @return the contribuable
     */
    public ContribuablePar getContribuable()
    {
        return contribuable;
    }

    /**
     * Sets the contribuable.
     *
     * @param contribuable the new contribuable
     */
    public void setContribuable(ContribuablePar contribuable)
    {
        this.contribuable = contribuable;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param memento
     * @param level
     * @see fr.gouv.finances.lombok.webflow.MementoOriginator#setMemento(fr.gouv.finances.lombok.webflow.Memento, int)
     */
    public void setMemento(Memento memento, int level)
    {
        // Relecture du formulaire original dans le memento
        GestionAvisImpositionForm formulaireOriginal =
            (GestionAvisImpositionForm) ((FormMemento) memento).getObjectDeepClone();

        // Un seul niveau de sauvegarde
        try
        {
            PropertyUtils.copyProperties(this.contribuable, formulaireOriginal.getContribuable());
        }
        catch (IllegalAccessException exception)
        {
            throw new ProgrammationException(exception);

        }
        catch (InvocationTargetException exception)
        {
            throw new ProgrammationException(exception);
        }
        catch (NoSuchMethodException e)
        {
            throw new ProgrammationException(e);
        }

    }

}
