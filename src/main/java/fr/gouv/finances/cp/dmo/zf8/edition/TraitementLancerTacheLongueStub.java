/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : TraitementLancerTacheLongueStub.java
 *
 */
package fr.gouv.finances.cp.dmo.zf8.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import fr.gouv.finances.cp.dmo.editionbean.EditionContribuable;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunStubImpl;

/**
 * Class TraitementLancerTacheLongueStub DGFiP.
 *
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class TraitementLancerTacheLongueStub extends AbstractServiceEditionCommunStubImpl
{
    /**
     * Constructeur
     */
    public TraitementLancerTacheLongueStub()
    {
        super();
    }

    /**
     * methode Creates the bean collection : DGFiP.
     *
     * @return collection
     */
    public static Collection createBeanCollection()
    {
        Collection<EditionContribuable> lesEdContribuables = new ArrayList<>();
        // rajout donnée fictive
        EditionContribuable uneEdContribuable = new EditionContribuable();
        uneEdContribuable.setIdentifiant("identifiant");
        uneEdContribuable.setNom("nom");
        uneEdContribuable.setPrenom("prenom");
        uneEdContribuable.setSolde("solde");
        uneEdContribuable.setTypeContribuable("TypeContribuable");
        uneEdContribuable.setNombreDeContriTraites("5");
        lesEdContribuables.add(uneEdContribuable);
        return lesEdContribuables;
    }

    /**
     * Accesseur de l attribut description.
     *
     * @param parametresEdition param
     * @return description
     */
    public String getDescription(Map parametresEdition)
    {
        return "tot3";

    }
}
