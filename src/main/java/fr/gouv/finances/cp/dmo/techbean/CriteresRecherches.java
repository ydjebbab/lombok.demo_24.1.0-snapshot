/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : CriteresRecherches.java
 *
 */
package fr.gouv.finances.cp.dmo.techbean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Class CriteresRecherches
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class CriteresRecherches extends BaseTechBean
{
    /** Constant : MAXFETCH_ONE_TIME. */
    public static final int MAXFETCH_ONE_TIME = 65000;

    /** Constant : RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT. */
    public static final int RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT = 1;

    /** Constant : RECHERCHE_CONTRIBUABLE_PAR_MAIL. */
    public static final int RECHERCHE_CONTRIBUABLE_PAR_MAIL = 2;

    /** Constant : RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT_ET_MAIL. */
    public static final int RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT_ET_MAIL = 3;

    /** Constant : RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT_OU_MAIL. */
    public static final int RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT_OU_MAIL = 4;

    /** Constant : RECHERCHE_CONTRIBUABLE_PAR_AVIS_IMPOSITION. */
    public static final int RECHERCHE_CONTRIBUABLE_PAR_AVIS_IMPOSITION = 5;

    /** Constant : ORDRE_AUCUN. */
    public static final int ORDRE_AUCUN = 1;

    /** Constant : ORDRE_CROISSANT. */
    public static final int ORDRE_CROISSANT = 2;

    /** Constant : ORDRE_DECROISSANT. */
    public static final int ORDRE_DECROISSANT = 3;

    /** Constant : TRI_PAR_IDENTIFIANT. */
    public static final int TRI_PAR_IDENTIFIANT = 1;

    /** Constant : TRI_PAR_CODE_POSTAL. */
    public static final int TRI_PAR_CODE_POSTAL = 2;

    /** Constant : TRI_PAR_LOCALITE. */
    public static final int TRI_PAR_LOCALITE = 3;

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = -2814338910312051298L;

    /** Constant : tid_typesderecherche. */
    private static final int[] tid_typesderecherche =
        {RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT, RECHERCHE_CONTRIBUABLE_PAR_MAIL,
                RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT_ET_MAIL, RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT_OU_MAIL,
                RECHERCHE_CONTRIBUABLE_PAR_AVIS_IMPOSITION};

    /** Constant : tname_typesderecherche. */
    private static final String[] tname_typesderecherche =
        {"Recherche par l'identifiant", "Recherche par l'adresse de messagerie",
                "Recherche par l'identifiant ET l'adresse messagerie",
                "Recherche par l'identifiant OU l'adresse messagerie", "Recherche par avis d'imposition"};

    /** Constant : tid_typesdecivilite. */
    private static final int[] tid_typesdecivilite = {1, 2, 3};

    /** Constant : tname_typesdecivilite. */
    private static final String[] tname_typesdecivilite = {"Madame", "Mademoiselle", "Monsieur"};

    /** Constant : tid_typesdeordredetri. */
    private static final int[] tid_typesdeordredetri = {ORDRE_AUCUN, ORDRE_CROISSANT, ORDRE_DECROISSANT};

    /** Constant : tname_typesdeordredetri. */
    private static final String[] tname_typesdeordredetri = {"Aucun", "Croissant", "Décroissant"};

    /** Constant : tid_typesdecriteredetri. */
    private static final int[] tid_typesdecriteredetri = {TRI_PAR_IDENTIFIANT, TRI_PAR_CODE_POSTAL, TRI_PAR_LOCALITE};

    /** Constant : tname_typesdecriteredetri. */
    private static final String[] tname_typesdecriteredetri = {"Code utilisateur", "Code postal", "Localité"};

    /** identifiant. */
    private String identifiant;

    /** password. */
    private String password;

    /** reference avis. */
    private String referenceAvis;

    /** adresse mail. */
    private String adresseMail;

    /** type recherche id. */
    private Integer typeRechercheId;

    /** type tri id. */
    private int typeTriId;

    /** type critere de tri id. */
    private int typeCritereDeTriId;

    /** type nb lignes id. */
    private int typeNbLignesId;

    /** liste contribuable_p_. */
    private String listeContribuable_p_;

    /** types civilite id. */
    private List<String> typesCiviliteId;

    /** code impot. */
    private Long codeImpot;

    /** type contri part. */
    private Boolean typeContriPart;

    /**
     * Instanciation de criteres recherches.
     */
    public CriteresRecherches()
    {
        super();
        this.typesCiviliteId = new ArrayList<>();

        // positionnement du type de recherche par défaut
        this.typeRechercheId = Integer.valueOf(1);

        // Postionnement des checkbox par défaut
        this.typesCiviliteId.add("1");
        this.typesCiviliteId.add("2");
        this.typesCiviliteId.add("3");

        // Type de critère de tri par défaut
        this.typeCritereDeTriId = 1;

        this.typeContriPart = Boolean.FALSE;
    }

    /**
     * Accesseur de l attribut types contri part.
     * 
     * @return types contri part
     */
    public static final Map getTypesContriPart()
    {
        Map<String, String> uneTreeMap = new TreeMap<>();

        uneTreeMap.put("1", "Oui");
        uneTreeMap.put("0", "Non");
        return uneTreeMap;
    }

    /**
     * Accesseur de l attribut typesdecivilite.
     * 
     * @return typesdecivilite
     */
    public static final Map getTypesdecivilite()
    {
        Map<Integer, String> uneTreeMap = new HashMap<>();

        for (int i = 0; i < tid_typesdecivilite.length; i++)
        {
            uneTreeMap.put(Integer.valueOf(tid_typesdecivilite[i]), tname_typesdecivilite[i]);
        }
        return uneTreeMap;
    }

    /**
     * Accesseur de l attribut typesdecriteredetri.
     * 
     * @return typesdecriteredetri
     */
    public static final Map getTypesdecriteredetri()
    {
        Map<Integer, String> uneTreeMap = new TreeMap<>();

        for (int i = 0; i < tid_typesdecriteredetri.length; i++)
        {
            uneTreeMap.put(Integer.valueOf(tid_typesdecriteredetri[i]), tname_typesdecriteredetri[i]);
        }
        return uneTreeMap;
    }

    /**
     * Accesseur de l attribut typesdenblignes.
     * 
     * @return typesdenblignes
     */
    public static final Map getTypesdenblignes()
    {
        Map<Integer, String> uneTreeMap = new TreeMap<>();

        for (int i = 1; i < 11; i++)
        {
            uneTreeMap.put(Integer.valueOf(i * 5000), "" + (i * 5000));
        }
        return uneTreeMap;
    }

    /**
     * Accesseur de l attribut typesderecherche.
     * 
     * @return typesderecherche
     */
    public static final Map getTypesderecherche()
    {
        Map<Integer, String> uneTreeMap = new TreeMap<>();

        for (int i = 0; i < tid_typesderecherche.length; i++)
        {
            uneTreeMap.put(Integer.valueOf(tid_typesderecherche[i]), tname_typesderecherche[i]);
        }
        return uneTreeMap;
    }

    /**
     * Accesseur de l attribut typesordredetri.
     * 
     * @return typesordredetri
     */
    public static final Map getTypesordredetri()
    {
        Map<Integer, String> uneTreeMap = new TreeMap<>();

        for (int i = 0; i < tid_typesdeordredetri.length; i++)
        {
            uneTreeMap.put(Integer.valueOf(tid_typesdeordredetri[i]), tname_typesdeordredetri[i]);
        }
        return uneTreeMap;
    }

    /**
     * Accesseur de l attribut adresse mail.
     * 
     * @return adresse mail
     */
    public String getAdresseMail()
    {
        return adresseMail;
    }

    /**
     * Accesseur de l attribut code impot.
     * 
     * @return code impot
     */
    public Long getCodeImpot()
    {
        return codeImpot;
    }

    /**
     * Accesseur de l attribut identifiant.
     * 
     * @return identifiant
     */
    public String getIdentifiant()
    {
        return identifiant;
    }

    /**
     * Accesseur de l attribut liste contribuable_p_.
     * 
     * @return liste contribuable_p_
     */
    public String getListeContribuable_p_()
    {
        return listeContribuable_p_;
    }

    /**
     * Accesseur de l attribut maximum lignes retournees.
     * 
     * @return maximum lignes retournees
     */
    public int getMaximumLignesRetournees()
    {
        int result = MAXFETCH_ONE_TIME;
        if (this.getTypeNbLignesId() > 0 && this.getTypeNbLignesId() < CriteresRecherches.MAXFETCH_ONE_TIME)
        {
            result = this.getTypeNbLignesId();
        }
        return result;
    }

    /**
     * Accesseur de l attribut password.
     * 
     * @return password
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * Accesseur de l attribut reference avis.
     * 
     * @return reference avis
     */
    public String getReferenceAvis()
    {
        return referenceAvis;
    }

    /**
     * Accesseur de l attribut type contri part.
     * 
     * @return type contri part
     */
    public Boolean getTypeContriPart()
    {
        return typeContriPart;
    }

    /**
     * Accesseur de l attribut type critere de tri id.
     * 
     * @return type critere de tri id
     */
    public int getTypeCritereDeTriId()
    {
        return typeCritereDeTriId;
    }

    /**
     * Accesseur de l attribut type nb lignes id.
     * 
     * @return type nb lignes id
     */
    public int getTypeNbLignesId()
    {
        return typeNbLignesId;
    }

    /**
     * Accesseur de l attribut type recherche id.
     * 
     * @return type recherche id
     */
    public Integer getTypeRechercheId()
    {
        return typeRechercheId;
    }

    /**
     * Accesseur de l attribut types civilite id.
     * 
     * @return types civilite id
     */
    public List<String> getTypesCiviliteId()
    {
        return typesCiviliteId;
    }

    /**
     * Accesseur de l attribut type tri id.
     * 
     * @return type tri id
     */
    public int getTypeTriId()
    {
        return typeTriId;
    }

    /**
     * Modificateur de l attribut adresse mail.
     * 
     * @param adressemail le nouveau adresse mail
     */
    public void setAdresseMail(String adressemail)
    {
        this.adresseMail = adressemail;
    }

    /**
     * Modificateur de l attribut code impot.
     * 
     * @param codeImpot le nouveau code impot
     */
    public void setCodeImpot(Long codeImpot)
    {
        this.codeImpot = codeImpot;
    }

    /**
     * Modificateur de l attribut identifiant.
     * 
     * @param identifiant le nouveau identifiant
     */
    public void setIdentifiant(String identifiant)
    {
        this.identifiant = identifiant;
    }

    /**
     * Modificateur de l attribut liste contribuable_p_.
     * 
     * @param listecontribuable_p_ le nouveau liste contribuable_p_
     */
    public void setListeContribuable_p_(String listecontribuable_p_)
    {
        this.listeContribuable_p_ = listecontribuable_p_;
    }

    /**
     * Modificateur de l attribut password.
     * 
     * @param password le nouveau password
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * Modificateur de l attribut reference avis.
     * 
     * @param referenceavis le nouveau reference avis
     */
    public void setReferenceAvis(String referenceavis)
    {
        this.referenceAvis = referenceavis;
    }

    /**
     * Modificateur de l attribut type contri part.
     * 
     * @param typeContriPart le nouveau type contri part
     */
    public void setTypeContriPart(Boolean typeContriPart)
    {
        this.typeContriPart = typeContriPart;
    }

    /**
     * Modificateur de l attribut type critere de tri id.
     * 
     * @param typecriteredetriid le nouveau type critere de tri id
     */
    public void setTypeCritereDeTriId(int typecriteredetriid)
    {
        this.typeCritereDeTriId = typecriteredetriid;
    }

    /**
     * Modificateur de l attribut type nb lignes id.
     * 
     * @param typenblignesid le nouveau type nb lignes id
     */
    public void setTypeNbLignesId(int typenblignesid)
    {
        this.typeNbLignesId = typenblignesid;
    }

    /**
     * Modificateur de l attribut type recherche id.
     * 
     * @param typeRechercheId le nouveau type recherche id
     */
    public void setTypeRechercheId(Integer typeRechercheId)
    {
        this.typeRechercheId = typeRechercheId;
    }

    /**
     * Modificateur de l attribut types civilite id.
     * 
     * @param typesciviliteid le nouveau types civilite id
     */
    public void setTypesCiviliteId(List<String> typesciviliteid)
    {
        this.typesCiviliteId = typesciviliteid;
    }

    /**
     * Modificateur de l attribut type tri id.
     * 
     * @param typetriid le nouveau type tri id
     */
    public void setTypeTriId(int typetriid)
    {
        this.typeTriId = typetriid;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        String lss = System.getProperty(";");
        StringBuilder rc = new StringBuilder();
        rc.append("identifiant = ");
        rc.append(this.getIdentifiant());
        rc.append(lss);
        rc.append("adressemail = ");
        rc.append(this.getAdresseMail());
        rc.append(lss);
        rc.append("typerechercheid = ");
        rc.append(this.typeRechercheId);
        rc.append(lss);
        rc.append("typetriid = ");
        rc.append(this.getTypeTriId());
        rc.append(lss);
        rc.append("typecriteredetriid = ");
        rc.append(this.getTypeCritereDeTriId());
        rc.append(lss);
        rc.append("typenblignesid = ");
        rc.append(this.getTypeNbLignesId());
        rc.append(lss);

        for (int i = 0; i < this.getTypesCiviliteId().size(); i++)
        {
            rc.append("Typesciviliteid[" + i + "] = ");
            rc.append(this.getTypesCiviliteId().get(i));
            rc.append(lss);
        }

        return rc.toString();
    }

}
