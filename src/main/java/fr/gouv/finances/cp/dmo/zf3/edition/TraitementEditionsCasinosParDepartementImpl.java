/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementEditionsCasinosParDepartementImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import java.util.Collection;
import java.util.Map;

import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;

/**
 * Class TraitementEditionsCasinosParDepartementImpl DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementEditionsCasinosParDepartementImpl extends AbstractServiceEditionCommunImpl
{

    /**
     * Constructeur
     */
    public TraitementEditionsCasinosParDepartementImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return collection
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerDatasource(java.util.Map)
     */
    @Override
    public Collection creerDatasource(Map parametresEdition)
    {
        return TraitementEditionsCasinosParDepartementStub.createBeanCollection();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {

        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {

        return parametresEdition;
    }

}
