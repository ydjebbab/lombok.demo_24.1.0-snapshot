/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.batch;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.dao.IChargementReferenceDao;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ExploitationException;

/**
 * Batch TraitementChargementDonneesDeReferenceImpl
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Service("traitementchargementdonneesdereference")
@Profile("batch")
@Lazy(true)
public class TraitementChargementDonneesDeReferenceImpl extends ServiceBatchCommunImpl
{

    /**
     * fichiercodepays_entree.
     */
    private String fichiercodepays_entree = null;

    /**
     * fichiercodepays_sortie.
     */
    private String fichiercodepays_sortie = null;

    /**
     * fichiertypeimpot_entree.
     */
    private String fichiertypeimpot_entree = null;

    /**
     * fichiertypeimpot_sortie.
     */
    private String fichiertypeimpot_sortie = null;

    /**
     * fichiercivilite_entree.
     */
    private String fichiercivilite_entree = null;

    /**
     * fichiersituationfami_entree.
     */
    private String fichiersituationfami_entree = null;

    @Autowired()
    @Qualifier("chargementReferenceDao")
    private IChargementReferenceDao chargementreferencedao;

    public TraitementChargementDonneesDeReferenceImpl()
    {
        super();
    }

    @Value("${traitementchargementdonneesdereference.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }

    public void chargerCiviliteXmlToBase()
    {
        log.warn("Chargement des civilites à partir du fichier " + this.fichiercivilite_entree);
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource res1 = drl.getResource(this.fichiercivilite_entree);
        InputStream input = null;
        if (!res1.exists())
        {
            throw new ExploitationException(res1.getFilename() + " introuvable");
        }
        try
        {
            SAXReader sar = new SAXReader(false);
            Document xmldoc = null;
            input = res1.getInputStream();
            xmldoc = sar.read(input);
            // TO catch exception
            chargementreferencedao.loadCiviliteFromXml(xmldoc);
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }
        catch (IOException e)
        {
            log.warn("Erreur de lecture du fichier : " + this.fichiercivilite_entree);
            throw ApplicationExceptionTransformateur.transformer("Erreur lors de la lecture du fichier : " + this.fichiercivilite_entree,
                e);
        }
        catch (DocumentException e)
        {
            log.warn("Erreur de traitement du fichier : " + this.fichiercivilite_entree);
            throw ApplicationExceptionTransformateur.transformer("Erreur lors du traitement du fichier : " + this.fichiercivilite_entree,
                e);
        }
        finally
        {
            try
            {
                if (input != null)
                {
                    // ferme le stream
                    input.close();
                }
            }
            catch (IOException ex)
            {
                log.warn("Impossible de fermer le fichier", ex);
            }
        }
    }

    public void chargerCodePaysXmlToBase()
    {
        log.info("chargement des codes pays à partir du fichier " + this.fichiercodepays_entree);
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource res1 = drl.getResource(this.fichiercodepays_entree);
        InputStream input = null;
        if (!res1.exists())
        {
            throw new ExploitationException(res1.getFilename() + " introuvable");
        }
        try
        {
            SAXReader sar = new SAXReader(false);
            Document xmldoc = null;
            input = res1.getInputStream();
            xmldoc = sar.read(input);
            chargementreferencedao.loadPaysFromXml(xmldoc);
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }
        catch (DocumentException e)
        {
            log.warn("Erreur de traitement du fichier : " + this.fichiercodepays_entree);
            throw ApplicationExceptionTransformateur.transformer("Erreur lors du traitement du fichier : " + this.fichiercodepays_entree,
                e);
        }
        catch (IOException e)
        {
            log.warn("Erreur de lecture du fichier : " + this.fichiercodepays_entree);
            throw ApplicationExceptionTransformateur.transformer("Erreur lors de la lecture du fichier : " + this.fichiercodepays_entree,
                e);
        }
        finally
        {
            try
            {
                if (input != null)
                {
                    // ferme le stream
                    input.close();
                }
            }
            catch (IOException ex)
            {
                log.warn("Impossible de fermer le fichier", ex);
            }
        }
    }

    /**
     * methode Charger situation familiale xml to base
     */
    public void chargerSituationFamilialeXmlToBase()
    {
        log.warn("chargement des situations familiales à partir du fichier " + this.fichiersituationfami_entree);
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource res1 = drl.getResource(this.fichiersituationfami_entree);
        InputStream input = null;
        if (!res1.exists())
        {
            throw new ExploitationException(res1.getFilename() + " introuvable");
        }
        try
        {
            SAXReader sar = new SAXReader(false);
            Document xmldoc = null;
            input = res1.getInputStream();
            xmldoc = sar.read(input);
            // TO catch exception
            chargementreferencedao.loadSituationFamiFromXml(xmldoc);
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }
        catch (IOException e)
        {
            log.warn("Erreur de lecture du fichier : " + this.fichiersituationfami_entree);
            throw ApplicationExceptionTransformateur
                .transformer("Erreur lors de la lecture du fichier : " + this.fichiersituationfami_entree, e);
        }
        catch (DocumentException e)
        {
            log.warn("Erreur de traitement du fichier : " + this.fichiersituationfami_entree);
            throw ApplicationExceptionTransformateur
                .transformer("Erreur lors du traitement du fichier : " + this.fichiersituationfami_entree, e);
        }
        finally
        {
            try
            {
                if (input != null)
                {
                    // ferme le stream
                    input.close();
                }
            }
            catch (IOException ex)
            {
                log.warn("Impossible de fermer le fichier", ex);
            }
        }
    }

    /**
     * methode Charger type impot xml to base
     */
    public void chargerTypeImpotXmlToBase()
    {
        log.warn("chargement des types impots à partir du fichier " + this.fichiertypeimpot_entree);
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource res1 = drl.getResource(this.fichiertypeimpot_entree);
        InputStream input = null;
        if (!res1.exists())
        {
            throw new ExploitationException(res1.getFilename() + " introuvable");
        }
        try
        {
            SAXReader sar = new SAXReader(false);
            Document xmldoc = null;
            input = res1.getInputStream();
            xmldoc = sar.read(input);
            // TO catch exception
            chargementreferencedao.loadTypeImpotFromXml(xmldoc);
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }
        catch (IOException e)
        {
            log.warn("Erreur de lecture du fichier : " + this.fichiertypeimpot_entree);
            throw ApplicationExceptionTransformateur.transformer("Erreur lors de la lecture du fichier : " + this.fichiertypeimpot_entree,
                e);
        }
        catch (DocumentException e)
        {
            log.warn("Erreur de traitement du fichier : " + this.fichiertypeimpot_entree);
            throw ApplicationExceptionTransformateur.transformer("Erreur lors du traitement du fichier : " + this.fichiertypeimpot_entree,
                e);
        }
        finally
        {
            try
            {
                if (input != null)
                {
                    // ferme le stream
                    input.close();
                }
            }
            catch (IOException ex)
            {
                log.warn("Impossible de fermer le fichier", ex);
            }
        }
    }

    /**
     * methode Decharger code pays from base to xml
     */
    public void dechargerCodePaysFromBaseToXml() // pour pays : à remonter dans commun
    {
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource res1 = drl.getResource(this.fichiercodepays_sortie);
        XMLWriter xmlw = null;
        try
        {
            Document xmldoc = null;
            xmldoc = chargementreferencedao.unloadPaysToXml();
            OutputFormat outputformat = new OutputFormat("  ", true);
            outputformat.setEncoding("ISO-8859-1");
            xmlw = new XMLWriter(new FileWriter(res1.getFile()), outputformat);
            xmlw.write(xmldoc);
            xmlw.close();
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }
        catch (IOException e)
        {
            log.warn("Erreur de lecture du fichier : " + this.fichiercodepays_sortie);
            throw ApplicationExceptionTransformateur.transformer("Erreur lors de la lecture du fichier : " + this.fichiercodepays_sortie,
                e);
        }
        finally
        {
            try
            {
                if (xmlw != null)
                {
                    // ferme le writer
                    xmlw.close();
                }
            }
            catch (IOException ex)
            {
                log.warn("Impossible de fermer le fichier", ex);
            }
        }
    }

    /**
     * methode Decharger type impot from base to xml
     */
    public void dechargerTypeImpotFromBaseToXml()
    {
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource res1 = drl.getResource(this.fichiertypeimpot_sortie);
        XMLWriter xmlw = null;
        try
        {
            Document xmldoc = null;
            xmldoc = chargementreferencedao.unloadTypeImpotToXml();
            OutputFormat outputformat = new OutputFormat("  ", true);
            outputformat.setEncoding("ISO-8859-1");
            xmlw = new XMLWriter(new FileWriter(res1.getFile()), outputformat);
            xmlw.write(xmldoc);
            xmlw.close();
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer(e);
        }
        catch (IOException e)
        {
            log.warn("Erreur de lecture du fichier : " + this.fichiertypeimpot_sortie);
            throw ApplicationExceptionTransformateur.transformer("Erreur lors de la lecture du fichier : " + this.fichiertypeimpot_sortie,
                e);
        }
        finally
        {
            try
            {
                if (xmlw != null)
                {
                    // ferme le writer
                    xmlw.close();
                }
            }
            catch (IOException ex)
            {
                log.warn("Impossible de fermer le fichier", ex);
            }
        }
    }

    /**
     * Accesseur de l attribut chargementreferencedao.
     *
     * @return chargementreferencedao
     */
    public IChargementReferenceDao getChargementreferencedao()
    {
        return chargementreferencedao;
    }

    /**
     * Accesseur de l attribut fichiercivilite_entree.
     *
     * @return fichiercivilite_entree
     */
    public String getFichiercivilite_entree()
    {
        return fichiercivilite_entree;
    }

    /**
     * Accesseur de l attribut fichiercodepays_entree.
     *
     * @return fichiercodepays_entree
     */
    public String getFichiercodepays_entree()
    {
        return fichiercodepays_entree;
    }

    /**
     * Accesseur de l attribut fichiercodepays_sortie.
     *
     * @return fichiercodepays_sortie
     */
    public String getFichiercodepays_sortie()
    {
        return fichiercodepays_sortie;
    }

    /**
     * Accesseur de l attribut fichiersituationfami_entree.
     *
     * @return fichiersituationfami_entree
     */
    public String getFichiersituationfami_entree()
    {
        return fichiersituationfami_entree;
    }

    /**
     * Accesseur de l attribut fichiertypeimpot_entree.
     *
     * @return fichiertypeimpot_entree
     */
    public String getFichiertypeimpot_entree()
    {
        return fichiertypeimpot_entree;
    }

    /**
     * Accesseur de l attribut fichiertypeimpot_sortie.
     *
     * @return fichiertypeimpot_sortie
     */
    public String getFichiertypeimpot_sortie()
    {
        return fichiertypeimpot_sortie;
    }

    /**
     * Modificateur de l attribut chargementreferencedao.
     *
     * @param chargementreferencedao le nouveau chargementreferencedao
     */
    public void setChargementreferencedao(IChargementReferenceDao chargementreferencedao)
    {
        this.chargementreferencedao = chargementreferencedao;
    }

    /**
     * Modificateur de l attribut fichiercivilite_entree.
     *
     * @param fichiercivilite_entree le nouveau fichiercivilite_entree
     */
    @Value("${traitementchargementdonneesdereference.fichier.civilite}")
    public void setFichiercivilite_entree(String fichiercivilite_entree)
    {
        this.fichiercivilite_entree = fichiercivilite_entree;
    }

    /**
     * Modificateur de l attribut fichiercodepays_entree.
     *
     * @param fichiercodepays_entree le nouveau fichiercodepays_entree
     */
    @Value("${traitementchargementdonneesdereference.fichier.pays}")
    public void setFichiercodepays_entree(String fichiercodepays_entree)
    {
        this.fichiercodepays_entree = fichiercodepays_entree;
    }

    /**
     * Modificateur de l attribut fichiercodepays_sortie.
     *
     * @param fichiercodepays_sortie le nouveau fichiercodepays_sortie
     */
    public void setFichiercodepays_sortie(String fichiercodepays_sortie)
    {
        this.fichiercodepays_sortie = fichiercodepays_sortie;
    }

    /**
     * Modificateur de l attribut fichiersituationfami_entree.
     *
     * @param fichiersituationfami_entree le nouveau fichiersituationfami_entree
     */
    @Value("${traitementchargementdonneesdereference.fichier.situationfami}")
    public void setFichiersituationfami_entree(String fichiersituationfami_entree)
    {
        this.fichiersituationfami_entree = fichiersituationfami_entree;
    }

    /**
     * Modificateur de l attribut fichiertypeimpot_entree.
     *
     * @param fichiertypeimpot_entree le nouveau fichiertypeimpot_entree
     */
    @Value("${traitementchargementdonneesdereference.fichier.typeimpot}")
    public void setFichiertypeimpot_entree(String fichiertypeimpot_entree)
    {
        this.fichiertypeimpot_entree = fichiertypeimpot_entree;
    }

    /**
     * Modificateur de l attribut fichiertypeimpot_sortie.
     *
     * @param fichiertypeimpot_sortie le nouveau fichiertypeimpot_sortie
     */
    public void setFichiertypeimpot_sortie(String fichiertypeimpot_sortie)
    {
        this.fichiertypeimpot_sortie = fichiertypeimpot_sortie;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        Map lesArgumentsPasses = this.getArguments();
        // avec meme nom existe dans les arguments passés
        if (lesArgumentsPasses != null)
        {
            if (lesArgumentsPasses.containsKey("fichier.pays"))
            {
                this.setFichiercodepays_entree((String) lesArgumentsPasses.get("fichier.pays"));
                log.debug("valeur codepays_entree:" + (String) lesArgumentsPasses.get("fichier.pays"));
            }
            if (lesArgumentsPasses.containsKey("fichier.typeimpot"))
            {
                this.setFichiertypeimpot_entree((String) lesArgumentsPasses.get("fichier.typeimpot"));
                log.debug("valeur typeimpot_entree:" + (String) lesArgumentsPasses.get("fichier.typeimpot"));
            }
            if (lesArgumentsPasses.containsKey("fichier.civilite"))
            {
                this.setFichiercivilite_entree((String) lesArgumentsPasses.get("fichier.civilite"));
                log.debug("valeur civilite_entree:" + (String) lesArgumentsPasses.get("fichier.civilite"));
            }
            if (lesArgumentsPasses.containsKey("fichier.situationfami"))
            {
                this.setFichiersituationfami_entree((String) lesArgumentsPasses.get("fichier.situationfami"));
                log.debug("valeur situationfami_entree:" + (String) lesArgumentsPasses.get("fichier.situationfami"));
            }
        }
        log.info("valeur codepays_entree:" + fichiercodepays_entree);
        log.info("valeur typeimpot_entree:" + fichiertypeimpot_entree);
        log.info("valeur civilite_entree:" + fichiercivilite_entree);
        log.info("valeur situationfami_entree:" + fichiersituationfami_entree);
        if (fichiercodepays_entree != null)
        {
            this.chargerCodePaysXmlToBase();
        }
        if (fichiertypeimpot_entree != null)
        {
            this.chargerTypeImpotXmlToBase();
        }
        if (fichiercivilite_entree != null)
        {
            this.chargerCiviliteXmlToBase();
        }
        if (fichiercivilite_entree != null)
        {
            this.chargerSituationFamilialeXmlToBase();
        }
        if (fichiercodepays_sortie != null)
        {
            this.dechargerCodePaysFromBaseToXml();
        }
        if (fichiertypeimpot_sortie != null)
        {
            this.dechargerTypeImpotFromBaseToXml();
        }
    }

}
