package fr.gouv.finances.cp.dmo.entite;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AdresseContribuable.class)
public abstract class AdresseContribuable_ {

	public static volatile SingularAttribute<AdresseContribuable, String> ligne4;
	public static volatile SingularAttribute<AdresseContribuable, String> ligne3;
	public static volatile SingularAttribute<AdresseContribuable, String> ligne2;
	public static volatile SingularAttribute<AdresseContribuable, String> ville;
	public static volatile SingularAttribute<AdresseContribuable, String> ligne1;
	public static volatile SingularAttribute<AdresseContribuable, String> codePostal;
	public static volatile SingularAttribute<AdresseContribuable, String> pays;

}

