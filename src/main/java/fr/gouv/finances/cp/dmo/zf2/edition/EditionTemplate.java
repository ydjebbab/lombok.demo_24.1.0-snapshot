/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.zf2.edition;

import java.awt.Image;
import java.util.Collection;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Class EditionTemplate
 */
public class EditionTemplate extends BaseTechBean
{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -4249271720264041682L;

    /** logoEntete. */
    private Image logoEntete;

    /** logoPied. */
    private Image logoPied;

    /** collection. */
    private Collection collection;

    /**
     * Instanciation de edition template.
     */
    public EditionTemplate()
    {
        super();

    }

    /**
     * Accesseur de logoEntete
     *
     * @return logoEntete
     */
    public Image getLogoEntete()
    {
        return logoEntete;
    }

    /**
     * Mutateur de logoEntete
     *
     * @param logoEntete logoEntete
     */
    public void setLogoEntete(Image logoEntete)
    {
        this.logoEntete = logoEntete;
    }

    /**
     * Accesseur de logoPied
     *
     * @return logoPied
     */
    public Image getLogoPied()
    {
        return logoPied;
    }

    /**
     * Mutateur de logoPied
     *
     * @param logoPied logoPied
     */
    public void setLogoPied(Image logoPied)
    {
        this.logoPied = logoPied;
    }

    /**
     * Accesseur de collection
     *
     * @return collection
     */
    public Collection getCollection()
    {
        return collection;
    }

    /**
     * Mutateur de collection
     *
     * @param collection collection
     */
    public void setCollection(Collection collection)
    {
        this.collection = collection;
    }

}
