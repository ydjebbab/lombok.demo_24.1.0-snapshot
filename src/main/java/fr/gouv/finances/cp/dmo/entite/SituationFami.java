/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.entite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;

/**
 * Entité SituationFami
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "SITUATIONFAMI_SIFA")
@SequenceGenerator(name = "SITUATIONFAMI_SIFA_ID_GENERATOR", sequenceName = "SEQ_SITUATIONFAMI_SIFA", allocationSize = 1)
public class SituationFami extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @XmlAttribute
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SITUATIONFAMI_SIFA_ID_GENERATOR")
    private Long id;


    @Column(name = "CODE")
    private Long code;

    @Column(nullable = false, name = "LIBELLE")
    private String libelle;

    public SituationFami()
    {
        super();
    }

    public Long getCode()
    {
        return code;
    }

    public Long getId()
    {
        return id;
    }

    public String getLibelle()
    {
        return libelle;
    }


    public void setCode(Long code)
    {
        this.code = code;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param object
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }

        if (object == null || (object.getClass() != this.getClass()))
        {
            return false;
        }

        SituationFami myObj = (SituationFami) object;

        return ((code == null ? myObj.code == null : code.equals(myObj.code)));
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int hash = hashCODEHASHINIT;
        int varCode = 0;

        varCode = (null == code ? 0 : code.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        return hash;
    }
}
