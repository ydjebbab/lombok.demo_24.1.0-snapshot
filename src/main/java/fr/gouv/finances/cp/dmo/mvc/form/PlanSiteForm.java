/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;

/**
 * Class PlanSiteForm .
 */
public class PlanSiteForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de plan site form.
     */
    public PlanSiteForm()
    {
        super();
    }
}
