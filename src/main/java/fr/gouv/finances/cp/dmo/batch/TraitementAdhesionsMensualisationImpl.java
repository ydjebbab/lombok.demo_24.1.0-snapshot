/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.batch;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.service.IContratMensuService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;

/**
 * Class TraitementAdhesionsMensualisationImpl
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Service("traitementadhesionsmensualisation")
@Profile("batch")
@Lazy(true)
public class TraitementAdhesionsMensualisationImpl extends ServiceBatchCommunImpl
{

    @Autowired
    @Qualifier("contratMensuService")
    private IContratMensuService contratMensuService;

    public TraitementAdhesionsMensualisationImpl()
    {
        super();
    }

    @Value("${traitementadhesionsmensualisation.nombatch}")
    @Override
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        log.info(">>> debut methode traiterBatch");
        traiterContratsNeVerifiantPasRG71();
        traiterContratsVerifiantRG70();
        traiterContratsNeVerifiantPasRG70();
    }

    /**
     * methode Serialize to xml
     * 
     * @param doc param
     * @param out param
     * @param aEncodingScheme param
     */
    private void serializeToXml(Document document, OutputStream outputStream, String aEncodingScheme)
    {
        OutputFormat outputformat = OutputFormat.createPrettyPrint();
        outputformat.setEncoding(aEncodingScheme);
        XMLWriter writer;
        try
        {
            writer = new XMLWriter(outputStream, outputformat);
            writer.write(document);
            writer.flush();
        }
        catch (UnsupportedEncodingException unsupportedExc)
        {
            log.warn(String.valueOf(unsupportedExc.getStackTrace()), unsupportedExc);
            throw ApplicationExceptionTransformateur.transformer("Encodage non supporté", unsupportedExc);
        }
        catch (IOException ioExc)
        {
            log.warn(String.valueOf(ioExc.getStackTrace()), ioExc);
            throw ApplicationExceptionTransformateur.transformer("Erreur d'E/S", ioExc);
        }
        finally
        {
            try
            {
                if (outputStream != null)
                {
                    // fermer le stream
                    outputStream.close();
                }
            }
            catch (IOException ex)
            {
                log.error("Impossible de fermer le fichier", ex);

            }
        }
    }

    /**
     * methode Traiter contrats ne verifiant pas r g70 : DGFiP.
     */
    private void traiterContratsNeVerifiantPasRG70()
    {
        log.info("Traitement des contrats ne vérifiant pas RG70");
        contratMensuService.supprimerContratsNeVerifiantPasRG70EtPrevenirContribuableParMail();

    }

    /**
     * methode Traiter contrats ne verifiant pas r g71 : DGFiP.
     */
    private void traiterContratsNeVerifiantPasRG71()
    {
        log.info("Report de l'année de prise d'effet pour les contrats non traités ne vérifiant pas RG71");
        contratMensuService.reporterAnneePriseEffetPourContratsNonTraitesNeVerifiantPasRG71();
    }

    /**
     * methode Traiter contrats verifiant r g70 : DGFiP.
     */
    private void traiterContratsVerifiantRG70()
    {
        log.info("Traitement des contrats non traités vérifiant RG70 ");
        Document rapport = contratMensuService.recupererRapportXmlContratsNonTraiteVerifiantRG70();
        log.info("Export des entrées pour chaque contrat d'adhésion non traité vérifiant RG70 (exportAdhesion.xml)");
        File fichierXml = new File("exportAdhesion.xml");
        FileOutputStream streamXml;

        try
        {
            streamXml = new FileOutputStream(fichierXml);
            serializeToXml(rapport, streamXml, "UTF-8");
        }
        catch (FileNotFoundException e)
        {
            log.warn(String.valueOf(e.getStackTrace()));
            throw ApplicationExceptionTransformateur.transformer("Fichier non trouvé", e);
        }
    }

}
