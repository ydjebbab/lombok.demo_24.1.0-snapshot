/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementEditionsGraphiqueContriParTypeImpotImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fr.gouv.finances.cp.dmo.service.IStatistiquesService;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;

/**
 * Class TraitementEditionsGraphiqueContriParTypeImpotImpl DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementEditionsGraphiqueContriParTypeImpotImpl extends AbstractServiceEditionCommunImpl
{

    /** statistiquesserviceso. */
    private IStatistiquesService statistiquesserviceso;

    /**
     * Constructeur
     */
    public TraitementEditionsGraphiqueContriParTypeImpotImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return collection
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerDatasource(java.util.Map)
     */
    @Override
    public Collection creerDatasource(Map parametresEdition)
    {
        // TODO code a remplacer par un appel à un service metier sur
        // this.statistiquesserviceso
        return TraitementEditionsGraphiqueContriParTypeImpotStub.createBeanCollection();
    }

    /*
     * Construit pour une édition donnée le nom de fichier (utilisé pour l'enregistrement sur le poste client)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        return null;
    }

    /*
     * Construit la collection d’objets métiers (beans) qui sera utilisée pour élaborer l’édition. Dans cette methode il
     * faut : - récupérer des paramètres passés dans la map parametresEdition et qui serviront pour l’appel du service
     * métier (numéro de département ou plage de dates par exemple) - appeler le service métier en principe un service
     * de type rechercherXxx qui renverra une collection d’objets - éventuellement retraiter la collection d’objets pour
     * la présenter sous une forme particulière si l’on a pas réussi à traiter tous les aspects de présentation (format
     * particulier d’affichage pour des valeurs d’attributs par exemple) dans le modeler d’édition lui même - retourner
     * la collection en sortie de méthode
     */

    /*
     * Construit une Map contenant les valeurs des paramètres définis dans le modèle JASPER. Pour cela on utilise les
     * donnees de la map passée en paramètre. La plupart du temps les paramètres seront des paramètres d’entête (par
     * exemple le nom de l’utilisateur) le nom des parametre renvoyes doit correspondre au noms de parametres declares
     * dans la maquette d'edition
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {
        return new HashMap();
    }

    /**
     * Accesseur de l attribut statistiquesserviceso.
     * 
     * @return statistiquesserviceso
     */
    public IStatistiquesService getStatistiquesserviceso()
    {
        return statistiquesserviceso;
    }

    /**
     * Modificateur de l attribut statistiquesserviceso.
     * 
     * @param statistiquesserviceso le nouveau statistiquesserviceso
     */
    public void setStatistiquesserviceso(IStatistiquesService statistiquesserviceso)
    {
        this.statistiquesserviceso = statistiquesserviceso;
    }

}
