package fr.gouv.finances.cp.dmo.rest.dto;

import java.io.Serializable;

/**
 * @author celinio fernandes Date: Mar 9, 2020
 */
public class CentreImpotDto implements Serializable
{
    private static final long serialVersionUID = 14564343L;

    private Long id;

    private String commune;

    private int nombreAgents;

    private boolean ouvert;

    public CentreImpotDto()
    {
        super();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getCommune()
    {
        return commune;
    }

    public void setCommune(String commune)
    {
        this.commune = commune;
    }

    public int getNombreAgents()
    {
        return nombreAgents;
    }

    public void setNombreAgents(int nombreAgents)
    {
        this.nombreAgents = nombreAgents;
    }

    public boolean isOuvert()
    {
        return ouvert;
    }

    public void setOuvert(boolean ouvert)
    {
        this.ouvert = ouvert;
    }

}
