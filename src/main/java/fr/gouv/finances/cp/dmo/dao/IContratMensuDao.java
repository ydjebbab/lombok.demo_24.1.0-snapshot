/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ContratMensuDao.java
 *
 */
package fr.gouv.finances.cp.dmo.dao;

import java.util.List;

import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa;

/**
 * Interface ContratMensuDao.
 * 
 * @author chouard-cp
 * @author CF: migration vers JPA
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
public interface IContratMensuDao extends BaseDaoJpa
{

    /**
     * methode Enregistrer comme traite contrats verifiant r g70 
     * 
     * @return int
     */
    public int saveCommeTraiteContratsVerifiantRG70();

    /**
     * methode Find contrats ne verifiant pas r g70 : DGFiP.
     * 
     * @param startRecord param
     * @param endRecord param
     * @return list
     */
    public List<ContratMensu> findContratsNeVerifiantPasRG70(final int startRecord, final int endRecord);

    /**
     * methode Find contrats non traite verifiant r g70 :
     * 
     * @param startRecord param
     * @param endRecord param
     * @return list
     */
    public List<ContratMensu> findContratsNonTraiteVerifiantRG70(final int startRecord, final int endRecord);

    /**
     * Rechercher un contratMensu par contribuable et type impot.
     * 
     * @param contribuablePar
     * @param typeimpot 
     * @return contratpour contribuable et type impot
     */
    public ContratMensu findContratpourContribuableEtTypeImpot(ContribuablePar contribuablePar, TypeImpot typeimpot);

    /**
     * Compter le nombre de contrats non traites et ne verifiant pas r g70.
     * 
     * @return nombre contrats non traite ne verifiant pas r g70
     */
    public int countNombreContratsNonTraiteNeVerifiantPasRG70();

    /**
     * Compter le nombre de contrats non traites et verifiant r g70.
     * 
     * @return nombre contrats non traite verifiant r g70
     */
    public int countNombreContratsNonTraiteVerifiantRG70();

    /**
     * methode Reporter annee prise effet pour contrats non traites ne verifiant pas r g71
     * 
     * @return integer
     */
    public Integer reporterAnneePriseEffetPourContratsNonTraitesNeVerifiantPasRG71();

    /**
     * methode Supprimer contrats ne verifiant pas r g70 
     * 
     * @return int
     */
    public int deleteContratsNeVerifiantPasRG70();
    
    
    public void saveContratMensualisation(final ContratMensu contratMensu);
    
}
