/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementEditionsAgregAvisParTypeImpotStub.java
 *
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunStubImpl;

/**
 * Class TraitementEditionsAgregAvisParTypeImpotStub DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class TraitementEditionsAgregAvisParTypeImpotStub extends AbstractServiceEditionCommunStubImpl
{

    /**
     * Constructeur
     */
    public TraitementEditionsAgregAvisParTypeImpotStub()
    {
        super();
    }

}
