/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * 
 *
 * fichier : TestPopulationServiceImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.dao.ITestPopulationDao;
import fr.gouv.finances.cp.dmo.entite.VilleTest;
import fr.gouv.finances.cp.dmo.service.ITestPopulationService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Class TestPopulationServiceImpl.
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @author CF: passage XML vers annotations
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Service("testPopulationService")
public class TestPopulationServiceImpl extends BaseServiceImpl implements ITestPopulationService
{

    @Autowired
    private ITestPopulationDao testPopulationDao;

    /**
     * Constructeur
     */
    public TestPopulationServiceImpl()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param origineId
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.ITestPopulationService#rechercherVillesParOrigine(Long origineId)
     */
    @Override
    public List<VilleTest> rechercherVillesParOrigine(Long origineId)
    {
        return testPopulationDao.findVillesParOrigine(origineId);
    }

}
