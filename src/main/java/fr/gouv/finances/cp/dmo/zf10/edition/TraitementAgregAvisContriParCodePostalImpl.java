/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementAgregAvisContriParCodePostalImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf10.edition;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperHtml;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class TraitementAgregAvisContriParCodePostalImpl DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementAgregAvisContriParCodePostalImpl extends AbstractServiceEditionCommunImpl
{

    /**
     * Constructeur
     */
    public TraitementAgregAvisContriParCodePostalImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return collection
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerDatasource(java.util.Map)
     */
    @Override
    public Collection creerDatasource(Map parametresEdition)
    {
        return TraitementAvisContriParCodePostalStub.createBeanCollection();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        return "agregaviscontriparcodepostal";
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {
        Map<Object, Object> map = new HashMap<Object, Object>();
        map.put("CODE_POSTAL", "78000");
        return map;
    }

    /**
     * Configure dynamiquement la vue Html utilisée par jasperreports.
     * 
     * @param parametresEdition param
     * @return ParametrageVueJasperHtml
     */
    @Override
    protected ParametrageVueJasperHtml configurerLaVueHtml(Map parametresEdition)
    {
        ParametrageVueJasperHtml parametrageVueJasperHtml = (ParametrageVueJasperHtml) this.getParametrageVueJasper();

        String repCompletSite = null;
        String repBaseImgDansSite = null;
        String repImagDansSite = null;
        String uriImgDansSite = null;
        String nomDuFichier = this.creerNomDuFichier(parametresEdition);

        // TODO vérifier que le nom du fichier ne possède pas
        // d'extension et l'enlever si nécessaire

        if (parametresEdition != null && parametresEdition.containsKey("repertoireCompletSiteStatique")
            && parametresEdition.containsKey("repertoireBaseImageDansSiteStatique") && nomDuFichier != null
            && StringUtils.isNotBlank(nomDuFichier))
        {
            repCompletSite = (String) parametresEdition.get("repertoireCompletSiteStatique");
            repBaseImgDansSite = (String) parametresEdition.get("repertoireBaseImageDansSiteStatique");
            repImagDansSite = repCompletSite + "/" + nomDuFichier;
            uriImgDansSite =
                "/" + parametrageVueJasperHtml.getImageURI() + "/" + repBaseImgDansSite + "/" + nomDuFichier + "/";
        }
        else
        {
            throw new ProgrammationException(
                "Paramètres insuffisants : impossible de déterminer le répertoire de stockage ou l'URI d'accès des images");
        }

        parametrageVueJasperHtml.setImagesDirName(repImagDansSite);
        parametrageVueJasperHtml.setImageURI(uriImgDansSite);

        return parametrageVueJasperHtml;
    }

}
