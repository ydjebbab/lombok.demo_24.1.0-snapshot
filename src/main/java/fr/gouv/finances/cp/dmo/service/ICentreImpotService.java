package fr.gouv.finances.cp.dmo.service;

import java.util.List;

import fr.gouv.finances.cp.dmo.entite.CentreImpot;

/**
 * @author celinio fernandes Date: Mar 9, 2020
 */
public interface ICentreImpotService
{

    public void ajouter(CentreImpot centreImpot);
    
    public List<CentreImpot> rechercherTous();

}
