package fr.gouv.finances.cp.dmo.entite;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SituationFami.class)
public abstract class SituationFami_ {

	public static volatile SingularAttribute<SituationFami, Long> code;
	public static volatile SingularAttribute<SituationFami, String> libelle;
	public static volatile SingularAttribute<SituationFami, Long> id;
	public static volatile SingularAttribute<SituationFami, Integer> version;

}

