/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : DemandeAdhesionMensualisationForm.java
 *
 */

package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;

/**
 * Class DemandeAdhesionMensualisationForm.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
public class DemandeAdhesionMensualisationForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    // TODO : Changer par un champ simple Reference Avis d'imposition (on
    // utilise que ce champ)
    /** avis imposition. */
    private AvisImposition avisImposition = new AvisImposition();

    /** nouveau contrat. */
    private ContratMensu nouveauContrat = new ContratMensu();

    /** titulaire compte. */
    private String titulaireCompte;

    /**
     * Instanciation de demande adhesion mensualisation form.
     */
    public DemandeAdhesionMensualisationForm()
    {
        super();
    }

    /**
     * Gets the avis imposition.
     *
     * @return the avis imposition
     */
    public AvisImposition getAvisImposition()
    {
        return avisImposition;
    }

    /**
     * Gets the nouveau contrat.
     *
     * @return the nouveau contrat
     */
    public ContratMensu getNouveauContrat()
    {
        return nouveauContrat;
    }

    /**
     * Gets the titulaire compte.
     *
     * @return the titulaire compte
     */
    public String getTitulaireCompte()
    {
        return titulaireCompte;
    }

    /**
     * Sets the avis imposition.
     *
     * @param avisimposition the new avis imposition
     */
    public void setAvisImposition(AvisImposition avisimposition)
    {
        this.avisImposition = avisimposition;
    }

    /**
     * Sets the nouveau contrat.
     *
     * @param nouveaucontrat the new nouveau contrat
     */
    public void setNouveauContrat(ContratMensu nouveaucontrat)
    {
        this.nouveauContrat = nouveaucontrat;
    }

    /**
     * Sets the titulaire compte.
     *
     * @param titulairecompte the new titulaire compte
     */
    public void setTitulaireCompte(String titulairecompte)
    {
        this.titulaireCompte = titulairecompte;
    }
}
