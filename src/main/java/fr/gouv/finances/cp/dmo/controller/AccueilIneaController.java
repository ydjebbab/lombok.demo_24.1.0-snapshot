package fr.gouv.finances.cp.dmo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
@ApiModel(description = "Service gérant l'accueil INEA")
@RequestMapping("/accueil.inea")
public class AccueilIneaController
{
    @ApiOperation(value = "Redirection vers zfinea/accueil", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Succès de la redirection"),
            @ApiResponse(code = 401, message = "Accès non autorisé à cette ressource"),
            @ApiResponse(code = 403, message = "Accès interdit à la ressource"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @GetMapping
    public String afficher()
    {
        /** afficher la page "mon compte" */
        return "zfinea/accueil";
    }

}
