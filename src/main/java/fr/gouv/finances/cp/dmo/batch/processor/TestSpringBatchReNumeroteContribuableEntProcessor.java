/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.batch.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import fr.gouv.finances.cp.dmo.entite.ContribuableEnt;

/**
 * Class TestSpringBatchReNumeroteContribuableEntProcessor 
 */
public class TestSpringBatchReNumeroteContribuableEntProcessor implements ItemProcessor<ContribuableEnt, ContribuableEnt>
{
    private static final Logger log = LoggerFactory.getLogger(TestSpringBatchReNumeroteContribuableEntProcessor.class);

    private String prefixeidcontribuable;

    public TestSpringBatchReNumeroteContribuableEntProcessor()
    {
        super(); 
    }
    
    public String getPrefixeidcontribuable()
    {
        return prefixeidcontribuable;
    }

    public void setPrefixeidcontribuable(String prefixeidcontribuable)
    {
        this.prefixeidcontribuable = prefixeidcontribuable;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public ContribuableEnt process(ContribuableEnt item) throws Exception
    {
        String oldIdentifiant = item.getIdentifiant();
        item.setIdentifiant(prefixeidcontribuable + oldIdentifiant);
        log.info("Renumérotation du contribuable identifiant " + oldIdentifiant + " en " + item.getIdentifiant());
        return item;
    }



}