/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * 
 *
 * fichier : TraitementEditionsExportCodesPostauxImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf2.edition;

import java.util.Collection;
import java.util.Map;

import fr.gouv.finances.lombok.adresse.service.AdresseService;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;

/**
 * Class TraitementEditionsExportCodesPostauxImpl DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementEditionsExportCodesPostauxImpl extends AbstractServiceEditionCommunImpl
{

    /** adresseserviceso. */
    private AdresseService adresseserviceso;

    /**
     * Constructeur
     */
    public TraitementEditionsExportCodesPostauxImpl()
    {
        super();

    }

    /**
     * Accesseur de l attribut adresseserviceso.
     * 
     * @return adresseserviceso
     */
    public AdresseService getAdresseserviceso()
    {
        return adresseserviceso;
    }

    /**
     * Modificateur de l attribut adresseserviceso.
     * 
     * @param adresseserviceso le nouveau adresseserviceso
     */
    public void setAdresseserviceso(AdresseService adresseserviceso)
    {
        this.adresseserviceso = adresseserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {
        return null;
    }

    /*
     * JRFileVirtualizer virtualizer = new JRFileVirtualizer(2,"tempLocation");
     * parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
     */

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return description
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#getDescription(java.util.Map)
     */
    @Override
    public String getDescription(Map parametresEdition)
    {
        return "tot2";
    }

    @Override
    public Collection creerDatasource(Map parametresEdition)
    {
        return adresseserviceso.rechercherTousLesCodesPostaux();
    }

}
