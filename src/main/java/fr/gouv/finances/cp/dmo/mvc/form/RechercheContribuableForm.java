/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : RechercheContribuableForm.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.techbean.CriteresRecherches;

/**
 * Class RechercheContribuableForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class RechercheContribuableForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = -1186510463592024838L;

    // Champs du formulaire
    /** crit. */
    private CriteresRecherches crit;

    /** contribuable. */
    private ContribuablePar contribuable;

    /**
     * Instanciation de recherche contribuable form.
     */
    public RechercheContribuableForm()
    {
        crit = new CriteresRecherches();
    }

    /**
     * Gets the contribuable.
     *
     * @return the contribuable
     */
    public ContribuablePar getContribuable()
    {
        return contribuable;
    }

    /**
     * Gets the crit.
     *
     * @return the crit
     */
    public CriteresRecherches getCrit()
    {
        return crit;
    }

    /**
     * Sets the contribuable.
     *
     * @param contribuable the new contribuable
     */
    public void setContribuable(ContribuablePar contribuable)
    {
        this.contribuable = contribuable;
    }

    /**
     * Sets the crit.
     *
     * @param crit the new crit
     */
    public void setCrit(CriteresRecherches crit)
    {
        this.crit = crit;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return string
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
        return this.getCrit().toString();
    }

}
