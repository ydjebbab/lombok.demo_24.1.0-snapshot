/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.Rib;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.mvc.form.DemandeAdhesionMensualisationForm;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.lombok.mel.techbean.Mel;
import fr.gouv.finances.lombok.mel.techbean.MelRegleGestionException;
import fr.gouv.finances.lombok.mvc.TelechargementView;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.structure.bean.StructureRegleGestionException;
import fr.gouv.finances.lombok.upload.bean.ContenuFichier;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.propertyeditor.SelectEditor;

/**
 * Action DemandeAdhesionMensualisationFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class DemandeAdhesionMensualisationFormAction extends FormAction
{
    private static final Logger log = LoggerFactory.getLogger(DemandeAdhesionMensualisationFormAction.class);

    private IContribuableService contribuableserviceso;

    public DemandeAdhesionMensualisationFormAction()
    {
        super();
    }

    public DemandeAdhesionMensualisationFormAction(Class arg0)
    {
        super(arg0);
    }

    public IContribuableService getContribuableserviceso()
    {
        return contribuableserviceso;
    }

    public void setContribuableserviceso(IContribuableService contribuableService)
    {
        this.contribuableserviceso = contribuableService;
    }

    /**
     * methode Editer recapitulatif
     * 
     * @param reqc param
     * @return event
     */
    public Event editerRecapitulatif(RequestContext requestContext)
    {
        log.debug(">>> Debut methode editerRecapitulatif");
        Event result = error();
        DemandeAdhesionMensualisationForm demandeAdhesionMensualisationForm =
            (DemandeAdhesionMensualisationForm) requestContext.getFlowScope().get(getFormObjectName());

        // Préparation de l'édition
        try
        {
            Document document = new Document();

            ByteArrayOutputStream baos = new ByteArrayOutputStream(4096);
            PdfWriter writer = PdfWriter.getInstance(document, baos);

            writer.setViewerPreferences(PdfWriter.AllowPrinting | PdfWriter.PageLayoutSinglePage);

            document.open();
            document.add(new Paragraph(demandeAdhesionMensualisationForm.getAvisImposition().getReference()));
            document.add(new Paragraph(demandeAdhesionMensualisationForm.getNouveauContrat().getContribuablePar().getAdresseMail()));
            document.close();

            FichierJoint fichierJoint = new FichierJoint();
            ContenuFichier contenuFichier = new ContenuFichier();

            contenuFichier.setData(baos.toByteArray());
            fichierJoint.setLeContenuDuFichier(contenuFichier);
            fichierJoint.setDateHeureSoumission(new Date());
            fichierJoint.setTypeMimeFichier("application/pdf");
            fichierJoint.setNomFichierOriginal("recapitulatifAdhesion.pdf");
            fichierJoint.setTailleFichier(baos.size());

            if (fichierJoint.getTailleFichier() > 0)
            {
                // Sauvegarde de l'édition dans le contexte pour utilisation par le contrôleur de téléchargement
                requestContext.getRequestScope().put(TelechargementView.FICHIER_A_TELECHARGER, fichierJoint);
                result = success();
            }
        }
        catch (DocumentException e)
        {
            throw new ProgrammationException(e);
        }
        return result;
    }

    /**
     * methode Executer adherer ala mensualisation
     * 
     * @param reqc param
     * @return event
     */
    public Event executerAdhererAlaMensualisation(RequestContext requestContext)
    {
        log.debug(">>> Debut methode executerAdhererAlaMensualisation");
        DemandeAdhesionMensualisationForm demandeAdhesionMensualisationForm =
            (DemandeAdhesionMensualisationForm) requestContext.getFlowScope().get(getFormObjectName());

        /**
         * <pre>
         * On fait le choix ici de passer à la méthode métier les objets à mettre à jour de manière séparée pour plus de
         * clarté et pour lui laisser reconstruire les liens éventuellement incomplets entre objets (par exemple le lien entre
         * contribuable et le nouveau contrat mensualisation n'a pas été construit). Seul le RIB n'est pas passé de manière
         * explicite car il a été créé avec le contrat de mensualisation (les autres objets existent déjà)
         * </pre>
         */
        ContribuablePar contribuablePar = demandeAdhesionMensualisationForm.getNouveauContrat().getContribuablePar();
        TypeImpot typeImpot = demandeAdhesionMensualisationForm.getNouveauContrat().getTypeImpot();
        ContratMensu contratMensu = demandeAdhesionMensualisationForm.getNouveauContrat();
        String numAvisImposition = demandeAdhesionMensualisationForm.getAvisImposition().getReference();
        String identifiantUtilisateur = contribuablePar.getIdentifiant();

        contribuableserviceso.adhererAlaMensualisation(contribuablePar, typeImpot, contratMensu, numAvisImposition,
            identifiantUtilisateur);

        return success();
    }

    /**
     * methode Preparer calculer annees prise d'effet possibles
     * 
     * @param reqc param
     * @return event
     */
    public Event preparerCalculerAnneesPriseDEffetPossibles(RequestContext requestContext)
    {
        log.debug(">>> Debut methode preparerCalculerAnneesPriseDEffetPossibles");
        List unelistetypesimpots = (List) requestContext.getFlowScope().get("listetypesimpots");
        Map listeanneepriseeffet = contribuableserviceso.calculerAnneesPriseDEffetPossiblesRG13(unelistetypesimpots);

        requestContext.getFlowScope().put("listeanneepriseeffet", listeanneepriseeffet);
        return success();
    }

    /**
     * methode Preparer rechercher liste types impots disponibles a la mensualisation pour un contribuable
     * 
     * @param reqc param
     * @return event
     */
    public Event preparerRechercherListeTypesImpotsDisponiblesALaMensualisationPourUnContribuable(RequestContext requestContext)
    {
        log.debug(">>> Debut methode preparerRechercherListeTypesImpotsDisponiblesALaMensualisationPourUnContribuable");
        DemandeAdhesionMensualisationForm demandeAdhesionMensualisationForm = (DemandeAdhesionMensualisationForm) requestContext.getFlowScope().get(getFormObjectName());
        /*
         * for (Map.Entry<String,Object>entry : reqc.getExternalContext().getRequestParameterMap().asMap().entrySet()) {
         * System.out.println(entry.getKey()+"/"+entry.getValue()); };
         */
        String acap = requestContext.getExternalContext().getGlobalSessionMap().getString("portail_agent_V2");
        log.info(acap);

        ContribuablePar contribuablePar = demandeAdhesionMensualisationForm.getNouveauContrat().getContribuablePar();

        try
        {
            List<TypeImpot> typesImpots =
                contribuableserviceso
                    .rechercherListeTypesImpotsDisponiblesALaMensualisationPourUnContribuable(contribuablePar);
            requestContext.getFlowScope().put("listetypesimpots", typesImpots);
            return success();
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(requestContext).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rgExc.getMessage());
            return error();
        }
    }

    /**
     * Accesseur de l'attribut cookie acap.
     *
     * @param reqc
     * @return cookie acap
     */
    public Cookie getCookieACAP(RequestContext requestContext)
    {
        log.debug(">>> Debut methode getCookieACAP");
        HttpServletRequest req = (HttpServletRequest) requestContext.getExternalContext().getNativeRequest();
        final String nomCookieACAP = "portail_agent_V2";
        Cookie acap = null;

        Cookie[] cookies = req.getCookies();
        if (cookies != null)
        {
            for (Cookie ck : cookies)
            {
                // Recherche du cookie et stockage en tant qu'attribut dans le request scope.
                // Si une session existe, alors on le stocke dans la session.
                // Sinon on le stocke dans le request scope.
                if (nomCookieACAP.equalsIgnoreCase(ck.getName()))
                {
                    acap = ck;
                    break;
                }
            }
        }
        return acap;
    }

    /**
     * methode Checks for acap cookie
     *
     * @param reqc
     * @return true, si c'est vrai
     */
    public boolean hasACAPCookie(RequestContext requestContext)
    {
        log.debug(">>> Debut methode hasACAPCookie");
        HttpServletRequest req = (HttpServletRequest) requestContext.getExternalContext().getNativeRequest();
        boolean ret = false;

        Cookie[] cookies = req.getCookies();
        if (cookies != null)
        {
            for (Cookie ck : cookies)
            {
                // Recherche du cookie et stockage en tant qu'attribut dans le request scope.
                // Si une session existe, alors on le stocke dans la session.
                // Sinon on le stocke dans le request scope.
                if ("portail_agent_V2".equalsIgnoreCase(ck.getName()))
                {
                    ret = true;
                    break;
                }
            }
        }
        return ret;
    }

    /**
     * methode Preparer rechercher un contribuable et avis imposition et contrat mensu
     * 
     * @param reqc param
     * @return event
     */
    public Event preparerRechercherUnContribuableEtAvisImpositionEtContratMensu(RequestContext requestContext)
    {
        log.debug(">>> Debut methode preparerRechercherUnContribuableEtAvisImpositionEtContratMensu");
        // Récupération des caractéristiques de la personne connectée dans le contexte sécurisé pour retrouver l'uid
        PersonneAnnuaire personneAnnuaire = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();
        try
        {
            ContribuablePar contribuablePar =
                contribuableserviceso
                    .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID(personneAnnuaire
                        .getUid());
            /*
             * Rattachement du contribuable au nouveau contrat dans le formulaire plutôt que dans le contexte de flux car l'adresse
             * mail contribuable peut être modifiée dans l'écran SAISIERIB
             */
            DemandeAdhesionMensualisationForm demandeAdhesionMensualisationForm =
                (DemandeAdhesionMensualisationForm) requestContext.getFlowScope().get(getFormObjectName());

            /*
             * Attention par contre de ne pas raccrocher le Contrat au contribuable car la RG12 (vérifier pas d' adhésion en cours
             * sur ce type impot) va renvoyer une erreur au moment de l'adhesion.
             */
            demandeAdhesionMensualisationForm.getNouveauContrat().setContribuablePar(contribuablePar);
            return success();
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            // erreur uid contribuable non trouvé en base
            Errors errors = new FormObjectAccessor(requestContext).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rgExc.getMessage());
            return error();
        }
    }

    /**
     * methode Valider verifier adresse mel
     * 
     * @param reqc param
     * @return event
     */
    public Event validerVerifierAdresseMel(RequestContext requestContext)
    {
        log.debug(">>> Debut methode validerVerifierAdresseMel");
        DemandeAdhesionMensualisationForm demandeAdhesionMensualisationForm = (DemandeAdhesionMensualisationForm) requestContext.getFlowScope().get(getFormObjectName());
        String adresseMail = demandeAdhesionMensualisationForm.getNouveauContrat().getContribuablePar().getAdresseMail();
        Mel mel = new Mel();

        try
        {
            mel.verifierAdresseMel(adresseMail);
            return success();
        }
        catch (MelRegleGestionException melRGExc)
        {
            log.error(melRGExc.getMessage(), melRGExc);
            Errors errors = new FormObjectAccessor(requestContext).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject("nouveauContrat.contribuablePar.adresseMail", null, melRGExc.getMessage());
            return error();
        }
    }

    /**
     * methode Valider verifier avis imposition est attache au contribuable
     * 
     * @param reqc param
     * @return event
     */
    public Event validerVerifierAvisImpositionEstAttacheAuContribuable(RequestContext requestContext)
    {
        log.debug(">>> Debut methode validerVerifierAvisImpositionEstAttacheAuContribuable");
        DemandeAdhesionMensualisationForm demandeAdhesionMensualisationForm = (DemandeAdhesionMensualisationForm) requestContext.getFlowScope().get(getFormObjectName());
        AvisImposition avisImposition = demandeAdhesionMensualisationForm.getAvisImposition();
        ContribuablePar contribuablePar = demandeAdhesionMensualisationForm.getNouveauContrat().getContribuablePar();

        try
        {
            contribuablePar.verifierAvisImpositionEstAttacheAuContribuableRG14(avisImposition.getReference());
            return success();
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(requestContext).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject("avisImposition.reference", null, rgExc.getMessage());
            return error();
        }
    }

    /**
     * <pre>
     * Methode de validation metier faisant appel à un service métier 
     * Règle de nommage : valider NomDeLaMethodeMetierAppelee
     * </pre>
     * 
     * @param reqc param
     * @return the event
     */
    public Event validerVerifierAvisImpositionEstAttacheAUnTypeImpot(RequestContext requestContext)
    {
        log.debug(">>> Debut methode validerVerifierAvisImpositionEstAttacheAUnTypeImpot");
        DemandeAdhesionMensualisationForm demandeAdhesionMensualisationForm = (DemandeAdhesionMensualisationForm) requestContext.getFlowScope().get(getFormObjectName());
        ContribuablePar contribuablePar = demandeAdhesionMensualisationForm.getNouveauContrat().getContribuablePar();
        AvisImposition avisSaisie = demandeAdhesionMensualisationForm.getAvisImposition();
        TypeImpot typeImpot = demandeAdhesionMensualisationForm.getNouveauContrat().getTypeImpot();

        try
        {
            AvisImposition aviscomplet = contribuablePar.rechercherAvisImpositionAttache(avisSaisie.getReference());
            aviscomplet.verifierAvisDImpositionEstAttacheAUnTypeImpotRG15(typeImpot.getCodeImpot());
            return success();
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(requestContext).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject("nouveauContrat.typeImpot.id", null, rgExc.getMessage());
            return error();
        }
    }

    /**
     * <pre>
     * Methode de validation metier faisant appel à un service métier 
     * (si travaille sur plusieurs types d'objets ou doit faire des accès base
     * ou à un objet métier dans le cas ou il porte toutes les données nécessaires à la validation)
     * Règle de nommage : validate NomDeLaMethodeMetierAppelee
     * </pre>
     * 
     * @param reqc param
     * @return the event
     */
    public Event validerVerifierCleRib(RequestContext reqc)
    {
        log.debug(">>> Debut methode validerVerifierCleRib");
        DemandeAdhesionMensualisationForm demandeAdhesionMensualisationForm =  (DemandeAdhesionMensualisationForm) reqc.getFlowScope().get(getFormObjectName());
        Rib rib = demandeAdhesionMensualisationForm.getNouveauContrat().getRib();
        try
        {
            /**
             * <pre>
             * On peut utiliser les mécanismes d'exceptions pour les appels à la couche métier. 
             * Cela permet de redescendre à l'utilisateur le bon message quelque soit l'erreur qui s'est produite. 
             * Dans ce cas il faut mettre des messages clairs pour les exceptions métiers puisqu'elles seront affichées à l'utilisateur. 
             * Exemple ici : 
             * Si erreur sur longueur code banque le message "Le code banque doit comporter 5 caractères" sera affiché. 
             * Si erreur sur clé rib, le message "La clé rib n'est pas validée" sera affiché. 
             * Ceci peut être utilisé dans le batch également pour remonter des erreurs dans les logs.
             * </pre>
             */
            rib.verifierCleRib();
            return success();
        }
        catch (StructureRegleGestionException structRGExc)
        {
            log.error(structRGExc.getMessage(), structRGExc);
            Errors errors = new FormObjectAccessor(reqc).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject("nouveauContrat.rib.cleRib", null, structRGExc.getMessage());
            return error();
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param reqc
     * @param per
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext requestContext, PropertyEditorRegistry propertyEditorRegistry)
    {
        log.debug(">>> Debut methode registerPropertyEditors");
        super.registerPropertyEditors(requestContext, propertyEditorRegistry);
        /**
         * <pre>
         * Récupère la liste typeimpot dans le flux et la passe au propertyditor de manière à ce qu'il puisse faire
         * automatiquement la correspondance entre attribut id et objet typeimpot correspondant lors du bind.
         * </pre>
         */

        List listeTypesImpots = (List) requestContext.getFlowScope().get("listetypesimpots");
        // db.registerCustomEditor(TypeImpot.class, new
        // ListeTypeImpotEditor(listeTypesImpots) );
        propertyEditorRegistry.registerCustomEditor(TypeImpot.class, new SelectEditor(listeTypesImpots, "id"));
    }

}
