/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.ITypeImpotJdbcDao;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;

/**
 * DAO TypeImpotJdbcDaoImpl
 */
@Repository("typeImpotJdbcDao")
@Transactional(transactionManager="transactionManager")
public class TypeImpotJdbcDaoImpl extends BaseDaoJpaImpl implements ITypeImpotJdbcDao
{
    private static final Logger log = LoggerFactory.getLogger(TypeImpotJdbcDaoImpl.class);
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public TypeImpotJdbcDaoImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.dao.ITypeImpotJdbcDao#loadTypeImpot()
     */
    @Override
    public List<TypeImpot> loadTypeImpot()
    {
        log.debug(">>> Debut methode loadTypeImpot");
        String sql = "select *  from TYPEIMPOT_TYIM ";
        List<TypeImpot> listeTypeImpot = jdbcTemplate.queryForList(sql, TypeImpot.class);
        return listeTypeImpot;
        
    }
}
