/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TestPopulationService.java
 *
 */
package fr.gouv.finances.cp.dmo.service;

import java.util.List;

import fr.gouv.finances.cp.dmo.entite.VilleTest;

/**
 * Interface ITestPopulationService plus tard.
 * 
 * @author chouard-cp
 * @author CF
 * @version $Revision: 1.3 $ Date: 14 déc. 2009
 */
public interface ITestPopulationService
{

    /**
     * methode Rechercher villes par origine
     * 
     * @param origineId
     * @return list< ville test>
     */
    List<VilleTest> rechercherVillesParOrigine(Long origineId);
}