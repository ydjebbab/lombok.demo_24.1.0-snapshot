package fr.gouv.finances.cp.dmo.entite;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(HabitantTest.class)
public abstract class HabitantTest_ {

	public static volatile SingularAttribute<HabitantTest, Long> id;
	public static volatile SingularAttribute<HabitantTest, OrigineTest> origine;
	public static volatile SingularAttribute<HabitantTest, Integer> version;
	public static volatile SingularAttribute<HabitantTest, String> nom;
	public static volatile SingularAttribute<HabitantTest, String> prenom;

}

