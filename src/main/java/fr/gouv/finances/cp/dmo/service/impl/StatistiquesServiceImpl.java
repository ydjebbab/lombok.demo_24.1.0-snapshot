/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : StatistiquesServiceImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.dao.IStatistiquesDao;
import fr.gouv.finances.cp.dmo.service.IEditionJasperService;
import fr.gouv.finances.cp.dmo.service.IStatistiquesService;
import fr.gouv.finances.cp.dmo.techbean.StatistiquesAvisImpositions;
import fr.gouv.finances.cp.dmo.techbean.StatistiquesContribuables;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Class StatistiquesServiceImpl
 * 
 * @author chouard-cp
 * @author CF: passage XML vers annotations
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Service("statistiquesService")
public class StatistiquesServiceImpl extends BaseServiceImpl implements IStatistiquesService
{
    @Autowired
    private IStatistiquesDao statistiquesDao;

    private IEditionJasperService editionjasperserviceso;

    public StatistiquesServiceImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.IStatistiquesService#calculerAgregationAvisImpositionParTypeImpot()
     */
    @Override
    public List<StatistiquesAvisImpositions> calculerAgregationAvisImpositionParTypeImpot()
    {
        List<StatistiquesAvisImpositions> result = statistiquesDao.agregerAvisImpositionParTypeImpot();
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param seuil
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.IStatistiquesService#calculerAgregationAvisImpositionParTypeImpotSeuil(java.lang.Double)
     */
    @Override
    public List<StatistiquesAvisImpositions> calculerAgregationAvisImpositionParTypeImpotSeuil(Double seuil)
    {
        List<StatistiquesAvisImpositions> result = statistiquesDao.agregerAvisImpositionParTypeImpotSeuil(seuil);
        // BeanComparator NaturalOrderBeanComparator = new
        // BeanComparator("annee", new NullComparator());
        // Collections.sort((List) result, NaturalOrderBeanComparator);
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.IStatistiquesService#calculerAgregationContribuableParTypeImpot()
     */
    @Override
    public List<StatistiquesContribuables> calculerAgregationContribuableParTypeImpot()
    {
        List<StatistiquesContribuables> listeStatistiquesContribuables = statistiquesDao.agregerContribuableParTypeImpot();
        return listeStatistiquesContribuables;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.IStatistiquesService#calculerAgregationParContribuable()
     */
    @Override
    public List<StatistiquesContribuables> calculerAgregationParContribuable()
    {
        List<StatistiquesContribuables> listeStatistiquesContribuables = statistiquesDao.agregerParContribuable();
        return listeStatistiquesContribuables;
    }

    /**
     * methode Calculer stat par type impot : DGFiP.
     */
    public void calculerStatParTypeImpot()
    {

    }

    /**
     * Accesseur de l attribut editionjasperserviceso.
     * 
     * @return editionjasperserviceso
     */
    public IEditionJasperService getEditionjasperserviceso()
    {
        return editionjasperserviceso;
    }

    /**
     * Modificateur de l attribut editionjasperserviceso.
     * 
     * @param editionjasperserviceso le nouveau editionjasperserviceso
     */
    public void setEditionjasperserviceso(IEditionJasperService editionjasperserviceso)
    {
        this.editionjasperserviceso = editionjasperserviceso;
    }

}
