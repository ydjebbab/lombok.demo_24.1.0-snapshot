/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.entite;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Entité DocumentJoint
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */

@Entity
@Table(name = "DOCUMENTJOINT_DOC")
@SequenceGenerator(name = "DOCUMENTJOINT_ID_GENERATOR", sequenceName = "SEQ_DOCUMENTJOINT_DOC", allocationSize = 1)
@AttributeOverrides({
        @AttributeOverride(name = "identifiant", column = @Column(nullable = true, name = "IDENTIFIANT")),
        @AttributeOverride(name = "id", column = @Column(name = "id"))
})
public class DocumentJoint extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DOCUMENTJOINT_ID_GENERATOR")
    @Column(name = "id")
    private Long id;

    @Column(nullable = true, name = "IDENTIFIANT")
    private String identifiant;

    @ManyToOne(fetch = FetchType.LAZY) // defaut est EAGER
    @JoinColumn(name = "ZFIJ_ID_leFichierJoint", foreignKey = @ForeignKey(name = "FK_DOC_FIC_1"))
    private FichierJoint leFichierJoint;

    public DocumentJoint()
    {
        super();
    }

    public Long getId()
    {
        return id;
    }

    public String getIdentifiant()
    {
        return identifiant;
    }

    public FichierJoint getLeFichierJoint()
    {
        return leFichierJoint;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setIdentifiant(String identifiant)
    {
        this.identifiant = identifiant;
    }

    public void setLeFichierJoint(FichierJoint lefichierJoint)
    {
        this.leFichierJoint = lefichierJoint;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param object
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }

        if (object == null || (object.getClass() != this.getClass()))
        {
            return false;
        }

        DocumentJoint myObj = (DocumentJoint) object;

        return ((identifiant == null ? myObj.identifiant == null : identifiant.equals(myObj.identifiant)) && (leFichierJoint == null
            ? myObj.leFichierJoint == null : leFichierJoint.equals(myObj.leFichierJoint)));
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int hash = hashCODEHASHINIT;
        int varCode = 0;

        varCode = (null == identifiant ? 0 : identifiant.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        varCode = (null == leFichierJoint ? 0 : leFichierJoint.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        return hash;

    }

}
