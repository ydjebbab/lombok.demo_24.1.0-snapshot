package fr.gouv.finances.cp.dmo.entite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;

/**
 * @author celfer Date: 24 déc. 2019
 */
@Entity
@Table(name = "REVENU_FONCIER")
@SequenceGenerator(name = "REVENU_FONCIER_ID_GENERATOR", sequenceName = "SEQ_REVENU_FONCIER", allocationSize = 1)
public class RevenuFoncier extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REVENU_FONCIER_ID_GENERATOR")
    @Column(name = "id")
    private Long id;

    // Vide ou meublée
    @Column(nullable = true, name = "TYPELOCATION")
    private String typeLocation;

    @Column(nullable = true, name = "MONTANTREVENU")
    private int montantRevenu;

    public RevenuFoncier()
    {
        super();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getTypeLocation()
    {
        return typeLocation;
    }

    public void setTypeLocation(String typeLocation)
    {
        this.typeLocation = typeLocation;
    }

    public int getMontantRevenu()
    {
        return montantRevenu;
    }

    public void setMontantRevenu(int montantRevenu)
    {
        this.montantRevenu = montantRevenu;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + montantRevenu;
        result = prime * result + ((typeLocation == null) ? 0 : typeLocation.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RevenuFoncier other = (RevenuFoncier) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (montantRevenu != other.montantRevenu)
            return false;
        if (typeLocation == null)
        {
            if (other.typeLocation != null)
                return false;
        }
        else if (!typeLocation.equals(other.typeLocation))
            return false;
        return true;
    }

}
