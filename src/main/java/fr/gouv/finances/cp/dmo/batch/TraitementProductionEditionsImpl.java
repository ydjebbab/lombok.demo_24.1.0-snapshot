/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.batch;

import java.util.HashMap;
import java.util.Map;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Batch TraitementProductionEditionsImpl
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Service("traitementproductioneditions")
@Profile("batch")
@Lazy(true)
public class TraitementProductionEditionsImpl extends ServiceBatchCommunImpl {

    @Autowired()
    @Qualifier("editiondemandeserviceimpl")
    private EditionDemandeService editiondemandeserviceso;

    public TraitementProductionEditionsImpl() {
        super();
    }
    
    @Value("${traitementproductioneditions.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch) {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementproductioneditions.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch) {
        super.setNomBatch(nomBatch);
    }

    public EditionDemandeService getEditiondemandeserviceso() {
        return editiondemandeserviceso;
    }

    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso) {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch() {
        Map<?,?> parametresEdition = new HashMap<>();
        EditionSynchroneNotifieeParMail editionSynchroneNotifieeParMail = editiondemandeserviceso.initEditionSynchroneNotifieeParMail("zf2.adressescontribuables.edition", "amleplatinec-cp", "wenceslas.petit@cp.finances.gouv.fr");
        editionSynchroneNotifieeParMail.getDestinationEdition().ajouterUnProfilDestinataire("ADMINISTRATEUR").ajouterUnFiltre("CODE_STRUCTURE_SUP").affecterLaValeur("0350000").affecterLaValeur("0750000");
        editionSynchroneNotifieeParMail = editiondemandeserviceso.declencherEdition(editionSynchroneNotifieeParMail, parametresEdition);
    }


}
