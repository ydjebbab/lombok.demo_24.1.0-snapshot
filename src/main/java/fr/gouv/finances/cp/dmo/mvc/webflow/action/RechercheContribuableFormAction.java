/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : RechercheContribuableFormAction.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.context.MessageSource;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.mvc.form.RechercheContribuableForm;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.cp.dmo.techbean.CriteresRecherches;
import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.apptags.util.MaxFetchTableUtil;
import fr.gouv.finances.lombok.util.format.BooleanFormat;
import fr.gouv.finances.lombok.util.format.BooleanFormat.TypeFormat;
import fr.gouv.finances.lombok.util.format.FormaterNombre;
import fr.gouv.finances.lombok.util.propertyeditor.CpBooleanEditor;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomNumberEditor;

/**
 * Action RechercheContribuableFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class RechercheContribuableFormAction extends FormAction
{
    private static final Logger log = LoggerFactory.getLogger(RechercheContribuableFormAction.class);

    /** Constant : COLLECTION_CONTRIBUABLES. */
    public static final String COLLECTION_CONTRIBUABLES = "listecontribuable";

    /** Constant : TYPES_DE_RECHERCHE. */
    public static final String TYPES_DE_RECHERCHE = "typesderecherche";

    /** Constant : TYPES_DE_CIVILITE. */
    public static final String TYPES_DE_CIVILITE = "typesdecivilite";

    /** Constant : TYPES_DE_TRI. */
    public static final String TYPES_DE_TRI = "typesdetri";

    /** Constant : TYPES_DE_CRITERES_DE_TRI. */
    public static final String TYPES_DE_CRITERES_DE_TRI = "typesdecriteredetri";

    /** Constant : TYPES_DE_NBLIGNES. */
    public static final String TYPES_DE_NBLIGNES = "typesdenblignes";

    /** Constant : TYPES_CONTRIPART. */
    public static final String TYPES_CONTRIPART = "typecontripart";

    /** contribuableserviceso. */
    private IContribuableService contribuableserviceso;

    /**
     * Constructeur
     */
    public RechercheContribuableFormAction()
    {
        super();
    }

    /**
     * Constructeur
     * 
     * @param arg0 param
     */
    public RechercheContribuableFormAction(Class<?> arg0)
    {
        super(arg0);
    }

    /**
     * methode Executer rechercher 
     * 
     * @param context param
     * @return event
     */
    public Event executerRechercher(RequestContext context)
    {
        log.debug(">>> Debut methode executerRechercher");
        // Lecture d'info stocker dans le fichier application.properties
        // String version = messageSource.getMessage("appli.version", null, null);
        // String webroot = messageSource.getMessage("appli.web.root", null, Locale.FRANCE);

        RechercheContribuableForm recherchecontribuableform =
            (RechercheContribuableForm) context.getFlowScope().get(getFormObjectName());
        int maxLignesRetournees = recherchecontribuableform.getCrit().getMaximumLignesRetournees();

        // CF : passage en Java 8
        Optional<List<? extends Contribuable>> listeContribuables =
            contribuableserviceso.rechercherListeContribuable(recherchecontribuableform.getCrit());

        context.getExternalContext().getGlobalSessionMap().put("uid", recherchecontribuableform.getCrit().getIdentifiant());

        context.getFlowScope().put(COLLECTION_CONTRIBUABLES, listeContribuables.get());
        context.getExternalContext().getGlobalSessionMap().put(COLLECTION_CONTRIBUABLES, listeContribuables);
        // Création de la liste des id à partir de la liste des éléments
        // (on préselectionne tout)
        List preSelectedRowids = new ArrayList();

        // sélection du premier contribuable
        if (listeContribuables.isPresent() && !listeContribuables.get().isEmpty())
        {
            String idContribuable = listeContribuables.get().get(0).getId().toString();
            preSelectedRowids.add(idContribuable);
        }

        // Sauvegarde dans le context de flux des rowids
        // pré-sélectionnés
        CheckboxSelectUtil.enregistreRowidsPreSelectionnesDansFlowScope(context, "rechcont", preSelectedRowids);

        // Sauvegarde de l'objet de paramétrage des checkboxes dans le
        // FlowScope
        CheckboxSelectUtil.parametrerCheckboxes(context, "rechcont").utiliserRowid("id").utiliserListeElements(
            "listecontribuable").utiliserMsgSelectionneUnSeulElement("Sélectionnez un seul contribuable à la fois.")
            .utiliserMsgAucunEltSelectionne("Auncun contribuable n'est sélectionné");

        MaxFetchTableUtil.gererMaxFetch(context, "rechcont", "listecontribuable", maxLignesRetournees + 1);

        return success();
    }

    /**
     * Gets the contribuableserviceso.
     *
     * @return the contribuableserviceso
     */
    public IContribuableService getContribuableserviceso()
    {
        return contribuableserviceso;
    }
    
    /**
     * Sets the contribuableserviceso.
     *
     * @param contribuableserviceso the new contribuableserviceso
     */
    public void setContribuableserviceso(IContribuableService contribuableserviceso)
    {
        this.contribuableserviceso = contribuableserviceso;
    }

    /**
     * methode Preparer bean criteres de recherche
     * 
     * @param rc param
     * @return event
     */
    public Event preparerBeanCriteresDeRecherche(RequestContext rc)
    {
        log.debug(">>> Debut methode preparerBeanCriteresDeRecherche");
        // Initialisation bean typesderecherche

        Map typesderecherche = CriteresRecherches.getTypesderecherche();
        rc.getFlowScope().put(TYPES_DE_RECHERCHE, typesderecherche);

        // Initialisation bean typesdecivilite

        Map typesdecivilite = CriteresRecherches.getTypesdecivilite();
        rc.getFlowScope().put(TYPES_DE_CIVILITE, typesdecivilite);

        // Initialisation bean Ordre de tri

        Map typesdetri = CriteresRecherches.getTypesordredetri();
        rc.getFlowScope().put(TYPES_DE_TRI, typesdetri);

        // Initialisation bean typesdecriteredetri

        Map typesdecriteredetri = CriteresRecherches.getTypesdecriteredetri();
        rc.getFlowScope().put(TYPES_DE_CRITERES_DE_TRI, typesdecriteredetri);

        // Initialisation bean Nombre de contribuables à retourner

        Map typesdenblignes = CriteresRecherches.getTypesdenblignes();
        rc.getFlowScope().put(TYPES_DE_NBLIGNES, typesdenblignes);

        // Initialisation du bouton radio choix contribuable particulier
        Map typescontripart = CriteresRecherches.getTypesContriPart();
        rc.getFlowScope().put(TYPES_CONTRIPART, typescontripart);

        return success();
    }

    /**
     * methode Reset table state 
     * 
     * @param context param
     * @return event
     */
    public Event resetTableState(RequestContext context)
    {
        log.debug(">>> Debut methode resetTableState");
        // context.getExternalContext().getRequestMap().put("gotofirstpage","true");
        context.getRequestScope().put("gotofirstpage", "true");
        return success();
    }

    /**
     * Modificateur de l attribut message source.
     *
     * @param messageSource le nouveau message source
     */
    public void setMessageSource(MessageSource messageSource)
    {
    }

    /**
     * methode Traiter checkboxes entre pages 
     * 
     * @param context param
     * @return event
     */
    public Event traiterCheckboxesEntrePages(RequestContext context)
    {
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context, "rechcont");
        return success();
    };

    /**
     * methode Traiter message erreur retourne 
     * 
     * @param context param
     * @return event
     */
    public Event traiterMessageErreurRetourne(RequestContext context)
    {
        CheckboxSelectUtil.traiterMessageErreurRetourne(context);
        return success();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param rc
     * @param per
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext rc, PropertyEditorRegistry per)
    {
        super.registerPropertyEditors(rc, per);

        per.registerCustomEditor(Integer.class, "crit.typeRechercheId", new CpCustomNumberEditor(Integer.class,
            FormaterNombre.getFormatNombreZeroDecimale()));

        per.registerCustomEditor(List.class, "crit.typesCiviliteId", new CustomCollectionEditor(List.class, false));

        per.registerCustomEditor(Boolean.class, "crit.typeContriPart", new CpBooleanEditor(BooleanFormat
            .getInstance(TypeFormat.UN_ZERO)));
    }

}
