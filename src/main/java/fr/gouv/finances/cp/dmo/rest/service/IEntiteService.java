package fr.gouv.finances.cp.dmo.rest.service;

import java.util.Map;

public interface IEntiteService 
{
    public Object merge(Object entite);
    
    public void persist(Object entite);
    
    public void remove(Object entite);
    
    public Object get(Class<?> entityClass, Object primaryKey);
    
    public Map<Long, Long> getAvailableIds(Class<?> entityClass);
}
