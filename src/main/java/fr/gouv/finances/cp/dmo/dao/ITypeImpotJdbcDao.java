/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : AdresseContribuableDao.java
 *
 */
package fr.gouv.finances.cp.dmo.dao;

import java.util.List;

import fr.gouv.finances.cp.dmo.entite.TypeImpot;

/**
 * Interface AdresseContribuableDao.
 * 
 * @author chouard-cp
 * @version $Revision: 1.2 $ Date: 14 déc. 2009
 */
public interface ITypeImpotJdbcDao
{

    /**
     * methode Load pays
     * 
     * @return list
     */
    public List<TypeImpot>  loadTypeImpot();
}
