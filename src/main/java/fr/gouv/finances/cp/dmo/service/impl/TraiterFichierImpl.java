/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */

package fr.gouv.finances.cp.dmo.service.impl;

import java.io.File;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A class providing several handling methods for different types of payloads.
 */
public class TraiterFichierImpl
{

    private static Logger log = LoggerFactory.getLogger(TraiterFichierImpl.class);

    public TraiterFichierImpl()
    {
        super();
    }

    /**
     * methode Handle string
     *
     * @param input
     * @return string
     */
    public String handleString(String input)
    {
        log.info("Copying text: " + input);
        return input.toUpperCase(Locale.FRENCH);
    }

    /**
     * methode Handle file
     *
     * @param input
     * @return file
     */
    public File handleFile(File input)
    {
        log.info("Copying file: " + input.getAbsolutePath());
        return input;
    }

}
