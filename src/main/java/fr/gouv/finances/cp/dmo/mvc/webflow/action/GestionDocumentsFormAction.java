/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.beans.PropertyEditor;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.DocumentJoint;
import fr.gouv.finances.cp.dmo.mvc.form.GestionDocumentsForm;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.clamav.bean.ClamAvResponse;
import fr.gouv.finances.lombok.clamav.bean.ClamAvResponse.CategorieReponse;
import fr.gouv.finances.lombok.clamav.service.ClamAVCheckService;
import fr.gouv.finances.lombok.mvc.TelechargementView;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.upload.service.FichierJointService;
import fr.gouv.finances.lombok.util.propertyeditor.FichierJointPropertyEditor;
import fr.gouv.finances.lombok.util.propertyeditor.SelectHashCodeEditor;

/**
 * Action GestionDocumentsFormAction
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class GestionDocumentsFormAction extends FormAction
{
    private static String UPLOAD_MAX_SIZE = "uploadmaxsize";

    private IContribuableService contribuableserviceso;

    private FichierJointService fichierjointserviceso;

    private ClamAVCheckService clamServiceCheck;

    public GestionDocumentsFormAction()
    {
        super();
    }

    public GestionDocumentsFormAction(Class arg0)
    {
        super(arg0);
    }

    public ClamAVCheckService getClamServiceCheck()
    {
        return clamServiceCheck;
    }

    public IContribuableService getContribuableserviceso()
    {
        return contribuableserviceso;
    }

    public FichierJointService getFichierjointserviceso()
    {
        return fichierjointserviceso;
    }

    public void setClamServiceCheck(ClamAVCheckService clamServiceCheck)
    {
        this.clamServiceCheck = clamServiceCheck;
    }

    public void setContribuableserviceso(IContribuableService contribuableserviceso)
    {
        this.contribuableserviceso = contribuableserviceso;
    }

    public void setFichierjointserviceso(FichierJointService fichierjointserviceso)
    {
        this.fichierjointserviceso = fichierjointserviceso;
    }

    /**
     * methode Ajouter un document
     *
     * @param context param
     * @return event
     */
    public Event ajouterUnDocument(RequestContext context)
    {

        // Lecture du formulaire
        GestionDocumentsForm gestionDocumentsForm =
            (GestionDocumentsForm) context.getConversationScope().get(getFormObjectName());

        DocumentJoint unDocumentJoint = gestionDocumentsForm.getDocumentJoint();
        Contribuable contribuable = gestionDocumentsForm.getContribuable();

        if (contribuable instanceof ContribuablePar)
        {
            ContribuablePar contribuablePar = (ContribuablePar) contribuable;
            contribuableserviceso.ajouterUnDocumentAuContribuable(contribuablePar, unDocumentJoint);

            // RAZ du fichier transféré dans le formulaire
            gestionDocumentsForm.setFichierJoint(new FichierJoint());
            gestionDocumentsForm.setDocumentJoint(new DocumentJoint());
        }
        return success();
    }

    /**
     * methode Initialiser formulaire : DGFiP.
     *
     * @param request param
     * @return event
     */
    public Event initialiserFormulaire(RequestContext request)
    {
        Event result;
        // Lecture du formulaire
        GestionDocumentsForm gestionDocumentsForm =
            (GestionDocumentsForm) request.getConversationScope().get(getFormObjectName());

        // Récupération de l'original dans le contexte de flux
        Object original = CheckboxSelectUtil.extraitUnElementSelectionneDansLeFluxParent(request, "rechcont");

        if (!(original instanceof ContribuablePar))
        {
            result = error();
        }
        else
        {
            contribuableserviceso.chargerLesDocumentsJoints((ContribuablePar) original);

            // On attache le contribuable au formulaire
            gestionDocumentsForm.setContribuable((ContribuablePar) original);

            // Initialisation du Document joint attaché au
            // formulaire
            DocumentJoint documentJoint = new DocumentJoint();
            documentJoint.setLeFichierJoint(new FichierJoint());
            gestionDocumentsForm.setDocumentJoint(documentJoint);

            // Sauvegarde de l'objet de paramétrage des checkboxes
            // dans le FlowScope
            CheckboxSelectUtil.parametrerCheckboxes(request, "tabledoc").utiliserRowid("hashcode")
                .utiliserListeElements("gestiondocumentsform.contribuable.lesDocsJoints")
                .utiliserMsgSelectionneUnSeulElement("Sélectionnez un seul document à la fois.")
                .utiliserMsgAucunEltSelectionne("Auncun document n'est sélectionné");
            result = success();
        }
        return result;
    }

    /**
     * methode Initialisertransfert
     *
     * @param context param
     * @return event
     */
    public Event initialisertransfert(RequestContext context)
    {
        GestionDocumentsForm gestionDocumentsForm =
            (GestionDocumentsForm) context.getConversationScope().get(getFormObjectName());
        gestionDocumentsForm.setFichierJoint(new FichierJoint());
        context.getFlowScope().put(UPLOAD_MAX_SIZE, fichierjointserviceso.getUploadmaxsize());
        return success();
    }

    /**
     * methode Supprimer un document
     *
     * @param context param
     * @return event
     */
    public Event supprimerUnDocument(RequestContext context)
    {
        Collection elementsSelectionnes =
            CheckboxSelectUtil.extraitLesElementsSelectionnesDansLeFlux(context, "tabledoc");
        if (CheckboxSelectUtil.auMoinsUnElementSelectionne(elementsSelectionnes))
        {
            GestionDocumentsForm gestionDocumentsForm =
                (GestionDocumentsForm) context.getConversationScope().get(getFormObjectName());
            Contribuable contribuable = gestionDocumentsForm.getContribuable();

            if (contribuable instanceof ContribuablePar)
            {
                ContribuablePar contribuablePar = (ContribuablePar) contribuable;
                contribuableserviceso.supprimerDesDocumentsDuContribuable(contribuablePar, elementsSelectionnes);
            }
        }
        return success();
    }

    /**
     * methode Telecharger un document
     *
     * @param context param
     * @return event
     */
    public Event telechargerUnDocument(RequestContext context)
    {
        GestionDocumentsForm gestionDocumentsForm =
            (GestionDocumentsForm) context.getConversationScope().get(getFormObjectName());
        Contribuable contribuable = gestionDocumentsForm.getContribuable();

        if (contribuable instanceof ContribuablePar)
        {
            ContribuablePar contribuablePar = (ContribuablePar) contribuable;
            List lesDocsJoints = contribuablePar.getLesDocsJoints();
            PropertyEditor propertyEditor = new SelectHashCodeEditor(lesDocsJoints);

            propertyEditor.setAsText(context.getRequestParameters().get("hashcode"));
            DocumentJoint leDocumentJoint = (DocumentJoint) propertyEditor.getValue();
            FichierJoint fichier =
                this.fichierjointserviceso.rechercherUnFichierJointEtSonContenuParId(leDocumentJoint
                    .getLeFichierJoint().getId());
            context.getRequestScope().put(TelechargementView.FICHIER_A_TELECHARGER, fichier);
        }
        return success();
    }

    /**
     * methode Traiter checkboxes entre pages
     *
     * @param context param
     * @return event
     */
    public Event traiterCheckboxesEntrePages(RequestContext context)
    {
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context, "tabledoc");
        return success();
    }

    /**
     * methode Validertransfert
     *
     * @param context param
     * @return event
     */
    public Event validertransfert(RequestContext context)
    {
        GestionDocumentsForm gestionDocumentsForm =
            (GestionDocumentsForm) context.getConversationScope().get(getFormObjectName());
        FichierJoint unFichierJoint = gestionDocumentsForm.getFichierJoint();

        gestionDocumentsForm.getDocumentJoint().setLeFichierJoint(unFichierJoint);
        return verifierIntegriteDocumentAntivirus(context);
        // return success();
    }

    /**
     * on integre la verification du document via l antivirus clamav.
     *
     * @param context DOCUMENTEZ_MOI
     * @return event
     */
    public Event verifierIntegriteDocumentAntivirus(RequestContext context)
    {
        Event retour = null;

        // Lecture du formulaire
        GestionDocumentsForm gestionDocumentsForm =
            (GestionDocumentsForm) context.getConversationScope().get(getFormObjectName());

        DocumentJoint unDocumentJoint = gestionDocumentsForm.getDocumentJoint();

        // Analyse du fichier pour détection de virus
        ClamAvResponse uneClamAvResponse = clamServiceCheck.checkByStream((unDocumentJoint.getLeFichierJoint()));

        if (uneClamAvResponse == null
            || uneClamAvResponse.getCategorieReponse() == CategorieReponse.NO_SERVICE)
        {
            logger.info("Service indisponible");

            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.rejectValue("fichierJoint", "exception.clamav.noservice",
                "L'accès au service de contrôle des fichiers a échoué. Merci de ré-essayer ultérieurement");
            retour = error();
        }
        else if (uneClamAvResponse.getCategorieReponse() == CategorieReponse.VIRUS)
        {
            if (logger.isInfoEnabled())
            {
                logger.info("Virus detecté sur le fichier "
                    + unDocumentJoint.getLeFichierJoint().getNomFichierOriginal());
            }
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.rejectValue("fichierJoint", "exception.clamav.virus",
                "Un virus a été détecté sur la pièce jointe. Elle ne peut être prise en compte");
            retour = error();
        }
        else if (uneClamAvResponse.getCategorieReponse() == CategorieReponse.TIMEOUT)
        {
            logger.info("Timeout ClamAV");
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.rejectValue("fichierJoint", "exception.clamav.timeout",
                "L'analyse du fichier comporte un temps de réponse trop long. Re-essayez ultérieurement");
            retour = error();
        }
        else if (uneClamAvResponse.getCategorieReponse() == CategorieReponse.FICHIER_VIDE)
        {
            logger.info("Fichier vide");
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.rejectValue("fichierJoint", "exception.clamav.fichierVide", "Le fichier est vide");
            retour = error();
        }
        else if (uneClamAvResponse.getCategorieReponse() == CategorieReponse.NO_VIRUS)
        {
            logger.info("Aucun virus détecté");
            retour = success();
        }

        return retour;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param rc
     * @param per
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext rc, PropertyEditorRegistry per)
    {
        super.registerPropertyEditors(rc, per);
        per.registerCustomEditor(FichierJoint.class, "fichierJoint", new FichierJointPropertyEditor());

        SimpleDateFormat dateformat1 = new SimpleDateFormat("dd/MM/yyyy H:m:s", Locale.FRANCE);
        per.registerCustomEditor(Date.class, "fichierJoint.dateHeureSoumission",
            new CustomDateEditor(dateformat1, true));
    }

}
