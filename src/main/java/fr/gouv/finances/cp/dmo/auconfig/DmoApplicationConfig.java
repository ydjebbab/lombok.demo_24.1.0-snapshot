package fr.gouv.finances.cp.dmo.auconfig;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Classe de configuration qui permet de conditionner les ressources à importer
 * 
 * @author celfer Date: 20 févr. 2020
 */
@Configuration
public class DmoApplicationConfig
{

    @Configuration
    @ConditionalOnExpression("'${lombok.composant.edition.inclus}' == 'true'")
    @ImportResource({"classpath:conf/application/applicationContext-edition-zf2.xml",
            "classpath:conf/application/applicationContext-edition-zf3.xml",
            "classpath:conf/application/applicationContext-edition-zf8.xml",
            "classpath:conf/application/applicationContext-edition-zf10.xml"})
    public static class EditionRequisApplicationConfig
    {
        private static final Logger log = LoggerFactory.getLogger(EditionRequisApplicationConfig.class);

        @PostConstruct
        public void init()
        {
            log.debug(">>> Après initialisation de EditionRequisApplicationConfig");
        }
    }

}
