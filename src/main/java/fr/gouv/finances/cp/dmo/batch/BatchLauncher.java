/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.cp.dmo.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

import fr.gouv.finances.lombok.batch.AbstractBootBatchStarter;
import fr.gouv.finances.lombok.config.LombokRootApplicationConfiguration;
import fr.gouv.finances.lombok.jpa.config.PersistenceJPAConfig;

/**
 * Classe de batch sans fichier XML et sans l'ancienne classe BatchStarter
 * 
 * @author chouard Date: 18 déc. 2017
 * @author CF: ajout de @ComponentScan et import de PersistenceJPAConfig
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class, JmxAutoConfiguration.class,
        IntegrationAutoConfiguration.class
})
@Profile("batch")
@Import({LombokRootApplicationConfiguration.class, PersistenceJPAConfig.class})
@ComponentScan(basePackages = {"fr.gouv.finances.cp.dmo.service.impl", "fr.gouv.finances.cp.dmo.batch"})
public class BatchLauncher extends AbstractBootBatchStarter
{
    /**
     * Création du contexte du batch en ajoutant les profils communs aux batchs.
     *
     * @param args arguments du batch
     */
    public static void main(String[] args)
    {
        // Surcharge la variable spring.profiles.active avec les profils pour les batchs s'ils sont différents de
        // l'applicatif
        setActiveProfiles("embedded,jpa,edition,batchoptimisation,atlas,journal,adresse,springbatch,jdbc,bancaire");
        // construction du contexte avec ajout du profil batch
        SpringApplicationBuilder lombokBatchBuilder = new SpringApplicationBuilder(BatchLauncher.class)
            .profiles("batch")
            .web(false);
        // system.exit pour que l'appelant récupère les codes retour
        System.exit(SpringApplication.exit(lombokBatchBuilder.run(args)));
    }
}
