package fr.gouv.finances.cp.dmo.entite;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(VilleTest.class)
public abstract class VilleTest_ {

	public static volatile SingularAttribute<VilleTest, String> libelle;
	public static volatile SingularAttribute<VilleTest, Long> id;
	public static volatile SingularAttribute<VilleTest, Integer> version;
	public static volatile SetAttribute<VilleTest, HabitantTest> listeHabitants;

}

