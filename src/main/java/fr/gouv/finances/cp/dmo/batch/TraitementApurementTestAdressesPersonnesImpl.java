/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.batch;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import fr.gouv.finances.cp.dmo.entite.AdresseTest;
import fr.gouv.finances.cp.dmo.entite.PersonneTest;
import fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.util.hibernate.UtilitaireTransactionSurScrollIteratorSupportService;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Batch permettant de tester les mises à jour pour une relation bidirectionnelle n-n de chaque côté de la relation avec
 * un des côtés étant indiqué "inverse".
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Service("traitementapurementtestadressespersonnes")
@Profile("batch")
@Lazy(true)
public class TraitementApurementTestAdressesPersonnesImpl extends ServiceBatchCommunImpl
{
    @Autowired()
    @Qualifier("utilitairetransactionsurscrolliteratorsupportservice")
    protected UtilitaireTransactionSurScrollIteratorSupportService transactionScrollIterator;

    @Autowired()
    private ITestAdressesPersonnesService testAdressesPersonnesService;

    public TraitementApurementTestAdressesPersonnesImpl()
    {
        super();
    }

    @Value("${traitementapurementadressespersonnes.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch)
    {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementapurementadressespersonnes.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }

    public void setTransactionScrollIterator(UtilitaireTransactionSurScrollIteratorSupportService transactionScrollIterator)
    {
        this.transactionScrollIterator = transactionScrollIterator;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        log.info(">>> debut methode traiterBatch");
        TransactionStatus statusTransactionEcriture = transactionScrollIterator.demarrerTransactionEnglobeeDeMiseAJourDUnLot();
        ScrollIterator iterPersonne = testAdressesPersonnesService.rechercherToutesLesPersonnesIterator(100);
        try
        {
            while (iterPersonne.hasNext())
            {
                // PersonneTest personneTest = iterPersonne.next();
                PersonneTest personneTest = (PersonneTest) iterPersonne.nextObjetMetier();
                /* Stockage des adresses qui vont être détachées pour traitement ultérieur */
                List<AdresseTest> listeAdresse = new ArrayList<>();
                listeAdresse.addAll(personneTest.getListeAdresses());
                /* Détacher les adresses de la personne en cours de suppression */
                for (AdresseTest adresseTest : listeAdresse)
                {
                    personneTest.detacherUneAdresse(adresseTest);
                }
                testAdressesPersonnesService.supprimerPersonne(personneTest);
                Iterator<AdresseTest> iterAdresse = listeAdresse.iterator();
                while (iterAdresse.hasNext())
                {
                    AdresseTest adresseTest = iterAdresse.next();
                    if (adresseTest.getListePersonnes() != null && adresseTest.getListePersonnes().isEmpty())
                    {
                        testAdressesPersonnesService.supprimerAdresse(adresseTest);
                    }
                }
            }
            transactionScrollIterator.commiterTransactionEnglobeeDeMiseAJourDUnLot(statusTransactionEcriture);
        }
        catch (RuntimeException rune)
        {
            log.warn("erreur", rune);
            transactionScrollIterator.rollBackSurTransactionEnglobeeDeMiseAJourDUnLot(statusTransactionEcriture);
        }
    }

}
