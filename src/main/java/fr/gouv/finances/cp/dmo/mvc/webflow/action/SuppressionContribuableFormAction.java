/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.mvc.form.SuppressionContribuableForm;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.exception.VerrouillageOptimisteException;

/**
 * Action SuppressionContribuableFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class SuppressionContribuableFormAction extends FormAction
{

    private static final Logger log = LoggerFactory.getLogger(SuppressionContribuableFormAction.class);

    public static final String LISTECONTRIBUABLES_FLOWSCOPE = "listecontribuable";

    public static final String LISTESELECTEDIDS_FLOWSCOPE = "selectedids";

    public static final String LISTEDESCONTRIBUABLESASUPPRIMER_REQUESTSCOPE = "listedescontribuablesasupprimer";

    public static final String ERREURENTREEFLUX_FLOWSCOPE = "erreurentreflux";

    private IContribuableService contribuableserviceso;

    public SuppressionContribuableFormAction()
    {
        super();
    }

    public SuppressionContribuableFormAction(Class<?> arg0)
    {
        super(arg0);
    }

    public IContribuableService getContribuableserviceso()
    {
        return contribuableserviceso;
    }

    public void setContribuableserviceso(IContribuableService contribuableserviceso)
    {
        this.contribuableserviceso = contribuableserviceso;
    }

    /**
     * methode Executer supprimer
     * 
     * @param context param
     * @return event
     */
    public Event executerSupprimer(RequestContext context)
    {
        // Lecture de la liste des contribuables à supprimer dans le
        // formulaire
        SuppressionContribuableForm supp =
            (SuppressionContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                SuppressionContribuableForm.class);

        List listecontribuableasupprimer = supp.getListeDesContribuablesASupprimer();

        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        try
        {
            contribuableserviceso.supprimerDesContribuablesParticuliers(listecontribuableasupprimer, personne.getUid());
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rgExc.getMessage());
            return result("incorrectdata");
        }
        catch (VerrouillageOptimisteException verrOptimisteExc)
        {
            log.error(verrOptimisteExc.getMessage(), verrOptimisteExc);
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, verrOptimisteExc.getMessage());
            return error();
        }

        // MAJ de la liste des contribuables dans le contexte du flux
        CheckboxSelectUtil.supprimeDesElementsDansLeTableau(context, listecontribuableasupprimer, "rechcont");

        return success();
    }

    /**
     * methode Initialiser formulaire
     * 
     * @param context param
     * @return event
     */
    public Event initialiserFormulaire(RequestContext context)
    {
        SuppressionContribuableForm suppressionContribuableForm;
        suppressionContribuableForm = (SuppressionContribuableForm) context.getFlowScope().get(getFormObjectName());

        Collection collASupprimer =
            CheckboxSelectUtil.extraitLesElementsSelectionnesDansLeFluxParent(context, "rechcont");

        List listeAsupprimer = new ArrayList(collASupprimer);
        if (!CheckboxSelectUtil.auMoinsUnElementSelectionne(listeAsupprimer))
        {
            return error();
        }
        suppressionContribuableForm.setListeDesContribuablesASupprimer(listeAsupprimer);
        return success();

    }

    /**
     * methode Recharger entree flux
     * 
     * @param context param
     * @return event
     */
    public Event rechargerEntreeFlux(RequestContext context)
    {
        try
        {
            // Recupération des paramètres en entrée du Flux
            List listecontribuable = (List) CheckboxSelectUtil.litLesElementsDuTableau(context, "rechcont");

            // Réactualisation de la liste des contribuables à
            // partir de leur id
            List<Contribuable> listecontribuableactualisee =
                contribuableserviceso.actualiserListeContribuables(new ArrayList<Contribuable>(listecontribuable));

            CheckboxSelectUtil.remplaceLesElementsDuTableau(context, listecontribuableactualisee, "rechcont");
            return success();
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            return error();
        }
    }

}