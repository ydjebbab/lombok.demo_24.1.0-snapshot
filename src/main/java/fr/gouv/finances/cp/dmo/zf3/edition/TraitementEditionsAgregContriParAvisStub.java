/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementEditionsAgregContriParAvisStub.java
 *
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import java.util.ArrayList;
import java.util.Collection;

import fr.gouv.finances.cp.dmo.techbean.StatistiquesContribuables;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunStubImpl;

/**
 * Class TraitementEditionsAgregContriParAvisStub DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementEditionsAgregContriParAvisStub extends AbstractServiceEditionCommunStubImpl
{
    /**
     * Constructeur
     */
    public TraitementEditionsAgregContriParAvisStub()
    {
        super();
    }

    /**
     * methode Creates the bean collection : DGFiP.
     * 
     * @return collection
     */
    public static Collection<StatistiquesContribuables> createBeanCollection()
    {
        Collection<StatistiquesContribuables> list = new ArrayList<>();
        for (int i = 1; i < 100; i++)
        {
            list.add(new StatistiquesContribuables("utilisateur" + i, Long.valueOf(i), Long.valueOf(3 * (long) i), Long
                .valueOf(i), "Impôt sur le revenu", Double.valueOf(96 * (double) i), Double.valueOf(5 * (double) i)));
        }
        return list;
    }

}
