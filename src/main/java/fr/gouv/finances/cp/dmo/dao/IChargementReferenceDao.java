/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ChargementReferenceDao.java
 *
 */
package fr.gouv.finances.cp.dmo.dao;

import org.dom4j.Document;

import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa;

/**
 * Interface ChargementReferenceDao
 * 
 * @author chouard-cp
 * @author CF: migration vers JPA
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public interface IChargementReferenceDao extends BaseDaoJpa
{

    /**
     * methode Load civilite from xml
     * 
     * @param document
     */
    public void loadCiviliteFromXml(final Document document);

    /**
     * methode Load pays from xml
     * 
     * @param document
     */
    public void loadPaysFromXml(final Document document);

    /**
     * methode Load situation fami from xml
     * 
     * @param document
     */
    public void loadSituationFamiFromXml(final Document document);

    /**
     * methode Load type impot from xml
     * 
     * @param document
     */
    public void loadTypeImpotFromXml(final Document document);

    /**
     * methode Unload pays to xml
     * 
     * @return document
     */
    public Document unloadPaysToXml();

    /**
     * methode Unload type impot to xml
     * 
     * @return document
     */
    public Document unloadTypeImpotToXml();
}
