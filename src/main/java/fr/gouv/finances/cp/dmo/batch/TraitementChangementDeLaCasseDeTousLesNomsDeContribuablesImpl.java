/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.batch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;

/**
 * Batch utilisant un service démontrant l'utilisation d'un scrolliterator pour mettre à jour tous les contribuables.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Service("traitementchangementdelacassedetouslesnomsdecontribuables")
@Profile("batch")
@Lazy(true)
public class TraitementChangementDeLaCasseDeTousLesNomsDeContribuablesImpl extends ServiceBatchCommunImpl
{

    @Autowired
    private IContribuableService contribuableService;
    
    public TraitementChangementDeLaCasseDeTousLesNomsDeContribuablesImpl()
    {
        super(); 
    }

    @Value("${traitementadhesionsmensualisation.nombatch}")
    @Override
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }

    public IContribuableService getContribuableService()
    {
        return contribuableService;
    }

    public void setContribuableService(IContribuableService contribuableService)
    {
        this.contribuableService = contribuableService;
    }

    @Override
    public void traiterBatch()
    {
        contribuableService.changerLaCasseDeTousLesNomsDeContribuables();
    }
}
