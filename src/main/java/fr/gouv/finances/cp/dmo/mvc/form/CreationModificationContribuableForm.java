/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.cp.dmo.entite.AdresseContribuable;
import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.webflow.FormMemento;
import fr.gouv.finances.lombok.webflow.Memento;
import fr.gouv.finances.lombok.webflow.MementoFormStatePersister;
import fr.gouv.finances.lombok.webflow.MementoOriginator;

/**
 * Class CreationModificationContribuableForm
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */

public class CreationModificationContribuableForm implements Serializable, MementoOriginator
{
    private static final Logger log = LoggerFactory.getLogger(CreationModificationContribuableForm.class);

    private static final long serialVersionUID = 1L;

    private Contribuable contribuable;

    private AdresseContribuable adresseContribuable = new AdresseContribuable();

    private String typeDeContribuable;

    public CreationModificationContribuableForm()
    {
        super();
    }

    public void setTypeDeContribuable(String typeDeContribuable)
    {
        this.typeDeContribuable = typeDeContribuable;
    }

    public boolean estContribuableParticulier()
    {
        return contribuable instanceof ContribuablePar;
    }

    public AdresseContribuable getAdresse()
    {
        return adresseContribuable;
    }

    public Contribuable getContribuable()
    {
        return contribuable;
    }

    public String getTypeDeContribuable()
    {
        return typeDeContribuable;
    }

    public void setAdresse(AdresseContribuable adresseContribuable)
    {
        this.adresseContribuable = adresseContribuable;
    }

    public void setContribuable(Contribuable contribuable)
    {
        this.contribuable = contribuable;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return memento
     * @see fr.gouv.finances.lombok.webflow.MementoOriginator#createMemento()
     */
    @Override
    public Memento createMemento()
    {
        log.debug(">>> Debut methode createMemento();");
        FormMemento formMemento = new FormMemento(this);
        return formMemento;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param memento
     * @param level
     * @see fr.gouv.finances.lombok.webflow.MementoOriginator#setMemento(fr.gouv.finances.lombok.webflow.Memento, int)
     */
    @Override
    public void setMemento(Memento memento, int level)
    {
        log.debug(">>> Debut methode setMemento();");
        FormMemento formMemento = (FormMemento) memento;
        // Rappel du formulaire initial
        CreationModificationContribuableForm formulaireOriginal =
            (CreationModificationContribuableForm) formMemento.getObjectDeepClone();

        try
        {
            switch (level)
            {
                // Restauration des informations de l'écran
                // principal de modification, en cas d'annulation
                // des modifications (
                case MementoFormStatePersister.LEVEL1:
                    // Restauration des informations lors du retour
                    // à la page précédent l'état "normalisation de
                    // l'adresseContribuable"

                    // Pas de break : donc execute le meme code pour Level 1 ou 2
                case MementoFormStatePersister.LEVEL2:
                    // Copie dans le formulaire des propriétés contribuable
                    // sauvegardées
                    PropertyUtils.copyProperties(this.contribuable, formulaireOriginal.getContribuable());

                    // Copie dans le formulaire des propriétés contribuable
                    // sauvegardées
                    if (contribuable instanceof ContribuablePar)
                    {
                        AdresseContribuable adresseCourante = ((ContribuablePar) contribuable).getAdressePrincipale();

                        List lesAdresses = ((ContribuablePar) formulaireOriginal.getContribuable()).getListeAdresses();
                        if (lesAdresses != null && lesAdresses.size() > 0)
                        {
                            AdresseContribuable adresseOriginale = (AdresseContribuable) lesAdresses.get(0);
                            PropertyUtils.copyProperties(adresseCourante, adresseOriginale);
                        }
                    }
                    break;
                case MementoFormStatePersister.LEVEL3:
                    PropertyUtils.copyProperties(this.contribuable, formulaireOriginal.getContribuable());
                    break;
                default:
                    break;
            }

        }
        catch (IllegalAccessException e)
        {
            throw new ProgrammationException(e);
        }
        catch (InvocationTargetException e)
        {
            throw new ProgrammationException(e);
        }
        catch (NoSuchMethodException e)
        {
            throw new ProgrammationException(e);
        }
    }

}
