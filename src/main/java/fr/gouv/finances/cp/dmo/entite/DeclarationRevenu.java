package fr.gouv.finances.cp.dmo.entite;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;

/**
 * @author celfer Date: 19 déc. 2019
 */
@Entity
@Table(name = "DECLARATION_REVENU")
@SequenceGenerator(name = "DECLARATION_REVENU_ID_GENERATOR", sequenceName = "SEQ_DECLARATION_REVENU", allocationSize = 1)
public class DeclarationRevenu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DECLARATION_REVENU_ID_GENERATOR")
    @Column(name = "id")
    private Long id;

    @Column(nullable = true, name = "NUMEROFISCAL")
    private Long numeroFiscal;

    @Column(nullable = true, name = "MONTANTREVENU")
    private int montantRevenu;

    @Column(nullable = true, name = "TAUXPRELEVEMENT")
    private int tauxPrelevement;

    @Column(nullable = true, name = "REDUCTION")
    private int reduction;

    @Column(nullable = true, name = "CREDITIMPOT")
    private int creditImpot;

    @Column(nullable = true, name = "DONEFFECTUE")
    private int donEffectue;

    @Column(nullable = true, name = "CONTRIBAUDIOPUBLIC")
    private boolean contributionAudiovisuelPublic;

    @Column(nullable = true, name = "INFORMATIONS")
    private String informations;

    @OneToMany(fetch = FetchType.LAZY)  // unidirectionnel
    @JoinColumn(name = "id_declarationrevenu")
    private List<RevenuFoncier> listeRevenuFoncier;

    @OneToMany(fetch = FetchType.LAZY) // unidirectionnel
    @JoinColumn(name = "id_declarationrevenu")
    private List<RevenuMobilier> listeRevenuMobilier;

    @ManyToOne // bidirectionnel
    @JoinColumn(nullable = false, name = "CONT_ID_LISTEDECLARATIONREVENU")
    private ContribuablePar contribuablePar;

    public DeclarationRevenu()
    {
        super();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public int getTauxPrelevement()
    {
        return tauxPrelevement;
    }

    public void setTauxPrelevement(int tauxPrelevement)
    {
        this.tauxPrelevement = tauxPrelevement;
    }

    public int getReduction()
    {
        return reduction;
    }

    public void setReduction(int reduction)
    {
        this.reduction = reduction;
    }

    public int getCreditImpot()
    {
        return creditImpot;
    }

    public void setCreditImpot(int creditImpot)
    {
        this.creditImpot = creditImpot;
    }

    public Long getNumeroFiscal()
    {
        return numeroFiscal;
    }

    public void setNumeroFiscal(Long numeroFiscal)
    {
        this.numeroFiscal = numeroFiscal;
    }

    public int getMontantRevenu()
    {
        return montantRevenu;
    }

    public void setMontantRevenu(int montantRevenu)
    {
        this.montantRevenu = montantRevenu;
    }

    public int getDonEffectue()
    {
        return donEffectue;
    }

    public void setDonEffectue(int donEffectue)
    {
        this.donEffectue = donEffectue;
    }

    public boolean isContributionAudiovisuelPublic()
    {
        return contributionAudiovisuelPublic;
    }

    public void setContributionAudiovisuelPublic(boolean contributionAudiovisuelPublic)
    {
        this.contributionAudiovisuelPublic = contributionAudiovisuelPublic;
    }

    public String getInformations()
    {
        return informations;
    }

    public void setInformations(String informations)
    {
        this.informations = informations;
    }

    public ContribuablePar getContribuablePar()
    {
        return contribuablePar;
    }

    public void setContribuablePar(ContribuablePar contribuablePar)
    {
        this.contribuablePar = contribuablePar;
    }

    public List<RevenuFoncier> getListeRevenuFoncier()
    {
        return listeRevenuFoncier;
    }

    public void setListeRevenuFoncier(List<RevenuFoncier> listeRevenuFoncier)
    {
        this.listeRevenuFoncier = listeRevenuFoncier;
    }

    public List<RevenuMobilier> getListeRevenuMobilier()
    {
        return listeRevenuMobilier;
    }

    public void setListeRevenuMobilier(List<RevenuMobilier> listeRevenuMobilier)
    {
        this.listeRevenuMobilier = listeRevenuMobilier;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((contribuablePar == null) ? 0 : contribuablePar.hashCode());
        result = prime * result + (contributionAudiovisuelPublic ? 1231 : 1237);
        result = prime * result + creditImpot;
        result = prime * result + donEffectue;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((informations == null) ? 0 : informations.hashCode());
        result = prime * result + ((listeRevenuFoncier == null) ? 0 : listeRevenuFoncier.hashCode());
        result = prime * result + ((listeRevenuMobilier == null) ? 0 : listeRevenuMobilier.hashCode());
        result = prime * result + montantRevenu;
        result = prime * result + ((numeroFiscal == null) ? 0 : numeroFiscal.hashCode());
        result = prime * result + reduction;
        result = prime * result + tauxPrelevement;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DeclarationRevenu other = (DeclarationRevenu) obj;
        if (contribuablePar == null)
        {
            if (other.contribuablePar != null)
                return false;
        }
        else if (!contribuablePar.equals(other.contribuablePar))
            return false;
        if (contributionAudiovisuelPublic != other.contributionAudiovisuelPublic)
            return false;
        if (creditImpot != other.creditImpot)
            return false;
        if (donEffectue != other.donEffectue)
            return false;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (informations == null)
        {
            if (other.informations != null)
                return false;
        }
        else if (!informations.equals(other.informations))
            return false;
        if (listeRevenuFoncier == null)
        {
            if (other.listeRevenuFoncier != null)
                return false;
        }
        else if (!listeRevenuFoncier.equals(other.listeRevenuFoncier))
            return false;
        if (listeRevenuMobilier == null)
        {
            if (other.listeRevenuMobilier != null)
                return false;
        }
        else if (!listeRevenuMobilier.equals(other.listeRevenuMobilier))
            return false;
        if (montantRevenu != other.montantRevenu)
            return false;
        if (numeroFiscal == null)
        {
            if (other.numeroFiscal != null)
                return false;
        }
        else if (!numeroFiscal.equals(other.numeroFiscal))
            return false;
        if (reduction != other.reduction)
            return false;
        if (tauxPrelevement != other.tauxPrelevement)
            return false;
        return true;
    }

}
