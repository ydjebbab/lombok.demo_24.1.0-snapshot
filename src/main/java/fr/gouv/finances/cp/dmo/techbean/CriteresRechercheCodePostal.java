/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : CriteresRechercheCodePostal.java
 *
 */
package fr.gouv.finances.cp.dmo.techbean;

import java.util.Map;
import java.util.TreeMap;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * CriteresRechercheCodePostal
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class CriteresRechercheCodePostal extends BaseTechBean
{
    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 2149863586205484063L;

    /** Constant : tid_typesderecherche. */
    private static final int[] tid_typesderecherche = {1, 2, 3};

    /** Constant : tname_typesderecherche. */
    private static final String[] tname_typesderecherche =
        {"Afficher les codes postaux", "Afficher les codes postaux et les CEDEX", "Afficher les CEDEX"};

    /** code. */
    private String code;

    /** ville. */
    private String ville;

    /** departement. */
    private String departement;

    /** region. */
    private String region;

    /** cedex ou et cp. */
    private int cedexOuEtCp;

    /**
     * Instanciation de criteres recherche code postal.
     */
    public CriteresRechercheCodePostal()
    {
        super();
        // positionnement du type de recherche par défaut
        this.cedexOuEtCp = 1;
    }

    /**
     * Accesseur de l attribut typesderecherche.
     * 
     * @return typesderecherche
     */
    public static final Map getTypesderecherche()
    {
        Map<Integer, String> uneHashMap = new TreeMap<>();

        for (int i = 0; i < tid_typesderecherche.length; i++)
        {
            uneHashMap.put(Integer.valueOf(tid_typesderecherche[i]), tname_typesderecherche[i]);
        }
        return uneHashMap;
    }

    /**
     * Accesseur de l attribut cedex ou et cp.
     * 
     * @return cedex ou et cp
     */
    public int getCedexOuEtCp()
    {
        return cedexOuEtCp;
    }

    /**
     * Accesseur de l attribut code.
     * 
     * @return code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * Accesseur de l attribut departement.
     * 
     * @return departement
     */
    public String getDepartement()
    {
        return departement;
    }

    /**
     * Accesseur de l attribut region.
     * 
     * @return region
     */
    public String getRegion()
    {
        return region;
    }

    /**
     * Accesseur de l attribut ville.
     * 
     * @return ville
     */
    public String getVille()
    {
        return ville;
    }

    /**
     * Modificateur de l attribut cedex ou et cp.
     * 
     * @param cedexouetcp le nouveau cedex ou et cp
     */
    public void setCedexOuEtCp(int cedexouetcp)
    {
        this.cedexOuEtCp = cedexouetcp;
    }

    /**
     * Modificateur de l attribut code.
     * 
     * @param code le nouveau code
     */
    public void setCode(String code)
    {
        this.code = code;
    }

    /**
     * Modificateur de l attribut departement.
     * 
     * @param departement le nouveau departement
     */
    public void setDepartement(String departement)
    {
        this.departement = departement;
    }

    /**
     * Modificateur de l attribut region.
     * 
     * @param region le nouveau region
     */
    public void setRegion(String region)
    {
        this.region = region;
    }

    /**
     * Modificateur de l attribut ville.
     * 
     * @param ville le nouveau ville
     */
    public void setVille(String ville)
    {
        this.ville = ville;
    }

}
