/*
 * Copyright (c) 2018 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.stereotype.Component;

/**
 * Classe permettant de logguer les propriétés de l'environnement.
 */
@Component
public class LombokPropertiesLogger
{

    /** Constant : log. */
    private static final Logger log = LoggerFactory.getLogger(LombokPropertiesLogger.class);

    /** environment. */
    @Resource
    private ConfigurableEnvironment environment;

    /**
     * methode Handle event : evenement lorsque l'application est préparée par boot.
     *
     * @param event
     */
    @EventListener
    public void handleEvent(ContextRefreshedEvent event)
    {
        printProperties();
    }

    /**
     * methode Prints the properties : log des propriétés.
     */
    @Conditional(OnLombokPropertiesLoggerDebug.class)
    public void printProperties()
    {

        findPropertiesPropertySources().stream().forEach(propertySource -> Arrays.asList(propertySource.getPropertyNames()).stream()
            .sorted().forEach(property -> log.info("{}={}", property, environment.getProperty(property))));

    }

    /**
     * methode Find properties property sources : récuperer les sources de propriétés.
     *
     * @return list
     */
    private List<EnumerablePropertySource> findPropertiesPropertySources()
    {
        List<EnumerablePropertySource> propertiesPropertySources = new LinkedList<>();
        for (PropertySource<?> propertySource : environment.getPropertySources())
        {
            if (propertySource instanceof EnumerablePropertySource)
            {
                propertiesPropertySources.add((EnumerablePropertySource) propertySource);
            }
        }
        return propertiesPropertySources;
    }

    /**
     * Class OnLombokPropertiesLoggerDebug condition: si le logger est en debug.
     */
    public class OnLombokPropertiesLoggerDebug implements Condition
    {

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see org.springframework.context.annotation.Condition#matches(org.springframework.context.annotation.ConditionContext,
         *      org.springframework.core.type.AnnotatedTypeMetadata)
         */
        @Override
        public boolean matches(ConditionContext arg0, AnnotatedTypeMetadata arg1)
        {
            return log.isDebugEnabled();
        }
    }
}
