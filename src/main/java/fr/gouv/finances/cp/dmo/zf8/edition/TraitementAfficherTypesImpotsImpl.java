/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : TraitementLancerTacheLongueImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf8.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import fr.gouv.finances.cp.dmo.editionbean.EditionTypeImpot;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.service.IReferenceService;
import fr.gouv.finances.cp.dmo.techbean.CalculMontants;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;

/**
 * Class TraitementAfficherTypesImpotsImpl DGFiP.
 *
 * @author amleplatinec-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementAfficherTypesImpotsImpl extends AbstractServiceEditionCommunImpl
{

    /** referenceserviceso. */
    private IReferenceService referenceserviceso;

    /**
     * Constructeur
     */
    public TraitementAfficherTypesImpotsImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param parametresEdition
     * @return collection
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerDatasource(java.util.Map)
     */
    @Override
    public Collection creerDatasource(Map parametresEdition)
    {

        Collection<EditionTypeImpot> lesEditionTypeImpot = new ArrayList<>();
        List<TypeImpot> lesTypesImpots = referenceserviceso.rechercherTousLesTypesImpots();
        if (lesTypesImpots != null && lesTypesImpots.size() > 0)
        {
            for (int i = 0; i < lesTypesImpots.size(); i++)
            {
                EditionTypeImpot uneEditionTypeImpot = new EditionTypeImpot();

                uneEditionTypeImpot.setCodeImpot(lesTypesImpots.get(i).getCodeImpot());
                uneEditionTypeImpot.setNomImpot(lesTypesImpots.get(i).getNomImpot());
                uneEditionTypeImpot.setEstDestineAuxParticuliers(lesTypesImpots.get(i).getEstDestineAuxParticuliers());
                uneEditionTypeImpot
                    .setEstOuvertALaMensualisation(lesTypesImpots.get(i).getEstOuvertALaMensualisation());
                uneEditionTypeImpot.setJourLimiteAdhesion(lesTypesImpots.get(i).getJourLimiteAdhesion());
                uneEditionTypeImpot.setSeuil(lesTypesImpots.get(i).getSeuil());
                uneEditionTypeImpot.setTaux(lesTypesImpots.get(i).getTaux());
                uneEditionTypeImpot.getLesTypesImpots().addAll(lesTypesImpots);
                CalculMontants unCalculMontants = new CalculMontants();
                uneEditionTypeImpot.setPourcentageEstOuvertALaMensualisation(unCalculMontants
                    .calculerPourcentageEstOuvertALaMensualisation(lesTypesImpots));
                uneEditionTypeImpot.setDecompteEstOuvertALaMensualisation(unCalculMontants
                    .calculerDecompteEstOuvertALaMensualisation(lesTypesImpots));
                uneEditionTypeImpot.setDecompteJourLimiteAdhesion(unCalculMontants
                    .calculerDecompteJourLimiteAdhesion(lesTypesImpots));
                uneEditionTypeImpot.setMoyenneNonNullCodeImpot(unCalculMontants
                    .calculerMoyenneNonNullCodeImpot(lesTypesImpots));
                uneEditionTypeImpot.setPourcentageJourLimiteAdhesion(unCalculMontants
                    .calculerPourcentageJourLimiteAdhesion(lesTypesImpots));

                lesEditionTypeImpot.add(uneEditionTypeImpot);

            }
        }

        return lesEditionTypeImpot;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        String result = "TraitementAfficherTypesImpotsImpl";
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param parametresEdition
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {
        return null;
    }

    /**
     * Accesseur de l attribut referenceserviceso.
     *
     * @return referenceserviceso
     */
    public IReferenceService getReferenceserviceso()
    {
        return referenceserviceso;
    }

    /**
     * Modificateur de l attribut referenceserviceso.
     *
     * @param referenceserviceso le nouveau referenceserviceso
     */
    public void setReferenceserviceso(IReferenceService referenceserviceso)
    {
        this.referenceserviceso = referenceserviceso;
    }
}
