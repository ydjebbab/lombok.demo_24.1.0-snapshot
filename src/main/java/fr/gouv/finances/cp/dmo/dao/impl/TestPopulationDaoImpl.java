/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.ITestPopulationDao;
import fr.gouv.finances.cp.dmo.entite.HabitantTest;
import fr.gouv.finances.cp.dmo.entite.HabitantTest_;
import fr.gouv.finances.cp.dmo.entite.OrigineTest;
import fr.gouv.finances.cp.dmo.entite.VilleTest;
import fr.gouv.finances.cp.dmo.entite.VilleTest_;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;

/**
 * DAO TestPopulationDaoImpl.
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Repository("testPopulationDao")
@Transactional(transactionManager="transactionManager")
public class TestPopulationDaoImpl extends BaseDaoJpaImpl implements ITestPopulationDao
{
    private static final Logger log = LoggerFactory.getLogger(TestPopulationDaoImpl.class);

    public TestPopulationDaoImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param origineId
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.ITestPopulationDao#findVillesParOrigine(Long origineId)
     */
    @Override
    public List<VilleTest> findVillesParOrigine(Long origineId)
    {
        log.debug(">>> Debut methode findVillesParOrigine");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<VilleTest> criteriaQuery = criteriabuilder.createQuery(VilleTest.class);
        Root<VilleTest> villeTestRoot = criteriaQuery.from(VilleTest.class);
        villeTestRoot.fetch(VilleTest_.listeHabitants, JoinType.INNER);
        Join<VilleTest, HabitantTest> joinVilleHabitant = villeTestRoot.join(VilleTest_.listeHabitants);

        OrigineTest origineTest = entityManager.find(OrigineTest.class, origineId);

        Predicate predicate = criteriabuilder.conjunction();
        // Recherche dans toutes les villes d'habitants qui ont pour origine l'origine en paramètre
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(joinVilleHabitant.get(HabitantTest_.origine), origineTest));
        criteriaQuery.where(predicate);

        criteriaQuery.select(villeTestRoot);

        // Pour debug seulement
        TypedQuery<VilleTest> villeTestTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("villeTestTypedQuery : " + villeTestTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<VilleTest> villeTestTypedQueryListeResultat = villeTestTypedQuery.getResultList();
        log.debug("villeTestTypedQueryListeResultat : " + villeTestTypedQueryListeResultat);
        return villeTestTypedQueryListeResultat;
    }
}
