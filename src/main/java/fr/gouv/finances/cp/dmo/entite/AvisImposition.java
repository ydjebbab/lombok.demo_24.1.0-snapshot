/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */

package fr.gouv.finances.cp.dmo.entite;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Entité AvisImposition
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Entity
@Table(name = "AVISIMPOSITION_AVIS")
@SequenceGenerator(name = "AVISIMPOSITION_ID_GENERATOR", sequenceName = "SEQ_AVISIMPOSITION_AVIS", allocationSize = 1)
public class AvisImposition extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AVISIMPOSITION_ID_GENERATOR")
    @Column(name = "AVISIMPOSITION_ID")
    private Long id;

    @Column(nullable = false, name = "REFERENCE")
    private String reference;

    @Column(nullable = true, name = "MONTANT")
    private Double montant;

    @Column(nullable = true, name = "DATEROLE")
    private Date dateRole;

    // @Cascade(CascadeType.SAVE_UPDATE)
    @ManyToOne(cascade = javax.persistence.CascadeType.MERGE) // bidirectionnel
    @JoinColumn(nullable = false, name = "CONT_ID_LISTEAVISIMPOSITION")
    private ContribuablePar contribuablePar;

    @ManyToOne(fetch = FetchType.EAGER) // defaut est EAGER. Unidirectionnel.
    // @JoinColumn(name="FK_AVIS_TYIM_1") // FK dans TypeImpot
    @JoinColumn(name = "TYIM_ID_TYPEIMPOT", foreignKey = @ForeignKey(name = "FK_AVIS_TYIM_1"))
    private TypeImpot typeImpot;

    public AvisImposition()
    {
        super();
        this.reference = "";
    }

    public AvisImposition(ContribuablePar contribuablepar, Date daterole, Long id, Double montant, String reference,
        TypeImpot typeimpot)
    {
        super();
        this.contribuablePar = contribuablepar;
        this.dateRole = daterole;
        this.id = id;
        this.montant = montant;
        this.reference = reference;
        this.typeImpot = typeimpot;
    }

    public ContribuablePar getContribuablePar()
    {
        return contribuablePar;
    }

    public Date getDateRole()
    {
        return this.dateRole;
    }

    public Long getId()
    {
        return id;
    }

    public Double getMontant()
    {
        return montant;
    }

    public String getReference()
    {
        return reference;
    }

    public TypeImpot getTypeImpot()
    {
        return typeImpot;
    }

    public void setContribuablePar(ContribuablePar contribuablepar)
    {
        this.contribuablePar = contribuablepar;
    }

    public void setDateRole(Date daterole)
    {
        this.dateRole = daterole;
    }

    public void setMontant(Double montant)
    {
        this.montant = montant;
    }

    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public void setTypeImpot(TypeImpot typeimpot)
    {
        this.typeImpot = typeimpot;
    }

    /**
     * methode Verifier avis d imposition est attache a un type impot r g15 : DGFiP.
     * 
     * @param codeimpotacomparer param
     */
    public void verifierAvisDImpositionEstAttacheAUnTypeImpotRG15(Long codeimpotacomparer)
    {

        if (this.getTypeImpot() == null || this.getTypeImpot().getCodeImpot() == null
            || this.getTypeImpot().getCodeImpot().compareTo(codeimpotacomparer) != 0)
        {
            throw new RegleGestionException(Messages.getString("exception.avisimpositionnonrattacheacetypeimpot.part1")
                + this.getReference() + " " + Messages.getString("exception.avisimpositionnonrattacheacetypeimpot.part2"));

        }

    }

    // La clé métier est constituée par l'attribut reference
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param object
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }

        if (object == null || (object.getClass() != this.getClass()))
        {
            return false;
        }
        AvisImposition myObj = (AvisImposition) object;
        return (reference == null ? myObj.getReference() == null : reference.equals(myObj.getReference()));
    }

    /**
     * Hash code.
     * 
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int hash = hashCODEHASHINIT;
        int varCode = 0;

        varCode = (null == reference ? 0 : reference.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        return hash;
    }

}
