/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.session.ConcurrentSessionControlAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

/**
 * RestrictSessionAuthenticationStrategy.java exemple de mise en place d'un cas de nombre maximal d'utilisateurs
 * connectés
 *
 * @author amleplatinec Date: 21 sept. 2012
 */
public class RestrictSessionAuthenticationStrategy extends ConcurrentSessionControlAuthenticationStrategy
{

    /** max users. */
    private int maxUsers;

    /** session registry. */
    private SessionRegistry sessionRegistry;

    /**
     * Instanciation de restrict session authentication strategy.
     *
     * @param sessionRegistry 
     */
    public RestrictSessionAuthenticationStrategy(SessionRegistry sessionRegistry)
    {
        super(sessionRegistry);
        this.sessionRegistry = sessionRegistry;
    }

    /**
     * Accesseur de l attribut max users.
     *
     * @return max users
     */
    public int getMaxUsers()
    {
        return maxUsers;
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see org.springframework.security.web.authentication.session.ConcurrentSessionControlAuthenticationStrategy#onAuthentication(org.springframework.security.core.Authentication, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void onAuthentication(Authentication authentication, HttpServletRequest request, HttpServletResponse response)
    {

        if (sessionRegistry.getAllPrincipals().size() + 1 > maxUsers)
        {
            throw new SessionAuthenticationException("Nombre maximum d'utilisateurs atteint : reconnectez vous ultérieurement");
        }
        super.onAuthentication(authentication, request, response);
    }

    /**
     * Modificateur de l attribut max users.
     *
     * @param maxUsers le nouveau max users
     */
    public void setMaxUsers(int maxUsers)
    {
        this.maxUsers = maxUsers;
    }
}
