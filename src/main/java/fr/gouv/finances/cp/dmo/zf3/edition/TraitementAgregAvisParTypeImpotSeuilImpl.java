/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementAgregAvisParTypeImpotSeuilImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fr.gouv.finances.cp.dmo.service.IStatistiquesService;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;

/**
 * Class TraitementAgregAvisParTypeImpotSeuilImpl DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementAgregAvisParTypeImpotSeuilImpl extends AbstractServiceEditionCommunImpl
{

    /** statistiquesserviceso. */
    private IStatistiquesService statistiquesserviceso;

    /**
     * Constructeur
     */
    public TraitementAgregAvisParTypeImpotSeuilImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return collection
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerDatasource(java.util.Map)
     */
    @Override
    public Collection creerDatasource(Map parametresEdition)
    {
        Double seuil = null;
        if (parametresEdition != null)
        {
            seuil = (Double) parametresEdition.get("servicemetier.seuil");
        }

        // Exécution de la requête qui produit la source de données
        Collection data = statistiquesserviceso.calculerAgregationAvisImpositionParTypeImpotSeuil(seuil);
        return data;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        String result = "TraitementAgregAvisParTypeImpotSeuilImpl";
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {

        Double seuil = null;
        String utilisateur = null;

        if (parametresEdition != null)
        {
            seuil = (Double) parametresEdition.get("servicemetier.seuil");
            utilisateur = (String) parametresEdition.get("entete.utilisateur");
        }
        Map<Object, Object> parametresJasper = new HashMap<Object, Object>(2);
        parametresJasper.put("SEUIL", seuil);
        parametresJasper.put("UTILISATEUR", utilisateur);
        return parametresJasper;
    }

    /**
     * méthode à surcharger pour ajouter des éléments dans la description affichée à l'utilisateur.
     * 
     * @param ParametresEdition param
     * @return the description
     */
    @Override
    public String getDescription(Map ParametresEdition)
    {

        StringBuffer desc = new StringBuffer();
        desc.append(this.getNomEdition());
        if (ParametresEdition != null && ParametresEdition.size() > 0)
        {
            desc.append(" valeur du seuil : ");
            desc.append(ParametresEdition.values());
        }
        return desc.toString();
    }

    /**
     * Accesseur de l attribut statistiquesserviceso.
     * 
     * @return statistiquesserviceso
     */
    public IStatistiquesService getStatistiquesserviceso()
    {
        return statistiquesserviceso;
    }

    /**
     * Modificateur de l attribut statistiquesserviceso.
     * 
     * @param statistiquesserviceso le nouveau statistiquesserviceso
     */
    public void setStatistiquesserviceso(IStatistiquesService statistiquesserviceso)
    {
        this.statistiquesserviceso = statistiquesserviceso;
    }
}
