/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.entite;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;

/**
 * Entité VilleTest.
 *
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Entity
@Table(name = "VILLETEST_VILT")
@SequenceGenerator(name = "VILLE_ID_GENERATOR", sequenceName = "SEQ_VILLETEST_VILT", allocationSize = 1)
public class VilleTest extends BaseEntity
{
    private static final int _31 = 31;

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VILLE_ID_GENERATOR")
    @Column(name = "VILLE_ID")
    private Long id;

    @Column(name = "LIBELLE")
    private String libelle;

    @JoinColumn(name = "VILLE_ID")
    @OneToMany
    private Set<HabitantTest> listeHabitants = new HashSet<HabitantTest>();

    public VilleTest()
    {
        super();
    }

    public Long getId()
    {
        return id;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public Set<HabitantTest> getListeHabitants()
    {
        return listeHabitants;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public void setListeHabitants(Set<HabitantTest> listeHabitants)
    {
        this.listeHabitants = listeHabitants;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        VilleTest other = (VilleTest) obj;
        if (libelle == null)
        {
            if (other.libelle != null)
            {
                return false;
            }
        }
        else if (!libelle.equals(other.libelle))
        {
            return false;
        }
        return true;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = _31;
        int result = 1;
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        return result;
    }

}
