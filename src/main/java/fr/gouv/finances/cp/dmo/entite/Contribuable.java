/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */

package fr.gouv.finances.cp.dmo.entite;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;

/**
 * Entité Contribuable
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Entity
@Table(name = "CONTRIBUABLE_CONT")
@SequenceGenerator(name = "CONTRIBUABLE_CONT_ID_GENERATOR", sequenceName = "SEQ_CONTRIBUABLE_CONT", allocationSize = 1)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "CONTRIBUABLE_DIS")
public class Contribuable extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTRIBUABLE_CONT_ID_GENERATOR")
    @Column(name = "cont_id")
    protected Long id;

    @Column(name = "IDENTIFIANT")
    protected String identifiant;

    public Contribuable()
    {
        super();
        this.identifiant = "";
    }

    public Contribuable(Long id)
    {
        super();
        this.id = id;
    }

    public Contribuable(String identifiant)
    {
        super();
        this.identifiant = identifiant;
    }

    public Long getId()
    {
        return id;
    }

    public String getIdentifiant()
    {
        return identifiant;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setIdentifiant(String identifiant)
    {
        this.identifiant = identifiant;
    }

    // La clé métier est constituée par l'attribut identifiant
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param object
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }

        if (object == null || (object.getClass() != this.getClass()))
        {
            return false;
        }
        Contribuable myObj = (Contribuable) object;
        return (identifiant == null ? myObj.getIdentifiant() == null : identifiant.equals(myObj.getIdentifiant()));
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int hash = hashCODEHASHINIT;
        int varCode = 0;

        varCode = (null == identifiant ? 0 : identifiant.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        return hash;
    }

}
