/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ReferenceTypesImpotsForm.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.cp.dmo.entite.TypeImpot;

/**
 * Class ReferenceTypesImpotsForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ReferenceTypesImpotsForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** type impot. */
    private TypeImpot typeImpot;

    /**
     * Instanciation de reference types impots form.
     */
    public ReferenceTypesImpotsForm()
    {
        super();
    }

    /**
     * Gets the type impot.
     *
     * @return the type impot
     */
    public TypeImpot getTypeImpot()
    {
        return typeImpot;
    }

    /**
     * Sets the type impot.
     *
     * @param typeImpot the new type impot
     */
    public void setTypeImpot(TypeImpot typeImpot)
    {
        this.typeImpot = typeImpot;
    }

}
