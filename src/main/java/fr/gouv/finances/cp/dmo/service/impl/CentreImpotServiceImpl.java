package fr.gouv.finances.cp.dmo.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.dao.ICentreImpotDao;
import fr.gouv.finances.cp.dmo.entite.CentreImpot;
import fr.gouv.finances.cp.dmo.service.ICentreImpotService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;

/**
 * Implémentation de classe de service de gestion du centre des impôts
 * @author celinio fernandes
 * Date: Mar 11, 2020
 */
@Service("centreImpotService")
public class CentreImpotServiceImpl extends BaseServiceImpl implements ICentreImpotService
{
    private static final Logger log = LoggerFactory.getLogger(CentreImpotServiceImpl.class);

    @Autowired
    private ICentreImpotDao centreImpotDao;

    @Override
    public void ajouter(CentreImpot centreImpot)
    {
        log.debug(">>> Debut methode ajouter");
        centreImpotDao.add(centreImpot);
    }

    @Override
    public List<CentreImpot> rechercherTous()
    {
        log.debug(">>> Debut methode rechercherTous");
        List<CentreImpot> listeCentreImpot = centreImpotDao.findAll();
        return listeCentreImpot;
    }

}
