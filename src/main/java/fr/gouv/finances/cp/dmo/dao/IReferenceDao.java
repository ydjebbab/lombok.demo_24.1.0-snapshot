/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ReferenceDao.java
 *
 */
package fr.gouv.finances.cp.dmo.dao;

import java.util.Collection;
import java.util.List;

import fr.gouv.finances.cp.dmo.entite.Civilite;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa;

/**
 * Interface ReferenceDao
 * 
 * @author chouard-cp
 * @author CF: migration vers JPA
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public interface IReferenceDao extends BaseDaoJpa
{

    /**
     * methode Clear
     */
    public void clear();

    /**
     * methode Find civilite par code
     * 
     * @param code param
     * @return civilite
     */
    public Civilite findCiviliteParCode(Long code);

    /**
     * methode Find type impot par code impot
     * 
     * @param code param
     * @return type impot
     */
    public TypeImpot findTypeImpotParCodeImpot(Long code);

    /**
     * methode Find types impots mensualisables
     * 
     * @return list< type impot>
     */
    public List<TypeImpot> findTypesImpotsMensualisables();

    /**
     * Verifie si civilite utilise.
     * 
     * @param civilite param
     * @return true, si c'est civilite utilise
     */
    public boolean isCiviliteUtilise(Civilite civilite);

    /**
     * Verifie si code civilite deja utilise.
     * 
     * @param civilite param
     * @return true, si c'est code civilite deja utilise
     */
    public boolean isCodeCiviliteDejaUtilise(Civilite civilite);

    /**
     * Verifie si code type impot deja utilise.
     * 
     * @param typeImpot param
     * @return true, si c'est code type impot deja utilise
     */
    public boolean isCodeTypeImpotDejaUtilise(TypeImpot typeImpot);

    /**
     * Verifie si type impot est utilise.
     * 
     * @param typeImpot param
     * @return true, si c'est type impot utilise
     */
    public boolean isTypeImpotUtilise(TypeImpot typeImpot);

    /**
     * methode Save civilite
     * 
     * @param civilite param
     */
    public void saveCivilite(Civilite civilite);

    /**
     * methode Save type impot
     * 
     * @param typeImpot 
     */
    public void saveTypeImpot(TypeImpot typeImpot);

    /**
     * methode Supprimer types impots
     * 
     * @param elementsAsupprimer param
     */
    public void deleteTypesImpots(Collection elementsAsupprimer);
}
