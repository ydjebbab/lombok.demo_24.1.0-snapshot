/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : AvantGenerationMenuServiceImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.menu;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.mvc.webflow.action.TestTagsFormAction;
import fr.gouv.finances.lombok.menu.service.AvantGenerationMenuService;
import fr.gouv.finances.lombok.menu.techbean.BarreDeMenusHierarchiques;
import fr.gouv.finances.lombok.menu.techbean.RubriqueLien;
import fr.gouv.finances.lombok.menu.techbean.SousMenu;

/* Demo d'un intercepteur avant habilitation
 * exemple : selon l'origine de l'utilisateur on envoie sur adresse portail classqiue de l'application, sinon sur l'adresse ader  */

/**
 * Class AvantGenerationMenuServiceImpl
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
@Service("avantgenerationmenuserviceimpl")
public class AvantGenerationMenuServiceImpl implements AvantGenerationMenuService
{
    private static final Log log = LogFactory.getLog(AvantGenerationMenuServiceImpl.class);

    public AvantGenerationMenuServiceImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uneBarreDeMenusDynamique
     * @param parametreIntercepteur
     * @see fr.gouv.finances.lombok.menu.service.AvantGenerationMenuService#executerApresFiltrageMenu(fr.gouv.finances.lombok.menu.techbean.BarreDeMenusHierarchiques,
     *      java.lang.Object)
     */
    @Override
    public void executerApresFiltrageMenu(BarreDeMenusHierarchiques uneBarreDeMenusDynamique,
        Object parametreIntercepteur)
    {

        if ((parametreIntercepteur instanceof String)
            && (((String) parametreIntercepteur) == TestTagsFormAction.class.getName()))
        {
            Date maintenant = Calendar.getInstance().getTime();
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy", Locale.FRANCE);
            SousMenu premiersousmenu = (SousMenu) uneBarreDeMenusDynamique.listeDeSousMenus.get(0);
            RubriqueLien unerubriquelien = new RubriqueLien();

            unerubriquelien.libelle = "Test intercepteur élément dynamique : " + f.format(maintenant);
            unerubriquelien.url = "http://ulysse";
            unerubriquelien.verifierUrl = "N";
            unerubriquelien.commentaire = "Test de l'intercepteur avant generation menu" + this.getClass().getName();
            unerubriquelien.afficherSiRienDAutorise = "N";
            unerubriquelien.setUrlEstAutorisee(true);
            premiersousmenu.listeDeRubriques.add(unerubriquelien);
        }
        // rajout pour test récupération provenance adresse ip utilsateur
        if ((parametreIntercepteur instanceof String) && (((String) parametreIntercepteur).startsWith("10")))
        {
            if (log.isDebugEnabled())
            {
                log.debug("adresseIpTransmise correspond a une personne interne" + (String) parametreIntercepteur);
            }

            // ajout nouveau sous-menu
            SousMenu smAccesCompas = new SousMenu();
            smAccesCompas.libelle = "Retour Compas";
            smAccesCompas.afficherSiRienDAutorise = "N";
            smAccesCompas.commentaire = "Retourner à Compas";

            RubriqueLien uneRubriqueLien = new RubriqueLien();
            uneRubriqueLien.libelle = "autres opérations";

            /*
             * compléter ici l url vers laquelle doit accéder l'utilisateur ex cps.appli.cp ou cps.finances.ader.gouv.fr
             */
            uneRubriqueLien.url = "http://cps.appli.cp";
            uneRubriqueLien.verifierUrl = "N";
            uneRubriqueLien.commentaire = "Retourner à Compas";
            uneRubriqueLien.afficherSiRienDAutorise = "N";
            uneRubriqueLien.setUrlEstAutorisee(true);

            smAccesCompas.listeDeRubriques.add(uneRubriqueLien);

            uneBarreDeMenusDynamique.listeDeSousMenus.add(smAccesCompas);
        }
        if ((parametreIntercepteur instanceof String) && (((String) parametreIntercepteur).startsWith("161")))
        {
            if (log.isDebugEnabled())
            {
                log.debug("adresseIpTransmise correspond a une personne externe" + (String) parametreIntercepteur);
            }

            // ajout nouveau sous-menu
            SousMenu smAccesCompas = new SousMenu();
            smAccesCompas.libelle = "Retour Compas";
            smAccesCompas.afficherSiRienDAutorise = "N";
            smAccesCompas.commentaire = "Retourner à Compas";

            RubriqueLien uneRubriqueLien = new RubriqueLien();
            uneRubriqueLien.libelle = "autres opérations";

            /*
             * compléter ici l url vers laquelle doit accéder l'utilisateur ex cps.appli.cp ou cps.finances.ader.gouv.fr
             */
            uneRubriqueLien.url = "http://cps.finances.ader.gouv.fr";
            uneRubriqueLien.verifierUrl = "N";
            uneRubriqueLien.commentaire = "Retourner à Compas";
            uneRubriqueLien.afficherSiRienDAutorise = "N";
            uneRubriqueLien.setUrlEstAutorisee(true);

            smAccesCompas.listeDeRubriques.add(uneRubriqueLien);

            uneBarreDeMenusDynamique.listeDeSousMenus.add(smAccesCompas);
        }
    }

}
