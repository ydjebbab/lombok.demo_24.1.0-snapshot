/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */

package fr.gouv.finances.cp.dmo.zf3.edition;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import fr.gouv.finances.cp.dmo.editionbean.Casino;
import fr.gouv.finances.cp.dmo.editionbean.DgcpMontant;
import fr.gouv.finances.cp.dmo.editionbean.ResultatMensuelExploitation;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunStubImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Classe TraitementEditionLettreAuMaireStub pour la mise au point de l'édition de la lettre au maire.
 */
public class TraitementEditionLettreAuMaireStub extends AbstractServiceEditionCommunStubImpl
{

    /**
     * Constructeur
     */
    public TraitementEditionLettreAuMaireStub()
    {
        super();
    }

    /**
     * methode Creates the bean collection : .
     * 
     * @return collection
     */
    public static Collection<ResultatMensuelExploitation> createBeanCollection()
    {
        ResultatMensuelExploitation leRme = new ResultatMensuelExploitation();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE);
        Casino unCasino = new Casino();

        unCasino.setInseeCom("14562");

        unCasino.setNoServPub(1);
        unCasino.setLibelle("Casino de SAINT-AUBIN-SUR-MER");
        unCasino.setCodPostCompt("014018");
        unCasino.setCodePosteSie("014000");

        leRme.setCasino(unCasino);
        leRme.setAnneeDebutSaison("2011");
        leRme.setAnneeFinSaison("2012");
        leRme.setAnneeCivileExploitation("2011");
        leRme.setLibelleMoisCivil("Mai");
        leRme.setCasinoEnseigne("Casino de SAINT-AUBIN-SUR-MER");
        leRme.setCasinoImplantationLigne1("128 rue Pasteur");
        leRme.setCasinoImplantationLigne4("14750 ST AUBIN SUR MER");

        leRme.setComptableLibellePoste("COURSEULLES-SUR-MER");
        leRme.setComptableLigne2(" 4 RUE ARTHUR LEDUC");
        leRme.setComptableLigne4("14470 COURSEUILLES SUR MER");

        leRme.setCommuneCiviliteMaire("Monsieur Le Maire");
        leRme.setCommuneDenomination("CABOURG");
        leRme.setCommuneAdrLigne1("Avenue de la mer");
        leRme.setCommuneAdrLigne2("14390 CABOURG");

        leRme.setTrdMPbrl(new DgcpMontant("53302.60"));

        leRme.setTrdCsPbrl(new DgcpMontant("357115.80"));
        leRme.setTrdCaPbrl(new DgcpMontant("248558.30"));

        leRme.setMasMPbrl(new DgcpMontant("337914.47"));
        leRme.setMasCsPbrl(new DgcpMontant("1878403.16"));
        leRme.setMasCaPbrl(new DgcpMontant("1248384.48"));

        leRme.setrPrgMCommune(new DgcpMontant("10694.00"));
        leRme.setrPrgCsCommune(new DgcpMontant("41788.00"));
        leRme.setrPrgCaCommune(new DgcpMontant("33078.00"));

        leRme.setpTccMMontant(new DgcpMontant("25540.00"));
        leRme.setpTccCsMontant(new DgcpMontant("146532.00"));
        leRme.setpTccCaMontant(new DgcpMontant("98226.00"));

        leRme.setpPrgMAEmployer(new DgcpMontant("0.00"));
        leRme.setpPrgCsAEmployer(new DgcpMontant("19425.00"));
        leRme.setpPrgCaAEmployer(new DgcpMontant("16564.00"));

        try
        {
            leRme.setDateConstatationDroits(new Timestamp(dateFormat.parse("2013-02-11 16:30:51").getTime()));
        }
        catch (ParseException parsee)
        {
            throw new ProgrammationException(parsee);
        }

        List<ResultatMensuelExploitation> collection = new ArrayList<>();
        collection.add(leRme);
        return collection;

    }

}
