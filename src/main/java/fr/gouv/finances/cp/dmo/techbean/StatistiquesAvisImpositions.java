/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : StatistiquesAvisImpositions.java
 *
 */
package fr.gouv.finances.cp.dmo.techbean;

import java.util.Date;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Class StatistiquesAvisImpositions DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class StatistiquesAvisImpositions extends BaseTechBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** nombre avis. */
    private Long nombreAvis;

    /** montant total. */
    private Double montantTotal;

    /** montant moyen. */
    private Double montantMoyen;

    /** date role min. */
    private Date dateRoleMin;

    /** date role max. */
    private Date dateRoleMax;

    /** civilite. */
    private String civilite;

    /** code impot. */
    private Long codeImpot;

    /** nom impot. */
    private String nomImpot;

    /** annee. */
    private String annee;

    /**
     * Constructeur
     */
    public StatistiquesAvisImpositions()
    {
        super();
    }

    /**
     * Accesseur de l attribut annee.
     * 
     * @return annee
     */
    public String getAnnee()
    {
        return annee;
    }

    /**
     * Accesseur de l attribut civilite.
     * 
     * @return civilite
     */
    public String getCivilite()
    {
        return civilite;
    }

    /**
     * Accesseur de l attribut code impot.
     * 
     * @return code impot
     */
    public Long getCodeImpot()
    {
        return codeImpot;
    }

    /**
     * Accesseur de l attribut date role max.
     * 
     * @return date role max
     */
    public Date getDateRoleMax()
    {
        return dateRoleMax;
    }

    /**
     * Accesseur de l attribut date role min.
     * 
     * @return date role min
     */
    public Date getDateRoleMin()
    {
        return dateRoleMin;
    }

    /**
     * Accesseur de l attribut montant moyen.
     * 
     * @return montant moyen
     */
    public Double getMontantMoyen()
    {
        return montantMoyen;
    }

    /**
     * Accesseur de l attribut montant total.
     * 
     * @return montant total
     */
    public Double getMontantTotal()
    {
        return montantTotal;
    }

    /**
     * Accesseur de l attribut nombre avis.
     * 
     * @return nombre avis
     */
    public Long getNombreAvis()
    {
        return nombreAvis;
    }

    /**
     * Accesseur de l attribut nom impot.
     * 
     * @return nom impot
     */
    public String getNomImpot()
    {
        return nomImpot;
    }

    /**
     * Modificateur de l attribut annee.
     * 
     * @param annee le nouveau annee
     */
    public void setAnnee(String annee)
    {
        this.annee = annee;
    }

    /**
     * Modificateur de l attribut civilite.
     * 
     * @param civilite le nouveau civilite
     */
    public void setCivilite(String civilite)
    {
        this.civilite = civilite;
    }

    /**
     * Modificateur de l attribut code impot.
     * 
     * @param codeImpot le nouveau code impot
     */
    public void setCodeImpot(Long codeImpot)
    {
        this.codeImpot = codeImpot;
    }

    /**
     * Modificateur de l attribut date role max.
     * 
     * @param dateRoleMax le nouveau date role max
     */
    public void setDateRoleMax(Date dateRoleMax)
    {
        this.dateRoleMax = dateRoleMax;
    }

    /**
     * Modificateur de l attribut date role min.
     * 
     * @param dateRoleMin le nouveau date role min
     */
    public void setDateRoleMin(Date dateRoleMin)
    {
        this.dateRoleMin = dateRoleMin;
    }

    /**
     * Modificateur de l attribut montant moyen.
     * 
     * @param montantMoyen le nouveau montant moyen
     */
    public void setMontantMoyen(Double montantMoyen)
    {
        this.montantMoyen = montantMoyen;
    }

    /**
     * Modificateur de l attribut montant total.
     * 
     * @param montantTotal le nouveau montant total
     */
    public void setMontantTotal(Double montantTotal)
    {
        this.montantTotal = montantTotal;
    }

    /**
     * Modificateur de l attribut nombre avis.
     * 
     * @param nombreAvis le nouveau nombre avis
     */
    public void setNombreAvis(Long nombreAvis)
    {
        this.nombreAvis = nombreAvis;
    }

    /**
     * Modificateur de l attribut nom impot.
     * 
     * @param nomImpot le nouveau nom impot
     */
    public void setNomImpot(String nomImpot)
    {
        this.nomImpot = nomImpot;
    }

}
