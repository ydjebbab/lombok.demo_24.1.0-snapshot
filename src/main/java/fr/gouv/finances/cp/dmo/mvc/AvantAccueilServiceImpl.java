/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : AvantAccueilServiceImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.lombok.mvc.AvantAccueilService;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Exemple d'implémentation de AvantAccueilService afin de rajouter du code spécifique à l'utilisateur lors de l'accès à
 * la page d'accueil.
 *
 * @author chouard-cp
 * @author CF : passage XML vers annotations
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
@Service("AvantAccueilService")
public class AvantAccueilServiceImpl implements AvantAccueilService
{
    private static final Logger log = LoggerFactory.getLogger(AvantAccueilServiceImpl.class);

    @Autowired
    private IContribuableService contribuableService;

    public AvantAccueilServiceImpl()
    {
        super();
    }

    /**
     * méthode ou l'on rajoute le code spécifique /controles à effectuer.
     *
     * @param request param
     */
    @Override
    public void executerAvantPremierPassageSurAccueil(HttpServletRequest request)
    {
        // exemple de code métier
        ContribuablePar contribuablePar = new ContribuablePar();
        PersonneAnnuaire persAnnu = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        try
        {
            contribuablePar =
                contribuableService
                    .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID(persAnnu
                        .getUid());

            request.getSession().setAttribute("contextutil",
                "nombre d'avis d'imposition émis   " + contribuablePar.getListeAvisImposition().size());
        }
        catch (RegleGestionException exc)
        {
            log.error(exc.getMessage(), exc);
            request.getSession().setAttribute("contextutil", "nombre d'avis d'imposition émis   " + 0);
        }
    }

    /**
     * <pre>
     * méthode à implémenter si l'on veut supprimer de la session les éléments mis dans executer avantAccueil
     *  (exemple: info affichée sur la page d'accueil qui ne doit plus etre affichée lorsque l'on revient sur accueil).
     * </pre>
     *
     * @param request param
     */
    @Override
    public void mettreAJourSessionApresPassageAccueil(HttpServletRequest request)
    {
        request.getSession().setAttribute("contextutil", null);
        // request.getSession().removeAttribute("FLUX_ACCUEIL");
    }

}
