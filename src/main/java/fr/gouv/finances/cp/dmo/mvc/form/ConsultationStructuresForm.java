/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.util.List;

import fr.gouv.finances.lombok.apptags.tree.mvc.form.TreeForm;
import fr.gouv.finances.lombok.structure.bean.StructureCP;

/**
 * Class ConsultationStructuresForm 
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class ConsultationStructuresForm extends TreeForm
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** structure selectionnee. */
    private StructureCP structureSelectionnee;

    /** structures selectionnees. */
    private List<StructureCP> structuresSelectionnees;

    /** node ids. */
    private List<String> nodeIds;

    /** identitenominoe. */
    private String identitenominoe;

    public ConsultationStructuresForm()
    {
        super(); 
    }

    /**
     * Gets the identitenominoe.
     *
     * @return the identitenominoe
     */
    public String getIdentitenominoe()
    {
        return identitenominoe;
    }

    /**
     * Gets the node ids.
     *
     * @return the node ids
     */
    public List<String> getNodeIds()
    {
        return nodeIds;
    }

    /**
     * Gets the structure selectionnee.
     *
     * @return the structure selectionnee
     */
    public StructureCP getStructureSelectionnee()
    {
        return structureSelectionnee;
    }

    /**
     * Gets the structures selectionnees.
     *
     * @return the structures selectionnees
     */
    public List<StructureCP> getStructuresSelectionnees()
    {
        return structuresSelectionnees;
    }

    /**
     * Sets the identitenominoe.
     *
     * @param identitenominoe the new identitenominoe
     */
    public void setIdentitenominoe(String identitenominoe)
    {
        this.identitenominoe = identitenominoe;
    }

    /**
     * Sets the node ids.
     *
     * @param nodeIds the new node ids
     */
    public void setNodeIds(List<String> nodeIds)
    {
        this.nodeIds = nodeIds;
    }

    /**
     * Sets the structure selectionnee.
     *
     * @param structureSelectionnee the new structure selectionnee
     */
    public void setStructureSelectionnee(StructureCP structureSelectionnee)
    {
        this.structureSelectionnee = structureSelectionnee;
    }

    /**
     * Sets the structures selectionnees.
     *
     * @param structuresSelectionnees the new structures selectionnees
     */
    public void setStructuresSelectionnees(List<StructureCP> structuresSelectionnees)
    {
        this.structuresSelectionnees = structuresSelectionnees;
    }

}
