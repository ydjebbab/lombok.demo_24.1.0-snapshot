/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : TraitementLancerTacheLongueImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf8.edition;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.service.IReferenceService;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;

/**
 * Class TraitementAfficherTypesImpotsImpl DGFiP.
 *
 * @author amleplatinec-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class TraitementAfficherTypesImpotsJdbcImpl extends AbstractServiceEditionCommunImpl
{

    /** referenceserviceso. */
    private IReferenceService referenceserviceso;

    /**
     * Constructeur
     */
    public TraitementAfficherTypesImpotsJdbcImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param parametresEdition
     * @return collection
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerDatasource(java.util.Map)
     */
    @Override
    public Collection creerDatasource(Map parametresEdition)
    {

        List<TypeImpot> lesTypesImpots = referenceserviceso.rechercherTousLesTypesImpotsJdbc();

        return lesTypesImpots;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        String result = "TraitementAfficherTypesImpotsImpl";
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param parametresEdition
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {
        return null;
    }

    /**
     * Accesseur de l attribut referenceserviceso.
     *
     * @return referenceserviceso
     */
    public IReferenceService getReferenceserviceso()
    {
        return referenceserviceso;
    }

    /**
     * Modificateur de l attribut referenceserviceso.
     *
     * @param referenceserviceso le nouveau referenceserviceso
     */
    public void setReferenceserviceso(IReferenceService referenceserviceso)
    {
        this.referenceserviceso = referenceserviceso;
    }
}
