package fr.gouv.finances.cp.dmo.entite;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TypeImpot.class)
public abstract class TypeImpot_ {

	public static volatile SingularAttribute<TypeImpot, Boolean> estDestineAuxParticuliers;
	public static volatile SingularAttribute<TypeImpot, BigDecimal> seuil;
	public static volatile SingularAttribute<TypeImpot, Date> jourLimiteAdhesion;
	public static volatile SingularAttribute<TypeImpot, BigDecimal> taux;
	public static volatile SingularAttribute<TypeImpot, String> nomImpot;
	public static volatile SingularAttribute<TypeImpot, Boolean> estOuvertALaMensualisation;
	public static volatile SingularAttribute<TypeImpot, Long> id;
	public static volatile SingularAttribute<TypeImpot, Long> codeImpot;
	public static volatile SingularAttribute<TypeImpot, Integer> version;
	public static volatile SingularAttribute<TypeImpot, Date> jourOuvertureAdhesion;

}

