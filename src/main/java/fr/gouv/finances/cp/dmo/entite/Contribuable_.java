package fr.gouv.finances.cp.dmo.entite;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Contribuable.class)
public abstract class Contribuable_ {

	public static volatile SingularAttribute<Contribuable, String> identifiant;
	public static volatile SingularAttribute<Contribuable, Long> id;
	public static volatile SingularAttribute<Contribuable, Integer> version;

}

