package fr.gouv.finances.cp.dmo.entite;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PersonneTest.class)
public abstract class PersonneTest_ {

	public static volatile SingularAttribute<PersonneTest, Long> id;
	public static volatile SetAttribute<PersonneTest, AdresseTest> listeAdresses;
	public static volatile SingularAttribute<PersonneTest, Integer> version;
	public static volatile SingularAttribute<PersonneTest, String> nom;
	public static volatile SingularAttribute<PersonneTest, String> prenom;

}

