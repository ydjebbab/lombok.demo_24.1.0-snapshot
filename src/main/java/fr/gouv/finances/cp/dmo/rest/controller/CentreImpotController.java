package fr.gouv.finances.cp.dmo.rest.controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.gouv.finances.cp.dmo.entite.CentreImpot;
import fr.gouv.finances.cp.dmo.service.ICentreImpotService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Services REST liés au centre des impôts
 * @author celinio fernandes
 * Date: Mar 11, 2020
 */
@Path("/centreimpot")
@Api(value = "Centre impot REST", produces = MediaType.APPLICATION_JSON)
public class CentreImpotController
{

    private static final Logger log = LoggerFactory.getLogger(CentreImpotController.class);

    @Autowired
    ICentreImpotService centreImpotService;

    @POST
    @Path("/ajout/{commune}/{nbAgents}/{ouvert}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Ajout de CDI", httpMethod = "POST", notes = "Ajouter un CDI", response = Response.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "CDI valide et créé"),
            @ApiResponse(code = 400, message = "Mauvaise requête"),
            @ApiResponse(code = 500, message = "Erreur interne")})       
    public Response ajouterCentreImpot(@PathParam("commune") String commune, @PathParam("nbAgents") Integer nbAgents,
        @PathParam("ouvert") Boolean ouvert)
    {
        log.debug(">>> Debut methode ajouterCentreImpot");
        Response response;

        CentreImpot centreImpot = new CentreImpot();
        centreImpot.setCommune(commune);
        centreImpot.setNombreAgents(nbAgents);
        centreImpot.setOuvert(ouvert);

        centreImpotService.ajouter(centreImpot);

        response = Response.status(Status.CREATED).build();
        return response;
    }
    
    
    @GET
    @Path("/findAll")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Liste des CDI", httpMethod = "GET", notes = "Lister les CDI", response = Response.class)
    @ApiResponses(value = {            
            @ApiResponse(code = 400, message = "Mauvaise requête"),
            @ApiResponse(code = 500, message = "Erreur interne")})       
    public Response findAll()
    {
        log.debug(">>> Debut methode findAll");
        Response response;

        List<CentreImpot> listeCentreImpot = centreImpotService.rechercherTous();

        response = Response.status(Status.OK).entity(listeCentreImpot).build();
        return response;
    }
}
