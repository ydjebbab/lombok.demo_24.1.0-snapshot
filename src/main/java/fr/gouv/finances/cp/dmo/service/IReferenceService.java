/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ReferenceService.java
 *
 */
package fr.gouv.finances.cp.dmo.service;

import java.util.Collection;
import java.util.List;

import fr.gouv.finances.cp.dmo.entite.Civilite;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;

/**
 * Interface IReferenceService
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public interface IReferenceService
{

    /**
     * methode Charger list civilite : DGFiP.
     * 
     * @return list< civilite>
     */
    public List<Civilite> chargerListCivilite();

    /**
     * methode Enregistrerun type impot : DGFiP.
     * 
     * @param unTypeImpot param
     */
    public void enregistrerTypeImpot(TypeImpot unTypeImpot);

    /**
     * Accesseur de l attribut limite seuil.
     * 
     * @return limite seuil
     */
    public double getLimiteSeuil();

    /**
     * methode Rechercher tous les types impots : DGFiP.
     * 
     * @return list< type impot>
     */
    public List<TypeImpot> rechercherTousLesTypesImpots();

    /**
     * pour test jdbc.
     * 
     * @return list
     */
    public List<TypeImpot> rechercherTousLesTypesImpotsJdbc();

    /**
     * methode Rechercher types impots disponbiles a la mensualisation : DGFiP.
     * 
     * @return list< type impot>
     */
    public List<TypeImpot> rechercherTypesImpotsDisponbilesALaMensualisation();

    /**
     * methode Supprimer types impots : DGFiP.
     * 
     * @param elementsAsupprimer param
     */
    public void supprimerTypesImpots(Collection elementsAsupprimer);
}