/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : CreationModificationContribuableValidator.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.validator;

import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.mvc.form.CreationModificationContribuableForm;

/**
 * Class CreationModificationContribuableValidator DGFiP.
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */

public class CreationModificationContribuableValidator implements Validator
{
    private static final Logger log = LoggerFactory.getLogger(CreationModificationContribuableValidator.class);
    /**
     * Instanciation de creation modification contribuable validator.
     */
    public CreationModificationContribuableValidator()
    {
        super();
    }

    /**
     * methode Avertissement surface modification contribuable : DGFiP.
     *
     * @param command param
     * @param errors param
     */
    public void avertissementSurfaceModificationContribuable(CreationModificationContribuableForm command, Errors errors)
    {
        log.debug(">>> Debut methode avertissementSurfaceModificationContribuable");
        Contribuable unContribuable = command.getContribuable();

        if (unContribuable instanceof ContribuablePar)
        {
            ContribuablePar unContribuableParticulier = (ContribuablePar) unContribuable;

            // Contrôle du mail
            EmailValidator emailvalidator = EmailValidator.getInstance();
            if (!emailvalidator.isValid(unContribuableParticulier.getAdresseMail()))
            {
                // errors.rejectValue("contribuable.adresseMail",
                // "rejected.contribuable.adressemailobligatoire", "Addresse mail non valide");
                errors.reject(null, "Addresse mail non valide");
            }
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    public boolean supports(Class clazz)
    {
        return clazz.equals(CreationModificationContribuableForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param arg0
     * @param arg1
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    public void validate(Object arg0, Errors arg1)
    {
        log.debug(">>> Debut methode validate");
        this.validateSurfaceModificationContribuable((CreationModificationContribuableForm) arg0, arg1);
    }

    /**
     * methode Validate surface modification contribuable : DGFiP.
     *
     * @param command param
     * @param errors param
     */
    public void validateSurfaceModificationContribuable(CreationModificationContribuableForm command, Errors errors)
    {
        log.debug(">>> Debut methode validateSurfaceModificationContribuable");
        Contribuable unContribuable = command.getContribuable();
        // Contrôle de surface de l'identifiant
        if (GenericValidator.isBlankOrNull(command.getContribuable().getIdentifiant()))
        {
            errors.rejectValue("contribuable.identifiant", "required.contribuable.identifiant",
                "La saisie de l'identifiant est obligatoire.");
            errors.rejectValue("contribuable.identifiant", "rejected.contribuable.length",
                "L'identifiant doit être composé de 2 à 50 caractères");
            errors.rejectValue("contribuable.identifiant", "rejected.contribuable.formeidentifiant",
                "L'identifiant doit comporter des caractères alphanumériques et se terminer par \"-cp\"");

        }
        else
        {
            if (command.getContribuable().getIdentifiant() == null
                || !GenericValidator.minLength(command.getContribuable().getIdentifiant(), 4)
                || !GenericValidator.maxLength(command.getContribuable().getIdentifiant(), 50))
            {
                errors.rejectValue("contribuable.identifiant", "rejected.contribuable.length",
                    "L'identifiant doit être composé de 2 à 50 caractères");
            }

            if (!GenericValidator.matchRegexp(command.getContribuable().getIdentifiant(), "[A-Za-z0-9]*-cp$"))
            {
                errors.rejectValue("contribuable.identifiant", "rejected.contribuable.formeidentifiant",
                    "L'identifiant doit comporter des caractères alphanumériques et se terminer par \"-cp\"");
            }

            if (unContribuable instanceof ContribuablePar)
            {
                ContribuablePar unContribuableParticulier = (ContribuablePar) unContribuable;
                this.validateSurfaceModificationContribuableParticulier(unContribuableParticulier, errors);
            }
        }
    }

    /**
     * methode Validate surface sélection type contribuable pour ajout d'un nouveau contribuable : DGFiP.
     *
     * @param command DOCUMENTEZ_MOI
     * @param errors param
     */
    public void validateSurfaceSelectionTypeContribuable(CreationModificationContribuableForm command, Errors errors)
    {
        log.debug(">>> Debut methode validateSurfaceSelectionTypeContribuable");
        String typeContribuable = command.getTypeDeContribuable();

        if (!("contribuableparticulier".equals(typeContribuable) || "contribuableentreprise".equals(typeContribuable)))
        {
            errors.rejectValue("typeDeContribuable", "required.typeDeContribuable",
                "La sélection d'un type de contribuable est obligatoire");
        }
    }

    /**
     * methode Validate surface modification contribuable particulier : DGFiP.
     *
     * @param unContribuableParticulier param
     * @param errors param
     */

    private void validateSurfaceModificationContribuableParticulier(ContribuablePar unContribuableParticulier,
        Errors errors)
    {
        log.debug(">>> Debut methode validateSurfaceModificationContribuableParticulier");
        // Contrôle de surface du nom
        if (GenericValidator.isBlankOrNull(unContribuableParticulier.getNom()))
        {
            errors.rejectValue("contribuable.nom", "required.contribuable.nom", "La saisie du nom est obligatoire.");
        }

        if (unContribuableParticulier.getNom() == null
            || !GenericValidator.maxLength(unContribuableParticulier.getNom(), 36))
        {
            errors.rejectValue("contribuable.nom", "rejected.nom.length",
                "Le nom doit contenir au maximum 36 caractères");
        }

        // Contrôle de surface du prénom

        if (GenericValidator.isBlankOrNull(unContribuableParticulier.getPrenom()))
        {
            errors.rejectValue("contribuable.prenom", "required.contribuable.prenom",
                "La saisie du prénom est obligatoire.");
        }

        if (unContribuableParticulier.getPrenom() == null
            || !GenericValidator.maxLength(unContribuableParticulier.getPrenom(), 32))
        {
            errors.rejectValue("contribuable.prenom", "rejected.prenom.length",
                "Le prénom doit contenir au maximum 32 caractères");
        }

        // Contrôle de la civilité

        if (unContribuableParticulier.getCivilite().getCode().longValue() < 0)
        {
            errors.rejectValue("contribuable.civilite", "required.contribuable.civilite",
                "Saisie de la civilité obligatoire");
        }

        // Contrôle de surface du codepostal

        if (((unContribuableParticulier.getListeAdresses().get(0))).getCodePostal() == null
            || !GenericValidator.maxLength(((unContribuableParticulier.getListeAdresses().get(0))).getCodePostal(), 8))
        {
            errors.rejectValue("contribuable.adressePrincipale.codePostal", "rejected.adresse_codepostal.length",
                "Le code postal doit contenir au maximum 8 caractères");
        }

        // Contrôle de surface de la ville

        if (GenericValidator.isBlankOrNull((unContribuableParticulier.getListeAdresses().get(0)).getVille()))
        {
            errors.rejectValue("contribuable.adressePrincipale.ville", "required.contribuable.adresse.ville",
                "La saisie de la ville est obligatoire.");
        }

        if ((unContribuableParticulier.getListeAdresses().get(0)).getVille() == null
            || !GenericValidator.maxLength(((unContribuableParticulier.getListeAdresses().get(0))).getVille(), 32))
        {
            errors.rejectValue("contribuable.adressePrincipale.ville", "rejected.adresse_localite.length",
                "La ville doit contenir au maximum 32 caractères");
        }

        // Contrôle de surface du pays

        if (((unContribuableParticulier.getListeAdresses().get(0))).getPays() == null
            || ((unContribuableParticulier.getListeAdresses().get(0))).getPays().contentEquals(
                new StringBuffer("-999999")))
        {
            errors.rejectValue("contribuable.adressePrincipale.pays", null, "Sélection du pays obligatoire");
        }

        if (((unContribuableParticulier.getListeAdresses().get(0))).getPays() == null
            || !GenericValidator.maxLength(((unContribuableParticulier.getListeAdresses().get(0))).getPays(), 50))
        {
            errors.rejectValue("contribuable.adressePrincipale.pays", "rejected.pays.length",
                "Le pays doit contenir au maximum 50 caractères");
        }

    }

}
