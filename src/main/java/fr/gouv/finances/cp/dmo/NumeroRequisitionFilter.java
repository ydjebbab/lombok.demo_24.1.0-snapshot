package fr.gouv.finances.cp.dmo;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

public class NumeroRequisitionFilter implements Filter
{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        // rien à faire
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException
    {

        chain.doFilter(request, response);

        String numReqValue = request.getParameter("numeroRequisition");
        if (numReqValue != null)
        {
            HttpSession session = ((HttpServletRequest) request).getSession(false);
            session.setAttribute("numeroRequisition", numReqValue);
        }

    }

    @Override
    public void destroy()
    {
        // toujours rien à faire
    }

}
