/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.beans.PropertyEditor;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.cp.dmo.entite.AdresseContribuable;
import fr.gouv.finances.cp.dmo.entite.Civilite;
import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuableEnt;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.mvc.form.CreationModificationContribuableForm;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.cp.dmo.service.IReferenceService;
import fr.gouv.finances.lombok.adresse.service.AdresseService;
import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.exception.VerrouillageOptimisteException;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomDateEditor;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomNumberEditor;
import fr.gouv.finances.lombok.util.propertyeditor.CpStringCaracteresSpecifiquesWordPropertyEditor;
import fr.gouv.finances.lombok.util.propertyeditor.CpStringKeyboardEditor;
import fr.gouv.finances.lombok.util.propertyeditor.FichierJointPropertyEditor;
import fr.gouv.finances.lombok.util.propertyeditor.SelectEditor;
import fr.gouv.finances.lombok.util.propertyeditor.SelectHashCodeEditor;
import fr.gouv.finances.lombok.webflow.FormStatePersister;
import fr.gouv.finances.lombok.webflow.MementoFormStatePersister;

/**
 * Action CreationModificationContribuableFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
public class CreationModificationContribuableFormAction extends FormAction
{
    private static final Logger log = LoggerFactory.getLogger(CreationModificationContribuableFormAction.class);

    private IContribuableService contribuableserviceso;

    private AdresseService adresseserviceso;

    private IReferenceService referenceserviceso;

    public CreationModificationContribuableFormAction()
    {
        super();
    }

    public CreationModificationContribuableFormAction(Class arg0)
    {
        super(arg0);
    }

    public void setAdresseserviceso(AdresseService adresseserviceso)
    {
        this.adresseserviceso = adresseserviceso;
    }

    public void setContribuableserviceso(IContribuableService contribuableserviceso)
    {
        this.contribuableserviceso = contribuableserviceso;
    }

    public void setReferenceserviceso(IReferenceService referenceserviceso)
    {
        this.referenceserviceso = referenceserviceso;
    }

    public AdresseService getAdresseserviceso()
    {
        return adresseserviceso;
    }

    public IContribuableService getContribuableserviceso()
    {
        return contribuableserviceso;
    }

    public IReferenceService getReferenceserviceso()
    {
        return referenceserviceso;
    }

    /**
     * Affichage de messages si adresse messagerie non valide.
     * 
     * @param context param
     * @return the event
     */
    public Event afficherMessagesAvertissement(RequestContext context)
    {
        log.debug(">>> Debut methode afficherMessagesAvertissement");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        ContribuablePar contribuablepar = (ContribuablePar) creationModificationContribuableForm.getContribuable();

        // Contrôle du mail
        EmailValidator emailvalidator = EmailValidator.getInstance();
        if (!emailvalidator.isValid(contribuablepar.getAdresseMail()))
        {
            Errors warningErrors = new BindException(creationModificationContribuableForm, getFormObjectName());

            /*
             * warningErrors.rejectValue("contribuable.adresseMail", "contribuable.adresseMail.warn", new Object[] {
             * contribuablepar.getAdresseMail() }, "L'adresse de messagerie n'est pas valide.");
             */
            warningErrors.rejectValue("contribuable.adresseMail", null,
                new Object[] {contribuablepar.getAdresseMail()}, "L'adresse de messagerie n'est pas valide.");

            context.getRequestScope().put("warning", warningErrors);
            return error();
        }

        return success();
    }

    /**
     * Ajoute une adresse.
     * 
     * @param context param
     * @return the event
     */
    public Event ajouterUneAdresse(RequestContext context)
    {
        log.debug(">>> Debut methode ajouterUneAdresse");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        ContribuablePar contribuablepar = (ContribuablePar) creationModificationContribuableForm.getContribuable();
        AdresseContribuable nouvelleAdresse = creationModificationContribuableForm.getAdresse();

        // ajout de l'adresse dans la liste des adresses
        contribuablepar.getListeAdresses().add(nouvelleAdresse);

        // raz de l'adresse du formulaire
        creationModificationContribuableForm.setAdresse(null);

        return success();
    }

    /**
     * Ajoute une adresse.
     * 
     * @param context param
     * @return the event
     */
    public Event ajouterUneAdresseALaListe(RequestContext context)
    {
        log.debug(">>> Debut methode ajouterUneAdresseALaListe");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        ((ContribuablePar) creationModificationContribuableForm.getContribuable()).getListeAdresses().add(
            new AdresseContribuable());

        return success();
    }

    /**
     * Détermination du type de contribuable.
     * 
     * @param request param
     * @return the event
     */
    public Event determinerTypeDeContribuable(RequestContext request)
    {
        log.debug(">>> Debut methode determinerTypeDeContribuable");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) request.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);

        Contribuable contribuable = creationModificationContribuableForm.getContribuable();

        if (contribuable instanceof ContribuablePar)
        {
            return result("contribuableparticulier");
        }
        else
        {
            return result("contribuableentreprise");
        }
    }

    /**
     * Création d'un contribuable.
     * 
     * @param context param
     * @return the event
     */
    public Event executerCreer(RequestContext context)
    {
        log.debug(">>> Debut methode executerCreer");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        Contribuable contribuable = creationModificationContribuableForm.getContribuable();
        try
        {
            PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();
            contribuableserviceso.ajouterUnContribuable(contribuable, personne.getUid());
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rgExc.getMessage());
            return result("incorrectdata");
        }
        catch (VerrouillageOptimisteException verrOptimisteExc)
        {
            log.error(verrOptimisteExc.getMessage(), verrOptimisteExc);
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, verrOptimisteExc.getMessage());
            return error();
        }

        // Ajout du contribuable dans la liste des contribuables en
        // entrée du flux
        CheckboxSelectUtil.ajouteUnElementDansLeTableau(context, contribuable, "rechcont");
        return success();
    }

    /**
     * Modification d'un contribuable.
     * 
     * @param context param
     * @return the event
     */
    public Event executerModifier(RequestContext context)
    {
        log.debug(">>> Debut methode executerModifier");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        ContribuablePar contribuablepar = (ContribuablePar) creationModificationContribuableForm.getContribuable();
        try
        {
            PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();
            contribuableserviceso.modifierUnContribuable(contribuablepar, personne.getUid());
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rgExc.getMessage());
            return result("incorrectdata");
        }
        catch (VerrouillageOptimisteException verrOptimisteExc)
        {
            log.error(verrOptimisteExc.getMessage(), verrOptimisteExc);
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, verrOptimisteExc.getMessage());
            return error();
        }

        return success();
    }

    /**
     * NOrmalisation de l'adresse d'un contribuable.
     * 
     * @param context param
     * @return the event
     */
    public Event executerNormaliserAdresseContribuable(RequestContext context)
    {
        log.debug(">>> Debut methode executerNormaliserAdresseContribuable");
        Event result;
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        Contribuable contribuable = creationModificationContribuableForm.getContribuable();

        if (contribuable instanceof ContribuablePar)
        {
            ContribuablePar unContribuableParticulier = (ContribuablePar) contribuable;
            // Sauvegarde dans le contexte du flux des éléments
            // saisis par l'utilisateur
            // Avant normalisation de l'adresse
            FormStatePersister mgo = new MementoFormStatePersister();
            mgo.saveState(creationModificationContribuableForm, context, this, MementoFormStatePersister.LEVEL2);

            // Normalisation des champs de l'adresse
            contribuableserviceso.normaliserAdresseContribuable(unContribuableParticulier);

            try
            {
                adresseserviceso.verifierCodePostalCorrespondAUnLibelleCommuneRG23(unContribuableParticulier
                    .getAdressePrincipale().getCodePostal(), unContribuableParticulier.getAdressePrincipale()
                        .getVille());
            }
            catch (RegleGestionException rgExc)
            {
                log.error(rgExc.getMessage(), rgExc);
                List listevilles =
                    adresseserviceso.rechercherVillesPourUnCodePostal(unContribuableParticulier.getAdressePrincipale()
                        .getCodePostal());
                context.getFlowScope().put("listevilles", listevilles);
                return result("successapresnormalisation");
            }
            result = success();
        }
        else
        {
            // TT relatifs à un contribuable de type entreprise
            result = success();
        }
        return result;
    }

    /**
     * Initialisation du formulaire : ajout de l'adresse.
     * 
     * @param context param
     * @return the event
     */
    public Event initFormAjoutAdresse(RequestContext context)
    {
        log.debug(">>> Debut methode initFormAjoutAdresse");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        creationModificationContribuableForm.setAdresse(new AdresseContribuable());
        return success();
    }

    /**
     * Initialise l'adresse pour le formulaire.
     * 
     * @param context param
     * @return the event
     */
    public Event initFormEditionAdresse(RequestContext context)
    {
        log.debug(">>> Debut methode initFormEditionAdresse");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        ContribuablePar contribuablepar = (ContribuablePar) creationModificationContribuableForm.getContribuable();

        List lesAdresses = contribuablepar.getListeAdresses();
        PropertyEditor propertyEditor = new SelectHashCodeEditor(lesAdresses);

        propertyEditor.setAsText(context.getExternalContext().getRequestParameterMap().get("hashcode"));
        AdresseContribuable adresseSelectionne = (AdresseContribuable) propertyEditor.getValue();

        creationModificationContribuableForm.setAdresse(adresseSelectionne);
        return success();
    }

    /**
     * Initialisation du formulaire : affectation du contribuable Entreprise.
     * 
     * @param request param
     * @return the event
     */
    public Event initFormPourContribuableEntreprise(RequestContext request)
    {
        log.debug(">>> Debut methode initFormPourContribuableEntreprise");
        // Lecture du formulaire
        CreationModificationContribuableForm form =
            (CreationModificationContribuableForm) request.getFlowScope().get(getFormObjectName());

        Contribuable unContribuable = form.getContribuable();
        if (!(unContribuable instanceof ContribuableEnt))
        {
            form.setContribuable(new ContribuableEnt());
        }
        return success();
    }

    /**
     * Initialisation du formulaire : affectation du contribuable particulier.
     * 
     * @param request param
     * @return the event
     */
    public Event initFormPourContribuableParticulier(RequestContext request)
    {
        log.debug(">>> Debut methode initFormPourContribuableParticulier");
        // Lecture du formulaire
        CreationModificationContribuableForm form =
            (CreationModificationContribuableForm) request.getFlowScope().get(getFormObjectName());

        Contribuable unContribuable = form.getContribuable();
        if (!(unContribuable instanceof ContribuablePar))
        {
            form.setContribuable(new ContribuablePar());
        }
        return success();
    }

    /**
     * Initialiser formulaire pour ajout.
     * 
     * @param request param
     * @return the event
     */
    public Event initialiserFormulairePourAjout(RequestContext request)
    {
        log.debug(">>> Debut methode initialiserFormulairePourAjout");
        return success();
    }

    /**
     * Initialisation du formulaire en cas de modification.
     * 
     * @param request param
     * @return the event
     */
    public Event initialiserFormulairePourModification(RequestContext request)
    {
        log.debug(">>> Debut methode initialiserFormulairePourModification");
        Event result;
        // Lecture du formulaire
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) request.getFlowScope().get(getFormObjectName());

        // Récupération du contribuable selectionne dans le flux parent
        Contribuable contribuableSelectionne =
            (Contribuable) CheckboxSelectUtil.extraitUnElementSelectionneDansLeFluxParent(request, "rechcont");

        // on traite le cas ou on ne vient pas du flux principal mais on fait un appel direct
        // avec des paramètres dans l'url
        if (contribuableSelectionne == null)
        {
            String uidcontribuable = request.getRequestScope().getString("uidcontribuable");
            if (uidcontribuable != null)
            {
                contribuableSelectionne =
                    contribuableserviceso
                        .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID(uidcontribuable);
            }
        }

        if (contribuableSelectionne == null)
        {
            result = error();
        }
        else
        {
            // Mise à jour du formulaire à partir de l'élément
            // sélectionné
            creationModificationContribuableForm.setContribuable(contribuableSelectionne);

            // Sauvegarde de l'état du formulaire
            FormStatePersister mgo = new MementoFormStatePersister();
            mgo.saveState(creationModificationContribuableForm, request, this, MementoFormStatePersister.LEVEL1);
            result = success();
        }
        return result;

    }

    /**
     * methode Initialiser formulaire pour modification rd : DOCUMENTEZ_MOI.
     *
     * @param request DOCUMENTEZ_MOI
     * @return event
     */
    public Event initialiserFormulairePourModificationRD(RequestContext request)
    {
        log.debug(">>> Debut methode initialiserFormulairePourModificationRD");
        Event result;
        // Lecture du formulaire
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) request.getFlowScope().get(getFormObjectName());

        // Récupération du contribuable selectionne dans le flux parent
        Contribuable contribuableSelectionne =
            (Contribuable) CheckboxSelectUtil.extraitUnElementSelectionneDansLeFluxParent(request, "rechcont");

        // on traite le cas ou on ne vient pas du flux principal mais on fait un appel direct
        // avec des paramètres dans l'url
        if (contribuableSelectionne == null)
        {
            String uidcontribuable = request.getExternalContext().getSessionMap().getString("uidcontribuable");

            if (uidcontribuable != null)
            {
                contribuableSelectionne =
                    contribuableserviceso
                        .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID(uidcontribuable);
            }
        }

        if (contribuableSelectionne == null)
        {
            result = error();
        }
        else
        {
            // Mise à jour du formulaire à partir de l'élément
            // sélectionné
            creationModificationContribuableForm.setContribuable(contribuableSelectionne);

            // Sauvegarde de l'état du formulaire
            FormStatePersister mgo = new MementoFormStatePersister();
            mgo.saveState(creationModificationContribuableForm, request, this, MementoFormStatePersister.LEVEL1);
            result = success();
        }
        return result;

    }

    /**
     * Met à jour une Adresse.
     * 
     * @param context param
     * @return the event
     */
    public Event majUneAdresse(RequestContext context)
    {
        log.debug(">>> Debut methode majUneAdresse");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);

        // Sav du contribuable
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();
        contribuableserviceso.modifierUnContribuable(creationModificationContribuableForm.getContribuable(), personne
            .getUid());

        // raz de l'adresse du formulaire
        creationModificationContribuableForm.setAdresse(null);
        return success();
    }

    /**
     * paramétrage pour utilisation de cases à cocher dans letableau extremecomponents des adresses.
     * 
     * @param request param
     * @return the event
     */
    public Event parametrerCheckboxAdresses(RequestContext request)
    {
        log.debug(">>> Debut methode parametrerCheckboxAdresses");
        CheckboxSelectUtil.parametrerCheckboxes(request, "tableadresses").utiliserRowid("row").utiliserListeElements(
            "creationmodificationcontribuableform.contribuable.listeAdresses").utiliserMsgSelectionneUnSeulElement(
                "Sélectionnez une seule adresse à la fois.")
            .utiliserMsgAucunEltSelectionne(
                "Aucune adresse n'est sélectionnée");

        return success();
    }

    /**
     * Préparation des listes de pays et de civilité : mise dans le flow.
     * 
     * @param context param
     * @return the event
     */
    public Event preparerChargerListesPaysEtCivilites(RequestContext context)
    {
        log.debug(">>> Debut methode preparerChargerListesPaysEtCivilites");
        // Enregistrement dans le contexte du flux de la liste des pays
        Map<String, String> unemappays;

        unemappays = adresseserviceso.chargerMapPaysIso();

        context.getFlowScope().put("paysmap", unemappays);

        // Enregistrement dans le contexte du flux de la liste des
        // civilités
        List<Civilite> unelistecivilite = referenceserviceso.chargerListCivilite();
        context.getFlowScope().put("civiliteliste", unelistecivilite);

        return success();
    }

    /**
     * Préparationdes types de contribuable : mise dans le flow et le formulaire.
     * 
     * @param context param
     * @return the event
     */
    public Event preparerChargerListeTypesContribuables(RequestContext context)
    {
        log.debug(">>> Debut methode preparerChargerListeTypesContribuables");
        Map<String, String> uneMapTypeDeContribuable = new HashMap<String, String>();

        uneMapTypeDeContribuable.put("contribuableparticulier", "Contribuable de type particulier");
        uneMapTypeDeContribuable.put("contribuableentreprise", "Contribuable de type entreprise");

        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);

        creationModificationContribuableForm.setTypeDeContribuable("contribuableparticulier");

        context.getFlowScope().put("typescontribuablesmap", uneMapTypeDeContribuable);

        return success();
    }

    /**
     * Recharge à l'entréedu flux la liste des contribuables.
     * 
     * @param context param
     * @return the event
     */
    public Event rechargerEntreeFlux(RequestContext context)
    {
        log.debug(">>> Debut methode rechargerEntreeFlux");
        // Recupération du tableau en entree du Flux
        List listecontribuable = new ArrayList(CheckboxSelectUtil.litLesElementsDuTableau(context, "rechcont"));

        // Réactualisation de la liste des contribuables à partir de
        // leur id
        List listecontribuableactualisee =
            contribuableserviceso.actualiserListeContribuables(new ArrayList(listecontribuable));

        CheckboxSelectUtil.remplaceLesElementsDuTableau(context, listecontribuableactualisee, "rechcont");

        return success();
    }

    /**
     * Restauration du formulaire.
     * 
     * @param context param
     * @return the event
     */
    public Event restaureApresNormalisation(RequestContext context)
    {
        log.debug(">>> Debut methode restaureApresNormalisation");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);

        FormStatePersister mgo = new MementoFormStatePersister();
        mgo.restoreState(creationModificationContribuableForm, context, this, MementoFormStatePersister.LEVEL2);
        return success();
    }

    /**
     * Restauration du formulaire.
     * 
     * @param context param
     * @return the event
     */
    public Event restaureOriginal(RequestContext context)
    {
        log.debug(">>> Debut methode restaureOriginal");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);

        FormStatePersister mgo = new MementoFormStatePersister();
        mgo.restoreState(creationModificationContribuableForm, context, this, MementoFormStatePersister.LEVEL1);
        return success();

    }

    /**
     * Restaure l'état du fotmulaire.
     * 
     * @param context param
     * @return the event
     */
    public Event restaurerFormulaireAvantEditionDesAdresses(RequestContext context)
    {
        log.debug(">>> Debut methode restaurerFormulaireAvantEditionDesAdresses");
        // restauration de l'état du formulaire
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        FormStatePersister mgo = new MementoFormStatePersister();
        mgo.restoreState(creationModificationContribuableForm, context, this, MementoFormStatePersister.LEVEL3);

        return success();
    }

    /**
     * Sauvegarde le formulaire.
     * 
     * @param context param
     * @return the event
     */
    public Event sauvegardeFormulaireAvantEditionDesAdresses(RequestContext context)
    {
        log.debug(">>> Debut methode sauvegardeFormulaireAvantEditionDesAdresses");
        // Sauvegarde de l'état du formulaire
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        FormStatePersister mgo = new MementoFormStatePersister();
        mgo.saveState(creationModificationContribuableForm, context, this, MementoFormStatePersister.LEVEL3);

        return success();
    }

    /**
     * Supprime une adresse.
     * 
     * @param context param
     * @return the event
     */
    public Event supprimerUneAdresse(RequestContext context)
    {
        log.debug(">>> Debut methode supprimerUneAdresse");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        ContribuablePar contribuablepar = (ContribuablePar) creationModificationContribuableForm.getContribuable();
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context, "tableadresses");
        Collection adressesASupprimer =
            CheckboxSelectUtil.extraitLesElementsSelectionnesDansLeFlux(context, "tableadresses");
        if (contribuablepar.getListeAdresses() != null && adressesASupprimer != null)
        {
            contribuablepar.getListeAdresses().removeAll(adressesASupprimer);
        }

        return success();
    }

    /**
     * Vérification de l'existence d'un code postal.
     * 
     * @param context param
     * @return the event
     */
    public Event validerVerifierExistenceCodePostal(RequestContext context)
    {
        log.debug(">>> Debut methode validerVerifierExistenceCodePostal");
        CreationModificationContribuableForm creationModificationContribuableForm =
            (CreationModificationContribuableForm) context.getFlowScope().getRequired(getFormObjectName(),
                CreationModificationContribuableForm.class);
        ContribuablePar contribuablepar = (ContribuablePar) creationModificationContribuableForm.getContribuable();

        // Contrôle de l'existence du code postal
        try
        {
            adresseserviceso.verifierExistenceCodePostalRG22(((contribuablepar.getListeAdresses().get(0)))
                .getCodePostal());
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.rejectValue("contribuable.adressePrincipale.codePostal", null, rgExc.getMessage());
            return error();
        }
        return success();
    }

    /*
     * (non-Javadoc)
     * @seeorg.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.
     * RequestContext, org.springframework.beans.PropertyEditorRegistry)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param rc
     * @param per
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext rc, PropertyEditorRegistry per)
    {
        log.debug(">>> Debut methode registerPropertyEditors");
        super.registerPropertyEditors(rc, per);

        // Enregistrement des propertyeditor

        /*
         * \t The tab character ('\u0009') \n The newline (line feed) character (' ') \r The carriage-return character
         * (' ') \f The form-feed character ('\u000C') \a The alert (bell) character ('\u0007') \e The escape character
         * ('\u001B')
         */

        // String charsToDelete = "\t\n\r\f";
        // binder.registerCustomEditor(String.class, new StringTrimmerEditor(charsToDelete, false));
        per.registerCustomEditor(String.class, new CpStringKeyboardEditor(true, true, true));
        per.registerCustomEditor(String.class, new CpStringCaracteresSpecifiquesWordPropertyEditor(true, true, true));
        // Formatage des champs de type Long : on autorise les valeurs
        // nulles
        per.registerCustomEditor(Long.class, new CpCustomNumberEditor(Long.class));

        // Formatage d'un montant pour un champ spécifique du formulaire
        NumberFormat unNumberFormat = NumberFormat.getInstance(Locale.FRANCE);
        unNumberFormat.setGroupingUsed(true);
        unNumberFormat.setMinimumFractionDigits(2);
        per.registerCustomEditor(Double.class, "contribuable.solde", new CpCustomNumberEditor(Double.class,
            unNumberFormat, CpCustomNumberEditor.NULL));

        // Formatage d'un fuseau horaire
        // binder.registerCustomEditor(TimeZone.class, new
        // TimeZoneEditor(true));

        // Bind des upload de fichier
        per.registerCustomEditor(MultipartFile.class, new FichierJointPropertyEditor());

        // Dates
        per.registerCustomEditor(Date.class, new CpCustomDateEditor());

        // Liste déroulante choix de la civilité d'un contribuable
        List unelistecivilite = (List) rc.getFlowScope().get("civiliteliste");
        per.registerCustomEditor(Civilite.class, "contribuable.civilite", new SelectEditor(unelistecivilite, "code"));
    }

}
