/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ContribuableEnt.java
 *
 */
package fr.gouv.finances.cp.dmo.entite;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entité ContribuableEnt
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
@XmlRootElement(name = "record")
@Entity
@DiscriminatorValue("CE")
public class ContribuableEnt extends Contribuable
{

    private static final long serialVersionUID = 1L;

    private String nom;

    private Double solde;

    private String telephoneFixe;

    public ContribuableEnt()
    {
        super();
    }

    public ContribuableEnt(Long id)
    {
        super(id);
    }

    public ContribuableEnt(String identifiant)
    {
        super(identifiant);
    }

    public String getNom()
    {
        return nom;
    }

    public Double getSolde()
    {
        return solde;
    }

    public String getTelephoneFixe()
    {
        return telephoneFixe;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public void setSolde(Double solde)
    {
        this.solde = solde;
    }

    public void setTelephoneFixe(String telephoneFixe)
    {
        this.telephoneFixe = telephoneFixe;
    }

}
