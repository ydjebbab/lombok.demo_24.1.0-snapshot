/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : StatistiquesContribuables.java
 *
 */
package fr.gouv.finances.cp.dmo.techbean;

import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Class StatistiquesContribuables DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class StatistiquesContribuables extends BaseTechBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** nombre contribuables. */
    private Long nombreContribuables;

    /** code impot. */
    private Long codeImpot;

    /** nom impot. */
    private String nomImpot;

    /** montant total. */
    private Double montantTotal;

    /** montant moyen. */
    private Double montantMoyen;

    /** identifiant. */
    private String identifiant;

    /** nombre avis. */
    private Long nombreAvis;

    /** annee de naissance. */
    private String anneeDeNaissance;

    /**
     * Constructeur
     */
    public StatistiquesContribuables()
    {
        super();
    }

    /**
     * Constructeur
     * 
     * @param identifiant param
     * @param nombreAvis param
     * @param nombreContribuables param
     * @param codeImpot param
     * @param nomImpot param
     * @param montantTotal param
     * @param montantMoyen param
     */
    public StatistiquesContribuables(String identifiant, Long nombreAvis, Long nombreContribuables,
        Long codeImpot, String nomImpot, Double montantTotal, Double montantMoyen)
    {
        super();
        this.identifiant = identifiant;
        this.nombreAvis = nombreAvis;
        this.nombreContribuables = nombreContribuables;
        this.codeImpot = codeImpot;
        this.nomImpot = nomImpot;
        this.montantTotal = montantTotal;
        this.montantMoyen = montantMoyen;
    }

    /**
     * Accesseur de l attribut annee de naissance.
     * 
     * @return annee de naissance
     */
    public String getAnneeDeNaissance()
    {
        return anneeDeNaissance;
    }

    /**
     * Accesseur de l attribut code impot.
     * 
     * @return code impot
     */
    public Long getCodeImpot()
    {
        return codeImpot;
    }

    /**
     * Accesseur de l attribut identifiant.
     * 
     * @return identifiant
     */
    public String getIdentifiant()
    {
        return identifiant;
    }

    /**
     * Accesseur de l attribut montant moyen.
     * 
     * @return montant moyen
     */
    public Double getMontantMoyen()
    {
        return montantMoyen;
    }

    /**
     * Accesseur de l attribut montant total.
     * 
     * @return montant total
     */
    public Double getMontantTotal()
    {
        return montantTotal;
    }

    /**
     * Accesseur de l attribut nombre avis.
     * 
     * @return nombre avis
     */
    public Long getNombreAvis()
    {
        return nombreAvis;
    }

    /**
     * Accesseur de l attribut nombre contribuables.
     * 
     * @return nombre contribuables
     */
    public Long getNombreContribuables()
    {
        return nombreContribuables;
    }

    /**
     * Accesseur de l attribut nom impot.
     * 
     * @return nom impot
     */
    public String getNomImpot()
    {
        return nomImpot;
    }

    /**
     * Modificateur de l attribut annee de naissance.
     * 
     * @param anneeDeNaissance le nouveau annee de naissance
     */
    public void setAnneeDeNaissance(String anneeDeNaissance)
    {
        this.anneeDeNaissance = anneeDeNaissance;
    }

    /**
     * Modificateur de l attribut code impot.
     * 
     * @param codeImpot le nouveau code impot
     */
    public void setCodeImpot(Long codeImpot)
    {
        this.codeImpot = codeImpot;
    }

    /**
     * Modificateur de l attribut identifiant.
     * 
     * @param identifiant le nouveau identifiant
     */
    public void setIdentifiant(String identifiant)
    {
        this.identifiant = identifiant;
    }

    /**
     * Modificateur de l attribut montant moyen.
     * 
     * @param montantMoyen le nouveau montant moyen
     */
    public void setMontantMoyen(Double montantMoyen)
    {
        this.montantMoyen = montantMoyen;
    }

    /**
     * Modificateur de l attribut montant total.
     * 
     * @param montantTotal le nouveau montant total
     */
    public void setMontantTotal(Double montantTotal)
    {
        this.montantTotal = montantTotal;
    }

    /**
     * Modificateur de l attribut nombre avis.
     * 
     * @param nombreAvis le nouveau nombre avis
     */
    public void setNombreAvis(Long nombreAvis)
    {
        this.nombreAvis = nombreAvis;
    }

    /**
     * Modificateur de l attribut nombre contribuables.
     * 
     * @param nombreContribuables le nouveau nombre contribuables
     */
    public void setNombreContribuables(Long nombreContribuables)
    {
        this.nombreContribuables = nombreContribuables;
    }

    /**
     * Modificateur de l attribut nom impot.
     * 
     * @param nomImpot le nouveau nom impot
     */
    public void setNomImpot(String nomImpot)
    {
        this.nomImpot = nomImpot;
    }

}
