/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : LancerTacheLongueFormAction.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.util.HashMap;

import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;

/**
 * Action LancerTacheLongueFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class LancerTacheLongueFormAction extends FormAction
{
    /** editiondemandeserviceso. */
    private EditionDemandeService editiondemandeserviceso;

    /**
     * Constructeur
     */
    public LancerTacheLongueFormAction()
    {
        super();
    }

    /**
     * Instanciation de lancer tache longue form action.
     * 
     * @param formObjectClass param
     */
    public LancerTacheLongueFormAction(Class formObjectClass)
    {
        super(formObjectClass);
    }

    /**
     * Gets the editiondemandeserviceso.
     *
     * @return the editiondemandeserviceso
     */
    public EditionDemandeService getEditiondemandeserviceso()
    {
        return editiondemandeserviceso;
    }

    /**
     * Sets the editiondemandeserviceso.
     *
     * @param editiondemandeserviceso the new editiondemandeserviceso
     */
    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso)
    {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    /**
     * methode Lancer tache : DGFiP.
     * 
     * @param context param
     * @return event
     */
    public Event lancerTache(RequestContext context)
    {
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        EditionAsynchroneNotifieeParMail editionAsynchroneNotifieeParMail =
            editiondemandeserviceso.initEditionAsynchroneNotifieeParMail("zf8.lancertachelongue.edition", personne
                .getUid(), personne.getMail());

        editionAsynchroneNotifieeParMail =
            editiondemandeserviceso.declencherEdition(editionAsynchroneNotifieeParMail, new HashMap<>());

        context.getFlowScope().put("jobHistory", editionAsynchroneNotifieeParMail);
        return success();
    }

}
