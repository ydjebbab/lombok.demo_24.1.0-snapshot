package fr.gouv.finances.cp.dmo.entite;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContratMensu.class)
public abstract class ContratMensu_ {

	public static volatile SingularAttribute<ContratMensu, ContribuablePar> contribuablePar;
	public static volatile SingularAttribute<ContratMensu, Long> numeroContrat;
	public static volatile SingularAttribute<ContratMensu, Boolean> estTraite;
	public static volatile SingularAttribute<ContratMensu, Rib> rib;
	public static volatile SingularAttribute<ContratMensu, Long> id;
	public static volatile SingularAttribute<ContratMensu, Long> anneePriseEffet;
	public static volatile SingularAttribute<ContratMensu, TypeImpot> typeImpot;
	public static volatile SingularAttribute<ContratMensu, Date> dateDemande;
	public static volatile SingularAttribute<ContratMensu, Integer> version;

}

