/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : DocumentJointDao.java
 *
 */

package fr.gouv.finances.cp.dmo.dao;

import java.util.Collection;

import fr.gouv.finances.cp.dmo.entite.DocumentJoint;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa;

/**
 * Interface DocumentJointDao
 * 
 * @author chouard-cp
 * @author CF: migration vers JPA
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public interface IDocumentJointDao extends BaseDaoJpa
{

    /**
     * methode Delete collection documents joints
     * 
     * @param documentsASupprimer param
     */
    public void deleteCollectionDocumentsJoints(Collection documentsASupprimer);

    /**
     * methode Find document joint par id
     * 
     * @param identifiant 
     * @return document joint
     */
    public DocumentJoint findDocumentJointParId(Long identifiant);
}
