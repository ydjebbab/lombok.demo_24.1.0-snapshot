/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 * - CF
 *
 */
package fr.gouv.finances.cp.dmo.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Selection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IStatistiquesDao;
import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.AvisImposition_;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar_;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.entite.TypeImpot_;
import fr.gouv.finances.cp.dmo.techbean.StatistiquesAvisImpositions;
import fr.gouv.finances.cp.dmo.techbean.StatistiquesContribuables;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;

/**
 * DAO StatistiquesDaoImpl
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
@Repository("statistiquesDao")
@Transactional(transactionManager="transactionManager")
public class StatistiquesDaoImpl extends BaseDaoJpaImpl implements IStatistiquesDao
{
    private static final Logger log = LoggerFactory.getLogger(StatistiquesDaoImpl.class);

    private static final String CODE_IMPOT_ALIAS = "codeImpot";

    private static final String NOM_IMPOT_ALIAS = "nomImpot";

    private static final String NOMBRE_AVIS_ALIAS = "nombreAvis";

    private static final String MONTANT_TOTAL_ALIAS = "montantTotal";

    private static final String MONTANT_MOYEN_ALIAS = "montantMoyen";

    private static final String DATE_ROLE_MIN_ALIAS = "dateRoleMin";

    private static final String DATE_ROLE_MAX_ALIAS = "dateRoleMax";

    private static final String IDENTIFIANT_ALIAS = "identifiant";

    public StatistiquesDaoImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.IStatistiquesDao#agregerAvisImpositionParTypeImpot()
     */
    @Override
    public List<StatistiquesAvisImpositions> agregerAvisImpositionParTypeImpot()
    {
        log.debug(">>> Debut methode agregerAvisImpositionParTypeImpot");
        List<StatistiquesAvisImpositions> listeStatistiquesAvisImpositions = new ArrayList<>();

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriabuilder.createTupleQuery();
        Root<AvisImposition> avisImpositionRoot = criteriaQuery.from(AvisImposition.class);

        Path<TypeImpot> typeImpotPath = avisImpositionRoot.get(AvisImposition_.typeImpot);
        Path<Long> codeImpotPath = typeImpotPath.get(TypeImpot_.codeImpot);
        Path<String> nomImpotPath = typeImpotPath.get(TypeImpot_.nomImpot);

        Path<String> referencePath = avisImpositionRoot.get(AvisImposition_.reference);
        Selection<Long> referenceSelection = criteriabuilder.count(referencePath);

        Path<Double> montantPath = avisImpositionRoot.get(AvisImposition_.montant);
        Path<Date> dateRolePath = avisImpositionRoot.get(AvisImposition_.dateRole);

        criteriaQuery.multiselect(codeImpotPath.alias(CODE_IMPOT_ALIAS), nomImpotPath.alias(NOM_IMPOT_ALIAS),
            referenceSelection.alias(NOMBRE_AVIS_ALIAS),
            criteriabuilder.sum(montantPath).alias(MONTANT_TOTAL_ALIAS),
            criteriabuilder.avg(montantPath).alias(MONTANT_MOYEN_ALIAS),
            criteriabuilder.least(dateRolePath).alias(DATE_ROLE_MIN_ALIAS),
            criteriabuilder.greatest(dateRolePath).alias(DATE_ROLE_MAX_ALIAS));

        criteriaQuery.groupBy(codeImpotPath, nomImpotPath);

        TypedQuery<Tuple> typedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("typedQuery : " + typedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<Tuple> listeTuples = typedQuery.getResultList();
        log.debug("listeTuples : " + listeTuples);

        if (null != listeTuples && !listeTuples.isEmpty())
        {
            log.debug("Taille listeTuples : " + listeTuples.size());
        }

        Consumer<Tuple> consumerTuples = new Consumer<Tuple>()
        {
            @Override
            public void accept(Tuple tuple)
            {
                StatistiquesAvisImpositions statistiquesAvisImpositions = new StatistiquesAvisImpositions();

                Long nombreAvis = tuple.get(NOMBRE_AVIS_ALIAS, Long.class);
                log.debug(" nombreAvis : " + nombreAvis);
                statistiquesAvisImpositions.setNombreAvis(nombreAvis);

                Double montantTotal = tuple.get(MONTANT_TOTAL_ALIAS, Double.class);
                log.debug(" montantTotal : " + montantTotal);
                statistiquesAvisImpositions.setMontantTotal(montantTotal);

                Double montantMoyen = tuple.get(MONTANT_MOYEN_ALIAS, Double.class);
                log.debug(" montantMoyen : " + montantMoyen);
                statistiquesAvisImpositions.setMontantMoyen(montantMoyen);

                Date dateRoleMin = tuple.get(DATE_ROLE_MIN_ALIAS, Date.class);
                log.debug(" dateRoleMin : " + dateRoleMin);
                statistiquesAvisImpositions.setDateRoleMin(dateRoleMin);

                Date dateRoleMax = tuple.get(DATE_ROLE_MAX_ALIAS, Date.class);
                log.debug(" dateRoleMax : " + dateRoleMax);
                statistiquesAvisImpositions.setDateRoleMax(dateRoleMax);

                Long codeImpot = tuple.get(CODE_IMPOT_ALIAS, Long.class);
                log.debug(" codeImpot : " + codeImpot);
                statistiquesAvisImpositions.setCodeImpot(codeImpot);

                String nomImpot = tuple.get(NOM_IMPOT_ALIAS, String.class);
                log.debug(" nomImpot : " + nomImpot);
                statistiquesAvisImpositions.setNomImpot(nomImpot);

                listeStatistiquesAvisImpositions.add(statistiquesAvisImpositions);
            }
        };

        if (null != listeTuples)
        {
            listeTuples.forEach(consumerTuples);
        }

        return listeStatistiquesAvisImpositions;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param seuil
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.IStatistiquesDao#agregerAvisImpositionParTypeImpotSeuil(java.lang.Double)
     */
    @Override
    public List<StatistiquesAvisImpositions> agregerAvisImpositionParTypeImpotSeuil(Double seuil)
    {
        log.debug(">>> Debut methode agregerAvisImpositionParTypeImpotSeuil");

        List<StatistiquesAvisImpositions> listeStatistiquesAvisImpositions = new ArrayList<>();

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriabuilder.createTupleQuery();
        Root<AvisImposition> avisImpositionRoot = criteriaQuery.from(AvisImposition.class);

        Path<ContribuablePar> contribuableParPath = avisImpositionRoot.get(AvisImposition_.contribuablePar);
        Path<String> anneeDeNaissancePath = contribuableParPath.get(ContribuablePar_.anneeDeNaissance);

        Path<TypeImpot> typeImpotPath = avisImpositionRoot.get(AvisImposition_.typeImpot);
        Path<Long> codeImpotPath = typeImpotPath.get(TypeImpot_.codeImpot);
        Path<String> nomImpotPath = typeImpotPath.get(TypeImpot_.nomImpot);

        Path<String> referencePath = avisImpositionRoot.get(AvisImposition_.reference);
        Selection<Long> referenceSelection = criteriabuilder.count(referencePath);

        Path<Double> montantPath = avisImpositionRoot.get(AvisImposition_.montant);
        Path<Date> dateRolePath = avisImpositionRoot.get(AvisImposition_.dateRole);

        criteriaQuery.multiselect(anneeDeNaissancePath.alias("annee"), codeImpotPath.alias(CODE_IMPOT_ALIAS),
            nomImpotPath.alias(NOM_IMPOT_ALIAS),
            referenceSelection.alias(NOMBRE_AVIS_ALIAS),
            criteriabuilder.sum(montantPath).alias(MONTANT_TOTAL_ALIAS),
            criteriabuilder.avg(montantPath).alias(MONTANT_MOYEN_ALIAS),
            criteriabuilder.least(dateRolePath).alias(DATE_ROLE_MIN_ALIAS),
            criteriabuilder.greatest(dateRolePath).alias(DATE_ROLE_MAX_ALIAS));

        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.ge(avisImpositionRoot.get(AvisImposition_.montant), seuil));
        criteriaQuery.where(predicate);

        Order anneeNaissanceAscOrder = criteriabuilder.asc(anneeDeNaissancePath);
        criteriaQuery.orderBy(anneeNaissanceAscOrder);

        criteriaQuery.groupBy(anneeDeNaissancePath, codeImpotPath, nomImpotPath);

        TypedQuery<Tuple> typedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("typedQuery : " + typedQuery.unwrap(org.hibernate.Query.class).getQueryString());
        // typedQuery.unwrap(org.hibernate.Query.class)
        // .setResultTransformer(new AliasToBeanResultTransformer(StatistiquesAvisImpositions.class));
        // .setResultTransformer( Transformers.aliasToBean( StatistiquesAvisImpositions.class ) );

        List<Tuple> listeTuples = typedQuery.getResultList();
        log.debug("listeTuples : " + listeTuples);

        if (null != listeTuples && !listeTuples.isEmpty())
        {
            log.debug("Taille listeTuples : " + listeTuples.size());
        }

        Consumer<Tuple> consumerTuples = new Consumer<Tuple>()
        {
            @Override
            public void accept(Tuple tuple)
            {
                StatistiquesAvisImpositions statistiquesAvisImpositions = new StatistiquesAvisImpositions();

                Long nombreAvis = tuple.get(NOMBRE_AVIS_ALIAS, Long.class);
                log.debug(" nombreAvis : " + nombreAvis);
                statistiquesAvisImpositions.setNombreAvis(nombreAvis);

                Double montantTotal = tuple.get(MONTANT_TOTAL_ALIAS, Double.class);
                log.debug(" montantTotal : " + montantTotal);
                statistiquesAvisImpositions.setMontantTotal(montantTotal);

                Double montantMoyen = tuple.get(MONTANT_MOYEN_ALIAS, Double.class);
                log.debug(" montantMoyen : " + montantMoyen);
                statistiquesAvisImpositions.setMontantMoyen(montantMoyen);

                Date dateRoleMin = tuple.get(DATE_ROLE_MIN_ALIAS, Date.class);
                log.debug(" dateRoleMin : " + dateRoleMin);
                statistiquesAvisImpositions.setDateRoleMin(dateRoleMin);

                Date dateRoleMax = tuple.get(DATE_ROLE_MAX_ALIAS, Date.class);
                log.debug(" dateRoleMax : " + dateRoleMax);
                statistiquesAvisImpositions.setDateRoleMax(dateRoleMax);

                Long codeImpot = tuple.get(CODE_IMPOT_ALIAS, Long.class);
                log.debug(" codeImpot : " + codeImpot);
                statistiquesAvisImpositions.setCodeImpot(codeImpot);

                String nomImpot = tuple.get(NOM_IMPOT_ALIAS, String.class);
                log.debug(" nomImpot : " + nomImpot);
                statistiquesAvisImpositions.setNomImpot(nomImpot);

                String annee = tuple.get("annee", String.class);
                log.debug(" annee : " + annee);
                statistiquesAvisImpositions.setAnnee(annee);

                listeStatistiquesAvisImpositions.add(statistiquesAvisImpositions);
            }
        };

        if (null != listeTuples)
        {
            listeTuples.forEach(consumerTuples);
        }

        return listeStatistiquesAvisImpositions;
    }

    /**
     * Compte pour chaque TypeImpot le nombre de contribuables qui possèdent au moins un avis d'imposition de ce
     * TypeImpot.
     * 
     * @return the list
     * 
     */
    @Override
    public List<StatistiquesContribuables> agregerContribuableParTypeImpot()
    {
        log.debug(">>> Debut methode agregerContribuableParTypeImpot");

        List<StatistiquesContribuables> listeStatistiquesContribuables = new ArrayList<>();

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriabuilder.createTupleQuery();
        Root<AvisImposition> avisImpositionRoot = criteriaQuery.from(AvisImposition.class);

        Path<TypeImpot> typeImpotPath = avisImpositionRoot.get(AvisImposition_.typeImpot);
        Path<Long> codeImpotPath = typeImpotPath.get(TypeImpot_.codeImpot);
        Path<String> nomImpotPath = typeImpotPath.get(TypeImpot_.nomImpot);
        Path<ContribuablePar> contribuableParPath = avisImpositionRoot.get(AvisImposition_.contribuablePar);
        Path<String> identifiantPath = contribuableParPath.get(ContribuablePar_.identifiant);
        Path<Double> montantPath = avisImpositionRoot.get(AvisImposition_.montant);

        // Selection<Long> selectionCodeImpot = codeImpot.alias(CODE_IMPOT_ALIAS);
        // Selection<String> selectionNomImpot = nomImpot.alias(NOM_IMPOT_ALIAS);
        Selection<Long> identifiantSelection = criteriabuilder.countDistinct(identifiantPath);

        criteriaQuery.multiselect(identifiantSelection.alias("nombreContribuables"), codeImpotPath.alias(CODE_IMPOT_ALIAS),
            nomImpotPath.alias(NOM_IMPOT_ALIAS),
            criteriabuilder.avg(montantPath).alias(MONTANT_MOYEN_ALIAS),
            criteriabuilder.sum(montantPath).alias(MONTANT_TOTAL_ALIAS));

        criteriaQuery.groupBy(codeImpotPath, nomImpotPath);

        TypedQuery<Tuple> typedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("typedQuery : " + typedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<Tuple> listeTuples = typedQuery.getResultList();
        log.debug("listeTuples : " + listeTuples);

        if (null != listeTuples && !listeTuples.isEmpty())
        {
            log.debug("Taille listeTuples : " + listeTuples.size());
        }

        Consumer<Tuple> consumerTuples = new Consumer<Tuple>()
        {
            @Override
            public void accept(Tuple tuple)
            {
                StatistiquesContribuables statistiquesContribuables = new StatistiquesContribuables();

                Long nombreContribuables = tuple.get("nombreContribuables", Long.class);
                log.debug(" nombreContribuables : " + nombreContribuables);
                statistiquesContribuables.setNombreContribuables(nombreContribuables);

                Double montantMoyen = tuple.get(MONTANT_MOYEN_ALIAS, Double.class);
                log.debug(" montantMoyen : " + montantMoyen);
                statistiquesContribuables.setMontantMoyen(montantMoyen);

                Double montantTotal = tuple.get(MONTANT_TOTAL_ALIAS, Double.class);
                log.debug(" montantTotal : " + montantTotal);
                statistiquesContribuables.setMontantTotal(montantTotal);

                Long codeImpotTuple = tuple.get(CODE_IMPOT_ALIAS, Long.class);
                log.debug(" codeImpotTuple : " + codeImpotTuple);
                statistiquesContribuables.setCodeImpot(codeImpotTuple);

                String nomImpotTuple = tuple.get(NOM_IMPOT_ALIAS, String.class);
                log.debug(" nomImpotTuple : " + nomImpotTuple);
                statistiquesContribuables.setNomImpot(nomImpotTuple);
                listeStatistiquesContribuables.add(statistiquesContribuables);
            }
        };

        if (null != listeTuples)
        {
            listeTuples.forEach(consumerTuples);
        }

        return listeStatistiquesContribuables;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.IStatistiquesDao#agregerParContribuable()
     */
    @Override
    public List<StatistiquesContribuables> agregerParContribuable()
    {
        log.debug(">>> Debut methode agregerParContribuable");

        List<StatistiquesContribuables> listeStatistiquesContribuables = new ArrayList<>();

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriabuilder.createTupleQuery();
        Root<ContribuablePar> contribuableParRoot = criteriaQuery.from(ContribuablePar.class);
        Join<ContribuablePar, AvisImposition> joinContribuableParAvisImposition =
            contribuableParRoot.join(ContribuablePar_.listeAvisImposition, JoinType.INNER);

        Path<String> identifiantPath = contribuableParRoot.get(ContribuablePar_.identifiant);
        Path<Double> montantPath = joinContribuableParAvisImposition.get(AvisImposition_.montant);
        Selection<Long> montantCountSelection = criteriabuilder.countDistinct(montantPath);

        criteriaQuery.multiselect(identifiantPath.alias(IDENTIFIANT_ALIAS), montantCountSelection.alias(NOMBRE_AVIS_ALIAS),
            criteriabuilder.avg(montantPath).alias(MONTANT_MOYEN_ALIAS),
            criteriabuilder.sum(montantPath).alias(MONTANT_TOTAL_ALIAS));

        criteriaQuery.groupBy(identifiantPath);

        TypedQuery<Tuple> typedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("typedQuery : " + typedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<Tuple> listeTuples = typedQuery.getResultList();
        log.debug("listeTuples : " + listeTuples);

        if (null != listeTuples && !listeTuples.isEmpty())
        {
            log.debug("Taille listeTuples : " + listeTuples.size());
        }

        Consumer<Tuple> consumerTuples = new Consumer<Tuple>()
        {
            @Override
            public void accept(Tuple tuple)
            {
                StatistiquesContribuables statistiquesContribuables = new StatistiquesContribuables();

                String identifiantContribuable = tuple.get(IDENTIFIANT_ALIAS, String.class);
                log.debug(" identifiantContribuable : " + identifiantContribuable);
                statistiquesContribuables.setIdentifiant(identifiantContribuable);

                Double montantMoyen = tuple.get(MONTANT_MOYEN_ALIAS, Double.class);
                log.debug(" montantMoyen : " + montantMoyen);
                statistiquesContribuables.setMontantMoyen(montantMoyen);

                Double montantTotal = tuple.get(MONTANT_TOTAL_ALIAS, Double.class);
                log.debug(" montantTotal : " + montantTotal);
                statistiquesContribuables.setMontantTotal(montantTotal);

                Long nombreAvisTuple = tuple.get(NOMBRE_AVIS_ALIAS, Long.class);
                log.debug(" nombreAvisTuple : " + nombreAvisTuple);
                statistiquesContribuables.setNombreAvis(nombreAvisTuple);
                listeStatistiquesContribuables.add(statistiquesContribuables);
            }
        };

        if (null != listeTuples)
        {
            listeTuples.forEach(consumerTuples);
        }
        return listeStatistiquesContribuables;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.IStatistiquesDao#statPremiersAvisImposition()
     */
    @Override
    public List statPremiersAvisImposition()
    {
        log.debug(">>> Debut methode statPremiersAvisImposition");
        // CF: méthode appelée nulle part
        /*
         * // Récupération de l'objet hibernatetemplate utilisé HibernateTemplate hibernatetemplate =
         * getHibernateTemplate(); // Paramétrage du l'objet hibernatetemplate si nécessaire //
         * hibernatetemplate.setFetchSize(10); // hibernatetemplate.setMaxResults(10); // Création d'un requête détachée
         * DetachedCriteria criteria = DetachedCriteria.forClass(AvisImposition.class, "avisImposition");
         * criteria.createCriteria("contribuablePar").add( Restrictions.le("dateDeNaissance",
         * Calendar.getInstance().getTime())); // restriction (<=) sur la date d'ouverture des sous regies de // la
         * regie // Création d'alias criteria.createAlias("typeImpot", "typeImpot"); // Création de la projection //
         * Ajout de restriction // Ajout d'un ordre de tri criteria.addOrder(Order.asc("reference")); // Affectation de
         * la projection à la requête // Chargement d'objets référencés criteria.setFetchMode("typeImpot",
         * FetchMode.JOIN); // Stratégie de transformation du résultat // criteria.setResultTransformer( new //
         * AliasToBeanResultTransformer(AvisImposition.class) ); // criteria.setResultTransformer( new //
         * RootEntityResultTransformer()); criteria.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE); //
         * Exécution de la requête List result = hibernatetemplate.findByCriteria(criteria); return result;
         */
        return null;
    }

    /**
     * methode Template critere de recherche
     * 
     * @return list
     */
    public List templateCritereDeRecherche()
    {
        log.debug(">>> Debut methode templateCritereDeRecherche");
        // CF: méthode appelée nulle part
        /*
         * // Récupération de l'objet hibernatetemplate utilisé HibernateTemplate hibernatetemplate =
         * getHibernateTemplate(); // Paramétrage du nombre maximum de lignes retournées par la // requête //
         * hibernatetemplate.setMaxResults(maxResults); // Création d'un requête détachée DetachedCriteria criteria =
         * DetachedCriteria.forClass(AvisImposition.class); // Création des alias sur les associations
         * criteria.createAlias("typeImpot", "typeImpot"); // Résolution des associations
         * criteria.setFetchMode("typeImpot", FetchMode.JOIN); // Ajout de critères de recherche de type restriction //
         * criteria.add(Restrictions.eq("",)); // Créer des critères de recherche sur des entités associées //
         * criteria.createCriteria(); // Création d'une liste de projection ProjectionList uneprojection =
         * Projections.projectionList(); uneprojection.add(Projections.groupProperty("typeImpot.codeImpot"),
         * "codeImpot").add( Projections.groupProperty("typeImpot.nomImpot"),
         * "nomImpot").add(Projections.count("reference"), "nombreAvis").add(Projections.sum("montant"),
         * "montantTotal").add(Projections.avg("montant"), "montantMoyen").add(Projections.min("dateRole"),
         * "dateRoleMin").add(Projections.max("dateRole"), "dateRoleMax"); // Affectation de projections à la requête
         * criteria.setProjection(uneprojection); // Ajout de critères de tri // Stratégie de transformation du résultat
         * criteria.setResultTransformer(new AliasToBeanResultTransformer(StatistiquesAvisImpositions.class)); //
         * Exécution de la requête List result = hibernatetemplate.findByCriteria(criteria); return result;
         */
        return null;
    }

}
