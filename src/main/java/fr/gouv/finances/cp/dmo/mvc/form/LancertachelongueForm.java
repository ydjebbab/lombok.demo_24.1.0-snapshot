/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;

/**
 * Class LancertachelongueForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class LancertachelongueForm implements Serializable
{
    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instanciation de lancertachelongue form.
     */
    public LancertachelongueForm()
    {
        super(); // super constructeur

    }

}
