/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : AdresseContribuableDao.java
 *
 */
package fr.gouv.finances.cp.dmo.dao;

import java.util.List;

import fr.gouv.finances.lombok.adresse.bean.PaysIso;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa;

/**
 * Interface AdresseContribuableDao.
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.3 $ Date: 14 déc. 2009
 */
public interface IAdresseContribuableDao extends BaseDaoJpa
{

    /**
     * methode Load pays 
     * 
     * @return list
     */
    public List<PaysIso> loadPays();
}
