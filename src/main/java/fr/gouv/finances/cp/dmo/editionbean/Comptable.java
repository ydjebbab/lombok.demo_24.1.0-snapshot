/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.editionbean;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Comptable.java class Comptable de la commune du Casino.
 * 
 * @author rtoutain-cp Date: 8 févr. 2011
 */

public class Comptable extends BaseBean
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** adresse bp. */
    private String adrBp;

    /** adresse complement. */
    private String adrCompl;

    /** adresse coordonnee bp. */
    private String adrCoordBp;

    /** adresse rue. */
    private String adrRue;

    /** code poste. */
    private String codPost;

    /** dept geographique. */
    private String deptGeo;

    /** insee de la commune. */
    private String inseeCom;

    /** libelle. */
    private String libelle;

    /** libelle de la commune. */
    private String libelleCom;

    /** mel. */
    private String mel;

    /** sages. */
    private String sages;

    /** telephone. */
    private String telephone;

    /**
     * Constructeur de la classe Comptable.java
     */
    public Comptable()
    {
        super();

    }

    /**
     * Accesseur de l attribut adr bp.
     * 
     * @return adr bp
     */
    public String getAdrBp()
    {
        return adrBp;
    }

    /**
     * Modificateur de l attribut adr bp.
     * 
     * @param adrBp le nouveau adr bp
     */
    public void setAdrBp(String adrBp)
    {
        this.adrBp = adrBp;
    }

    /**
     * Accesseur de l attribut adr compl.
     * 
     * @return adr compl
     */
    public String getAdrCompl()
    {
        return adrCompl;
    }

    /**
     * Modificateur de l attribut adr compl.
     * 
     * @param adrCompl le nouveau adr compl
     */
    public void setAdrCompl(String adrCompl)
    {
        this.adrCompl = adrCompl;
    }

    /**
     * Accesseur de l attribut adr coord bp.
     * 
     * @return adr coord bp
     */
    public String getAdrCoordBp()
    {
        return adrCoordBp;
    }

    /**
     * Modificateur de l attribut adr coord bp.
     * 
     * @param adrCoordBp le nouveau adr coord bp
     */
    public void setAdrCoordBp(String adrCoordBp)
    {
        this.adrCoordBp = adrCoordBp;
    }

    /**
     * Accesseur de l attribut adr rue.
     * 
     * @return adr rue
     */
    public String getAdrRue()
    {
        return adrRue;
    }

    /**
     * Modificateur de l attribut adr rue.
     * 
     * @param adrRue le nouveau adr rue
     */
    public void setAdrRue(String adrRue)
    {
        this.adrRue = adrRue;
    }

    /**
     * Accesseur de l attribut cod post.
     * 
     * @return cod post
     */
    public String getCodPost()
    {
        return codPost;
    }

    /**
     * Modificateur de l attribut cod post.
     * 
     * @param codPost le nouveau cod post
     */
    public void setCodPost(String codPost)
    {
        this.codPost = codPost;
    }

    /**
     * Accesseur de l attribut dept geo.
     * 
     * @return dept geo
     */
    public String getDeptGeo()
    {
        return deptGeo;
    }

    /**
     * Modificateur de l attribut dept geo.
     * 
     * @param deptGeo le nouveau dept geo
     */
    public void setDeptGeo(String deptGeo)
    {
        this.deptGeo = deptGeo;
    }

    /**
     * Accesseur de l attribut insee com.
     * 
     * @return insee com
     */
    public String getInseeCom()
    {
        return inseeCom;
    }

    /**
     * Modificateur de l attribut insee com.
     * 
     * @param inseeCom le nouveau insee com
     */
    public void setInseeCom(String inseeCom)
    {
        this.inseeCom = inseeCom;
    }

    /**
     * Accesseur de l attribut libelle.
     * 
     * @return libelle
     */
    public String getLibelle()
    {
        return libelle;
    }

    /**
     * Modificateur de l attribut libelle.
     * 
     * @param libelle le nouveau libelle
     */
    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    /**
     * Accesseur de l attribut libelle com.
     * 
     * @return libelle com
     */
    public String getLibelleCom()
    {
        return libelleCom;
    }

    /**
     * Modificateur de l attribut libelle com.
     * 
     * @param libelleCom le nouveau libelle com
     */
    public void setLibelleCom(String libelleCom)
    {
        this.libelleCom = libelleCom;
    }

    /**
     * Accesseur de l attribut mel.
     * 
     * @return mel
     */
    public String getMel()
    {
        return mel;
    }

    /**
     * Modificateur de l attribut mel.
     * 
     * @param mel le nouveau mel
     */
    public void setMel(String mel)
    {
        this.mel = mel;
    }

    /**
     * Accesseur de l attribut telephone.
     * 
     * @return telephone
     */
    public String getTelephone()
    {
        return telephone;
    }

    /**
     * Modificateur de l attribut telephone.
     * 
     * @param telephone le nouveau telephone
     */
    public void setTelephone(String telephone)
    {
        this.telephone = telephone;
    }

    /**
     * Accesseur de l attribut serialversionuid.
     * 
     * @return serialversionuid
     */
    public static long getSerialversionuid()
    {
        return serialVersionUID;
    }

    /**
     * Accesseur de sages
     *
     * @return sages
     */
    public String getSages()
    {
        return sages;
    }

    /**
     * Mutateur de sages
     *
     * @param sages sages
     */
    public void setSages(String sages)
    {
        this.sages = sages;
    }

}
