/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.batch;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;

/**
 * Batch TraitementProductionEditionsListeContribuablesAvecAdressesImpl
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Service("traitementproductioneditionslistecontribuablesavecadresses")
@Profile("batch")
@Lazy(true)
public class TraitementProductionEditionsListeContribuablesAvecAdressesImpl extends ServiceBatchCommunImpl
{

    @Autowired()
    @Qualifier("editiondemandeserviceimpl")
    private EditionDemandeService editiondemandeserviceso;

    public TraitementProductionEditionsListeContribuablesAvecAdressesImpl()
    {
        super();
    }

    @Value("${traitementproductioneditionslistecontribuablesavecadresses.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch)
    {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementproductioneditionslistecontribuablesavecadresses.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }

    public EditionDemandeService getEditiondemandeserviceso()
    {
        return editiondemandeserviceso;
    }

    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso)
    {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        EditionSynchroneNonNotifieeParMail editionSynchroneNonNotifieeParMail =
            editiondemandeserviceso.initEditionSynchroneNonNotifieeParMail("zf2.adressescontribuables.edition", "amleplatinec-cp");
        Map lesParameditions = new HashMap();
        editionSynchroneNonNotifieeParMail =
            editiondemandeserviceso.declencherEditionCompressee(editionSynchroneNonNotifieeParMail, lesParameditions);
    }

}
