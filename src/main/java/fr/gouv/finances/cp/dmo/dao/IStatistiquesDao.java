/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : StatistiquesDao.java
 *
 */
package fr.gouv.finances.cp.dmo.dao;

import java.util.List;

import fr.gouv.finances.cp.dmo.techbean.StatistiquesAvisImpositions;
import fr.gouv.finances.cp.dmo.techbean.StatistiquesContribuables;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa;

/**
 * Interface StatistiquesDao
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public interface IStatistiquesDao extends BaseDaoJpa
{

    /**
     * methode Agregation avis imposition par type impot
     * 
     * @return list
     */
    public List<StatistiquesAvisImpositions> agregerAvisImpositionParTypeImpot();

    /**
     * methode Agregation avis imposition par type impot seuil
     * 
     * @param seuil param
     * @return list
     */
    public List<StatistiquesAvisImpositions> agregerAvisImpositionParTypeImpotSeuil(Double seuil);

    /**
     * methode Agregation contribuable par type impot
     * 
     * @return list
     */
    public List<StatistiquesContribuables> agregerContribuableParTypeImpot();

    /**
     * methode Agregation par contribuable
     * 
     * @return list
     */
    public List<StatistiquesContribuables> agregerParContribuable();

    /**
     * methode Stat premiers avis imposition
     * 
     * @return list
     */
    public List statPremiersAvisImposition();
}
