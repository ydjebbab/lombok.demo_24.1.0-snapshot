/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.lombok.apptags.tree.bean.Tree;
import fr.gouv.finances.lombok.apptags.tree.bean.TreeNode;
import fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action.AbstractTreeNavigationFormAction;

/**
 * Action TestTagTreeFormAction 
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TestTagTreeFormAction extends AbstractTreeNavigationFormAction
{

    /** Constant : PREFIX_NOEUD_TYPE1. */
    public static final String PREFIX_NOEUD_TYPE1 = "type1";

    /** Constant : PREFIX_NOEUD_TYPE2. */
    public static final String PREFIX_NOEUD_TYPE2 = "type2";

    /**
     * Constructeur 
     */
    public TestTagTreeFormAction()
    {
        super(); 
    }

    /**
     * Constructeur 
     *
     * @param formObjectClass
     */
    public TestTagTreeFormAction(Class<?> formObjectClass)
    {
        super(formObjectClass); 

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param requestContext
     * @return tree model name
     * @see fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action.AbstractTreeNavigationFormAction
     *      #getTreeModelName(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public String getTreeModelName(RequestContext requestContext)
    {
        return "arbre.exemple";
    }

    /**
     * methode Initialise formulaire : DGFiP.
     * 
     * @param requestContext param
     * @return event
     * @throws Exception the exception
     */
    public Event initialiseFormulaire(RequestContext requestContext) throws Exception
    {
        return success();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param requestContext
     * @return tree
     * @see fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action.AbstractTreeNavigationFormAction
     *      #lectureDuNoeudRacineEtDeSesEnfants(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Tree lectureDuNoeudRacineEtDeSesEnfants(RequestContext requestContext)
    {
        Tree tree = new Tree();
        tree.setSingleSelectionMode(true);

        TreeNode root = new TreeNode();
        root.setName("Noeud racine");
        root.setId("racine");

        // Création de noeuds de type folder
        TreeNode enfant1 = new TreeNode();
        enfant1.setName("Enfant 1");
        enfant1.setPrefixNodeId(PREFIX_NOEUD_TYPE1);
        enfant1.setType(PREFIX_NOEUD_TYPE1);
        enfant1.setId("1");
        enfant1.setFolder(true);

        TreeNode enfant2 = new TreeNode();
        enfant2.setName("Enfant 2");
        enfant2.setPrefixNodeId(PREFIX_NOEUD_TYPE1);
        enfant2.setType(PREFIX_NOEUD_TYPE1);
        enfant2.setId("2");
        enfant2.setFolder(true);

        root.addChild(enfant1);
        root.addChild(enfant2);

        // Création de noeud de type feuille
        TreeNode feuille1 = new TreeNode();
        feuille1.setName("Feuille 1");
        feuille1.setPrefixNodeId(PREFIX_NOEUD_TYPE2);
        feuille1.setType(PREFIX_NOEUD_TYPE2);
        feuille1.setId("1");
        feuille1.setFolder(false);
        feuille1.setHasUnloadedChildren(false);

        TreeNode feuille2 = new TreeNode();
        feuille2.setName("Feuille 2");
        feuille2.setPrefixNodeId(PREFIX_NOEUD_TYPE2);
        feuille2.setType(PREFIX_NOEUD_TYPE2);
        feuille2.setId("2");
        feuille2.setFolder(false);
        feuille2.setHasUnloadedChildren(false);

        enfant1.addChild(feuille1);
        enfant2.addChild(feuille2);

        tree.setRoot(root);

        return tree;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tree
     * @param node
     * @param requestContext
     * @see fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action.AbstractTreeNavigationFormAction#lectureEnfantsEtPetitsEnfants(fr.gouv.finances.lombok.apptags.tree.bean.Tree,
     *      fr.gouv.finances.lombok.apptags.tree.bean.TreeNode, org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public void lectureEnfantsEtPetitsEnfants(Tree tree, TreeNode node, RequestContext requestContext)
    {
        // TODO Auto-generated method stub
    }

}
