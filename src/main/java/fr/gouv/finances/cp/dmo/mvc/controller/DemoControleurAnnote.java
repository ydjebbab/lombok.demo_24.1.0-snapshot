/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Exemple de controleur annoté qui renvoit une page web contenant Hello Lombok !
 */
@Controller
public class DemoControleurAnnote
{
    public DemoControleurAnnote()
    {
        super();
    }

    /**
     * <pre>
     * Envoi une page web lorsque l'utilisateur saisit l'une des URLS suivantes :
     * - http://localhost:8080/dmo/hellolombok.ex 
     * - http://localhost:8080/dmo/async/hellolombok 
     * - http://localhost:8080/dmo/rest/hellolombok 
     * (voir web-fragment.xml de lombok pour tous les mappings par défaut)
     * Si l'utilisateur n'est pas connecté (pas de session) et qu'il utilise une URL avec .ex alors l'écran de connexion
     * est appelé (fonctionne ainsi car on passe pas des urls suffixées en .ex, voir dispatcher-servlet.xml)
     * </pre>
     * 
     * @return string
     */
    @RequestMapping(value = {"/hellolombok.ex", "/hellolombok"})
    @ResponseBody
    public String helloLombok()
    {
        return "Hello Lombok !";
    }

}
