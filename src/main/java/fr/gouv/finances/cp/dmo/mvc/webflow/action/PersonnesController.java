/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.cp.dmo.techbean.CriteresRecherches;

/**
 * <pre>
 * Test Lombok avec client riche appelant des services REST. 
 * CF: passage en Java 8
 * </pre>
 */
@Controller
@RequestMapping("/controller")
public class PersonnesController
{

    /** context. */
    @Autowired
    private HttpServletRequest context;

    /** contribuableserviceso. */
    @Autowired
    private IContribuableService contribuableserviceso;

    /**
     * Constructeur
     */
    public PersonnesController()
    {
        super();
    }

    /**
     * Gets the contribuableserviceso.
     *
     * @return the contribuableserviceso
     */
    public IContribuableService getContribuableserviceso()
    {
        return contribuableserviceso;
    }

    /**
     * Sets the contribuableserviceso.
     *
     * @param contribuableserviceso the new contribuableserviceso
     */
    public void setContribuableserviceso(IContribuableService contribuableserviceso)
    {
        this.contribuableserviceso = contribuableserviceso;
    }

    /**
     * methode Delete : .
     *
     * @param uid
     */
    @RequestMapping(value = "/delete", method = RequestMethod.GET, headers = "Accept=*/*")
    @ResponseBody
    public void delete(@RequestParam("uid") String uid)
    {
        List<ContribuablePar> lesContribuables = new ArrayList<>();
        ContribuablePar unContribuable =
            contribuableserviceso.rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID("usertest3416-cp");
        lesContribuables.add(unContribuable);

        if (unContribuable != null)
        {
            contribuableserviceso.supprimerDesContribuablesParticuliers(lesContribuables, "amleplatinec-cp");
        }
        // return prepareJsonResponseASupprimer(unContribuable , null);
        // * return "zf2/confirmercontribuableamodifier";
    };

    /**
     * methode Search : .
     *
     * @param uid
     * @return string
     */
    @RequestMapping(value = "/search", method = {RequestMethod.GET}, headers = "Accept=*/*")
    @ResponseBody
    public String search(@RequestParam("uid") String uid)
    {
        CriteresRecherches criteresRecherches = new CriteresRecherches();
        criteresRecherches.setIdentifiant(uid);
        int count = 0;

        // CF : passage en Java 8
        Optional<List<? extends Contribuable>> listeContribuables = contribuableserviceso.rechercherListeContribuable(criteresRecherches);
        if (listeContribuables.isPresent())
        {
            count = listeContribuables.get().size();
        }
        return prepareJsonResponse(listeContribuables.get(), count, null);
    }

    /**
     * methode Modify1 : .
     *
     * @param uid
     * @return model and view
     */
    @RequestMapping(value = "/modify1", method = RequestMethod.GET, headers = "Accept=*/*")
    @ResponseBody
    public ModelAndView modify1(@RequestParam("uid") String uid)
    {
        //RestTemplate restTemplate = new RestTemplate();
        CriteresRecherches criteresRecherches = new CriteresRecherches();
        criteresRecherches.setIdentifiant(uid);

        // CF : passage en Java 8
        Optional<List<? extends Contribuable>> listeContribuables = contribuableserviceso.rechercherListeContribuable(criteresRecherches);
        if (listeContribuables.isPresent())
        {
            int count = listeContribuables.get().size();
        }

        context.getSession().setAttribute("uidcontribuable", uid);
        context.getSession().setAttribute("contribuablePar", (ContribuablePar) (listeContribuables.get().get(0)));
        // href="http://localhost:8080/dmoweb/zf2/flux.ex?_flowId=modificationcontribuableRD-flow&uidcontribuable=chouard-cp"

        return new ModelAndView(
            "redirect:http://localhost:8081/dmoweb/zf2/flux.ex?_flowId=modificationcontribuableRD-flow&uidcontribuable=" + uid);
    }

    /**
     * methode Modify : .
     *
     * @param uid
     */
    @RequestMapping(value = "/modify", method = RequestMethod.GET, headers = "Accept=*/*")
    @ResponseBody
    public void modify(@RequestParam("uid") String uid)
    {
        CriteresRecherches criteresRecherches = new CriteresRecherches();

        // CF : passage en Java 8
        criteresRecherches.setIdentifiant(uid);
        Optional<List<? extends Contribuable>> listeContribuables = contribuableserviceso.rechercherListeContribuable(criteresRecherches);
        if (listeContribuables.isPresent())
        {
            int count = listeContribuables.get().size();
        }

        context.getSession().setAttribute("uidcontribuable", uid);
        context.getSession().setAttribute("contribuablePar", (ContribuablePar) (listeContribuables.get().get(0)));
        // href="http://localhost:8080/dmoweb/zf2/flux.ex?_flowId=modificationcontribuableRD-flow&uidcontribuable=chouard-cp"
        // return "zf2/confirmercontribuableamodifier";
    }

    /**
     * methode Prepare json response : .
     *
     * @param articles DOCUMENTEZ_MOI
     * @param count DOCUMENTEZ_MOI
     * @param parametresAjax DOCUMENTEZ_MOI
     * @return string
     */
    private String prepareJsonResponse(List<? extends Contribuable> contribuables, int count, MultiValueMap<String, String> parametresAjax)
    {
        JSONObject reponseJson = new JSONObject();
        try
        {
            reponseJson.append("iTotalRecords", count);
            // reponseJson.setiTotalRecords(count);
            Iterator iter = contribuables.iterator();
            String[] aaData = new String[7];
            while (iter.hasNext())
            {
                ContribuablePar uncont = (ContribuablePar) iter.next();
                if (uncont != null)
                {
                    aaData =
                        new String[] {uncont.getIdentifiant(), uncont.getCivilite().getLibelle(), uncont.getNom(), uncont.getPrenom(),
                                uncont.getAdresseMail(), uncont.getDateDeNaissance().toString(), uncont.getSolde().toString()};
                }
                reponseJson.append("aaData", aaData);
            }
        }
        catch (JSONException jsonExc)
        {
            Log.error(jsonExc.getMessage(), jsonExc);
        }
        return reponseJson.toString();
    }

    /**
     * Accesseur de l attribut int first value.
     *
     * @param parametres DOCUMENTEZ_MOI
     * @param key DOCUMENTEZ_MOI
     * @return int first value
     */
    private Integer getIntFirstValue(MultiValueMap<String, String> parametres, String key)
    {
        Integer res = null;
        String s = parametres.getFirst(key);
        if (StringUtils.isNotEmpty(s))
        {
            res = Integer.parseInt(s);
        }
        return res;
    }

}
