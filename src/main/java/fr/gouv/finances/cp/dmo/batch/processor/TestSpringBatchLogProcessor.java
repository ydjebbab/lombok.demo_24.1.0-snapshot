/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.batch.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import fr.gouv.finances.cp.dmo.entite.ContribuableEnt;

/**
 * Class TestSpringBatchLogProcessor
 */
public class TestSpringBatchLogProcessor implements ItemProcessor<ContribuableEnt, ContribuableEnt>
{
    private static final Logger log = LoggerFactory.getLogger(TestSpringBatchLogProcessor.class);

    public TestSpringBatchLogProcessor()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.batch.item.ItemProcessor#process(java.lang.Object)
     */
    @Override
    public ContribuableEnt process(ContribuableEnt item) throws Exception
    {
        log.info("Traitement du contribuable..." + item.getNom());
        return item;
    }

}