/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ContribuableService.java
 *
 */

package fr.gouv.finances.cp.dmo.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.DocumentJoint;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.techbean.CriteresRecherches;

/**
 * Interface IContribuableService
 * 
 * @author chouard-cp
 * @author CF passage en java 8
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
public interface IContribuableService
{

    /**
     * Actualisation de contribuables.
     * 
     * @param listecontribuables param
     * @return the list
     */
    public List<Contribuable> actualiserListeContribuables(List<Contribuable> listecontribuables);

    /**
     * Adhésion à la mensualisation.
     * 
     * @param uncontribuable                param
     * @param untypeimpot                   param
     * @param uncontrat                     param
     * @param referenceavisimposition       param
     * @param identifiantutilisateuroubatch param
     */
    public void adhererAlaMensualisation(ContribuablePar uncontribuable, TypeImpot untypeimpot, ContratMensu uncontrat,
        String referenceavisimposition, String identifiantutilisateuroubatch);

    /**
     * Ajout de contribuables.
     * 
     * @param contribuable param
     * @param adminid      param
     */
    public void ajouterUnContribuable(Contribuable contribuable, String adminid);

    /**
     * Ajouter un document au contribuable.
     * 
     * @param unContribuable  param
     * @param unDocumentJoint param
     */
    public void ajouterUnDocumentAuContribuable(ContribuablePar unContribuable, DocumentJoint unDocumentJoint);

    /**
     * Calcul des années possibles.
     * 
     * @param listetypesimpots param
     * @return the map
     */
    public Map calculerAnneesPriseDEffetPossiblesRG13(List listetypesimpots);

    /**
     * changer La Casse De Tous Les Noms De Contribuables.
     * 
     * @return the int
     */
    public int changerLaCasseDeTousLesNomsDeContribuables();

    /**
     * Charger les documents joints.
     * 
     * @param contribuable param
     */
    public void chargerLesDocumentsJoints(ContribuablePar contribuable);

    /**
     * Modification d'1 contribuables.
     * 
     * @param contribuablep param
     * @param adminid       param
     */
    public void modifierUnContribuable(Contribuable contribuablep, String adminid);

    /**
     * Normalisation de l'adresse d'un contribuable.
     * 
     * @param contribuable param
     */
    public void normaliserAdresseContribuable(ContribuablePar contribuable);

    /**
     * Recherche d'une liste de contribuables à partir de critéres de recherche.
     * 
     * @param criteresrecherches param
     * @return the list
     */
    public Optional<List<? extends Contribuable>> rechercherListeContribuable(final CriteresRecherches criteresrecherches);

    /**
     * Recherche des listes d'impots disponibles à la mensualisation pour 1 contribuable.
     * 
     * @param contribuable param
     * @return the list
     */
    public List<TypeImpot> rechercherListeTypesImpotsDisponiblesALaMensualisationPourUnContribuable(ContribuablePar contribuable);

    /**
     * Recherche d'un contribuable et de ses contrats de mensu , avis d'imposition et type impot à partir de la reference de
     * l'avis d'imposition.
     * 
     * @param referenceavisimposition param
     * @return the contribuable par
     */
    public ContribuablePar rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParReferenceAvis(
        String referenceavisimposition);

    /**
     * Recherche d'un contribuable et de ses contrats de mensu , avis d'imposition et type impot à partir de l'UID.
     * 
     * @param identifiantutilisateur param
     * @return the contribuable par
     */
    public ContribuablePar rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID(
        String identifiantutilisateur);

    /**
     * Suppression de contribuables de type particulier.
     * 
     * @param contribuablesasupprimer param
     * @param adminid                 param
     */
    public void supprimerDesContribuablesParticuliers(List contribuablesasupprimer, String adminid);

    /**
     * Supprimer des documents au contribuable.
     * 
     * @param unContribuable   param
     * @param lesDocASupprimer param
     */
    public void supprimerDesDocumentsDuContribuable(ContribuablePar unContribuable, Collection lesDocASupprimer);

    public List<ContribuablePar> rechercherListeContribuables();
}
