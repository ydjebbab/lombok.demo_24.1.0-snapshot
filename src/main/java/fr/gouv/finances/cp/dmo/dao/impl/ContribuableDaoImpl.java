/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */

package fr.gouv.finances.cp.dmo.dao.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityGraph;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUtil;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.annotations.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IContribuableDao;
import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.AvisImposition_;
import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuableEnt;
import fr.gouv.finances.cp.dmo.entite.ContribuableEnt_;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar_;
import fr.gouv.finances.cp.dmo.entite.PersonneTest;
import fr.gouv.finances.cp.dmo.entite.PersonneTest_;
import fr.gouv.finances.cp.dmo.techbean.CriteresRecherches;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * DAO ContribuableDaoImpl
 * 
 * @author chouard-cp
 * @author CF : passage vers JPA
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Repository("contribuableDao")
public class ContribuableDaoImpl extends BaseDaoJpaImpl implements IContribuableDao
{
    private static final Logger log = LoggerFactory.getLogger(ContribuableDaoImpl.class);

    public ContribuableDaoImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param contribuable
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#deleteContribuable(fr.gouv.finances.cp.dmo.entite.Contribuable)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public void deleteContribuable(Contribuable contribuable)
    {
        log.debug(">>> Debut methode deleteContribuable");
        deleteObject(contribuable);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param reference
     * @return avis imposition
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#findAvisImpositionParReference(java.lang.String)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public AvisImposition findAvisImpositionParReference(String reference)
    {
        // FIXME : CF Cette méthode devrait aller dans un DAO nommé AvisImpositionDaoImpl ...
        log.debug(">>> Debut methode findAvisImpositionParReference");
        // ******************************************************************
        // 1. Création de la requête dynamique (clause FROM)
        // ******************************************************************
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<AvisImposition> criteriaQuery = criteriabuilder.createQuery(AvisImposition.class);
        Root<AvisImposition> avisImpositionRoot = criteriaQuery.from(AvisImposition.class);

        // ******************************************************************
        // 2. Traitement de la liste des critères : clause WHERE
        // ******************************************************************
        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(avisImpositionRoot.get(AvisImposition_.reference), reference));
        criteriaQuery.where(predicate);

        // ******************************************************************
        // 3. Clause SELECT
        // ******************************************************************
        criteriaQuery.select(avisImpositionRoot);

        // ******************************************************************
        // 4. Execution
        // ******************************************************************
        // Pour debug seulement
        TypedQuery<AvisImposition> avisImpositionTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("avisImpositionTypedQuery : " + avisImpositionTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<AvisImposition> avisImpositionListeResultat = avisImpositionTypedQuery.getResultList();
        log.debug("avisImpositionListeResultat : " + avisImpositionListeResultat);
        // On attend un seul avis d'imposition car la reference constitue la clef métier
        if (avisImpositionListeResultat != null && !avisImpositionListeResultat.isEmpty())
        {
            log.debug("avisImpositionListeResultat.size() : " + avisImpositionListeResultat.size());
            return avisImpositionListeResultat.get(0);
        }
        else
        {
            return null;
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param iid
     * @param identifiant
     * @return true, si c'est vrai
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#findIndentifiantContribuableEstAffecte(java.lang.Long,
     *      java.lang.String)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public boolean findIndentifiantContribuableEstAffecte(Long iid, String identifiant)
    {
        log.debug(">>> Debut methode findIndentifiantContribuableEstAffecte");

        boolean isIdentifiantContribuableAffecte = false;

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContribuablePar> criteriaQuery = criteriabuilder.createQuery(ContribuablePar.class);
        Root<ContribuablePar> contribuableParRoot = criteriaQuery.from(ContribuablePar.class);

        Predicate predicate = criteriabuilder.conjunction();
        predicate =
            criteriabuilder.and(predicate, criteriabuilder.equal(contribuableParRoot.get(ContribuablePar_.identifiant), identifiant));
        if (iid != null)
        {
            predicate = criteriabuilder.and(predicate, criteriabuilder.notEqual(contribuableParRoot.get(ContribuablePar_.id), iid));
        }
        criteriaQuery.where(predicate);

        criteriaQuery.select(contribuableParRoot);

        // Pour debug seulement
        TypedQuery<ContribuablePar> contribuableParTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("contribuableParTypedQuery : " + contribuableParTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<ContribuablePar> contribuableParListeResultat = contribuableParTypedQuery.getResultList();
        log.debug("contribuableParListeResultat : " + contribuableParListeResultat);

        if (null != contribuableParListeResultat && !contribuableParListeResultat.isEmpty())
        {
            isIdentifiantContribuableAffecte = true;
        }
        return isIdentifiantContribuableAffecte;
    }

    /**
     * Recherche de contribuables
     * 
     * @param criteresrecherches param
     * @return the list< contribuable>
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public List<Contribuable> findListContribuableViaCriteresRecherches(final CriteresRecherches criteresrecherches)
    {
        log.debug(">>> Debut methode findListContribuableViaCriteresRecherches");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Contribuable> criteriaQuery = criteriabuilder.createQuery(Contribuable.class);
        Root<Contribuable> contribuableRoot = criteriaQuery.from(Contribuable.class);
        Root<ContribuablePar> contribuableParRoot = criteriabuilder.treat(contribuableRoot, ContribuablePar.class);
        // Root<ContribuableEnt> contribuableEntRoot = criteriabuilder.treat(contribuableRoot, ContribuableEnt.class);

        Predicate predicate = criteriabuilder.conjunction();

        switch (criteresrecherches.getTypeRechercheId().intValue())
        {
            case CriteresRecherches.RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT:
                predicate = criteriabuilder.like(contribuableRoot.get(ContribuablePar_.identifiant),
                    '%' + criteresrecherches.getIdentifiant() + '%');
                break;
            case CriteresRecherches.RECHERCHE_CONTRIBUABLE_PAR_MAIL:
                predicate = criteriabuilder.like(contribuableParRoot.get(ContribuablePar_.adresseMail),
                    '%' + criteresrecherches.getAdresseMail() + '%');
                break;
            case CriteresRecherches.RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT_ET_MAIL:
                predicate = criteriabuilder.and(
                    criteriabuilder.like(contribuableRoot.get(ContribuablePar_.identifiant),
                        '%' + criteresrecherches.getIdentifiant() + '%'),
                    criteriabuilder.like(contribuableParRoot.get(ContribuablePar_.adresseMail),
                        '%' + criteresrecherches.getAdresseMail() + '%'));
                break;
            case CriteresRecherches.RECHERCHE_CONTRIBUABLE_PAR_IDENTIFIANT_OU_MAIL:
                predicate = criteriabuilder.or(
                    criteriabuilder.like(contribuableRoot.get(ContribuablePar_.identifiant),
                        '%' + criteresrecherches.getIdentifiant() + '%'),
                    criteriabuilder.like(contribuableParRoot.get(ContribuablePar_.adresseMail),
                        '%' + criteresrecherches.getAdresseMail() + '%'));
                break;
        }
        criteriaQuery.where(predicate);

        criteriaQuery.select(contribuableRoot);
        // Pour debug seulement
        TypedQuery<Contribuable> contribuableTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("contribuableTypedQuery : " + contribuableTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());
        contribuableTypedQuery.setMaxResults(criteresrecherches.getMaximumLignesRetournees() + 1);

        List<Contribuable> contribuableListeResultat = contribuableTypedQuery.getResultList();
        log.debug("contribuableListeResultat : " + contribuableListeResultat);

        return contribuableListeResultat;
    }

    /*
     * exemple de requête en HQL (mieux vaut utiliser les critères)
     */

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param uid
     * @return avis imposition
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#findMaxAvisPourUnContribuable(java.lang.String)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public AvisImposition findMaxAvisPourUnContribuable(String uid)
    {
        log.debug(">>> Debut methode findMaxAvisPourUnContribuable");
        // FIXME : CF TODO
        // CF: Méthode non utilisée. A faire plus tard éventuellement.

        /*
         * HibernateTemplate hibernateTemplate = getHibernateTemplate(); DetachedCriteria maxAvisPourUnContribuable =
         * DetachedCriteria.forClass(AvisImposition.class, "avismax");
         * maxAvisPourUnContribuable.setProjection(Property.forName("montant").max());
         * maxAvisPourUnContribuable.add(Property.forName("avismax.contribuablePar").eqProperty("avis.contribuablePar")) ;
         * DetachedCriteria criteria = DetachedCriteria.forClass(AvisImposition.class, "avis");
         * criteria.add(Property.forName("montant").eq(maxAvisPourUnContribuable)); criteria.setFetchMode("contribuablePar",
         * FetchMode.JOIN); criteria.createAlias("contribuablePar", "contribuable");
         * criteria.add(Restrictions.eq("contribuable.identifiant", uid)); Iterator iteravisimposition =
         * hibernateTemplate.findByCriteria(criteria).iterator(); AvisImposition unavis = null; if
         * (iteravisimposition.hasNext()) { unavis = (AvisImposition) iteravisimposition.next(); } return null;
         */
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param contribuablepar
     * @return true, si c'est vrai
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#findNomPrenomAdresseMailExisteDeja(fr.gouv.finances.cp.dmo.entite.ContribuablePar)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public boolean findNomPrenomAdresseMailExisteDeja(final ContribuablePar contribuablepar)
    {
        log.debug(">>> Debut methode findNomPrenomAdresseMailExisteDeja");
        boolean rec = false;
        Long id = contribuablepar.getId();
        String nom = contribuablepar.getNom();
        String prenom = contribuablepar.getPrenom();
        String adressemail = contribuablepar.getAdresseMail();

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContribuablePar> criteriaQuery = criteriabuilder.createQuery(ContribuablePar.class);
        Root<ContribuablePar> contribuableParRoot = criteriaQuery.from(ContribuablePar.class);

        Predicate predicate = criteriabuilder.conjunction();
        if (adressemail != null)
        {
            predicate =
                criteriabuilder.and(predicate, criteriabuilder.equal(contribuableParRoot.get(ContribuablePar_.adresseMail), adressemail));
        }
        if (id != null)
        {
            predicate = criteriabuilder.and(predicate, criteriabuilder.notEqual(contribuableParRoot.get(ContribuablePar_.id), id));
        }
        criteriaQuery.where(predicate);
        criteriaQuery.select(contribuableParRoot);

        // Pour debug seulement
        TypedQuery<ContribuablePar> contribuableParTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("contribuableParTypedQuery : " + contribuableParTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<ContribuablePar> contribuableParListeResultat = contribuableParTypedQuery.getResultList();
        log.debug("contribuableParListeResultat : " + contribuableParListeResultat);

        String patt = "[-'\\s]*";
        Pattern r = Pattern.compile(patt);

        for (Iterator<ContribuablePar> iter = contribuableParListeResultat.iterator(); iter.hasNext();)
        {
            ContribuablePar contri = iter.next();
            rec = comparerString(r, nom, contri.getNom()) && comparerString(r, prenom, contri.getPrenom());
            if (rec)
            {
                break;
            }
        }
        return rec;
    }

    /**
     * Recherche un contribuable de type entreprise par son UID.
     * 
     * @param identifiant param
     * @return un contribuable entreprise
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public ContribuableEnt findContribuableEntrepriseParUID(final String identifiant)
    {
        log.debug(">>> Debut methode findUnContribuableEntrepriseParUID");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContribuableEnt> criteriaQuery = criteriabuilder.createQuery(ContribuableEnt.class);
        Root<ContribuableEnt> contribuableEntRoot = criteriaQuery.from(ContribuableEnt.class);

        Predicate predicate = criteriabuilder.conjunction();
        predicate =
            criteriabuilder.and(predicate, criteriabuilder.equal(contribuableEntRoot.get(ContribuableEnt_.identifiant), identifiant));
        criteriaQuery.where(predicate);

        criteriaQuery.select(contribuableEntRoot);

        // Pour debug seulement
        TypedQuery<ContribuableEnt> contribuableEntTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("contribuableEntTypedQuery : " + contribuableEntTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        ContribuableEnt contribuableEntResultat = contribuableEntTypedQuery.getSingleResult();
        log.debug("contribuableEntResultat : " + contribuableEntResultat);
        return contribuableEntResultat;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param referenceAvis
     * @return contribuable par
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#findContribuableEtAvisImpositionEtContratsMensualisationParReferenceAvis(java.lang.String)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public ContribuablePar findContribuableEtAvisImpositionEtContratsMensualisationParReferenceAvis(
        String referenceAvis)
    {
        log.debug(">>> Debut methode findUnContribuableEtAvisImpositionEtContratsMensualisationParReferenceAvis");
        ContribuablePar contribuablePar = null;

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<AvisImposition> criteriaQuery = criteriabuilder.createQuery(AvisImposition.class);
        Root<AvisImposition> avisImpositionRoot = criteriaQuery.from(AvisImposition.class);
        avisImpositionRoot.fetch("contribuablePar", JoinType.INNER);

        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(avisImpositionRoot.get(AvisImposition_.reference), referenceAvis));
        criteriaQuery.where(predicate);

        criteriaQuery.select(avisImpositionRoot);

        // Pour debug seulement
        TypedQuery<AvisImposition> avisImpositionTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("avisImpositionTypedQuery : " + avisImpositionTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<AvisImposition> avisImpositionListe = avisImpositionTypedQuery.getResultList();
        log.debug("avisImpositionListe : " + avisImpositionListe);

        if (null != avisImpositionListe && !avisImpositionListe.isEmpty())
        {
            contribuablePar = avisImpositionListe.get(0).getContribuablePar();
            contribuablePar = findContribuableEtAvisImpositionEtContratsMensualisationParUID(contribuablePar.getIdentifiant());
        }

        return contribuablePar;
    }

    /**
     * Exemple de requête avec critère : à privilégier.
     * 
     * @param identifiant param
     * @return the contribuable par
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    @SuppressWarnings("unchecked")
    public ContribuablePar findContribuableEtAvisImpositionEtContratsMensualisationParUID(final String identifiant)
    {
        log.debug(">>> Debut methode findUnContribuableEtAvisImpositionEtContratsMensualisationParUID");
        log.debug("identifiant : " + identifiant);

        EntityGraph<ContribuablePar> entityGraphAvisImposition =
            (EntityGraph<ContribuablePar>) entityManager.getEntityGraph(ContribuablePar.AVISIMPOSITIONCONTRATMENSU_GRAPH);

        EntityGraph<ContribuablePar> entityGraphDeclarationRevenu =
            (EntityGraph<ContribuablePar>) entityManager.getEntityGraph(ContribuablePar.DECLARATIONREVENU_GRAPH);

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContribuablePar> criteriaQuery = criteriabuilder.createQuery(ContribuablePar.class);
        Root<ContribuablePar> contribuableParRoot = criteriaQuery.from(ContribuablePar.class);

        criteriaQuery.select(contribuableParRoot);

        Predicate predicate = criteriabuilder.conjunction();
        predicate =
            criteriabuilder.and(predicate, criteriabuilder.equal(contribuableParRoot.get(ContribuablePar_.identifiant), identifiant));
        criteriaQuery.where(predicate);

        TypedQuery<ContribuablePar> contribuableParTypedQuery = entityManager.createQuery(criteriaQuery);
        contribuableParTypedQuery.setHint(QueryHints.LOADGRAPH, entityGraphAvisImposition);
        contribuableParTypedQuery.setHint(QueryHints.LOADGRAPH, entityGraphDeclarationRevenu);
        // contribuableParTypedQuery.setHint(QueryHints.FETCHGRAPH, entityGraph);

        // Pour debug seulement
        log.debug("contribuableParTypedQuery : " + contribuableParTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<ContribuablePar> contribuableParListeResultat = contribuableParTypedQuery.getResultList();

        // Pour debug seulement
        if (null != contribuableParListeResultat)
        {
            for (ContribuablePar contribuablePar : contribuableParListeResultat)
            {
                afficherEtatChargement(contribuablePar);
            }
        }

        log.debug("contribuableParListeResultat : " + contribuableParListeResultat);
        Iterator<ContribuablePar> itercontribuable = contribuableParListeResultat.iterator();
        ContribuablePar contribuablePar = null;
        if (itercontribuable.hasNext())
        {
            contribuablePar = itercontribuable.next();
        }
        return contribuablePar;
    }

    // Exemple en requêtant des sous-classes particulières
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param identifiant
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#findContribuableParSousClasseEtIdentifiant(java.lang.String)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public List<Contribuable> findContribuableParSousClasseEtIdentifiant(final String identifiant)
    {
        log.debug(">>> Debut methode findUnContribuableParSousClasseEtIdentifiant");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Contribuable> criteriaQuery = criteriabuilder.createQuery(Contribuable.class);
        Root<Contribuable> contribuableRoot = criteriaQuery.from(Contribuable.class);
        Root<ContribuablePar> contribuableParRoot = criteriabuilder.treat(contribuableRoot, ContribuablePar.class);

        Predicate predicate = criteriabuilder.equal(contribuableParRoot.get(ContribuablePar_.identifiant), identifiant);
        criteriaQuery.where(predicate);

        criteriaQuery.select(contribuableParRoot);

        // Pour debug seulement
        TypedQuery<Contribuable> contribuableTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("contribuableTypedQuery : " + contribuableTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<Contribuable> contribuableListeResultat = contribuableTypedQuery.getResultList();
        log.debug("contribuableListeResultat : " + contribuableListeResultat);
        return contribuableListeResultat;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param contribuable
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#initializeLesDocumentsJoints(fr.gouv.finances.cp.dmo.entite.ContribuablePar)
     */
    @Override
    public void initializeLesDocumentsJoints(final ContribuablePar contribuable)
    {
        log.debug(">>> Debut methode initializeLesDocumentsJoints");
        // CF : pour le préchargement des collections.
        // Dans JPA, il n'y a pas d'équivalent de Hibernate.isInitialized
        // https://stackoverflow.com/questions/29368563/jpa-equivalent-command-to-hibernate-initialize

        /*
         * HibernateTemplate hibernateTemplate = getHibernateTemplate(); hibernateTemplate.execute(new HibernateCallback() {
         * public Object doInHibernate(Session session) throws HibernateException { session.lock(contribuable, LockMode.READ);
         * if (!Hibernate.isInitialized(contribuable.getLesDocsJoints())) {
         * Hibernate.initialize(contribuable.getLesDocsJoints()); } for (Iterator docjointIterator =
         * contribuable.getLesDocsJoints().iterator(); docjointIterator.hasNext();) { DocumentJoint documentjoint =
         * (DocumentJoint) docjointIterator.next(); session.lock(documentjoint, LockMode.READ); if
         * (!Hibernate.isInitialized(documentjoint.getLeFichierJoint())) {
         * Hibernate.initialize(documentjoint.getLeFichierJoint()); } } ; return null; } });
         */
    }

    /**
     * Exemple pour revenir charger les objets liés après avoir charger l'objet principal sans fetchmode.join : genere une
     * requete par initialize. Donc si il y a une bonne probabilte que l'utilisateur accede au donnes rattachees, il faut
     * les charger dans la premiere requete sinon il faut utiliser cette technique
     * 
     * @param contribuable param
     */
    @Override
    public void initializeListesRattacheesAUnContribuable(final ContribuablePar contribuable)
    {
        log.debug(">>> Debut methode initializeListesRattacheesAUnContribuable");
        // CF : pour le préchargement des collections.
        // Dans JPA, il n'y a pas d'équivalent de Hibernate.isInitialized
        // https://stackoverflow.com/questions/29368563/jpa-equivalent-command-to-hibernate-initialize

        /*
         * HibernateTemplate hibernateTemplate = getHibernateTemplate(); hibernateTemplate.execute(new HibernateCallback() {
         * public Object doInHibernate(Session session) throws HibernateException {
         * Hibernate.initialize(contribuable.getListeContratMensu());
         * Hibernate.initialize(contribuable.getListeAvisImposition()); Hibernate.initialize(contribuable.getListeAdresses());
         * Hibernate.initialize(contribuable.getLesDocsJoints()); return null; } });
         */

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param contribuablePar
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#refreshContribuableParticulier(fr.gouv.finances.cp.dmo.entite.ContribuablePar)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public void refreshContribuableParticulier(ContribuablePar contribuablePar)
    {
        log.debug(">>> Debut methode refreshContribuableParticulier");
        entityManager.refresh(contribuablePar);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param contribuablepar
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#saveContribuableParticulier(fr.gouv.finances.cp.dmo.entite.ContribuablePar)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public void saveContribuableParticulier(final ContribuablePar contribuablepar)
    {
        if (LOGGER.isDebugEnabled())
        {
            log.debug(">>> Debut methode saveContribuableParticulier");
            LOGGER.debug("Identifiant du contribuablepar : " + contribuablepar.getIdentifiant());
        }
        modifyObject(contribuablepar);
        // getHibernateTemplate().saveOrUpdate(contribuablepar);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param contribuableent
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#saveContribuableEntreprise(fr.gouv.finances.cp.dmo.entite.ContribuableEnt)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public void saveContribuableEntreprise(final ContribuableEnt contribuableent)
    {
        if (LOGGER.isDebugEnabled())
        {
            log.debug(">>> Debut methode saveContribuableEntreprise");
            LOGGER.debug("Identifiant du contribuablepar : " + contribuableent.getIdentifiant());
        }
        modifyObject(contribuableent);
        // getHibernateTemplate().saveOrUpdate(contribuableent);
    }

    /**
     * <pre>
     * Cette méthode permet d'obtenir un iterateur permettant de parcourir tous les contribuablesparticuliers. 
     * Lorsque l'objet suivant est demandé via l'iterateur (next). Lorsqu'il n'y a plus d'objets "ContribuablePar" à lire dans
     * le bloc d'objets courant, le curseur scrollableresult est sollicité pour lire en base et mapper un nouveau bloc
     * d'objets "ContribuablePar".
     * </pre>
     * 
     * @param nombreOccurences : Nombre d'occurences lues à la fois par le scrolliterator (compromis performance/occupation
     *                         mémoire)
     * @return un DGCPScolliterator qui permet d'itérer dans le résultat en sollicitant le curseur chaque fois que
     *         l'enregistrement suivant n'est pas chargé
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public ScrollIterator findTousContribuableParIterator(int nombreOccurences)
    {
        log.debug(">>> Debut methode findTousContribuableParIterator");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContribuablePar> criteriaQuery = criteriabuilder.createQuery(ContribuablePar.class);
        Root<Contribuable> contribuableRoot = criteriaQuery.from(Contribuable.class);
        Root<ContribuablePar> contribuableParRoot = criteriabuilder.treat(contribuableRoot, ContribuablePar.class);

        criteriaQuery.select(contribuableParRoot);

        ScrollIterator scrollIterator = getScrollIterator(criteriaQuery, nombreOccurences);
        return scrollIterator;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nombreOccurences
     * @return dgcp scroll iterator
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#findTousContribuablesEtAdressesParIterator(int)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public ScrollIterator findTousContribuablesEtAdressesParIterator(int nombreOccurences)
    {
        log.debug(">>> Debut methode findTousContribuablesEtAdressesParIterator");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContribuablePar> criteriaQuery = criteriabuilder.createQuery(ContribuablePar.class);
        Root<Contribuable> contribuableRoot = criteriaQuery.from(Contribuable.class);
        Root<ContribuablePar> contribuableParRoot = criteriabuilder.treat(contribuableRoot, ContribuablePar.class);
        contribuableParRoot.fetch(ContribuablePar_.listeAdresses, JoinType.INNER);

        criteriaQuery.select(contribuableParRoot);

        ScrollIterator scrollIterator = getScrollIterator(criteriaQuery, nombreOccurences);
        return scrollIterator;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nombreOccurences
     * @return dgcp scroll iterator
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#findTousContribuablesEtAdressesTestParIterator(int)
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public ScrollIterator findTousContribuablesEtAdressesTestParIterator(int nombreOccurences)
    {
        log.debug(">>> Debut methode findTousContribuablesEtAdressesTestParIterator");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PersonneTest> criteriaQuery = criteriabuilder.createQuery(PersonneTest.class);
        Root<PersonneTest> personneTestRoot = criteriaQuery.from(PersonneTest.class);
        personneTestRoot.fetch(PersonneTest_.listeAdresses, JoinType.INNER);

        criteriaQuery.select(personneTestRoot);

        ScrollIterator scrollIterator = getScrollIterator(criteriaQuery, nombreOccurences);
        return scrollIterator;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.IContribuableDao#findTousContribuablesEtAdressesTestWithoutIterator()
     */
    @Override
    @Transactional(transactionManager = "transactionManager")
    public List<PersonneTest> findTousContribuablesEtAdressesTestWithoutIterator()
    {
        log.debug(">>> Debut methode findTousContribuablesEtAdressesTestWithoutIterator");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PersonneTest> criteriaQuery = criteriabuilder.createQuery(PersonneTest.class);
        Root<PersonneTest> personneTestRoot = criteriaQuery.from(PersonneTest.class);
        personneTestRoot.fetch(PersonneTest_.listeAdresses, JoinType.INNER);

        criteriaQuery.select(personneTestRoot);

        // Pour debug seulement
        TypedQuery<PersonneTest> personneTestTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("personneTestTypedQuery : " + personneTestTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<PersonneTest> personneTestListeResultat = personneTestTypedQuery.getResultList();
        log.debug("personneTestListeResultat : " + personneTestListeResultat);
        return personneTestListeResultat;
    }

    /**
     * Comparaison de strings
     * 
     * @param patr param
     * @param str1 param
     * @param str2 param
     * @return true, si c'est vrai
     */
    private boolean comparerString(Pattern pattern, String str1, String str2)
    {
        boolean rcc = false;
        Matcher mmm;

        if ((str1 == null && str2 == null))
        {
            rcc = false;
        }
        else if (str1 != null && str1.equals(str2))
        {
            rcc = true;
        }
        else
        {
            // normalisation de la chaîne str1
            mmm = pattern.matcher(str1);
            String str1normalise = mmm.replaceAll("").toUpperCase(Locale.FRANCE);

            // normalisation de la chaîne str2
            mmm = pattern.matcher(str2);
            String str2normalise = mmm.replaceAll("").toUpperCase(Locale.FRANCE);

            rcc = str1normalise.equals(str2normalise);
        }
        return rcc;
    }

    /*
     * CF : méthode utilitaire pour debug
     */
    private void afficherEtatChargement(ContribuablePar contribuablePar)
    {
        PersistenceUtil pu = Persistence.getPersistenceUtil();
        log.debug(" chargement de contribuablePar : " + pu.isLoaded(contribuablePar));
        log.debug(" chargement de contribuablePar.listeAvisImposition : " + pu.isLoaded(contribuablePar, "listeAvisImposition"));
        log.debug(" chargement de contribuablePar.listeContratMensu : " + pu.isLoaded(contribuablePar, "listeContratMensu"));
        log.debug(" chargement de contribuablePar.anneeDeNaissance : " + pu.isLoaded(contribuablePar, "anneeDeNaissance"));
        log.debug(" chargement de contribuablePar.listeAdresses : " + pu.isLoaded(contribuablePar, "listeAdresses"));
        log.debug(" chargement de contribuablePar.situationFamiliale : " + pu.isLoaded(contribuablePar, "situationFamiliale"));
    }

    @Override
    public List<ContribuablePar> findAllContribuable()
    {
        log.debug(">>> Debut methode findAllContribuable");

        // EntityGraph<ContribuablePar> entityGraphDocsJoints =
        // (EntityGraph<ContribuablePar>) entityManager.getEntityGraph(ContribuablePar.TOUT_GRAPH);

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContribuablePar> criteriaQuery = criteriabuilder.createQuery(ContribuablePar.class);
        Root<ContribuablePar> contribuableParRoot = criteriaQuery.from(ContribuablePar.class);

        // criteriaQuery.select(contribuableParRoot);
        criteriaQuery.multiselect(contribuableParRoot.get(ContribuablePar_.nom), contribuableParRoot.get(ContribuablePar_.prenom),
            contribuableParRoot.get(ContribuablePar_.adresseMail), contribuableParRoot.get(ContribuablePar_.telephoneFixe),
            contribuableParRoot.get(ContribuablePar_.telephonePortable));

        // Pour debug seulement
        TypedQuery<ContribuablePar> contribuableTypedQuery = entityManager.createQuery(criteriaQuery);
        // contribuableTypedQuery.setHint(QueryHints.LOADGRAPH, entityGraphDocsJoints);
        log.debug("contribuableTypedQuery : " + contribuableTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<ContribuablePar> contribuableParListResultat = contribuableTypedQuery.getResultList();
        // log.debug("contribuableListResultat : " + contribuableListResultat);
        return contribuableParListResultat;
    }

}
