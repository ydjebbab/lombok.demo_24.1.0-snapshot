/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : RechercheCodePostalFormAction.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.cp.dmo.mvc.form.RechercheCodePostalForm;
import fr.gouv.finances.cp.dmo.techbean.CriteresRechercheCodePostal;
import fr.gouv.finances.lombok.adresse.service.AdresseService;

/**
 * Action RechercheCodePostalFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class RechercheCodePostalFormAction extends FormAction
{
    /** Constant : ATTR_COMMUNE_SAV. */
    private static final String ATTR_COMMUNE_SAV = "communesav";

    /** Constant : ATTR_CODEPOSTAL_SAV. */
    private static final String ATTR_CODEPOSTAL_SAV = "codepostalsav";

    /** adresseserviceso. */
    private AdresseService adresseserviceso;

    /**
     * Constructeur
     */
    public RechercheCodePostalFormAction()
    {
        super();
    }

    /**
     * Constructeur
     * 
     * @param arg0 param
     */
    public RechercheCodePostalFormAction(Class<?> arg0)
    {
        super(arg0);
    }

    /**
     * methode Executer rechercher
     * 
     * @param rc param
     * @return event
     */
    public Event executerRechercher(RequestContext rc)
    {
        RechercheCodePostalForm codepostal = (RechercheCodePostalForm) rc.getFlowScope().get(getFormObjectName());
        List listecodepostaux =
            adresseserviceso.rechercherCodePostalParCodeVille(codepostal.getCritere().getCode(), codepostal
                .getCritere().getVille());
        rc.getFlowScope().put("listecodepostaux", listecodepostaux);

        return success();
    }

    /**
     * Gets the adresseserviceso.
     *
     * @return the adresseserviceso
     */
    public AdresseService getAdresseserviceso()
    {
        return adresseserviceso;
    }

    /**
     * Sets the adresseserviceso.
     *
     * @param adresseserviceso the new adresseserviceso
     */
    public void setAdresseserviceso(AdresseService adresseserviceso)
    {
        this.adresseserviceso = adresseserviceso;
    }

    /**
     * methode Initialiser formulaire
     * 
     * @param rc param
     * @return event
     */
    public Event initialiserFormulaire(RequestContext rc)
    {

        // Récupération des attributs paramètres en entrée du flux
        // s'ils sont présents
        String ville = (String) rc.getFlowScope().get("ville");
        String code = (String) rc.getFlowScope().get("code");

        // Sauvegardes dans des paramètres d'entrée dans le contexte du
        // flux
        rc.getFlowScope().put(RechercheCodePostalFormAction.ATTR_CODEPOSTAL_SAV, code);
        rc.getFlowScope().put(RechercheCodePostalFormAction.ATTR_COMMUNE_SAV, ville);

        // Création du formulaire
        RechercheCodePostalForm supportform = (RechercheCodePostalForm) rc.getFlowScope().get(getFormObjectName());

        // Affectation de copies des paramètres d'entrée au formulaire
        supportform.getCritere().setCode(code);
        supportform.getCritere().setVille(ville);

        // return supportform;
        return success();
    }

    /**
     * methode Preparer bean criteres de recherche
     * 
     * @param rc param
     * @return event
     */
    public Event preparerBeanCriteresDeRecherche(RequestContext rc)
    {
        // Initialisation bean typesderecherche
        Map typesderecherche = CriteresRechercheCodePostal.getTypesderecherche();
        rc.getFlowScope().put("typesderecherche", typesderecherche);

        // Initialisation d'une liste vide
        List listecodepostaux = new ArrayList();
        rc.getFlowScope().put("listecodepostaux", listecodepostaux);
        return success();
    }

    /**
     * methode Selectionner
     * 
     * @param rc param
     * @return event
     */
    public Event selectionner(RequestContext rc)
    {
        String code = rc.getExternalContext().getRequestParameterMap().get("code");
        rc.getFlowScope().put("code", code);
        String ville = rc.getExternalContext().getRequestParameterMap().get("ville");
        rc.getFlowScope().put("ville", ville);
        return success();
    }

}
