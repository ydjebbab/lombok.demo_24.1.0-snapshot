/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.config;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import fr.gouv.finances.lombok.config.LombokWebSharedConfiguration;
import fr.gouv.finances.lombok.config.ServiceWebSecurityConfigurer;
import fr.gouv.finances.lombok.securite.springsecurity.AucunLdapAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.LdapAuthentificationSecurityLombok;
import fr.gouv.finances.lombok.securite.springsecurity.RealAuthentificationSecurityLombok;

/**
 * <pre>
 * Classe de configuration de la sécurité dans l'application démo. 
 * Configuration à compléter pour les URLS.
 * Cette classe remplace la partie servlet déclarée dans le fichier WEB.XML et elle remplace
 * également les définitions contenues dans les fichiers APPLICATIONCONTEXT-COMMUN-SECURITY.XML et SECURITY.XML
 * Normalement une source de propriétés est créée par spring boot 
 * (depuis le fichier APPLICATION.PROPERTIES pour permettre de résoudre les annotations Value)
 * </pre>
 */
@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = {"fr.gouv.finances.cp.dmo.controller", "fr.gouv.finances.cp.dmo.rest.controller"})
public class LombokWebSecurityConfiguration extends WebSecurityConfigurerAdapter
{
    private static final Logger log = LoggerFactory.getLogger(LombokWebSecurityConfiguration.class);

    private final String administrateur = Authorite.ADMINISTRATEUR.name();

    private final String exploitant = Authorite.EXPLOITANT.name();

    private final String chefProjetApplicatif = Authorite.CHEFDEPROJETAPPLICATIF.name();

    private final String particulier = Authorite.PARTICULIER.name();

    private final String roleQuiNexistePas = Authorite.ROLE_QUI_N_EXISTE_PAS.name();

    private final String rolls = Authorite.ROLLS.name();

    private final String notaires = Authorite.NOTAIRES.name();

    private static final String[] FLOWS_PRIVE = new String[] {
            ".*creationcontribuable-flow.*",
            ".*editionlistecontribuables-flow.*",
            ".*gestionavisimposition-flow.*",
            ".*gestiondocuments-flow.*",
            ".*modificationcontribuable-flow.*",
            ".*recherchecodepostal-flow.*",
            ".*recherchecontribuable-flow.*",
            ".*suppressioncontribuable-flow.*",
            ".*statistiques-flow.*",
            ".*referencetypesimpots-flow.*",
            ".*referencetypesimpotspouredition-flow.*"};

    private static final String[] FLOWS_PRIVE2 = new String[] {
            ".*demandeadhesionmensualisation-flow.*",
            ".*selectionavisimpositionrattache-flow.*"};

    private static final String[] FLOWS_PUBLIC = new String[] {
            ".*lancertachelongue-flow.*",
            ".*testtags-flow.*",
            ".*testtagtree-flow.*",
            ".*consultationstructures-flow.*",
            ".*rechercheguichetbancaire-flow.*",
            ".*ajoutdocumentatlas-flow.*",
            ".*recherchedocumentatlas-flow.*",
            ".*administrationeditions-flow.*",
            ".*administrationpurgeseditions-flow.*",
            ".*consultationbanette-flow.*",
            ".*statistiqueseditions-flow.*",
            ".*affichagecaracteristiquesutilisateur-flow.*",
            ".*rechercheoperationsjournal-flow.*",
            ".*transfereroperationsjournal-flow.*",
            ".*catalogues-flow.*",
            ".*classeurs-flow.*",
            ".*creationmessage-flow.*",
            ".*creationmessageavecrestrictionsprofilsetcodiques-flow.*",
            ".*modificationmessage-flow.*",
            ".*recherchemessages-flow.*",
            ".*recherchemessagestypes-flow.*",
            ".*suppressionmessages-flow.*",
            ".*creationmessagetype-flow.*",
            ".*suppressionmessagestype-flow.*",
            ".*recherchetousmessages-flow.*",
            ".*creationapplication-flow.*",
            ".*suppressionapplication-flow.*"
    };

    @Value(value = "${lombok.aptera.codeappli}")
    String codeApplicationDansHabilitations;

    @Value(value = "${authentification.modeauthentification}")
    String modeAuthentification;

    @Value(value = "${annuaire.typeannuaire}")
    String typeAnnuaire;

    @Autowired
    LombokWebSharedConfiguration lombokSiremeConfig;

    /** handler en cas d'authentification réussie **/
    @Autowired
    AuthenticationSuccessHandler authenticationSuccessHandler;

    /** handler en cas d'authentification en échec **/
    @Autowired
    AuthenticationFailureHandler authenticationFailureHandler;

    /**
     * Ce controlleur est retrouvé grâce à l'annotation de scan
     */
    @Autowired
    private AccessDeniedHandler accessDeniedHandler;

    // CF: Le bean logoutSuccessHandler est défini dans LombokWebSharedConfiguration
    @Autowired
    private LogoutSuccessHandler logoutSuccessHandler;

    /**
     * classes de configuration additionnelles (cumulatives) qui héritent de l'interface ServiceWebSecurityConfigurer
     */
    @Autowired(required = false)
    List<ServiceWebSecurityConfigurer> additionalWebSecurityConfigurer;

    public LombokWebSecurityConfiguration()
    {
        super();
    }

    public LombokWebSecurityConfiguration(boolean disableDefaults)
    {
        super(disableDefaults);
    }

    /**
     * Provider pour l'accès sans LDAP.
     *
     * @return aucun ldap authentification security lombok
     */
    @Bean
    AucunLdapAuthentificationSecurityLombok authenticationAucunLdapProvider()
    {
        AucunLdapAuthentificationSecurityLombok aucunLdapProvider = new AucunLdapAuthentificationSecurityLombok();
        aucunLdapProvider.setTypeAnnuaire(typeAnnuaire);
        aucunLdapProvider.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        aucunLdapProvider.setModeAuthentification(modeAuthentification);
        return aucunLdapProvider;
    }

    /**
     * Provider pour l'accès avec token REAL.
     *
     * @return real authentification security lombok
     */
    @Bean
    RealAuthentificationSecurityLombok authentificationRealProvider()
    {
        RealAuthentificationSecurityLombok realProvider = new RealAuthentificationSecurityLombok();
        realProvider.setTypeAnnuaire(typeAnnuaire);
        realProvider.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        realProvider.setModeAuthentification(modeAuthentification);
        return realProvider;
    }

    /**
     * Provider pour l'accès avec ldap.
     *
     * @return ldap authentification security lombok
     */
    @Bean
    LdapAuthentificationSecurityLombok authenticationLdapProvider()
    {
        LdapAuthentificationSecurityLombok ldapProvider = new LdapAuthentificationSecurityLombok();
        ldapProvider.setAuthentificationEtLectureHabilitationsService(lombokSiremeConfig.authentificationEtLectureHabilitationsService());
        ldapProvider.setCodeApplicationDansHabilitations(codeApplicationDansHabilitations);
        ldapProvider.setModeAuthentification(modeAuthentification);
        ldapProvider.setSiremeserviceso(lombokSiremeConfig.getSiremeserviceso());

        return ldapProvider;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        log.debug(">>> Debut methode LombokWebSecurityConfiguration.configure()");

        // @formatter:off
        http
            .csrf()
                .disable()
            .formLogin()
                .loginPage("/identification.ex")
                .loginProcessingUrl("/j_spring_security_check")
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .and()
            .logout()
                .logoutUrl("/j_spring_security_logout")
                // CF : ajout
                .logoutSuccessHandler(logoutSuccessHandler)
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID")
                .and()
            .exceptionHandling().accessDeniedHandler(accessDeniedHandler)
                .and()
            .authorizeRequests()
                // autorisation des ressource lombok web et webjars                
                .antMatchers("/webjars/**", "/composants/**", "/application/**", "/rest/**").permitAll()
                // autorisation feuille de style inea applicative
                .antMatchers("/css/**").permitAll()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/", "/identification.ex", "/errors/**", "/j_appelportail", "/simulportail.html").permitAll()
                // Swagger
                .antMatchers("/swagger-ui.html","/swagger-resources/**","/api-docs", "/v2/**").permitAll() 
                .antMatchers("/rest2/**").permitAll()
                .antMatchers("/monitoring").hasAnyAuthority(exploitant, chefProjetApplicatif, particulier, administrateur, rolls)
                .regexMatchers("\\A/accueil.ex.*").hasAnyAuthority(exploitant, chefProjetApplicatif, particulier, administrateur, rolls, notaires)
                .regexMatchers(FLOWS_PRIVE).hasAnyAuthority(exploitant, chefProjetApplicatif, administrateur, rolls)
                .regexMatchers(FLOWS_PRIVE2).hasAnyAuthority(exploitant, chefProjetApplicatif, particulier, rolls)
                .regexMatchers(FLOWS_PUBLIC).hasAnyAuthority(exploitant, chefProjetApplicatif, particulier, administrateur, rolls)      

                // TODO : à supprimer ?
                //  .regexMatchers(".*/rest/greeting.*")
                //      .hasAnyAuthority(
                //          Authorite.EXPLOITANT.toString(),
                //          Authorite.CHEFDEPROJETAPPLICATIF.toString(),
                //          Authorite.PARTICULIER.toString(),
                //          Authorite.ADMINISTRATEUR.toString(),
                //          Authorite.ROLLS.toString())
                
                //Permet d'empêcher par défaut l'exécution de flux non déclarés 
                .regexMatchers(".*/flux.ex?_flowId=.*").hasAuthority(roleQuiNexistePas)
                .regexMatchers(".*/flux.ex.*").authenticated()
                // Toutes les autres requêtes
                .anyRequest().denyAll();
        // @formatter:on

        // configurations supplémentaires (cumulatives) dans des implémentations de ServiceWebSecurityConfigurer
        // (ex: LombokCorsConfiguration)
        if (additionalWebSecurityConfigurer != null)
        {
            for (ServiceWebSecurityConfigurer config : additionalWebSecurityConfigurer)
            {
                config.configure(http);
            }
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        auth.authenticationProvider(authenticationAucunLdapProvider());
        auth.authenticationProvider(authentificationRealProvider());
        auth.authenticationProvider(authenticationLdapProvider());
    }
}
