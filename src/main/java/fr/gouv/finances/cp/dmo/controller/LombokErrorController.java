/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * <pre>
 *  Classe gérant les codes retour HTTP. Remplace la gestion faite par TOMCAT.
 *  Pour qu'elle fonctionne en mode TOMCAT STANDALONE, il faut créer un paragraphe <error-page> pointant vers /lombok-status-errors
 *  dans le fichier web.xml de l'application.
 *  Pour lombok c'est fait dans le fichier web-fragment du module lombo-webfragment.
 * </pre>
 */
@Controller
@ApiModel(description = "Service gérant les codes retour HTTP")
public class LombokErrorController implements ErrorController
{
    private static final String PATH = "/error";

    /**
     * Méthode appelée pour la résolution des codes erreurs HTTP. Il faut que la ressource (/errors/erreurstd) soit
     * atteignable par spring et qu'elle puisse être rendue (c'est une jsp).
     *
     * @param httpRequest
     * @return model and view
     */
    // @RequestMapping(value = "lombok-status-errors", method = RequestMethod.GET)
    @ApiOperation(value = "Service gérant les codes retour HTTP", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Succès"),
            @ApiResponse(code = 401, message = "Accès non autorisé à cette ressource"),
            @ApiResponse(code = 403, message = "Accès interdit à la ressource"),
            @ApiResponse(code = 404, message = "Ressource non trouvée")
    })
    @RequestMapping(value="/error", method=RequestMethod.GET)
    public ModelAndView renderErrorPage(HttpServletRequest httpRequest)
    {

        ModelAndView errorPage = new ModelAndView("errors/erreurstd");
        String errorMsg = "";
        int httpErrorCode = getErrorCode(httpRequest);

        switch (httpErrorCode)
        {
            case 400:
            {
                errorMsg = "Http Error Code: 400. Bad Request";
                break;
            }
            case 401:
            {
                errorMsg = "Http Error Code: 401. Unauthorized";
                break;
            }
            case 404:
            {
                errorMsg = "Http Error Code: 404. Resource not found";
                break;
            }
            case 500:
            {
                errorMsg = "Http Error Code: 500. Internal Server Error";
                break;
            }
        }
        errorPage.addObject("exceptionmessage", errorMsg);
        return errorPage;
    }

    private int getErrorCode(HttpServletRequest httpRequest)
    {
        return (Integer) httpRequest.getAttribute("javax.servlet.error.status_code");
    }

    @Override
    public String getErrorPath()
    {
        return PATH;
    }

}
