package fr.gouv.finances.cp.dmo.dao;

import java.util.List;

import fr.gouv.finances.cp.dmo.entite.CentreImpot;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa;

/**
 * DAO pour la gestion des contres des impôts
 * @author celinio fernandes
 * Date: Mar 9, 2020
 */
public interface ICentreImpotDao extends BaseDaoJpa
{
        
    public void add(CentreImpot centreImpot) ;
    
    public List<CentreImpot> findAll() ;
}

