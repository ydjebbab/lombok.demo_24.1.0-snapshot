/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : SelectionAvisImpositionRattacheFormAction.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.util.ArrayList;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.service.IContribuableService;

/**
 * Action SelectionAvisImpositionRattacheFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class SelectionAvisImpositionRattacheFormAction extends FormAction
{
    private static final Logger log = LoggerFactory.getLogger(SelectionAvisImpositionRattacheFormAction.class);

    /** contribuableserviceso. */
    private IContribuableService contribuableserviceso;

    /**
     * Constructeur
     */
    public SelectionAvisImpositionRattacheFormAction()
    {
        super();
    }

    /**
     * Constructeur
     * 
     * @param arg0 param
     */
    public SelectionAvisImpositionRattacheFormAction(Class<?> arg0)
    {
        super(arg0);
    }

    /**
     * Gets the contribuableserviceso.
     *
     * @return the contribuableserviceso
     */
    public IContribuableService getContribuableserviceso()
    {
        return contribuableserviceso;
    }

    /**
     * Sets the contribuableserviceso.
     *
     * @param contribuableserviceso the new contribuableserviceso
     */
    public void setContribuableserviceso(IContribuableService contribuableserviceso)
    {
        this.contribuableserviceso = contribuableserviceso;
    }

    /**
     * methode Preparer rechercher avis imposition
     * 
     * @param rc param
     * @return event
     */
    public Event preparerRechercherAvisImposition(RequestContext rc)
    {
        log.debug(">>> Debut methode preparerRechercherAvisImposition");
        ContribuablePar contribuable = (ContribuablePar) rc.getFlowScope().get("contribuablepar");
        // A supprimer quand les avis d'imposition seront rattachés
        // directement au contribuable en début de flux paraent
        // ContribuablePar
        // contribuable=contribuableserviceso.rechercherUnContribuableParticulieAvecAvisImpositionEtContratMensu(uncontribuable.getIdentifiant());
        // ***********
        Set listeavisimposition = contribuable.getListeAvisImposition();

        // TODO récupérer pour affichage également le libellé de type
        // impot de l'avis d'imposition (lien a créer dans entre les
        // beans
        if (contribuable.getListeAvisImposition() != null)
        {
            rc.getRequestScope().put("listeavisimposition", new ArrayList(listeavisimposition));
            return success();
        }
        else
        {
            return error();
        }
    }

}
