/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementTestRequetesAdressesPersonnesImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.batch;

import java.util.List;
import java.util.Set;

import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.entite.AdresseTest;
import fr.gouv.finances.cp.dmo.entite.PersonneTest;
import fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;

/**
 * Batch permettant de tester les mises à jour pour une relation bidirectionnelle n-n de chaque côté de la relation avec
 * un des côtés étant indiqué "inverse".
 * 
 * @author chouard-cp
 * @author CF: migration vers JPA
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Service("traitementtestrequetesadressespersonnes")
@Profile("batch")
@Lazy(true)
public class TraitementTestRequetesAdressesPersonnesImpl extends ServiceBatchCommunImpl
{
    @Autowired
    private ITestAdressesPersonnesService testAdressesPersonnesService;

    public TraitementTestRequetesAdressesPersonnesImpl()
    {
        super();
    }

    @Value("${traitementtestrequetesadressespersonnes.nombatch}")
    @Override
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }

    @Value("${traitementtestchargeedition.versionbatch}")
    @Override
    public void setVersionBatch(String versionBatch)
    {
        super.setVersionBatch(versionBatch);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        String nom = "Fer";
        String prenom = "Cel";
        String voie1 = "VOIE1";
        String voie2 = "VOIE2";
        String ville1 = "VILLE1";
        String ville2 = "VILLE2";

        // Chargement d'une personne et d'une adresse et test de l'ajout personne -> adresse
        PersonneTest personne = testAdressesPersonnesService.rechercherPersonneEtAdressesParNom(nom, prenom);
        AdresseTest adresse1 = testAdressesPersonnesService.rechercherAdresseEtPersonnesParVoieEtVille(voie1, ville1);
        testAdressesPersonnesService.ajouterAdresseALaPersonne(personne, adresse1);

        // Chargement d'une seconde adresse et test de l'ajout adresse -> personne
        AdresseTest adresse2 = testAdressesPersonnesService.rechercherAdresseEtPersonnesParVoieEtVille(voie2, ville2);
        testAdressesPersonnesService.ajouterPersonneALAdresse(adresse2, personne);

        // Rechargement de la personne et vérification des ajouts précédents
        personne = testAdressesPersonnesService.rechercherPersonneEtAdressesParNom(nom, prenom);
        if (personne.isAdresseRattachee(adresse1))
        {
            log.info("L'ajout personne -> adresse fonctionne correctement.");
        }
        else
        {
            log.warn("L'ajout personne -> adresse ne fonctionne pas !");
        }
        if (personne.isAdresseRattachee(adresse2))
        {
            log.info("L'ajout adresse -> personne fonctionne correctement.");
        }
        else
        {
            log.warn("L'ajout adresse -> personne ne fonctionne pas !");
        }

        // On recherche les personnes ayant une adresse dans une ville donnée
        // puis on affiche les personnes et les adresses correspondantes
        rechercherPersonnesEtAdressesParVille(ville1);
    }

    private void rechercherPersonnesEtAdressesParVille(String ville)
    {
        List<PersonneTest> listePersonneTest =
            testAdressesPersonnesService.rechercherPersonnesEtAdressesParVille(ville);
        try
        {
            if (null != listePersonneTest)
            {
                for (PersonneTest personneTest : listePersonneTest)
                {
                    log.info(personneTest.getPrenom() + " " + personneTest.getNom());
                    Set<AdresseTest> adresseTestSet = personneTest.getListeAdresses();
                    for (AdresseTest adresseTest : adresseTestSet)
                        log.info(adresseTest.getVoie() + " " + adresseTest.getVille());
                }
            }
        }
        catch (LazyInitializationException lazyExc)
        {
            log.error("Les adresses ne sont pas chargées !", lazyExc);
        }
    }

}
