package fr.gouv.finances.cp.dmo.config;

import java.util.Arrays;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import fr.gouv.finances.lombok.config.ServiceWebSecurityConfigurer;

/**
 * <pre>
 * Classe de configuration de Spring CORS. 
 * L'activation de CORS avec cette configuration se fait à travers la propriété spring.security.cors.enabled   
 * </pre>
 */

@Configuration
@ConditionalOnProperty(value="spring.security.cors.enabled", havingValue = "true")
public class LombokCorsConfiguration implements ServiceWebSecurityConfigurer
{
    @Override
    public void configure(HttpSecurity http) throws Exception
    {
        http.cors();
    }
    
    @Bean
    public CorsConfigurationSource corsConfigurationSource() 
    {
        final CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowedOrigins(Arrays.asList("http://localhost:8081"));
        corsConfiguration.setAllowedMethods(Arrays.asList("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        corsConfiguration.setAllowCredentials(true);
        corsConfiguration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type", "x-xsrf-token"));
        // URLs pour lesquelles la configuration CORS s'applique
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", corsConfiguration);
        return source;
    }
}
