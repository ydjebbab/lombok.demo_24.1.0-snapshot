/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : DocumentJointDaoImpl.java
 *
 */

package fr.gouv.finances.cp.dmo.dao.impl;

import java.util.Collection;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IDocumentJointDao;
import fr.gouv.finances.cp.dmo.entite.DocumentJoint;
import fr.gouv.finances.cp.dmo.entite.DocumentJoint_;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;

/**
 * DAO DocumentJointDaoImpl
 * 
 * @author chouard-cp
 * @author CF : passage vers JPA
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Repository("documentJointDao")
@Transactional(transactionManager="transactionManager")
public class DocumentJointDaoImpl extends BaseDaoJpaImpl implements IDocumentJointDao
{
    private static final Logger log = LoggerFactory.getLogger(DocumentJointDaoImpl.class);

    public DocumentJointDaoImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param documentsASupprimer
     * @see fr.gouv.finances.cp.dmo.dao.IDocumentJointDao#deleteCollectionDocumentsJoints(java.util.Collection)
     */
    @Override
    public void deleteCollectionDocumentsJoints(Collection documentsASupprimer)
    {
        log.debug(">>> Debut methode deleteCollectionDocumentsJoints");
        documentsASupprimer.forEach(this::deleteObject);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param id
     * @return document joint
     * @see fr.gouv.finances.cp.dmo.dao.IDocumentJointDao#findDocumentJointParId(java.lang.Long)
     */
    @Override
    public DocumentJoint findDocumentJointParId(Long id)
    {
        log.debug(">>> Debut methode findDocumentJointParId");
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DocumentJoint> criteriaQuery = criteriabuilder.createQuery(DocumentJoint.class);
        Root<DocumentJoint> documentJointRoot = criteriaQuery.from(DocumentJoint.class);
        documentJointRoot.fetch(DocumentJoint_.leFichierJoint, JoinType.INNER);

        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(documentJointRoot.get(DocumentJoint_.id), id));
        criteriaQuery.where(predicate);

        criteriaQuery.select(documentJointRoot);

        // Pour debug seulement
        TypedQuery<DocumentJoint> documentJointTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("documentJointTypedQuery : " + documentJointTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        DocumentJoint documentJointResultat = documentJointTypedQuery.getSingleResult();
        log.debug("documentJointResultat : " + documentJointResultat);

        return documentJointResultat;
    }
}
