/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */

package fr.gouv.finances.cp.dmo.ldap;

import java.util.Iterator;
import java.util.List;

import javax.naming.Name;
import javax.naming.directory.SearchControls;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class AbonneDaoImpl.
 */
@Component("abonneDao")
@ComponentScan(value = {"fr.gouv.finances.cp.dmo.ldap"})
@Transactional
public class AbonneDaoImpl implements AbonneDao
{
    static final SearchControls searchControls =
        new SearchControls(SearchControls.SUBTREE_SCOPE, 0, 100000, null, true, false);

    @Autowired
    private LdapTemplate ldapTemplate;

    public AbonneDaoImpl()
    {
        super();
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.dmo.ldap.AbonneDao#create(fr.gouv.finances.cp.dmo.ldap.Abonne)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.ldap.AbonneDao#create(fr.gouv.finances.cp.dmo.ldap.Abonne)
     */
    @Override
    public void create(Abonne abonne)
    {
        ldapTemplate.create(abonne);
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.dmo.ldap.AbonneDao#delete(fr.gouv.finances.cp.dmo.ldap.Abonne)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.ldap.AbonneDao#delete(fr.gouv.finances.cp.dmo.ldap.Abonne)
     */
    @Override
    public void delete(Abonne abonne)
    {
        ldapTemplate.delete(abonne);
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.dmo.ldap.AbonneDao#findAll()
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.ldap.AbonneDao#findAll()
     */
    @Override
    public List<Abonne> findAll()
    {
        return ldapTemplate.findAll(bindDN(), searchControls, Abonne.class);
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.dmo.ldap.AbonneDao#findByDn(javax.naming.Name)
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.ldap.AbonneDao#findByDn(javax.naming.Name)
     */
    @Override
    public Abonne findByDn(Name dnn)
    {
        return ldapTemplate.findByDn(dnn, Abonne.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.ldap.AbonneDao#update(fr.gouv.finances.cp.dmo.ldap.Abonne)
     */
    @Override
    public void update(Abonne abonne)
    {
        ldapTemplate.update(abonne);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.ldap.AbonneDao#findBySn(java.lang.String)
     */
    @Override
    public Abonne findBySn(String snn)
    {
        Abonne unAbonne = null;

        EqualsFilter filter = new EqualsFilter("sn", snn);
        List<Abonne> lesAbonnes = ldapTemplate.find(bindDN(), filter, searchControls, Abonne.class);
        Iterator iter = lesAbonnes.iterator();
        if (iter.hasNext())
        {
            unAbonne = (Abonne) (iter.next());
        }

        return unAbonne;
    }

    /**
     * Bind dn.
     *
     * @return the javax.naming. name
     */
    public static javax.naming.Name bindDN()
    {
        return LdapNameBuilder.newInstance().add("c", "fr").add("o", "gouv")
            .add("ou", "MEFI").add("ou", "abonnes-dgcp-xt")
            .add("ou", "divers-xt").build();
    }

    /**
     * Gets the ldap template.
     *
     * @return the ldap template
     */
    public LdapTemplate getLdapTemplate()
    {
        return ldapTemplate;
    }

    /**
     * Sets the ldap template.
     *
     * @param ldapTemplate the new ldap template
     */
    public void setLdapTemplate(LdapTemplate ldapTemplate)
    {
        this.ldapTemplate = ldapTemplate;
    }

}