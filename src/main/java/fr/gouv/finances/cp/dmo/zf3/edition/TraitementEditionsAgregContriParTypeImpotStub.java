/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import java.util.ArrayList;
import java.util.Collection;

import fr.gouv.finances.cp.dmo.techbean.StatistiquesContribuables;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunStubImpl;

/**
 * Class TraitementEditionsAgregContriParTypeImpotStub DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
public class TraitementEditionsAgregContriParTypeImpotStub extends AbstractServiceEditionCommunStubImpl
{
    /**
     * Constructeur
     */
    public TraitementEditionsAgregContriParTypeImpotStub()
    {
        super();
    }

    /**
     * methode Creates the bean collection : DGFiP.
     * 
     * @return collection
     */
    public static Collection<StatistiquesContribuables> createBeanCollection()
    {
        Collection<StatistiquesContribuables> list = new ArrayList<>();
        int iii = 1;

        list.add(new StatistiquesContribuables("utilisateur" + iii, Long.valueOf(iii), Long.valueOf(3 * (long) iii), Long
            .valueOf(iii), "Impôt sur le revenu", Double.valueOf(96 * (double) iii), Double.valueOf(27865946)));
        iii++;
        list.add(new StatistiquesContribuables("utilisateur" + iii, Long.valueOf(iii), Long.valueOf(3 * (long) iii), Long
            .valueOf(iii), "Taxe foncière", Double.valueOf(96 * (double) iii), Double.valueOf(20865946)));
        iii++;
        list.add(new StatistiquesContribuables("utilisateur" + iii, Long.valueOf(iii), Long.valueOf(3 * (long) iii), Long
            .valueOf(iii), "Taxe zéro", Double.valueOf(96 * (double) iii), Double.valueOf(10865946)));
        iii++;
        list.add(new StatistiquesContribuables("utilisateur" + iii, Long.valueOf(iii), Long.valueOf(3 * (long) iii), Long
            .valueOf(iii), "Taxe d'habitation", Double.valueOf(96 * (double) iii), Double.valueOf(30865946)));
        iii++;
        list.add(new StatistiquesContribuables("utilisateur" + iii, Long.valueOf(iii), Long.valueOf(3 * (long) iii), Long
            .valueOf(iii), "ISF", Double.valueOf(96 * (double) iii), Double.valueOf(40865946)));

        return list;
    }

}
