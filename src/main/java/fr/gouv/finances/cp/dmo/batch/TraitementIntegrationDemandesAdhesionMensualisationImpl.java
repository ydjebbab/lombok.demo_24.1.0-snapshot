/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 */
package fr.gouv.finances.cp.dmo.batch;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.Rib;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;

/**
 * Batch TraitementIntegrationDemandesAdhesionMensualisationImpl
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Service("traitementintegrationdemandesadhesionmensualisation")
@Profile("batch")
@Lazy(true)
public class TraitementIntegrationDemandesAdhesionMensualisationImpl extends ServiceBatchCommunImpl
{

    @Autowired()
    private IContribuableService contribuableService;

    private String urlfichieradhesions;

    public TraitementIntegrationDemandesAdhesionMensualisationImpl()
    {
        super();
    }

    public IContribuableService getContribuableService()
    {
        return contribuableService;
    }

    public String getUrlfichieradhesions()
    {
        return urlfichieradhesions;
    }

    public void setContribuableService(IContribuableService contribuableService)
    {
        this.contribuableService = contribuableService;
    }

    @Value("${traitementintegrationdemandesadhesionmensualisation.urlfichieradhesions}")
    public void setUrlfichieradhesions(String menurl)
    {
        this.urlfichieradhesions = menurl;
    }

    @Value("${traitementintegrationdemandesadhesionmensualisation.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch)
    {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementintegrationdemandesadhesionmensualisation.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        traiterFichierMen();
    }

    private void traiterFichierMen()
    {
        Pattern menpattern = Pattern.compile("(\\d*);(\\d*);(\\d*);(\\d*);(\\d*);(\\d*);(\\d*);([^\\r]*)", Pattern.MULTILINE);
        Matcher menmatcher;
        String filename;
        try
        {
            filename = new URL(urlfichieradhesions).getFile();
            try (FileInputStream fis = new FileInputStream(filename);
                FileChannel fchannel = fis.getChannel())
            {
                // Create a read-only CharBuffer on the file
                ByteBuffer bbuf = fchannel.map(FileChannel.MapMode.READ_ONLY, 0, (int) fchannel.size());
                CharBuffer cbuf = Charset.forName("8859_1").newDecoder().decode(bbuf);
                menmatcher = menpattern.matcher(cbuf);
                int nbArticleTraite = 0;
                int nbArticleErreur = 0;
                int nbTotalArticles = 0;
                List<TypeImpot> listeTypeImpot = null;
                while (menmatcher.find())
                {
                    nbTotalArticles += 1;
                    String refAvis = menmatcher.group(1);
                    ContribuablePar contribuable = contribuableService
                        .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParReferenceAvis(refAvis);
                    if (contribuable != null)
                    {
                        listeTypeImpot =
                            contribuableService.rechercherListeTypesImpotsDisponiblesALaMensualisationPourUnContribuable(contribuable);
                        if (listeTypeImpot != null && !listeTypeImpot.isEmpty())
                        {
                            ListIterator<TypeImpot> iter = listeTypeImpot.listIterator();
                            while (iter.hasNext())
                            {
                                TypeImpot element = iter.next();
                                if (element.getCodeImpot().compareTo(Long.decode(menmatcher.group(2))) == 0)
                                {
                                    // Contrat
                                    ContratMensu nouveauContrat = new ContratMensu();
                                    nouveauContrat.getTypeImpot().setCodeImpot(Long.decode(menmatcher.group(2)));
                                    nouveauContrat.setAnneePriseEffet(Long.valueOf(menmatcher.group(3)));
                                    // RIB
                                    Rib rib = new Rib();
                                    rib.setCodeBanque(menmatcher.group(4));
                                    rib.setCodeGuichet(menmatcher.group(5));
                                    rib.setNumeroCompte(menmatcher.group(6));
                                    rib.setCleRib(menmatcher.group(7));
                                    /*
                                     * TODO problème à résoudre avec le titulaire du compte
                                     * nouveauContrat.getRib().setNom(menmatcher.group(8));
                                     */
                                    nouveauContrat.setRib(rib);
                                    try
                                    {
                                        contribuableService.adhererAlaMensualisation(contribuable, element, nouveauContrat, refAvis,
                                            "batch");
                                        nbArticleTraite += 1;
                                    }
                                    catch (RuntimeException e)
                                    {
                                        log.error("Erreur pour la référence d'avis :" + refAvis);
                                        log.error("(Message de l'erreur:");
                                        log.error("", e);
                                        nbArticleErreur += 1;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        log.info("Aucun contribuable rattaché à l'avis : " + refAvis);
                        nbArticleErreur += 1;
                    }
                }
                log.info("Nombre d'articles traités  :" + nbArticleTraite);
                log.info("Nombre d'articles en erreur:" + nbArticleErreur);
                log.info("Nombre total d'articles    :" + nbTotalArticles);
            }
            catch (IOException ioExc)
            {
                log.warn("Impossible de fermer le fichier", ioExc);
                throw ApplicationExceptionTransformateur.transformer(ioExc);
            }
        }
        catch (MalformedURLException malURLExc)
        {
            log.error(malURLExc.getMessage(), malURLExc);
        }
    }

}
