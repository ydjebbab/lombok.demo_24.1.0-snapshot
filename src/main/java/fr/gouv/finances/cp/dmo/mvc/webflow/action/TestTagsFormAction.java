/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TestTagsFormAction.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.mvc.form.TestTagsForm;
import fr.gouv.finances.lombok.menu.service.MenuService;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomDateEditor;

/**
 * Action TestTagsFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
public class TestTagsFormAction extends FormAction
{
    /** menuserviceso. */
    public MenuService menuserviceso;

    /** list elements de la liste multiple. */
    private List<TypeImpot> listElementsDeLaListeMultiple = new ArrayList<TypeImpot>();

    /** hash set elements de la liste multiple. */
    private Set<TypeImpot> hashSetElementsDeLaListeMultiple = new LinkedHashSet<TypeImpot>();

    /** linked hash set elements de la liste multiple. */
    private Set<TypeImpot> linkedHashSetElementsDeLaListeMultiple = new LinkedHashSet<TypeImpot>();

    /** map elements de la liste multiple. */
    private Map<Long, String> mapElementsDeLaListeMultiple = new HashMap<Long, String>();

    /** linked hash map elements de la liste multiple. */
    private Map<Long, String> linkedHashMapElementsDeLaListeMultiple = new LinkedHashMap<Long, String>();

    /**
     * Instanciation de test tags form action.
     */
    public TestTagsFormAction()
    {
        super();
    }

    /**
     * Instanciation de test tags form action.
     * 
     * @param arg0 param
     */
    public TestTagsFormAction(Class<?> arg0)
    {
        super(arg0);
    }

    /**
     * Gets the menuserviceso.
     *
     * @return the menuserviceso
     */
    public MenuService getMenuserviceso()
    {
        return menuserviceso;
    }

    /**
     * Sets the menuserviceso.
     *
     * @param menuserviceso the new menuserviceso
     */
    public void setMenuserviceso(MenuService menuserviceso)
    {
        this.menuserviceso = menuserviceso;
    }

    /**
     * methode Preparer controles : DGFiP.
     * 
     * @param request param
     * @return event
     */
    public Event preparerControles(RequestContext request)
    {
        TestTagsForm form = (TestTagsForm) request.getFlowScope().get(getFormObjectName());
        request.getFlowScope().put("setSelected", null);
        form.setSetSelected(null);
        // List<TypeImpot> listElementsDeLaListeMultiple = new ArrayList<TypeImpot>();
        // Set<TypeImpot> hashSetElementsDeLaListeMultiple = new HashSet<TypeImpot>();
        // Set<TypeImpot> linkedHashSetElementsDeLaListeMultiple = new LinkedHashSet<TypeImpot>();
        // Map<Long, String> mapElementsDeLaListeMultiple = new HashMap<Long, String>();
        // Map<Long, String> linkedHashMapElementsDeLaListeMultiple = new LinkedHashMap<Long, String>();

        TypeImpot unTypeImpot;
        Long codeImpot = Long.valueOf(0);
        String nomImpot = "Aucune sélection";
        unTypeImpot = new TypeImpot();
        unTypeImpot.setCodeImpot(codeImpot);
        unTypeImpot.setNomImpot(nomImpot);
        hashSetElementsDeLaListeMultiple.add(unTypeImpot);

        String[] items =
            new String[] {"taxes foncières", "taxe habitation", "impot sur le revenu", "taxe professionnelle",
                    "taxe sur les ordures ménagères"};
        for (int i = 0; i < 5; i++)
        {
            codeImpot = Long.valueOf((long) i + 1);
            nomImpot = items[i];
            unTypeImpot = new TypeImpot();
            unTypeImpot.setCodeImpot(codeImpot);
            unTypeImpot.setNomImpot(nomImpot);

            listElementsDeLaListeMultiple.add(unTypeImpot);
            hashSetElementsDeLaListeMultiple.add(unTypeImpot);
            linkedHashSetElementsDeLaListeMultiple.add(unTypeImpot);
            mapElementsDeLaListeMultiple.put(codeImpot, nomImpot);
            linkedHashMapElementsDeLaListeMultiple.put(codeImpot, nomImpot);
        }

        // pour pas de selection

        if (request.getFlowScope().get("elecmentselctiones") != null)
        {
            request.getFlowScope().put("listelementsdelalistemultipleselectionnes", request.getFlowScope().get("elecmentselctiones"));
        }

        request.getFlowScope().put("listelementsdelalistemultiple", listElementsDeLaListeMultiple);
        request.getFlowScope().put("hashsetelementsdelalistemultiple", hashSetElementsDeLaListeMultiple);
        request.getFlowScope().put("linkedhashsetelementsdelalistemultiple", linkedHashSetElementsDeLaListeMultiple);
        request.getFlowScope().put("mapelementsdelalistemultiple", mapElementsDeLaListeMultiple);
        request.getFlowScope().put("linkedhashmapelementsdelalistemultiple", linkedHashMapElementsDeLaListeMultiple);

        form.setMultipleListFromArrayList(listElementsDeLaListeMultiple);
        form.setMultipleListFromHashSet(hashSetElementsDeLaListeMultiple);
        return success();
    }

    /**
     * methode Preparer controlesbis : .
     *
     * @param request DOCUMENTEZ_MOI
     * @return event
     */
    public Event preparerControlesbis(RequestContext request)
    {
        TestTagsForm form = (TestTagsForm) request.getFlowScope().get(getFormObjectName());

        if (request.getFlowScope().get("elecmentselctiones") != null)
        {
            List ListString = new ArrayList();
            if (request.getFlowScope().get("elecmentselctiones") != null)
            {
                Iterator iter = ((Set<TypeImpot>) request.getFlowScope().get("elecmentselctiones")).iterator();
                while (iter.hasNext())
                {
                    ListString.add(((TypeImpot) (iter.next())).getCodeImpot().toString());

                }
                form.setSetSelected(ListString);
                request.getFlowScope().put("setSelected", ListString);
            }
        }
        return success();
    }

    /**
     * methode Valid disabled : DGFiP.
     * 
     * @param request param
     * @return event
     */
    public Event validDisabled(RequestContext request)
    {
        request.getFlowScope().put("lectureseule", "true");
        request.getFlowScope().put("inactif", "true");
        request.getFlowScope().put("inactifquandlectureseule", "true");
        return success();
    }

    /**
     * methode Valid disabled readonly : DGFiP.
     * 
     * @param request param
     * @return event
     */
    public Event validDisabledReadonly(RequestContext request)
    {
        request.getFlowScope().put("lectureseule", "true");
        request.getFlowScope().put("inactif", "");
        request.getFlowScope().put("inactifquandlectureseule", "true");
        return success();
    }

    /**
     * methode Valid not disabled readonly : DGFiP.
     * 
     * @param request param
     * @return event
     */
    public Event validNotDisabledReadonly(RequestContext request)
    {
        request.getFlowScope().put("lectureseule", "true");
        request.getFlowScope().put("inactif", "");
        request.getFlowScope().put("inactifquandlectureseule", "false");
        return success();
    }

    /**
     * methode Valid readonly : DGFiP.
     * 
     * @param request param
     * @return event
     */
    public Event validReadonly(RequestContext request)
    {
        TestTagsForm form = (TestTagsForm) request.getFlowScope().get(getFormObjectName());

        form.getMultipleListFromArrayList();
        form.getMultipleListFromHashSet();
        form.setSetSelected(null);

        request.getFlowScope().put("elecmentselctiones", form.getMultipleListFromHashSet());
        request.getFlowScope().put("setSelected", null);

        form.setSetSelected(null);

        // request.getFlowScope().put("lectureseule",
        // "true");
        // request.getFlowScope().put("inactifquandlectureseule",
        // "true");
        return success();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param request
     * @param per
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext request, PropertyEditorRegistry per)
    {
        super.registerPropertyEditors(request, per);

        per.registerCustomEditor(Date.class, "dateField", new CpCustomDateEditor());
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.FRANCE);
        per.registerCustomEditor(Date.class, "dateHeureField", new CustomDateEditor(dateFormat, true));

        per.registerCustomEditor(List.class, "multipleListFromArrayList", new CustomCollectionEditor(List.class, false)
        {

            protected Object convertElement(Object element)
            {
                Integer name;
                name = (Integer.parseInt(element.toString()));
                TypeImpot unTypeImpot = listElementsDeLaListeMultiple.get(name);
                return unTypeImpot;
            }
        });

        per.registerCustomEditor(LinkedHashSet.class, "multipleListFromHashSet", new CustomCollectionEditor(HashSet.class, false)
        {

            protected Object convertElement(Object element)
            {
                Long name;
                TypeImpot unTypeImpot = null;
                name = (Long.valueOf(element.toString()));
                Iterator iter = hashSetElementsDeLaListeMultiple.iterator();
                while (iter.hasNext())
                {
                    unTypeImpot = (TypeImpot) (iter.next());
                    if (unTypeImpot.getCodeImpot().compareTo(name) == 0)
                    {
                        return unTypeImpot;
                    }
                }
                return unTypeImpot;

            }
        });

        // test intercepteur avant generation menu
        menuserviceso.calculerBarreDeMenusDynamique(request.getExternalContext().getContextPath(),
            TestTagsFormAction.class.getName());
    }

}
