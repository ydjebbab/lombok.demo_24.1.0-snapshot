/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
 */

package fr.gouv.finances.cp.dmo.entite;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import fr.gouv.finances.lombok.apptags.util.format.BooleanFormat.TypeFormat;
import fr.gouv.finances.lombok.apptags.util.format.FormaterBoolean;
import fr.gouv.finances.lombok.jpa.util.BaseEntity;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Entité TypeImpot.
 *
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "TYPEIMPOT_TYIM")
@SequenceGenerator(name = "TYPEIMPOT_TYIM_ID_GENERATOR", sequenceName = "SEQ_TYPEIMPOT_TYIM", allocationSize = 1)
public class TypeImpot extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @XmlAttribute
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TYPEIMPOT_TYIM_ID_GENERATOR")
    private Long id;

    @XmlElement(name = "codeimpot")
    @Column(name = "codeImpot")
    private Long codeImpot;

    @XmlElement(name = "nomimpot")
    @Column(nullable = false, name = "NOMIMPOT")
    private String nomImpot;

    @XmlElement(name = "estouvertalamensualisation")
    @Column(nullable = true, name = "ESTOUVERTALAMENSUALISATION")
    private Boolean estOuvertALaMensualisation;

    @XmlElement(name = "estdestineauxparticuliers")
    @Column(nullable = true, name = "ESTDESTINEAUXPARTICULIERS")
    private Boolean estDestineAuxParticuliers;

    @XmlElement(name = "jourouvertureadhesion")
    @Column(nullable = true, name = "JOUROUVERTUREADHESION")
    private Date jourOuvertureAdhesion;

    @XmlElement(name = "jourlimiteadhesion")
    @Column(nullable = true, name = "JOURLIMITEADHESION")
    private Date jourLimiteAdhesion;

    @XmlElement(name = "taux")
    @Column(name = "TAUX")
    private BigDecimal taux;

    @XmlElement(name = "seuil")
    @Column(name = "SEUIL")
    private BigDecimal seuil;

    //@XmlElement(name = "version")
    //@Version
    //@Column(name = "VERSION")
    //private int version;

    public TypeImpot()
    {
        super();
    }

    public TypeImpot(Long code)
    {
        super();
        this.codeImpot = code;
    }

    // rajout de 2 attributs qui seront affichés uniquement dans le jrxml
    // afin de ne plus afficher true ou false mais le format requis
    // pas de possibilité de pattern pour ce type dans le jrxml
    public TypeImpot(Long codeimpot, Boolean estdestineauxparticuliers, Boolean estouvertalamensualisation, Long id,
        Date jourlimiteadhesion, Date jourouvertureadhesion, String nomimpot, BigDecimal seuil,
        BigDecimal taux)
    {
        super();
        this.codeImpot = codeimpot;
        this.estDestineAuxParticuliers = estdestineauxparticuliers;
        this.estOuvertALaMensualisation = estouvertalamensualisation;
        this.id = id;
        this.jourLimiteAdhesion = jourlimiteadhesion;
        this.jourOuvertureAdhesion = jourouvertureadhesion;
        this.nomImpot = nomimpot;
        this.seuil = seuil;
        this.taux = taux;
    }

    public TypeImpot(Long code, String nom, Boolean estmensualisable)
    {
        super();
        this.codeImpot = code;
        this.nomImpot = nom;
        this.estOuvertALaMensualisation = estmensualisable;
    }

    public Long getCodeImpot()
    {
        return codeImpot;
    }

    public Boolean getEstDestineAuxParticuliers()
    {
        return estDestineAuxParticuliers;
    }

    public String getEstDestineAuxParticuliersString()
    {
        return FormaterBoolean.getFormat(TypeFormat.OUI_NON).format(getEstDestineAuxParticuliers());
    }

    public Boolean getEstOuvertALaMensualisation()
    {
        return estOuvertALaMensualisation;
    }

    public String getEstOuvertALaMensualisationString()
    {
        return FormaterBoolean.getFormat(TypeFormat.VRAI_FAUX).format(getEstOuvertALaMensualisation());
    }

    public Long getId()
    {
        return id;
    }

    public Date getJourLimiteAdhesion()
    {
        return jourLimiteAdhesion;
    }

    public Date getJourOuvertureAdhesion()
    {
        return jourOuvertureAdhesion;
    }

    public String getNomImpot()
    {
        return nomImpot;
    }

    public BigDecimal getSeuil()
    {
        return seuil;
    }

    public BigDecimal getTaux()
    {
        return taux;
    }

    public void setCodeImpot(Long code)
    {
        this.codeImpot = code;
    }

    public void setEstDestineAuxParticuliers(Boolean estDestineAuxParticuliers)
    {
        this.estDestineAuxParticuliers = estDestineAuxParticuliers;
    }

    public void setEstDestineAuxParticuliersString(String estDestineAuxParticuliersString)
    {
        // RAS
    }

    public void setEstOuvertALaMensualisation(Boolean estOuvertALaMensualisation)
    {
        this.estOuvertALaMensualisation = estOuvertALaMensualisation;
    }

    public void setEstOuvertALaMensualisationString(String estOuvertALaMensualisationString)
    {
        // RAS
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setJourLimiteAdhesion(Date jourlimiteadhesion)
    {
        this.jourLimiteAdhesion = jourlimiteadhesion;
    }

    public void setJourOuvertureAdhesion(Date jourouvertureadhesion)
    {
        this.jourOuvertureAdhesion = jourouvertureadhesion;
    }

    public void setNomImpot(String nom)
    {
        this.nomImpot = nom;
    }

    public void setSeuil(BigDecimal seuil)
    {
        this.seuil = seuil;
    }

    public void setTaux(BigDecimal taux)
    {
        this.taux = taux;
    }

    /**
     * methode Verifier est destine auxparticuliers r g11 : DGFiP.
     */
    public void verifierEstDestineAuxparticuliersRG11()
    {
        // deuxième règle de gestion simpliste : toujours pour
        // l'illustration

        if (!this.getEstDestineAuxParticuliers().booleanValue())
        {
            throw new RegleGestionException(Messages.getString("exception.typeimpotnondestineauxparticuliers"));
        }
    }

    /**
     * methode Verifier est ouvert a la mensualisation r g10 : DGFiP.
     */
    public void verifierEstOuvertALaMensualisationRG10()
    {
        // règle de gestion simpliste : uniquement pour l'illustration

        if (!this.getEstOuvertALaMensualisation().booleanValue())
        {
            throw new RegleGestionException(Messages.getString("exception.typeimpotnonouvertalamensualisation"));
        }
    }

    /**
     * Equals.
     *
     * @param object param
     * @return true, if equals
     * @see java.lang.Object#equals(Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (object == null || (object.getClass() != this.getClass()))
        {
            return false;
        }
        TypeImpot myObj = (TypeImpot) object;
        return ((codeImpot == null ? myObj.getCodeImpot() == null : codeImpot.equals(myObj.getCodeImpot())));
    }

    /**
     * Hash code.
     *
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int hash = hashCODEHASHINIT;
        int varCode = 0;

        varCode = (null == codeImpot ? 0 : codeImpot.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        return hash;
    }
}
