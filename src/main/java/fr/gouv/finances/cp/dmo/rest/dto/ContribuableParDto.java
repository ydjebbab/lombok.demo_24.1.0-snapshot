package fr.gouv.finances.cp.dmo.rest.dto;

import java.io.Serializable;

/**
 * DTO contribuable
 * @author celinio fernandes
 * Date: Mar 5, 2020
 */
public class ContribuableParDto implements Serializable
{
    private static final long serialVersionUID = 14564343L;

    public ContribuableParDto()
    {
        super();
    }


    private String adresseMail;


    private String nom;


    private String prenom;


    public String getAdresseMail()
    {
        return adresseMail;
    }


    public void setAdresseMail(String adresseMail)
    {
        this.adresseMail = adresseMail;
    }


    public String getNom()
    {
        return nom;
    }


    public void setNom(String nom)
    {
        this.nom = nom;
    }


    public String getPrenom()
    {
        return prenom;
    }


    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    

}
