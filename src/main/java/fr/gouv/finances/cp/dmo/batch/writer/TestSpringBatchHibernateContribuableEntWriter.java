/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.batch.writer;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import fr.gouv.finances.cp.dmo.dao.IContribuableDao;
import fr.gouv.finances.cp.dmo.entite.ContribuableEnt;

/**
 * Class TestSpringBatchHibernateContribuableEntWriter
 */
public class TestSpringBatchHibernateContribuableEntWriter implements ItemWriter<ContribuableEnt>, InitializingBean
{
    private IContribuableDao contribuabledao;

    public TestSpringBatchHibernateContribuableEntWriter()
    {
        super();
    }

    /**
     * (Repris de l'exemple
     * org.springframework.batch.sample.domain.trade.internal.HibernateAwareCustomerCreditItemWriter Delegates writing
     * to a custom DAO and flushes + clears hibernate session to fulfill the {@link ItemWriter} contract.
     *
     * @param items 
     * @throws Exception the exception
     */

    @Override
    public void write(List<? extends ContribuableEnt> items) throws Exception
    {
        for (ContribuableEnt contribuableent : items)
        {
            contribuabledao.saveObject(contribuableent);
        }

        // Nécessaire pour remplir le contrat de "ItemWriter"
        // (Repris de l'exemple
        // org.springframework.batch.sample.domain.trade.internal.HibernateAwareCustomerCreditItemWriter)
        contribuabledao.flush();
        contribuabledao.clearPersistenceContext();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
     */
    @Override
    public void afterPropertiesSet() throws Exception
    {
        Assert.notNull(contribuabledao, "ContribuableDao doit être initialisé");
    }

    /**
     * Accesseur de l attribut contribuabledao.
     *
     * @return contribuabledao
     */
    public IContribuableDao getContribuabledao()
    {
        return contribuabledao;
    }

    /**
     * Modificateur de l attribut contribuabledao.
     *
     * @param contribuabledao le nouveau contribuabledao
     */
    public void setContribuabledao(IContribuableDao contribuabledao)
    {
        this.contribuabledao = contribuabledao;
    }

}
