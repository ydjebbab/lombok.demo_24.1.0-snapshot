package fr.gouv.finances.cp.dmo.entite;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ContribuableEnt.class)
public abstract class ContribuableEnt_ extends fr.gouv.finances.cp.dmo.entite.Contribuable_ {

	public static volatile SingularAttribute<ContribuableEnt, Double> solde;
	public static volatile SingularAttribute<ContribuableEnt, String> telephoneFixe;
	public static volatile SingularAttribute<ContribuableEnt, String> nom;

}

