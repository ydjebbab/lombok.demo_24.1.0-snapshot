package fr.gouv.finances.cp.dmo.entite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;

/**
 * 
 * @author celfer
 * Date: 24 déc. 2019
 */
@Entity
@Table(name = "REVENU_MOBILIER")
@SequenceGenerator(name = "REVENU_MOBILIER_ID_GENERATOR", sequenceName = "SEQ_REVENU_MOBILIER", allocationSize = 1)
public class RevenuMobilier extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REVENU_MOBILIER_ID_GENERATOR")
    @Column(name = "id")
    private Long id;

    // Plan d'épargne logement
    @Column(nullable = true, name = "MONTANTPEL")
    private int montantPEL;

    // Compte épargne logement
    @Column(nullable = true, name = "MONTANTCEL")
    private int montantCEL;

    // Plan d'épargne en actions
    @Column(nullable = true, name = "MONTANTPEA")
    private int montantPEA;

    // Plan d'épargne entreprise
    @Column(nullable = true, name = "MONTANTPEE")
    private int montantPEE;

    @Column(nullable = true, name = "MONTANTASSURANCEVIE")
    private int montantAssuranceVie;

    @Column(nullable = true, name = "MONTANTLIVRETA")
    private int montantLivretA;

    @Column(nullable = true, name = "MONTANTACTIONS")
    private int montantActions;
    
    public RevenuMobilier()
    {
        super();
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public int getMontantPEL()
    {
        return montantPEL;
    }

    public void setMontantPEL(int montantPEL)
    {
        this.montantPEL = montantPEL;
    }

    public int getMontantCEL()
    {
        return montantCEL;
    }

    public void setMontantCEL(int montantCEL)
    {
        this.montantCEL = montantCEL;
    }

    public int getMontantPEA()
    {
        return montantPEA;
    }

    public void setMontantPEA(int montantPEA)
    {
        this.montantPEA = montantPEA;
    }

    public int getMontantPEE()
    {
        return montantPEE;
    }

    public void setMontantPEE(int montantPEE)
    {
        this.montantPEE = montantPEE;
    }

    public int getMontantAssuranceVie()
    {
        return montantAssuranceVie;
    }

    public void setMontantAssuranceVie(int montantAssuranceVie)
    {
        this.montantAssuranceVie = montantAssuranceVie;
    }

    public int getMontantLivretA()
    {
        return montantLivretA;
    }

    public void setMontantLivretA(int montantLivretA)
    {
        this.montantLivretA = montantLivretA;
    }

    public int getMontantActions()
    {
        return montantActions;
    }

    public void setMontantActions(int montantActions)
    {
        this.montantActions = montantActions;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + montantActions;
        result = prime * result + montantAssuranceVie;
        result = prime * result + montantCEL;
        result = prime * result + montantLivretA;
        result = prime * result + montantPEA;
        result = prime * result + montantPEE;
        result = prime * result + montantPEL;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RevenuMobilier other = (RevenuMobilier) obj;
        if (id == null)
        {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        if (montantActions != other.montantActions)
            return false;
        if (montantAssuranceVie != other.montantAssuranceVie)
            return false;
        if (montantCEL != other.montantCEL)
            return false;
        if (montantLivretA != other.montantLivretA)
            return false;
        if (montantPEA != other.montantPEA)
            return false;
        if (montantPEE != other.montantPEE)
            return false;
        if (montantPEL != other.montantPEL)
            return false;
        return true;
    }
}
