/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TestPopulationDao.java
 *
 */
package fr.gouv.finances.cp.dmo.dao;

import java.util.List;

import fr.gouv.finances.cp.dmo.entite.VilleTest;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa;

/**
 * Interface TestPopulationDao
 * 
 * @author chouard-cp
 * @author CF: migration vers JPA
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public interface ITestPopulationDao extends BaseDaoJpa
{

    /**
     * methode Find villes par origine
     * 
     * @param origineId 
     * @return list< ville test>
     */
    List<VilleTest> findVillesParOrigine(Long origineId);
}
