/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : AdresseTest.java
 *
 */
package fr.gouv.finances.cp.dmo.entite;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Entité AdresseTest
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Entity
@Table(name = "ADRESSETEST_ADRT")
@SequenceGenerator(name = "ADRESSETEST_ADRT_ID_GENERATOR", sequenceName = "SEQ_ADRESSETEST_ADRT", allocationSize = 1)
public class AdresseTest extends BaseEntity
{
    private static final long serialVersionUID = 9020390286450038707L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ADRESSETEST_ADRT_ID_GENERATOR")
    @Column(name = "ADRESSE_ID")
    private Long id;

    @Column(name = "VOIE")
    private String voie;

    @Column(name = "VILLE")
    private String ville;

    @ManyToMany
    @JoinTable(name = "PERSONNEADRESSETEST_PADT", joinColumns = @JoinColumn(name = "ADRESSE_ID"), inverseJoinColumns = @JoinColumn(name = "PERSONNE_ID"))
    private Set<PersonneTest> listePersonnes = new HashSet<PersonneTest>();

    public AdresseTest()
    {
        super();
    }

    public AdresseTest(String voie, String ville)
    {
        super();
        this.voie = voie;
        this.ville = ville;
    }

    /**
     * methode Detacher une personne : DGFiP.
     * 
     * @param personne param
     */
    public void detacherUnePersonne(PersonneTest personne)
    {
        personne.getListeAdresses().remove(this);
        getListePersonnes().remove(personne);
    }

    public Long getId()
    {
        return id;
    }

    public Set<PersonneTest> getListePersonnes()
    {
        return listePersonnes;
    }

    public String getVille()
    {
        return ville;
    }

    public String getVoie()
    {
        return voie;
    }

    /**
     * methode Rattacher une personne : DGFiP.
     * 
     * @param personne param
     */
    public void rattacherUnePersonne(PersonneTest personne)
    {
        if (personne != null)
        {
            personne.getListeAdresses().add(this);
            getListePersonnes().add(personne);
        }
        else
        {
            throw new ProgrammationException("erreur.programmation");
        }
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setListePersonnes(Set<PersonneTest> listePersonnes)
    {
        this.listePersonnes = listePersonnes;
    }

    public void setVille(String ville)
    {
        this.ville = ville;
    }

    public void setVoie(String voie)
    {
        this.voie = voie;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param obj
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final AdresseTest other = (AdresseTest) obj;
        if (ville == null)
        {
            if (other.ville != null)
            {
                return false;
            }
        }
        else if (!ville.equals(other.ville))
        {
            return false;
        }
        if (voie == null)
        {
            if (other.voie != null)
            {
                return false;
            }
        }
        else if (!voie.equals(other.voie))
        {
            return false;
        }
        return true;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = hashCODEHASHMULT;
        int result = 1;

        result = prime * result + ((ville == null) ? 0 : ville.hashCode());
        result = prime * result + ((voie == null) ? 0 : voie.hashCode());
        return result;
    }

}
