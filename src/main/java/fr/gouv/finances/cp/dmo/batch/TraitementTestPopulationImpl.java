/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementTestPopulationImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.batch;

import java.util.List;

import org.hibernate.LazyInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.entite.HabitantTest;
import fr.gouv.finances.cp.dmo.entite.VilleTest;
import fr.gouv.finances.cp.dmo.service.ITestPopulationService;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;

/**
 * Batch TraitementTestPopulationImpl.
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Service("traitementtestpopulation")
@Profile("batch")
@Lazy(true)
public class TraitementTestPopulationImpl extends ServiceBatchCommunImpl
{

    /**
     * testpopulationserviceso.
     */
    @Autowired()
    @Qualifier("testpopulationserviceso")
    private ITestPopulationService testpopulationserviceso;

    /**
     * Constructeur
     */
    public TraitementTestPopulationImpl()
    {
        super();
    }

    @Value("${traitementtestpopulation.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch)
    {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementtestpopulation.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }

    /**
     * Accesseur de l attribut testpopulationserviceso.
     *
     * @return testpopulationserviceso
     */
    public ITestPopulationService getTestpopulationserviceso()
    {
        return testpopulationserviceso;
    }

    /**
     * Modificateur de l attribut testpopulationserviceso.
     *
     * @param testpopulationserviceso le nouveau testpopulationserviceso
     */
    public void setTestpopulationserviceso(ITestPopulationService testpopulationserviceso)
    {
        this.testpopulationserviceso = testpopulationserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        Long origineId = Long.valueOf(1); // France

        List<VilleTest> listeVilles = testpopulationserviceso.rechercherVillesParOrigine(origineId);
        for (VilleTest ville : listeVilles)
        {
            log.info(ville.getLibelle());
            try
            {
                for (HabitantTest habitant : ville.getListeHabitants())
                {
                    log.info(habitant.getPrenom() + " " + habitant.getNom());
                }
            }
            catch (LazyInitializationException lazyExc)
            {
                log.info("Les habitants n'ont pas été chargés, la collection est lazy.", lazyExc);
            }
        }
    }

}
