/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.editionbean;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe pour les montants avec 2 décimales, identique à celle de Forte.
 * 
 * @author jldebon-cp Date: 4 févr. 2011
 */
public class DgcpMontant extends BigDecimal
{
    private static final Logger log = LoggerFactory.getLogger(DgcpMontant.class);

    public static final DgcpMontant DGCP_MT_ZERO = DgcpMontant.valueOf(BigDecimal.ZERO);

    public static final DgcpMontant DGCP_MT_UN = DgcpMontant.valueOf(BigDecimal.ONE);

    public static final DgcpMontant DGCP_MT_CENT = new DgcpMontant("100");

    public static final DgcpMontant DGCP_MT_MAXI = new DgcpMontant("999999999999");

    private static final long serialVersionUID = 1L;

    public DgcpMontant(char[] val)
    {
        super(val);
    }

    public DgcpMontant(String val)
    {
        super(val);
    }

    public DgcpMontant(double val)
    {
        super(val);
    }

    public DgcpMontant(BigInteger val)
    {
        super(val);
    }

    public DgcpMontant(int val)
    {
        super(val);
    }

    public DgcpMontant(long val)
    {
        super(val);
    }

    public DgcpMontant(char[] val, MathContext mctx)
    {
        super(val, mctx);
    }

    public DgcpMontant(String val, MathContext mctx)
    {
        super(val, mctx);
    }

    public DgcpMontant(double val, MathContext mctx)
    {
        super(val, mctx);
    }

    public DgcpMontant(BigInteger val, MathContext mctx)
    {
        super(val, mctx);
    }

    public DgcpMontant(BigInteger unscaledVal, int scale)
    {
        super(unscaledVal, scale);
    }

    public DgcpMontant(int val, MathContext mctx)
    {
        super(val, mctx);
    }

    public DgcpMontant(long val, MathContext mctx)
    {
        super(val, mctx);
    }

    public DgcpMontant(char[] val, int offset, int len)
    {
        super(val, offset, len);
    }

    public DgcpMontant(BigInteger unscaledVal, int scale, MathContext mctx)
    {
        super(unscaledVal, scale, mctx);
    }

    public DgcpMontant(char[] val, int offset, int len, MathContext mctx)
    {
        super(val, offset, len, mctx);
    }

    /**
     * methode valueOf : Convertit un BigDecimal en DgcpMontant et retourne une nouvelle instance.
     * 
     * @param val BigDecimal à convertir
     * @return DgcpMontant
     */
    public static DgcpMontant valueOf(BigDecimal val)
    {
        if (val == null)
        {
            throw new IllegalArgumentException("Anomalie conversion en DgcpMontant");
        }
        else
        {
            return new DgcpMontant(val.toString());
        }
    }

    /**
     * methode add : Appelle la méthode add de BigDecimal et retourne un nouveau DgcpMontant.
     * 
     * @param augend value to be added to this <tt>DgcpMontant</tt>.
     * @return DgcpMontant = <tt>this + augend</tt>
     */
    @Override
    public DgcpMontant add(BigDecimal augend)
    {
        return DgcpMontant.valueOf(super.add(augend));
    }

    /**
     * methode divide : Appelle la méthode divide de BigDecimal et retourne un nouveau DgcpMontant. Si le résultat de la
     * division est non fini -> provoque ArithmeticException dans ce cas, le résultat est tronqué et arrondi à 10
     * décimales qd scale courant < 10 ou tronqué et arrondi au scale courant qd scale courant >= 10
     * 
     * @param divisor value by which this <tt>DgcpMontant</tt> is to be divided.
     * @return DgcpMontant = <tt>this / divisor</tt>
     */
    @Override
    public DgcpMontant divide(BigDecimal divisor)
    {
        DgcpMontant result;

        try
        {
            result = DgcpMontant.valueOf(super.divide(divisor));
        }
        catch (ArithmeticException excep)
        {

            if (this.scale() < 10)
            {
                result = DgcpMontant.valueOf(super.setScale(10));
                result = DgcpMontant.valueOf(result.divide(divisor, RoundingMode.HALF_UP));
            }
            else
            {
                result = DgcpMontant.valueOf(super.divide(divisor, RoundingMode.HALF_UP));
            }
            log.error(excep.getMessage(), excep);
        }

        return result;
    }

    /**
     * methode multiply : Appelle la méthode multiply de BigDecimal et retourne un nouveau DgcpMontant.
     * 
     * @param multiplicand value to be multiplied by this <tt>DgcpMontant</tt>.
     * @return DgcpMontant = <tt>this * multiplicand</tt>
     */
    @Override
    public DgcpMontant multiply(BigDecimal multiplicand)
    {
        return DgcpMontant.valueOf(super.multiply(multiplicand));
    }

    /**
     * methode subtract : Appelle la méthode subtract de BigDecimal et retourne un nouveau DgcpMontant.
     * 
     * @param subtrahend value to be subtracted from this <tt>DgcpMontant</tt>.
     * @return DgcpMontant = <tt>this - subtrahend</tt>
     */
    @Override
    public DgcpMontant subtract(BigDecimal subtrahend)
    {
        return DgcpMontant.valueOf(super.subtract(subtrahend));
    }

    /**
     * methode round2Dec : Appelle la méthode setScale de BigDecimal et retourne un nouveau DgcpMontant arrondi à 2
     * décimales avec le mode d'arrondi HALF_UP (arrondi supérieur de n,nn50)
     * 
     * @return a <tt>DgcpMontant</tt> rounded according to the <tt>HALF_UP</tt> settings.
     */
    public DgcpMontant round2Dec()
    {
        return DgcpMontant.valueOf(super.setScale(2, ROUND_HALF_UP));
    }

    /**
     * methode round0Dec : Appelle la méthode setScale de BigDecimal et retourne un nouveau DgcpMontant arrondi au
     * nombre entier avec le mode d'arrondi HALF_UP (arrondi supérieur de n,50)
     * 
     * @return a <tt>DgcpMontant</tt> rounded according to the <tt>HALF_UP</tt> settings.
     */
    public DgcpMontant round0Dec()
    {
        return DgcpMontant.valueOf(super.setScale(0, ROUND_HALF_UP));
    }

}
