/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.editionbean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.gouv.finances.cp.dmo.techbean.CalculMontants;
import fr.gouv.finances.lombok.util.base.BaseTechBean;

/**
 * Exomple de bean pour que JASPER affiche un tableau avec des montants précalculés
 * 
 * @author amleplatinec
 * @version $Revision: 1.5 $
 */
public class EditionTypeImpot extends BaseTechBean
{

    /** serialVersionUID - long,. */
    private static final long serialVersionUID = 1L;

    /** id. */
    private Long id;

    /** code impot. */
    private Long codeImpot;

    /** nom impot. */
    private String nomImpot;

    /** est ouvert a la mensualisation. */
    private Boolean estOuvertALaMensualisation;

    /** est destine aux particuliers. */
    private Boolean estDestineAuxParticuliers;

    /** jour ouverture adhesion. */
    private Date jourOuvertureAdhesion;

    /** jour limite adhesion. */
    private Date jourLimiteAdhesion;

    /** taux. */
    private BigDecimal taux;

    /** seuil. */
    private BigDecimal seuil;

    /** lesTypesImpots - List, attribut nécessaire pour les calculs précalculés. */
    private List lesTypesImpots = new ArrayList<>();

    /**
     * Constructeur 
     */
    public EditionTypeImpot()
    {
        super(); 

    }

    /**
     * Gets the code impot.
     * 
     * @return the code impot
     */
    public Long getCodeImpot()
    {
        return codeImpot;
    }

    /**
     * Accesseur de l attribut decompte est ouvert a la mensualisation.
     * 
     * @return decompte est ouvert a la mensualisation
     */
    public Double getDecompteEstOuvertALaMensualisation()
    {
        CalculMontants calculMontants = new CalculMontants();
        return calculMontants.calculerDecompteEstOuvertALaMensualisation(lesTypesImpots);
    }

    /**
     * Accesseur de l attribut decompte jour limite adhesion.
     * 
     * @return decompte jour limite adhesion
     */
    public Double getDecompteJourLimiteAdhesion()
    {
        CalculMontants calculMontants = new CalculMontants();
        return calculMontants.calculerDecompteJourLimiteAdhesion(lesTypesImpots);
    }

    /**
     * Gets the est destine aux particuliers.
     * 
     * @return the est destine aux particuliers
     */
    public Boolean getEstDestineAuxParticuliers()
    {
        return estDestineAuxParticuliers;
    }

    /**
     * Gets the est ouvert a la mensualisation.
     * 
     * @return the est ouvert a la mensualisation
     */
    public Boolean getEstOuvertALaMensualisation()
    {
        return estOuvertALaMensualisation;
    }

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Long getId()
    {
        return id;
    }

    /**
     * Gets the jour limite adhesion.
     * 
     * @return the jour limite adhesion
     */
    public Date getJourLimiteAdhesion()
    {
        return jourLimiteAdhesion;
    }

    /**
     * Gets the jour ouverture adhesion.
     * 
     * @return the jour ouverture adhesion
     */
    public Date getJourOuvertureAdhesion()
    {
        return jourOuvertureAdhesion;
    }

    /**
     * Gets the lesTypesImpots - List, attribut nécessaire pour les calculs précalculés.
     * 
     * @return the lesTypesImpots - List, attribut nécessaire pour les calculs précalculés
     */
    public List getLesTypesImpots()
    {
        return lesTypesImpots;
    }

    /**
     * Accesseur de l attribut moyenne non null code impot.
     * 
     * @return moyenne non null code impot
     */
    public BigDecimal getMoyenneNonNullCodeImpot()
    {
        CalculMontants calculMontants = new CalculMontants();
        return calculMontants.calculerMoyenneNonNullCodeImpot(lesTypesImpots);
    }

    /**
     * Gets the nom impot.
     * 
     * @return the nom impot
     */
    public String getNomImpot()
    {
        return nomImpot;
    }

    /**
     * Accesseur de l attribut pourcentage est ouvert a la mensualisation.
     * 
     * @return pourcentage est ouvert a la mensualisation
     */
    public BigDecimal getPourcentageEstOuvertALaMensualisation()
    {
        CalculMontants calculMontants = new CalculMontants();
        return calculMontants.calculerPourcentageEstOuvertALaMensualisation(lesTypesImpots);
    }

    /**
     * Accesseur de l attribut pourcentage jour limite adhesion.
     * 
     * @return pourcentage jour limite adhesion
     */
    public BigDecimal getPourcentageJourLimiteAdhesion()
    {
        CalculMontants calculMontants = new CalculMontants();
        return calculMontants.calculerPourcentageJourLimiteAdhesion(lesTypesImpots);
    }

    /**
     * Gets the seuil.
     * 
     * @return the seuil
     */
    public BigDecimal getSeuil()
    {
        return seuil;
    }

    /**
     * Gets the taux.
     * 
     * @return the taux
     */
    public BigDecimal getTaux()
    {
        return taux;
    }

    /**
     * Sets the code impot.
     * 
     * @param codeImpot the new code impot
     */
    public void setCodeImpot(Long codeImpot)
    {
        this.codeImpot = codeImpot;
    }

    /**
     * Modificateur de l attribut decompte est ouvert a la mensualisation.
     * 
     * @param decompteEstOuvertALaMensualisation le nouveau decompte est ouvert a la mensualisation
     */
    public void setDecompteEstOuvertALaMensualisation(Double decompteEstOuvertALaMensualisation)
    {
    }

    /**
     * Modificateur de l attribut decompte jour limite adhesion.
     * 
     * @param decompteJourLimiteAdhesion le nouveau decompte jour limite adhesion
     */
    public void setDecompteJourLimiteAdhesion(Double decompteJourLimiteAdhesion)
    {
    }

    /**
     * Sets the est destine aux particuliers.
     * 
     * @param estDestineAuxParticuliers the new est destine aux particuliers
     */
    public void setEstDestineAuxParticuliers(Boolean estDestineAuxParticuliers)
    {
        this.estDestineAuxParticuliers = estDestineAuxParticuliers;
    }

    /**
     * Sets the est ouvert a la mensualisation.
     * 
     * @param estOuvertALaMensualisation the new est ouvert a la mensualisation
     */
    public void setEstOuvertALaMensualisation(Boolean estOuvertALaMensualisation)
    {
        this.estOuvertALaMensualisation = estOuvertALaMensualisation;
    }

    /**
     * Sets the id.
     * 
     * @param id the new id
     */
    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Sets the jour limite adhesion.
     * 
     * @param jourLimiteAdhesion the new jour limite adhesion
     */
    public void setJourLimiteAdhesion(Date jourLimiteAdhesion)
    {
        this.jourLimiteAdhesion = jourLimiteAdhesion;
    }

    /**
     * Sets the jour ouverture adhesion.
     * 
     * @param jourOuvertureAdhesion the new jour ouverture adhesion
     */
    public void setJourOuvertureAdhesion(Date jourOuvertureAdhesion)
    {
        this.jourOuvertureAdhesion = jourOuvertureAdhesion;
    }

    /**
     * Sets the lesTypesImpots - List, attribut nécessaire pour les calculs précalculés.
     * 
     * @param lesTypesImpots the new lesTypesImpots - List, attribut nécessaire pour les calculs précalculés
     */
    public void setLesTypesImpots(List lesTypesImpots)
    {
        this.lesTypesImpots = lesTypesImpots;
    }

    /**
     * Modificateur de l attribut moyenne non null code impot.
     * 
     * @param moyenneNonNullCodeImpot le nouveau moyenne non null code impot
     */
    public void setMoyenneNonNullCodeImpot(BigDecimal moyenneNonNullCodeImpot)
    {
    }

    /**
     * Sets the nom impot.
     * 
     * @param nomImpot the new nom impot
     */
    public void setNomImpot(String nomImpot)
    {
        this.nomImpot = nomImpot;
    }

    /**
     * Modificateur de l attribut pourcentage est ouvert a la mensualisation.
     * 
     * @param pourcentageEstOuvertALaMensualisation le nouveau pourcentage est ouvert a la mensualisation
     */
    public void setPourcentageEstOuvertALaMensualisation(BigDecimal pourcentageEstOuvertALaMensualisation)
    {
        new CalculMontants();

    }

    /**
     * Modificateur de l attribut pourcentage jour limite adhesion.
     * 
     * @param pourcentageJourLimiteAdhesion le nouveau pourcentage jour limite adhesion
     */
    public void setPourcentageJourLimiteAdhesion(BigDecimal pourcentageJourLimiteAdhesion)
    {
    }

    /**
     * Sets the seuil.
     * 
     * @param seuil the new seuil
     */
    public void setSeuil(BigDecimal seuil)
    {
        this.seuil = seuil;
    }

    /**
     * Sets the taux.
     * 
     * @param taux the new taux
     */
    public void setTaux(BigDecimal taux)
    {
        this.taux = taux;
    }

}
