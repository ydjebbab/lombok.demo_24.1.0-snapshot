/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TestAdressesPersonnesDao.java
 *
 */
package fr.gouv.finances.cp.dmo.dao;

import java.util.List;

import fr.gouv.finances.cp.dmo.entite.AdresseTest;
import fr.gouv.finances.cp.dmo.entite.PersonneTest;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Interface TestAdressesPersonnesDao DAO utilisé pour les tests Hibernate des relations n-n bi-directionnelles.
 * 
 * @author chouard-cp
 * @author CF: migration vers JPA
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
public interface ITestAdressesPersonnesDao extends BaseDaoJpa
{

    /**
     * methode Find les personnes et adresses par ville
     * 
     * @param ville param
     * @return list< personne test>
     */
    List<PersonneTest> findPersonnesEtAdressesParVille(String ville);

    /**
     * methode Find toutes les personnes
     * 
     * @return list< personne test>
     */
    List<PersonneTest> findToutesLesPersonnes();

    /**
     * methode Find toutes les personnes par iterator.
     * 
     * @param nombreOccurences taille du bloc de fetch
     * @return dgcp scroll iterator
     */
    public ScrollIterator findToutesLesPersonnesParIterator(int nombreOccurences);

    /**
     * methode Find une adresse et personne par voie et ville
     * 
     * @param voie param
     * @param ville param
     * @return adresse test
     */
    AdresseTest findAdresseEtPersonneParVoieEtVille(String voie, String ville);

    /**
     * methode Find une personne et adresses par nom
     * 
     * @param nom param
     * @param prenom param
     * @return personne test
     */
    PersonneTest findPersonneEtAdressesParNom(String nom, String prenom);
}
