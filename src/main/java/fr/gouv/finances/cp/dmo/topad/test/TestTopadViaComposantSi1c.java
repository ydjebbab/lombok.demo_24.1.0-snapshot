/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.topad.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.ws.SlpaUtilStandalone;
import fr.gouv.finances.lombok.uuid.UuidFactoryLombokImpl;
import fr.gouv.impots.appli.commun.frameworks.gestionappelsservices.GestionnaireServiceAssistant;
import fr.gouv.impots.appli.commun.frameworks.gestionappelsservices.coordination.ReponseServiceInterface;
import fr.gouv.impots.appli.commun.frameworks.gestionappelsservices.coordination.ov.ServiceAsynchroneValeur;
import fr.gouv.impots.appli.commun.frameworks.gestionappelsservices.transverse.ServiceAssistantFactoryImplJax;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechDysfonctionnementErreur;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechIndisponibiliteErreur;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechProtocolaireErreur;
import fr.gouv.impots.appli.commun.frameworks.gestionuuid.GestionnaireUUID;

/**
 * Class TestTopadViaComposantSi1c .
 */
public final class TestTopadViaComposantSi1c
{

    /**
     * Instanciation de test topad via composant si 1 c.
     */
    private TestTopadViaComposantSi1c()
    {
        throw new AssertionError("Instantiating classe utilitaire ...");

    }

    /**
     * Methode main .
     *
     * @param args on aurait pu surcharger les parametres en entrée
     */
    public static void main(String[] args)
    {

        File file =
            new File(
                "src/main/java/fr/gouv/finances/cp/dmo/topad/test/CSVAPT03.codifierAdresseFr.voulu.xml");

        try (FileInputStream serviceAssitantJaxProperties = new FileInputStream("src/main/resources/serviceAssistantJax.properties");
            OutputStream outputStream = new FileOutputStream(file))
        {

            GestionnaireUUID.addUUIDFactory(new UuidFactoryLombokImpl());
            Properties props = new Properties();
            props.load(serviceAssitantJaxProperties);
            ServiceAssistantFactoryImplJax serviceAssistant = new ServiceAssistantFactoryImplJax(props, new SlpaUtilStandalone());
            GestionnaireServiceAssistant.addServiceAssistantFactory(serviceAssistant);
            ReponseServiceInterface reponseIEIS = GestionnaireServiceAssistant.invoquer(null, "obtenirIEIS", null);
            ServiceAsynchroneValeur savTopad = new ServiceAsynchroneValeur();
            savTopad.setIeis(((ServiceAsynchroneValeur) reponseIEIS.getReponse()[0]).getIeis());
            FileDataSource fileDS =
                new FileDataSource(
                    "src/main/java/fr/gouv/finances/cp/dmo/topad/test/CSVAPT03.codifierAdresseFr.xml");
            DataHandler dataHandler = new DataHandler(fileDS);
            Object[] params = {savTopad, dataHandler};
            ReponseServiceInterface reponse = GestionnaireServiceAssistant.invoquer(null, "codifierAdresseFr", params);
            Object[] reponses = reponse.getReponse();
            DataHandler reponseTopad = (DataHandler) reponses[0];

            InputStream inputStream = null;

            inputStream = reponseTopad.getInputStream();

            byte[] buf = new byte[8192];
            int len;
            while ((len = inputStream.read(buf)) != -1)
            {
                outputStream.write(buf, 0, len);
            }
            inputStream.close();
            outputStream.close();
            serviceAssitantJaxProperties.close();

        }

        catch (TechDysfonctionnementErreur techErr)
        {
            throw ApplicationExceptionTransformateur.transformer(techErr);
        }
        catch (TechIndisponibiliteErreur techIndispoErr)
        {
            throw ApplicationExceptionTransformateur.transformer(techIndispoErr);
        }
        catch (TechProtocolaireErreur techProtoErr)
        {
            throw ApplicationExceptionTransformateur.transformer(techProtoErr);
        }
        catch (FileNotFoundException noFileExc)
        {
            throw new ProgrammationException(noFileExc);
        }
        catch (IOException ioExc)
        {
            throw new ProgrammationException(ioExc);
        }
    }

}
