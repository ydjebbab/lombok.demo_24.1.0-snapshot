/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : TraitementLancerTacheLongueImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf8.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import fr.gouv.finances.cp.dmo.editionbean.EditionContribuable;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;

/**
 * Class TraitementLancerTacheLongueImpl DGFiP.
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementLancerTacheLongueImpl extends AbstractServiceEditionCommunImpl
{
    private IContribuableService contribuableserviceso;

    /**
     * Constructeur
     */
    public TraitementLancerTacheLongueImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param parametresEdition
     * @return collection
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerDatasource(java.util.Map)
     */
    @Override
    public Collection creerDatasource(Map parametresEdition)
    {
        List<EditionContribuable> result = new ArrayList<>();

        int nbecontribuablemodifie = contribuableserviceso.changerLaCasseDeTousLesNomsDeContribuables();
        EditionContribuable uneEditionContribuable = new EditionContribuable();

        String valeurString = String.valueOf(nbecontribuablemodifie);
        uneEditionContribuable.setNombreDeContriTraites(valeurString);
        result.add(uneEditionContribuable);

        return result;

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        String result = "TraitementLancerTacheLongueImpl";
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param parametresEdition
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl#creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Accesseur de l attribut contribuableserviceso.
     *
     * @return contribuableserviceso
     */
    public IContribuableService getContribuableserviceso()
    {
        return contribuableserviceso;
    }

    /**
     * Modificateur de l attribut contribuableserviceso.
     *
     * @param contribuableserviceso le nouveau contribuableserviceso
     */
    public void setContribuableserviceso(IContribuableService contribuableserviceso)
    {
        this.contribuableserviceso = contribuableserviceso;
    }
}
