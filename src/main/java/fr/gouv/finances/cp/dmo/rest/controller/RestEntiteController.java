package fr.gouv.finances.cp.dmo.rest.controller;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;

import javax.persistence.metamodel.EntityType;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.gouv.finances.cp.dmo.rest.service.IEntiteService;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;

@Component
@ComponentScan(basePackages = "fr.gouv.finances.cp.dmo.service.impl")
@Path("/entite")
public class RestEntiteController extends BaseDaoJpaImpl
{
    @Controller
    static class FaviconController
    {

        @GetMapping("favicon.ico")
        @ResponseBody
        void returnNoFavicon()
        {
            // RAS
        }
    }

    @Autowired
    IEntiteService entiteService;

    /**
     * Author : CF 
     * <pre>
     * - Renvoie un objet Response 
     * - Mécanisme CORS : la réponse est enrichie avec un header Access-Control-Allow-Origin avec la
     *  valeur * pour autoriser tous les sites à l'appeler.
     * </pre>
     * @return
     */
    @GET
    @Path("/entitesAutre")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEntitesAutre()
    {
        Response response;
        Map<String, String> mEntites = new TreeMap<>();
        entityManager.getMetamodel().getEntities()
            .stream()
            .forEach(e -> mEntites.put(e.getName(), e.getName()));

        response = Response.status(Status.CREATED).header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Credentials", "true")
            .header("Access-Control-Allow-Headers",
                "origin, content-type, accept, authorization")
            .header("Access-Control-Allow-Methods",
                "GET, POST, PUT, DELETE, OPTIONS, HEAD")
            .entity(mEntites).build();

        return response;
    }

    @GET
    @Path("/entites")
    @Produces({MediaType.APPLICATION_JSON})
    public Map<String, String> getEntites()
    {
        Map<String, String> mEntites = new TreeMap<>();

        entityManager.getMetamodel().getEntities()
            .stream()
            .forEach(e -> mEntites.put(e.getName(), e.getName()));

        return mEntites;
    }

    @GET
    @Path("/ids/{nomentite}")
    @Produces({MediaType.APPLICATION_JSON})
    public Map<Long, Long> getIds(@PathParam(value = "nomentite") String nomEntite)
    {

        // on tente de retrouver la classe par le nom de l'entité...
        Class<?> type = getClassFromName(nomEntite);

        Map<Long, Long> mIds = entiteService.getAvailableIds(type);

        return mIds;
    }

    @GET
    @Path("/entite/{nomentite}/{cle}")
    @Produces({MediaType.APPLICATION_JSON})
    public Object getEntite(@PathParam("nomentite") String nomEntite,
        @PathParam(value = "cle") Long cle)
    {

        // on tente de retrouver la classe par le nom de l'entité...
        Class<?> type = getClassFromName(nomEntite);

        // on tente de récupérer l'entité en base
        return entiteService.get(type, cle);
    }

    @PUT
    @Path("/entite/{nomentite}/{cle}")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Object putEntite(@PathParam("nomentite") String nomEntite,
        @PathParam(value = "cle") Long cle, Object entiteClient)
    {

        // on tente de retrouver la classe par le nom de l'entité...
        Class<?> type = getClassFromName(nomEntite);

        ObjectMapper mapper = new ObjectMapper();
        Object entite = mapper.convertValue(entiteClient, type);

        // on tente de mettre à jour l'entité en base
        return entiteService.merge(entite);
    }

    @GET
    @Path("/entite/{nomentite}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getEntiteSquelette(@PathParam("nomentite") String nomEntite)
        throws InstantiationException, IllegalAccessException
    {

        // on tente de retrouver la classe par le nom de l'entité...
        Class<?> type = getClassFromName(nomEntite);

        // on tente de récupérer l'entité en base
        Object entite = type.newInstance();

        return Response.status(Response.Status.OK).entity(entite).build();
    }

    @POST
    @Path("/entite/{nomentite}")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Object postEntite(@PathParam("nomentite") String nomEntite,
        Object entiteClient)
    {

        // on tente de retrouver la classe par le nom de l'entité...
        Class<?> type = getClassFromName(nomEntite);

        ObjectMapper mapper = new ObjectMapper();
        Object entite = mapper.convertValue(entiteClient, type);

        // on tente de mettre à jour l'entité en base
        entiteService.persist(entite);

        return entite;
    }

    @DELETE
    @Path("/entite/{nomentite}/{cle}")
    @Produces({MediaType.APPLICATION_JSON})
    public Object deleteEntite(@PathParam("nomentite") String nomEntite,
        @PathParam(value = "cle") Long cle)
    {

        // on tente de retrouver la classe par le nom de l'entité...
        Class<?> type = getClassFromName(nomEntite);

        // on tente de récupérer l'entité en base
        Object entite = entiteService.get(type, cle);

        // on tente de mettre à jour l'entité en base
        entiteService.remove(entite);

        return null;
    }

    @Provider
    public class ExceptionHandler
        implements ExceptionMapper<Exception>
    {

        @Override
        public Response toResponse(Exception ex)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }

    @Provider
    public class NoSuchElementExceptionHandler
        implements ExceptionMapper<NoSuchElementException>
    {

        @Override
        public Response toResponse(NoSuchElementException ex)
        {
            return Response.status(Response.Status.NOT_FOUND).entity(ex.getMessage()).build();
        }
    }

    private Class<?> getClassFromName(String nomentite)
    {
        EntityType<?> entityType = entityManager.getMetamodel().getEntities()
            .stream()
            .filter(e -> e.getJavaType().getSimpleName().equalsIgnoreCase(nomentite))
            .findFirst()
            .get();
        return entityType.getJavaType();
    }
}
