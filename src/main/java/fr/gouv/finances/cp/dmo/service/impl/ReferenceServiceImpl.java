/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ReferenceServiceImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.dao.IReferenceDao;
import fr.gouv.finances.cp.dmo.dao.ITypeImpotJdbcDao;
import fr.gouv.finances.cp.dmo.entite.Civilite;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.service.IReferenceService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Class ReferenceServiceImpl
 * 
 * @author chouard-cp
 * @author CF: passage XML vers annotations
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
@Service("referenceService")
public class ReferenceServiceImpl extends BaseServiceImpl implements IReferenceService
{
    private static final Logger log = LoggerFactory.getLogger(ReferenceServiceImpl.class);

    @Autowired
    private IReferenceDao referenceDao;

    @Autowired
    private ITypeImpotJdbcDao typeImpotJdbcDao;
    
    @Value(value = "${referenceservice.limiteseuil}")
    private int limiteSeuil;

    public ReferenceServiceImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.IReferenceService#chargerListCivilite()
     */
    @Override
    public List<Civilite> chargerListCivilite()
    {
        log.debug(">>> Debut methode chargerListCivilite()");
        List<Civilite> listeCivilites = referenceDao.loadAllObjects(Civilite.class);
        // CF : correction
        // On tri les éléments sur le libelle avant édition
        Comparator<Civilite> comparator = new BeanComparator<>("libelle");
        Collections.sort(listeCivilites, comparator);
        return listeCivilites;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param typeImpot
     * @see fr.gouv.finances.cp.dmo.service.IReferenceService#enregistrerTypeImpot(fr.gouv.finances.cp.dmo.entite.TypeImpot)
     */
    @Override
    public void enregistrerTypeImpot(TypeImpot typeImpot)
    {
        log.debug(">>> Debut methode enregistrerTypeImpot()");
        if (referenceDao.isCodeTypeImpotDejaUtilise(typeImpot))
        {
            throw new RegleGestionException(Messages.getString("exception.typeimpot.codeimpotdejautilise"));
        }
        referenceDao.saveTypeImpot(typeImpot);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @return the limite seuil
     * @see fr.gouv.finances.cp.dmo.service.IReferenceService#getLimiteSeuil()
     */
    @Override
    public double getLimiteSeuil()
    {
        return Double.valueOf(limiteSeuil);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.IReferenceService#rechercherTousLesTypesImpots()
     */
    @Override
    public List<TypeImpot> rechercherTousLesTypesImpots()
    {
        log.debug(">>> Debut methode rechercherTousLesTypesImpots()");
        return referenceDao.loadAllObjects(TypeImpot.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.service.IReferenceService#rechercherTousLesTypesImpotsJdbc()
     */
    @Override
    public List<TypeImpot> rechercherTousLesTypesImpotsJdbc()
    {
        log.debug(">>> Debut methode rechercherTousLesTypesImpotsJdbc()");
        return typeImpotJdbcDao.loadTypeImpot();
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.dmo.service.impl.ReferenceService#rechercherTypesImpotsDisponbilesALaMensualisation()
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.IReferenceService#rechercherTypesImpotsDisponbilesALaMensualisation()
     */
    @Override
    public List<TypeImpot> rechercherTypesImpotsDisponbilesALaMensualisation()
    {
        log.debug(">>> Debut methode rechercherTypesImpotsDisponbilesALaMensualisation()");
        List<TypeImpot> listeTypesimpots = referenceDao.findTypesImpotsMensualisables();
        List<TypeImpot> listeTypesimpotsdisponiblesalamensualisation = new ArrayList<>();
        TypeImpot typeimpot;

        Iterator<TypeImpot> iter = listeTypesimpots.iterator();
        while (iter.hasNext())
        {
            typeimpot = iter.next();
            try
            {
                typeimpot.verifierEstOuvertALaMensualisationRG10();
                typeimpot.verifierEstDestineAuxparticuliersRG11();
                listeTypesimpotsdisponiblesalamensualisation.add(typeimpot);
            }
            catch (RegleGestionException rgExc)
            {
                // cas ou les RG ne sont pas vérifiées : rien à
                // faire
                log.info("rg non vérifiée : on ne fait rien ", rgExc);
            }
        }
        return listeTypesimpotsdisponiblesalamensualisation;
    }



    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param elementsAsupprimer
     * @see fr.gouv.finances.cp.dmo.service.IReferenceService#supprimerTypesImpots(java.util.Collection)
     */
    @Override
    public void supprimerTypesImpots(Collection elementsAsupprimer)
    {
        log.debug(">>> Debut methode supprimerTypesImpots()");
        for (Iterator iter = elementsAsupprimer.iterator(); iter.hasNext();)
        {
            TypeImpot unTypeImpot = (TypeImpot) iter.next();
            if (referenceDao.isTypeImpotUtilise(unTypeImpot))
            {
                throw new RegleGestionException(Messages.getString("exception.typeimpot.typeimpotutilise"));
            }
        }
        referenceDao.deleteTypesImpots(elementsAsupprimer);

    }
}
