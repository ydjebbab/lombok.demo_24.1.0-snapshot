/*
 * Copyright (c) 2020 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.dmo.entite.Rib;
import fr.gouv.finances.cp.dmo.mvc.form.RechercheGuichetBancaireForm;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Class RechercheGuichetBancaireValidator
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class RechercheGuichetBancaireValidator implements Validator
{
    private static final Logger log = LoggerFactory.getLogger(RechercheGuichetBancaireValidator.class);

    public RechercheGuichetBancaireValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    @Override
    public boolean supports(Class clazz)
    {
        return clazz.equals(RechercheGuichetBancaireForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param command
     * @param errors
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    @Override
    public void validate(Object command, Errors errors)
    {
        // renvoi rien (et je ne sais pas pourquoi)
    }

    /**
     * methode Validate surface saisie rib : DGFiP.
     *
     * @param command param
     * @param errors param
     */
    public void validateSurfaceSaisieRib(RechercheGuichetBancaireForm command, Errors errors)
    {

        Rib unRib = command.getRib();
        try
        {
            unRib.verifierFormeCodebanque();
        }
        catch (RegleGestionException exception)
        {
            log.error(exception.getMessage(), exception);
            errors.rejectValue("rib.codeBanque", null, exception.getMessage());
        }

        try
        {
            unRib.verifierFormeCodeguichet();
        }
        catch (RegleGestionException exception)
        {
            log.error(exception.getMessage(), exception);
            errors.rejectValue("rib.codeGuichet", null, exception.getMessage());
        }

        try
        {
            unRib.verifierFormeNumerocompte();
        }
        catch (RegleGestionException exception)
        {
            log.error(exception.getMessage(), exception);
            errors.rejectValue("rib.numeroCompte", null, exception.getMessage());
        }

        try
        {
            unRib.verifierFormeClerib();
        }
        catch (RegleGestionException exception)
        {
            log.error(exception.getMessage(), exception);
            errors.rejectValue("rib.cleRib", null, exception.getMessage());
        }

    }
}
