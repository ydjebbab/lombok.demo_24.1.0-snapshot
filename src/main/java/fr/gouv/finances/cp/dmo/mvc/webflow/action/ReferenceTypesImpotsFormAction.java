/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.beans.PropertyEditor;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.cp.dmo.editionbean.EditionTypeImpot;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.mvc.form.ReferenceTypesImpotsForm;
import fr.gouv.finances.cp.dmo.service.IReferenceService;
import fr.gouv.finances.cp.dmo.techbean.CalculMontants;
import fr.gouv.finances.lombok.apptags.util.CheckboxSelectUtil;
import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.format.FormaterNombre;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomDateEditor;
import fr.gouv.finances.lombok.util.propertyeditor.CpCustomNumberEditor;
import fr.gouv.finances.lombok.util.propertyeditor.SelectEditor;

/**
 * Action ReferenceTypesImpotsFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
public class ReferenceTypesImpotsFormAction extends FormAction
{
    private static final Logger log = LoggerFactory.getLogger(ReferenceTypesImpotsFormAction.class);

    private static String LISTE_DES_ELEMENTS_DANS_LE_FLUX = "referencetypesimpots";

    private static String LISTE_DES_ELEMENTS_A_SUPPRIMER_DANS_LE_FLUX = "typesimpotsasupprimer";

    private static String TABLEID = "typesimpots";

    private IReferenceService referenceserviceso;

    private EditionDemandeService editiondemandeserviceso;

    public ReferenceTypesImpotsFormAction()
    {
        super();
    }

    public ReferenceTypesImpotsFormAction(Class<?> arg0)
    {
        super(arg0);
    }

    public EditionDemandeService getEditiondemandeserviceso()
    {
        return editiondemandeserviceso;
    }

    public IReferenceService getReferenceserviceso()
    {
        return referenceserviceso;
    }

    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso)
    {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    public void setReferenceserviceso(IReferenceService referenceserviceso)
    {
        this.referenceserviceso = referenceserviceso;
    }

    /**
     * methode Ajouter
     * 
     * @param rc param
     * @return event
     */
    public Event ajouter(RequestContext rc)
    {
        Event result = success();

        ReferenceTypesImpotsForm form = (ReferenceTypesImpotsForm) rc.getFlowScope().get(getFormObjectName());
        TypeImpot typeImpotAEnregistrer = form.getTypeImpot();

        try
        {
            referenceserviceso.enregistrerTypeImpot(typeImpotAEnregistrer);
            Collection<? super Object> listeDesTypesImpots =
                rc.getFlowScope().getCollection(LISTE_DES_ELEMENTS_DANS_LE_FLUX);

            listeDesTypesImpots.add(typeImpotAEnregistrer);
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(rc).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.rejectValue("typeImpot.codeImpot", null, rgExc.getMessage());
            result = error();
        }
        return result;
    }

    /**
     * methode Initialiser formulaire
     * 
     * @param request param
     * @return event
     */
    public Event initialiserFormulaire(RequestContext request)
    {
        ReferenceTypesImpotsForm referenceTypesImpotsForm = (ReferenceTypesImpotsForm) request.getFlowScope().get(getFormObjectName());
        TypeImpot typeImpot = new TypeImpot();
        referenceTypesImpotsForm.setTypeImpot(typeImpot);
        return success();
    }

    /**
     * methode Modifier
     * 
     * @param rc param
     * @return event
     */
    public Event modifier(RequestContext rc)
    {
        Event result = success();

        ReferenceTypesImpotsForm referenceTypesImpotsForm = (ReferenceTypesImpotsForm) rc.getFlowScope().get(getFormObjectName());
        TypeImpot typeImpot = referenceTypesImpotsForm.getTypeImpot();

        try
        {
            referenceserviceso.enregistrerTypeImpot(typeImpot);
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(rc).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.rejectValue("typeImpot.codeImpot", null, rgExc.getMessage());
            result = error();
        }

        return result;
    }

    /**
     * methode Parametrer checkboxes
     * 
     * @param request param
     * @return event
     */
    public Event parametrerCheckboxes(RequestContext request)
    {
        // Sauvegarde de l'objet de paramétrage des checkboxes dans le FlowScope
        CheckboxSelectUtil.parametrerCheckboxes(request, TABLEID).utiliserRowid("id").utiliserListeElements(
            LISTE_DES_ELEMENTS_DANS_LE_FLUX).utiliserMsgSelectionneUnSeulElement(
                "Sélectionnez un seul type d'impôt à la fois.")
            .utiliserMsgAucunEltSelectionne(
                "Aucun type d'impôt n'est sélectionné");
        return success();
    }

    /**
     * methode Preparer checkbox
     * 
     * @param request param
     * @return event
     */
    public Event preparerCheckbox(RequestContext request)
    {
        Map<String, String> typeImpotMensualisableMap = new HashMap<>(1);
        typeImpotMensualisableMap.put("TRUE", "Impôt destiné aux particuliers");
        request.getFlowScope().put("checkboxtypeimpotmensualisable", typeImpotMensualisableMap);
        return success();
    }

    /**
     * Preparer liste types impots
     * 
     * @param rc param
     * @return event
     */
    public Event preparerListeTypesImpots(RequestContext rc)
    {
        List<TypeImpot> typeImpotListe = referenceserviceso.rechercherTousLesTypesImpots();
        double seuil = referenceserviceso.getLimiteSeuil();
        rc.getFlowScope().put("LimiteSeuilTypeImpot", seuil);
        rc.getFlowScope().put(LISTE_DES_ELEMENTS_DANS_LE_FLUX, typeImpotListe);
        return success();
    }

    /**
     * methode Preparer liste types impots
     * 
     * @param rc param
     * @return event
     */
    public Event preparerListeTypesImpotsPourEdition(RequestContext rc)
    {
        List<TypeImpot> typeImpotListe = referenceserviceso.rechercherTousLesTypesImpots();

        // pour besoin edition jasper , on doit transformer les types impots en edition type impot
        // Enregistrement dans le contexte du seuil utilisé par l'intercepteur TypeImpotRowInterceptor

        List<EditionTypeImpot> referenceeditiontypesimpots = new ArrayList<>();
        if (typeImpotListe != null && !typeImpotListe.isEmpty())
        {
            for (int i = 0; i < typeImpotListe.size(); i++)
            {
                EditionTypeImpot uneEditionTypeImpot = new EditionTypeImpot();

                uneEditionTypeImpot.setId(typeImpotListe.get(i).getId());
                uneEditionTypeImpot.setCodeImpot(typeImpotListe.get(i).getCodeImpot());
                uneEditionTypeImpot.setNomImpot(typeImpotListe.get(i).getNomImpot());
                uneEditionTypeImpot.setEstDestineAuxParticuliers(typeImpotListe.get(i)
                    .getEstDestineAuxParticuliers());
                uneEditionTypeImpot.setEstOuvertALaMensualisation(typeImpotListe.get(i)
                    .getEstOuvertALaMensualisation());
                uneEditionTypeImpot.setJourLimiteAdhesion(typeImpotListe.get(i).getJourLimiteAdhesion());
                uneEditionTypeImpot.setJourOuvertureAdhesion(typeImpotListe.get(i).getJourOuvertureAdhesion());
                uneEditionTypeImpot.setSeuil(typeImpotListe.get(i).getSeuil());
                uneEditionTypeImpot.setTaux(typeImpotListe.get(i).getTaux());
                uneEditionTypeImpot.getLesTypesImpots().addAll(typeImpotListe);
                CalculMontants unCalculMontants = new CalculMontants();

                rc.getFlowScope().put("PourcentageEstOuvertALaMensualisation",
                    unCalculMontants.calculerPourcentageEstOuvertALaMensualisation(typeImpotListe));

                rc.getFlowScope().put("DecompteEstOuvertALaMensualisation",
                    unCalculMontants.calculerDecompteEstOuvertALaMensualisation(typeImpotListe).intValue());

                rc.getFlowScope().put("DecompteJourLimiteAdhesion",
                    unCalculMontants.calculerDecompteJourLimiteAdhesion(typeImpotListe).intValue());

                rc.getFlowScope().put("MoyenneNonNullCodeImpot",
                    unCalculMontants.calculerMoyenneNonNullCodeImpot(typeImpotListe));

                rc.getFlowScope().put("PourcentageJourLimiteAdhesion",
                    unCalculMontants.calculerPourcentageJourLimiteAdhesion(typeImpotListe));

                referenceeditiontypesimpots.add(uneEditionTypeImpot);
            }
        }
        double seuil = referenceserviceso.getLimiteSeuil();
        rc.getFlowScope().put("LimiteSeuilTypeImpot", seuil);
        rc.getFlowScope().put(LISTE_DES_ELEMENTS_DANS_LE_FLUX, referenceeditiontypesimpots);
        return success();
    }

    /**
     * methode Produire edition via jdbc
     *
     * @param rc
     * @return event
     */
    public Event produireEditionViaJdbc(RequestContext rc)
    {
        // Récupération des caractéristiques de la personne connectée
        // dans le contexte sécurisé pour retrouver l'uid

        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();
        EditionAsynchroneNotifieeParMail editionAsynchroneNotifieeParMail =
            editiondemandeserviceso.initEditionAsynchroneNotifieeParMail("zf8.affichertypesimpotsjdbc.edition", personne
                .getUid(), personne.getMail());
        editionAsynchroneNotifieeParMail =
            editiondemandeserviceso.declencherEdition(editionAsynchroneNotifieeParMail, new HashMap());
        rc.getFlowScope().put("jobHistory", editionAsynchroneNotifieeParMail);

        return success();
    }

    /**
     * methode Selectionner par une checkbox
     * 
     * @param rc param
     * @return event
     */
    public Event selectionnerParUneCheckbox(RequestContext rc)
    {
        Event result;

        Object elementSelectionne = CheckboxSelectUtil.extraitUnElementSelectionneDansLeFlux(rc, TABLEID);
        if (elementSelectionne != null)
        {
            ReferenceTypesImpotsForm form = (ReferenceTypesImpotsForm) rc.getFlowScope().get(getFormObjectName());
            form.setTypeImpot((TypeImpot) elementSelectionne);
            result = success();
        }
        else
        {
            result = error();
        }
        return result;
    }

    /**
     * methode Selectionner par un lien
     * 
     * @param rc param
     * @return event
     */
    public Event selectionnerParUnLien(RequestContext rc)
    {
        Event result;

        List referencetypesimpots = (List) rc.getFlowScope().get(LISTE_DES_ELEMENTS_DANS_LE_FLUX);
        PropertyEditor propertyEditor = new SelectEditor(referencetypesimpots, "id");

        propertyEditor.setAsText(rc.getRequestParameters().get("id"));
        TypeImpot unTypeImpotSelectionne = (TypeImpot) propertyEditor.getValue();
        if (unTypeImpotSelectionne != null)
        {
            ReferenceTypesImpotsForm form = (ReferenceTypesImpotsForm) rc.getFlowScope().get(getFormObjectName());
            form.setTypeImpot(unTypeImpotSelectionne);
            result = success();
        }
        else
        {
            result = error();
        }

        return result;
    }

    /**
     * methode Selectionner types impots a supprimer
     * 
     * @param rc param
     * @return event
     */
    public Event selectionnerTypesImpotsASupprimer(RequestContext rc)
    {
        Event result;
        Collection elementsSelectionnes = CheckboxSelectUtil.extraitLesElementsSelectionnesDansLeFlux(rc, TABLEID);

        if (CheckboxSelectUtil.auMoinsUnElementSelectionne(elementsSelectionnes))
        {
            rc.getFlowScope().put(LISTE_DES_ELEMENTS_A_SUPPRIMER_DANS_LE_FLUX, elementsSelectionnes);
            result = success();
        }
        else
        {
            result = error();
        }
        return result;
    }

    /**
     * methode Supprimer types impots
     * 
     * @param rc param
     * @return event
     */
    public Event supprimerTypesImpots(RequestContext rc)
    {
        Event result = success();

        Collection<?> elementsASupprimer =
            (Collection<?>) rc.getFlowScope().get(LISTE_DES_ELEMENTS_A_SUPPRIMER_DANS_LE_FLUX);

        try
        {
            referenceserviceso.supprimerTypesImpots(elementsASupprimer);
            Collection<?> listeDesTypesImpots = rc.getFlowScope().getCollection(LISTE_DES_ELEMENTS_DANS_LE_FLUX);
            listeDesTypesImpots.removeAll(elementsASupprimer);
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(rc).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rgExc.getMessage());
            result = error();
        }

        return result;
    }

    /**
     * methode Traiter checkboxes entre pages
     * 
     * @param context param
     * @return event
     */
    public Event traiterCheckboxesEntrePages(RequestContext context)
    {
        CheckboxSelectUtil.traiterCheckboxesEntrePages(context, TABLEID);
        return success();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param rc
     * @param per
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext rc, PropertyEditorRegistry per)
    {
        super.registerPropertyEditors(rc, per);
        per.registerCustomEditor(Date.class, "typeImpot.jourOuvertureAdhesion", new CpCustomDateEditor());

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.FRANCE);
        dateFormat.setLenient(false);

        per.registerCustomEditor(Date.class, "typeImpot.jourLimiteAdhesion", new CustomDateEditor(dateFormat, true));

        // NumberFormat unNumberFormat = NumberFormat.getInstance(Locale.FRANCE);
        // NumberFormat unNumberFormat = DecimalFormat.getIntegerInstance(Locale.FRANCE);
        // unNumberFormat.setMaximumFractionDigits(0);

        per.registerCustomEditor(BigDecimal.class, "typeImpot.taux", new CpCustomNumberEditor(BigDecimal.class,
            FormaterNombre.getFormatNombreDeuxDecimales(), CpCustomNumberEditor.NULL, true));

        /*
         * per.registerCustomEditor(BigDecimal.class, "typeImpot.taux", new
         * CpCustomNumberEditor(BigDecimal.class,unNumberFormat, CpCustomNumberEditor.NULL,true));
         */
        per.registerCustomEditor(BigDecimal.class, "typeImpot.seuil", new CpCustomNumberEditor(BigDecimal.class,
            FormaterNombre.getFormatNombreDeuxDecimales(), CpCustomNumberEditor.NULL));
    }

}
