package fr.gouv.finances.cp.dmo.rest.service.impl;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.rest.service.IEntiteService;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;

@Service("entiteService")
@CacheConfig(cacheNames = {"entites"})
@Transactional(transactionManager = "transactionManager")
public class EntiteServiceImpl extends BaseDaoJpaImpl implements IEntiteService 
{
    private static final Logger log = LoggerFactory.getLogger(EntiteServiceImpl.class);

    @Override
    public Object merge(Object entite)
    {
        return entityManager.merge(entite);
    }

    @Override
    public void persist(Object entite)
    {
        entityManager.persist(entite);
    }

    @Override
    public void remove(Object entite)
    {
        entityManager.remove(entityManager.contains(entite) ? entite : entityManager.merge(entite));
    }

    @Override
    public Object get(Class<?> entityClass, Object primaryKey)
    {
        return entityManager.find(entityClass, primaryKey);
    }

    @Override
    // @Cacheable
    public Map<Long, Long> getAvailableIds(Class<?> entityClass)
    {
        log.debug(">>> Entrée dans getAvailableIds()");
        Map<Long, Long> mIds = new HashMap<>();

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery(entityClass);
        Root rootEntry = cq.from(entityClass);
        CriteriaQuery all = cq.select(rootEntry);
        TypedQuery allQuery = entityManager.createQuery(all);
        allQuery.getResultList().stream()
            .forEach(r -> {
                try
                {
                    Method method = r.getClass().getMethod("getId");
                    Long id = (Long) method.invoke(r);
                    mIds.put(id, id);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            });
        return mIds;
    }
}
