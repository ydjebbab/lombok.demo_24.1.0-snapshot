/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TimeZoneEditor.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.editor;

import java.beans.PropertyEditorSupport;
import java.util.TimeZone;

import org.springframework.util.StringUtils;

/**
 * Class TimeZoneEditor DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class TimeZoneEditor extends PropertyEditorSupport
{

    /** allow empty. */
    private final boolean allowEmpty;

    /**
     * Instanciation de time zone editor.
     * 
     * @param allowEmpty param
     */
    public TimeZoneEditor(boolean allowEmpty)
    {
        super();
        this.allowEmpty = allowEmpty;
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see java.beans.PropertyEditorSupport#getAsText()
     */
    @Override
    public String getAsText()
    {
        TimeZone value = (TimeZone) getValue();

        return (value != null ? value.getID() : "");
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException
    {

        if (this.allowEmpty && !StringUtils.hasText(text))
        {
            // Traite une chaine vide comme une valeur nulle.
            setValue(null);
        }
        else if (text != null && !estUnFuseauHoraire(text))
        {
            throw new IllegalArgumentException("Impossible de convertir en fuseau horaire");
        }
        else
        {
            {
                setValue(TimeZone.getTimeZone(text));
            }
        }
    }

    /**
     * methode Est un fuseau horaire : DGFiP.
     * 
     * @param text param
     * @return true, si c'est vrai
     */
    private boolean estUnFuseauHoraire(String text)
    {
        return true;
    }

}
