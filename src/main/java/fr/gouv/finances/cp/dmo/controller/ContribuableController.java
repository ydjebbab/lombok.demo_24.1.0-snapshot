package fr.gouv.finances.cp.dmo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.cp.dmo.techbean.CriteresRecherches;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Web service pour la gestion des contribuables
 * 
 * @author celfer Date: 5 juin 2019
 */
@RestController
@RequestMapping("contribuable")
@Api(value = "/contribuable", consumes = "application/json")
public class ContribuableController
{
    @Autowired
    IContribuableService contribuableService;

    // Exemple : http://localhost:8084/dmo/contribuable/recherche/01111111111
    @GetMapping("/recherche/{avisImposition}")
    @ApiOperation(value = "Recherche de contribuable par avis d'imposition", notes = "Les détails du contribuable sont restitués", response = Contribuable.class)
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Référence d'avis d'imposition invalide"),
            @ApiResponse(code = 404, message = "Contribuable non trouvé")})
    public ResponseEntity<Contribuable> getContribuableParAvisImposition(
        @ApiParam(value = "Avis d'imposition à rechercher", required = true) @PathVariable String avisImposition)
    {
        ContribuablePar contribuablePar = new ContribuablePar();

        CriteresRecherches criteresRecherches = new CriteresRecherches();
        criteresRecherches.setTypeRechercheId(CriteresRecherches.RECHERCHE_CONTRIBUABLE_PAR_AVIS_IMPOSITION);
        criteresRecherches.setReferenceAvis(avisImposition);
        Optional<List<? extends Contribuable>> listeBaseBean = contribuableService.rechercherListeContribuable(criteresRecherches);
        List<? extends Contribuable> listeContribuable = listeBaseBean.get();

        if (null != listeContribuable && !listeContribuable.isEmpty())
        {
            contribuablePar = (ContribuablePar) listeContribuable.get(0);
            contribuablePar.setLesDocsJoints(null);
            contribuablePar.setListeAvisImposition(null);
            return new ResponseEntity<Contribuable>(contribuablePar, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<Contribuable>(HttpStatus.NOT_FOUND);
        }
    }

    // Exemple : http://localhost:8084/dmo/contribuable/modification/123
    @PostMapping("/modification/{adminId}")
    @ApiOperation(value = "Modification de contribuable", notes = "Les détails du contribuable sont modifiés", response = Contribuable.class)
    @ApiResponses(value = {@ApiResponse(code = 400, message = "Contribuable invalide"),
            @ApiResponse(code = 404, message = "Contribuable non trouvé")})
    public ResponseEntity<Contribuable> modifierContribuable(
        @ApiParam(value = "Contribuable", required = true) @RequestBody Contribuable contribuable,
        @ApiParam(value = "Id admin", required = true) @PathVariable String adminId)
    {
        try
        {
            contribuableService.modifierUnContribuable(contribuable, adminId);
            return new ResponseEntity<Contribuable>(contribuable, HttpStatus.OK);
        }
        catch (Exception exception)
        {
            return new ResponseEntity<Contribuable>(HttpStatus.NOT_FOUND);
        }
    }

}
