/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementAgregAvisParTypeImpotSeuilStub.java
 *
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;

import fr.gouv.finances.cp.dmo.techbean.StatistiquesAvisImpositions;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunStubImpl;

/**
 * Class TraitementAgregAvisParTypeImpotSeuilStub DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
public class TraitementAgregAvisParTypeImpotSeuilStub extends AbstractServiceEditionCommunStubImpl
{
    /**
     * Constructeur
     */
    public TraitementAgregAvisParTypeImpotSeuilStub()
    {
        super();
    }

    /**
     * methode Creates the bean collection : DGFiP.
     * 
     * @return collection
     */
    public static Collection createBeanCollection()
    {
        Collection<StatistiquesAvisImpositions> list = new ArrayList<>();

        for (int i = 1; i < 20; i++)
        {
            StatistiquesAvisImpositions statistiquesAvisImpositions = new StatistiquesAvisImpositions();
            statistiquesAvisImpositions.setNomImpot("Taxe foncière spéciale " + i);
            if (i % 3 == 0)
            {
                statistiquesAvisImpositions.setAnnee("1945");
            }
            else if (i % 2 == 0)
            {
                statistiquesAvisImpositions.setAnnee("1960");
            }
            else
            {
                statistiquesAvisImpositions.setAnnee("1980");
            }
            statistiquesAvisImpositions.setMontantTotal(new Double("111111"));
            statistiquesAvisImpositions.setMontantMoyen(new Double("5555"));
            statistiquesAvisImpositions.setNombreAvis(Long.valueOf("10"));
            statistiquesAvisImpositions.setDateRoleMax(new Date());
            statistiquesAvisImpositions.setDateRoleMin(new Date());
            list.add(statistiquesAvisImpositions);
        }

        // tri de la liste par année de naissance
        List<Comparator> sortFields = new ArrayList<Comparator>();
        sortFields.add(new BeanComparator("annee"));
        sortFields.add(new BeanComparator("nomImpot"));
        ComparatorChain multiSort = new ComparatorChain(sortFields);
        Collections.sort((List<StatistiquesAvisImpositions>) list,
            (Comparator<? super StatistiquesAvisImpositions>) multiSort);

        return list;
    }

}
