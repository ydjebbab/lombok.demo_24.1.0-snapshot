/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : CustomizeChart.java
 *
 */
package fr.gouv.finances.cp.dmo.zf3.edition;

import java.awt.Color;

import net.sf.jasperreports.engine.JRChart;
import net.sf.jasperreports.engine.JRChartCustomizer;
import net.sf.jasperreports.engine.JRDefaultScriptlet;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;

/**
 * Class CustomizeChart DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class CustomizeChart extends JRDefaultScriptlet implements JRChartCustomizer
{

    /** pie plot. */
    private PiePlot piePlot;

    /**
     * Constructeur 
     */
    public CustomizeChart()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param arg0
     * @param arg1
     * @see net.sf.jasperreports.engine.JRChartCustomizer#customize(org.jfree.chart.JFreeChart,
     *      net.sf.jasperreports.engine.JRChart)
     */
    @Override
    public void customize(JFreeChart arg0, JRChart arg1)
    {

        PiePlot piePlot = (PiePlot) arg0.getPlot();
        setPiePlot(piePlot);
        piePlot.setDataset(piePlot.getDataset());

        // solution ci desssous ne marche qu avec la légende
        // piePlot.setIgnoreNullValues(true);
        // piePlot.setSectionOutlinesVisible(true);

        // solution qui marche à priori dans tous les cas
        piePlot.setIgnoreZeroValues(true);
        // correspond à premier champ du stub
        piePlot.setSectionPaint(piePlot.getDataset().getKey(0), Color.PINK);
        piePlot.setSectionPaint(piePlot.getDataset().getKey(1), Color.BLUE);
        piePlot.setSectionPaint(piePlot.getDataset().getKey(2), Color.GREEN);
        piePlot.setSectionPaint(piePlot.getDataset().getKey(3), Color.YELLOW);
        // piePlot.setSectionPaint(4, Color.PINK);

        // pour éclater une part de camenbert (attention pas dynamique)
        piePlot.setExplodePercent(piePlot.getDataset().getKey(0), 0.25);
        piePlot.setExplodePercent(piePlot.getDataset().getKey(1), 0.25);
        piePlot.setExplodePercent(piePlot.getDataset().getKey(2), 0.25);
        piePlot.setExplodePercent(piePlot.getDataset().getKey(3), 0.25);

    }

    /**
     * Gets the pie plot.
     *
     * @return the pie plot
     */
    public PiePlot getPiePlot()
    {
        return piePlot;
    }

    /**
     * Sets the pie plot.
     *
     * @param piePlot the new pie plot
     */
    public void setPiePlot(PiePlot piePlot)
    {
        this.piePlot = piePlot;
    }

}
