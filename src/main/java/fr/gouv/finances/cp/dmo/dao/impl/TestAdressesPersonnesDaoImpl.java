/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.ITestAdressesPersonnesDao;
import fr.gouv.finances.cp.dmo.entite.AdresseTest;
import fr.gouv.finances.cp.dmo.entite.AdresseTest_;
import fr.gouv.finances.cp.dmo.entite.PersonneTest;
import fr.gouv.finances.cp.dmo.entite.PersonneTest_;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * DAO utilisé pour les tests Hibernate des relations n-n bi-directionnelles.
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Repository("testAdressesPersonnesDao")
@Transactional(transactionManager="transactionManager")
public class TestAdressesPersonnesDaoImpl extends BaseDaoJpaImpl implements ITestAdressesPersonnesDao
{
    private static final Logger log = LoggerFactory.getLogger(TestAdressesPersonnesDaoImpl.class);

    public TestAdressesPersonnesDaoImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param ville
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.ITestAdressesPersonnesDao#findPersonnesEtAdressesParVille(java.lang.String)
     */
    @Override
    public List<PersonneTest> findPersonnesEtAdressesParVille(String ville)
    {
        log.debug(">>> Debut methode findLesPersonnesEtAdressesParVille");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PersonneTest> criteriaQuery = criteriabuilder.createQuery(PersonneTest.class);
        Root<PersonneTest> personneTestRoot = criteriaQuery.from(PersonneTest.class);
        personneTestRoot.fetch(PersonneTest_.listeAdresses, JoinType.INNER);
        Join<PersonneTest, AdresseTest> joinPersonneAdresse = personneTestRoot.join(PersonneTest_.listeAdresses);

        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(joinPersonneAdresse.get(AdresseTest_.ville), ville));
        criteriaQuery.where(predicate);

        criteriaQuery.select(personneTestRoot);

        // Pour debug seulement
        TypedQuery<PersonneTest> personneTestTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("personneTestTypedQuery : " + personneTestTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<PersonneTest> personneTestTypedQueryListeResultat = personneTestTypedQuery.getResultList();
        log.debug("personneTestTypedQueryListeResultat : " + personneTestTypedQueryListeResultat);
        return personneTestTypedQueryListeResultat;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.ITestAdressesPersonnesDao#findToutesLesPersonnes()
     */
    @Override
    public List<PersonneTest> findToutesLesPersonnes()
    {
        log.debug(">>> Debut methode findToutesLesPersonnes");
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PersonneTest> criteriaQuery = criteriabuilder.createQuery(PersonneTest.class);

        List<PersonneTest> listePersonneTest = findAll(criteriaQuery);
        return listePersonneTest;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.dao.ITestAdressesPersonnesDao#findToutesLesPersonnesParIterator(int)
     */
    @Override
    public ScrollIterator findToutesLesPersonnesParIterator(int nombreOccurences)
    {
        log.debug(">>> Debut methode findToutesLesPersonnesParIterator");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PersonneTest> criteriaQuery = criteriabuilder.createQuery(PersonneTest.class);
        Root<PersonneTest> personneTestRoot = criteriaQuery.from(PersonneTest.class);

        criteriaQuery.select(personneTestRoot);

        ScrollIterator scrollIterator = getScrollIterator(criteriaQuery, nombreOccurences);
        return scrollIterator;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param voie
     * @param ville
     * @return adresse test
     * @see fr.gouv.finances.cp.dmo.dao.ITestAdressesPersonnesDao#findAdresseEtPersonneParVoieEtVille(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public AdresseTest findAdresseEtPersonneParVoieEtVille(String voie, String ville)
    {
        log.debug(">>> Debut methode findAdresseEtPersonneParVoieEtVille");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<AdresseTest> criteriaQuery = criteriabuilder.createQuery(AdresseTest.class);
        Root<AdresseTest> adresseTestRoot = criteriaQuery.from(AdresseTest.class);
        adresseTestRoot.fetch(AdresseTest_.listePersonnes, JoinType.INNER);

        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(adresseTestRoot.get(AdresseTest_.voie), voie));
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(adresseTestRoot.get(AdresseTest_.ville), ville));
        criteriaQuery.where(predicate);

        criteriaQuery.select(adresseTestRoot);

        // Pour debug seulement
        TypedQuery<AdresseTest> adresseTestTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("adresseTestTypedQuery : " + adresseTestTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<AdresseTest> adresseTestTypedQueryListeResultat = adresseTestTypedQuery.getResultList();
        log.debug("adresseTestTypedQueryListeResultat : " + adresseTestTypedQueryListeResultat);

        if (adresseTestTypedQueryListeResultat != null && !adresseTestTypedQueryListeResultat.isEmpty())
        {
            log.debug("adresseTestTypedQueryListeResultat.size() : " + adresseTestTypedQueryListeResultat.size());
            return adresseTestTypedQueryListeResultat.get(0);
        }
        else
        {
            return null;
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nom
     * @param prenom
     * @return personne test
     * @see fr.gouv.finances.cp.dmo.dao.ITestAdressesPersonnesDao#findPersonneEtAdressesParNom(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public PersonneTest findPersonneEtAdressesParNom(String nom, String prenom)
    {
        log.debug(">>> Debut methode findPersonneEtAdressesParNom");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PersonneTest> criteriaQuery = criteriabuilder.createQuery(PersonneTest.class);
        Root<PersonneTest> personneTestRoot = criteriaQuery.from(PersonneTest.class);
        personneTestRoot.fetch(PersonneTest_.listeAdresses, JoinType.INNER);

        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(personneTestRoot.get(PersonneTest_.nom), nom));
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(personneTestRoot.get(PersonneTest_.prenom), prenom));
        criteriaQuery.where(predicate);

        criteriaQuery.select(personneTestRoot);

        // Pour debug seulement
        TypedQuery<PersonneTest> personneTestTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("personneTestTypedQuery : " + personneTestTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<PersonneTest> personneTestTypedQueryListeResultat = personneTestTypedQuery.getResultList();
        log.debug("personneTestTypedQueryListeResultat : " + personneTestTypedQueryListeResultat);

        if (personneTestTypedQueryListeResultat != null && !personneTestTypedQueryListeResultat.isEmpty())
        {
            log.debug("personneTestTypedQueryListeResultat.size() : " + personneTestTypedQueryListeResultat.size());
            return personneTestTypedQueryListeResultat.get(0);
        }
        else
        {
            return null;
        }
    }
}