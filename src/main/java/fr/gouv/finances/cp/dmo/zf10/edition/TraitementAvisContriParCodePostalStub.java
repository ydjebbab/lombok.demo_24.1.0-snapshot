/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.zf10.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import fr.gouv.finances.cp.dmo.techbean.StatistiquesContribuables;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunStubImpl;

/**
 * Class TraitementAvisContriParCodePostalStub DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementAvisContriParCodePostalStub extends AbstractServiceEditionCommunStubImpl
{
    /**
     * Constructeur
     */
    public TraitementAvisContriParCodePostalStub()
    {
        super();
    }

    /**
     * methode Creates the bean collection : DGFiP.
     * 
     * @return collection
     */
    public static Collection createBeanCollection()
    {
        Collection<StatistiquesContribuables> list = new ArrayList<>();
        StatistiquesContribuables stats;
        for (int i = 1; i < 10; i++)
        {
            stats = new StatistiquesContribuables();
            stats.setAnneeDeNaissance("196" + i);
            stats.setMontantMoyen(Double.valueOf((double) 500 + i * 100));
            stats.setNombreContribuables(Long.valueOf((long) i * 1000));
            stats.setMontantTotal(new Double((stats.getMontantMoyen().doubleValue() * stats.getNombreContribuables()
                .doubleValue())));
            list.add(stats);
        }
        return list;

    }

    /**
     * Accesseur de l attribut description.
     * 
     * @param parametresEdition param
     * @return description
     */
    public String getDescription(Map parametresEdition)
    {
        return "tot4";

    }
}
