/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import org.springframework.webflow.action.FormAction;

import fr.gouv.finances.lombok.menu.service.MenuService;

/**
 * Class PlanSiteFormAction .
 */
public class PlanSiteFormAction extends FormAction
{

    /** menuserviceso. */
    public MenuService menuserviceso;

    /**
     * Constructeur
     */
    public PlanSiteFormAction()
    {
        super();
    }

    /**
     * Constructeur
     *
     * @param arg0 
     */
    public PlanSiteFormAction(Class<?> arg0)
    {
        super(arg0);
    }

    /**
     * Gets the menuserviceso.
     *
     * @return the menuserviceso
     */
    public MenuService getMenuserviceso()
    {
        return menuserviceso;
    }

    /**
     * Sets the menuserviceso.
     *
     * @param menuserviceso the new menuserviceso
     */
    public void setMenuserviceso(MenuService menuserviceso)
    {
        this.menuserviceso = menuserviceso;
    }

}
