package fr.gouv.finances.cp.dmo.entite;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AdresseTest.class)
public abstract class AdresseTest_ {

	public static volatile SingularAttribute<AdresseTest, String> voie;
	public static volatile SingularAttribute<AdresseTest, String> ville;
	public static volatile SetAttribute<AdresseTest, PersonneTest> listePersonnes;
	public static volatile SingularAttribute<AdresseTest, Long> id;
	public static volatile SingularAttribute<AdresseTest, Integer> version;

}

