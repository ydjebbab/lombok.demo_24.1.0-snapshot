/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : AdresseContribuableDaoImpl.java
 *
 */

package fr.gouv.finances.cp.dmo.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IAdresseContribuableDao;
import fr.gouv.finances.lombok.adresse.bean.PaysIso;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;

/**
 * DAO AdresseContribuableDaoImpl
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Repository("adresseContribuableDao")
@Transactional(transactionManager="transactionManager")
public class AdresseContribuableDaoImpl extends BaseDaoJpaImpl implements IAdresseContribuableDao
{
    private static final Logger log = LoggerFactory.getLogger(AdresseContribuableDaoImpl.class);

    public AdresseContribuableDaoImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.IAdresseContribuableDao#loadPays()
     */
    @Override
    public List<PaysIso> loadPays()
    {
        log.debug(">>> Debut methode loadPays");
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<PaysIso> criteriaQuery = criteriabuilder.createQuery(PaysIso.class);

        List<PaysIso> paysIsoListeResultat = findAll(criteriaQuery);
        return paysIsoListeResultat;
    }

}
