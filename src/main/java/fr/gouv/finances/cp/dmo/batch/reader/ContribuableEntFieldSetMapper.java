/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.batch.reader;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import fr.gouv.finances.cp.dmo.entite.ContribuableEnt;

/**
 * Class ContribuableEntFieldSetMapper
 */
public class ContribuableEntFieldSetMapper implements FieldSetMapper<ContribuableEnt>
{

    public ContribuableEntFieldSetMapper()
    {
        super(); 
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.batch.item.file.mapping.FieldSetMapper#mapFieldSet(org.springframework.batch.item.file.transform.FieldSet)
     */
    @Override
    public ContribuableEnt mapFieldSet(FieldSet fieldSet) throws BindException
    {
        /*
         * 0id, 1contribuable_dis, 2version, 3identifiant, 4civi_id_civilite, 5sifa_id_situationfamiliale, 6adressemail,
         * 7nom, 8prenom, 9telephonefixe, 10telephoneportable, 11datedemiseajour, 12datedenaissance, 13fuseauhorairetel,
         * 14solde
         */
        ContribuableEnt contribuableent = new ContribuableEnt();
        contribuableent.setIdentifiant(fieldSet.readString(3));
        contribuableent.setNom(fieldSet.readString(7));
        contribuableent.setSolde(fieldSet.readDouble(14));
        contribuableent.setTelephoneFixe(fieldSet.readString(9));
        return contribuableent;
    }

}