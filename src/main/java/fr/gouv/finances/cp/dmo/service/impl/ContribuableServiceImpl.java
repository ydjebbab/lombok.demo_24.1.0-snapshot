/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;

import fr.gouv.finances.cp.dmo.dao.IContribuableDao;
import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuableEnt;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.DocumentJoint;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.cp.dmo.service.IDocumentJointService;
import fr.gouv.finances.cp.dmo.service.IReferenceService;
import fr.gouv.finances.cp.dmo.techbean.CriteresRecherches;
import fr.gouv.finances.lombok.journal.bean.IdentifiantsMetierObjOperation;
import fr.gouv.finances.lombok.journal.bean.ParametreOperation;
import fr.gouv.finances.lombok.journal.service.JournalService;
import fr.gouv.finances.lombok.mel.service.MelService;
import fr.gouv.finances.lombok.mel.techbean.Mel;
import fr.gouv.finances.lombok.mel.techbean.MelProgrammationException;
import fr.gouv.finances.lombok.mel.techbean.PieceJointe;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.upload.service.FichierJointService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.cle.ApplicationPropertiesUtil;
import fr.gouv.finances.lombok.util.exception.ApplicationException;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.exception.VerrouillageOptimisteSuppressionException;
import fr.gouv.finances.lombok.util.messages.Messages;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Class ContribuableServiceImpl
 *
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @author CF: passage XML vers annotations
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
@Service("contribuableService")
public class ContribuableServiceImpl extends BaseServiceImpl implements IContribuableService
{
    private static final Logger log = LoggerFactory.getLogger(ContribuableServiceImpl.class);

    @Autowired
    private IContribuableDao contribuableDao;

    @Autowired
    private IReferenceService referenceService;

    @Autowired
    private MelService serveurmel;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    @Qualifier("journalserviceimpl")
    private JournalService journalService;

    @Autowired
    @Qualifier("fichierjointserviceimpl")
    private FichierJointService fichierJointService;

    @Autowired
    private IDocumentJointService documentJointService;

    @Autowired
    private MessageSource messageSource;

    public ContribuableServiceImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param listeContribuables
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#actualiserListeContribuables(java.util.List)
     */
    @Override
    public List<Contribuable> actualiserListeContribuables(List<Contribuable> listeContribuables)
    {
        log.debug(">>> Debut methode actualiserListeContribuables");
        List<Contribuable> contribuableListe = new ArrayList<>();

        // CF : passage en Java 8
        listeContribuables.forEach(contribuable -> {
            ContribuablePar contribuableactuel = (ContribuablePar) contribuable;
            try
            {
                contribuableDao.refreshContribuableParticulier(contribuableactuel);
                contribuableDao.initializeListesRattacheesAUnContribuable(contribuableactuel);
                contribuableListe.add(contribuableactuel);
            }
            catch (DataAccessException exc)
            {
                ApplicationException aexc = ApplicationExceptionTransformateur.transformer(exc);

                // L'actualisation de la liste se poursuit même si un élément a été supprimé
                if (!(aexc instanceof VerrouillageOptimisteSuppressionException))
                {
                    throw aexc;
                }
            }
        });
        return contribuableListe;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param contribuablePar
     * @param typeImpot
     * @param contratMensu
     * @param referenceAvisImposition
     * @param identifiantUtilisateurOuBatch
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#adhererAlaMensualisation(fr.gouv.finances.cp.dmo.entite.ContribuablePar,
     *      fr.gouv.finances.cp.dmo.entite.TypeImpot, fr.gouv.finances.cp.dmo.entite.ContratMensu, java.lang.String,
     *      java.lang.String)
     */
    @Override
    public void adhererAlaMensualisation(final ContribuablePar contribuablePar, final TypeImpot typeImpot,
        final ContratMensu contratMensu, final String referenceAvisImposition, final String identifiantUtilisateurOuBatch)
    {
        log.debug(">>> Debut methode adhererAlaMensualisation");
        if (log.isDebugEnabled())
        {
            log.debug("adhesion contribuable identifiant :" + contribuablePar.getIdentifiant() + " id type impot :"
                + typeImpot.getId() + " annee prise effet contrat " + contratMensu.getAnneePriseEffet()
                + " reference avis imposition " + referenceAvisImposition + " identifiant utilisateur/batch "
                + identifiantUtilisateurOuBatch);
        }

        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                @Override
                public void doInTransactionWithoutResult(TransactionStatus status)
                {
                    typeImpot.verifierEstOuvertALaMensualisationRG10();
                    typeImpot.verifierEstDestineAuxparticuliersRG11();
                    contribuablePar.verifierPasDAdhesionEnCoursSurCeTypeDImpotPourCeContribuableRG12(typeImpot.getCodeImpot());
                    contribuablePar.verifierAvisImpositionEstAttacheAuContribuableRG14(referenceAvisImposition);
                    contribuablePar.rechercherAvisImpositionAttache(referenceAvisImposition)
                        .verifierAvisDImpositionEstAttacheAUnTypeImpotRG15(typeImpot.getCodeImpot());
                    contratMensu.getRib().verifierCleRib();
                    log.debug("Regles de gestion passees avec succes");

                    /*
                     * construction (ou reconstruction) des liens entre objets
                     */
                    contribuablePar.getListeContratMensu().add(contratMensu);
                    contratMensu.setContribuablePar(contribuablePar);
                    contratMensu.setTypeImpot(typeImpot);
                    contratMensu.setNumeroContrat(calculerNumeroContratAdhesionRG18(referenceAvisImposition));

                    log.debug("Liens entres objets et calcul numero contrat passe avec succes");

                    /*
                     * enregistrement de la demande d'adhesion
                     */
                    log.debug("contribuablePar : " + contribuablePar);
                    contribuableDao.modifyObject(contribuablePar);
                    // CF: version Hibernate
                    // contribuabledao.saveObject(uncontribuable);
                    log.debug("Sauvegarde contribuable et nouveau contrat attache passe avec succes");

                    /*
                     * Enregistrement dans le journal des opérations
                     */
                    Set<ParametreOperation> parametreOperationSet = new HashSet<>();

                    ParametreOperation paramOperTypeImpot = new ParametreOperation();
                    paramOperTypeImpot.setNom("Type impot");
                    paramOperTypeImpot.setValeur(typeImpot.getNomImpot());
                    paramOperTypeImpot.setNumIncrement(Long.valueOf(1));

                    ParametreOperation paramOperNumContratMensu = new ParametreOperation();
                    paramOperNumContratMensu.setNom("Numero Contrat Mensualisation");
                    paramOperNumContratMensu.setValeur(contratMensu.getNumeroContrat().toString());
                    paramOperNumContratMensu.setNumIncrement(Long.valueOf(2));

                    ParametreOperation paramOperRefAvisImpos = new ParametreOperation();
                    paramOperRefAvisImpos.setNom("Reference Avis Imposition");
                    paramOperRefAvisImpos.setValeur(referenceAvisImposition);
                    paramOperRefAvisImpos.setNumIncrement(Long.valueOf(3));

                    parametreOperationSet.add(paramOperTypeImpot);
                    parametreOperationSet.add(paramOperNumContratMensu);
                    parametreOperationSet.add(paramOperRefAvisImpos);

                    log.debug("Recherche d'avis d'imposition");
                    AvisImposition avisImposition = contribuableDao.findAvisImpositionParReference(referenceAvisImposition);

                    paramOperRefAvisImpos.setNom("Montant avis à mensualiser");
                    paramOperRefAvisImpos.setValeur(avisImposition.getMontant().toString());

                    IdentifiantsMetierObjOperation identifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
                    identifiantsMetierObjOperation.setLibelleIdentifiantMetier1("numero contribuable");
                    identifiantsMetierObjOperation.setValeurIdentifiantMetier1(contribuablePar.getIdentifiant());

                    log.debug("Ajout d'une operation au journal");
                    journalService.ajouterUneOperationAuJournal(identifiantUtilisateurOuBatch, parametreOperationSet,
                        "adhesion a la mensualisation", "identifiantStructure", "COURT",
                        identifiantsMetierObjOperation, "A", false);

                    log.debug("Mise à jour du journal passée avec succes");
                    envoyerMail(contratMensu, referenceAvisImposition);
                }
            });
        }
        // Intéret du bloc d'exception : identifier les exceptions
        // verrouillage optimiste
        // ajouter une info sur le contexte du plantage
        catch (RuntimeException exc)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur sur adhésion contribuable avec identifiant : "
                + identifiantUtilisateurOuBatch, exc);
        }
        log.debug("Commit transaction passe avec succes");
    }

    /* transaction gérée par spring sur le DAO */
    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param contribuable
     * @param adminid
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#ajouterUnContribuable(fr.gouv.finances.cp.dmo.entite.Contribuable,
     *      java.lang.String)
     */
    @Override
    public void ajouterUnContribuable(final Contribuable contribuable, final String adminid)
    {
        log.debug(">>> Debut methode ajouterUnContribuable");
        try
        {
            if (contribuable instanceof ContribuablePar)
            {
                final ContribuablePar contribuableParticulier = (ContribuablePar) contribuable;
                transactionTemplate.execute(new TransactionCallbackWithoutResult()
                {
                    @Override
                    public void doInTransactionWithoutResult(TransactionStatus status)
                    {
                        verifierIdentifiantContribuableNExistePasRG20(contribuableParticulier);
                        verifierUniciteNomPrenomAdressemailRG21(contribuableParticulier);
                        contribuableDao.saveObject(contribuableParticulier);

                        /*
                         * enregistrement dans le journal des opérations
                         */

                        Set<ParametreOperation> parametreOperationSet = new HashSet<ParametreOperation>();

                        ParametreOperation paramOperContribuable = new ParametreOperation();
                        paramOperContribuable.setNom("Identifiant contribuable particulier");
                        paramOperContribuable.setValeur(contribuableParticulier.getIdentifiant());
                        parametreOperationSet.add(paramOperContribuable);

                        IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
                        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("numero contribuable");
                        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1(contribuableParticulier.getIdentifiant());

                        journalService.ajouterUneOperationAuJournal(adminid, parametreOperationSet, "ajouter un contribuable",
                            "identifiantStructure", "COURT", unIdentifiantsMetierObjOperation, "A", false);
                    }
                });
            }
            else if (contribuable instanceof ContribuableEnt)
            {
                final ContribuableEnt contribuableEntreprise = (ContribuableEnt) contribuable;
                transactionTemplate.execute(new TransactionCallbackWithoutResult()
                {
                    @Override
                    public void doInTransactionWithoutResult(TransactionStatus status)
                    {
                        verifierIdentifiantContribuableNExistePasRG20(contribuableEntreprise);
                        contribuableDao.saveObject(contribuableEntreprise);

                        /*
                         * enregistrement dans le journal des opérations
                         */
                        Set<ParametreOperation> parametreOperationSet = new HashSet<>();

                        ParametreOperation paramOperContribuable = new ParametreOperation();
                        paramOperContribuable.setNom("Identifiant contribuable particulier");
                        paramOperContribuable.setValeur(contribuableEntreprise.getIdentifiant());

                        parametreOperationSet.add(paramOperContribuable);

                        IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation = new IdentifiantsMetierObjOperation();
                        unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("numero contribuable");
                        unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1(contribuableEntreprise.getIdentifiant());

                        journalService.ajouterUneOperationAuJournal(adminid, parametreOperationSet,
                            "ajouter un contribuable entreprise", "identifiantStructure", "COURT",
                            unIdentifiantsMetierObjOperation, "A", false);
                    }
                });
            }
        }
        // Intéret du bloc d'exception : identifier les exception
        // verrouillage optimiste
        // ajouter une info sur le contexte du plantage
        catch (RuntimeException exc)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur sur ajout du contribuable "
                + contribuable.getIdentifiant(), exc);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param contribuablePar
     * @param documentJoint
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#ajouterUnDocumentAuContribuable
     *      (fr.gouv.finances.cp.dmo.entite.ContribuablePar, fr.gouv.finances.cp.dmo.entite.DocumentJoint)
     */
    @Override
    public void ajouterUnDocumentAuContribuable(final ContribuablePar contribuablePar,
        final DocumentJoint documentJoint)
    {
        log.debug(">>> Debut methode ajouterUnDocumentAuContribuable");
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                @Override
                public void doInTransactionWithoutResult(TransactionStatus status)
                {
                    contribuablePar.getLesDocsJoints().add(documentJoint);
                    fichierJointService.sauvegarderUnFichierJoint(documentJoint.getLeFichierJoint());
                    contribuableDao.saveContribuableParticulier(contribuablePar);
                }
            });
        }
        catch (RuntimeException exc)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur sur la création des documents du contribuable " + contribuablePar.getIdentifiant(), exc);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param listetypesimpots
     * @return map
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#calculerAnneesPriseDEffetPossiblesRG13(java.util.List)
     */
    @Override
    public Map calculerAnneesPriseDEffetPossiblesRG13(List listetypesimpots)
    {
        log.debug(">>> Debut methode calculerAnneesPriseDEffetPossiblesRG13");
        Map<String, String> uneHashMap = new TreeMap<String, String>();
        uneHashMap.put("2005", "2005");
        uneHashMap.put("2006", "2006");
        return uneHashMap;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#changerLaCasseDeTousLesNomsDeContribuables()
     */
    @Override
    public int changerLaCasseDeTousLesNomsDeContribuables()
    {
        log.debug(">>> Debut methode changerLaCasseDeTousLesNomsDeContribuables");
        Object retour = null;
        try
        {
            retour = transactionTemplate.execute(new TransactionCallback<Object>()
            {
                @Override
                public Integer doInTransaction(TransactionStatus arg0)
                {
                    int iii = 0;
                    ScrollIterator structureIterator = contribuableDao.findTousContribuableParIterator(100);

                    while (structureIterator.hasNext())
                    {
                        ContribuablePar unContribuable = (ContribuablePar) structureIterator.nextObjetMetier();

                        // mise à jour pour la démo du nom de contribuable
                        if (unContribuable.getNom() != null)
                        {
                            if (iii == 0)
                            {
                                log.info("Lecture premier contribuable : " + unContribuable.getNom());
                            }
                            if (!unContribuable.getNom().equals(unContribuable.getNom().toUpperCase()))
                            {
                                unContribuable.setNom(unContribuable.getNom().toUpperCase());
                            }
                            else
                            {
                                unContribuable.setNom(unContribuable.getNom().toLowerCase());
                            }
                        }

                        contribuableDao.saveObject(unContribuable);

                        /*
                         * Le flush fait par lot sur une série de requetes préparées identiques est un facteur important de gain de
                         * performances pour la moteur de SGBD
                         */
                        iii++;
                        if (iii % 100 == 0)
                        {
                            contribuableDao.flush();
                            contribuableDao.clearPersistenceContext();
                            log.info("flush : " + iii);
                        }
                    }
                    log.info("nombre de contribuables traités : " + iii);
                    return iii;
                }
            });
        }
        catch (RuntimeException exc)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur lors du changement de la casse", exc);
        }
        return (Integer) retour;

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param contribuable
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#chargerLesDocumentsJoints(fr.gouv.finances.cp.dmo.entite.ContribuablePar)
     */
    @Override
    public void chargerLesDocumentsJoints(ContribuablePar contribuable)
    {
        log.debug(">>> Debut methode chargerLesDocumentsJoints");
        contribuableDao.initializeLesDocumentsJoints(contribuable);
    }

    /* transaction gérée par spring sur le DAO */
    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param contribuable
     * @param adminid
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#modifierUnContribuable(fr.gouv.finances.cp.dmo.entite.Contribuable,
     *      java.lang.String)
     */
    @Override
    public void modifierUnContribuable(final Contribuable contribuable, final String adminid)
    {
        log.debug(">>> Debut methode modifierUnContribuable");
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                public void doInTransactionWithoutResult(TransactionStatus status)
                {
                    verifierIdentifiantContribuableNExistePasRG20(contribuable);

                    // CF : version JPA
                    contribuableDao.modifyObject(contribuable);
                    // CF : version Hibernate
                    // contribuabledao.saveObject(contribuable);

                    /*
                     * enregistrement dans le journal des opérations
                     */

                    Set<ParametreOperation> parametreOperationSet = new HashSet<>();

                    if (contribuable instanceof ContribuablePar)
                    {
                        final ContribuablePar contribuableParticulier = (ContribuablePar) contribuable;
                        ParametreOperation paramOperContribuable = new ParametreOperation();

                        paramOperContribuable.setNom("Nom du contribuable");
                        paramOperContribuable.setValeur(contribuableParticulier.getNom());
                        paramOperContribuable.setNumIncrement(Long.valueOf(1));
                        parametreOperationSet.add(paramOperContribuable);

                        ParametreOperation paramOperContribuable1 = new ParametreOperation();
                        paramOperContribuable1.setNom("Prenom du contribuable");
                        paramOperContribuable1.setValeur(contribuableParticulier.getPrenom());
                        paramOperContribuable1.setNumIncrement(Long.valueOf(2));
                        parametreOperationSet.add(paramOperContribuable1);

                        ParametreOperation paramOperContribuable2 = new ParametreOperation();
                        paramOperContribuable2.setNom("Identifiant contribuable particulier");
                        paramOperContribuable2.setValeur(contribuableParticulier.getIdentifiant());
                        paramOperContribuable2.setNumIncrement(Long.valueOf(3));
                        parametreOperationSet.add(paramOperContribuable2);
                    }
                    IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation =
                        new IdentifiantsMetierObjOperation();
                    unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("numero contribuable");
                    unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1(contribuable.getIdentifiant());

                    journalService.ajouterUneOperationAuJournal(adminid, parametreOperationSet, "modifier un contribuable",
                        "identifiantStructure", "COURT", unIdentifiantsMetierObjOperation, "M", false);
                }
            });
        }
        // Intéret du bloc d'exception : identifier les exception
        // verrouillage optimiste
        // ajouter une info sur le contexte du plantage
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur sur modification du contribuable "
                + contribuable.getIdentifiant(), e);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param contribuablePar
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#normaliserAdresseContribuable(fr.gouv.finances.cp.dmo.entite.ContribuablePar)
     */
    @Override
    public void normaliserAdresseContribuable(ContribuablePar contribuablePar)
    {
        log.debug(">>> Debut methode normaliserAdresseContribuable");
        if (contribuablePar.getAdressePrincipale().getLigne3() != null
            && contribuablePar.getAdressePrincipale().getLigne3().length() > 0)
        {
            contribuablePar.getAdressePrincipale().setLigne3(normaliserChaine(contribuablePar.getAdressePrincipale().getLigne3()));
            contribuablePar.getAdressePrincipale().setLigne4(normaliserChaine(contribuablePar.getAdressePrincipale().getLigne4()));
            contribuablePar.getAdressePrincipale().setVille(normaliserChaine(contribuablePar.getAdressePrincipale().getVille()));
        }
    }

    /*
     * (non-Javadoc)
     * @see fr.gouv.finances.cp.dmo.service.ContribuableService#rechercherListeContribuable(fr.gouv.finances.cp.dmo.techbean
     * .CriteresRecherches)
     */
    /**
     * <pre>
     *  CF: passage en Java 8
     *  (methode de remplacement) {@inheritDoc}.
     * </pre>
     *
     * @param criteresRecherches
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#rechercherListeContribuable(fr.gouv.finances.cp.dmo.techbean.CriteresRecherches)
     */
    @Override
    public Optional<List<? extends Contribuable>> rechercherListeContribuable(final CriteresRecherches criteresRecherches)
    {
        log.debug(">>> Debut methode rechercherListeContribuable");
        // log.warn("Service appli.version " + messageSource.getMessage("appli.version", null, Locale.FRANCE));
        log.warn("Service appli.version " + ApplicationPropertiesUtil.getProperty("appli.version"));

        List<Contribuable> listeContribuables;
        if (criteresRecherches.getTypeRechercheId() != null
            && criteresRecherches.getTypeRechercheId().intValue() != CriteresRecherches.RECHERCHE_CONTRIBUABLE_PAR_AVIS_IMPOSITION)
        {
            listeContribuables = contribuableDao.findListContribuableViaCriteresRecherches(criteresRecherches);
        }
        else
        {
            listeContribuables = new ArrayList<>();
            ContribuablePar contribuablePar =
                contribuableDao
                    .findContribuableEtAvisImpositionEtContratsMensualisationParReferenceAvis(criteresRecherches.getReferenceAvis());
            if (contribuablePar != null)
            {
                listeContribuables.add(contribuablePar);
            }
        }
        return Optional.ofNullable(listeContribuables);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param contribuablePar
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#rechercherListeTypesImpotsDisponiblesALaMensualisationPourUnContribuable(fr.gouv.finances.cp.dmo.entite.ContribuablePar)
     */
    @Override
    public List<TypeImpot> rechercherListeTypesImpotsDisponiblesALaMensualisationPourUnContribuable(ContribuablePar contribuablePar)
    {
        log.debug(">>> Debut methode rechercherListeTypesImpotsDisponiblesALaMensualisationPourUnContribuable");
        List<TypeImpot> typesimpots = null;
        List<TypeImpot> typesimpotsdisponibles = null;

        typesimpots = referenceService.rechercherTypesImpotsDisponbilesALaMensualisation();

        if (typesimpots != null)
        {
            typesimpotsdisponibles =
                this.constituerListeTypesImpotsDisponiblesEtNonDejaMensualises(typesimpots, contribuablePar);
        }

        if (typesimpotsdisponibles == null || typesimpotsdisponibles.size() <= 0)
        {
            throw new RegleGestionException(Messages.getString("exception.aucunimpotdisponible"));

        }
        return typesimpotsdisponibles;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param referenceavisimposition
     * @return contribuable par
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParReferenceAvis(java.lang.String)
     */
    @Override
    public ContribuablePar rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParReferenceAvis(
        String referenceavisimposition)
    {
        log.debug(">>> Debut methode rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParReferenceAvis");
        return contribuableDao
            .findContribuableEtAvisImpositionEtContratsMensualisationParReferenceAvis(referenceavisimposition);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param identifiantutilisateur
     * @return contribuable par
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID(java.lang.String)
     */
    @Override
    public ContribuablePar rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID(
        String identifiantutilisateur)
    {
        log.debug(">>> Debut methode rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID");
        try
        {
            ContribuablePar contribuablepar =
                contribuableDao
                    .findContribuableEtAvisImpositionEtContratsMensualisationParUID(identifiantutilisateur);
            if (contribuablepar == null)
            {
                throw new RegleGestionException(Messages.getString("exception.contribuablenontrouve"));
            }
            else
            {
                return contribuablepar;
            }
        }
        // intéret du bloc d'exception : ajouter une information
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer("erreur lors de la recherche du contribuable : "
                + identifiantutilisateur, e);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param listecontribuableasupprimer
     * @param adminid
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#supprimerDesContribuablesParticuliers(java.util.List,
     *      java.lang.String)
     */
    @Override
    public void supprimerDesContribuablesParticuliers(final List listecontribuableasupprimer, final String adminid)
    {
        log.debug(">>> Debut methode supprimerDesContribuablesParticuliers");
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                @Override
                public void doInTransactionWithoutResult(TransactionStatus status)
                {
                    Contribuable uncontribuableasupprimer = null;
                    Iterator uniterator = listecontribuableasupprimer.iterator();

                    while (uniterator.hasNext())
                    {
                        uncontribuableasupprimer = (Contribuable) uniterator.next();
                        try
                        {
                            contribuableDao.deleteContribuable(uncontribuableasupprimer);

                            Set<ParametreOperation> lesParamOper = new HashSet<ParametreOperation>();
                            ParametreOperation paramOperContribuable = new ParametreOperation();

                            paramOperContribuable.setNom("Identifiant contribuable particulier");
                            paramOperContribuable.setValeur(uncontribuableasupprimer.getIdentifiant());
                            lesParamOper.add(paramOperContribuable);
                            IdentifiantsMetierObjOperation unIdentifiantsMetierObjOperation =
                                new IdentifiantsMetierObjOperation();
                            unIdentifiantsMetierObjOperation.setLibelleIdentifiantMetier1("numero contribuable");
                            unIdentifiantsMetierObjOperation.setValeurIdentifiantMetier1(uncontribuableasupprimer
                                .getIdentifiant());
                            journalService.ajouterUneOperationAuJournal(adminid, lesParamOper,
                                "supprimer un contribuable", "", "TRESCOURT", unIdentifiantsMetierObjOperation, "S",
                                false);

                        }
                        // ajouter une info sur le
                        // contexte du plantage
                        catch (RuntimeException e)
                        {
                            throw ApplicationExceptionTransformateur.transformer(
                                "Erreur sur suppression contribuable avec identifiant : "
                                    + uncontribuableasupprimer.getIdentifiant(),
                                e);
                        }

                    }
                }
            });
        }
        // Interêt du bloc d'exception : identifier les exceptions
        // verouillage optimiste
        // ajouter une info sur le contexte du plantage
        catch (RuntimeException exc)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur sur la validation de la transaction de suppression  de plusieurs contribuables ", exc);
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param contribuablePar
     * @param lesDocASupprimer
     * @see fr.gouv.finances.cp.dmo.service.IContribuableService#supprimerDesDocumentsDuContribuable(fr.gouv.finances.cp.dmo.entite.ContribuablePar,
     *      java.util.Collection)
     */
    @Override
    public void supprimerDesDocumentsDuContribuable(final ContribuablePar contribuablePar,
        final Collection lesDocASupprimer)
    {
        log.debug(">>> Debut methode supprimerDesDocumentsDuContribuable");
        try
        {
            transactionTemplate.execute(new TransactionCallbackWithoutResult()
            {
                @Override
                public void doInTransactionWithoutResult(TransactionStatus status)
                {
                    contribuablePar.getLesDocsJoints().removeAll(lesDocASupprimer);
                    List<FichierJoint> lesFichiersASupprimer = new ArrayList<>();
                    for (Iterator docsIter = lesDocASupprimer.iterator(); docsIter.hasNext();)
                    {
                        DocumentJoint unDocASupprimer = (DocumentJoint) docsIter.next();
                        lesFichiersASupprimer.add(unDocASupprimer.getLeFichierJoint());
                    }

                    contribuableDao.saveContribuableParticulier(contribuablePar);
                    documentJointService.supprimerLesDocumentsJoints(lesDocASupprimer);
                    fichierJointService.supprimerLesFichiersJoints(lesFichiersASupprimer);
                }
            });
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "Erreur sur la suppression des documents du contribuable " + contribuablePar.getIdentifiant(), e);
        }
    }

    /**
     * methode Verifier unicite nom prenom adressemail r g21
     *
     * @param contribuablePar
     */
    public void verifierUniciteNomPrenomAdressemailRG21(ContribuablePar contribuablePar)
    {
        log.debug(">>> Debut methode verifierUniciteNomPrenomAdressemailRG21");
        if (contribuableDao.findNomPrenomAdresseMailExisteDeja(contribuablePar))
        {
            throw new RegleGestionException(Messages.getString("exception.contribuable.nomprenomadressemailnonunique"));
        }
    }

    /**
     * methode Calculer types impots mensualisables actuellement
     *
     * @param listetypeimpotmensualisable param
     * @return list
     */
    protected List calculerTypesImpotsMensualisablesActuellement(List listetypeimpotmensualisable)
    {
        log.debug(">>> Debut methode calculerTypesImpotsMensualisablesActuellement");
        // TODO Ecrire le filtre
        return listetypeimpotmensualisable;
    }

    /**
     * methode Calculer numero contrat adhesion r g18 : DGFiP.
     *
     * @param referenceavisimposition param
     * @return long
     */
    private Long calculerNumeroContratAdhesionRG18(String referenceavisimposition)
    {
        log.debug(">>> Debut methode calculerNumeroContratAdhesionRG18");
        Date date = new Date();
        return (Long.valueOf(date.getTime() + Long.valueOf(referenceavisimposition).longValue()));
    }

    /**
     * methode Constituer liste types impots disponibles et non deja mensualises
     *
     * @param typesimpots  param
     * @param contribuable param
     * @return list< type impot>
     */
    private List<TypeImpot> constituerListeTypesImpotsDisponiblesEtNonDejaMensualises(List<TypeImpot> typesimpots,
        ContribuablePar contribuable)
    {
        log.debug(">>> Debut methode constituerListeTypesImpotsDisponiblesEtNonDejaMensualises");
        Iterator<TypeImpot> iter = typesimpots.iterator();
        TypeImpot untypeimpot;
        List<TypeImpot> listeTInondejamensu = new ArrayList<>();

        if (log.isDebugEnabled())
        {
            log.debug("Liste impots non mensualises pour contribuable : " + contribuable.getIdentifiant());
        }

        while (iter.hasNext())
        {
            untypeimpot = (TypeImpot) iter.next();

            if (log.isDebugEnabled())
            {
                log.debug("Id Type impot trouve : " + untypeimpot.getId());
            }

            try
            {
                contribuable.verifierPasDAdhesionEnCoursSurCeTypeDImpotPourCeContribuableRG12(untypeimpot
                    .getCodeImpot());
                listeTInondejamensu.add(untypeimpot);
                log.debug("impot ajoute à la liste");
            }
            catch (RegleGestionException regleGestionExc)
            {
                // Adhesion en cours : rien à faire
                log.warn("impot non ajoute à la liste", regleGestionExc);
            }
        }
        return listeTInondejamensu;
    }

    /**
     * methode Construire document confirmation adhesion
     *
     * @param reference param
     * @return input stream source
     */
    private InputStreamSource construireDocumentConfirmationAdhesion(String reference)
    {
        log.debug(">>> Debut methode construireDocumentConfirmationAdhesion");
        Document document = new Document();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try
        {
            PdfWriter.getInstance(document, baos);
            document.open();
            document.add(new Paragraph("Référence :"));
            document.add(new Paragraph(reference));
            document.close();
        }
        catch (DocumentException e)
        {
            throw new RegleGestionException(Messages.getString("exception.erreurcreationpdfconfirmationadhesion"), e);
        }

        InputStreamSource source = new InputStreamSource()
        {
            @Override
            public InputStream getInputStream()
            {
                return new ByteArrayInputStream(baos.toByteArray());
            }
        };
        return source;
    }

    /**
     * methode Envoyer mel confirmation
     *
     * @param adressemel            param
     * @param referenceconfirmation param
     */
    private void envoyerMailConfirmation(String adressemel, String referenceConfirmation)
    {
        log.debug(">>> Debut methode envoyerMailConfirmation");
        InputStreamSource source = this.construireDocumentConfirmationAdhesion(referenceConfirmation);
        Mel unmel = new Mel();

        unmel.ajouterDestinataireA(adressemel);
        unmel.setDe("demandeadhesion@cp.finances.gouv.fr");
        unmel.setObjet("confirmation demande d'adhésion");
        unmel.setMesssage("Votre demande d'adhesion est confirmée - voir la pièce jointe pour la référence");
        PieceJointe pJointe = new PieceJointe("Reference", source);
        unmel.ajouterPieceJointe(pJointe);
        serveurmel.envoyerMel(unmel);
    }

    /**
     * methode Normaliserchaine
     *
     * @param chaine param
     * @return string
     */
    private String normaliserChaine(String chaine)
    {
        log.debug(">>> Debut methode normaliserChaine");
        String rchaine = null;
        if (chaine != null && chaine.length() > 0)
        {
            // Transformation de la chaîne en majuscule
            StringBuilder tempo = new StringBuilder(chaine.toUpperCase(Locale.FRANCE).trim());

            // Les signes de ponctuation !"#$%&'(
            // )*+,-./:;<=>?@[\]^_`{|}~ et les caractères soulignés
            // sont remplacés
            // par un caractère blanc
            Pattern pat = Pattern.compile("\\p{Punct}");
            Matcher mat = pat.matcher(tempo);
            String str = mat.replaceAll(" ");

            tempo.delete(0, tempo.length());
            tempo.append(str);

            // Les caractères blancs consécutifs [\t\n\x0B\f\r] sont
            // remplacés par un seul caractère blanc
            pat = Pattern.compile("[\\s]{2,}");
            mat = pat.matcher(tempo);
            str = mat.replaceAll(" ");
            tempo.delete(0, tempo.length());
            tempo.append(str);

            rchaine = tempo.toString();
        }
        return rchaine;
    }

    /**
     * methode Verifier identifiant contribuable n existe pas r g20
     *
     * @param contribuable param
     */
    private void verifierIdentifiantContribuableNExistePasRG20(Contribuable contribuable)
    {
        log.debug(">>> Debut methode verifierIdentifiantContribuableNExistePasRG20");
        Long ident = contribuable.getId();
        String identifiant = contribuable.getIdentifiant();

        if (contribuableDao.findIndentifiantContribuableEstAffecte(ident, identifiant))
        {
            throw new RegleGestionException(Messages.getString("exception.contribuable.identifiantnonunique"));
        }
    }

    /**
     * Envoi du mail et PDF de confirmation
     */
    private void envoyerMail(final ContratMensu contratMensu, final String referenceavisimposition)
    {
        log.debug(">>> Debut methode envoyerMail");
        try
        {
            String adressemel = contratMensu.getContribuablePar().getAdresseMail();
            envoyerMailConfirmation(adressemel, referenceavisimposition);
            log.debug("envoi mail passé avec succes");
        }
        catch (MelProgrammationException melExc)
        {
            log.warn("L'envoi de mail a échoué", melExc);
        }
    }

    @Override
    public List<ContribuablePar> rechercherListeContribuables()
    {
        log.debug(">>> Debut methode rechercherListeContribuables");
        List<ContribuablePar> listeContribuables = contribuableDao.findAllContribuable();

        return listeContribuables;
    }

}
