/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementEditionsadressescontribuablesStub.java
 *
 */
package fr.gouv.finances.cp.dmo.zf2.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;

import fr.gouv.finances.cp.dmo.entite.AdresseContribuable;
import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.SituationFami;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunStubImpl;

/**
 * Class TraitementEditionsadressescontribuablesStub DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementEditionsadressescontribuablesStub extends AbstractServiceEditionCommunStubImpl
{

    /**
     * Instanciation de traitement editionsadressescontribuables stub.
     */
    public TraitementEditionsadressescontribuablesStub()
    {
        super();
    }

    /**
     * methode Creates the bean collection : DGFiP.
     * 
     * @return collection
     */
    public static Collection createBeanCollection()
    {
        List<Contribuable> list = new ArrayList<>();
        ContribuablePar uncontri;
        AdresseContribuable uneAdresse;
        SituationFami situationFamilialeMarie = new SituationFami();
        situationFamilialeMarie.setCode(1L);
        situationFamilialeMarie.setLibelle("Marié(e)");

        SituationFami situationFamilialeCelibataire = new SituationFami();
        situationFamilialeCelibataire.setCode(2L);
        situationFamilialeCelibataire.setLibelle("Célibataire");

        SituationFami situationFamilialeAutre = new SituationFami();
        situationFamilialeAutre.setCode(3L);
        situationFamilialeAutre.setLibelle("Autres");

        for (int i = 0; i < 60; i++)
        {
            uncontri = new ContribuablePar("usertest" + i + "-cp");
            uncontri.setNom("nom" + i);
            uncontri.setPrenom("prenom" + i);
            if (i % 3 == 0)
            {
                uncontri.setSituationFamiliale(situationFamilialeMarie);
            }
            else if (i % 2 == 0)
            {
                uncontri.setSituationFamiliale(situationFamilialeCelibataire);
            }
            else
            {
                uncontri.setSituationFamiliale(situationFamilialeAutre);
            }

            uncontri.setSolde(new Double(100 * i));
            uneAdresse = new AdresseContribuable();

            // Ajout d'adresses
            uneAdresse.setLigne2(i + " rue Auguste Blanqui");
            uneAdresse.setLigne3("Bâtiment n° " + i);
            uneAdresse.setLigne4("Bureau n° " + i);
            uneAdresse.setCodePostal("93000");
            uneAdresse.setVille("Montreuil");
            uneAdresse.setPays("FRANCE");
            // uncontri.getListeAdresses().add(uneAdresse);
            uncontri.getListeAdresses().add(null);

            uneAdresse = new AdresseContribuable();
            uneAdresse.setLigne2(i + " rue du petit Cuvier");
            uneAdresse.setLigne4("Chambre n° " + i);
            uneAdresse.setCodePostal("93000");
            uneAdresse.setVille("Montreuil");
            uneAdresse.setPays("FRANCE");
            // uncontri.getListeAdresses().add(uneAdresse);
            uncontri.getListeAdresses().add(null);

            uneAdresse = new AdresseContribuable();
            uneAdresse.setLigne2(i + " rue de bercy");
            uneAdresse.setLigne3("Bâtiment n° " + i);
            uneAdresse.setLigne4("Bureau n° " + i);
            uneAdresse.setCodePostal("93000");
            uneAdresse.setVille("Montreuil");
            uneAdresse.setPays("FRANCE");
            // uncontri.getListeAdresses().add(uneAdresse);
            uncontri.getListeAdresses().add(null);

            list.add(uncontri);

            // Tri des éléments avant édition
            List<Comparator> sortFields = new ArrayList<Comparator>();
            sortFields.add(new BeanComparator("situationFamiliale.code"));
            ComparatorChain multiSort = new ComparatorChain(sortFields);
            Collections.sort(list, multiSort);

        }
        return list;
    }

    /**
     * methode Creer nom du fichier : DGFiP.
     * 
     * @param parametresEdition param
     * @return string
     */
    public String creerNomDuFichier(Map parametresEdition)
    {
        return null;
    }

    /**
     * methode Creer parametres : DGFiP.
     * 
     * @param parametresEdition param
     * @return map
     */
    public Map creerParametres(Map parametresEdition)
    {
        return null;
    }

}
