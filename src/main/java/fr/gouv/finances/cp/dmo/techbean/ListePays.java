/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.techbean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.gouv.finances.lombok.adresse.bean.PaysIso;

/**
 * Class ListePays
 */
@XmlRootElement(name = "ISO_3166-1_List_fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ListePays
{

    /** liste pays. */
    @XmlElement(name = "ISO_3166-1_Entry")
    private List<PaysIso> listePays;

    public ListePays()
    {
        super();
    }

    /**
     * Accesseur de l attribut liste pays.
     *
     * @return liste pays
     */
    public List<PaysIso> getListePays()
    {
        return listePays;
    }

    /**
     * Modificateur de l attribut liste pays.
     *
     * @param listePays le nouveau liste pays
     */
    public void setListePays(List<PaysIso> listePays)
    {
        this.listePays = listePays;
    }

}