/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.ldap;

import javax.naming.Name;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.DnAttribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

import fr.gouv.finances.lombok.util.base.BaseBean;

/**
 * Class Abonne de type dgcp partenaire ; cad personne nominative on précise ici la hiérararchie dans le ldap exemple :
 * "dgcpPartenaire", "pkiUser", "top" ou "externeDgcp", "pkiUser", "top" base sert à la construction du dn pour les
 * recherches et les opérations de création supresssion.
 */
@Entry(objectClasses = {"dgcpPartenaire", "pkiUser", "top"}, base = "ou=divers-xt,ou=abonnes-dgcp-xt,ou=MEFI,o=gouv,c=fr")
public final class Abonne extends BaseBean
{
    private static final long serialVersionUID = 1L;

    @Id
    private Name dn;

    /** The uid. */
    // DnAttribute formera un dn dy type uid=***,ou=abonnes-dgcp-xt,ou=MEFI,o=gouv,c=fr
    @Attribute(name = "uid")
    @DnAttribute(value = "uid", index = 5)
    private String uid;

    /** The postal code. */
    @Attribute(name = "postalCode")
    private String postalCode;

    /** The postal address. */
    @Attribute(name = "postalAddress")
    private String postalAddress;

    /** The mail. */
    @Attribute(name = "mail")
    private String mail;

    /** The password. */
    @Attribute(name = "userPassword")
    private String password;

    // /** The object class names. */
    // attribut inutile tient compte de ObjectClass mis en annotation
    // @Attribute(name = "objectClass")
    // private List<String> objectClassNames = new ArrayList();

    /** The personal title. */
    @Attribute(name = "personalTitle")
    private String personalTitle;

    /** The sn. */
    @Attribute(name = "sn")
    private String sn;

    /** The given name. */
    @Attribute(name = "givenName")
    private String givenName;

    /** The population. */
    @Attribute(name = "population")
    private String population;

    /** The l. */
    @Attribute(name = "l")
    private String l;

    /** The c. */

    @Attribute(name = "c")
    private String c;

    /** The affectation. */
    @Attribute(name = "affectation")
    private String affectation;

    /** The create. */
    @Attribute(name = "create")
    private String create;

    public Abonne()
    {
        super();
    }

    /**
     * methode Equals : .
     *
     * @param obj the obj
     * @return true, if successful
     */
    @Override
    public boolean equals(Object obj)
    {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    /**
     * Gets the affectation.
     *
     * @return the affectation
     */
    public String getAffectation()
    {
        return affectation;
    }

    /**
     * Gets the c.
     *
     * @return the c
     */
    public String getC()
    {
        return c;
    }

    /**
     * Gets the create.
     *
     * @return the create
     */
    public String getCreate()
    {
        return create;
    }

    /**
     * Gets the dn.
     *
     * @return the dn
     */
    public Name getDn()
    {
        return dn;
    }

    /**
     * Gets the given name.
     *
     * @return the given name
     */
    public String getGivenName()
    {
        return givenName;
    }

    /**
     * Gets the l.
     *
     * @return the l
     */
    public String getL()
    {
        return l;
    }

    /**
     * Gets the mail.
     *
     * @return the mail
     */
    public String getMail()
    {
        return mail;
    }

    // public List<String> getObjectClassNames() {
    // return objectClassNames;
    // }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * Gets the personal title.
     *
     * @return the personal title
     */
    public String getPersonalTitle()
    {
        return personalTitle;
    }

    /**
     * Gets the population.
     *
     * @return the population
     */
    public String getPopulation()
    {
        return population;
    }

    /**
     * Gets the postal address.
     *
     * @return the postal address
     */
    public String getPostalAddress()
    {
        return postalAddress;
    }

    /**
     * Gets the postal code.
     *
     * @return the postal code
     */
    public String getPostalCode()
    {
        return postalCode;
    }

    /**
     * Gets the sn.
     *
     * @return the sn
     */
    public String getSn()
    {
        return sn;
    }

    /**
     * Gets the uid.
     *
     * @return the uid
     */
    public String getUid()
    {
        return uid;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /**
     * Sets the affectation.
     *
     * @param affectation the new affectation
     */
    public void setAffectation(String affectation)
    {
        this.affectation = affectation;
    }

    /**
     * Sets the c.
     *
     * @param c the new c
     */
    public void setC(String c)
    {
        this.c = c;
    }

    /**
     * Sets the create.
     *
     * @param create the new create
     */
    public void setCreate(String create)
    {
        this.create = create;
    }

    /**
     * Sets the dn.
     *
     * @param dn the new dn
     */
    public void setDn(Name dn)
    {
        this.dn = dn;
    }

    /**
     * Sets the given name.
     *
     * @param givenName the new given name
     */
    public void setGivenName(String givenName)
    {
        this.givenName = givenName;
    }

    /**
     * Sets the l.
     *
     * @param l the new l
     */
    public void setL(String l)
    {
        this.l = l;
    }

    /**
     * Sets the mail.
     *
     * @param mail the new mail
     */
    public void setMail(String mail)
    {
        this.mail = mail;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * Sets the personal title.
     *
     * @param personalTitle the new personal title
     */
    public void setPersonalTitle(String personalTitle)
    {
        this.personalTitle = personalTitle;
    }

    /**
     * Sets the population.
     *
     * @param population the new population
     */
    public void setPopulation(String population)
    {
        this.population = population;
    }

    /**
     * Sets the postal address.
     *
     * @param postalAddress the new postal address
     */
    public void setPostalAddress(String postalAddress)
    {
        this.postalAddress = postalAddress;
    }

    /**
     * Sets the postal code.
     *
     * @param postalCode the new postal code
     */
    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    /**
     * Sets the sn.
     *
     * @param sn the new sn
     */
    public void setSn(String sn)
    {
        this.sn = sn;
    }

    /**
     * Sets the uid.
     *
     * @param uid the new uid
     */
    public void setUid(String uid)
    {
        this.uid = uid;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder abonneStr =
            new StringBuilder("Abonne=[ uid = " + uid + ", postalCode = " + postalCode + ", postalAdress = " + postalAddress + ", ]");
        return abonneStr.toString();
    }
}