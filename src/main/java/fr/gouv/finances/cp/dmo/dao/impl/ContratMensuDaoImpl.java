/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */

package fr.gouv.finances.cp.dmo.dao.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CacheStoreMode;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IContratMensuDao;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.ContratMensu_;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.entite.TypeImpot_;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * DAO ContratMensuDaoImpl.
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.6 $ Date: 14 déc. 2009
 */
@Repository("contratMensuDao")
@Transactional(transactionManager="transactionManager")
public class ContratMensuDaoImpl extends BaseDaoJpaImpl implements IContratMensuDao
{
    private static final Logger log = LoggerFactory.getLogger(ContratMensuDaoImpl.class);

    private final Calendar cal = Calendar.getInstance();

    public ContratMensuDaoImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see fr.gouv.finances.cp.dmo.dao.IContratMensuDao#saveCommeTraiteContratsVerifiantRG70()
     */
    @Override
    public int saveCommeTraiteContratsVerifiantRG70()
    {
        log.debug(">>> Debut methode saveCommeTraiteContratsVerifiantRG70");
        Date dateLimite = ContratMensu.getDateLimiteValidite();

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaUpdate<ContratMensu> criteriaUpdate = criteriabuilder.createCriteriaUpdate(ContratMensu.class);
        Root<ContratMensu> contratMensuRoot = criteriaUpdate.from(ContratMensu.class);
        criteriaUpdate.set(contratMensuRoot.get(ContratMensu_.estTraite), true);

        Expression<Boolean> filterPredicateDateDemande =
            criteriabuilder.lessThan(contratMensuRoot.get(ContratMensu_.dateDemande), dateLimite);

        Expression<Boolean> filterPredicateEstTraite =
            criteriabuilder.equal(contratMensuRoot.get(ContratMensu_.estTraite), false);

        Expression<Boolean> filterPredicate = criteriabuilder.and(filterPredicateDateDemande, filterPredicateEstTraite);

        criteriaUpdate.where(filterPredicate);

        int nbContratsTraites = entityManager.createQuery(criteriaUpdate).executeUpdate();
        log.debug("nbContratsTraites :  " + nbContratsTraites);
        return nbContratsTraites;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param startRecord
     * @param endRecord
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.IContratMensuDao#findContratsNeVerifiantPasRG70(int, int)
     */
    @Override
    public List<ContratMensu> findContratsNeVerifiantPasRG70(final int startRecord, final int endRecord)
    {
        log.debug(">>> Debut methode findContratsNeVerifiantPasRG70");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContratMensu> criteriaQuery = criteriabuilder.createQuery(ContratMensu.class);
        Root<ContratMensu> contratMensuRoot = criteriaQuery.from(ContratMensu.class);
        contratMensuRoot.fetch(ContratMensu_.contribuablePar, JoinType.INNER);

        criteriaQuery.select(contratMensuRoot);

        Long annee = Long.valueOf(cal.get(Calendar.YEAR));

        Predicate predicate = criteriabuilder.conjunction();
        predicate =
            criteriabuilder.and(predicate, criteriabuilder.lessThanOrEqualTo(contratMensuRoot.get(ContratMensu_.anneePriseEffet), annee));
        predicate = criteriabuilder.and(predicate,
            criteriabuilder.lessThanOrEqualTo(contratMensuRoot.get(ContratMensu_.dateDemande), ContratMensu.getDateLimiteValidite()));
        criteriaQuery.where(predicate);

        // Pour debug seulement
        TypedQuery<ContratMensu> contratMensuTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("contratMensuTypedQuery : " + contratMensuTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<ContratMensu> contratsNeVerifiantPasRG70Liste =
            contratMensuTypedQuery.setFirstResult(startRecord).setMaxResults(endRecord).getResultList();
        log.debug("contratsNeVerifiantPasRG70Liste : " + contratsNeVerifiantPasRG70Liste);

        if (LOGGER.isDebugEnabled())
        {
            if (contratsNeVerifiantPasRG70Liste != null)
            {
                LOGGER.debug("Nombre de contrats récupérés ne verifiant pas RG70 : " + contratsNeVerifiantPasRG70Liste.size());
            }
            else
            {
                LOGGER.debug("Nombre de contrats récupérés ne verifiant pas RG70 : 0");
            }
        }
        return contratsNeVerifiantPasRG70Liste;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param startRecord
     * @param endRecord
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.IContratMensuDao#findContratsNonTraiteVerifiantRG70(int, int)
     */
    @Override
    public List<ContratMensu> findContratsNonTraiteVerifiantRG70(final int startRecord, final int endRecord)
    {
        log.debug(">>> Debut methode findContratsNonTraiteVerifiantRG70");
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContratMensu> criteriaQuery = criteriabuilder.createQuery(ContratMensu.class);
        Root<ContratMensu> contratMensuRoot = criteriaQuery.from(ContratMensu.class);
        contratMensuRoot.fetch(ContratMensu_.contribuablePar, JoinType.INNER);

        criteriaQuery.select(contratMensuRoot);

        Long annee = Long.valueOf(cal.get(Calendar.YEAR));

        Predicate predicate = criteriabuilder.conjunction();
        predicate =
            criteriabuilder.and(predicate, criteriabuilder.lessThanOrEqualTo(contratMensuRoot.get(ContratMensu_.anneePriseEffet), annee));
        predicate = criteriabuilder.and(predicate,
            criteriabuilder.greaterThanOrEqualTo(contratMensuRoot.get(ContratMensu_.dateDemande), ContratMensu.getDateLimiteValidite()));
        criteriaQuery.where(predicate);

        // Pour debug seulement
        TypedQuery<ContratMensu> contratMensuTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("contratMensuTypedQuery : " + contratMensuTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<ContratMensu> contratsVerifiantPasRG70Liste =
            contratMensuTypedQuery.setFirstResult(startRecord).setMaxResults(endRecord).getResultList();
        log.debug("contratsVerifiantPasRG70Liste : " + contratsVerifiantPasRG70Liste);

        if (LOGGER.isDebugEnabled())
        {
            if (contratsVerifiantPasRG70Liste != null)
            {
                LOGGER.debug("Nombre de contrats récupérés verifiant RG70 : " + contratsVerifiantPasRG70Liste.size());
            }
            else
            {
                LOGGER.debug("Nombre de contrats récupérés verifiant RG70 : 0");
            }
        }
        return contratsVerifiantPasRG70Liste;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param contribuablePar
     * @param typeimpot
     * @return contratpour contribuable et type impot
     * @see fr.gouv.finances.cp.dmo.dao.IContratMensuDao#findContratpourContribuableEtTypeImpot(fr.gouv.finances.cp.dmo.entite.ContribuablePar,
     *      fr.gouv.finances.cp.dmo.entite.TypeImpot)
     */
    @Override
    public ContratMensu findContratpourContribuableEtTypeImpot(ContribuablePar contribuablePar, TypeImpot typeimpot)
    {
        log.debug(">>> Debut methode findContratpourContribuableEtTypeImpot");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContratMensu> criteriaQuery = criteriabuilder.createQuery(ContratMensu.class);
        Root<ContratMensu> contratMensuRoot = criteriaQuery.from(ContratMensu.class);

        criteriaQuery.select(contratMensuRoot);

        Predicate predicate = criteriabuilder.conjunction();
        predicate =
            criteriabuilder.and(predicate, criteriabuilder.equal(contratMensuRoot.get(ContratMensu_.contribuablePar), contribuablePar));
        predicate = criteriabuilder.and(predicate,
            criteriabuilder.equal(contratMensuRoot.get(ContratMensu_.typeImpot), typeimpot));
        criteriaQuery.where(predicate);

        // Pour debug seulement
        TypedQuery<ContratMensu> contratMensuTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("contratMensuTypedQuery : " + contratMensuTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<ContratMensu> contratMensuListe = contratMensuTypedQuery.getResultList();
        log.debug("contratMensuListe : " + contratMensuListe);

        if (null != contratMensuListe && !contratMensuListe.isEmpty())
        {
            // On attend au maximum un contrat de mensualisation par
            // couple (typeimpot,contribuablepar)
            return (ContratMensu) contratMensuListe.get(0);
        }
        else
        {
            return null;
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return nombre contrats non traite ne verifiant pas r g70
     * @see fr.gouv.finances.cp.dmo.dao.IContratMensuDao#countNombreContratsNonTraiteNeVerifiantPasRG70()
     */
    @Override
    public int countNombreContratsNonTraiteNeVerifiantPasRG70()
    {
        log.debug(">>> Debut methode countNombreContratsNonTraiteNeVerifiantPasRG70");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriabuilder.createQuery(Long.class);
        Root<ContratMensu> contratMensuRoot = criteriaQuery.from(ContratMensu.class);

        Long annee = Long.valueOf(cal.get(Calendar.YEAR));
        Predicate predicate = criteriabuilder.conjunction();
        predicate =
            criteriabuilder.and(predicate, criteriabuilder.lessThanOrEqualTo(contratMensuRoot.get(ContratMensu_.anneePriseEffet), annee));
        predicate = criteriabuilder.and(predicate,
            criteriabuilder.lessThanOrEqualTo(contratMensuRoot.get(ContratMensu_.dateDemande), ContratMensu.getDateLimiteValidite()));
        criteriaQuery.where(predicate);

        criteriaQuery.select(criteriabuilder.count(contratMensuRoot));

        TypedQuery<Long> longTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("longTypedQuery : " + longTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        Long nombreContrats = longTypedQuery.getSingleResult();

        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Nombre de contrats ne verifiant pas RG70 : " + nombreContrats.intValue());
        }
        return nombreContrats.intValue();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return nombre contrats non traite verifiant r g70
     * @see fr.gouv.finances.cp.dmo.dao.IContratMensuDao#countNombreContratsNonTraiteVerifiantRG70()
     */
    @Override
    public int countNombreContratsNonTraiteVerifiantRG70()
    {
        log.debug(">>> Debut methode countNombreContratsNonTraiteVerifiantRG70");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriabuilder.createQuery(Long.class);
        Root<ContratMensu> contratMensuRoot = criteriaQuery.from(ContratMensu.class);

        Long annee = Long.valueOf(cal.get(Calendar.YEAR));
        Predicate predicate = criteriabuilder.conjunction();
        predicate =
            criteriabuilder.and(predicate, criteriabuilder.lessThanOrEqualTo(contratMensuRoot.get(ContratMensu_.anneePriseEffet), annee));
        predicate = criteriabuilder.and(predicate,
            criteriabuilder.greaterThanOrEqualTo(contratMensuRoot.get(ContratMensu_.dateDemande), ContratMensu.getDateLimiteValidite()));
        criteriaQuery.where(predicate);

        criteriaQuery.select(criteriabuilder.count(contratMensuRoot));

        TypedQuery<Long> longTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("longTypedQuery : " + longTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        Long nombreContrats = longTypedQuery.getSingleResult();

        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Nombre de contrats ne verifiant pas RG70 : " + nombreContrats.intValue());
        }
        return nombreContrats.intValue();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return integer
     * @see fr.gouv.finances.cp.dmo.dao.IContratMensuDao#reporterAnneePriseEffetPourContratsNonTraitesNeVerifiantPasRG71()
     */
    @Override
    public Integer reporterAnneePriseEffetPourContratsNonTraitesNeVerifiantPasRG71()
    {
        log.debug(">>> Debut methode reporterAnneePriseEffetPourContratsNonTraitesNeVerifiantPasRG71");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ContratMensu> criteriaQuery = criteriabuilder.createQuery(ContratMensu.class);
        Root<ContratMensu> contratMensuRoot = criteriaQuery.from(ContratMensu.class);

        Long annee = Long.valueOf(cal.get(Calendar.YEAR));
        Date aujourdhui = cal.getTime();
        // CF: pour test (renvoie 8 contrats)
        // Date aujourdhui = new GregorianCalendar(2001, Calendar.FEBRUARY, 11).getTime();

        Predicate predicate = criteriabuilder.conjunction();
        predicate =
            criteriabuilder.and(predicate, criteriabuilder.lessThan(contratMensuRoot.get(ContratMensu_.anneePriseEffet), annee));

        Path<TypeImpot> typeImpot = contratMensuRoot.get(ContratMensu_.typeImpot);
        Path<Date> jourLimiteAdhesion = typeImpot.get(TypeImpot_.jourLimiteAdhesion);
        predicate = criteriabuilder.and(predicate, criteriabuilder.greaterThan(jourLimiteAdhesion, aujourdhui));
        criteriaQuery.where(predicate);

        entityManager.setProperty("javax.persistence.cache.storeMode", CacheStoreMode.BYPASS);

        ScrollIterator scrollIterator = getScrollIterator(criteriaQuery, 0);

        int count = 0;
        while (scrollIterator.hasNext())
        {
            ++count;
            ContratMensu contratMensu = (ContratMensu) scrollIterator.nextObjetMetier();
            contratMensu.setAnneePriseEffet(Long.valueOf(contratMensu.getAnneePriseEffet().longValue() + 1L));
            // session.saveOrUpdate(element);
            modifyObject(contratMensu);
            contratMensu.setAnneePriseEffet(Long.valueOf(contratMensu.getAnneePriseEffet().longValue() + 1));
            // le flush et la libération de mémoire sont appelés tous les 20 enregistrements
            if (++count % 20 == 0)
            {
                flush();
                clearSession();
            }
        }
        return new Integer(count);
    }

    /**
     * methode Save contratmensualisation
     * 
     * @param contratMensu 
     */
    @Override
    public void saveContratMensualisation(final ContratMensu contratMensu)
    {
        log.debug(">>> Debut methode saveContratmensualisation");
        log.debug("Id de contratMensu : " + contratMensu.getId());

        modifyObject(contratMensu);
        // getHibernateTemplate().saveOrUpdate(contratMensu);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see fr.gouv.finances.cp.dmo.dao.IContratMensuDao#deleteContratsNeVerifiantPasRG70()
     */
    @Override
    public int deleteContratsNeVerifiantPasRG70()
    {
        log.debug(">>> Debut methode deleteContratsNeVerifiantPasRG70");
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaDelete<ContratMensu> criteriaDelete = criteriabuilder.createCriteriaDelete(ContratMensu.class);
        Root<ContratMensu> contratMensuRoot = criteriaDelete.from(ContratMensu.class);

        Expression<Boolean> filterPredicateDateDemande =
            criteriabuilder.lessThan(contratMensuRoot.get(ContratMensu_.dateDemande), ContratMensu.getDateLimiteValidite());

        Expression<Boolean> filterPredicateEstTraite =
            criteriabuilder.equal(contratMensuRoot.get(ContratMensu_.estTraite), false);

        Expression<Boolean> filterPredicate = criteriabuilder.and(filterPredicateDateDemande, filterPredicateEstTraite);

        criteriaDelete.where(filterPredicate);

        int nbContratsSupprimes = entityManager.createQuery(criteriaDelete).executeUpdate();
        if (LOGGER.isDebugEnabled())
        {
            LOGGER.debug("Nombre de contrats supprimés ne verifiant pas RG70 : " + nbContratsSupprimes);
        }
        return nbContratsSupprimes;
    }

}
