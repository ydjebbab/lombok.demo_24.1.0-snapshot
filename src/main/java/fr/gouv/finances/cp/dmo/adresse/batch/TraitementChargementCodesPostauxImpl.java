/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementChargementCodesPostauxImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.adresse.batch;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.dao.AdresseDao;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunParserTexteImpl;
import fr.gouv.finances.lombok.journal.dao.JournalDao;
import fr.gouv.finances.lombok.parsertexte.integration.GestionnaireImportationFichiers;
import fr.gouv.finances.lombok.parsertexte.integration.IntegrationException;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Classe TraitementChargementCodesPostauxImpl.java.
 * 
 * @author amleplatinec
 * @version $Revision: 1.5 $
 */
public class TraitementChargementCodesPostauxImpl extends ServiceBatchCommunParserTexteImpl
{
    int iii = 0;

    /** adressedao. */
    private AdresseDao adressedao;

    /** fichier codes postaux. */
    private String fichierCodesPostaux;

    /** transaction template. */
    private TransactionTemplate transactionTemplate;

    /** journaldao. */
    private JournalDao journaldao;

    public TraitementChargementCodesPostauxImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param pObjetsExtraits le objets extraits
     * @param pIdLigne le id ligne
     * @param pintNumLigne
     * @param pAnomalieDetecte le anomalie detecte
     * @param pContenuLigne le contenu ligne
     * @return int
     * @see fr.gouv.finances.lombok.parsertexte.integration.GestionnaireObjetExtrait#extractionObjet(java.util.Map,
     *      java.lang.String, int, boolean, java.lang.String)
     */
    @Override
    public int extractionObjet(Map pObjetsExtraits, String pIdLigne, int pintNumLigne, boolean pAnomalieDetecte,
        String pContenuLigne)
    {
        iii = iii + 1;
        Object unobjet = pObjetsExtraits.get("CodePostal");
        CodePostal uncodepostal;

        if (unobjet instanceof CodePostal)
        {
            uncodepostal = (CodePostal) unobjet;
            log.debug("Integration code postal :" + uncodepostal.getCode() + " " + uncodepostal.getVille());
            adressedao.saveObject(uncodepostal);
            /*
             * Set<ParametreOperation> lesParamOperation = new HashSet<ParametreOperation>(); ParametreOperation
             * paramOper = new ParametreOperation(); paramOper.setNom(uncodepostal.getVille());
             * paramOper.setValeur(uncodepostal.getCode()); lesParamOperation.add(paramOper);
             * IdentifiantsMetierObjOperation unIdentifiantsMetierObjOp = new IdentifiantsMetierObjOperation();
             * unIdentifiantsMetierObjOp.setLibelleIdentifiantMetier1("libelleIdentifiantMetier1");
             * unIdentifiantsMetierObjOp.setValeurIdentifiantMetier1("valeurIdentifiantMetier1"); OperationJournal
             * uneOperationJournal = new OperationJournal(); uneOperationJournal.setApurementPossible(true);
             * uneOperationJournal.setCodeOperation("ajout"); uneOperationJournal.setDateHeureOperation(new Date());
             * uneOperationJournal.setIdentifiantStructure(null);
             * uneOperationJournal.setIdentifiantUtilisateurOuBatch("batch" + uncodepostal.getCode() +
             * uncodepostal.getVille()); uneOperationJournal.setLesParamOperation(null);
             * uneOperationJournal.setNatureOperation("ajout d un code postal");
             * uneOperationJournal.setTypeDureeDeConservation("COURT");
             * uneOperationJournal.setUnIdentifiantsMetierObjOperation(null);
             * journaldao.saveUneNouvelleOperationAuJournal(uneOperationJournal);
             */

            if (iii % 30 == 0)
            {
                adressedao.flush();
                journaldao.flush();
                adressedao.clearPersistenceContext();
                journaldao.clearPersistenceContext();
            }
        }
        else
        {
            throw new ProgrammationException(
                "Erreur : le parseur de texte renvoie un type d'objet non attendu Attendu:CodePostal Renvoyé:"
                    + unobjet.getClass().getName());
        }

        return 0;
    }

    /**
     * Accesseur de l attribut adressedao.
     * 
     * @return adressedao
     */
    public AdresseDao getAdressedao()
    {
        return adressedao;
    }

    /**
     * Accesseur de l attribut fichier codes postaux.
     * 
     * @return fichier codes postaux
     */
    public String getFichierCodesPostaux()
    {
        return fichierCodesPostaux;
    }

    /**
     * Accesseur de l attribut journaldao.
     * 
     * @return journaldao
     */
    public JournalDao getJournaldao()
    {
        return journaldao;
    }

    /**
     * Accesseur de l attribut transaction template.
     * 
     * @return transaction template
     */
    public TransactionTemplate getTransactionTemplate()
    {
        return transactionTemplate;
    }

    /**
     * Modificateur de l attribut adressedao.
     * 
     * @param adressedao le nouveau adressedao
     */
    public void setAdressedao(AdresseDao adressedao)
    {
        this.adressedao = adressedao;
    }

    /**
     * Modificateur de l attribut fichier codes postaux.
     * 
     * @param fichierCodesPostaux le nouveau fichier codes postaux
     */
    public void setFichierCodesPostaux(String fichierCodesPostaux)
    {
        this.fichierCodesPostaux = fichierCodesPostaux;
    }

    /**
     * Modificateur de l attribut journaldao.
     * 
     * @param journaldao le nouveau journaldao
     */
    public void setJournaldao(JournalDao journaldao)
    {
        this.journaldao = journaldao;
    }

    /**
     * Modificateur de l attribut transaction template.
     * 
     * @param transactionTemplate le nouveau transaction template
     */
    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        final GestionnaireImportationFichiers unGestionnaireImportationFichiers =
            this.construireUnGestionnaireImportationFichiers();
        // astuce pour pouvoir référencer l'instance du batch dans le callback
        final TraitementChargementCodesPostauxImpl batchSelfRef = this;
        final File fichierCodesPostaux;

        try
        {
            fichierCodesPostaux = new File(new URL(this.fichierCodesPostaux).getFile());
        }
        catch (MalformedURLException e)
        {
            throw ApplicationExceptionTransformateur.transformer("l'url spécifiée est erronée", e);
        }

        transactionTemplate.execute(new TransactionCallbackWithoutResult()
        {
            @Override
            public void doInTransactionWithoutResult(TransactionStatus status)
            {
                adressedao.deleteTousLesCodesPostaux();
                adressedao.flush();

                try
                {
                    unGestionnaireImportationFichiers.lectureEtDecodageFichier(fichierCodesPostaux, batchSelfRef);
                }
                catch (IntegrationException e)
                {
                    throw ApplicationExceptionTransformateur.transformer(e);
                }
            }
        });
    }

}
