/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.techbean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.gouv.finances.cp.dmo.entite.Civilite;

/**
 * Class ListeCivilites
 */
@XmlRootElement(name = "civiliteliste")
@XmlAccessorType(XmlAccessType.FIELD)
public class ListeCivilites
{

    /** liste civilites. */
    @XmlElement(name = "entrycivilite")
    private List<Civilite> listeCivilites;

    public ListeCivilites()
    {
        super();
    }

    /**
     * Accesseur de l attribut liste civilites.
     *
     * @return liste civilites
     */
    public List<Civilite> getListeCivilites()
    {
        return listeCivilites;
    }

    /**
     * Modificateur de l attribut liste civilites.
     *
     * @param listeCivilites le nouveau liste civilites
     */
    public void setListeCivilites(List<Civilite> listeCivilites)
    {
        this.listeCivilites = listeCivilites;
    }

}
