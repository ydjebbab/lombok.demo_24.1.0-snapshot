/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.entite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;

/**
 * Entité OrigineTest
 *
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Entity
@Table(name = "ORIGINETEST_ORIT")
@SequenceGenerator(name = "ORIGINE_ID_GENERATOR", sequenceName = "SEQ_ORIGINETEST_ORIT", allocationSize = 1)
public class OrigineTest extends BaseEntity
{
    private static final int _31 = 31;

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORIGINE_ID_GENERATOR")
    @Column(name = "ORIGINE_ID")
    private Long id;

    @Column(name = "LIBELLE")
    private String libelle;

    public OrigineTest()
    {
        super();
    }

    public Long getId()
    {
        return id;
    }

    public String getLibelle()
    {
        return libelle;
    }


    public void setId(Long id)
    {
        this.id = id;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }


    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        OrigineTest other = (OrigineTest) obj;
        if (libelle == null)
        {
            if (other.libelle != null)
            {
                return false;
            }
        }
        else if (!libelle.equals(other.libelle))
        {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode()
    {
        final int prime = _31;
        int result = 1;
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        return result;
    }

}
