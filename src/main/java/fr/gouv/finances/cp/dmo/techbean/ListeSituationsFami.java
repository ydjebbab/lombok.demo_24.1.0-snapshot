/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.techbean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.gouv.finances.cp.dmo.entite.SituationFami;

/**
 * Class ListeSituationsFami
 */
@XmlRootElement(name = "situationfamiliste")
@XmlAccessorType(XmlAccessType.FIELD)
public class ListeSituationsFami
{

    /** liste situations fami. */
    @XmlElement(name = "entrysituationfami")
    private List<SituationFami> listeSituationsFami;

    public ListeSituationsFami()
    {
        super();
    }

    /**
     * Accesseur de l attribut liste situations fami.
     *
     * @return liste situations fami
     */
    public List<SituationFami> getListeSituationsFami()
    {
        return listeSituationsFami;
    }

    /**
     * Modificateur de l attribut liste situations fami.
     *
     * @param listeSituationsFami le nouveau liste situations fami
     */
    public void setListeSituationsFami(List<SituationFami> listeSituationsFami)
    {
        this.listeSituationsFami = listeSituationsFami;
    }

}
