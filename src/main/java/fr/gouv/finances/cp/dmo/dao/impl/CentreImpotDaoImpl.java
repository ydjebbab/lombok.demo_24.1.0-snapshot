package fr.gouv.finances.cp.dmo.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.ICentreImpotDao;
import fr.gouv.finances.cp.dmo.entite.CentreImpot;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;

/**
 * DAO pour la gestion del'entité centre des impôts
 * @author celinio fernandes
 * Date: Mar 11, 2020
 */
@Repository("centreImpotDao")
public class CentreImpotDaoImpl extends BaseDaoJpaImpl implements ICentreImpotDao
{
    private static final Logger log = LoggerFactory.getLogger(CentreImpotDaoImpl.class);

    public CentreImpotDaoImpl()
    {
        super();
    }

    @Override
    @Transactional(transactionManager = "transactionManager")
    public void add(CentreImpot centreImpot)
    {
        log.debug(">>> Debut methode add");
        saveObject(centreImpot);
    }

    @Override
    @Transactional(transactionManager = "transactionManager")
    public List<CentreImpot> findAll()
    {
        log.debug(">>> Debut methode findAll");

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<CentreImpot> criteriaQuery = criteriabuilder.createQuery(CentreImpot.class);
        Root<CentreImpot> centreImpotRoot = criteriaQuery.from(CentreImpot.class);

        criteriaQuery.select(centreImpotRoot);

        // Pour debug seulement
        TypedQuery<CentreImpot> centreImpotTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("centreImpotTypedQuery : " + centreImpotTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<CentreImpot> centreImpotListResultat = centreImpotTypedQuery.getResultList();
        // log.debug("centreImpotListResultat : " + centreImpotListResultat);
        return centreImpotListResultat;
    }

}
