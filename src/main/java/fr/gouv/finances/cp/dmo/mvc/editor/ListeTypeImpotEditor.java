/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.mvc.editor;

import java.beans.PropertyEditorSupport;
import java.util.Iterator;
import java.util.List;

import fr.gouv.finances.cp.dmo.entite.TypeImpot;

/**
 * Class ListeTypeImpotEditor 
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class ListeTypeImpotEditor extends PropertyEditorSupport
{
    private final List listetypesimpots;

    public ListeTypeImpotEditor(List listetypeimpots)
    {
        super();
        this.listetypesimpots = listetypeimpots;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.beans.PropertyEditorSupport#getAsText()
     */
    @Override
    public String getAsText()
    {
        String textid = "";
        TypeImpot unTypeImpot = (TypeImpot) getValue();

        if (unTypeImpot != null && unTypeImpot.getId() != null)
        {
            textid = unTypeImpot.getId().toString();
        }
        return textid;
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException
    {
        boolean trouve = false;
        Long ident = Long.valueOf(text);
        TypeImpot unTypeImpot;

        if (this.listetypesimpots != null)
        {
            Iterator iter = this.listetypesimpots.iterator();
            while (iter.hasNext() && trouve == false)
            {
                unTypeImpot = (TypeImpot) iter.next();
                if (unTypeImpot.getId().compareTo(ident) == 0)
                {
                    this.setValue(unTypeImpot);
                    trouve = true;
                }
            }
        }
        if (trouve == false)
        {
            this.setValue(null);
        }
    }

}
