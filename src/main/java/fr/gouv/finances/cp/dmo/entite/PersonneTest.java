/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.entite;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Entité PersonneTest
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Entity
@Table(name = "PERSONNETEST_PRST")
@SequenceGenerator(name = "PERSONNE_ID_GENERATOR", sequenceName = "SEQ_PERSONNETEST_PRST", allocationSize = 1)
public class PersonneTest extends BaseEntity
{
    private static final long serialVersionUID = 1833766807109760102L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PERSONNE_ID_GENERATOR")
    @Column(name = "PERSONNE_ID")
    private Long id;

    @Column(name = "NOM")
    private String nom;

    @Column(name = "PRENOM")
    private String prenom;

    @ManyToMany
    @JoinTable(name = "PERSONNEADRESSETEST_PADT", joinColumns = @JoinColumn(name = "PERSONNE_ID"), inverseJoinColumns = @JoinColumn(name = "ADRESSE_ID"))
    private Set<AdresseTest> listeAdresses = new HashSet<AdresseTest>();

    public PersonneTest()
    {
        super();
    }

    public PersonneTest(String nom, String prenom)
    {
        super();
        this.nom = nom;
        this.prenom = prenom;
    }

    /**
     * methode Detacher une adresse
     * 
     * @param adresse param
     */
    public void detacherUneAdresse(AdresseTest adresse)
    {
        adresse.getListePersonnes().remove(this);
        getListeAdresses().remove(adresse);
    }

    public Long getId()
    {
        return id;
    }

    public Set<AdresseTest> getListeAdresses()
    {
        return listeAdresses;
    }

    public String getNom()
    {
        return nom;
    }

    public String getPrenom()
    {
        return prenom;
    }

    /**
     * Verifie si adresse rattachee.
     * 
     * @param adresse param
     * @return true, si c'est adresse rattachee
     */
    public boolean isAdresseRattachee(AdresseTest adresse)
    {
        return getListeAdresses().contains(adresse);
    }

    /**
     * methode Rattacher une nouvelle adresse
     * 
     * @param adresse param
     */
    public void rattacherUneNouvelleAdresse(AdresseTest adresse)
    {
        if (adresse != null)
        {
            adresse.getListePersonnes().add(this);
            getListeAdresses().add(adresse);
        }
        else
        {
            throw new ProgrammationException("erreur.programmation");
        }
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setListeAdresses(Set<AdresseTest> listeAdresses)
    {
        this.listeAdresses = listeAdresses;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param obj
     * @return true, si c'est vrai
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final PersonneTest other = (PersonneTest) obj;
        if (nom == null)
        {
            if (other.nom != null)
            {
                return false;
            }
        }
        else if (!nom.equals(other.nom))
        {
            return false;
        }
        if (prenom == null)
        {
            if (other.prenom != null)
            {
                return false;
            }
        }
        else if (!prenom.equals(other.prenom))
        {
            return false;
        }
        return true;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        final int prime = hashCODEHASHMULT;
        int result = 1;

        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
        return result;
    }
}
