/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;
import java.util.List;

import fr.gouv.finances.lombok.adresse.bean.Departement;
import fr.gouv.finances.lombok.structure.bean.StructureCP;
import fr.gouv.finances.lombok.structure.techbean.CriteresRechercheStructureCP;

/**
 * Class RechercheLibelleStructureForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class RechercheLibelleStructureForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** unestructurearechercher. */
    private StructureCP unestructurearechercher = new StructureCP();

    /** une structure selectionnee. */
    private StructureCP uneStructureSelectionnee;

    /** codes categories structure cp. */
    private List<String> codesCategoriesStructureCP;

    /** departements. */
    private List<Departement> departements;

    /** codes annexes. */
    private List<String> codesAnnexes;

    /** departement. */
    private Departement departement;

    /** criteres recherche. */
    private CriteresRechercheStructureCP criteresRecherche;

    /**
     * Constructeur de la classe RechercheLibelleStructureForm.java
     */
    public RechercheLibelleStructureForm()
    {
        super(); // Raccord de constructeur auto-généré

    }

    /**
     * Gets the codes annexes.
     *
     * @return the codes annexes
     */
    public List<String> getCodesAnnexes()
    {
        return codesAnnexes;
    }

    /**
     * Gets the codes categories structure cp.
     *
     * @return the codes categories structure cp
     */
    public List<String> getCodesCategoriesStructureCP()
    {
        return codesCategoriesStructureCP;
    }

    /**
     * Gets the criteres recherche.
     *
     * @return the criteres recherche
     */
    public CriteresRechercheStructureCP getCriteresRecherche()
    {
        return criteresRecherche;
    }

    /**
     * Gets the departement.
     *
     * @return the departement
     */
    public Departement getDepartement()
    {
        return departement;
    }

    /**
     * Gets the departements.
     *
     * @return the departements
     */
    public List<Departement> getDepartements()
    {
        return departements;
    }

    /**
     * Gets the unestructurearechercher.
     *
     * @return the unestructurearechercher
     */
    public StructureCP getUnestructurearechercher()
    {
        return unestructurearechercher;
    }

    /**
     * Gets the une structure selectionnee.
     *
     * @return the une structure selectionnee
     */
    public StructureCP getUneStructureSelectionnee()
    {
        return uneStructureSelectionnee;
    }

    /**
     * Sets the codes annexes.
     *
     * @param codesAnnexes the new codes annexes
     */
    public void setCodesAnnexes(List<String> codesAnnexes)
    {
        this.codesAnnexes = codesAnnexes;
    }

    /**
     * Sets the codes categories structure cp.
     *
     * @param codesCategoriesStructureCP the new codes categories structure cp
     */
    public void setCodesCategoriesStructureCP(List<String> codesCategoriesStructureCP)
    {
        this.codesCategoriesStructureCP = codesCategoriesStructureCP;
    }

    /**
     * Sets the criteres recherche.
     *
     * @param criteresRecherche the new criteres recherche
     */
    public void setCriteresRecherche(CriteresRechercheStructureCP criteresRecherche)
    {
        this.criteresRecherche = criteresRecherche;
    }

    /**
     * Sets the departement.
     *
     * @param departement the new departement
     */
    public void setDepartement(Departement departement)
    {
        this.departement = departement;
    }

    /**
     * Sets the departements.
     *
     * @param departements the new departements
     */
    public void setDepartements(List<Departement> departements)
    {
        this.departements = departements;
    }

    /**
     * Sets the unestructurearechercher.
     *
     * @param unestructurearechercher the new unestructurearechercher
     */
    public void setUnestructurearechercher(StructureCP unestructurearechercher)
    {
        this.unestructurearechercher = unestructurearechercher;
    }

    /**
     * Sets the une structure selectionnee.
     *
     * @param uneStructureSelectionnee the new une structure selectionnee
     */
    public void setUneStructureSelectionnee(StructureCP uneStructureSelectionnee)
    {
        this.uneStructureSelectionnee = uneStructureSelectionnee;
    }

}
