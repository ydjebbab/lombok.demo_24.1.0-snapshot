/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : RechercheCodePostalValidator.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.validator;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.dmo.mvc.form.RechercheCodePostalForm;

/**
 * Class RechercheCodePostalValidator DGFiP.
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class RechercheCodePostalValidator implements Validator
{

    /**
     * Instanciation de recherche code postal validator.
     */
    public RechercheCodePostalValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    public boolean supports(Class clazz)
    {
        return clazz.equals(RechercheCodePostalForm.class);

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param arg0
     * @param arg1
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    public void validate(Object arg0, Errors arg1)
    {

    }

    /**
     * methode Validate surface recherche code postal : DGFiP.
     *
     * @param command param
     * @param errors param
     */
    public void validateSurfaceRechercheCodePostal(RechercheCodePostalForm command, Errors errors)
    {
        // Critères de recherche invalides
        // Vous devez saisir au moins :
        // 2 caractères pour le code postal et 1 pour la commune
        // ou 3 pour le code postal
        // ou 3 pour la commune
        boolean cond1 = false;
        boolean cond2 = false;
        boolean cond3 = false;

        // 3 caractères pour le code postal
        cond1 =
            ((!GenericValidator.isBlankOrNull(command.getCritere().getCode()))
                && GenericValidator.isInt(command.getCritere().getCode()) && GenericValidator.minLength(command
                    .getCritere().getCode(), 3));

        // 3 caractères pour la commune
        cond2 =
            ((!GenericValidator.isBlankOrNull(command.getCritere().getVille())) && GenericValidator.minLength(command
                .getCritere().getVille(), 3));

        // 2 caractères pour le code postal et 1 pour la commune
        cond3 =
            ((!GenericValidator.isBlankOrNull(command.getCritere().getCode()))
                && (!GenericValidator.isBlankOrNull(command.getCritere().getVille()))
                && GenericValidator.isInt(command.getCritere().getCode())
                && GenericValidator.minLength(command.getCritere().getCode(), 2) && GenericValidator.minLength(command
                    .getCritere().getVille(), 1));

        if ((!cond1) && (!cond2) && (!cond3))
        {
            errors.rejectValue("critere.code", "rejected.criteres", "CRITERES DE RECHERCHE NON VALIDES."
                + "Vous devez saisir au moins : 2 caractères pour le code postal et 1 pour la commune"
                + "ou 3 caractères pour le code postal" + "ou 3 caractères pour la commune");
        }

    }

}
