/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.batch;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;

import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;

/* La plupart du code de branchement sur Spring Batch peut être rendu générique par exemple dans une classe SimpleTraitementSpringBatch */
/**
 * Batch TraitementTestSpringBatchImpl .
 */
public class TraitementTestSpringBatchImpl extends
    ServiceBatchCommunImpl
{

    /** joblauncher. */
    private JobLauncher joblauncher;

    /** job. */
    private Job job;

    /**
     * Constructeur 
     */
    public TraitementTestSpringBatchImpl()
    {
        super(); 

    }

    /**
     * Modificateur de l attribut job.
     *
     * @param job le nouveau job
     */
    public void setJob(Job job)
    {
        this.job = job;
    }

    /**
     * Modificateur de l attribut joblauncher.
     *
     * @param joblauncher le nouveau joblauncher
     */
    public void setJoblauncher(
        JobLauncher joblauncher)
    {
        this.joblauncher = joblauncher;
    }

    /**
     * methode Mapper arguments sur job parameters : .
     *
     * @return job parameters
     */
    private JobParameters mapperArgumentsSurJobParameters()
    {

        Map<String, JobParameter> mapParameters = new HashMap<>();
        Iterator argumentsIterator = this.getArguments().entrySet().iterator();
        while (argumentsIterator.hasNext())
        {
            Map.Entry argument = (Map.Entry) argumentsIterator.next();
            mapParameters.put(argument.getKey().toString(), new JobParameter(argument.getValue().toString()));
        }
        return new JobParameters(mapParameters);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {

        JobParameters parametres = mapperArgumentsSurJobParameters();
        JobExecution execution = null;
        try
        {
            execution = joblauncher.run(job, parametres);
        }
        catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
            | JobParametersInvalidException e)
        {
            ApplicationExceptionTransformateur.transformer(e);
        }

        if (execution != null && execution.getExitStatus() != null)
        {
            // Il faut probablement améliorer le mapping exitstatus/exceptions
            if (("COMPLETED".compareToIgnoreCase(execution.getExitStatus().getExitCode())) != 0)
                throw new RuntimeException(execution.getExitStatus().getExitDescription());
        }
    }

}
