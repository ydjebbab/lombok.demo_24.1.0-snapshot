package fr.gouv.finances.cp.dmo.rest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.rest.dto.ContribuableParDto;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Services REST pour la gestion des contribuables
 * 
 * @author celinio fernandes Date: Mar 4, 2020
 */
@Path("/contribuables")
@Api(value = "Contribuable REST", produces = MediaType.APPLICATION_JSON)
public class ContribuableController
{
    private static final Logger log = LoggerFactory.getLogger(ContribuableController.class);

    @Autowired
    IContribuableService contribuableService;

    @GET
    @Path("/tous")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Recherche des contribuables", httpMethod = "GET", notes = "Recherche des contribuables", response = Response.class)
    @ApiResponses(value = {
            // @ApiResponse(code = 201, message = "Demande valide. Formalités et dispositions trouvées."),
            @ApiResponse(code = 400, message = "Contribuable non trouvé"),
            @ApiResponse(code = 500, message = "Erreur interne")})
    public Response findListContribuablePar()
    {
        log.debug(">>> Debut methode findListContribuablePar");
        Response response;

        List<ContribuableParDto> listeContribuableDto = new ArrayList<>();

        // TODO : créer un DTO pour l'entité contribuable
        List<ContribuablePar> listeContribuable = contribuableService.rechercherListeContribuables();
        // TODO : utiliser dozer à la place d'une boucle
        for (ContribuablePar contribuablePar : listeContribuable)
        {
            ContribuableParDto contribuableParDto = new ContribuableParDto();
            contribuableParDto.setNom(contribuablePar.getNom());
            contribuableParDto.setPrenom(contribuablePar.getPrenom());
            contribuableParDto.setAdresseMail(contribuablePar.getAdresseMail());
            listeContribuableDto.add(contribuableParDto);
        }

        response = Response.status(Status.OK).entity(listeContribuableDto).build();
        return response;
    }

}
