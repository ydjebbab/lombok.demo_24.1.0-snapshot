/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 * Copyright 2002-2005 the original author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*
 *
 * fichier : MultiFormatDateEditor.java
 *
 */

package fr.gouv.finances.cp.dmo.mvc.editor;

import java.beans.PropertyEditorSupport;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.util.StringUtils;

/**
 * PropertyEditor for <code>java.util.Date</code>, supporting a custom <code>java.text.DateFormat</code>
 * <p>
 * In web MVC code, this editor will typically be registered with <code>binder.registerCustomEditor</code> calls in an
 * implementation of BaseCommandController's <code>initBinder</code> method.
 *
 * @author Juergen Hoeller
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 * @see java.util.Date
 * @see java.text.DateFormat
 * @see org.springframework.validation.DataBinder#registerCustomEditor
 * @since 28.04.2003
 */
public class MultiFormatDateEditor extends PropertyEditorSupport
{
    private final DateFormat dateFormat;

    private final List datesFormats;

    private final boolean allowEmpty;

    private final int exactDateLength;

    private String parseError = null;

    /**
     * Create a new MultiFormatDateEditor instance, using the given DateFormat for parsing and rendering.
     * <p>
     * The "allowEmpty" parameter states if an empty String should be allowed for parsing, i.e. get interpreted as null
     * value. Otherwise, an IllegalArgumentException gets thrown in that case.
     * 
     * @param dateFormat DateFormat to use for parsing and rendering
     * @param allowEmpty if empty strings should be allowed
     */
    public MultiFormatDateEditor(DateFormat dateFormat, boolean allowEmpty)
    {
        super();
        this.datesFormats = null;
        this.dateFormat = dateFormat;
        this.allowEmpty = allowEmpty;
        this.exactDateLength = -1;
    }

    /**
     * Create a new MultiFormatDateEditor instance, using the given DateFormat for parsing and rendering.
     * <p>
     * The "allowEmpty" parameter states if an empty String should be allowed for parsing, i.e. get interpreted as null
     * value. Otherwise, an IllegalArgumentException gets thrown in that case.
     * <p>
     * The "exactDateLength" parameter states that IllegalArgumentException gets thrown if the String does not exactly
     * match the length specified. This is useful because SimpleDateFormat does not enforce strict parsing of the year
     * part, not even with <code>setLenient(false)</code>. Without an "exactDateLength" specified, the "01/01/05" would
     * get parsed to "01/01/0005".
     * 
     * @param dateFormat DateFormat to use for parsing and rendering
     * @param allowEmpty if empty strings should be allowed
     * @param exactDateLength the exact expected length of the date String
     */
    public MultiFormatDateEditor(DateFormat dateFormat, boolean allowEmpty, int exactDateLength)
    {
        super();
        this.dateFormat = dateFormat;
        this.datesFormats = null;
        this.allowEmpty = allowEmpty;
        this.exactDateLength = exactDateLength;
    }

    /**
     * Instanciation de multi format date editor.
     * 
     * @param datesFormats param
     * @param allowEmpty param
     * @param exactDateLength param
     */
    public MultiFormatDateEditor(List datesFormats, boolean allowEmpty, int exactDateLength)
    {
        super();
        this.dateFormat = null;
        this.datesFormats = datesFormats;
        this.allowEmpty = allowEmpty;
        this.exactDateLength = exactDateLength;
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see java.beans.PropertyEditorSupport#getAsText()
     */
    @Override
    public String getAsText()
    {
        Date value = (Date) getValue();
        if (this.dateFormat != null)
        {
            return (value != null ? this.dateFormat.format(value) : "");
        }
        else if (this.datesFormats != null)
        {
            return (value != null ? ((DateFormat) this.datesFormats.get(0)).format(value) : "");
        }
        else
        {
            return "";
        }
    }

    /** 
     * (methode de remplacement)
     * {@inheritDoc}
     * @see java.beans.PropertyEditorSupport#setAsText(java.lang.String)
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException
    {
        boolean rcf = false;
        if (this.allowEmpty && !StringUtils.hasText(text))
        {
            // Treat empty String as null value.
            setValue(null);
            rcf = true;
        }
        else if (text != null && this.exactDateLength >= 0 && text.length() != this.exactDateLength)
        {
            throw new IllegalArgumentException("Could not parse date: it is not exactly" + this.exactDateLength
                + "characters long");
        }
        else
        {
            // Liste de formats fournis en entrée
            if (this.datesFormats != null)
            {
                for (Iterator iterDatesFormats = this.datesFormats.iterator(); iterDatesFormats.hasNext();)
                {
                    DateFormat dateFormatToTry = (DateFormat) iterDatesFormats.next();
                    rcf = tryToParse(text, dateFormatToTry);
                    if (rcf)
                    {
                        break;
                    }
                }
            }
            else if (this.dateFormat != null)
            // Un format fourni en entrée
            {
                rcf = tryToParse(text, this.dateFormat);
            }
        }

        if (!rcf)
        {
            throw new IllegalArgumentException("Could not parse date: " + this.parseError);
        }

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param value le nouveau value
     * @see java.beans.PropertyEditorSupport#setValue(java.lang.Object)
     */
    @Override
    public void setValue(Object value)
    {
        super.setValue(value);
    }

    /**
     * methode Try to parse : DGFiP.
     * 
     * @param text param
     * @param dateFormatToTry param
     * @return true, si c'est vrai
     */
    private boolean tryToParse(String text, DateFormat dateFormatToTry)
    {
        try
        {
            setValue(dateFormatToTry.parse(text));
        }
        catch (ParseException ex)
        {
            this.parseError = this.parseError + ex.getMessage();
            return false;
        }
        return true;
    }

}
