/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.interceptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.lombok.apptags.table.bean.Row;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.interceptor.RowInterceptor;

/**
 * Class TypeImpotRowInterceptor
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class TypeImpotRowInterceptor implements RowInterceptor
{

    /** defaultStyle - String, style par défaut de la ligne. */
    private String defaultStyle;

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * Constructeur
     */
    public TypeImpotRowInterceptor()
    {
        super();

    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tableModel
     * @param row
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.RowInterceptor#addRowAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Row)
     */
    @Override
    public void addRowAttributes(TableModel tableModel, Row row)
    {
        // Aucun attribut à ajouter
    }

    /**
     * Accesseur du style par défaut de la colonne
     * 
     * @return le style par défaut de la colonne
     */
    public String getDefaultStyle()
    {
        return defaultStyle;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param model
     * @param row
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.RowInterceptor#modifyRowAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Row)
     */
    @Override
    public void modifyRowAttributes(TableModel model, Row row)
    {
        // Lecture du bean de la ligne courante
        Object rowBean = model.getCurrentRowBean();

        if (rowBean instanceof TypeImpot)
        {
            Double seuil = (Double) model.getContext().getRequestAttribute("LimiteSeuilTypeImpot");
            StringBuffer newStyle = new StringBuffer();

            // Si le style par défaut est déjà mémorisé, on l'ajoute au nouveau style
            if (getDefaultStyle() != null)
            {
                newStyle.append(getDefaultStyle());
            }

            // Sinon, on mémorise le style par défaut, s'il existe, et on l'ajoute au nouveau style
            else if (row.getStyle() != null)
            {
                setDefaultStyle(row.getStyle());
                newStyle.append(getDefaultStyle());
            }

            // S'il n'existe pas de style par défaut, on l'initialise avec une chaîne vide
            else
            {
                setDefaultStyle("");
            }

            if (((TypeImpot) rowBean).getSeuil() != null
                && ((TypeImpot) rowBean).getSeuil().doubleValue() > seuil.doubleValue())
            {
                newStyle.append(";color:red;");
            }
            else
            {
                newStyle.append(";color:black;");
            }
            row.setStyle(newStyle.toString());
        }
        else
        {
            log.warn("Cet intercepteur doit être utilisé avec des objets de type TypeImpot.");
        }
    }

    /**
     * Modificateur du style par défaut de la colonne
     * 
     * @param defaultStyle le nouveau style par défaut de la colonne
     */
    public void setDefaultStyle(String defaultStyle)
    {
        this.defaultStyle = defaultStyle;
    }

}
