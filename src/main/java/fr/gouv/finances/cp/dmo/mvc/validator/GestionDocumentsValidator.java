/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
*
 *
 * fichier : GestionDocumentsValidator.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.validator;

import org.apache.commons.lang.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.dmo.entite.DocumentJoint;
import fr.gouv.finances.cp.dmo.mvc.form.GestionDocumentsForm;

/**
 * Class GestionDocumentsValidator DGFiP.
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class GestionDocumentsValidator implements Validator
{

    /**
     * Instanciation de gestion documents validator.
     */
    public GestionDocumentsValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    public boolean supports(Class clazz)
    {
        return clazz.equals(GestionDocumentsForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param arg0
     * @param arg1
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    public void validate(Object arg0, Errors arg1)
    {
    }

    /**
     * methode Validate surface document : DGFiP.
     *
     * @param command param
     * @param errors param
     */
    public void validateSurfaceDocument(GestionDocumentsForm command, Errors errors)
    {

        DocumentJoint documentJoint = command.getDocumentJoint();

        if (documentJoint == null || documentJoint.getLeFichierJoint() == null
            || documentJoint.getLeFichierJoint().getTailleFichier() == 0)
        {
            errors.reject("documentjoint.vide", "Aucun fichier à ajouter ou fichier vide");
        }

        if (command.getDocumentJoint() == null || StringUtils.isBlank(command.getDocumentJoint().getIdentifiant()))
        {
            errors.rejectValue("documentJoint.identifiant", "rejected.documentJoint.identifiant",
                "Le champ \"identifiant\" est obligatoire");
        }
    }

}
