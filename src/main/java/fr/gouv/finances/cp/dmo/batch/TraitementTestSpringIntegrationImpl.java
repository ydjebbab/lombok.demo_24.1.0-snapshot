/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.batch;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;

/**
 * Batch TraitementTestSpringIntegrationImpl .
 */
@Service("traitementtestspringintegration")
@Profile("batch")
@Lazy(true)
public class TraitementTestSpringIntegrationImpl extends ServiceBatchCommunImpl
{

    /**
     * Constructeur
     */
    public TraitementTestSpringIntegrationImpl()
    {
        super();
    }

    @Value("${traitementtestspringintegration.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch)
    {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementtestspringintegration.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch)
    {
        super.setNomBatch(nomBatch);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch()
    {
        // correspondant aux conditions d'arrêt du process Spring Integration
        try
        {
            log.info("Appuyez sur la touche entrée pour terminer le processus");
            System.in.read();
        }
        catch (IOException exc)
        {
            log.error("Erreur d'entrée/sortie", exc);
        }
    }

}
