/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ReferenceDaoImpl.java
 *
 */

package fr.gouv.finances.cp.dmo.dao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IReferenceDao;
import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.AvisImposition_;
import fr.gouv.finances.cp.dmo.entite.Civilite;
import fr.gouv.finances.cp.dmo.entite.Civilite_;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.ContratMensu_;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.entite.TypeImpot_;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpaImpl;
import fr.gouv.finances.lombok.util.persistance.ModeCritereRecherche;

/**
 * DAO ReferenceDaoImpl
 * 
 * @author chouard-cp
 * @author CF : passage vers JPA
 * @version $Revision: 1.6 $ Date: 11 déc. 2009
 */
@Repository("referenceDao")
@Transactional(transactionManager="transactionManager")
public class ReferenceDaoImpl extends BaseDaoJpaImpl implements IReferenceDao
{
    private static final Logger log = LoggerFactory.getLogger(ReferenceDaoImpl.class);

    public ReferenceDaoImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see fr.gouv.finances.cp.dmo.dao.IReferenceDao#clear()
     */
    @Override
    public void clear()
    {
        clearSession();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param code
     * @return civilite
     * @see fr.gouv.finances.cp.dmo.dao.IReferenceDao#findCiviliteParCode(java.lang.Long)
     */
    @Override
    public Civilite findCiviliteParCode(Long code)
    {
        log.debug(">>> Debut methode findCiviliteParCode");
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Civilite> criteriaQuery = criteriabuilder.createQuery(Civilite.class);
        Root<Civilite> civiliteRoot = criteriaQuery.from(Civilite.class);

        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(civiliteRoot.get(Civilite_.code), code));
        criteriaQuery.where(predicate);

        criteriaQuery.select(civiliteRoot);

        // Pour debug seulement
        TypedQuery<Civilite> civiliteTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("civiliteTypedQuery : " + civiliteTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<Civilite> civiliteListeResultat = civiliteTypedQuery.getResultList();
        log.debug("civiliteListeResultat : " + civiliteListeResultat);

        if (null != civiliteListeResultat && !civiliteListeResultat.isEmpty())
        {
            Civilite civilite = civiliteListeResultat.get(0);
            return civilite;
        }
        else
        {
            return null;
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param code
     * @return type impot
     * @see fr.gouv.finances.cp.dmo.dao.IReferenceDao#findTypeImpotParCodeImpot(java.lang.Long)
     */
    @Override
    public TypeImpot findTypeImpotParCodeImpot(Long code)
    {
        log.debug(">>> Debut methode findTypeImpotParCodeImpot");
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TypeImpot> criteriaQuery = criteriabuilder.createQuery(TypeImpot.class);
        Root<TypeImpot> typeImpotRoot = criteriaQuery.from(TypeImpot.class);

        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(typeImpotRoot.get(TypeImpot_.codeImpot), code));
        criteriaQuery.where(predicate);

        criteriaQuery.select(typeImpotRoot);

        // Pour debug seulement
        TypedQuery<TypeImpot> typeImpotTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("typeImpotTypedQuery : " + typeImpotTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<TypeImpot> typeImpotListeResultat = typeImpotTypedQuery.getResultList();
        log.debug("typeImpotListeResultat : " + typeImpotListeResultat);

        if (null != typeImpotListeResultat && !typeImpotListeResultat.isEmpty())
        {
            TypeImpot typeImpot = typeImpotListeResultat.get(0);
            return typeImpot;
        }
        else
        {
            return null;
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.dao.IReferenceDao#findTypesImpotsMensualisables()
     */
    @Override
    public List<TypeImpot> findTypesImpotsMensualisables()
    {
        log.debug(">>> Debut methode findTypesImpotsMensualisables");
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<TypeImpot> criteriaQuery = criteriabuilder.createQuery(TypeImpot.class);
        Root<TypeImpot> typeImpotRoot = criteriaQuery.from(TypeImpot.class);

        criteriaQuery.select(typeImpotRoot);

        // Pour debug seulement
        TypedQuery<TypeImpot> typeImpotTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("typeImpotTypedQuery : " + typeImpotTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        List<TypeImpot> typeImpotListeResultat = typeImpotTypedQuery.getResultList();
        log.debug("typeImpotListeResultat : " + typeImpotListeResultat);
        return typeImpotListeResultat;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param civilite
     * @return true, si c'est civilite utilise
     * @see fr.gouv.finances.cp.dmo.dao.IReferenceDao#isCiviliteUtilise(fr.gouv.finances.cp.dmo.entite.Civilite)
     */
    @Override
    public boolean isCiviliteUtilise(Civilite civilite)
    {
        log.debug(">>> Debut methode isCiviliteUtilise");
        boolean civiliteUtiliseParContribuable = false;
        Map<String, String> criteres = new HashMap<>();
        criteres.put("civilite", "civilite");

        long total = countTotalNumberOfLines(Civilite.class, criteres, ModeCritereRecherche.EQUAL);
        if (total > 0)
        {
            civiliteUtiliseParContribuable = true;
        }

        return civiliteUtiliseParContribuable;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param civilite
     * @return true, si c'est code civilite deja utilise
     * @see fr.gouv.finances.cp.dmo.dao.IReferenceDao#isCodeCiviliteDejaUtilise(fr.gouv.finances.cp.dmo.entite.Civilite)
     */
    @Override
    public boolean isCodeCiviliteDejaUtilise(Civilite civilite)
    {
        log.debug(">>> Debut methode isCodeCiviliteDejaUtilise");
        boolean codeCiviliteDejaUtilise = false;

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriabuilder.createQuery(Long.class);
        Root<Civilite> civiliteRoot = criteriaQuery.from(Civilite.class);

        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(civiliteRoot.get(Civilite_.code), civilite.getCode()));
        if (null != civilite.getId())
        {
            predicate = criteriabuilder.and(predicate, criteriabuilder.notEqual(civiliteRoot.get(Civilite_.id), civilite.getId()));
        }
        criteriaQuery.where(predicate);
        criteriaQuery.select(criteriabuilder.count(civiliteRoot));

        // Pour debug seulement
        TypedQuery<Long> longTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("longTypedQuery : " + longTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        long total = longTypedQuery.getSingleResult();
        log.debug("total : " + total);

        if (total > 0)
        {
            codeCiviliteDejaUtilise = true;
        }

        return codeCiviliteDejaUtilise;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param typeImpot
     * @return true, si c'est code type impot deja utilise
     * @see fr.gouv.finances.cp.dmo.dao.IReferenceDao#isCodeTypeImpotDejaUtilise(fr.gouv.finances.cp.dmo.entite.TypeImpot)
     */
    @Override
    public boolean isCodeTypeImpotDejaUtilise(TypeImpot typeImpot)
    {
        log.debug(">>> Debut methode isCodeTypeImpotDejaUtilise");

        boolean codeDejaUtilise = false;

        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriabuilder.createQuery(Long.class);
        Root<TypeImpot> typeImpotRoot = criteriaQuery.from(TypeImpot.class);

        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(typeImpotRoot.get(TypeImpot_.codeImpot), typeImpot.getCodeImpot()));
        if (null != typeImpot.getId())
        {
            predicate = criteriabuilder.and(predicate, criteriabuilder.notEqual(typeImpotRoot.get(TypeImpot_.id), typeImpot.getId()));
        }
        criteriaQuery.where(predicate);
        criteriaQuery.select(criteriabuilder.count(typeImpotRoot));

        // Pour debug seulement
        TypedQuery<Long> longTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("longTypedQuery : " + longTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        long total = longTypedQuery.getSingleResult();
        log.debug("total : " + total);

        if (total > 0)
        {
            codeDejaUtilise = true;
        }
        return codeDejaUtilise;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param typeImpot
     * @return true, si c'est type impot utilise
     * @see fr.gouv.finances.cp.dmo.dao.IReferenceDao#isTypeImpotUtilise(fr.gouv.finances.cp.dmo.entite.TypeImpot)
     */
    @Override
    public boolean isTypeImpotUtilise(TypeImpot typeImpot)
    {
        log.debug(">>> Debut methode isTypeImpotUtilise");
        boolean typeImpotUtiliseParAvisImposition = false;
        boolean typeImpotUtiliseParContratMensu = false;
        
        // Recherche sur les avis d'imposition
        CriteriaBuilder criteriabuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriabuilder.createQuery(Long.class);
        Root<AvisImposition> avisImpositionRoot = criteriaQuery.from(AvisImposition.class);

        Predicate predicate = criteriabuilder.conjunction();
        predicate = criteriabuilder.and(predicate, criteriabuilder.equal(avisImpositionRoot.get(AvisImposition_.typeImpot), typeImpot));
        criteriaQuery.where(predicate);
        criteriaQuery.select(criteriabuilder.count(avisImpositionRoot));

        // Pour debug seulement
        TypedQuery<Long> longTypedQuery = entityManager.createQuery(criteriaQuery);
        log.debug("longTypedQuery : " + longTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        long total = longTypedQuery.getSingleResult();
        if (total > 0)
        {
            typeImpotUtiliseParAvisImposition = true;
        }
        
        // Recherche sur les contrats mensualisation        
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<ContratMensu> contratMensuRoot = cq.from(ContratMensu.class);

        Predicate pred = cb.conjunction();
        pred = cb.and(pred, cb.equal(contratMensuRoot.get(ContratMensu_.typeImpot), typeImpot));
        cq.where(pred);
        cq.select(cb.count(contratMensuRoot));

        // Pour debug seulement
        TypedQuery<Long> longContratMensuTypedQuery = entityManager.createQuery(cq);
        log.debug("longContratMensuTypedQuery : " + longContratMensuTypedQuery.unwrap(org.hibernate.Query.class).getQueryString());

        long totalMensu = longContratMensuTypedQuery.getSingleResult();
        if (totalMensu > 0)
        {
            typeImpotUtiliseParContratMensu = true;
        }        
        return typeImpotUtiliseParAvisImposition || typeImpotUtiliseParContratMensu;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param civilite
     * @see fr.gouv.finances.cp.dmo.dao.IReferenceDao#saveCivilite(fr.gouv.finances.cp.dmo.entite.Civilite)
     */
    @Override
    public void saveCivilite(Civilite civilite)
    {
        log.debug(">>> Debut methode saveCivilite");
        log.debug("Type impot : " + civilite.getLibelle());
        modifyObject(civilite);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param typeImpot
     * @see fr.gouv.finances.cp.dmo.dao.IReferenceDao#saveTypeImpot(fr.gouv.finances.cp.dmo.entite.TypeImpot)
     */
    @Override
    public void saveTypeImpot(TypeImpot typeImpot)
    {
        log.debug(">>> Debut methode saveTypeImpot");
        log.debug("Type impot : " + typeImpot.getNomImpot());
        modifyObject(typeImpot);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param elementsAsupprimer
     * @see fr.gouv.finances.cp.dmo.dao.IReferenceDao#deleteTypesImpots(java.util.Collection)
     */
    @Override
    public void deleteTypesImpots(Collection elementsAsupprimer)
    {
        log.debug(">>> Debut methode deleteTypesImpots");
        elementsAsupprimer.forEach(this::deleteObject);
    }

}
