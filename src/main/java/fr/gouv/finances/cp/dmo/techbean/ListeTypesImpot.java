/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.techbean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.gouv.finances.cp.dmo.entite.TypeImpot;

/**
 * Class ListeTypesImpot
 */
@XmlRootElement(name = "typeimpotliste")
@XmlAccessorType(XmlAccessType.FIELD)
public class ListeTypesImpot
{

    /** liste types impot. */
    @XmlElement(name = "entry")
    private List<TypeImpot> listeTypesImpot;

    public ListeTypesImpot()
    {
        super();
    }

    /**
     * Accesseur de l attribut liste types impot.
     *
     * @return liste types impot
     */
    public List<TypeImpot> getListeTypesImpot()
    {
        return listeTypesImpot;
    }

    /**
     * Modificateur de l attribut liste types impot.
     *
     * @param listeTypesImpot le nouveau liste types impot
     */
    public void setListeTypesImpot(List<TypeImpot> listeTypesImpot)
    {
        this.listeTypesImpot = listeTypesImpot;
    }

}