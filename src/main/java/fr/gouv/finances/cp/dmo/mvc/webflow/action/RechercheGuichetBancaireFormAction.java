/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.cp.dmo.entite.Rib;
import fr.gouv.finances.cp.dmo.mvc.form.RechercheGuichetBancaireForm;
import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;
import fr.gouv.finances.lombok.bancaire.service.GuichetBancaireService;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Action RechercheGuichetBancaireFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class RechercheGuichetBancaireFormAction extends FormAction
{
    private static final Logger log = LoggerFactory.getLogger(RechercheGuichetBancaireFormAction.class);

    private GuichetBancaireService guichetbancaireserviceso;

    public RechercheGuichetBancaireFormAction()
    {
        super();
    }

    public RechercheGuichetBancaireFormAction(Class<?> formObjectClass)
    {
        super(formObjectClass);
    }

    public GuichetBancaireService getGuichetbancaireserviceso()
    {
        return guichetbancaireserviceso;
    }

    public void setGuichetbancaireserviceso(GuichetBancaireService guichetbancaireserviceso)
    {
        this.guichetbancaireserviceso = guichetbancaireserviceso;
    }

    /**
     * methode Valider verifier existence code guichet code etablissement
     * 
     * @param context param
     * @return event
     */
    public Event validerVerifierExistenceCodeGuichetCodeEtablissement(RequestContext context)
    {
        RechercheGuichetBancaireForm form =
            (RechercheGuichetBancaireForm) context.getFlowScope().getRequired(getFormObjectName(),
                RechercheGuichetBancaireForm.class);

        Rib unRibSaisi = form.getRib();
        try
        {
            List lesGuichetsBancaires =
                this.guichetbancaireserviceso.verifierExistenceGuichetBancaire(unRibSaisi.getCodeGuichet(), unRibSaisi
                    .getCodeBanque());
            form.setUnguichetbancairearechercher((GuichetBancaire) (lesGuichetsBancaires.get(0)));
        }
        // cas ou aucun guichet bancaire n'est trouvé
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rgExc.getMessage());
            return error();
        }
        return success();
    }

}
