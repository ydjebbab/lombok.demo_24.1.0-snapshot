/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.mvc.form.GestionAvisImpositionForm;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.cp.dmo.service.IReferenceService;
import fr.gouv.finances.lombok.webflow.FormStatePersister;
import fr.gouv.finances.lombok.webflow.MementoFormStatePersister;

/**
 * Action GestionAvisImpositionFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class GestionAvisImpositionFormAction extends FormAction
{
    private static final Logger log = LoggerFactory.getLogger(GestionAvisImpositionFormAction.class);

    /** contribuableserviceso. */
    private IContribuableService contribuableserviceso;

    /** referenceserviceso. */
    private IReferenceService referenceserviceso;

    /**
     * Constructeur
     */
    public GestionAvisImpositionFormAction()
    {
        super();
    }

    /**
     * Constructeur
     *
     * @param formObjectClass
     */
    public GestionAvisImpositionFormAction(Class<?> formObjectClass)
    {
        super(formObjectClass);
    }

    /**
     * Gets the contribuableserviceso.
     *
     * @return the contribuableserviceso
     */
    public IContribuableService getContribuableserviceso()
    {
        return contribuableserviceso;
    }

    /**
     * Gets the referenceserviceso.
     *
     * @return the referenceserviceso
     */
    public IReferenceService getReferenceserviceso()
    {
        return referenceserviceso;
    }

    /**
     * Sets the contribuableserviceso.
     *
     * @param contribuableserviceso the new contribuableserviceso
     */
    public void setContribuableserviceso(IContribuableService contribuableserviceso)
    {
        this.contribuableserviceso = contribuableserviceso;
    }

    /**
     * Sets the referenceserviceso.
     *
     * @param referenceserviceso the new referenceserviceso
     */
    public void setReferenceserviceso(IReferenceService referenceserviceso)
    {
        this.referenceserviceso = referenceserviceso;
    }

    /**
     * methode Inits the form pour contribuable
     * 
     * @param request param
     * @return event
     */
    public Event initFormPourContribuable(RequestContext request)
    {
        log.debug(">>> Debut methode initFormPourContribuable");
        // Lecture du formulaire
        GestionAvisImpositionForm form = (GestionAvisImpositionForm) request.getFlowScope().get(getFormObjectName());

        // lecture du paramètre en entrée du flux
        ContribuablePar contribuable = (ContribuablePar) request.getFlowScope().get("contribuable");

        // Enregistrement dans le contexte de flux
        form.setContribuable(contribuable);

        // Sauvegarde de l'état initial du formulaire
        FormStatePersister mgo = new MementoFormStatePersister();
        mgo.saveState(form, request, this, MementoFormStatePersister.LEVEL1);

        return success();
    }

}
