/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 *
 *
 * fichier : ContratMensuServiceImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.service.impl;

import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.dao.IContratMensuDao;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.service.IContratMensuService;
import fr.gouv.finances.lombok.mel.service.MelService;
import fr.gouv.finances.lombok.mel.techbean.Mel;
import fr.gouv.finances.lombok.mel.techbean.MelExploitationException;
import fr.gouv.finances.lombok.mel.techbean.MelRegleGestionException;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Service ContratMensuServiceImpl
 * 
 * @author chouard-cp
 * @author CF: passage XML vers annotations
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Service("contratMensuService")
public class ContratMensuServiceImpl extends BaseServiceImpl implements IContratMensuService
{
    private static final Logger log = LoggerFactory.getLogger(ContratMensuServiceImpl.class);

    @Autowired
    private IContratMensuDao contratMensuDao;

    @Autowired
    private MelService serveurmel;

    public ContratMensuServiceImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return document
     * @see fr.gouv.finances.cp.dmo.service.IContratMensuService#recupererRapportXmlContratsNonTraiteVerifiantRG70()
     */
    @Override
    public Document recupererRapportXmlContratsNonTraiteVerifiantRG70()
    {

        // TODO Evolution à prévoir: utiliser un fichier temporaire
        // plutôt qu'un document qui pourrait avoir une taille trop
        // importante

        int nbTotalContratsATraiter = contratMensuDao.countNombreContratsNonTraiteVerifiantRG70();
        int nbContratsParPage = 2;
        int nbPagesTotal = nbTotalContratsATraiter / nbContratsParPage;

        log.info("Traitement des contrats vérifiant RG70 par page de " + nbContratsParPage + " pour un total de "
            + nbTotalContratsATraiter);

        Document doc = DocumentHelper.createDocument();
        doc.setXMLEncoding("UTF-8");
        Element rootxmlcontrat = doc.addElement("Contrats");

        List<ContratMensu> pageContrats = null;
        int nbContratsTraitesXml = 0;

        for (int pageCourante = 0; pageCourante <= nbPagesTotal; pageCourante++)
        {
            pageContrats =
                contratMensuDao.findContratsNonTraiteVerifiantRG70(pageCourante * nbContratsParPage, nbContratsParPage);
            if (pageContrats != null)
            {
                for (ContratMensu contratMensu : pageContrats)
                {
                    nbContratsTraitesXml++;

                    rootxmlcontrat.addElement("Contrat").addAttribute("numerocontrat",
                        contratMensu.getNumeroContrat().toString()).addAttribute("anneepriseeffet",
                            contratMensu.getAnneePriseEffet().toString())
                        .addAttribute("typeimpot",
                            contratMensu.getTypeImpot().getCodeImpot().toString())
                        .addElement("Rib").addAttribute("codebanque",
                            contratMensu.getRib().getCodeBanque())
                        .addAttribute("codeguichet", contratMensu.getRib().getCodeGuichet()).addAttribute(
                            "codeetablissement", contratMensu.getRib().getCodeBanque())
                        .addAttribute("numerocompte",
                            contratMensu.getRib().getNumeroCompte())
                        .addAttribute("clé", contratMensu.getRib().getCleRib())
                        .addElement("Contribuable").addAttribute("civilité",
                            contratMensu.getContribuablePar().getCivilite().getLibelle())
                        .addAttribute("nom",
                            contratMensu.getContribuablePar().getNom())
                        .addAttribute("prenom",
                            contratMensu.getContribuablePar().getPrenom())
                        .addAttribute("adresse",
                            contratMensu.getContribuablePar().getAdressePrincipale().getCodePostal())
                        .addAttribute(
                            "adressemail", contratMensu.getContribuablePar().getAdresseMail())
                        .addAttribute("telephonefixe",
                            contratMensu.getContribuablePar().getTelephoneFixe())
                        .addAttribute("telephoneportable",
                            contratMensu.getContribuablePar().getTelephonePortable());
                }
            }
        }

        if (nbTotalContratsATraiter - nbContratsTraitesXml == 0)
        {

            log.info("Nombre de contrats enregistrés dans l'export xml:" + nbTotalContratsATraiter);

            /*
             * Tous les contrats qui ont été traités sont marqués comme traités
             */
            int nbContratsTraites = contratMensuDao.saveCommeTraiteContratsVerifiantRG70();
            log.info("Nombre de contrats enregistrés comme traité:" + nbContratsTraites);
        }
        else
        {
            throw new ProgrammationException(
                "Erreur de programmation: Nombre de contrats exportés en xml ne correspond pas au nombre total de contrats à traiter.");
        }
        return doc;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return integer
     * @see fr.gouv.finances.cp.dmo.service.IContratMensuService#reporterAnneePriseEffetPourContratsNonTraitesNeVerifiantPasRG71()
     */
    @Override
    public Integer reporterAnneePriseEffetPourContratsNonTraitesNeVerifiantPasRG71()
    {
        return contratMensuDao.reporterAnneePriseEffetPourContratsNonTraitesNeVerifiantPasRG71();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @see fr.gouv.finances.cp.dmo.service.IContratMensuService#supprimerContratsNeVerifiantPasRG70EtPrevenirContribuableParMail()
     */
    @Override
    public void supprimerContratsNeVerifiantPasRG70EtPrevenirContribuableParMail()
    {
        int nbContrats = contratMensuDao.countNombreContratsNonTraiteNeVerifiantPasRG70();
        int nbContratsParPage = 2;
        int nbPagesTotal = nbContrats / nbContratsParPage;

        // TODO à voir, nous envoyons les mails avant la supression en
        // masse des contrats...

        for (int pageCourante = 0; pageCourante <= nbPagesTotal; pageCourante++)
        {
            List<ContratMensu> pageContrats = null;
            pageContrats =
                contratMensuDao.findContratsNeVerifiantPasRG70(pageCourante * nbContratsParPage, nbContratsParPage);
            if (pageContrats != null)
            {
                pageContrats.forEach(contratMensu -> {
                    {
                        try
                        {
                            Mel mel = new Mel();
                            mel.setDe(contratMensu.getContribuablePar().getAdresseMail());
                            mel.setObjet("Contrat Mensu supprimé");
                            mel.setMesssage("Suppression du contrat :" + contratMensu.getId());
                            mel.ajouterDestinataireA(contratMensu.getContribuablePar().getAdresseMail());
                            serveurmel.envoyerMel(mel);
                        }
                        catch (MelRegleGestionException melRGExc)
                        {
                            log.info("Erreur sur le mail du contribuable pour le contrat (" + contratMensu.getNumeroContrat()
                                + ").Le contrat a été supprimé sans notification. Message :", melRGExc);
                        }
                        catch (MelExploitationException melExploitExc)
                        {
                            log
                                .info("Erreur lors de l'envoi d'un mail au contribuable pour le prévenir de la suppression de son contrat ("
                                    + contratMensu.getNumeroContrat() + ")", melExploitExc);
                        }
                    }
                });
            }
        }
        contratMensuDao.deleteContratsNeVerifiantPasRG70();
    }
}
