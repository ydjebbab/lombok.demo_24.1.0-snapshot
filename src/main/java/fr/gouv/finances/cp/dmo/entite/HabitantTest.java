/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.entite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;

/**
 * Entité HabitantTest
 *
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Entity
@Table(name = "HABITANTTEST_HABT")
@SequenceGenerator(name = "HABITANT_ID_GENERATOR", sequenceName = "SEQ_HABITANTTEST_HABT", allocationSize = 1)
public class HabitantTest extends BaseEntity
{
    private static final int _31 = 31;

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "HABITANT_ID_GENERATOR")
    @Column(name = "HABITANT_ID")
    private Long id;


    @Column(name = "NOM")
    private String nom;

    @Column(name = "PRENOM")
    private String prenom;

    @ManyToOne
    @JoinColumn(name = "ORIGINE_ID")
    private OrigineTest origine;

    public HabitantTest()
    {
        super();
    }

    public Long getId()
    {
        return id;
    }

    public String getNom()
    {
        return nom;
    }

    public OrigineTest getOrigine()
    {
        return origine;
    }

    public String getPrenom()
    {
        return prenom;
    }


    public void setId(Long id)
    {
        this.id = id;
    }

    public void setNom(String nom)
    {
        this.nom = nom;
    }

    public void setOrigine(OrigineTest origine)
    {
        this.origine = origine;
    }

    public void setPrenom(String prenom)
    {
        this.prenom = prenom;
    }


    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        HabitantTest other = (HabitantTest) obj;
        if (nom == null)
        {
            if (other.nom != null)
            {
                return false;
            }
        }
        else if (!nom.equals(other.nom))
        {
            return false;
        }
        if (prenom == null)
        {
            if (other.prenom != null)
            {
                return false;
            }
        }
        else if (!prenom.equals(other.prenom))
        {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode()
    {
        final int prime = _31;
        int result = 1;
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
        return result;
    }
}
