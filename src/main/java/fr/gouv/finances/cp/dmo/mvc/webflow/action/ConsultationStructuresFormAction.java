/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.cp.dmo.mvc.form.ConsultationStructuresForm;
import fr.gouv.finances.lombok.adresse.bean.Departement;
import fr.gouv.finances.lombok.adresse.bean.Region;
import fr.gouv.finances.lombok.adresse.service.AdresseService;
import fr.gouv.finances.lombok.apptags.tree.bean.Tree;
import fr.gouv.finances.lombok.apptags.tree.bean.TreeNode;
import fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action.AbstractTreeNavigationFormAction;
import fr.gouv.finances.lombok.structure.bean.StructureCP;
import fr.gouv.finances.lombok.structure.service.LectureStructureCPService;

/**
 * Action ConsultationStructuresFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class ConsultationStructuresFormAction extends AbstractTreeNavigationFormAction
{
    // Préfixes des noeuds de premier niveau

    private static final String PREFIX_POSTES_EN_FRANCE = "POSTES_EN_FRANCE";

    private static final String PREFIX_POSTES_A_LETRANGER = "POSTES_A_LETRANGER";

    private static final String PREFIX_POSTES_A_ATTR_SPEC = "STR_ATTR_SPEC";

    private static final String PREFIX_SERVICES_CENTRAUX_DGCP = "SER_CENT_DGCP";

    private static final String PREFIX_STRUCTURES_FORMATION = "STRUC_FORMATION";

    // Id des noeuds de premier niveau
    private static final String ID_POSTES_EN_FRANCE = "POSTES_EN_FRANCE";

    private static final String ID_POSTES_ETRANGER = "POSTES_ETRANGER";

    private static final String ID_STRUCTURES_ATTRIBUTIONS_SPECIALES = "STR_ATTR_SPEC";

    private static final String ID_SERVICES_CENTRAUX_DGCP = "SER_CENT_DGCP";

    private static final String ID_STRUCTURES_FORMATION = "STRUC_FORMATION";

    // Préfixes des noeuds de deuxième niveau
    private static final String PREFIX_REGION = "REGION";

    // Préfixes des noeuds de troisième niveau
    private static final String PREFIX_DEPARTEMENT = "DEPARTEMENT";

    private AdresseService adresseserviceso;

    private LectureStructureCPService lecturestructureserviceso;

    public ConsultationStructuresFormAction()
    {
        super();
    }

    public ConsultationStructuresFormAction(Class<?> formObjectClass)
    {
        super(formObjectClass);
    }

    public void setAdresseserviceso(AdresseService adresseserviceso)
    {
        this.adresseserviceso = adresseserviceso;
    }

    public void setLecturestructurecpserviceso(LectureStructureCPService lecturestructureserviceso)
    {
        this.lecturestructureserviceso = lecturestructureserviceso;
    }

    /**
     * methode Charger detail structure
     * 
     * @param requestContext param
     * @return event
     * @throws Exception the exception
     */
    public Event chargerDetailStructure(RequestContext requestContext) throws Exception
    {
        StructureCP structureSelectionnee;
        ConsultationStructuresForm treeForm =
            (ConsultationStructuresForm) requestContext.getFlowScope().get(getFormObjectName());

        structureSelectionnee =
            this.lecturestructureserviceso.rechercherUneStructureCPEtTousLesElementsAttachesParIdentiteNominoe(treeForm
                .getIdentitenominoe());

        treeForm.setStructureSelectionnee(structureSelectionnee);
        return success();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param requestContext
     * @return tree model name
     * @see fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action.AbstractTreeNavigationFormAction#getTreeModelName(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public String getTreeModelName(RequestContext requestContext)
    {
        return "tree.model";
    }

    /**
     * methode Initialise formulaire
     * 
     * @param requestContext param
     * @return event
     * @throws Exception the exception
     */
    public Event initialiseFormulaire(RequestContext requestContext) throws Exception
    {
        ConsultationStructuresForm treeForm =
            (ConsultationStructuresForm) requestContext.getFlowScope().get(getFormObjectName());
        treeForm.setIdentitenominoe(null);
        treeForm.setStructureSelectionnee(null);
        treeForm.setStructuresSelectionnees(null);

        return success();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param requestContext
     * @return tree
     * @see fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action.AbstractTreeNavigationFormAction#lectureDuNoeudRacineEtDeSesEnfants(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Tree lectureDuNoeudRacineEtDeSesEnfants(RequestContext requestContext)
    {
        Tree tree = new Tree();

        // Noeud racine
        TreeNode node = new TreeNode();
        node.setId("root");
        node.setName("Structures DGCP");
        node.setFolder(true);
        node.setHasUnloadedChildren(Boolean.FALSE);

        // Postes en France
        TreeNode noeudStructuresEnFrance = new TreeNode();
        noeudStructuresEnFrance.setId(ID_POSTES_EN_FRANCE);
        noeudStructuresEnFrance.setName("Postes en France");
        noeudStructuresEnFrance.setFolder(true);
        noeudStructuresEnFrance.setPrefixNodeId(PREFIX_POSTES_EN_FRANCE);
        noeudStructuresEnFrance.setType(PREFIX_POSTES_EN_FRANCE);
        node.addChild(noeudStructuresEnFrance);

        // Postes à l'étranger
        TreeNode noeudStructuresALEtranger = new TreeNode();
        noeudStructuresALEtranger.setId(ID_POSTES_ETRANGER);
        noeudStructuresALEtranger.setPrefixNodeId(PREFIX_POSTES_A_LETRANGER);
        noeudStructuresALEtranger.setType(PREFIX_POSTES_A_LETRANGER);
        noeudStructuresALEtranger.setName("Postes à l'étranger");
        noeudStructuresALEtranger.setFolder(false);
        noeudStructuresALEtranger.setHasUnloadedChildren(Boolean.FALSE);
        node.addChild(noeudStructuresALEtranger);

        // Postes à attributions spéciales
        TreeNode noeudStructuresAttributionsSpeciales = new TreeNode();
        noeudStructuresAttributionsSpeciales.setId(ID_STRUCTURES_ATTRIBUTIONS_SPECIALES);
        noeudStructuresAttributionsSpeciales.setPrefixNodeId(PREFIX_POSTES_A_ATTR_SPEC);
        noeudStructuresAttributionsSpeciales.setType(PREFIX_POSTES_A_ATTR_SPEC);
        noeudStructuresAttributionsSpeciales.setName("Postes à attributions spéciales");
        noeudStructuresAttributionsSpeciales.setFolder(false);
        noeudStructuresAttributionsSpeciales.setHasUnloadedChildren(Boolean.FALSE);
        node.addChild(noeudStructuresAttributionsSpeciales);

        // Services centraux DGCP
        TreeNode noeudServicesCentrauxDGCP = new TreeNode();
        noeudServicesCentrauxDGCP.setId(ID_SERVICES_CENTRAUX_DGCP);
        noeudServicesCentrauxDGCP.setPrefixNodeId(PREFIX_SERVICES_CENTRAUX_DGCP);
        noeudServicesCentrauxDGCP.setType(PREFIX_SERVICES_CENTRAUX_DGCP);
        noeudServicesCentrauxDGCP.setName("Services centraux DGCP");
        noeudServicesCentrauxDGCP.setFolder(false);
        noeudServicesCentrauxDGCP.setHasUnloadedChildren(Boolean.FALSE);
        node.addChild(noeudServicesCentrauxDGCP);

        // Structures de formation
        TreeNode structuresFormation = new TreeNode();
        structuresFormation.setId(ID_STRUCTURES_FORMATION);
        structuresFormation.setPrefixNodeId(PREFIX_STRUCTURES_FORMATION);
        structuresFormation.setType(PREFIX_STRUCTURES_FORMATION);
        structuresFormation.setName("Structures de formation");
        structuresFormation.setFolder(false);
        structuresFormation.setHasUnloadedChildren(Boolean.FALSE);
        node.addChild(structuresFormation);

        tree.setRoot(node);
        tree.setSingleSelectionMode(true);
        tree.expand("root");
        return tree;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tree
     * @param node
     * @param requestContext
     * @see fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action.AbstractTreeNavigationFormAction#lectureEnfantsEtPetitsEnfants(fr.gouv.finances.lombok.apptags.tree.bean.Tree,
     *      fr.gouv.finances.lombok.apptags.tree.bean.TreeNode, org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public void lectureEnfantsEtPetitsEnfants(Tree tree, TreeNode node, RequestContext requestContext)
    {
        if (node.getHasUnloadedChildren())
        {
            if (PREFIX_POSTES_EN_FRANCE.equals(node.getType()))
            {
                // Pour les postes en france, on charge les régions et départements
                this.lectureEnfantsEtPetitsEnfantsPostesEnFrance(tree, node);
            }
        }
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param requestContext
     * @return event
     * @throws Exception the exception
     * @see fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action.AbstractTreeNavigationFormAction#selectNode(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Event selectNode(RequestContext requestContext) throws Exception
    {
        Event result = super.selectNode(requestContext);
        ConsultationStructuresForm treeForm =
            (ConsultationStructuresForm) requestContext.getFlowScope().get(getFormObjectName());

        // On attend au maximum un element selectionne
        if (treeForm.getSelectedNodes() != null && !treeForm.getSelectedNodes().isEmpty())
        {
            TreeNode node = treeForm.getSelectedNodes().iterator().next();

            List<StructureCP> structures = null;

            if (PREFIX_DEPARTEMENT.equals(node.getType()))
            {
                Departement departement =
                    this.adresseserviceso.rechercherUnDepartementParCodeDepartementINSEE(node.getObjectId());
                structures = this.lecturestructureserviceso.rechercherDesStructuresActivesCPParDepartement(departement);
            }
            else if (PREFIX_POSTES_A_LETRANGER.equals(node.getType()))
            {
                // Recherche de tous les postes actifs localisés à l'étranger
                structures = this.lecturestructureserviceso.rechercheLesPostesComptablesALEtrangerActifs();
            }
            else if (PREFIX_POSTES_A_ATTR_SPEC.equals(node.getType()))
            {
                structures = this.lecturestructureserviceso.rechercheLesPostesAAttributionsSpeciales();
            }
            else if (PREFIX_SERVICES_CENTRAUX_DGCP.equals(node.getType()))
            {
                // Recherche de toutes les structures de services centraux
                // structures = this.lecturestructureserviceso.rechercheLesStructuresDirections();

                List codesCategoriesStructuresCP = new ArrayList();
                codesCategoriesStructuresCP.add("04");
                structures =
                    this.lecturestructureserviceso
                        .rechercherDesStructuresCPActivesParCodescategories(codesCategoriesStructuresCP);
            }
            else if (PREFIX_STRUCTURES_FORMATION.equals(node.getType()))
            {
                structures = this.lecturestructureserviceso.rechercheLesStructuresDeFormation();
            }

            treeForm.setStructuresSelectionnees(structures);
        }

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param requestContext
     * @return event
     * @throws Exception the exception
     * @see fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action.AbstractTreeNavigationFormAction#selectNodeAndExpandAncestor(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Event selectNodeAndExpandAncestor(RequestContext requestContext) throws Exception
    {
        Event result = super.selectNodeAndExpandAncestor(requestContext);

        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param requestContext
     * @return event
     * @throws Exception the exception
     * @see fr.gouv.finances.lombok.apptags.tree.mvc.webflow.action.AbstractTreeNavigationFormAction#unSelectNode(org.springframework.webflow.execution.RequestContext)
     */
    @Override
    public Event unSelectNode(RequestContext requestContext) throws Exception
    {
        Event result = super.unSelectNode(requestContext);

        ConsultationStructuresForm treeForm =
            (ConsultationStructuresForm) requestContext.getFlowScope().get(getFormObjectName());
        treeForm.setStructureSelectionnee(null);

        return result;
    }

    /**
     * methode Lecture enfants et petits enfants postes en france
     * 
     * @param tree param
     * @param node param
     */
    private void lectureEnfantsEtPetitsEnfantsPostesEnFrance(Tree tree, TreeNode node)
    {
        List<Region> lesRegions = this.adresseserviceso.rechercherToutesLesRegionsAvecDepartement();

        // Ajout des noeuds région
        for (Region region : lesRegions)
        {
            TreeNode subNode = new TreeNode();
            subNode.setId(region.getCoderegionINSEE());
            subNode.setName(region.getLibelleregion());
            subNode.setPrefixNodeId(PREFIX_REGION);
            subNode.setType(PREFIX_REGION);
            subNode.setFolder(true);
            node.addChild(subNode);

            // Ajout des noeuds département
            Set<Departement> lesDepartements = region.getLesDepartements();
            for (Departement departement : lesDepartements)
            {
                TreeNode subNodeDpt = new TreeNode();
                subNodeDpt.setId(departement.getCodedepartementINSEE());
                subNodeDpt.setName(departement.getLibelledepartement());
                subNodeDpt.setPrefixNodeId(PREFIX_DEPARTEMENT);
                subNodeDpt.setType(PREFIX_DEPARTEMENT);
                subNodeDpt.setFolder(false);
                subNodeDpt.setHasUnloadedChildren(Boolean.FALSE);
                subNode.addChild(subNodeDpt);
            }
            subNode.setHasUnloadedChildren(Boolean.FALSE);
        }
        node.setHasUnloadedChildren(Boolean.FALSE);
    }

}
