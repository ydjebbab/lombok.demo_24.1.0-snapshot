/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TypeImpotColumnInterceptor.java
 *
 */
package fr.gouv.finances.cp.dmo.interceptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.lombok.apptags.table.bean.Column;
import fr.gouv.finances.lombok.apptags.table.core.TableModel;
import fr.gouv.finances.lombok.apptags.table.interceptor.ColumnInterceptor;

/**
 * Cette classe est un exemple de mise en place d'une image dans une cellule du
 * composant ectable pour une colonne à valeurs booléennes.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
public class TypeImpotColumnInterceptor implements ColumnInterceptor
{

    /** defaultStyle - String, style par défaut de la colonne. */
    private String defaultStyle;

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * Constructeur 
     */
    public TypeImpotColumnInterceptor()
    {
        super();

    }

    /*
     * (non-Javadoc)
     * @see
     * fr.gouv.finances.lombok.apptags.table.interceptor.ColumnInterceptor#addColumnAttributes(fr.gouv.finances.lombok
     * .apptags.table.core.TableModel, fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param tableModel
     * @param column
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.ColumnInterceptor#addColumnAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void addColumnAttributes(TableModel tableModel, Column column)
    {
        // Aucun attribut à ajouter
    }

    /**
     * Accesseur du style par défaut de la colonne
     * 
     * @return le style par défaut de la colonne
     */
    public String getDefaultStyle()
    {
        return defaultStyle;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param model
     * @param column
     * @see fr.gouv.finances.lombok.apptags.table.interceptor.ColumnInterceptor#modifyColumnAttributes(fr.gouv.finances.lombok.apptags.table.core.TableModel,
     *      fr.gouv.finances.lombok.apptags.table.bean.Column)
     */
    @Override
    public void modifyColumnAttributes(TableModel model, Column column)
    {

        // Lecture du bean de la ligne courante
        Object rowBean = model.getCurrentRowBean();

        if (rowBean instanceof TypeImpot)
        {
            Boolean ouvert = ((TypeImpot) rowBean).getEstOuvertALaMensualisation();
            StringBuffer newStyle = new StringBuffer();
            String context = model.getContext().getContextPath();

            // Si le style par défaut est déjà mémorisé, on l'ajoute au nouveau style
            if (getDefaultStyle() != null)
            {
                newStyle.append(getDefaultStyle());
            }

            // Sinon, on mémorise le style par défaut, s'il existe, et on l'ajoute au nouveau style
            else if (column.getStyle() != null)
            {
                setDefaultStyle(column.getStyle());
                newStyle.append(getDefaultStyle());
            }

            // S'il n'existe pas de style par défaut, on l'initialise avec une chaîne vide
            else
            {
                setDefaultStyle("");
            }

            if (ouvert.booleanValue())
            {
                newStyle.append(";background-image:url('" + context
                    + "/application/img/autorise.gif');background-repeat:no-repeat;background-position:center;");
            }
            else
            {
                newStyle.append(";background-image:url('" + context
                    + "/application/img/interdit.gif');background-repeat:no-repeat;background-position:center;");
            }
            column.setStyle(newStyle.toString());
        }
        else
        {
            log.warn("Cet intercepteur doit être utilisé avec des objets de type TypeImpot.");
        }

    }

    /**
     * Modificateur du style par défaut de la colonne
     * 
     * @param defaultStyle le nouveau style par défaut de la colonne
     */
    public void setDefaultStyle(String defaultStyle)
    {
        this.defaultStyle = defaultStyle;
    }

}
