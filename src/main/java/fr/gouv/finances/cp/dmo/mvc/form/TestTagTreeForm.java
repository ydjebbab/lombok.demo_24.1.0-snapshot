/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TestTagTreeForm.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import fr.gouv.finances.lombok.apptags.tree.mvc.form.TreeForm;

/**
 * Class TestTagTreeForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class TestTagTreeForm extends TreeForm
{

    /** serialVersionUID - long, . */
    private static final long serialVersionUID = 2732934583371499367L;

    /**
     * Constructeur de la classe TestTagTreeForm.java
     */
    public TestTagTreeForm()
    {
        super(); // Raccord de constructeur auto-généré

    }

}
