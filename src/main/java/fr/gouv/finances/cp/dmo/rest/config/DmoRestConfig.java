package fr.gouv.finances.cp.dmo.rest.config;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import fr.gouv.finances.cp.dmo.rest.controller.CentreImpotController;
import fr.gouv.finances.cp.dmo.rest.controller.ContribuableController;
import fr.gouv.finances.cp.dmo.rest.controller.RestEntiteController;
import fr.gouv.finances.cp.dmo.rest.controller.RestGlobalController;

@Configuration
@ApplicationPath("/rest")
public class DmoRestConfig extends ResourceConfig
{
    public DmoRestConfig() {
        register(RestEntiteController.class);
        register(RestGlobalController.class);
        register(ContribuableController.class);
        register(CentreImpotController.class);
    }
}
