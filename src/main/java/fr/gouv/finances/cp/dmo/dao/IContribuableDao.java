/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ContribuableDao.java
 *
 */

package fr.gouv.finances.cp.dmo.dao;

import java.util.List;

import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuableEnt;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.PersonneTest;
import fr.gouv.finances.cp.dmo.techbean.CriteresRecherches;
import fr.gouv.finances.lombok.jpa.dao.BaseDaoJpa;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Interface ContribuableDao.
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
public interface IContribuableDao extends BaseDaoJpa
{
    /**
     * Supprimer un contribuable.
     * 
     * @param contribuable param
     */
    public void deleteContribuable(Contribuable contribuable);

    /**
     * Rechercher un avis d'imposition par référence.
     * 
     * @param reference param
     * @return the avis imposition
     */
    public AvisImposition findAvisImpositionParReference(String reference);

    /**
     * Rechercher si l'identifiant du contribuable est affecté.
     * 
     * @param ident param
     * @param identifiant param
     * @return true, if find indentifiant contribuable est affecte
     */
    public boolean findIndentifiantContribuableEstAffecte(Long ident, String identifiant);

    /**
     * Rechercher d'une liste de contribuables à l'aide de critéres de recherche.
     * 
     * @param criteresrecherches (identifiant, mel, civilité,code postal...)
     * @return the list< contribuable>
     */
    public List<Contribuable> findListContribuableViaCriteresRecherches(final CriteresRecherches criteresrecherches);

    /**
     * Rechercher d'avis de montant maximal poyr un contribuable.
     * 
     * @param uid param
     * @return the avis imposition
     */
    public AvisImposition findMaxAvisPourUnContribuable(String uid);

    /**
     * Rechercher si le (nom + prenom +mail) existe déjà.
     * 
     * @param contribuablePar 
     * @return true, if find nom prenom adresse mail existe deja
     */
    public boolean findNomPrenomAdresseMailExisteDeja(ContribuablePar contribuablePar);

    /**
     * Rechercher un contribuable entreprise par son uid.
     * 
     * @param uid param
     * @return the contribuable ent
     */
    public ContribuableEnt findContribuableEntrepriseParUID(String uid);

    /**
     * Rechercher un contribuable et ses avis d'imposition et contrats de mensualisation par le référence de l'avis.
     * 
     * @param referenceAvis param
     * @return the contribuable par
     */
    public ContribuablePar findContribuableEtAvisImpositionEtContratsMensualisationParReferenceAvis(
        String referenceAvis);

    /**
     * Rechercher un contribuable et ses avis d'imposition et contrats de mensualisation par UID.
     * 
     * @param identifiant param
     * @return the contribuable par
     */
    public ContribuablePar findContribuableEtAvisImpositionEtContratsMensualisationParUID(final String identifiant);

    /**
     * Rechercher un contribuable à partir de son identifiant et n'étant pas de la sous classe entreprise.
     * 
     * @param identifiant param
     * @return the list< contribuable>
     */
    public List<Contribuable> findContribuableParSousClasseEtIdentifiant(final String identifiant);

    /**
     * Initialiser les documents joints.
     * 
     * @param contribuablePar 
     */
    public void initializeLesDocumentsJoints(final ContribuablePar contribuablePar);

    /**
     * Initialise les différentes listes rattachées au contribuables (impots, adresses, docs joints...)
     * 
     * @param contribuablePar 
     */
    public void initializeListesRattacheesAUnContribuable(ContribuablePar contribuablePar);

    /**
     * Rafraichir un contribuable.
     * 
     * @param contribuablePar 
     */
    public void refreshContribuableParticulier(ContribuablePar contribuablePar);

    /**
     * Sauvegarder un contribuable Particulier.
     * 
     * @param contribuablePar 
     */
    public void saveContribuableParticulier(final ContribuablePar contribuablePar);

    /**
     * Sauvegarder un contribuable Entreprise.
     *
     * @param contribuableEnt
     */
    public void saveContribuableEntreprise(final ContribuableEnt contribuableEnt);

    /**
     * Rechercher tous les contribuable par iterator
     *
     * @param iii
     * @return dgcp scroll iterator
     */
    public ScrollIterator findTousContribuableParIterator(int iii);

    /**
     * Rechercher tous les contribuables et adresses par iterator
     * 
     * @param nombreOccurences param
     * @return dgcp scroll iterator
     */
    public ScrollIterator findTousContribuablesEtAdressesParIterator(int nombreOccurences);

    /**
     * Rechercher tous les contribuables et adresses test par iterator
     * 
     * @param nombreOccurences param
     * @return dgcp scroll iterator
     */
    public ScrollIterator findTousContribuablesEtAdressesTestParIterator(int nombreOccurences);

    /**
     * Rechercher tous les contribuables et adresses test without iterator
     * 
     * @return list
     */
    public List<PersonneTest> findTousContribuablesEtAdressesTestWithoutIterator();
    
    
    public List<ContribuablePar> findAllContribuable();

}
