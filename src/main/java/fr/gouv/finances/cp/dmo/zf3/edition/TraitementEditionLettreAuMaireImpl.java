/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.zf3.edition;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.gouv.finances.cp.dmo.editionbean.Comptable;
import fr.gouv.finances.cp.dmo.editionbean.ResultatMensuelExploitation;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;

/**
 * Classe TraitementEditionsEcrituresCptbleImpl.java.
 */
public class TraitementEditionLettreAuMaireImpl extends AbstractServiceEditionCommunImpl
{

    /**
     * Constructeur
     */
    public TraitementEditionLettreAuMaireImpl()
    {
        super();
    }

    /**
     * Construire la collection d’objets métiers (beans) qui sera utilisée pour élaborer l’édition. Dans cette methode il
     * faut :
     * - récupérer des paramètres passés dans la map parametresEdition et qui serviront pour l’appel du service
     * métier (numéro de département ou plage de dates par exemple)
     * - appeler le service métier en principe un service de type rechercherXxx qui renverra une collection d’objets
     * - éventuellement retraiter la collection d’objets pour la présenter sous une forme particulière si l’on a pas
     * réussi à traiter tous les aspects de présentation (format particulier d’affichage pour des valeurs d’attributs
     * par exemple) dans le modeler d’édition lui même
     * - retourner la collection en sortie de méthode
     * 
     * @param parametresEdition les parametres de l'edition
     * @return une collection de pensionnes
     */
    @Override
    public Collection<ResultatMensuelExploitation> creerDatasource(Map parametresEdition)
    {
        List<ResultatMensuelExploitation> uneCollection = new ArrayList<>();

        ResultatMensuelExploitation unRME = (ResultatMensuelExploitation) (parametresEdition.get("leRme"));

        uneCollection.add(unRME);

        return uneCollection;
    }


    /**
     * Construire le nom de fichier pour l'édition qui sera présenté à l'utilisateur.
     * 
     * @param parametresEdition (pas utilisé)
     * @return le nom de l'édition
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl
     *      #creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        return null;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition Map
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl
     *      #creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {
        Map parametres = new HashMap();

        Comptable leComptable = (Comptable) (parametresEdition.get("leComptable"));

        parametres.put("libelleCom", leComptable.getLibelleCom());

        return parametres;
    }


}
