/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.core.collection.SharedAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import fr.gouv.finances.cp.dmo.editionbean.Comptable;
import fr.gouv.finances.cp.dmo.editionbean.ResultatMensuelExploitation;
import fr.gouv.finances.cp.dmo.mvc.form.StatistiquesForm;
import fr.gouv.finances.cp.dmo.service.IStatistiquesService;
import fr.gouv.finances.cp.dmo.techbean.StatistiquesAvisImpositions;
import fr.gouv.finances.cp.dmo.techbean.StatistiquesContribuables;
import fr.gouv.finances.cp.dmo.zf3.edition.TraitementEditionLettreAuMaireStub;
import fr.gouv.finances.lombok.edition.bean.EditionAsynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNotifieeParMail;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import fr.gouv.finances.lombok.mvc.TelechargementView;
import fr.gouv.finances.lombok.mvc.jasperreports.CpJasperReportsHtmlView;
import fr.gouv.finances.lombok.securite.springsecurity.UtilisateurSecurityLombokContainer;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;

/**
 * Action StatistiquesFormAction
 * 
 * @author chouard-cp
 * @author CF
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class StatistiquesFormAction extends FormAction
{
    private static final Logger log = LoggerFactory.getLogger(StatistiquesFormAction.class);

    private IStatistiquesService statistiquesserviceso;

    private EditionDemandeService editiondemandeserviceso;

    public StatistiquesFormAction()
    {
        super();
    }

    public StatistiquesFormAction(Class<?> arg0)
    {
        super(arg0);
    }

    public EditionDemandeService getEditiondemandeserviceso()
    {
        return editiondemandeserviceso;
    }

    public IStatistiquesService getStatistiquesserviceso()
    {
        return statistiquesserviceso;
    }

    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso)
    {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    public void setStatistiquesserviceso(IStatistiquesService statistiquesserviceso)
    {
        this.statistiquesserviceso = statistiquesserviceso;
    }

    /**
     * methode Executer agregation avis imposition par type impot
     * 
     * @param reqc param
     * @return event
     */
    public Event executerAgregationAvisImpositionParTypeImpot(RequestContext reqc)
    {
        log.debug(">>> Debut methode executerAgregationAvisImpositionParTypeImpot");

        List<StatistiquesAvisImpositions> listeStatistiquesAvisImpositions =
            statistiquesserviceso.calculerAgregationAvisImpositionParTypeImpot();

        // Production du graphique au format png
        FichierJoint fichierJoint =
            editiondemandeserviceso.declencherProductionEditionNonStockee("zf3.graphiquecontripartypeimpot.edition",
                new HashMap());

        // Production du graphique au format png
        FichierJoint fichierJointPng =
            editiondemandeserviceso.declencherProductionEditionNonStockee(
                "zf3.graphiquerepartavispartypeimpot.edition", new HashMap());

        reqc.getFlowScope().put("graphiquecontripartypeimpot", fichierJoint.getLeContenuDuFichier().getData());

        // Stockage dans le contexte de flux du graphique png produit
        reqc.getFlowScope().put("graphiquerepartavispartypeimpot", fichierJointPng.getLeContenuDuFichier().getData());

        reqc.getFlowScope().put("statistiques", listeStatistiquesAvisImpositions);
        return success();
    }

    /**
     * methode Executer agregation avis imposition par type impot seuil
     * 
     * @param reqc param
     * @return event
     */
    public Event executerAgregationAvisImpositionParTypeImpotSeuil(RequestContext reqc)
    {
        log.debug(">>> Debut methode executerAgregationAvisImpositionParTypeImpotSeuil");
        Double seuil = new Double(0);

        StatistiquesForm statistiquesform = (StatistiquesForm) reqc.getFlowScope().get(getFormObjectName());
        seuil = (statistiquesform.getSeuilAgregation() == null ? new Double(0) : statistiquesform.getSeuilAgregation());

        // Stockage du seuil en session
        reqc.getExternalContext().getSessionMap().put("seuil", seuil);

        List<StatistiquesAvisImpositions> listeStatistiquesAvisImpositions =
            statistiquesserviceso.calculerAgregationAvisImpositionParTypeImpotSeuil(seuil);
        reqc.getFlowScope().put("statistiques", listeStatistiquesAvisImpositions);
        return success();
    }

    /**
     * methode Executer agregation contribuable par type impot
     * 
     * @param reqc param
     * @return event
     */
    public Event executerAgregationContribuableParTypeImpot(RequestContext reqc)
    {
        log.debug(">>> Debut methode executerAgregationContribuableParTypeImpot");
        List<StatistiquesContribuables> listeStatistiquesContribuables = statistiquesserviceso.calculerAgregationContribuableParTypeImpot();
        reqc.getFlowScope().put("statistiques", listeStatistiquesContribuables);
        return success();
    }

    /**
     * methode Executer agregation par contribuable .
     * 
     * @param reqc param
     * @return event
     */
    public Event executerAgregationParContribuable(RequestContext reqc)
    {
        log.debug(">>> Debut methode executerAgregationParContribuable");
        List<StatistiquesContribuables> listeStatistiquesContribuables = statistiquesserviceso.calculerAgregationParContribuable();

        Map<Object, Object> mapParametres = new HashMap<Object, Object>();
        mapParametres.put("SEUIL", "30");
        reqc.getFlowScope().put("zf3.agregcontriparavis.parametres", mapParametres);

        reqc.getFlowScope().put("statistiques", listeStatistiquesContribuables);
        return success();
    }

    /**
     * methode Initialiser formulaire
     * 
     * @param request param
     * @return event
     */
    public Event initialiserFormulaire(RequestContext request)
    {
        log.debug(">>> Debut methode initialiserFormulaire");
        // Lecture du formulaire
        StatistiquesForm statistiquesForm = (StatistiquesForm) request.getFlowScope().get(getFormObjectName());

        request.getExternalContext().getApplicationMap();
        SharedAttributeMap sharedSessAttrMap = request.getExternalContext().getSessionMap();
        Double seuilSession = (Double) sharedSessAttrMap.get("seuilAgregation");

        statistiquesForm.setSeuilAgregation(seuilSession);

        return success();
    }

    /**
     * methode Preparer bean choix statistique
     * 
     * @param reqc param
     * @return event
     */
    public Event preparerBeanChoixStatistique(RequestContext reqc)
    {
        log.debug(">>> Debut methode preparerBeanChoixStatistique");
        Map<String, String> typesStatistiques = new HashMap<String, String>();
        typesStatistiques.put("agregavispartypeimpot", "Agrégation par type d'impôt des avis d'imposition ");
        typesStatistiques.put("agregcontrpartypeimpot", "Agrégation par type d'impôt des contribuables");
        typesStatistiques.put("agregavispartypeimpotseuil",
            "Agrégation par type d'impôt des avis d'imposition dont le montant est supérieur au 'seuil d'agrégation' ");
        typesStatistiques.put("agregparcontribuable", "Agrégation par contribuable des avis d'imposition");
        reqc.getFlowScope().put("rapports", typesStatistiques);

        return success();
    }

    /**
     * methode Produire edition agreg avis par type impot seuil
     * 
     * @param reqc param
     * @return event
     */
    public Event produireEditionAgregAvisParTypeImpotSeuil(RequestContext reqc)
    {
        log.debug(">>> Debut methode produireEditionAgregAvisParTypeImpotSeuil");
        StatistiquesForm statistiquesForm = (StatistiquesForm) reqc.getFlowScope().get(getFormObjectName());

        /*
         * Récupération des caractéristiques de la personne connectée dans le contexte sécurisé pour retrouver l'uid
         */
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        Map<Object, Object> parametresEdition = new HashMap<Object, Object>();
        parametresEdition.put("servicemetier.seuil", statistiquesForm.getSeuilAgregation());

        // Stockage du seuil en session
        reqc.getExternalContext().getSessionMap().put("seuil", statistiquesForm.getSeuilAgregation());

        EditionSynchroneNotifieeParMail editionSynchroneNonNotifieeParMail =
            editiondemandeserviceso.initEditionSynchroneNotifieeParMail("zf3.agregavispartypeimpotseuil.edition",
                personne.getUid(), personne.getMail());

        editionSynchroneNonNotifieeParMail =
            editiondemandeserviceso.declencherEdition(editionSynchroneNonNotifieeParMail, parametresEdition);

        reqc.getFlowScope().put("jobHistory", editionSynchroneNonNotifieeParMail);

        return success();
    }

    /**
     * methode Produire edition agreg contri par avis
     * 
     * @param reqc param
     * @return event
     */
    public Event produireEditionAgregContriParAvis(RequestContext reqc)
    {
        log.debug(">>> Debut methode produireEditionAgregContriParAvis");
        /*
         * Récupération des caractéristiques de la personne connectée dans le contexte sécurisé pour retrouver l'uid
         */
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        EditionAsynchroneNotifieeParMail editionAsynchroneNonNotifieeParMail =
            editiondemandeserviceso.initEditionAsynchroneNotifieeParMail("zf3.agregcontriparavis.edition", personne
                .getUid(), personne.getMail());

        editionAsynchroneNonNotifieeParMail =
            editiondemandeserviceso.declencherEditionCompressee(editionAsynchroneNonNotifieeParMail, new HashMap());

        reqc.getFlowScope().put("jobHistory", editionAsynchroneNonNotifieeParMail);

        return success();
    }

    /**
     * methode Produire edition agreg contri par type impot
     * 
     * @param reqc param
     * @return event
     */
    public Event produireEditionAgregContriParTypeImpot(RequestContext reqc)
    {
        log.debug(">>> Debut methode produireEditionAgregContriParTypeImpot");
        Map result =
            editiondemandeserviceso.declencherProductionHtmlEditionNonStockee("zf3.agregcontripartypeimpot.edition",
                new HashMap());

        JasperPrint jasperPrint = (JasperPrint) result.get(CpJasperReportsHtmlView.JASPER_PRINT_KEY);

        FichierJoint fichierJoint = (FichierJoint) result.get(CpJasperReportsHtmlView.FICHIER_JOINT_KEY);

        reqc.getFlowScope().put(TelechargementView.FICHIER_A_TELECHARGER,
            fichierJoint.getLeContenuDuFichier().getData());

        reqc.getFlowScope().put(TelechargementView.DEMANDE_ENREGISTREMENT_FICHIER, Boolean.FALSE);

        reqc.getExternalContext().getSessionMap().put(ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);

        return success();
    }

    /**
     * methode Produire edition casinos crosstab
     * 
     * @param reqc param
     * @return event
     */
    public Event produireEditionCasinosCrosstab(RequestContext reqc)
    {
        log.debug(">>> Debut methode produireEditionCasinosCrosstab");
        /*
         * Récupération des caractéristiques de la personne connectée dans le contexte sécurisé pour retrouver l'uid
         */
        PersonneAnnuaire personne = UtilisateurSecurityLombokContainer.getPersonneAnnuaire();

        EditionSynchroneNonNotifieeParMail editionSynchroneNonNotifieeParMail =
            editiondemandeserviceso.initEditionSynchroneNonNotifieeParMail("zf3.casinospardepartement.edition",
                personne.getUid());
        editionSynchroneNonNotifieeParMail =
            editiondemandeserviceso.declencherEdition(editionSynchroneNonNotifieeParMail, new HashMap());

        reqc.getFlowScope().put("jobHistory", editionSynchroneNonNotifieeParMail);

        return success();
    }

    /**
     * methode Produire edition casino test caracteres degrades.
     * 
     * @param reqc contexte de requête
     * @return event
     */
    public Event produireEditionCasinoTestCaracteresDegrades(RequestContext reqc)
    {
        log.debug(">>> Debut methode produireEditionCasinoTestCaracteresDegrades");
        ResultatMensuelExploitation unRme = TraitementEditionLettreAuMaireStub.createBeanCollection().iterator().next();

        Comptable leComptable = new Comptable();
        leComptable.setSages(unRme.getComptableCodiquePoste());

        // Edition PDF

        /*
         * Preparation du fichier d'edition en recuperant le contenu du formulaire et en preparant les parametres
         * necessaires a la constitution du fichier d'edition. L'edition est ensuite declenchee automatiquement avec
         * telechargement du fichier pdf produit. On charge les différentes données nécessaires pour constituer ensuite
         * le fichier d'édition. Les données chargées dans la map parametresEdition sont ensuite récupérées dans la
         * méthode creerDatasource de la classe TraitementEditionsStat01 qui constituera le fichier pdf à imprimer.
         */

        Map<String, Object> parametresEdition = new HashMap<>();

        parametresEdition.put("leRme", unRme);

        parametresEdition.put("leComptable", leComptable);

        /*
         * on va déclencher automatiquement la constitution du fichier d'édition sans le stocker dans le repository
         * d'édition
         */

        FichierJoint fichierJoint =
            editiondemandeserviceso.declencherProductionEditionNonStockee("zf3.lettreaumaire.edition", parametresEdition);

        /* Stockage dans le contexte de flux du fichier pdf produit qui sera automatiquement téléchargé */
        reqc.getFlowScope().put(TelechargementView.FICHIER_A_TELECHARGER, fichierJoint);

        return success();

    }

    /**
     * methode Voir edition
     * 
     * @param reqc param
     * @return event
     */
    public Event voirEdition(RequestContext reqc)
    {
        log.debug(">>> Debut methode voirEdition");
        StatistiquesForm statistiquesForm = (StatistiquesForm) reqc.getFlowScope().get(getFormObjectName());
        FichierJoint result = statistiquesForm.getEditionProduite();

        reqc.getFlowScope().put(TelechargementView.FICHIER_A_TELECHARGER, result);
        return success();
    }

}
