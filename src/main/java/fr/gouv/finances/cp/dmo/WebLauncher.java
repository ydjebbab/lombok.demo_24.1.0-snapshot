/*
 * Copyright (c) 2019 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo;

import org.apache.catalina.Container;
import org.apache.catalina.Context;
import org.apache.catalina.Wrapper;
import org.apache.tomcat.util.scan.StandardJarScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.integration.IntegrationAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * <pre>
 * Cette classe va démarrer l'application lombok.demo avec Spring Boot. 
 * Soit en mode "tomcat embedded", soit en mode "tomcat séparé".
 * </pre>
 */
@Configuration
@SpringBootApplication
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class, JmxAutoConfiguration.class,
        IntegrationAutoConfiguration.class, MultipartAutoConfiguration.class
        // , DevToolsDataSourceAutoConfiguration.class
})
// CF: ajout de fr.gouv.finances.lombok.jpa.config (lombok-jpa)
@ComponentScan(basePackages = {"fr.gouv.finances.lombok.config", "fr.gouv.finances.lombok.autoconfig", "fr.gouv.finances.lombok.stubs", "fr.gouv.finances.cp.dmo.config",
        "fr.gouv.finances.cp.dmo.controller", "fr.gouv.finances.lombok.jpa.config",  "fr.gouv.finances.cp.dmo.service.impl", 
        "fr.gouv.finances.cp.dmo.rest.service.impl", "fr.gouv.finances.cp.dmo.rest.config"})
public class WebLauncher extends SpringBootServletInitializer
{
    private static final Logger log = LoggerFactory.getLogger(WebLauncher.class);

    /**
     * (methode de remplacement) {@inheritDoc}
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder)
    {
        log.debug(">>> Debut methode configure");
        return WebLauncher.build(builder);
    }

    /**
     * Méthode de démarrage en mode "TOMCAT EMBEDDED" uniquement
     *
     * @param args arguments passés en paramètre (pour surcharger les propriétés du fichier application.properties par
     *        exemple)
     */
    public static void main(String[] args)
    {
        log.debug(">>> Debut methode main");
        WebLauncher.build(new SpringApplicationBuilder()).run(args);
    }

    /**
     * Méthode appelée en modes "TOMCAT SÉPARÉ" et "TOMCAT EMBEDDED"
     *
     * @param builder
     * @return spring application builder
     */
    private static SpringApplicationBuilder build(SpringApplicationBuilder builder)
    {
        log.debug(">>> Debut methode build");
        return builder.sources(WebLauncher.class);
    }

    /**
     * <pre>
     * Methode Servlet container customizer : customisation des init parameters du tomcat embedded (trimspaces et
     * development). 
     * Dans cette version de Spring Boot les propriétés n'existent pas encore
     * (server.**.init-parameter.developement).
     * Il faudra supprimer cette méthode et la remplacer par des paramètres lorsque nous monterons 
     * la version de Spring Boot.
     * </pre>
     *
     * @return embedded servlet container customizer
     */
    @Bean
    public EmbeddedServletContainerCustomizer servletContainerCustomizer()
    {
        return new EmbeddedServletContainerCustomizer()
        {
            @Override
            public void customize(ConfigurableEmbeddedServletContainer container)
            {
                if (container instanceof TomcatEmbeddedServletContainerFactory)
                {
                    customizeTomcat((TomcatEmbeddedServletContainerFactory) container);
                }
            }

            private void customizeTomcat(TomcatEmbeddedServletContainerFactory tomcatFactory)
            {
                // CF: on ne rentre pas dans cette méthode si spring-boot-devtools est activé
                log.debug(">>> Debut methode customizeTomcat");
                tomcatFactory.addContextCustomizers(new TomcatContextCustomizer()
                {
                    @Override
                    public void customize(Context context)
                    {
                        Container jsp = context.findChild("jsp");
                        if (jsp instanceof Wrapper)
                        {
                            // on applique les paramètres sur le tomcat embedded.
                            // avec la version de spring boot, on devrait pouvoir le faire
                            // directement dans le application.properties avec les clés
                            // server.jsp-servlet.init-parameters..
                            // CF: development à false = empêcher les JSPs d'être compilées encore
                            ((Wrapper) jsp).addInitParameter("development", "false");
                            ((Wrapper) jsp).addInitParameter("trimSpaces", "true");
                        }
                        // La version 1.3.8 de boot tire le tomcat embedded 8.0.37 qui ne permet pas
                        // de désactiver le scan des manifests : du coup au démarrage de tomcat il y
                        // a plein de warnings sur des jars dont le manifest est erronné.
                        // Nous ne pouvons pas désactiver le scan (tlds, fragments).
                        // La solution est d'utiliser tomcat 8.0.48 (surcharge dans dependencymanagement du pom parent),
                        // qui dispose de la méthode setScanManifest que l'on peut mettre à false. Voir ici:
                        // https://bz.apache.org/bugzilla/show_bug.cgi?id=59961
                        StandardJarScanner stdJarScanner = (StandardJarScanner) context.getJarScanner();
                        stdJarScanner.setScanManifest(false);
                    }
                });
            }
        };
    }
}
