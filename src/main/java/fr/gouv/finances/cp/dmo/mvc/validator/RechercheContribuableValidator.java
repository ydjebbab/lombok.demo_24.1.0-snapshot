/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) :
 * - chouard-cp
 * Copyright 2002-2004 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.

*
 *
 * fichier : RechercheContribuableValidator.java
 *
 */

package fr.gouv.finances.cp.dmo.mvc.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import fr.gouv.finances.cp.dmo.mvc.form.RechercheContribuableForm;

/**
 * Class RechercheContribuableValidator DGFiP.
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class RechercheContribuableValidator implements Validator
{

    /**
     * Instanciation de recherche contribuable validator.
     */
    public RechercheContribuableValidator()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param clazz
     * @return true, si c'est vrai
     * @see org.springframework.validation.Validator#supports(java.lang.Class)
     */
    public boolean supports(Class clazz)
    {
        return clazz.equals(RechercheContribuableForm.class);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @param obj
     * @param errors
     * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
     */
    public void validate(Object obj, Errors errors)
    {

    }

    /**
     * methode Validate surface critere recherche : DGFiP.
     *
     * @param command param
     * @param errors param
     */
    public void validateSurfaceCritereRecherche(RechercheContribuableForm command, Errors errors)
    {
        // Au moins un des deux critères "identifiant" ou "adresse mail"
        // doit être servi
        if ((command.getCrit().getIdentifiant() == null || command.getCrit().getIdentifiant().trim().length() == 0)
            && (command.getCrit().getAdresseMail() == null || command.getCrit().getAdresseMail().trim().length() == 0)
            && (command.getCrit().getReferenceAvis() == null || command.getCrit().getReferenceAvis().trim().length() == 0))

        {
            errors.rejectValue("crit.identifiant", "rejected.absencecritererecherche");
        }

        if (command.getCrit().getTypeRechercheId() == null)
        {
            errors.rejectValue("crit.typeRechercheId", "rejected.absencetyperechercheid");
        }
        // Vérification de la valeur des identifiants retournés par les
        // listes déroulantes
        // GenericValidator.isInt(recherche.getCrit().getTypecriteredetriid());

    }
}