/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 * 
 *
 * fichier : TestAdressesPersonnesServiceImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finances.cp.dmo.dao.ITestAdressesPersonnesDao;
import fr.gouv.finances.cp.dmo.entite.AdresseTest;
import fr.gouv.finances.cp.dmo.entite.PersonneTest;
import fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService;
import fr.gouv.finances.lombok.util.base.BaseServiceImpl;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Service utilisé pour les tests Hibernate sur les relations bi-directionnelles n-n.
 * 
 * @author chouard-cp
 * @author CF: migration vers JPA
 * @author CF: passage XML vers annotations
 * @version $Revision: 1.5 $ Date: 14 déc. 2009
 */
@Service("testAdressesPersonnesService")
public class TestAdressesPersonnesServiceImpl extends BaseServiceImpl implements ITestAdressesPersonnesService
{

    @Autowired
    private ITestAdressesPersonnesDao testAdressesPersonnesDao;

    public TestAdressesPersonnesServiceImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService#supprimerPersonne(fr.gouv.finances.cp.dmo.entite.PersonneTest)
     */
    @Override
    public void supprimerPersonne(PersonneTest personneTest)
    {
        testAdressesPersonnesDao.deleteObject(personneTest);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService#supprimerAdresse(fr.gouv.finances.cp.dmo.entite.AdresseTest)
     */
    @Override
    public void supprimerAdresse(AdresseTest adresseTest)
    {
        testAdressesPersonnesDao.deleteObject(adresseTest);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService#sauvegarderPersonne(fr.gouv.finances.cp.dmo.entite.PersonneTest)
     */
    @Override
    public void sauvegarderPersonne(PersonneTest personneTest)
    {
        testAdressesPersonnesDao.saveObject(personneTest);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService#sauvegarderAdresse(fr.gouv.finances.cp.dmo.entite.AdresseTest)
     */
    @Override
    public void sauvegarderAdresse(AdresseTest adresseTest)
    {
        testAdressesPersonnesDao.saveObject(adresseTest);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param personneTest
     * @param adresseTest
     * @see fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService#ajouterAdresseALaPersonne(fr.gouv.finances.cp.dmo.entite.PersonneTest,
     *      fr.gouv.finances.cp.dmo.entite.AdresseTest)
     */
    @Override
    public void ajouterAdresseALaPersonne(PersonneTest personneTest, AdresseTest adresseTest)
    {
        personneTest.rattacherUneNouvelleAdresse(adresseTest);
        testAdressesPersonnesDao.modifyObject(personneTest);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param adresseTest
     * @param personneTest
     * @see fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService#ajouterPersonneALAdresse(fr.gouv.finances.cp.dmo.entite.AdresseTest,
     *      fr.gouv.finances.cp.dmo.entite.PersonneTest)
     */
    @Override
    public void ajouterPersonneALAdresse(AdresseTest adresseTest, PersonneTest personneTest)
    {
        adresseTest.rattacherUnePersonne(personneTest);
        testAdressesPersonnesDao.modifyObject(adresseTest);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param ville
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService#rechercherPersonnesEtAdressesParVille(java.lang.String)
     */
    @Override
    public List<PersonneTest> rechercherPersonnesEtAdressesParVille(String ville)
    {
        return testAdressesPersonnesDao.findPersonnesEtAdressesParVille(ville);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @return list
     * @see fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService#rechercherToutesLesPersonnes()
     */
    @Override
    public List<PersonneTest> rechercherToutesLesPersonnes()
    {
        return testAdressesPersonnesDao.findToutesLesPersonnes();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService#rechercherToutesLesPersonnesIterator(int)
     */
    @Override
    public ScrollIterator rechercherToutesLesPersonnesIterator(int nombreOccurences)
    {
        return testAdressesPersonnesDao.findToutesLesPersonnesParIterator(nombreOccurences);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param voie
     * @param ville
     * @return adresse test
     * @see fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService#rechercherAdresseEtPersonnesParVoieEtVille(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public AdresseTest rechercherAdresseEtPersonnesParVoieEtVille(String voie, String ville)
    {
        return testAdressesPersonnesDao.findAdresseEtPersonneParVoieEtVille(voie, ville);
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param nom
     * @param prenom
     * @return personne test
     * @see fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService#rechercherPersonneEtAdressesParNom(java.lang.String,
     *      java.lang.String)
     */
    @Override
    public PersonneTest rechercherPersonneEtAdressesParNom(String nom, String prenom)
    {
        return testAdressesPersonnesDao.findPersonneEtAdressesParNom(nom, prenom);
    }

}