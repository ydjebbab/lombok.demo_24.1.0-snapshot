/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.cp.dmo.entite.Rib;
import fr.gouv.finances.lombok.bancaire.bean.GuichetBancaire;


/**
 * Class RechercheGuichetBancaireForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class RechercheGuichetBancaireForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** rib. */
    private Rib rib = new Rib();

    /** unguichetbancairearechercher. */
    private GuichetBancaire unguichetbancairearechercher = new GuichetBancaire();

    /**
     * Constructeur de la classe RechercheGuichetBancaireForm.java
     */
    public RechercheGuichetBancaireForm()
    {
        super(); // Raccord de constructeur auto-généré

    }

    /**
     * Gets the rib.
     *
     * @return the rib
     */
    public Rib getRib()
    {
        return rib;
    }

    /**
     * Gets the unguichetbancairearechercher.
     *
     * @return the unguichetbancairearechercher
     */
    public GuichetBancaire getUnguichetbancairearechercher()
    {
        return unguichetbancairearechercher;
    }

    /**
     * Sets the rib.
     *
     * @param rib the new rib
     */
    public void setRib(Rib rib)
    {
        this.rib = rib;
    }

    /**
     * Sets the unguichetbancairearechercher.
     *
     * @param unguichetbancairearechercher the new unguichetbancairearechercher
     */
    public void setUnguichetbancairearechercher(GuichetBancaire unguichetbancairearechercher)
    {
        this.unguichetbancairearechercher = unguichetbancairearechercher;
    }

}
