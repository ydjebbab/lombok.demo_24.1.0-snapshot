/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.editionbean;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;

/**
 * CasinoTaux.java Classe pour les taux avec 4 décimales, identique à celle de Forte.
 * 
 * @author jldebon-cp Date: 4 févr. 2011
 */
public class CasinoTaux extends BigDecimal
{

    /** Constant : CASINO_TX_ZERO. */
    public static final CasinoTaux CASINO_TX_ZERO = CasinoTaux.valueOf(BigDecimal.ZERO);

    /** Constant :  CASINO_TX_UN : CasinoTaux valeur à 1. */
    public static final CasinoTaux CASINO_TX_UN = CasinoTaux.valueOf(BigDecimal.ONE);


    /**
     * serialVersionUID - long, 
     */
    private static final long serialVersionUID = 1L;
    

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val valeur
     */
    public CasinoTaux(char[] val)
    {
        super(val); 
    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val valeur
     */
    public CasinoTaux(String val)
    {
        super(val); 
    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val valeur
     */
    public CasinoTaux(double val)
    {
        super(val); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val valeur
     */
    public CasinoTaux(BigInteger val)
    {
        super(val); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val valeur
     */
    public CasinoTaux(int val)
    {
        super(val); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val valeur
     */
    public CasinoTaux(long val)
    {
        super(val); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val char
     * @param mctx MathContext
     */
    public CasinoTaux(char[] val, MathContext mctx)
    {
        super(val, mctx); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val Valeur
     * @param mctx MathContext
     */
    public CasinoTaux(String val, MathContext mctx)
    {
        super(val, mctx); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val MathContextaleur
     * @param mctx MathContext
     */
    public CasinoTaux(double val, MathContext mctx)
    {
        super(val, mctx); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val Valeur
     * @param mctx MathContext
     */
    public CasinoTaux(BigInteger val, MathContext mctx)
    {
        super(val, mctx); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param unscaledVal BigInteger
     * @param scale int
     */
    public CasinoTaux(BigInteger unscaledVal, int scale)
    {
        super(unscaledVal, scale); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val Valeur
     * @param mctx MathContext
     */
    public CasinoTaux(int val, MathContext mctx)
    {
        super(val, mctx); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val Valeur
     * @param mctx MathContext
     */
    public CasinoTaux(long val, MathContext mctx)
    {
        super(val, mctx); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val char
     * @param offset int
     * @param len int
     */
    public CasinoTaux(char[] val, int offset, int len)
    {
        super(val, offset, len); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param unscaledVal BigInteger
     * @param scale int
     * @param mctx MathContext
     */
    public CasinoTaux(BigInteger unscaledVal, int scale, MathContext mctx)
    {
        super(unscaledVal, scale, mctx); 

    }

    /**
     * Constructeur de la classe CasinoTaux.java
     * 
     * @param val char
     * @param offset int
     * @param len int
     * @param mctx MathContext
     */
    public CasinoTaux(char[] val, int offset, int len, MathContext mctx)
    {
        super(val, offset, len, mctx); 

    }

    /**
     * methode valueOf : Convertit un BigDecimal en CasinoTaux et retourne une nouvelle instance.
     * 
     * @param val BigDecimal à convertir
     * @return CasinoTaux
     */
    public static CasinoTaux valueOf(BigDecimal val)
    {
        return new CasinoTaux(val.toString());
    }

    /**
     * methode round2Dec :  Appelle la méthode setScale de BigDecimal et retourne un nouveau CasinoTaux
     *                      arrondi à 2 décimales avec le mode d'arrondi HALF_UP (arrondi supérieur de n,nn50)
     * 
     * @return a <tt>CasinoTaux</tt> rounded according to the 
     *         <tt>HALF_UP</tt> settings.
     */
    public CasinoTaux round2Dec() 
    {
        return CasinoTaux.valueOf(super.setScale(2, ROUND_HALF_UP));
    }

    /**
     * methode round4Dec :  Appelle la méthode setScale de BigDecimal et retourne un nouveau CasinoTaux
     *                      arrondi à 4 décimales avec le mode d'arrondi HALF_UP (arrondi supérieur de n,nnnn50)
     * 
     * @return a <tt>CasinoTaux</tt> rounded according to the 
     *         <tt>HALF_UP</tt> settings.
     */
    public CasinoTaux round4Dec()
    {
        return CasinoTaux.valueOf(super.setScale(4, ROUND_HALF_UP));
    }

    /**
     * methode subtract : Appelle la méthode subtract de BigDecimal et retourne un nouveau CasinoTaux.
     * 
     * @param  subtrahend value to be subtracted from this <tt>CasinoTaux</tt>.
     * @return CasinoTaux = <tt>this - subtrahend</tt>
     */
    public CasinoTaux subtract(BigDecimal subtrahend)
    {
        return CasinoTaux.valueOf(super.subtract(subtrahend));
    }


}
