/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 */

package fr.gouv.finances.cp.dmo.entite;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import fr.gouv.finances.lombok.jpa.util.BaseEntity;

/**
 * Entité ContratMensu.
 * 
 * @author chouard-cp
 * @author CF : migration vers JPA
 * @version $Revision: 1.4 $ Date: 14 déc. 2009
 */
@Entity
@Table(name = "CONTRATMENSUALISATION_COME")
@SequenceGenerator(name = "CONTRATMENSU_ID_GENERATOR", sequenceName = "SEQ_CONTRATMENSU_COME", allocationSize = 1)
public class ContratMensu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private static final int NOMBRE_JOURS_VALIDITE = 60;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTRATMENSU_ID_GENERATOR")
    @Column(name = "CONTRATMENSU_ID")
    private Long id;

    // @Column(name = "NUMEROCONTRAT")
    private Long numeroContrat;

    @Column(nullable = true, name = "ANNEEPRISEEFFET")
    private Long anneePriseEffet;

    @Column(nullable = false, name = "ESTTRAITE")
    private Boolean estTraite = Boolean.FALSE;

    @Column(nullable = false, name = "DATEDEMANDE")
    private Date dateDemande = Calendar.getInstance().getTime();

    @ManyToOne(fetch = FetchType.EAGER) // defaut est EAGER. Unidirectionnel.
    @JoinColumn(name = "TYPEIMPOT_ID_TYPEIMPOT", foreignKey = @ForeignKey(name = "FK_COME_TYIM_1"))
    private TypeImpot typeImpot;

    @ManyToOne(cascade = CascadeType.ALL) // Unidirectionnel
    @JoinColumn(name = "RIB_ID_RIB", foreignKey = @ForeignKey(name = "FK_COME_RIB_1"))
    private Rib rib;

    // @Cascade(CascadeType.SAVE_UPDATE, CascadeType.DELETE)
    @ManyToOne(cascade = CascadeType.PERSIST) // bidirectionnel
    @JoinColumn(nullable = false, name = "CONT_ID_LISTECONTRATMENSU")
    private ContribuablePar contribuablePar;

    public ContratMensu()
    {
        super();
        this.contribuablePar = new ContribuablePar();
        this.rib = new Rib();
        this.typeImpot = new TypeImpot();
    }

    public ContratMensu(Long anneepriseeffet, Boolean esttraite, Date datedemande, ContribuablePar contribuablepar,
        Long id, Long numerocontrat, Rib rib, TypeImpot typeimpot)
    {
        super();
        this.anneePriseEffet = anneepriseeffet;
        this.estTraite = esttraite;
        this.dateDemande = datedemande;
        this.contribuablePar = contribuablepar;
        this.id = id;
        this.numeroContrat = numerocontrat;
        this.rib = rib;
        this.typeImpot = typeimpot;
    }

    public ContratMensu(TypeImpot typeimpot, Rib rib)
    {
        super();
        this.typeImpot = typeimpot;
        this.rib = rib;
    }

    public Long getAnneePriseEffet()
    {
        return anneePriseEffet;
    }

    public ContribuablePar getContribuablePar()
    {
        return contribuablePar;
    }

    public Date getDateDemande()
    {
        return dateDemande;
    }

    public Boolean getEstTraite()
    {
        return estTraite;
    }

    public Long getId()
    {
        return id;
    }

    public Long getNumeroContrat()
    {
        return numeroContrat;
    }

    public Rib getRib()
    {
        return rib;
    }

    public TypeImpot getTypeImpot()
    {
        return typeImpot;
    }

    public void setAnneePriseEffet(Long anneepriseeffet)
    {
        this.anneePriseEffet = anneepriseeffet;
    }

    public void setContribuablePar(ContribuablePar contribuablepar)
    {
        this.contribuablePar = contribuablepar;
    }

    public void setDateDemande(Date datedemande)
    {
        this.dateDemande = datedemande;
    }

    public void setEstTraite(Boolean esttraite)
    {
        this.estTraite = esttraite;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setNumeroContrat(Long numerocontrat)
    {
        this.numeroContrat = numerocontrat;
    }

    public void setRib(Rib rib)
    {
        this.rib = rib;
    }

    public void setTypeImpot(TypeImpot typeimpot)
    {
        this.typeImpot = typeimpot;
    }

    /**
     * methode Verifier non perime r g70 : DGFiP.
     * 
     * @return true, si c'est vrai
     */
    public boolean verifierNonPerimeRG70()
    {
        boolean estperime = false;

        if (getDateDemande().before(getDateLimiteValidite()))
        {
            estperime = true;
        }
        else
        {
            estperime = false;
        }
        return estperime;

    }

    /**
     * Accesseur de l attribut date limite validite.
     * 
     * @return date limite validite
     */
    public static Date getDateLimiteValidite()
    {
        String int24 = "24";
        String int3600 = "3600";
        String int1000 = "1000";
        Date datelimitevalidite =
            new Date(Calendar.getInstance().getTimeInMillis() - ContratMensu.NOMBRE_JOURS_VALIDITE
                * new Integer(int24).intValue() * new Integer(int3600).intValue() * new Integer(int1000).intValue());
        return datelimitevalidite;
    }

    /**
     * Hash code.
     * 
     * @return the int
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        int hash = hashCODEHASHINIT;
        int varCode = 0;

        varCode = (null == numeroContrat ? 0 : numeroContrat.hashCode());
        hash = hashCODEHASHMULT * hash + varCode;
        return hash;
    }

    /**
     * Equals.
     * 
     * @param object param
     * @return true, if equals
     * @see java.lang.Object#equals(Object)
     */
    @Override
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }

        if (object == null || (object.getClass() != this.getClass()))
        {
            return false;
        }

        ContratMensu myObj = (ContratMensu) object;

        return ((numeroContrat == null ? myObj.getNumeroContrat() == null : numeroContrat.equals(myObj
            .getNumeroContrat())));
    }

}
