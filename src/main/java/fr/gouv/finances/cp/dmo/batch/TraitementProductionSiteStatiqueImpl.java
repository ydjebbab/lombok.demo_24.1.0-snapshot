/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementProductionSiteStatiqueImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.batch;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.bean.JobHistory;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ExploitationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

/**
 * Batch TraitementProductionSiteStatiqueImpl 
 *
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@Service("traitementproductionsitestatique")
@Profile("batch")
@Lazy(true)
public class TraitementProductionSiteStatiqueImpl extends ServiceBatchCommunImpl {

    /**
     * editiondemandeserviceso.
     */
    @Autowired()
    @Qualifier("editiondemandeserviceimpl")
    private EditionDemandeService editiondemandeserviceso;

    /**
     * repertoire site statique.
     */
    private String repertoireSiteStatique;

    /**
     * base repertoire site statique.
     */
    private String baseRepertoireSiteStatique;

    /**
     * base uri site statique.
     */
    private String baseURISiteStatique;

    /**
     * Constructeur
     */
    public TraitementProductionSiteStatiqueImpl() {
        super();
    }

    @Value("${traitementproductionsitestatique.versionbatch}")
    @Override()
    public void setVersionBatch(String versionBatch) {
        super.setVersionBatch(versionBatch);
    }

    @Value("${traitementproductionsitestatique.nombatch}")
    @Override()
    public void setNomBatch(String nomBatch) {
        super.setNomBatch(nomBatch);
    }
    /**
     * Accesseur de l attribut base repertoire site statique.
     *
     * @return base repertoire site statique
     */
    public String getBaseRepertoireSiteStatique() {
        return baseRepertoireSiteStatique;
    }

    /**
     * Accesseur de l attribut base uri site statique.
     *
     * @return base uri site statique
     */
    public String getBaseURISiteStatique() {
        return baseURISiteStatique;
    }

    /**
     * Gets the editiondemandeserviceso.
     *
     * @return the editiondemandeserviceso
     */
    public EditionDemandeService getEditiondemandeserviceso() {
        return editiondemandeserviceso;
    }

    /**
     * Accesseur de l attribut repertoire site statique.
     *
     * @return repertoire site statique
     */
    public String getRepertoireSiteStatique() {
        return repertoireSiteStatique;
    }

    /**
     * Modificateur de l attribut base repertoire site statique.
     *
     * @param baseRepertoireSiteStatique le nouveau base repertoire site statique
     */
    @Value("${traitementproductionsitestatique.baserepertoiresitestatique}")
    public void setBaseRepertoireSiteStatique(String baseRepertoireSiteStatique) {
        this.baseRepertoireSiteStatique = baseRepertoireSiteStatique;
    }

    /**
     * Modificateur de l attribut base uri site statique.
     *
     * @param baseURISiteStatique le nouveau base uri site statique
     */
    public void setBaseURISiteStatique(String baseURISiteStatique) {
        this.baseURISiteStatique = baseURISiteStatique;
    }

    /**
     * Sets the editiondemandeserviceso.
     *
     * @param editiondemandeserviceso the editiondemandeserviceso to set
     */
    public void setEditiondemandeserviceso(EditionDemandeService editiondemandeserviceso) {
        this.editiondemandeserviceso = editiondemandeserviceso;
    }

    /**
     * Modificateur de l attribut repertoire site statique.
     *
     * @param repertoireSiteStatique le nouveau repertoire site statique
     */
    @Value("${traitementproductionsitestatique.repertoiresitestatique}")
    public void setRepertoireSiteStatique(String repertoireSiteStatique) {
        this.repertoireSiteStatique = repertoireSiteStatique;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     *
     * @see fr.gouv.finances.lombok.batch.ServiceBatchCommunImpl#traiterBatch()
     */
    @Override
    public void traiterBatch() {
        // Initialisation de l'édition
        EditionSynchroneNonNotifieeParMail editionSynchroneNonNotifieeParMail = editiondemandeserviceso.initEditionSynchroneNonNotifieeParMail("zf10.agregaviscontriparcodepost.edition", "wpetit-cp");
        // Paramètres de l'édition
        Map<String, String> parametresEdition = new HashMap<>();
        String repertoireCompletSiteStatique = this.baseRepertoireSiteStatique + "/" + this.repertoireSiteStatique;
        parametresEdition.put("repertoireCompletSiteStatique", repertoireCompletSiteStatique);
        parametresEdition.put("repertoireBaseImageDansSiteStatique", this.repertoireSiteStatique);
        editionSynchroneNonNotifieeParMail = editiondemandeserviceso.declencherEdition(editionSynchroneNonNotifieeParMail, parametresEdition);
        if (editionSynchroneNonNotifieeParMail != null && editionSynchroneNonNotifieeParMail.isDisponible()) {
            FichierJoint fichierJoint = editiondemandeserviceso.rechercherEdition((JobHistory) editionSynchroneNonNotifieeParMail);
            // Stockage du fichier dans l'arborescence cible du site
            // statique
            stockerSurDisque(fichierJoint, repertoireCompletSiteStatique);
        // Suppression de l'édition dans le repository
        // editiontraitementdemandeserviceso.supprimerEdition((JobHistory)
        // editionSynchroneNonNotifieeParMail);
        } else {
            if (editionSynchroneNonNotifieeParMail != null && editionSynchroneNonNotifieeParMail.getOrdoEdition() != null) {
                log.warn(editionSynchroneNonNotifieeParMail.getOrdoEdition().getMessage());
            }
        }
    }

    /**
     * methode Control and create dir 
     *
     * @param repertoire param
     */
    private void controlAndCreateDir(String repertoire) {
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource resourcerep = drl.getResource(repertoire);
        // Création du répertoire si nécessaire
        File filerep;
        try {
            filerep = resourcerep.getFile();
        } catch (IOException e) {
            throw ApplicationExceptionTransformateur.transformer(e);
        }
        if (!filerep.exists()) {
            boolean result;
            result = filerep.mkdirs();
            // thread => OK
            if ((!result) && (!filerep.exists())) {
                throw new ExploitationException("Impossible de créer le répertoire " + repertoire);
            }
        }
    }

    /**
     * methode Stocke sur disque 
     *
     * @param fichierJoint param
     * @param repertoire param
     */
    private void stockerSurDisque(FichierJoint fichierJoint, String repertoire) {
        // test sur non nullité des arguments
        this.controlAndCreateDir(repertoire);
        DefaultResourceLoader drl = new DefaultResourceLoader();
        Resource resource = drl.getResource(repertoire + "/" + fichierJoint.getNomFichierOriginal());
        FileOutputStream output = null;
        try {
            output = new FileOutputStream(resource.getFile());
            output.write(fichierJoint.getLeContenuDuFichier().getData());
            output.flush();
        } catch (IOException exc) {
            log.error("Erreur : ", exc);
        } finally {
            try {
                if (output != null) {
                    // ferme le stream
                    output.close();
                }
            } catch (IOException ex) {
                log.error("Impossible de fermer le fichier", ex);
            }
        }
    }

  
}
