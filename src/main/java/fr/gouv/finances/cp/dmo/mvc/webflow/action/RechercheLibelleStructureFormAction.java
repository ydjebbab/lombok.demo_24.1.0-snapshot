/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.beans.PropertyEditor;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyEditorRegistry;
import org.springframework.validation.Errors;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.action.FormObjectAccessor;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.ScopeType;

import fr.gouv.finances.cp.dmo.mvc.form.RechercheLibelleStructureForm;
import fr.gouv.finances.lombok.adresse.bean.Departement;
import fr.gouv.finances.lombok.adresse.service.AdresseService;
import fr.gouv.finances.lombok.structure.bean.CategorieStructureCP;
import fr.gouv.finances.lombok.structure.bean.DomaineCP;
import fr.gouv.finances.lombok.structure.bean.FonctionCP;
import fr.gouv.finances.lombok.structure.bean.ModeGestionCP;
import fr.gouv.finances.lombok.structure.bean.StructureCP;
import fr.gouv.finances.lombok.structure.bean.TypePoleActiviteCP;
import fr.gouv.finances.lombok.structure.bean.TypeServiceCP;
import fr.gouv.finances.lombok.structure.service.LectureReferencesStructureCPService;
import fr.gouv.finances.lombok.structure.service.LectureStructureCPService;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.propertyeditor.SelectEditor;

/**
 * Class RechercheLibelleStructureFormAction
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class RechercheLibelleStructureFormAction extends FormAction
{
    private static final Logger log = LoggerFactory.getLogger(RechercheLibelleStructureFormAction.class);

    private LectureReferencesStructureCPService lecturereferencesstructurecpserviceso;

    private LectureStructureCPService lecturestructurecpserviceso;

    private AdresseService adresseserviceso;

    public RechercheLibelleStructureFormAction()
    {
        super();
    }

    public RechercheLibelleStructureFormAction(Class<?> formObjectClass)
    {
        super(formObjectClass);
    }

    public void setAdresseserviceso(AdresseService adresseserviceso)
    {
        this.adresseserviceso = adresseserviceso;
    }

    public void setLecturereferencesstructurecpserviceso(
        LectureReferencesStructureCPService lecturereferencesstructurecpserviceso)
    {
        this.lecturereferencesstructurecpserviceso = lecturereferencesstructurecpserviceso;
    }

    public void setLecturestructurecpserviceso(LectureStructureCPService lecturestructurecpserviceso)
    {
        this.lecturestructurecpserviceso = lecturestructurecpserviceso;
    }

    /**
     * methode Consulter detail structure .
     * 
     * @param context param
     * @return event
     */
    @SuppressWarnings("unchecked")
    public Event consulterDetailStructure(RequestContext context)
    {
        Event result;

        List<StructureCP> structuresCP = (List<StructureCP>) context.getFlowScope().get("structurescp");
        PropertyEditor propertyEditor = new SelectEditor(structuresCP, "id");

        propertyEditor.setAsText(context.getRequestParameters().get("id"));
        StructureCP uneStructureSelectionnee = (StructureCP) propertyEditor.getValue();
        StructureCP structureSelectionneComplete =
            this.lecturestructurecpserviceso
                .rechercherUneStructureCPEtTousLesElementsAttachesParIdentiteNominoe(uneStructureSelectionnee
                    .getIdentiteNOMINOE());
        if (uneStructureSelectionnee != null)
        {
            RechercheLibelleStructureForm form =
                (RechercheLibelleStructureForm) context.getFlowScope().get(getFormObjectName());
            form.setUneStructureSelectionnee(structureSelectionneComplete);
            result = success();
        }
        else
        {
            result = error();
        }
        return result;
    }

    /**
     * methode Executer rechercher structures multi criteres
     * 
     * @param context param
     * @return event
     */
    public Event executerRechercherStructuresMultiCriteres(RequestContext context)
    {
        List<StructureCP> structuresCP = new ArrayList<>();
        context.getFlowScope().put("structurescp", structuresCP);
        return success();
    }

    /**
     * methode Executer rechercher structures par categorie et par departement
     * 
     * @param context param
     * @return event
     */
    public Event executerRechercherStructuresParCategorieEtParDepartement(RequestContext context)
    {
        RechercheLibelleStructureForm form =
            (RechercheLibelleStructureForm) context.getFlowScope().get(getFormObjectName());

        List<StructureCP> structuresCP =
            this.lecturestructurecpserviceso.rechercherDesStructuresCPActivesParCodescategoriesEtParDepartement(form
                .getCodesCategoriesStructureCP(), form.getDepartement());

        context.getFlowScope().put("structurescp", structuresCP);
        return success();
    }

    /**
     * methode Executer rechercher structures par code annexe et par departement
     * 
     * @param context param
     * @return event
     */
    public Event executerRechercherStructuresParCodeAnnexeEtParDepartement(RequestContext context)
    {
        RechercheLibelleStructureForm form =
            (RechercheLibelleStructureForm) context.getFlowScope().get(getFormObjectName());

        List<StructureCP> structuresCP =
            this.lecturestructurecpserviceso.rechercherDesStructuresActivesCPParCodeAnnexeParDepartement(form
                .getCodesAnnexes(), form.getDepartement());

        context.getFlowScope().put("structurescp", structuresCP);
        return success();
    }

    /**
     * methode Executer rechercher structures par codes annexes
     * 
     * @param context param
     * @return event
     */
    public Event executerRechercherStructuresParCodesAnnexes(RequestContext context)
    {
        RechercheLibelleStructureForm form =
            (RechercheLibelleStructureForm) context.getFlowScope().get(getFormObjectName());

        List<StructureCP> structuresCP =
            this.lecturestructurecpserviceso
                .rechercherDesStructuresCPActivesParCodesAnnexesAvecDepartementAvecRegion(form.getCodesAnnexes());

        context.getFlowScope().put("structurescp", structuresCP);
        return success();
    }

    /**
     * methode Executer rechercher structures par codes annexes et par categories
     * 
     * @param context param
     * @return event
     */
    public Event executerRechercherStructuresParCodesAnnexesEtParCategories(RequestContext context)
    {
        RechercheLibelleStructureForm form =
            (RechercheLibelleStructureForm) context.getFlowScope().get(getFormObjectName());

        List<StructureCP> structuresCP =
            this.lecturestructurecpserviceso.rechercherDesStructuresCPActivesParCodesAnnexesParCodescategories(form
                .getCodesCategoriesStructureCP(), form.getCodesAnnexes());

        context.getFlowScope().put("structurescp", structuresCP);
        return success();
    }

    /**
     * methode Executer rechercher structures par codes annexes et par categories et par departement
     * 
     * @param context param
     * @return event
     */
    public Event executerRechercherStructuresParCodesAnnexesEtParCategoriesEtParDepartement(RequestContext context)
    {
        RechercheLibelleStructureForm form =
            (RechercheLibelleStructureForm) context.getFlowScope().get(getFormObjectName());

        List<StructureCP> structuresCP =
            this.lecturestructurecpserviceso.rechercherDesStructuresCPParCodesAnnexesParCodesCategoriesParDepartement(
                form.getCodesCategoriesStructureCP(), form.getCodesAnnexes(), form.getDepartement());

        context.getFlowScope().put("structurescp", structuresCP);
        return success();
    }

    /**
     * methode Executer rechercher structures par codes categories
     * 
     * @param context param
     * @return event
     */
    public Event executerRechercherStructuresParCodesCategories(RequestContext context)
    {
        RechercheLibelleStructureForm form =
            (RechercheLibelleStructureForm) context.getFlowScope().get(getFormObjectName());
        List<StructureCP> structuresCP =
            this.lecturestructurecpserviceso
                .rechercherDesStructuresCPActivesParCodescategoriesAvecDepartementEtRegion(form
                    .getCodesCategoriesStructureCP());

        context.getFlowScope().put("structurescp", structuresCP);
        return success();
    }

    /**
     * methode Executer rechercher structures par departement .
     * 
     * @param context param
     * @return event
     */
    public Event executerRechercherStructuresParDepartement(RequestContext context)
    {
        RechercheLibelleStructureForm form =
            (RechercheLibelleStructureForm) context.getFlowScope().get(getFormObjectName());

        List<StructureCP> structuresCP =
            this.lecturestructurecpserviceso.rechercherDesStructuresActivesCPParDepartement(form.getDepartement());

        context.getFlowScope().put("structurescp", structuresCP);
        return success();
    }

    /**
     * methode Preparer liste des categories
     * 
     * @param context param
     * @return event
     */
    public Event preparerListeDesCategories(RequestContext context)
    {
        List<CategorieStructureCP> categoriesStructuresCP =
            this.lecturereferencesstructurecpserviceso.rechercherToutesCategorieStructureCP();
        context.getFlowScope().put("categoriesstructurescp", categoriesStructuresCP);
        return success();
    }

    /**
     * methode Preparer liste des codes annexes structures cp
     * 
     * @param context param
     * @return event
     */
    public Event preparerListeDesCodesAnnexesStructuresCP(RequestContext context)
    {
        List<String> codesAnnexesStructureCP =
            this.lecturereferencesstructurecpserviceso.rechercherTousLesCodeAnnexeStructureCP();
        context.getFlowScope().put("codesAnnexesStructureCP", codesAnnexesStructureCP);
        return success();
    }

    /**
     * methode Preparer liste des departements
     * 
     * @param context param
     * @return event
     */
    public Event preparerListeDesDepartements(RequestContext context)
    {
        List<Departement> departements = this.adresseserviceso.rechercherTousLesDepartementsAvecRegion();
        context.getFlowScope().put("departements", departements);
        return success();
    }

    /**
     * methode Preparer liste des domaines cp
     * 
     * @param context param
     * @return event
     */
    public Event preparerListeDesDomainesCP(RequestContext context)
    {
        List<DomaineCP> domainesCP = this.lecturereferencesstructurecpserviceso.rechercherTousDomaineCP();
        context.getFlowScope().put("domainesCP", domainesCP);
        return success();
    }

    /**
     * methode Preparer liste des etats structures cp
     * 
     * @param context param
     * @return event
     */
    public Event preparerListeDesEtatsStructuresCP(RequestContext context)
    {
        List<String> etatsStructureCP = this.lecturereferencesstructurecpserviceso.rechercherTousLesEtatsStructureCP();
        context.getFlowScope().put("etatsStructureCP", etatsStructureCP);
        return success();
    }

    /**
     * methode Preparer liste des fonctions cp
     * 
     * @param context param
     * @return event
     */
    public Event preparerListeDesFonctionsCP(RequestContext context)
    {
        List<FonctionCP> fonctionsCP = this.lecturereferencesstructurecpserviceso.rechercherToutesFonctionCP();
        context.getFlowScope().put("fonctionsCP", fonctionsCP);
        return success();
    }

    /**
     * methode Preparer liste des modes gestion cp
     * 
     * @param context param
     * @return event
     */
    public Event preparerListeDesModesGestionCP(RequestContext context)
    {
        List<ModeGestionCP> modesGestionsCP = this.lecturereferencesstructurecpserviceso.rechercherTousModeGestionCP();
        context.getFlowScope().put("modesGestionsCP", modesGestionsCP);
        return success();
    }

    /**
     * methode Preparer liste des types poles activite cp
     * 
     * @param context param
     * @return event
     */
    public Event preparerListeDesTypesPolesActiviteCP(RequestContext context)
    {
        List<TypePoleActiviteCP> typesPolesActivitesCP =
            this.lecturereferencesstructurecpserviceso.rechercherTousTypePoleActiviteCP();
        context.getFlowScope().put("typesPolesActivitesCP", typesPolesActivitesCP);
        return success();
    }

    /**
     * methode Preparer liste des types services cp
     * 
     * @param context param
     * @return event
     */
    public Event preparerListeDesTypesServicesCP(RequestContext context)
    {
        List<TypeServiceCP> typesServicesCP = this.lecturereferencesstructurecpserviceso.rechercherTousTypeServiceCP();
        context.getFlowScope().put("typesServicesCP", typesServicesCP);
        return success();
    }

    /**
     * methode Preparer rechercher des structures
     * 
     * @param rc param
     * @return event
     */
    public Event preparerRechercherDesStructures(RequestContext rc)
    {
        RechercheLibelleStructureForm form = (RechercheLibelleStructureForm) rc.getFlowScope().get(getFormObjectName());
        String codiquerecherche = form.getUnestructurearechercher().getCodique();
        String codeannexerecherche = form.getUnestructurearechercher().getCodeAnnexe();
        StructureCP uneStructure =
            this.lecturestructurecpserviceso.rechercherUneStructureCPParCodiqueEtCodeAnnexe(codiquerecherche,
                codeannexerecherche);
        StructureCP uneStrucutreavectouselements =
            this.lecturestructurecpserviceso
                .rechercherUneStructureCPEtTousLesElementsAttachesParIdentiteNominoe(uneStructure.getIdentiteNOMINOE());

        form.setUnestructurearechercher(uneStrucutreavectouselements);
        return success();
    }

    /**
     * methode Valider verifier existence du codique et annexe
     * 
     * @param context param
     * @return event
     */
    public Event validerVerifierExistenceDuCodiqueEtAnnexe(RequestContext context)
    {
        RechercheLibelleStructureForm form =
            (RechercheLibelleStructureForm) context.getFlowScope().getRequired(getFormObjectName(),
                RechercheLibelleStructureForm.class);

        StructureCP structureARechercher = form.getUnestructurearechercher();
        try
        {
            this.lecturestructurecpserviceso.verifierExistenceStructure(structureARechercher.getCodique(),
                structureARechercher.getCodeAnnexe());
        }
        catch (RegleGestionException rgExc)
        {
            log.error(rgExc.getMessage(), rgExc);
            Errors errors = new FormObjectAccessor(context).getFormErrors(getFormObjectName(), ScopeType.REQUEST);
            errors.reject(null, rgExc.getMessage());
            return error();
        }
        return success();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param rc
     * @param per
     * @see org.springframework.webflow.action.FormAction#registerPropertyEditors(org.springframework.webflow.execution.RequestContext,
     *      org.springframework.beans.PropertyEditorRegistry)
     */
    @Override
    protected void registerPropertyEditors(RequestContext rc, PropertyEditorRegistry per)
    {
        super.registerPropertyEditors(rc, per);
        List<Departement> listeDepartements = (List<Departement>) rc.getFlowScope().get("departements");
        per.registerCustomEditor(Departement.class, new SelectEditor(listeDepartements, "codedepartementINSEE"));
    }

}
