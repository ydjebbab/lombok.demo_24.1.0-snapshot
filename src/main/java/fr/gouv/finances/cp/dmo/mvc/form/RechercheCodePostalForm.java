/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : RechercheCodePostalForm.java
 *
 */
package fr.gouv.finances.cp.dmo.mvc.form;

import java.io.Serializable;

import fr.gouv.finances.cp.dmo.techbean.CriteresRechercheCodePostal;

/**
 * Class RechercheCodePostalForm DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class RechercheCodePostalForm implements Serializable
{

    /** Constant : serialVersionUID. */
    private static final long serialVersionUID = 6718130656721478851L;

    /** critere. */
    private CriteresRechercheCodePostal critere;

    /**
     * Instanciation de recherche code postal form.
     */
    public RechercheCodePostalForm()
    {
        this.critere = new CriteresRechercheCodePostal();
    }

    /**
     * Gets the critere.
     *
     * @return the critere
     */
    public CriteresRechercheCodePostal getCritere()
    {
        return critere;
    }

    /**
     * Sets the critere.
     *
     * @param critere the new critere
     */
    public void setCritere(CriteresRechercheCodePostal critere)
    {
        this.critere = critere;
    }

}
