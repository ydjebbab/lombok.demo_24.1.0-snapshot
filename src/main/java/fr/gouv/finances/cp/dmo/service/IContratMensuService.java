/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ContratMensuService.java
 *
 */
package fr.gouv.finances.cp.dmo.service;

import org.dom4j.Document;

/**
 * Interface IContratMensuService
 * 
 * @author chouard-cp
 * @author CF
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public interface IContratMensuService
{

    /**
     * methode Recuperer rapport xml contrats non traite verifiant r g70 : DGFiP.
     * 
     * @return document
     */
    public Document recupererRapportXmlContratsNonTraiteVerifiantRG70();

    /**
     * methode Reporter annee prise effet pour contrats non traites ne verifiant pas r g71 : DGFiP.
     * 
     * @return integer
     */
    public Integer reporterAnneePriseEffetPourContratsNonTraitesNeVerifiantPasRG71();

    /**
     * methode Supprimer contrats ne verifiant pas r g70 et prevenir contribuable par mail : DGFiP.
     */
    public void supprimerContratsNeVerifiantPasRG70EtPrevenirContribuableParMail();
}