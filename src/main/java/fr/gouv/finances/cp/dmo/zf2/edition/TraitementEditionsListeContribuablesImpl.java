/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementEditionsListeContribuablesImpl.java
 *
 */
package fr.gouv.finances.cp.dmo.zf2.edition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import fr.gouv.finances.cp.dmo.dao.IContribuableDao;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.lombok.edition.bean.AbstractParametrageVueJasper;
import fr.gouv.finances.lombok.edition.service.impl.AbstractServiceEditionCommunImpl;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.persistance.ScrollIterator;

/**
 * Class TraitementEditionsListeContribuablesImpl DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
public class TraitementEditionsListeContribuablesImpl extends AbstractServiceEditionCommunImpl
{

    /** transaction template. */
    private TransactionTemplate transactionTemplate;

    /** contribuablebatchdao. */
    private IContribuableDao contribuabledao;

    /** contribuableserviceso. */
    private IContribuableService contribuableserviceso;

    /**
     * Constructeur
     */
    public TraitementEditionsListeContribuablesImpl()
    {
        super();
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return collection
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceVirtualEditionCommunImpl#creerDatasource(java.util.Map)
     */
    @Override
    public Collection creerDatasource(Map parametresEdition)
    {

        List tousLesContribuables = new ArrayList();

        try
        {
            for (int j = 1; j < 10; j++)
            {
                Object lesContribuables = transactionTemplate.execute(new TransactionCallback<Object>()
                {
                    public List doInTransaction(TransactionStatus arg0)
                    {
                        List lesContribuables = new ArrayList();
                        ScrollIterator scrollIterator = contribuabledao.findTousContribuableParIterator(100);
                        while (scrollIterator.hasNext())
                        {
                            ContribuablePar unContribuable = (ContribuablePar) scrollIterator.nextObjetMetier();
                            // if
                            // (unContribuable.getClass().isInstance(ContribuablePar.class)&&unContribuable.getAdressePrincipale()!=null
                            // &&unContribuable.getListeAdresses()!=null){

                            lesContribuables.add(unContribuable);
                            // }

                            // On vide la session une fois l'objet lu pour éviter une
                            // exception levée
                            // par Hibernate lorsqu'un collection est associée à deux
                            // sessions
                            contribuabledao.clearPersistenceContext();
                        }
                        return lesContribuables;
                    }
                });
                tousLesContribuables.addAll((Collection) lesContribuables);
            }
        }
        catch (RuntimeException e)
        {
            throw ApplicationExceptionTransformateur.transformer("Erreur lors de la production de l'édition de la liste des contribuables",
                e);
        }

        // return null;

        /*
         * List lesContribuables =new ArrayList(); for (int i = 0; i <
         * Integer.parseInt(parametresEdition.get("nbredelancement").toString()); i++) { for (int j = 1; j < 10; j++){
         * CriteresRecherches criteresrecherches = new CriteresRecherches(); criteresrecherches.setTypeContriPart(true);
         * criteresrecherches.setIdentifiant("usertest" +j); List lesContribuablesj=
         * contribuableserviceso.rechercherListeContribuable(criteresrecherches);
         * lesContribuables.addAll(lesContribuablesj);}}
         */

        return tousLesContribuables;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return string
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceVirtualEditionCommunImpl#creerNomDuFichier(java.util.Map)
     */
    @Override
    public String creerNomDuFichier(Map parametresEdition)
    {
        String result = null;
        if (parametresEdition != null)
        {
            result = (String) parametresEdition.get(AbstractParametrageVueJasper.REPORT_FILENAME_KEY);
        }
        return result;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return map
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceVirtualEditionCommunImpl#creerParametresJasperPourLEntete(java.util.Map)
     */
    @Override
    public Map creerParametresJasperPourLEntete(Map parametresEdition)
    {
        return null;
    }

    /**
     * Accesseur de l attribut contribuabledao.
     * 
     * @return contribuabledao
     */
    public IContribuableDao getContribuabledao()
    {
        return contribuabledao;
    }

    /**
     * Accesseur de l attribut contribuableserviceso.
     * 
     * @return contribuableserviceso
     */
    public IContribuableService getContribuableserviceso()
    {
        return contribuableserviceso;
    }

    /**
     * (methode de remplacement) {@inheritDoc}.
     * 
     * @param parametresEdition
     * @return description
     * @see fr.gouv.finances.lombok.edition.service.impl.AbstractServiceVirtualEditionCommunImpl#getDescription(java.util.Map)
     */
    @Override
    public String getDescription(Map parametresEdition)
    {
        return "contribuables";

    }

    /**
     * Modificateur de l attribut contribuabledao.
     * 
     * @param contribuabledao le nouveau contribuabledao
     */
    public void setContribuabledao(IContribuableDao contribuabledao)
    {
        this.contribuabledao = contribuabledao;
    }

    /**
     * Modificateur de l attribut contribuableserviceso.
     * 
     * @param contribuableserviceso le nouveau contribuableserviceso
     */
    public void setContribuableserviceso(IContribuableService contribuableserviceso)
    {
        this.contribuableserviceso = contribuableserviceso;
    }

    /**
     * Accesseur de l attribut transaction template.
     * 
     * @return transaction template
     */
    public TransactionTemplate getTransactionTemplate()
    {
        return transactionTemplate;
    }

    /**
     * Modificateur de l attribut transaction template.
     * 
     * @param transactionTemplate le nouveau transaction template
     */
    public void setTransactionTemplate(TransactionTemplate transactionTemplate)
    {
        this.transactionTemplate = transactionTemplate;
    }

}
