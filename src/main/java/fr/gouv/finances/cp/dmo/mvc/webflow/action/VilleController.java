/*
 * Copyright (c) 2015 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.mvc.webflow.action;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.gouv.finances.lombok.adresse.dao.AdresseDao;

/**
 * Test Lombok avec client riche appelant des services REST.
 * 
 * @author : CF : migration vers JPA
 */
@Controller
@RequestMapping("/controller")
public class VilleController
{
    private static final Logger log = LoggerFactory.getLogger(VilleController.class);

    @Autowired
    private AdresseDao adressedao;

    public VilleController()
    {
        super();
    }

    /**
     * Gets the adressedao.
     *
     * @return the adressedao
     */
    public AdresseDao getAdressedao()
    {
        return adressedao;
    }

    /**
     * Sets the adressedao.
     *
     * @param adressedao the new adressedao
     */
    public void setAdressedao(AdresseDao adressedao)
    {
        this.adressedao = adressedao;
    }

    /**
     * Web service
     * 
     * @param query
     * @return country list
     */
    @RequestMapping(value = "/get_ville_list", method = RequestMethod.GET, headers = "Accept=*/*")
    @ResponseBody
    public List<String> getCountryList(@RequestParam("term") String query)
    {
        log.debug(">>> Debut getCountryList");
        List<String> laListe = adressedao.findLesCommunesParDebutLibelleCommune(query);
        return laListe;
    }

    /**
     * Web service
     *
     * @param query
     * @return pays list
     */
    @RequestMapping(value = "/get_pays_list", method = RequestMethod.GET, headers = "Accept=*/*")
    @ResponseBody
    public List<String> getPaysList(@RequestParam("term") String query)
    {
        log.debug(">>> Debut getPaysList");
        List<String> laListe = adressedao.findLesPaysParDebutLibellePays(query);
        return laListe;
    }

}
