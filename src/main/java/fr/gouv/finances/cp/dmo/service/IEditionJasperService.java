/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : EditionJasperService.java
 *
 */
package fr.gouv.finances.cp.dmo.service;

import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Interface IEditionJasperService
 * 
 * @author chouard-cp
 * @author CF
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public interface IEditionJasperService
{

    /**
     * methode Produit edition pdf : DGFiP.
     * 
     * @return fichier joint
     */
    public FichierJoint produitEditionPdf();
}
