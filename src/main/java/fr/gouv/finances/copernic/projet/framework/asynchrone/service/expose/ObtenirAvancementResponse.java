/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */

package fr.gouv.finances.copernic.projet.framework.asynchrone.service.expose;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import fr.gouv.impots.appli.commun.frameworks.gestionappelsservices.coordination.ov.AvancementValeur;

/**
 * <p>
 * Classe Java pour obtenirAvancementResponse complex type.
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="obtenirAvancementResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://asynchrone.objetsvaleurs.transverse.referentiels.commun.appli.impots.gouv.fr}AvancementValeur"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obtenirAvancementResponse", propOrder = {
        "result"
})
public class ObtenirAvancementResponse
{

    /** result. */
    @XmlElement(required = true, nillable = true)
    protected AvancementValeur result;

    /**
     * Accesseur de l attribut result.
     *
     * @return result
     */
    public AvancementValeur getResult()
    {
        return result;
    }

    /**
     * Modificateur de l attribut result.
     *
     * @param value le nouveau result
     */
    public void setResult(AvancementValeur value)
    {
        this.result = value;
    }

}
