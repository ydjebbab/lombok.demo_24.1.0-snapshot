/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */

package fr.gouv.finances.copernic.projet.framework.asynchrone.service.expose;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour obtenirIEISResponse complex type.
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="obtenirIEISResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obtenirIEISResponse", propOrder = {
        "result"
})
public class ObtenirIEISResponse
{

    /** result. */
    @XmlElement(required = true, nillable = true)
    protected byte[] result;

    /**
     * Accesseur de l attribut result.
     *
     * @return result
     */
    public byte[] getResult()
    {
        return result;
    }

    /**
     * Modificateur de l attribut result.
     *
     * @param value le nouveau result
     */
    public void setResult(byte[] value)
    {
        this.result = value;
    }

}
