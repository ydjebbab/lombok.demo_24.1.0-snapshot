/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.copernic.projet.framework.asynchrone.service.expose;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * fr.gouv.finances.copernic.projet.framework.asynchrone.service.expose package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 */
@XmlRegistry
public class ObjectFactory
{

    /** Constant : _ObtenirIEIS_QNAME. */
    private final static QName _ObtenirIEIS_QNAME = new QName(
        "http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr", "obtenirIEIS");

    /** Constant : _Acquitter_QNAME. */
    private final static QName _Acquitter_QNAME = new QName("http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr",
        "acquitter");

    /** Constant : _AcquitterResponse_QNAME. */
    private final static QName _AcquitterResponse_QNAME = new QName(
        "http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr", "acquitterResponse");

    /** Constant : _ObtenirIEISResponse_QNAME. */
    private final static QName _ObtenirIEISResponse_QNAME = new QName(
        "http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr", "obtenirIEISResponse");

    /** Constant : _ObtenirAvancementResponse_QNAME. */
    private final static QName _ObtenirAvancementResponse_QNAME = new QName(
        "http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr", "obtenirAvancementResponse");

    /** Constant : _ObtenirAvancement_QNAME. */
    private final static QName _ObtenirAvancement_QNAME = new QName(
        "http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr", "obtenirAvancement");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * fr.gouv.finances.copernic.projet.framework.asynchrone.service.expose
     */
    public ObjectFactory()
    {
    }

    /**
     * Create an instance of {@link Acquitter }.
     *
     * @return the acquitter
     */
    public Acquitter createAcquitter()
    {
        return new Acquitter();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Acquitter }{@code >}.
     *
     * @param value DOCUMENTEZ_MOI
     * @return the JAXB element< acquitter>
     */
    @XmlElementDecl(namespace = "http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr", name = "acquitter")
    public JAXBElement<Acquitter> createAcquitter(Acquitter value)
    {
        return new JAXBElement<Acquitter>(_Acquitter_QNAME, Acquitter.class, null, value);
    }

    /**
     * Create an instance of {@link AcquitterResponse }.
     *
     * @return the acquitter response
     */
    public AcquitterResponse createAcquitterResponse()
    {
        return new AcquitterResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcquitterResponse }{@code >}.
     *
     * @param value DOCUMENTEZ_MOI
     * @return the JAXB element< acquitter response>
     */
    @XmlElementDecl(namespace = "http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr", name = "acquitterResponse")
    public JAXBElement<AcquitterResponse> createAcquitterResponse(AcquitterResponse value)
    {
        return new JAXBElement<AcquitterResponse>(_AcquitterResponse_QNAME, AcquitterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link ObtenirAvancement }.
     *
     * @return the obtenir avancement
     */
    public ObtenirAvancement createObtenirAvancement()
    {
        return new ObtenirAvancement();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenirAvancement }{@code >}.
     *
     * @param value DOCUMENTEZ_MOI
     * @return the JAXB element< obtenir avancement>
     */
    @XmlElementDecl(namespace = "http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr", name = "obtenirAvancement")
    public JAXBElement<ObtenirAvancement> createObtenirAvancement(ObtenirAvancement value)
    {
        return new JAXBElement<ObtenirAvancement>(_ObtenirAvancement_QNAME, ObtenirAvancement.class, null, value);
    }

    /**
     * Create an instance of {@link ObtenirAvancementResponse }.
     *
     * @return the obtenir avancement response
     */
    public ObtenirAvancementResponse createObtenirAvancementResponse()
    {
        return new ObtenirAvancementResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenirAvancementResponse }{@code >}.
     *
     * @param value DOCUMENTEZ_MOI
     * @return the JAXB element< obtenir avancement response>
     */
    @XmlElementDecl(namespace = "http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr", name = "obtenirAvancementResponse")
    public JAXBElement<ObtenirAvancementResponse> createObtenirAvancementResponse(ObtenirAvancementResponse value)
    {
        return new JAXBElement<ObtenirAvancementResponse>(_ObtenirAvancementResponse_QNAME, ObtenirAvancementResponse.class, null, value);
    }

    /**
     * Create an instance of {@link ObtenirIEIS }.
     *
     * @return the obtenir ieis
     */
    public ObtenirIEIS createObtenirIEIS()
    {
        return new ObtenirIEIS();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenirIEIS }{@code >}.
     *
     * @param value DOCUMENTEZ_MOI
     * @return the JAXB element< obtenir iei s>
     */
    @XmlElementDecl(namespace = "http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr", name = "obtenirIEIS")
    public JAXBElement<ObtenirIEIS> createObtenirIEIS(ObtenirIEIS value)
    {
        return new JAXBElement<ObtenirIEIS>(_ObtenirIEIS_QNAME, ObtenirIEIS.class, null, value);
    }

    /**
     * Create an instance of {@link ObtenirIEISResponse }.
     *
     * @return the obtenir ieis response
     */
    public ObtenirIEISResponse createObtenirIEISResponse()
    {
        return new ObtenirIEISResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenirIEISResponse }{@code >}.
     *
     * @param value DOCUMENTEZ_MOI
     * @return the JAXB element< obtenir ieis response>
     */
    @XmlElementDecl(namespace = "http://expose.service.asynchrone.framework.projet.copernic.finances.gouv.fr", name = "obtenirIEISResponse")
    public JAXBElement<ObtenirIEISResponse> createObtenirIEISResponse(ObtenirIEISResponse value)
    {
        return new JAXBElement<ObtenirIEISResponse>(_ObtenirIEISResponse_QNAME, ObtenirIEISResponse.class, null, value);
    }

}
