/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */

package fr.gouv.finances.copernic.projet.framework.asynchrone.service.expose;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import fr.gouv.impots.appli.commun.frameworks.gestionappelsservices.coordination.ov.ServiceAsynchroneValeur;

/**
 * <p>
 * Classe Java pour obtenirAvancement complex type.
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="obtenirAvancement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceAsynchroneValeur" type="{http://asynchrone.objetsvaleurs.transverse.referentiels.commun.appli.impots.gouv.fr}ServiceAsynchroneValeur"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obtenirAvancement", propOrder = {
        "serviceAsynchroneValeur"
})
public class ObtenirAvancement
{

    /** service asynchrone valeur. */
    @XmlElement(required = true, nillable = true)
    protected ServiceAsynchroneValeur serviceAsynchroneValeur;

    /**
     * Accesseur de l attribut service asynchrone valeur.
     *
     * @return service asynchrone valeur
     */
    public ServiceAsynchroneValeur getServiceAsynchroneValeur()
    {
        return serviceAsynchroneValeur;
    }

    /**
     * Modificateur de l attribut service asynchrone valeur.
     *
     * @param value le nouveau service asynchrone valeur
     */
    public void setServiceAsynchroneValeur(ServiceAsynchroneValeur value)
    {
        this.serviceAsynchroneValeur = value;
    }

}
