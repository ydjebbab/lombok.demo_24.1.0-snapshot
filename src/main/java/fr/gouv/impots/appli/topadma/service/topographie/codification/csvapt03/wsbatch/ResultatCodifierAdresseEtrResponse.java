package fr.gouv.impots.appli.topadma.service.topographie.codification.csvapt03.wsbatch;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttachmentRef;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour resultatCodifierAdresseEtrResponse complex type.
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="resultatCodifierAdresseEtrResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://ws-i.org/profiles/basic/1.1/xsd}swaRef"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultatCodifierAdresseEtrResponse", propOrder = {
        "result"
})
public class ResultatCodifierAdresseEtrResponse
{

    @XmlElement(required = true, type = String.class, nillable = true)
    @XmlAttachmentRef
    protected DataHandler result;

    /**
     * Obtient la valeur de la propriété result.
     *
     * @return possible object is {@link String }
     */
    public DataHandler getResult()
    {
        return result;
    }

    /**
     * Définit la valeur de la propriété result.
     *
     * @param value allowed object is {@link String }
     */
    public void setResult(DataHandler value)
    {
        this.result = value;
    }

}
