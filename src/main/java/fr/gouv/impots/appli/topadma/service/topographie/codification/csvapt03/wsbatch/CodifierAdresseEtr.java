package fr.gouv.impots.appli.topadma.service.topographie.codification.csvapt03.wsbatch;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttachmentRef;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import fr.gouv.impots.appli.commun.frameworks.gestionappelsservices.coordination.ov.ServiceAsynchroneValeur;

/**
 * <p>
 * Classe Java pour codifierAdresseEtr complex type.
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="codifierAdresseEtr">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceAsynchroneValeur" type="{http://asynchrone.objetsvaleurs.transverse.referentiels.commun.appli.impots.gouv.fr}ServiceAsynchroneValeur"/>
 *         &lt;element name="requeteValeur" type="{http://ws-i.org/profiles/basic/1.1/xsd}swaRef"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "codifierAdresseEtr", propOrder = {
        "serviceAsynchroneValeur",
        "requeteValeur"
})
public class CodifierAdresseEtr
{

    @XmlElement(required = true, nillable = true)
    protected ServiceAsynchroneValeur serviceAsynchroneValeur;

    @XmlElement(required = true, type = String.class, nillable = true)
    @XmlAttachmentRef
    protected DataHandler requeteValeur;

    /**
     * Obtient la valeur de la propriété requeteValeur.
     *
     * @return possible object is {@link String }
     */
    public DataHandler getRequeteValeur()
    {
        return requeteValeur;
    }

    /**
     * Obtient la valeur de la propriété serviceAsynchroneValeur.
     *
     * @return possible object is {@link ServiceAsynchroneValeur }
     */
    public ServiceAsynchroneValeur getServiceAsynchroneValeur()
    {
        return serviceAsynchroneValeur;
    }

    /**
     * Définit la valeur de la propriété requeteValeur.
     *
     * @param value allowed object is {@link String }
     */
    public void setRequeteValeur(DataHandler value)
    {
        this.requeteValeur = value;
    }

    /**
     * Définit la valeur de la propriété serviceAsynchroneValeur.
     *
     * @param value allowed object is {@link ServiceAsynchroneValeur }
     */
    public void setServiceAsynchroneValeur(ServiceAsynchroneValeur value)
    {
        this.serviceAsynchroneValeur = value;
    }

}
