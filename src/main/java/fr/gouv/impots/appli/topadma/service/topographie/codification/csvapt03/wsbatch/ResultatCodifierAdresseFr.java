package fr.gouv.impots.appli.topadma.service.topographie.codification.csvapt03.wsbatch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import fr.gouv.impots.appli.commun.frameworks.gestionappelsservices.coordination.ov.ServiceAsynchroneValeur;

/**
 * <p>
 * Classe Java pour resultatCodifierAdresseFr complex type.
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="resultatCodifierAdresseFr">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serviceAsynchroneValeur" type="{http://asynchrone.objetsvaleurs.transverse.referentiels.commun.appli.impots.gouv.fr}ServiceAsynchroneValeur"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultatCodifierAdresseFr", propOrder = {
        "serviceAsynchroneValeur"
})
public class ResultatCodifierAdresseFr
{

    @XmlElement(required = true, nillable = true)
    protected ServiceAsynchroneValeur serviceAsynchroneValeur;

    /**
     * Obtient la valeur de la propriété serviceAsynchroneValeur.
     *
     * @return possible object is {@link ServiceAsynchroneValeur }
     */
    public ServiceAsynchroneValeur getServiceAsynchroneValeur()
    {
        return serviceAsynchroneValeur;
    }

    /**
     * Définit la valeur de la propriété serviceAsynchroneValeur.
     *
     * @param value allowed object is {@link ServiceAsynchroneValeur }
     */
    public void setServiceAsynchroneValeur(ServiceAsynchroneValeur value)
    {
        this.serviceAsynchroneValeur = value;
    }

}
