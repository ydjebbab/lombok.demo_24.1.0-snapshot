package fr.gouv.impots.appli.topadma.service.topographie.codification.csvapt03.wsbatch;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import fr.gouv.impots.appli.commun.frameworks.gestionappelsservices.coordination.ov.InformationsInterfaceValeur;

/**
 * <p>
 * Classe Java pour obtenirInformationsResponse complex type.
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="obtenirInformationsResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://objetsvaleurs.transverse.referentiels.commun.appli.impots.gouv.fr}InformationsInterfaceValeur"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "obtenirInformationsResponse", propOrder = {
        "result"
})
public class ObtenirInformationsResponse
{

    @XmlElement(required = true, nillable = true)
    protected InformationsInterfaceValeur result;

    /**
     * Obtient la valeur de la propriété result.
     *
     * @return possible object is {@link InformationsInterfaceValeur }
     */
    public InformationsInterfaceValeur getResult()
    {
        return result;
    }

    /**
     * Définit la valeur de la propriété result.
     *
     * @param value allowed object is {@link InformationsInterfaceValeur }
     */
    public void setResult(InformationsInterfaceValeur value)
    {
        this.result = value;
    }

}
