package fr.gouv.impots.appli.topadma.service.topographie.codification.csvapt03.wsbatch;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * fr.gouv.impots.appli.topadma.service.topographie.codification.csvapt03.wsbatch package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 */
@XmlRegistry
public class ObjectFactory
{

    private final static QName _CodifierAdresseFr_QNAME = new QName(
        "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", "codifierAdresseFr");

    private final static QName _CodifierAdresseFrResponse_QNAME = new QName(
        "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", "codifierAdresseFrResponse");

    private final static QName _CodifierAdresseEtrResponse_QNAME = new QName(
        "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", "codifierAdresseEtrResponse");

    private final static QName _ResultatCodifierAdresseEtrResponse_QNAME = new QName(
        "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", "resultatCodifierAdresseEtrResponse");

    private final static QName _ObtenirInformationsResponse_QNAME = new QName(
        "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", "obtenirInformationsResponse");

    private final static QName _ResultatCodifierAdresseFr_QNAME = new QName(
        "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", "resultatCodifierAdresseFr");

    private final static QName _ObtenirInformations_QNAME = new QName(
        "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", "obtenirInformations");

    private final static QName _ResultatCodifierAdresseEtr_QNAME = new QName(
        "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", "resultatCodifierAdresseEtr");

    private final static QName _ResultatCodifierAdresseFrResponse_QNAME = new QName(
        "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", "resultatCodifierAdresseFrResponse");

    private final static QName _CodifierAdresseEtr_QNAME = new QName(
        "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", "codifierAdresseEtr");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * fr.gouv.impots.appli.topadma.service.topographie.codification.csvapt03.wsbatch
     */
    public ObjectFactory()
    {
    }

    /**
     * Create an instance of {@link CodifierAdresseEtr }
     */
    public CodifierAdresseEtr createCodifierAdresseEtr()
    {
        return new CodifierAdresseEtr();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodifierAdresseEtr }{@code >}
     */
    @XmlElementDecl(namespace = "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", name = "codifierAdresseEtr")
    public JAXBElement<CodifierAdresseEtr> createCodifierAdresseEtr(CodifierAdresseEtr value)
    {
        return new JAXBElement<CodifierAdresseEtr>(_CodifierAdresseEtr_QNAME, CodifierAdresseEtr.class, null, value);
    }

    /**
     * Create an instance of {@link CodifierAdresseEtrResponse }
     */
    public CodifierAdresseEtrResponse createCodifierAdresseEtrResponse()
    {
        return new CodifierAdresseEtrResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodifierAdresseEtrResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", name = "codifierAdresseEtrResponse")
    public JAXBElement<CodifierAdresseEtrResponse> createCodifierAdresseEtrResponse(CodifierAdresseEtrResponse value)
    {
        return new JAXBElement<CodifierAdresseEtrResponse>(_CodifierAdresseEtrResponse_QNAME, CodifierAdresseEtrResponse.class, null, value);
    }

    /**
     * Create an instance of {@link CodifierAdresseFr }
     */
    public CodifierAdresseFr createCodifierAdresseFr()
    {
        return new CodifierAdresseFr();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodifierAdresseFr }{@code >}
     */
    @XmlElementDecl(namespace = "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", name = "codifierAdresseFr")
    public JAXBElement<CodifierAdresseFr> createCodifierAdresseFr(CodifierAdresseFr value)
    {
        return new JAXBElement<CodifierAdresseFr>(_CodifierAdresseFr_QNAME, CodifierAdresseFr.class, null, value);
    }

    /**
     * Create an instance of {@link CodifierAdresseFrResponse }
     */
    public CodifierAdresseFrResponse createCodifierAdresseFrResponse()
    {
        return new CodifierAdresseFrResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CodifierAdresseFrResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", name = "codifierAdresseFrResponse")
    public JAXBElement<CodifierAdresseFrResponse> createCodifierAdresseFrResponse(CodifierAdresseFrResponse value)
    {
        return new JAXBElement<CodifierAdresseFrResponse>(_CodifierAdresseFrResponse_QNAME, CodifierAdresseFrResponse.class, null, value);
    }

    /**
     * Create an instance of {@link ObtenirInformations }
     */
    public ObtenirInformations createObtenirInformations()
    {
        return new ObtenirInformations();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenirInformations }{@code >}
     */
    @XmlElementDecl(namespace = "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", name = "obtenirInformations")
    public JAXBElement<ObtenirInformations> createObtenirInformations(ObtenirInformations value)
    {
        return new JAXBElement<ObtenirInformations>(_ObtenirInformations_QNAME, ObtenirInformations.class, null, value);
    }

    /**
     * Create an instance of {@link ObtenirInformationsResponse }
     */
    public ObtenirInformationsResponse createObtenirInformationsResponse()
    {
        return new ObtenirInformationsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObtenirInformationsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", name = "obtenirInformationsResponse")
    public JAXBElement<ObtenirInformationsResponse> createObtenirInformationsResponse(ObtenirInformationsResponse value)
    {
        return new JAXBElement<ObtenirInformationsResponse>(_ObtenirInformationsResponse_QNAME, ObtenirInformationsResponse.class, null,
            value);
    }

    /**
     * Create an instance of {@link ResultatCodifierAdresseEtr }
     */
    public ResultatCodifierAdresseEtr createResultatCodifierAdresseEtr()
    {
        return new ResultatCodifierAdresseEtr();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultatCodifierAdresseEtr }{@code >}
     */
    @XmlElementDecl(namespace = "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", name = "resultatCodifierAdresseEtr")
    public JAXBElement<ResultatCodifierAdresseEtr> createResultatCodifierAdresseEtr(ResultatCodifierAdresseEtr value)
    {
        return new JAXBElement<ResultatCodifierAdresseEtr>(_ResultatCodifierAdresseEtr_QNAME, ResultatCodifierAdresseEtr.class, null, value);
    }

    /**
     * Create an instance of {@link ResultatCodifierAdresseEtrResponse }
     */
    public ResultatCodifierAdresseEtrResponse createResultatCodifierAdresseEtrResponse()
    {
        return new ResultatCodifierAdresseEtrResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultatCodifierAdresseEtrResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", name = "resultatCodifierAdresseEtrResponse")
    public JAXBElement<ResultatCodifierAdresseEtrResponse> createResultatCodifierAdresseEtrResponse(ResultatCodifierAdresseEtrResponse value)
    {
        return new JAXBElement<ResultatCodifierAdresseEtrResponse>(_ResultatCodifierAdresseEtrResponse_QNAME,
            ResultatCodifierAdresseEtrResponse.class, null, value);
    }

    /**
     * Create an instance of {@link ResultatCodifierAdresseFr }
     */
    public ResultatCodifierAdresseFr createResultatCodifierAdresseFr()
    {
        return new ResultatCodifierAdresseFr();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultatCodifierAdresseFr }{@code >}
     */
    @XmlElementDecl(namespace = "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", name = "resultatCodifierAdresseFr")
    public JAXBElement<ResultatCodifierAdresseFr> createResultatCodifierAdresseFr(ResultatCodifierAdresseFr value)
    {
        return new JAXBElement<ResultatCodifierAdresseFr>(_ResultatCodifierAdresseFr_QNAME, ResultatCodifierAdresseFr.class, null, value);
    }

    /**
     * Create an instance of {@link ResultatCodifierAdresseFrResponse }
     */
    public ResultatCodifierAdresseFrResponse createResultatCodifierAdresseFrResponse()
    {
        return new ResultatCodifierAdresseFrResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResultatCodifierAdresseFrResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://wsbatch.csvapt03.codification.topographie.service.topadma.appli.impots.gouv.fr", name = "resultatCodifierAdresseFrResponse")
    public JAXBElement<ResultatCodifierAdresseFrResponse> createResultatCodifierAdresseFrResponse(ResultatCodifierAdresseFrResponse value)
    {
        return new JAXBElement<ResultatCodifierAdresseFrResponse>(_ResultatCodifierAdresseFrResponse_QNAME,
            ResultatCodifierAdresseFrResponse.class, null, value);
    }

}
