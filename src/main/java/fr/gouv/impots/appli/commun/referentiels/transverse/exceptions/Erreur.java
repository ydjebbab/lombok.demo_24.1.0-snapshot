package fr.gouv.impots.appli.commun.referentiels.transverse.exceptions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour Erreur complex type.
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="Erreur">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="libelle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descriptif" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="severite" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Erreur", propOrder = {
        "id",
        "code",
        "libelle",
        "descriptif",
        "severite",
        "message"
})
@XmlSeeAlso({
        TechniqueErreur.class
})
public class Erreur
{

    @XmlElement(required = true, nillable = true)
    protected byte[] id;

    protected int code;

    @XmlElement(required = true, nillable = true)
    protected String libelle;

    @XmlElement(required = true, nillable = true)
    protected String descriptif;

    protected int severite;

    @XmlElement(required = true, nillable = true)
    protected String message;

    /**
     * Obtient la valeur de la propriété code.
     */
    public int getCode()
    {
        return code;
    }

    /**
     * Obtient la valeur de la propriété descriptif.
     *
     * @return possible object is {@link String }
     */
    public String getDescriptif()
    {
        return descriptif;
    }

    /**
     * Obtient la valeur de la propriété id.
     *
     * @return possible object is byte[]
     */
    public byte[] getId()
    {
        return id;
    }

    /**
     * Obtient la valeur de la propriété libelle.
     *
     * @return possible object is {@link String }
     */
    public String getLibelle()
    {
        return libelle;
    }

    /**
     * Obtient la valeur de la propriété message.
     *
     * @return possible object is {@link String }
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * Obtient la valeur de la propriété severite.
     */
    public int getSeverite()
    {
        return severite;
    }

    /**
     * Définit la valeur de la propriété code.
     */
    public void setCode(int value)
    {
        this.code = value;
    }

    /**
     * Définit la valeur de la propriété descriptif.
     *
     * @param value allowed object is {@link String }
     */
    public void setDescriptif(String value)
    {
        this.descriptif = value;
    }

    /**
     * Définit la valeur de la propriété id.
     *
     * @param value allowed object is byte[]
     */
    public void setId(byte[] value)
    {
        this.id = value;
    }

    /**
     * Définit la valeur de la propriété libelle.
     *
     * @param value allowed object is {@link String }
     */
    public void setLibelle(String value)
    {
        this.libelle = value;
    }

    /**
     * Définit la valeur de la propriété message.
     *
     * @param value allowed object is {@link String }
     */
    public void setMessage(String value)
    {
        this.message = value;
    }

    /**
     * Définit la valeur de la propriété severite.
     */
    public void setSeverite(int value)
    {
        this.severite = value;
    }

}
