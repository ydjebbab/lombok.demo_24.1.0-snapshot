package fr.gouv.impots.appli.commun.referentiels.transverse.exceptions;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java element interface generated in the
 * fr.gouv.impots.appli.commun.referentiels.transverse.exceptions package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in
 * this class.
 */
@XmlRegistry
public class ObjectFactory
{

    private final static QName _TechProtocolaireErreur_QNAME = new QName(
        "http://exceptions.transverse.referentiels.commun.appli.impots.gouv.fr", "TechProtocolaireErreur");

    private final static QName _TechDysfonctionnementErreur_QNAME = new QName(
        "http://exceptions.transverse.referentiels.commun.appli.impots.gouv.fr", "TechDysfonctionnementErreur");

    private final static QName _TechIndisponibiliteErreur_QNAME = new QName(
        "http://exceptions.transverse.referentiels.commun.appli.impots.gouv.fr", "TechIndisponibiliteErreur");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package:
     * fr.gouv.impots.appli.commun.referentiels.transverse.exceptions
     */
    public ObjectFactory()
    {
    }

    /**
     * Create an instance of {@link Erreur }
     */
    public Erreur createErreur()
    {
        return new Erreur();
    }

    /**
     * Create an instance of {@link TechDysfonctionnementErreur }
     */
    public TechDysfonctionnementErreur createTechDysfonctionnementErreur()
    {
        return new TechDysfonctionnementErreur();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TechDysfonctionnementErreur }{@code >}
     */
    @XmlElementDecl(namespace = "http://exceptions.transverse.referentiels.commun.appli.impots.gouv.fr", name = "TechDysfonctionnementErreur")
    public JAXBElement<TechDysfonctionnementErreur> createTechDysfonctionnementErreur(TechDysfonctionnementErreur value)
    {
        return new JAXBElement<TechDysfonctionnementErreur>(_TechDysfonctionnementErreur_QNAME, TechDysfonctionnementErreur.class, null,
            value);
    }

    /**
     * Create an instance of {@link TechIndisponibiliteErreur }
     */
    public TechIndisponibiliteErreur createTechIndisponibiliteErreur()
    {
        return new TechIndisponibiliteErreur();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TechIndisponibiliteErreur }{@code >}
     */
    @XmlElementDecl(namespace = "http://exceptions.transverse.referentiels.commun.appli.impots.gouv.fr", name = "TechIndisponibiliteErreur")
    public JAXBElement<TechIndisponibiliteErreur> createTechIndisponibiliteErreur(TechIndisponibiliteErreur value)
    {
        return new JAXBElement<TechIndisponibiliteErreur>(_TechIndisponibiliteErreur_QNAME, TechIndisponibiliteErreur.class, null, value);
    }

    /**
     * Create an instance of {@link TechniqueErreur }
     */
    public TechniqueErreur createTechniqueErreur()
    {
        return new TechniqueErreur();
    }

    /**
     * Create an instance of {@link TechProtocolaireErreur }
     */
    public TechProtocolaireErreur createTechProtocolaireErreur()
    {
        return new TechProtocolaireErreur();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TechProtocolaireErreur }{@code >}
     */
    @XmlElementDecl(namespace = "http://exceptions.transverse.referentiels.commun.appli.impots.gouv.fr", name = "TechProtocolaireErreur")
    public JAXBElement<TechProtocolaireErreur> createTechProtocolaireErreur(TechProtocolaireErreur value)
    {
        return new JAXBElement<TechProtocolaireErreur>(_TechProtocolaireErreur_QNAME, TechProtocolaireErreur.class, null, value);
    }

}
