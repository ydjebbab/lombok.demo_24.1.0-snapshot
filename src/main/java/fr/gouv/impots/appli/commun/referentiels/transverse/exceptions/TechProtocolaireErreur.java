package fr.gouv.impots.appli.commun.referentiels.transverse.exceptions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour TechProtocolaireErreur complex type.
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="TechProtocolaireErreur">
 *   &lt;complexContent>
 *     &lt;extension base="{http://exceptions.transverse.referentiels.commun.appli.impots.gouv.fr}TechniqueErreur">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TechProtocolaireErreur")
public class TechProtocolaireErreur
extends TechniqueErreur
{

}
