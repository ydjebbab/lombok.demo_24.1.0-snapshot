package fr.gouv.impots.appli.commun.referentiels.transverse.exceptions;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Classe Java pour TechniqueErreur complex type.
 * <p>
 * Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 *
 * <pre>
 * &lt;complexType name="TechniqueErreur">
 *   &lt;complexContent>
 *     &lt;extension base="{http://exceptions.transverse.referentiels.commun.appli.impots.gouv.fr}Erreur">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TechniqueErreur")
@XmlSeeAlso({
        TechDysfonctionnementErreur.class,
        TechIndisponibiliteErreur.class,
        TechProtocolaireErreur.class
})
public class TechniqueErreur
extends Erreur
{

}
