<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page resultatrecherchelistestructures.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Liste des structures" responsive="${responsive}">

    <app:chemins action="zf9/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Recherche de structures" transition="AutreRecherche" responsive="${responsive}">Recherche de structures</app:chemin>
        <app:chemin title="Consultation de structures" responsive="${responsive}">Consultation de structures</app:chemin>
    </app:chemins>

    <app:form action="zf9/flux.ex" formboxclass="eXtremeTable" formobjectname="recherchelibellestructureform" responsive="${responsive}">

        <%--
		<ec:table items="structurescp" action="${pageContext.request.contextPath}/zf9/flux.ex" tableId="structurescp"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml" filterable="true" showPagination="true"
            showExports="true" var="row" sortRowsCallback="cp" filterRowsCallback="cpfiltreinfsup">		
		--%>
        <ec:table items="structurescp" tableId="structurescp" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/zf9/flux.ex" autoIncludeParameters="false" form="formulaire" locale="fr_FR"
            filterable="true" sortable="true" sortRowsCallback="cp" filterRowsCallback="cpfiltreinfsup" showPagination="true"
            showNewPagination="true" showExportsBottom="false" showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')"
            positionnoresultsfound="body">

            <ec:exportPdf fileName="structures.pdf" tooltip="Export PDF" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="Liste de structures" imageName="pdf" view="cppdf" />

            <ec:exportXls encoding="UTF" fileName="structures.xls" imageName="xls" tooltip="Export Excel" view="cpjxl"
                viewResolver="cpjrxml" />

            <ec:exportCsv fileName="structures.csv" tooltip="Export CSV" delimiter="|" viewResolver="csv" />

            <ec:parameter name="_eventId_tableaunavigation" value="tableaunavigation" />
            <ec:row highlightRow="true">
                <%--
                <ec:column property="laCommune.leDepartement.libelledepartement" alias="libelledepartement" title="Département"
                    sortable="true" style="text-align: center" headerStyle="text-align: center" />
                <ec:column property="laCommune.libellecommune" alias="libellecommune" title="Ville" sortable="true"
                    style="text-align: center" headerStyle="text-align: center">
                </ec:column>
                <ec:column property="codique" alias="codique" title="Codique" sortable="true" style="text-align: center"
                    headerStyle="text-align: center" />
                <ec:column property="codeAnnexe" alias="codeannexe" title="Code annexe" sortable="true" style="text-align: center"
                    headerStyle="text-align: center">
                </ec:column>
                <ec:column property="liDenominationStandard" alias="liDenominationStandard" title="Nom du poste" sortable="true"
                    style="text-align: center" headerStyle="text-align: center">
                    <app:link action="zf9/flux.ex" transition="infosgeneralesstructure" attribute="id" value="${row.id}"
                        label="${row.liDenominationStandard}" active="true" title="Consulter le détail d'une structure" />
                </ec:column>
                <ec:column property="telStandard" alias="telstandard" title="Téléphone" sortable="true" style="text-align: center"
                    headerStyle="text-align: center">
                </ec:column>
                --%>

                <ec:column property="laCommune.leDepartement.libelledepartement" alias="libelledepartement" title="Département"
                    filterable="true" sortable="true" style="text-align: center" headerStyle="text-align: center"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=persist scope=col" />
                <ec:column property="laCommune.libellecommune" alias="libellecommune" title="Ville" sortable="true" filterable="true"
                    style="text-align: center" headerStyle="text-align: center" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=1 scope=col" />
                <ec:column property="codique" alias="codique" title="Codique" filterable="true" sortable="true" style="text-align: center"
                    headerStyle="text-align: center" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />
                <ec:column property="codeAnnexe" alias="codeannexe" title="Code annexe" filterable="true" sortable="true"
                    style="text-align: center" headerStyle="text-align: center" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=3 scope=col" />
                <ec:column property="liDenominationStandard" alias="liDenominationStandard" title="Nom du poste" sortable="true"
                    filterable="true" style="text-align: center" headerStyle="text-align: center" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=4 scope=col">
                    <app:link action="zf9/flux.ex" transition="infosgeneralesstructure" attribute="id" value="${row.id}"
                        label="${row.liDenominationStandard}" active="true" title="Consulter le détail d'une structure" />
                </ec:column>
                <ec:column property="telStandard" alias="telstandard" title="Téléphone" sortable="true" filterable="true"
                    style="text-align: center" headerStyle="text-align: center" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=5 scope=col">
                </ec:column>
            </ec:row>
        </ec:table>
        <p>
            <br />
        </p>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Autre recherche" transition="AutreRecherche" />
                    <app:submit label="Retour à l'accueil" transition="RetourAccueil" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Autre recherche" transition="AutreRecherche" responsive="${responsive}"  />
                    <app:submit label="Retour à l'accueil" transition="RetourAccueil" responsive="${responsive}"
                         />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>