<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisiecodesannexes.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Recherche d'une structure par code annexe" titrecontainer="Sélection d'un code annexe" menu="true"
    responsive="${responsive}">

    <app:chemins action="zf9/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Recherche de structures" transition="AutreRecherche" responsive="${responsive}">Recherche de structures</app:chemin>
        <app:chemin title="Recherche par code annexe" responsive="${responsive}">Recherche par code annexe</app:chemin>
    </app:chemins>

    <app:form action="zf9/flux.ex" formobjectname="recherchelibellestructureform" formboxclass="donnees" responsive="${responsive}">

        <app:selectmultiple attribut="codesAnnexes" itemslist="${codesAnnexesStructureCP}" libelle="Codes annexes :" size="1" theme="V"
            boxwidth="100%" requis="false" defautitem="false" labelboxwidth="18%" inputboxwidth="40%" compboxwidth="27%"
            consigne="Sélectionner un ou plusieurs codes annexes">
        </app:selectmultiple>

        <app:boxboutons>
            <app:submit transition="recherche_par_codesannexes" label="Rechercher par code annexe"
                title="Recherche toutes les structures par code annexe." responsive="${responsive}" />
            <app:submit label="Autre recherche" transition="AutreRecherche" responsive="${responsive}" />
            <app:submit label="Retour à l'accueil" transition="retourAccueil" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>