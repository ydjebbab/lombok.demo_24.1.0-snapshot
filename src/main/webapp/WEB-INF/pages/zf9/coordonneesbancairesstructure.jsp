<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page coordonneesbancairesstructure.jsp -->
<app:page titreecran="Recherche d'une structure par codique" titrecontainer="saisie du codique de la structure" menu="true">

    <app:onglets cleactive="2">
        <app:onglet cle="1" libelle="Informations générales" transition="infosgeneralesstructure" />
        <app:onglet cle="2" libelle="Références bancaires" />
    </app:onglets>

    <app:form action="zf9/flux.ex" formobjectname="recherchelibellestructureform" formboxclass="donnees sousonglets">

        <%-- Si les horaires sont disponibles, on les affiches --%>
        <c:if test="${!empty recherchelibellestructureform.uneStructureSelectionnee.lesCoordBancairesCP}">

            <p>Horaires d'ouverture</p>
            <ec:table items="recherchelibellestructureform.uneStructureSelectionnee.lesCoordBancairesCP"
                action="${pageContext.request.contextPath}/zf9/flux.ex" tableId="coordonneesbancaires" autoIncludeParameters="false"
                form="formulaire" locale="fr_FR" view="cpgroup" showStatusBar="false" filterable="false" showPagination="false"
                showExports="false" var="row" sortRowsCallback="cp" filterRowsCallback="cpfiltreinfsup">
                <ec:row>
                    <ech:column property="codeFlux" title="Code flux" sortable="true" style="text-align: center"
                        headerStyle="text-align: center" />
                    <ech:column property="codeIC" title="Identifiant client BDF" sortable="true" style="text-align: center"
                        headerStyle="text-align: center" />
                    <ech:column property="biC" title="BIC" sortable="true" style="text-align: center" headerStyle="text-align: center" />
                    <ech:column property="ibAN" title="IBAN" sortable="true" style="text-align: center" headerStyle="text-align: center" />
                    <ech:column property="leRIBClassique.codeBanque" title="Code banque" sortable="true" style="text-align: center"
                        headerStyle="text-align: center" />
                    <ech:column property="leRIBClassique.codeGuichet" title="Code guichet" sortable="true" style="text-align: center"
                        headerStyle="text-align: center" />
                    <ech:column property="leRIBClassique.numeroCompte" title="Compte" sortable="true" style="text-align: center"
                        headerStyle="text-align: center" />
                    <ech:column property="leRIBClassique.cleRib" title="Cle" sortable="true" style="text-align: center"
                        headerStyle="text-align: center" />
                </ec:row>
            </ec:table>
        </c:if>


        <app:boxboutons>
            <app:submit label="Retourner à la liste" transition="RetourListe" />
        </app:boxboutons>
    </app:form>
</app:page>