<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>
 
 
<!--  Page arborescencestructuresdetail.jsp -->
<app:page titreecran="Structures" 
          titrecontainer="Structures" 
          menu="true" 
          >
<app:form 
    method="post" 
    action="zf9/flux.ex" 
    formid="consultationstructuresform" 
    formobjectname="consultationstructuresform" 
    formboxid="donnees"
    formboxclass="donnees"
    >     
 
<app:button action="zf9/flux.ex" transition="collapseAll">Tout réduire</app:button>
<app:button action="zf9/flux.ex" transition="expandAll">Tout développer</app:button>
<app:button action="zf9/flux.ex" transition="unSelectAll">Tout dé-sélectionner</app:button>

<app:input attribut="text" libelle="" size="15" maxlength="40" 
  labelboxwidth="0%" inputboxwidth="50%"  compboxwidth="45%"  requis="false">
  <app:submit transition="findNodesByName" label="Rechercher"/>
</app:input>
  
     
        
      
   


        <div style="clear: both;">&nbsp;</div>
      
    <div id="treescroll" style="overflow:scroll;width: 19%; height: 600px;"  onscroll="javascript:document.getElementById('scroll').value =this.scrollTop;">
 
   
	<lb:tree tree="tree.model" includeRootNode="false"
		action="zf9/flux.ex"> 
		<lb:nodes>
			<lb:nodenavigation/>
			<%-- Noeuds de premier niveau--%>			
			<lb:nodeMatch type="POSTES_EN_FRANCE" >
				<lb:nodetext selectable="false" inline="true" navigable="true">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>
			<lb:nodeMatch type="POSTES_A_LETRANGER" >
				<lb:nodetext selectable="true" inline="true" navigable="false">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>
			<lb:nodeMatch type="STR_ATTR_SPEC" >
				<lb:nodetext selectable="true" inline="true" navigable="false">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>
			<lb:nodeMatch type="SER_CENT_DGCP" >
				<lb:nodetext selectable="true" inline="true" navigable="false">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>			
			<lb:nodeMatch type="STRUC_FORMATION" >
				<lb:nodetext selectable="true" inline="true" navigable="false">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>	
			
			<%-- Noeuds de deuxième niveau--%>			
			<lb:nodeMatch type="REGION" >
				<lb:nodetext selectable="false" inline="true" navigable="true">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>
			<lb:nodeMatch type="DEPARTEMENT" >
				<lb:nodetext selectable="true" inline="false" navigable="false">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>
		</lb:nodes>
	</lb:tree>
 
  </div> 
 
<c:if test="${!empty consultationstructuresform.structureSelectionnee}">
<div style="width: 68%;float:right;border:thin;border-style: solid;border-color: gray;">

<table>
<tr></tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr></tr>
<tr><td style="font-weight: bold;">Codique</td><td>${consultationstructuresform.structureSelectionnee.codique}</td></tr>
<tr><td style="font-weight: bold;">Catégorie</td><td>${consultationstructuresform.structureSelectionnee.laCategorieStructureCP.libellecategorie}</td></tr>
<tr><td rowspan="5" style="font-weight: bold;">Adresse postale</td><td>${consultationstructuresform.structureSelectionnee.ladressePostale.ligne2ServiceAppartEtgEsc}</td></tr>
<tr><td>${consultationstructuresform.structureSelectionnee.ladressePostale.ligne3BatimentImmeubleResid}</td></tr>
<tr><td>${consultationstructuresform.structureSelectionnee.ladressePostale.ligne4NumeroLibelleVoie}</td></tr>
<tr><td>${consultationstructuresform.structureSelectionnee.ladressePostale.ligne5LieuDitMentionSpeciale}</td></tr>
<tr><td>${consultationstructuresform.structureSelectionnee.ladressePostale.codePostalEtCedex}&nbsp;
${consultationstructuresform.structureSelectionnee.ladressePostale.ville}</td></tr>
<tr><td style="font-weight: bold;">Téléphone</td><td>${consultationstructuresform.structureSelectionnee.telStandard}</td></tr>
<tr><td style="font-weight: bold;">Télécopie</td><td>${consultationstructuresform.structureSelectionnee.telecopie}</td></tr>
<tr><td style="font-weight: bold;">Adresse mail</td><td>${consultationstructuresform.structureSelectionnee.adresseMelGenerique}</td></tr>	    
</table>
<app:boxboutons>
<app:submit transition="retourliste"  label="Retour à la liste"></app:submit>
</app:boxboutons>
</div>
</c:if>

<%--
Fix pour firefox et ie7
Ce div est ajouté pour forcer le div englobant (donnees) à s'étendre autour
des deux div flottant.
En utilisant clear:both dans le style de ce div, cet élément définit se positionne
sous les div flottants précédents et comme lui-même n'est pas flottant, il force
le div parent (donnees) à s'étendre pour l'englober
//TODO voir s'il faut ajouter ce div dans le tag app:form
 --%>
   <div style="clear:both;"></div> 

 
 </app:form>
</app:page>	
