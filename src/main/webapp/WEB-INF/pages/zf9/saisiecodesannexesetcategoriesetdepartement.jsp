<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisiecodesannexesetcategoriesetdepartement.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Recherche d'une structure par département, catégories et codes annexes"
    titrecontainer="Sélection d'un département, de catégories et codes annexes" menu="true" responsive="${responsive}">

    <app:chemins action="zf9/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Recherche de structures" transition="AutreRecherche" responsive="${responsive}">Recherche de structures</app:chemin>
        <app:chemin title="Recherche par département, catégories de structure et codes annexes" responsive="${responsive}">Recherche par département, catégories de structure et codes annexes</app:chemin>
    </app:chemins>

    <app:form action="zf9/flux.ex" formobjectname="recherchelibellestructureform" formboxclass="donnees" responsive="${responsive}">

        <app:select attribut="departement" itemslist="${departements}" libelle="Département :" label="codePlusLibelleDepartement"
            value="codedepartementINSEE" theme="V" boxwidth="100%" requis="false" defautitem="false" labelboxwidth="18%" inputboxwidth="40%"
            compboxwidth="27%" consigne="Sélectionner un département">
        </app:select>

        <app:selectmultiple attribut="codesAnnexes" itemslist="${codesAnnexesStructureCP}" libelle="Codes annexes :" size="10" theme="V"
            boxwidth="100%" requis="false" defautitem="false" labelboxwidth="18%" inputboxwidth="40%" compboxwidth="27%"
            consigne="Sélectionner un ou plusieurs codes annexes">
        </app:selectmultiple>

        <app:selectmultiple attribut="codesCategoriesStructureCP" itemslist="${categoriesstructurescp}" libelle="Catégorie :"
            label="libellecategorie" value="codecategorie" size="33" theme="V" boxwidth="100%" requis="false" defautitem="false"
            labelboxwidth="18%" inputboxwidth="40%" compboxwidth="27%" consigne="Sélectionner un ou plusieurs codes catégories">
        </app:selectmultiple>

        <app:boxboutons>
            <app:submit transition="recherche_par_codesannexesetcategoriesetdepartement"
                label="Rechercher par catégories, codes annexes et départements"
                title="Recherche toutes les structures par catégories, codes annexes et département" responsive="${responsive}" />
            <app:submit label="Autre recherche" transition="AutreRecherche" responsive="${responsive}" />
            <app:submit label="Retour à l'accueil" transition="RetourAccueil" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>