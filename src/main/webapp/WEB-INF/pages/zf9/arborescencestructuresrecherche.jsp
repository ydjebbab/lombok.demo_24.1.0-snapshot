<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/tags/html/includes.tagf" %>

<!--  Page arborescencestructuresrecherche.jsp -->
<app:page titreecran="Structures" 
          titrecontainer="Structures" 
          menu="true" 
          >

 <app:form 
	method="post" 
	action="zf9/flux.ex" 
	formid="consultationstructuresform" 
	formobjectname="consultationstructuresform" 
	onsubmit="return submit_form_noenter(500)"
	formboxid="donnees"
	formboxclass="donnees">

<div style="width: 30%;border:thin;border-style: solid;border-color: gray;float:left;display:block;">
<div>
<app:button action="zf9/flux.ex" transition="collapseAll">Tout réduire</app:button>
<app:button action="zf9/flux.ex" transition="expandAll">Tout développer</app:button>
<app:button action="zf9/flux.ex" transition="unSelectAll">Tout dé-sélectionner</app:button>
<app:input attribut="text" libelle="" size="15" maxlength="40" 
	labelboxwidth="0%" inputboxwidth="50%"  compboxwidth="45%"  requis="false">
	<app:submit transition="findNodesByName" label="Rechercher"/>
</app:input>
<div style="clear:both;"></div>
</div>
	<lb:tree tree="tree.model" includeRootNode="false"
		action="zf9/flux.ex"> 
		<lb:nodes>
			<lb:nodenavigation/>
			<%-- Noeuds de premier niveau--%>			
			<lb:nodeMatch type="POSTES_EN_FRANCE" >
				<lb:nodetext selectable="false" inline="true" navigable="true">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>
			<lb:nodeMatch type="POSTES_A_LETRANGER" >
				<lb:nodetext selectable="true" inline="true" navigable="false">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>
			<lb:nodeMatch type="STR_ATTR_SPEC" >
				<lb:nodetext selectable="true" inline="true" navigable="false">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>
			<lb:nodeMatch type="SER_CENT_DGCP" >
				<lb:nodetext selectable="true" inline="true" navigable="false">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>			
			<lb:nodeMatch type="STRUC_FORMATION" >
				<lb:nodetext selectable="true" inline="true" navigable="false">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>	
			
			<%-- Noeuds de deuxième niveau--%>			
			<lb:nodeMatch type="REGION" >
				<lb:nodetext selectable="false" inline="true" navigable="true">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>
			<lb:nodeMatch type="DEPARTEMENT" >
				<lb:nodetext selectable="true" inline="false" navigable="false">
					<lb:nodeName/>
				</lb:nodetext>
			</lb:nodeMatch>
		</lb:nodes>
	</lb:tree>
</div>

<c:if test="${!empty consultationstructuresform.foundNodes}">
<div style="width: 68%;float:right;border:thin;border-style: solid;border-color: gray;">

<c:set var="resultatrecherche" value="${consultationstructuresform.foundNodes}"/>
<c:set var="titrerecherche" value="Elements trouvés pour '${consultationstructuresform.text}'"/>

<ec:table items="resultatrecherche" form="consultationstructuresform"
filterable="false" sortable="false" showTitle="true" showExports="false" showPagination="true"
var="row" autoIncludeParameters="false" 
title="${titrerecherche}"
>
<ec:parameter name="_eventId_tableaunavigation" value="tableaunavigation"/>
<ec:row >
	<ec:column property="name">
		<lb:nodelink  action="zf9/flux.ex"  transition="selectNodeAndExpandAncestor"
						attribute="nodeId" value="${row.id}"
		>${row.name}</lb:nodelink>
	</ec:column>
</ec:row>
</ec:table>
</div>
</c:if>
<%--
Fix pour firefox et ie7
Ce div est ajouté pour forcer le div englobant (donnees) à s'étendre autour
des deux div flottant.
En utilisant clear:both dans le style de ce div, cet élément définit se positionne
sous les div flottants précédents et comme lui-même n'est pas flottant, il force
le div parent (donnees) à s'étendre pour l'englober
//TODO voir s'il faut ajouter ce div dans le tag app:form
 --%>
<div style="clear:both;"></div>

</app:form>

</app:page>	
