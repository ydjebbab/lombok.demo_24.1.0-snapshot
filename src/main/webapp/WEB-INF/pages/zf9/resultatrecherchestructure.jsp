<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page resultatrecherchestructure.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Recherche d'une structure par codique" titrecontainer="saisie du codique de la structure" menu="true"
    responsive="${responsive}">

    <app:onglets cleactive="2" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="saisie codique" responsive="${responsive}" ongletclass="btmodel_inv_half" />
        <app:onglet cle="2" libelle="detail structure" responsive="${responsive}" ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form action="zf9/flux.ex" formobjectname="recherchelibellestructureform" formboxclass="donnees sousonglets"
        responsive="${responsive}">

        <app:input attribut="unestructurearechercher.codique" libelle="codique :" readonly="true" maxlength="6" size="6" requis="false"
            responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.codeAnnexe" libelle="code annexe :" readonly="true" maxlength="1" size="1"
            requis="false" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.denominationStandard" libelle="denomination structure :" readonly="true" maxlength="50"
            size="50" requis="false" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.libelleLong" libelle="libelle :" readonly="true" maxlength="50" size="50"
            requis="false" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.telStandard" libelle="telephone standard :" readonly="true" maxlength="20" size="20"
            requis="false" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.ladressePostale.ligne2ServiceAppartEtgEsc" libelle="Adresse :" readonly="true"
            maxlength="50" size="50" requis="false" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.ladressePostale.ligne3BatimentImmeubleResid" libelle="         " readonly="true"
            maxlength="50" size="50" requis="false" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.ladressePostale.ligne4NumeroLibelleVoie" libelle="         " readonly="true"
            maxlength="50" size="50" requis="false" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.ladressePostale.ligne5LieuDitMentionSpeciale" libelle="         " readonly="true"
            maxlength="50" size="50" requis="false" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.ladressePostale.codePostalEtCedex" libelle="         " readonly="true" maxlength="50"
            size="50" requis="false" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.ladressePostale.ville" libelle="         " readonly="true" maxlength="50" size="50"
            requis="false" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.ladressePostale.pays" libelle="         " readonly="true" maxlength="20" size="20"
            requis="false" responsive="${responsive}" />
        <app:input attribut="unestructurearechercher.adresseMelGenerique" libelle="code annexe structure :" readonly="true" maxlength="50"
            size="50" requis="false" responsive="${responsive}" />
        <app:boxboutons>
            <app:submit label="Retour à l'accueil" transition="RetourAccueil" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>