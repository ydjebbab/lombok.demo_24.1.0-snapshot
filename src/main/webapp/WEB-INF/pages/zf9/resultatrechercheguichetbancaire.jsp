<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page resultatrechercheguichetbancaire.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Recherche de guichet bancaire" titrecontainer="Saisie du codique de la structure" menu="true"
    responsive="${responsive}">

    <app:chemins action="zf9/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Recherche de guichet bancaire" responsive="${responsive}">Recherche de guichet bancaire</app:chemin>
    </app:chemins>

    <app:onglets cleactive="2" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Saisie rib" responsive="${responsive}" ongletclass="btmodel_inv_half" />
        <app:onglet cle="2" libelle="Détail bancaire" responsive="${responsive}" ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form action="zf9/flux.ex" formobjectname="rechercheguichetbancaireform" formboxclass="donnees sousonglets"
        responsive="${responsive}">

        <app:input attribut="unguichetbancairearechercher.codeGuichet" libelle="code guichet :" readonly="true" maxlength="5" size="5"
            responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.codeEtablissement" libelle="code établissement :" readonly="true" maxlength="5"
            size="5" responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.denominationComplete" libelle="denominationComplete :" readonly="true"
            maxlength="40" size="40" responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.nomGuichet" libelle="nom du guichet :" readonly="true" maxlength="20" size="20"
            responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.adresseGuichet" libelle="adresse du guichet :" readonly="true" maxlength="29"
            size="29" responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.codePostal" libelle="code postal :" readonly="true" maxlength="5" size="5"
            responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.villeCodePostal" libelle="ville :" readonly="true" maxlength="21" size="21"
            responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.denominationAbregee" libelle="denomination abrégée :" readonly="true"
            maxlength="10" size="10" responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.libelleAbrevDeDomiciliation" libelle="libelle abréviatif de domiciliation :"
            readonly="true" maxlength="24" size="24" responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.codeReglementDesCredits" libelle="code réglement des crédits:" readonly="true"
            maxlength="4" size="4" responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.moisDeModification" libelle="mois de modification :" readonly="true" maxlength="2"
            size="2" responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.anneeDeModification" libelle="année de modification :" readonly="true"
            maxlength="2" size="2" responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.codeEnregistrement" libelle="code enregistrement :" readonly="true" maxlength="1"
            size="1" responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.nouveauCodeEtablissement" libelle="nouveau code établissement :" readonly="true"
            maxlength="5" size="5" responsive="${responsive}" bootstrapclass="btmodel_half" />
        <app:input attribut="unguichetbancairearechercher.nouveauCodeGuichet" libelle="nouveau code guichet :" readonly="true" maxlength="5"
            size="5" responsive="${responsive}" bootstrapclass="btmodel_half" />

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="Retour" id="enterButton" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" transition="Retour" id="enterButton" responsive="${responsive}"
                         />
                </c:otherwise>
            </c:choose>

        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>