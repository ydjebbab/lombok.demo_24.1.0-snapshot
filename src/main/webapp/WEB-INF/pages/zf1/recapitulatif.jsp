<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page recapitulatif.jsp -->
<c:set var="responsive" value="true" />

<app:page   titreecran="Adhésion à la mensualisation - Récapitulatif"
            titrecontainer="Adhésion à la mensualisation - Récapitulatif" 
            menu="true" responsive="${responsive}">

    <app:chemins action="zf1/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contrats" responsive="${responsive}">Contrats</app:chemin>
        <app:chemin title="Adhérer" responsive="${responsive}">Adhérer</app:chemin>
        <app:chemin title="Prélèvement mensuel" responsive="${responsive}">Prélèvement mensuel</app:chemin>
        <app:chemin title="Immédiat" responsive="${responsive}">Immédiat</app:chemin>
    </app:chemins>

    <app:onglets cleactive="3" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Type d'impôt" bouton="Debut" responsive="${responsive}" ongletclass="btmodel_inv_third" />
        <app:onglet cle="2" libelle="Références bancaires" bouton="Precedent" responsive="${responsive}" ongletclass="btmodel_inv_third" />
        <app:onglet cle="3" libelle="Récapitulatif" responsive="${responsive}" ongletclass="btmodel_inv_third" />
    </app:onglets>

    <app:form method="post" action="zf1/flux.ex" formid="saisiecontribuable" formobjectname="demandeadhesionmensualisationform"
        onsubmit="return submit_form_noenter(500)" formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}"
        rwdformclass="form-horizontal">

        <c:set var="readonly" value="true" />

        <app:input attribut="avisImposition.reference" libelle="Référence d'avis d'imposition :" maxlength="50" size="50"
            readonly="${readonly}" responsive="${responsive}" />
        <app:input attribut="nouveauContrat.anneePriseEffet" libelle="Année de prise d'effet :" maxlength="5" size="5"
            readonly="${readonly}" responsive="${responsive}" />
        <app:input attribut="nouveauContrat.typeImpot.nomImpot" libelle="Type d'impôt :" maxlength="20" size="20" readonly="${readonly}"
            responsive="${responsive}" />
        <app:input attribut="nouveauContrat.rib.codeBanque" libelle="RIB :" maxlength="20" size="20" readonly="${readonly}"
            responsive="${responsive}" />
        <app:input attribut="titulaireCompte" libelle="Titulaire du compte :" maxlength="40" size="40" readonly="${readonly}"
            responsive="${responsive}" />
        <app:input attribut="nouveauContrat.contribuablePar.adresseMail" libelle="Adresse mél : " maxlength="40" size="40"
            readonly="${readonly}" responsive="${responsive}" />
        <app:boxboutons>
            <app:submit label="Précédent" transition="Precedent" responsive="${responsive}"  />
            <app:submit label="Adhérer" transition="Adherer" class="primary" responsive="${responsive}"  />
            <app:submit label="Début" transition="Debut" responsive="${responsive}" inputclass="btn btn-default cache" />
        </app:boxboutons>

    </app:form>
</app:page>