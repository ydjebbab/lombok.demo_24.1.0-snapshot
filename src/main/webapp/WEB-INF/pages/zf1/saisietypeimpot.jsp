<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisietypeimpot.jsp -->
<c:set var="responsive" value="true" />

<app:page   titreecran="Adhésion à la mensualisation - Type d'impôt" 
            titrecontainer="Adhésion à la mensualisation - Type d'impôt" 
            menu="true" responsive="${responsive}">

    <app:chemins action="zf1/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contrats" responsive="${responsive}">Contrats</app:chemin>
        <app:chemin title="Adhérer" responsive="${responsive}">Adhérer</app:chemin>
        <app:chemin title="Prélèvement mensuel" responsive="${responsive}">Prélèvement mensuel</app:chemin>
        <app:chemin title="Immédiat" responsive="${responsive}">Immédiat</app:chemin>
    </app:chemins>

    <app:onglets cleactive="1" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Type d'impôt" responsive="${responsive}" ongletclass="btmodel_inv_third" />
        <app:onglet cle="2" libelle="Références bancaires" bouton="Suivant" exposecle="true" responsive="${responsive}"
            ongletclass="btmodel_inv_third" />
        <app:onglet cle="3" libelle="Récapitulatif" responsive="${responsive}" ongletclass="btmodel_inv_third" />
    </app:onglets>

    <app:form method="post" action="zf1/flux.ex" formid="saisiecontribuable" formobjectname="demandeadhesionmensualisationform"
        onsubmit="return submit_form_noenter(500)" formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}"
        rwdformclass="form-horizontal">

        <app:input attribut="avisImposition.reference" libelle="Référence d'avis d'imposition :" maxlength="50" size="50"
            inputboxwidth="initial" responsive="${responsive}" bootstraplabelclass="btmodel_third" bootstrapinputclass="btmodel_third"
            bootstrapbodyclass="btmodel_third_sm_right">
            <app:submit label="Rechercher avis" transition="RechercherAvis" responsive="${responsive}" />
        </app:input>

        <app:select attribut="nouveauContrat.typeImpot" label="nomImpot" value="id" libelle="Type d'impôt :" itemslist="${listetypesimpots}"
            responsive="${responsive}" bootstraplabelclass="btmodel_third" bootstrapinputclass="btmodel_third" />

        <app:select attribut="nouveauContrat.anneePriseEffet" libelle="Année de prise d'effet :" itemsmap="${listeanneepriseeffet}"
            defautitem="false" responsive="${responsive}" bootstraplabelclass="btmodel_third" bootstrapinputclass="btmodel_third" />


        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                    <app:submit label="Suivant" transition="Suivant" id="enterButton" />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil"
                        responsive="${responsive}"  />
                    <app:submit label="Suivant" transition="Suivant" id="enterButton" responsive="${responsive}"
                        inputclass="primary btn btn-default" />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>

    </app:form>
</app:page>