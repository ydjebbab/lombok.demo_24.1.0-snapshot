<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page demandeadhesiontraitee.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Adhésion à la mensualisation - Demande terminée" responsive="${responsive}">

    <app:chemins action="zf1/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contrats" responsive="${responsive}">Contrats</app:chemin>
        <app:chemin title="Adhérer" responsive="${responsive}">Adhérer</app:chemin>
        <app:chemin title="Prélèvement mensuel" responsive="${responsive}">Prélèvement mensuel</app:chemin>
        <app:chemin title="Immédiat" responsive="${responsive}">Immédiat</app:chemin>
    </app:chemins>

    <app:form action="zf1/flux.ex" responsive="${responsive}">

        <p>Votre demande d'adhésion concernant l'avis ${demandeadhesionmensualisationform.avisImposition.reference} a été prise en
            compte</p>
        <br />
        <p>
            Télécharger le document
            <app:link action="zf1/flux.ex" transition="telecharger" label="demandeadhesion.pdf" active="true" />
        </p>
        <app:boxboutons>
            <app:submit label="Retour à l'accueil" transition="retour" responsive="${responsive}"  />
        </app:boxboutons>
    </app:form>
</app:page>