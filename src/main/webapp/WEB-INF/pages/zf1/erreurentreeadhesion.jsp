<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page erreurentreeadhesion.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Adhésion à la mensualisation - Erreur" menu="true" responsive="${responsive}">

    <app:chemins action="zf1/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin responsive="${responsive}">Adhésion à la mensualisation impossible</app:chemin>
    </app:chemins>

    <app:form method="post" action="zf1/flux.ex" formobjectname="demandeadhesionmensualisationform"
        onsubmit="return submit_form_noenter(500)" formboxid="donnees" formboxclass="donnees" responsive="${responsive}">

        <app:boxboutons>
            <app:submit label="Abandon" transition="abandon" responsive="${responsive}"  />
        </app:boxboutons>
    </app:form>
</app:page>