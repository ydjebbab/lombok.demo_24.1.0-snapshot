<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page testformatboolean.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Test des formats booléens" menu="true" responsive="${responsive}">

    <app:chemins action="zf7/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Gestion des types d'impôt" transition="listeimpots" responsive="${responsive}">Gestion des types d'impôt</app:chemin>
        <app:chemin title="Pages de test" transition="pagesdetest" responsive="${responsive}">Pages de test</app:chemin>
        <app:chemin title="Format de booléans" responsive="${responsive}">Format de booléans</app:chemin>
    </app:chemins>

    <app:form action="zf7/flux.ex" formboxclass="eXtremeTable">

        <%--
        <ec:table items="referencetypesimpots" action="${pageContext.request.contextPath}/zf7/flux.ex" tableId="typesimpots"
            autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml" filterable="true" showPagination="false"
            showExports="true" var="row" sortRowsCallback="cp" filterRowsCallback="cp">
         --%>
        <ec:table items="referencetypesimpots" tableId="typesimpots" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/zf7/flux.ex" autoIncludeParameters="false" form="formulaire" locale="fr_FR"
            filterable="true" sortable="true" sortRowsCallback="cp" filterRowsCallback="cp" showPagination="true" showNewPagination="true"
            showExports="true" newPaginationNbPage="5" showStatusBar="true" showPaginationBottom="false" showExportsBottom="false"
            showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')" positionnoresultsfound="body">

            <ec:exportPdf fileName="listeDesTypesImpots.pdf" tooltip="Export PDF" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="test de formats booleans" imageName="pdf" view="cppdf" />

            <ec:exportXls encoding="UTF" fileName="listeDesTypesImpots.xls" imageName="xls" tooltip="Export Excel" view="cpxls" />

            <ec:exportCsv fileName="listeDesTypesImpots.csv" tooltip="Export CSV" delimiter="|" viewResolver="csv" />

            <ec:row highlightRow="true">
                <%--
                <ec:column property="estOuvertALaMensualisation" title="booleanouinon" width="10%" sortable="true" cell="booleanouinon"
                    alias="booleanouinon" style="text-align: center" headerStyle="text-align: center" />
                <ec:column property="estOuvertALaMensualisation" title="booleanon" width="10%" sortable="true" cell="booleanon"
                    alias="booleanon" style="text-align: center" headerStyle="text-align: center" />
                <ec:column property="estOuvertALaMensualisation" title="boolean10" width="10%" sortable="true" cell="boolean10"
                    alias="boolean10" style="text-align: center" headerStyle="text-align: center" />
                <ec:column property="estOuvertALaMensualisation" title="booleanvraifaux" width="10%" sortable="true" cell="booleanvraifaux"
                    alias="booleanvraifaux" style="text-align: center" headerStyle="text-align: center" />
                <ec:column property="estOuvertALaMensualisation" title="booleanvf" width="10%" sortable="true" cell="booleanvf"
                    alias="booleanvf" style="text-align: center" headerStyle="text-align: center" />
                 --%>
                <ec:column property="estOuvertALaMensualisation" title="booleanouinon" width="10%" sortable="true" filterable="true"
                    cell="booleanouinon" alias="booleanouinon" style="text-align: center" headerStyle="text-align: center"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=persist scope=col" />
                <ec:column property="estOuvertALaMensualisation" title="booleanon" width="10%" sortable="true" filterable="true"
                    cell="booleanon" alias="booleanon" style="text-align: center" headerStyle="text-align: center"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=1 scope=col" />
                <ec:column property="estOuvertALaMensualisation" title="boolean10" width="10%" sortable="true" filterable="true"
                    cell="boolean10" alias="boolean10" style="text-align: center" headerStyle="text-align: center"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />
                <ec:column property="estOuvertALaMensualisation" title="booleanvraifaux" width="10%" sortable="true" filterable="true"
                    cell="booleanvraifaux" alias="booleanvraifaux" style="text-align: center" headerStyle="text-align: center"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />
                <ec:column property="estOuvertALaMensualisation" title="booleanvf" width="10%" sortable="true" filterable="true"
                    cell="booleanvf" alias="booleanvf" style="text-align: center" headerStyle="text-align: center"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=4 scope=col" />
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" />
                    <app:submit label="Retourner à l'accueil" transition="retour" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" responsive="${responsive}"
                         />
                    <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>