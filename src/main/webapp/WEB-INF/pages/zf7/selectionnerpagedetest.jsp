<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page selectionnerpagedetest.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Pages de test" menu="true" responsive="${responsive}">

    <app:chemins action="zf7/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Gestion des types d'impôt" transition="retour" responsive="${responsive}">Gestion des types d'impôt</app:chemin>
        <app:chemin title="Pages de test" responsive="${responsive}">Pages de test</app:chemin>
    </app:chemins>

    <app:form action="zf7/flux.ex" responsive="${responsive}">

        <ul>
            <li><app:link action="zf7/flux.ex" label="Page de test des formats de nombres" transition="testformatnombre" /></li>
            <li><app:link action="zf7/flux.ex" label="Page de test des formats de monnaies" transition="testformatmonnaie" /></li>
            <li><app:link action="zf7/flux.ex" label="Page de test des formats de pourcentages" transition="testformatpourcentage" /></li>
            <li><app:link action="zf7/flux.ex" label="Page de test des formats de dates" transition="testformatdate" /></li>
            <li><app:link action="zf7/flux.ex" label="Page de test des formats de booléens" transition="testformatboolean" /></li>
        </ul>

        <app:boxboutons>
            <app:submit label="Retourner à la liste des types d'impôts" transition="retour" responsive="${responsive}" />
        </app:boxboutons>

    </app:form>
</app:page>