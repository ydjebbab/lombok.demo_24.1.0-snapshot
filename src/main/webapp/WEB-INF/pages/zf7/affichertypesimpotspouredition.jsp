<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page affichertypesimpotspouredition.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Exemple d'édition de types d'impôt" menu="true" responsive="${responsive}">

    <app:chemins action="zf7/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Exemple d'édition de types d'impôt" responsive="${responsive}">Exemple d'édition de types d'impôt</app:chemin>
    </app:chemins>

    <app:form action="zf7/flux.ex" formboxclass="eXtremeTable" formobjectname="referenceform" responsive="${responsive}">

        <%--
        <ec:table items="referencetypesimpots" 
                action="${pageContext.request.contextPath}/zf7/flux.ex" 
                title="table.title.referencetypesimpots"
                tableId="typesimpots" state="persist"
                autoIncludeParameters="false" 
                form="formulaire"
                locale="fr_FR" 
                view="cphtml"
                filterable="true"   
                showPagination="true" showExports="true" 
                var="row" 
                sortRowsCallback="cp" filterRowsCallback="cpfiltreinfsup"
                >
    --%>
        <ec:table title="table.title.referencetypesimpots" items="referencetypesimpots" tableId="typesimpots" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/zf7/flux.ex" state="persist" stateAttr="gotofirstpage" autoIncludeParameters="false"
            form="formulaire" locale="fr_FR" filterable="true" sortable="true" sortRowsCallback="cp" filterRowsCallback="cpfiltreinfsup"
            rowsDisplayed="10" showPagination="true" showNewPagination="true" showExports="true" newPaginationNbPage="5"
            showStatusBar="true" showPaginationBottom="false" showExportsBottom="false" showNewFilter="true"
            onInvokeAction="ecupdatectra('tableaunavigation')"
            libellenoresultsfound="Aucune personne ne correspond à vos criteres de recherche" positionnoresultsfound="body">

            <ec:exportPdf fileName="zf8.affichertypesimpots.edition" text="zf8.affichertypesimpots.parametres" tooltip="Export PDF"
                headerColor="black" headerBackgroundColor="#b6c2da" headerTitle="Liste des types impot" imageName="pdf" view="jasper"
                viewResolver="jasper" />

            <ec:parameter name="_eventId_tableaunavigation" value="tableaunavigation" />
            <ec:row highlightRow="true">

                <%--
                <ech:column property="chkbx" cell="cpcheckboxcell" rowid="id" viewsAllowed="cphtml" viewsDenied="cpods,cpjrxml"
                    headerCell="cpselectAll" width="5%" filterable="false" sortable="false"  />
            --%>
                <ech:column property="chkbx" cell="cpcheckboxcell" rowid="id" viewsAllowed="cphtml" viewsDenied="cpods,cpjrxml"
                    headerCell="cpselectAll" width="5%" filterable="false" sortable="false" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col" />

                <%--
                <ec:column property="codeImpot" title="table.column.codeimpot" width="20%" sortable="true" 
                filterable="true" style="text-align: center" headerStyle="text-align: center"
                 > </ec:column>
             --%>
                <ec:column property="codeImpot" title="Code impôt" width="20%" style="text-align: center" headerStyle="text-align: center"
                    filterable="true" sortable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col" />

                <%--
                <ec:column property="nomImpot"  title="table.column.nomimpot"  width="20%" sortable="true" 
                style="text-align: right" headerStyle="text-align: center" cell="" 
                 alias="nomImpot"            
                  > 
                    <app:link action="zf7/flux.ex"
                        transition="modifier"
                        attribute="id"
                        value="${row.id}"
                        label="${row.nomImpot}"
                        active="true" 
                        title="Editer le type d'impôt"
                        />
                </ec:column>
            --%>
                <ec:column property="nomImpot" title="Nom imôt" width="20%" style="text-align: right" headerStyle="text-align: center"
                    cell="" alias="nomImpot" filterable="true" sortable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col">
                    <%--                     <app:link action="zf7/flux.ex" transition="modifier" attribute="id" value="${row.id}" label="${row.nomImpot}" --%>
                    <%--                         active="true" title="Editer le type d'impôt" /> --%>
                </ec:column>

                <%--
                <ec:column property="estOuvertALaMensualisation" filterCell="droplist"
                  title="Ouvert à la mensualisation"  width="10%" sortable="true" cell="booleanvraifaux" 
                  style="text-align: center"  
                  headerStyle="text-align: center" viewsDenied="cpjrxml"
                  
                 > </ec:column>
            --%>
                <ec:column property="estOuvertALaMensualisation" filterCell="droplist" title="Ouvert à la mensualisation" width="10%"
                    cell="booleanvraifaux" style="text-align: center" headerStyle="text-align: center" viewsDenied="cpjrxml"
                    filterable="true" sortable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=1 scope=col" />

                <%--
                <ec:column alias="test" cell="image" property="estOuvertALaMensualisation" title="test" width="10%" 
                    sortable="true" style="text-align: center"  
                    headerStyle="text-align: center" 
                    interceptor="fr.gouv.finances.cp.dmo.interceptor.TypeImpotColumnInterceptor"/> 
            --%>
                <ec:column alias="test" cell="image" property="estOuvertALaMensualisation" title="test" width="10%"
                    style="text-align: center" headerStyle="text-align: center"
                    interceptor="fr.gouv.finances.cp.dmo.interceptor.TypeImpotColumnInterceptor" filterable="true" sortable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />

                <%--
                <ec:column property="estDestineAuxParticuliers" title="Destiné aux particuliers" width="10%" sortable="true" cell="booleanouinon" style="text-align: center" headerStyle="text-align: center"
                    viewsDenied="cpjrxml"> </ec:column>
            --%>
                <ec:column property="estDestineAuxParticuliers" title="Destiné aux particuliers" width="10%" cell="booleanouinon"
                    style="text-align: center" headerStyle="text-align: center" viewsDenied="cpjrxml" filterable="true" sortable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />

                <%--
                <ec:column property="jourLimiteAdhesion" title="Date limite de l'adhésion" width="10%" cell="dateheureminute" sortable="true" style="text-align: center" headerStyle="text-align: center"
                   viewsDenied="cpjrxml"> </ec:column>--%>
                <ec:column property="jourLimiteAdhesion" title="Date limite de l'adhésion" width="10%" cell="dateheureminute"
                    style="text-align: center" headerStyle="text-align: center" viewsDenied="cpjrxml" filterable="true" sortable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=4 scope=col" />

                <%--
                <ec:column property="seuil" title="Seuil" width="15%" cell="nombrecelldeuxdecimales" sortable="true" style="text-align: left;" headerStyle="text-align: center"
                  > </ec:column>
              --%>
                <ec:column property="seuil" title="Seuil" width="15%" cell="nombrecelldeuxdecimales" style="text-align: left;"
                    headerStyle="text-align: center" filterable="true" sortable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=5 scope=col" />

                <%--
                <ec:column property="taux" title="Taux" width="10%" cell="nombrecelldeuxdecimales" sortable="true" style="text-align: left;" headerStyle="text-align: center" 
                 > </ec:column>--%>
                <ec:column property="taux" title="Taux" width="10%" cell="nombrecelldeuxdecimales" style="text-align: left;"
                    headerStyle="text-align: center" filterable="true" sortable="true" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=6 scope=col" />
            </ec:row>
        </ec:table>

        <table>
            <tr>
                <td align="left" width="5%">Pourcentage :&nbsp;&nbsp</td>
                <td align="right" width="20%">&nbsp;&nbsp;</td>
                <td align="right" width="20%">&nbsp;&nbsp;</td>
                <td align="left" width="10%">${PourcentageEstOuvertALaMensualisation}%</td>
                <td align="right" width="10%">&nbsp;&nbsp;</td>
                <td align="right" width="10%">&nbsp;&nbsp;</td>
                <td align="left" width="10%">${PourcentageJourLimiteAdhesion}%</td>
                <td align="right" width="15%">&nbsp;&nbsp;</td>
                <td align="right" width="10%">&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td align="left" width="5%">Moyenne :&nbsp;&nbsp</td>
                <td align="left" width="20%">&nbsp;&nbsp&nbsp;&nbsp&nbsp;&nbsp &nbsp;&nbsp&nbsp;&nbsp &nbsp;&nbsp
                    ${MoyenneNonNullCodeImpot}</td>
                <td align="right" width="20%">&nbsp;&nbsp;</td>
                <td align="right" width="10%">&nbsp;&nbsp;</td>
                <td align="right" width="10%">&nbsp;&nbsp;</td>
                <td align="right" width="10%">&nbsp;&nbsp;</td>
                <td align="right" width="10%">&nbsp;&nbsp;</td>
                <td align="right" width="15%">&nbsp;&nbsp;</td>
                <td align="right" width="10%">&nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td align="left" width="5%">Décompte :&nbsp;&nbsp</td>
                <td align="right" width="20%">&nbsp;&nbsp;</td>
                <td align="right" width="20%">&nbsp;&nbsp;</td>
                <td align="left" width="10%">${DecompteEstOuvertALaMensualisation}</td>
                <td align="right" width="10%">&nbsp;&nbsp;</td>
                <td align="right" width="10%">&nbsp;&nbsp;</td>
                <td align="left" width="10%">${DecompteJourLimiteAdhesion}</td>
                <td align="right" width="15%">&nbsp;&nbsp;</td>
                <td align="right" width="10%">&nbsp;&nbsp;</td>
            </tr>

        </table>

        <app:boxboutons>
            <%--
            <app:submit label="Retour" transition="retour" responsive="${responsive}" />
            <app:submit label="Exemple d'édition jdbc" transition="editionjdbc" class="primary" responsive="${responsive}" />
    --%>
            <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}"  />
            <app:submit label="Exemple d'édition jdbc" transition="editionjdbc" class="primary" responsive="${responsive}"
                 />
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>