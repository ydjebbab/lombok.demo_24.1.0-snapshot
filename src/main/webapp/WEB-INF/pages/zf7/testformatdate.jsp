<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page testformatdate.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Test des formats date" menu="true" responsive="${responsive}">

    <app:chemins action="zf7/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Gestion des types d'impôt" transition="listeimpots" responsive="${responsive}">Gestion des types d'impôt</app:chemin>
        <app:chemin title="Pages de test" transition="pagesdetest" responsive="${responsive}">Pages de test</app:chemin>
        <app:chemin title="Format de dates" responsive="${responsive}">Format de dates</app:chemin>
    </app:chemins>

    <app:form action="zf7/flux.ex" formboxclass="eXtremeTable">

        <%--
		<ec:table items="referencetypesimpots" 
		action="${pageContext.request.contextPath}/zf7/flux.ex" 
		tableId="typesimpots"
		autoIncludeParameters="false"
		form="formulaire"
		locale="fr_FR" 
		view="cphtml"
		filterable="true" 	
		showPagination="false" showExports="true" 
		var="row" 
		sortRowsCallback="cp" filterRowsCallback="cp"
		>
		--%>
        <ec:table items="referencetypesimpots" tableId="typesimpots" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/zf7/flux.ex" autoIncludeParameters="false" form="formulaire" locale="fr_FR"
            filterable="true" sortable="true" sortRowsCallback="cp" filterRowsCallback="cp" showPagination="true" showNewPagination="true"
            showExports="true" newPaginationNbPage="5" showStatusBar="true" showPaginationBottom="false" showExportsBottom="false"
            showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')" positionnoresultsfound="body">

            <ec:exportPdf fileName="listeDesTypesImpots.pdf" tooltip="Export PDF" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="test de formats booleans" imageName="pdf" view="cppdf" />

            <ec:exportXls encoding="UTF" fileName="listeDesTypesImpots.xls" imageName="xls" tooltip="Export Excel" view="cpxls" />

            <ec:exportCsv fileName="listeDesTypesImpots.csv" tooltip="Export CSV" delimiter="|" viewResolver="csv" />

            <ec:row highlightRow="true">
                <%--
        <ec:column property="jourLimiteAdhesion" sortable="true" style="text-align: center" headerStyle="text-align: center" width="11%"
             cell="date"  title="date" alias="date"/>
        <ec:column property="jourLimiteAdhesion" sortable="true" style="text-align: center" headerStyle="text-align: center" width="11%"
             cell="datelong"  title="datelong" alias="datelong"/>
        <ec:column property="jourLimiteAdhesion" sortable="true" style="text-align: center" headerStyle="text-align: center" width="11%"
             cell="dateabrege"  title="dateabrege" alias="dateabrege"/>
            <ec:column property="jourLimiteAdhesion" sortable="true" style="text-align: center" headerStyle="text-align: center" width="11%"
             cell="dateheure"  title="dateheure" alias="dateheure"/>
            <ec:column property="jourLimiteAdhesion" sortable="true" style="text-align: center" headerStyle="text-align: center" width="11%"
             cell="dateheureminute"  title="dateheureminute" alias="dateheureminute"/>
            <ec:column property="jourLimiteAdhesion" sortable="true" style="text-align: center" headerStyle="text-align: center" width="11%"
             cell="dateheureminuteseconde"  title="dateheureminuteseconde" alias="dateheureminuteseconde"/>
            <ec:column property="jourLimiteAdhesion" sortable="true" style="text-align: center" headerStyle="text-align: center" width="11%"
             cell="heure"  title="heure" alias="heure" filterable="false"/>
            <ec:column property="jourLimiteAdhesion" sortable="true" style="text-align: center" headerStyle="text-align: center" width="11%"
             cell="heureminute"  title="heure minute" alias="heureminute" filterable="false"/>
            <ec:column property="jourLimiteAdhesion" sortable="true" style="text-align: center" headerStyle="text-align: center" width="11%"
             cell="heureminuteseconde"  title="heure minute seconde" alias="heureminuteseconde" filterable="false"/>
       --%>
                <ec:column property="jourLimiteAdhesion" sortable="true" filterable="true" style="text-align: center"
                    headerStyle="text-align: center" width="11%" cell="date" title="date" alias="date" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=persist scope=col" />
                <ec:column property="jourLimiteAdhesion" sortable="true" filterable="true" style="text-align: center"
                    headerStyle="text-align: center" width="11%" cell="datelong" title="datelong" alias="datelong"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=1 scope=col" />
                <ec:column property="jourLimiteAdhesion" sortable="true" filterable="true" style="text-align: center"
                    headerStyle="text-align: center" width="11%" cell="dateabrege" title="dateabrege" alias="dateabrege"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />
                <ec:column property="jourLimiteAdhesion" sortable="true" filterable="true" style="text-align: center"
                    headerStyle="text-align: center" width="11%" cell="dateheure" title="dateheure" alias="dateheure"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />
                <ec:column property="jourLimiteAdhesion" sortable="true" filterable="true" style="text-align: center"
                    headerStyle="text-align: center" width="11%" cell="dateheureminute" title="dateheureminute" alias="dateheureminute"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=4 scope=col" />
                <ec:column property="jourLimiteAdhesion" sortable="true" filterable="true" style="text-align: center"
                    headerStyle="text-align: center" width="11%" cell="dateheureminuteseconde" title="dateheureminuteseconde"
                    alias="dateheureminuteseconde" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=5 scope=col" />
                <ec:column property="jourLimiteAdhesion" sortable="true" filterable="true" style="text-align: center"
                    headerStyle="text-align: center" width="11%" cell="heure" title="heure" alias="heure" showResponsive="${responsive}"
                    dataAttributes="data-tablesaw-priority=6 scope=col" />
                <ec:column property="jourLimiteAdhesion" sortable="true" filterable="true" style="text-align: center"
                    headerStyle="text-align: center" width="11%" cell="heureminute" title="heure minute" alias="heureminute"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=7 scope=col" />
                <ec:column property="jourLimiteAdhesion" sortable="true" filterable="true" style="text-align: center"
                    headerStyle="text-align: center" width="11%" cell="heureminuteseconde" title="heure minute seconde"
                    alias="heureminuteseconde" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=8 scope=col" />
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" />
                    <app:submit label="Retourner à l'accueil" transition="retour" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" responsive="${responsive}"
                         />
                    <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>