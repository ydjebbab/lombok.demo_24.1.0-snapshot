<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page testformatpourcentage.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Test des formats pourcentage" menu="true" responsive="${responsive}">

    <app:chemins action="zf7/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Références" responsive="${responsive}">Références</app:chemin>
        <app:chemin title="Gestion des types d'impôt" transition="listeimpots" responsive="${responsive}">Gestion des types d'impôt</app:chemin>
        <app:chemin title="Pages de test" transition="pagesdetest" responsive="${responsive}">Pages de test</app:chemin>
        <app:chemin title="Format de pourcentage" responsive="${responsive}">Format de pourcentage</app:chemin>
    </app:chemins>

    <app:form action="zf7/flux.ex" formboxclass="eXtremeTable" responsive="${responsive}">

        <%--
<ec:table items="referencetypesimpots" 
        action="${pageContext.request.contextPath}/zf7/flux.ex" 
        tableId="testfmt1"
        autoIncludeParameters="false"
        form="formulaire"
        locale="fr_FR" 
        view="cphtml"
        filterable="true"   
        showPagination="false" showExports="true" 
        var="row" 
        sortRowsCallback="cp" filterRowsCallback="cp"
        >
 --%>
        <ec:table items="referencetypesimpots" tableId="testfmt1" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/zf7/flux.ex" autoIncludeParameters="false" form="formulaire" locale="fr_FR"
            filterable="true" sortable="true" sortRowsCallback="cp" filterRowsCallback="cp" showPagination="true" showNewPagination="true"
            showExports="true" newPaginationNbPage="5" showStatusBar="true" showPaginationBottom="false" showExportsBottom="false"
            showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')" positionnoresultsfound="body">

            <ec:exportPdf fileName="listeDesTypesImpots.pdf" tooltip="Export PDF" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="test de formats booleans" imageName="pdf" view="cppdf" />

            <ec:exportXls encoding="UTF" fileName="listeDesTypesImpots.xls" imageName="xls" tooltip="Export Excel" view="cpxls" />

            <ec:exportCsv fileName="listeDesTypesImpots.csv" tooltip="Export CSV" delimiter="|" viewResolver="csv" />

            <ecjrxml:exportPdf fileName="testfmt1.pdf" view="cpjrxml" viewResolver="cpjrxml" tooltip="Export PDF (jrxml)" imageName="pdf"
                text="Export PDF (jrxml)" nomapplication="DMO Test" modele="portrait" textepieddepage="Copyright DGCP " />

            <ec:row highlightRow="true">
                <%--
            <ec:column property="taux" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
             cell="pourcentcellzerodecimale"  title="pourcente zero decimale" alias="pourcentzerodecimale"/>
        <ec:column property="taux" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
             cell="pourcentcellunedecimale"  title="pourcente une decimale" alias="pourcentunedecimale"
             calc="total,decompte" calcTitle="Total,Decompte" 
             calcStyle="text-align: right;" calcTitleStyle="text-align: right;"
             />          
        <ec:column property="taux" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
             cell="pourcentcelldeuxdecimales"  title="pourcent deux decimales" alias="pourcentdeuxdecimales"
             calc="total,decompte" calcTitle="Total,Decompte" 
             calcStyle="text-align: right;" calcTitleStyle="text-align: right;"
             />
        <ec:column property="taux" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
             cell="pourcentcelltroisdecimales"  title="pourcent trois decimales" alias="pourcenttroisdecimales"
             calc="total,decompte" calcTitle="Total,Decompte" 
             calcStyle="text-align: right;" calcTitleStyle="text-align: right;"
             />          
        <ec:column property="taux" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
             cell="pourcentcellquatredecimales"  title="pourcent quatre decimales" alias="pourcentquatredecimales"
             calc="total,decompte" calcTitle="Total,Decompte" 
             calcStyle="text-align: right;" calcTitleStyle="text-align: right;"
             />
        <ec:column property="taux" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="16%"
             cell="pourcentcellcinqdecimales"  title="pourcent cinq decimales" alias="pourcentcinqdecimales"
             calc="total,decompte" calcTitle="Total,Decompte" 
             calcStyle="text-align: right;" calcTitleStyle="text-align: right;"
             /> 
              --%>
                <ec:column property="taux" sortable="true" filterable="true" headerStyle="text-align: center" style="text-align: right;"
                    width="16%" cell="pourcentcellzerodecimale" title="pourcente zero decimale" alias="pourcentzerodecimale"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=persist scope=col" />
                <ec:column property="taux" sortable="true" filterable="true" headerStyle="text-align: center" style="text-align: right;"
                    width="16%" cell="pourcentcellunedecimale" title="pourcente une decimale" alias="pourcentunedecimale"
                    calc="total,decompte" calcTitle="Total,Decompte" calcStyle="text-align: right;" calcTitleStyle="text-align: right;"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=1 scope=col" />
                <ec:column property="taux" sortable="true" filterable="true" headerStyle="text-align: center" style="text-align: right;"
                    width="16%" cell="pourcentcelldeuxdecimales" title="pourcent deux decimales" alias="pourcentdeuxdecimales"
                    calc="total,decompte" calcTitle="Total,Decompte" calcStyle="text-align: right;" calcTitleStyle="text-align: right;"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />
                <ec:column property="taux" sortable="true" filterable="true" headerStyle="text-align: center" style="text-align: right;"
                    width="16%" cell="pourcentcelltroisdecimales" title="pourcent trois decimales" alias="pourcenttroisdecimales"
                    calc="total,decompte" calcTitle="Total,Decompte" calcStyle="text-align: right;" calcTitleStyle="text-align: right;"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />
                <ec:column property="taux" sortable="true" filterable="true" headerStyle="text-align: center" style="text-align: right;"
                    width="16%" cell="pourcentcellquatredecimales" title="pourcent quatre decimales" alias="pourcentquatredecimales"
                    calc="total,decompte" calcTitle="Total,Decompte" calcStyle="text-align: right;" calcTitleStyle="text-align: right;"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=4 scope=col" />
                <ec:column property="taux" sortable="true" filterable="true" headerStyle="text-align: center" style="text-align: right;"
                    width="16%" cell="pourcentcellcinqdecimales" title="pourcent cinq decimales" alias="pourcentcinqdecimales"
                    calc="total,decompte" calcTitle="Total,Decompte" calcStyle="text-align: right;" calcTitleStyle="text-align: right;"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=5 scope=col" />

            </ec:row>
        </ec:table>

        <%--
<ec:table items="referencetypesimpots" 
        action="${pageContext.request.contextPath}/zf7/flux.ex" 
        tableId="testfmt2"
        autoIncludeParameters="false"
        form="formulaire"
        locale="fr_FR" 
        view="cphtml"
        filterable="true"   
        showPagination="false" showExports="true" 
        var="row" 
        sortRowsCallback="cp" filterRowsCallback="cp"
        >
         --%>
        <ec:table items="referencetypesimpots" tableId="testfmt2" view="cphtml" var="row"
            action="${pageContext.request.contextPath}/zf7/flux.ex" autoIncludeParameters="false" form="formulaire" locale="fr_FR"
            filterable="true" sortable="true" sortRowsCallback="cp" filterRowsCallback="cp" showPagination="true" showNewPagination="true"
            showExports="true" newPaginationNbPage="5" showStatusBar="true" showPaginationBottom="false" showExportsBottom="false"
            showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')" positionnoresultsfound="body">

            <ec:exportPdf fileName="listeDesTypesImpots.pdf" tooltip="Export PDF" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="test de formats booleans" imageName="pdf" view="cppdf" />

            <ec:exportXls encoding="UTF" fileName="listeDesTypesImpots.xls" imageName="xls" tooltip="Export Excel" view="cpxls" />

            <ec:exportCsv fileName="listeDesTypesImpots.csv" tooltip="Export CSV" delimiter="|" viewResolver="csv" />

            <ecjrxml:exportPdf fileName="testfmt2.pdf" view="cpjrxml" viewResolver="cpjrxml" tooltip="Export PDF (jrxml)" imageName="pdf"
                text="Export PDF (jrxml)" nomapplication="DMO Test" modele="portrait" textepieddepage="Copyright DGCP " />

            <ec:row highlightRow="true">
                <%--
        <ec:column property="taux" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
             cell="pourcentcellsixdecimales"  title="pourcente six decimales" alias="pourcentsixdecimales"/>
        <ec:column property="taux" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
             cell="pourcentcellseptdecimales"  title="pourcent sept decimales" alias="pourcentseptdecimales"/>
        <ec:column property="taux" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
             cell="pourcentcellhuitdecimales"  title="pourcent huit decimales" alias="pourcenthuitdecimales"/>           
        <ec:column property="taux" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
             cell="pourcentcellneufdecimales"  title="pourcent neuf decimales" alias="pourcentneufdecimales"/>
        <ec:column property="taux" sortable="true" headerStyle="text-align: center" style="text-align: right;" width="20%"
             cell="pourcentcelldixdecimales"  title="pourcent dix decimales" alias="pourcentdixdecimales"/> 
              --%>
                <ec:column property="taux" sortable="true" filterable="true" headerStyle="text-align: center" style="text-align: right;"
                    width="20%" cell="pourcentcellsixdecimales" title="pourcente six decimales" alias="pourcentsixdecimales"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=persist scope=col" />
                <ec:column property="taux" sortable="true" filterable="true" headerStyle="text-align: center" style="text-align: right;"
                    width="20%" cell="pourcentcellseptdecimales" title="pourcent sept decimales" alias="pourcentseptdecimales"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=1 scope=col" />
                <ec:column property="taux" sortable="true" filterable="true" headerStyle="text-align: center" style="text-align: right;"
                    width="20%" cell="pourcentcellhuitdecimales" title="pourcent huit decimales" alias="pourcenthuitdecimales"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />
                <ec:column property="taux" sortable="true" filterable="true" headerStyle="text-align: center" style="text-align: right;"
                    width="20%" cell="pourcentcellneufdecimales" title="pourcent neuf decimales" alias="pourcentneufdecimales"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />
                <ec:column property="taux" sortable="true" filterable="true" headerStyle="text-align: center" style="text-align: right;"
                    width="20%" cell="pourcentcelldixdecimales" title="pourcent dix decimales" alias="pourcentdixdecimales"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=4 scope=col" />

            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" />
                    <app:submit label="Retourner à l'accueil" transition="retour" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à la liste des tests" transition="pagesdetest" responsive="${responsive}"
                         />
                    <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>