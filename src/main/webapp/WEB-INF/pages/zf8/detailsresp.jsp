<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="true" />

<app:page titreecran="Test des sélections multiples" menu="true" responsive="${responsive}">

    <app:chemins action="zf8/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test des tags" transition="pagesdetest" responsive="${responsive}">Test des tags</app:chemin>
        <app:chemin title="Test du tag app:details responsive" responsive="${responsive}">Test du tag app:details responsive</app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="testtagsform" formboxclass="donnees" responsive="${responsive}">
        <fieldset>
            <legend>app:details</legend>

            <app:details responsive="${responsive}">
                <app:detailsummary responsive="${responsive}">
                   Exemple avec le détail caché
                  </app:detailsummary>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                    aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </app:details>


            <app:details>
                <app:detailsummary>
            Code source de l'exemple
            </app:detailsummary>
                <pre>
                    ${fn:escapeXml("
                    <app:details>   
                        <app:detailsummary>
                            Exemple avec le détail caché par défaut
                        </app:detailsummary> 
                          <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                             Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                             laboris nisi ut aliquip ex ea commodo consequat. 
                             Duis aute irure dolor in reprehenderit in voluptate velit 
                             esse cillum dolore eu fugiat nulla pariatur. 
                             Excepteur sint occaecat cupidatat non proident, sunt in 
                             culpa qui officia deserunt mollit anim id est laborum.</p>
                     </app:details>
                    ")}
                </pre>
            </app:details>

            <br />

            <app:details open="true">
                <app:detailsummary>
           Exemple avec le détail affiché par défaut (&lt;app:details open="true"&gt;)
       </app:detailsummary>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis
                    aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </app:details>

            <app:details>
                <app:detailsummary>
                    Code source de l'exemple
                </app:detailsummary>
                <pre>
                    ${fn:escapeXml("
                    <app:details open=\"true\">   
                        <app:detailsummary>
                              Exemple avec le détail affiché par défaut (&lt;app:details open=\"true\"&gt;)
                        </app:detailsummary> 
                          <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                          sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                             Ut enim ad minim veniam, quis nostrud exercitation ullamco 
                             laboris nisi ut aliquip ex ea commodo consequat. 
                             Duis aute irure dolor in reprehenderit in voluptate velit 
                             esse cillum dolore eu fugiat nulla pariatur. 
                             Excepteur sint occaecat cupidatat non proident, sunt in 
                             culpa qui officia deserunt mollit anim id est laborum.</p>
                     </app:details>
                    ")}
                </pre>
            </app:details>
        </fieldset>

        <fieldset>

            <legend>Critères de recherche</legend>

            <app:details open="true">
                <app:detailsummary>
                    <b>Période de recherche</b> (saisie par période prédéfinie OU par intervalle) </app:detailsummary>
                <p>
                    <label for="d1">Du</label> <input type="text" name="d1" id="d1" placeholder="mm/aaaa" /> <label for="d2">au</label> <input
                        type="text" name="d2" id="d2" placeholder="mm/aaaa" />
                </p>
            </app:details>

            <app:details open="true">
                <app:detailsummary>
                    <b>Localisations des biens</b>
                </app:detailsummary>

                <p>
                    <label for="pays">Du</label> <a href="#list">(Liste)</a> * <input type="text" name="pays" id="pays" value="France" /> <label
                        for="dep">Département/Tom</label> <a href="#list">(Liste)</a>* <input type="text" name="dep" id="dep"
                        value="Gironde" /> <br /> <label for="com"> Commune</label> <a href="#list">(Liste)</a>* <input type="text"
                        name="com" id="com" placeholder="" />

                </p>
            </app:details>
        </fieldset>
    </app:form>
    <center>
        <button class="" responsive="${responsive}">Rechercher</button>
        <button responsive="${responsive}">Mémoriser les critères</button>
    </center>
</app:page>