<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="false" />

<app:page titreecran="Test des sélections multiples">

    <app:chemins action="zf8/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test des tags" transition="pagesdetest" responsive="${responsive}">Test des tags</app:chemin>
        <app:chemin title="Test du tag app:input" responsive="${responsive}">Test du tag app:input</app:chemin>
        <app:chemin title="Validation" responsive="${responsive}">Validation</app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="testtagsform" formboxclass="donnees">

        <fieldset>
            <app:input attribut="inputField" maxlength="30" size="30" requis="false" inputboxwidth="20%" compboxwidth="40%"
                readonly="${lectureseule}" disabled="${inactif}" disabledwhenreadonly="${inactifquandlectureseule}" />
        </fieldset>

        <app:boxboutons>
            <app:submit label="Page précédente" transition="pageprecedente" />
            <app:submit label="Retour" transition="retour" responsive="${responsive}" />
            <app:submit label="Lancer la tache longue" transition="valider" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>