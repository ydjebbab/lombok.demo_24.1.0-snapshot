<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="true" />

<app:page titreecran="Test du tag tree" menu="true" responsive="${responsive}">

    <app:chemins action="zf8/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test du tag tree" responsive="${responsive}">Test du tag tree </app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="testtagtreeform" formboxclass="donnees">


        <lb:tree tree="arbre.exemple" includeRootNode="false" action="zf8/flux.ex">
            <lb:nodes>
                <lb:nodenavigation />

                <lb:nodeMatch type="type1">
                    <lb:nodetext selectable="false" inline="true" navigable="true">
                        <lb:nodeName />
                    </lb:nodetext>
                </lb:nodeMatch>

                <lb:nodeMatch type="type2">
                    <lb:nodetext selectable="true" inline="true" navigable="false">
                        <lb:nodeName />
                    </lb:nodetext>
                </lb:nodeMatch>

            </lb:nodes>

        </lb:tree>
    </app:form>
</app:page>