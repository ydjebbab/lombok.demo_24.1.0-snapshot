<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="true" />

<app:page titreecran="Test d'un lancement de tâche longue" menu="true" responsive="${responsive}">

    <app:chemins action="zf8/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test d'un lancement de tâche longue" responsive="${responsive}">Test d'un lancement de tâche longue</app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="lancertachelongueform" responsive="${responsive}">

        <I>Le bouton "Lancer la tache longue" permet de lancer une tache asynchrone qui va changer la casse de tous les noms des
            contribuables.
            <p>Vous serez averti par mel de la fin de la tache.</p>
        </I>
        <app:boxboutons>
            <app:submit label="Retourner à l'accueil" transition="annuler" responsive="${responsive}" />
            <app:submit label="Lancer la tâche longue" transition="valider" responsive="${responsive}" />
        </app:boxboutons>

    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>