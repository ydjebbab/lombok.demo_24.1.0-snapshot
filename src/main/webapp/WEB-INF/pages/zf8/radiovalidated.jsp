<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="false"/>

<app:page titreecran="Test des sélections multiples">

    <app:chemins action="zf8/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test des tags" transition="pagesdetest" responsive="${responsive}">Test des tags</app:chemin>
        <app:chemin title="Test du tag app:radio" responsive="${responsive}">Test du tag app:radio</app:chemin>
        <app:chemin title="Validation" responsive="${responsive}">Validation</app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="testtagsform" formboxclass="donnees">

        <fieldset>
            <app:radio attribut="radioFieldFromArrayList" label="nomImpot" value="codeImpot" itemslist="${listelementsdelalistemultiple}"
                requis="false" inputboxwidth="20%" compboxwidth="40%" readonly="${lectureseule}" />
            <app:radio attribut="radioFieldFromHashSet" label="nomImpot" value="codeImpot" itemsset="${hashsetelementsdelalistemultiple}"
                requis="false" readonly="${lectureseule}" />

            <app:radio attribut="radioFieldFromLinkedHashSet" label="nomImpot" value="codeImpot"
                itemsset="${linkedhashsetelementsdelalistemultiple}" requis="false" readonly="${lectureseule}" />

            <app:radio attribut="radioFieldFromHashMap" itemsmap="${mapelementsdelalistemultiple}" requis="false" readonly="${lectureseule}" />
            <app:radio attribut="radioFieldFromLinkedHashMap" itemsmap="${linkedhashmapelementsdelalistemultiple}" requis="false"
                readonly="${lectureseule}" />
        </fieldset>


        <app:boxboutons>
            <app:submit label="Page précédente" transition="pageprecedente" />
            <app:submit label="Retourner à la liste des tests" transition="pagesdetest" responsive="${responsive}" />
            <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>