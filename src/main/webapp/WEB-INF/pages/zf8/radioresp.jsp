<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="true" />

<app:page titreecran="Test des sélections multiples" responsive="${responsive}">

    <app:chemins action="zf8/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test des tags" transition="pagesdetest" responsive="${responsive}">Test des tags</app:chemin>
        <app:chemin title="Test du tag app:radio responsive" responsive="${responsive}">Test du tag app:radio responsive</app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="testtagsform" formboxclass="donnees" responsive="${responsive}">
        <c:set var="lectureseule" value="false" />
        <c:set var="inactif" value="false" />
        <c:set var="inactifquandlectureseule" value="true" />

        <fieldset>
            <app:radio attribut="radioFieldFromArrayList" label="nomImpot" value="codeImpot" itemslist="${listelementsdelalistemultiple}"
                requis="false" inputboxwidth="20%" compboxwidth="40%" readonly="${lectureseule}" responsive="${responsive}" />
            <app:radio attribut="radioFieldFromHashSet" label="nomImpot" value="codeImpot" itemsset="${hashsetelementsdelalistemultiple}"
                requis="false" readonly="${lectureseule}" responsive="${responsive}" />

            <app:radio attribut="radioFieldFromLinkedHashSet" label="nomImpot" value="codeImpot"
                itemsset="${linkedhashsetelementsdelalistemultiple}" requis="false" readonly="${lectureseule}" responsive="${responsive}" />

            <app:radio attribut="radioFieldFromHashMap" itemsmap="${mapelementsdelalistemultiple}" requis="false" readonly="${lectureseule}"
                responsive="${responsive}" />
            <app:radio attribut="radioFieldFromLinkedHashMap" itemsmap="${linkedhashmapelementsdelalistemultiple}" requis="false"
                readonly="${lectureseule}" responsive="${responsive}" />
        </fieldset>

        <app:boxboutons>
            <app:submit label="Valider (readonly)" transition="validerreadonly" responsive="${responsive}" />
            <app:submit label="Retourner à la liste des tests" transition="pagesdetest" responsive="${responsive}" />
            <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>