<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="true" />

<app:page titreecran="Test des sélections multiples" responsive="${responsive}">

    <app:chemins action="zf8/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test des tags" transition="pagesdetest" responsive="${responsive}">Test des tags</app:chemin>
        <app:chemin title="Test du tag app:checkbox" responsive="${responsive}">Test du tag app:checkbox</app:chemin>
        <app:chemin title="Validation" responsive="${responsive}">Validation</app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="testtagsform" formboxclass="donnees">

        <fieldset>
            <app:checkbox attribut="checkboxListFromArrayList" label="nomImpot" value="codeImpot"
                itemslist="${listelementsdelalistemultiple}" requis="false" inputboxwidth="20%" compboxwidth="40%"
                readonly="${lectureseule}" />
            <app:checkbox attribut="checkboxListFromHashSet" label="nomImpot" value="codeImpot"
                itemsset="${hashsetelementsdelalistemultiple}" requis="false" readonly="${lectureseule}" />

            <app:checkbox attribut="checkboxListFromLinkedHashSet" label="nomImpot" value="codeImpot"
                itemsset="${linkedhashsetelementsdelalistemultiple}" requis="false" readonly="${lectureseule}" />

            <app:checkbox attribut="checkboxListFromHashMap" itemsmap="${mapelementsdelalistemultiple}" requis="false"
                readonly="${lectureseule}" />
            <app:checkbox attribut="checkboxListFromLinkedHashMap" itemsmap="${linkedhashmapelementsdelalistemultiple}" requis="false"
                readonly="${lectureseule}" />
        </fieldset>


        <app:boxboutons>
            <app:submit label="Page précédente" transition="pageprecedente" responsive="${responsive}"/>
            <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}" />
            <app:submit label="Lancer la tâche longue" transition="valider" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>