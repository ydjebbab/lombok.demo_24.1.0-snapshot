<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="false" />

<app:page titreecran="Test des sélections multiples" menu="true" responsive="${responsive}">

    <app:chemins action="zf8/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test des tags" transition="pagesdetest" responsive="${responsive}">Test des tags</app:chemin>
        <app:chemin title="Test du tag app:select" responsive="${responsive}">Test du tag app:select</app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="testtagsform" formboxclass="donnees" responsive="${responsive}">


        <c:set var="lectureseule" value="false" />
        <c:set var="inactif" value="false" />
        <c:set var="inactifquandlectureseule" value="true" />

        <fieldset>
            <app:select attribut="selectFieldFromArrayList" label="nomImpot" value="codeImpot" itemslist="${listelementsdelalistemultiple}"
                defautitem="true" defautitemlabel="Aucune sélection" defautitemvalue="-999999" requis="false" inputboxwidth="20%"
                compboxwidth="40%" readonly="${lectureseule}" disabled="${inactif}" disabledwhenreadonly="${inactifquandlectureseule}"
                responsive="${responsive}" />
            <app:select attribut="selectFieldFromHashSet" label="nomImpot" value="codeImpot" itemsset="${hashsetelementsdelalistemultiple}"
                defautitem="true" defautitemlabel="Aucune sélection" defautitemvalue="-999999" requis="false" readonly="${lectureseule}"
                disabled="${inactif}" disabledwhenreadonly="${inactifquandlectureseule}" responsive="${responsive}" />

            <app:select attribut="selectFieldFromLinkedHashSet" label="nomImpot" value="codeImpot"
                itemsset="${linkedhashsetelementsdelalistemultiple}" defautitem="true" defautitemlabel="Aucune sélection"
                defautitemvalue="-999999" requis="false" readonly="${lectureseule}" disabled="${inactif}"
                disabledwhenreadonly="${inactifquandlectureseule}" responsive="${responsive}" />

            <app:select attribut="selectFieldFromHashMap" itemsmap="${mapelementsdelalistemultiple}" defautitem="true"
                defautitemlabel="Aucune sélection" defautitemvalue="-999999" requis="false" readonly="${lectureseule}" disabled="${inactif}"
                disabledwhenreadonly="${inactifquandlectureseule}" responsive="${responsive}" />
            <app:select attribut="selectFieldFromLinkedHashMap" itemsmap="${linkedhashmapelementsdelalistemultiple}" defautitem="true"
                defautitemlabel="Aucune sélection" defautitemvalue="-999999" requis="false" readonly="${lectureseule}" disabled="${inactif}"
                disabledwhenreadonly="${inactifquandlectureseule}" responsive="${responsive}" />
        </fieldset>

        <app:boxboutons>
            <app:submit label="Valider (readonly)" transition="validerreadonly" responsive="${responsive}" />
            <app:submit label="Valider (disabled)" transition="validerdisabled" responsive="${responsive}" />
            <app:submit label="Valider (disabledwhenreadonly)" transition="validerdisabledreadonly" responsive="${responsive}" />
            <app:submit label="Valider (not disabledwhenreadonly)" transition="validernotdisabledreadonly" responsive="${responsive}" />
            <app:submit label="Retourner à la liste des tests" transition="pagesdetest" responsive="${responsive}" />
            <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}" />
        </app:boxboutons>
    </app:form>
</app:page>