<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="false" />

<app:page titreecran="Test du tag page" menu="true" bandeau="true" donneescontainer="true" responsive="${responsive}">

    <app:chemins action="zf8/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test des tags" responsive="${responsive}">Test des tags</app:chemin>
        <app:chemin title="Test du tag app:page" responsive="${responsive}">Test du tag app:page</app:chemin>
    </app:chemins>

</app:page>