<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<c:set var="responsive" value="false" />

<app:page titreecran="Test des sélections multiples" responsive="${responsive}">
    <script type="text/javascript" src="<c:url value="/webjars/jquery/1.8.2/jquery.js"/>"></script>

    <app:chemins action="zf8/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Tests" responsive="${responsive}">Tests</app:chemin>
        <app:chemin title="Test des tags" transition="pagesdetest" responsive="${responsive}">Test des tags</app:chemin>
        <app:chemin title="Test du tag app:selectmultiple" responsive="${responsive}">Test du tag app:selectmultiple</app:chemin>
    </app:chemins>

    <app:form action="zf8/flux.ex" formobjectname="testtagsform" formboxclass="donnees" responsive="${responsive}">
        <c:set var="lectureseule" value="false" />
        <c:set var="inactif" value="false" />
        <c:set var="inactifquandlectureseule" value="true" />

        <fieldset>
            <app:selectmultiple attribut="multipleListFromArrayList" label="nomImpot" value="codeImpot"
                itemslist="${listelementsdelalistemultiple}" requis="false" size="30" inputboxwidth="20%" compboxwidth="40%"
                readonly="${lectureseule}" disabled="${inactif}" disabledwhenreadonly="${inactifquandlectureseule}"
                responsive="${responsive}">
            </app:selectmultiple>
            <app:selectmultiple attribut="multipleListFromHashSet" label="nomImpot" value="codeImpot"
                itemsset="${hashsetelementsdelalistemultiple}" defautitem="false" defautitemlabel="Aucune sélection"
                defautitemvalue="-999999" requis="false" size="4" readonly="${lectureseule}" disabled="${inactif}"
                disabledwhenreadonly="${inactifquandlectureseule}" responsive="${responsive}" />

            <input type="HIDDEN" id="setSelected" name="setSelected" value="${setSelected}" />


            <app:selectmultiple attribut="multipleListFromLinkedHashSet" label="nomImpot" value="codeImpot"
                itemsset="${linkedhashsetelementsdelalistemultiple}" defautitem="true" defautitemlabel="Aucune sélection"
                defautitemvalue="-999999" requis="false" size="4" readonly="${lectureseule}" disabled="${inactif}"
                disabledwhenreadonly="${inactifquandlectureseule}" responsive="${responsive}" />

            <app:selectmultiple attribut="multipleListFromHashMap" itemsmap="${mapelementsdelalistemultiple}" defautitem="true"
                defautitemlabel="Aucune sélection" defautitemvalue="-999999" requis="false" size="4" readonly="${lectureseule}"
                disabled="${inactif}" disabledwhenreadonly="${inactifquandlectureseule}" responsive="${responsive}" />
            <app:selectmultiple attribut="multipleListFromLinkedHashMap" itemsmap="${linkedhashmapelementsdelalistemultiple}"
                defautitem="true" defautitemlabel="Aucune sélection" defautitemvalue="-999999" requis="false" size="4"
                readonly="${lectureseule}" disabled="${inactif}" disabledwhenreadonly="${inactifquandlectureseule}"
                responsive="${responsive}" />
        </fieldset>

        <app:boxboutons>
            <app:submit label="Valider (readonly)" transition="validerreadonly" onclick="slectmultiple()" responsive="${responsive}" />
            <app:submit label="Valider (disabled)" transition="validerdisabled" responsive="${responsive}" />
            <app:submit label="Valider (disabledwhenreadonly)" transition="validerdisabledreadonly" responsive="${responsive}" />
            <app:submit label="Valider (not disabledwhenreadonly)" transition="validernotdisabledreadonly" responsive="${responsive}" />
            <app:submit label="Retourner à la liste des tests" transition="pagesdetest" responsive="${responsive}" />
            <app:submit label="Retourner à l'accueil" transition="retour" responsive="${responsive}" />
        </app:boxboutons>

        <script type="text/javascript">
									function slectmultiple() {

										var inputForm = document
												.getElementById("setSelected").value;

										var listid = document
												.getElementById('id3');

										// get the options length
										var len = listid.length;

										//on ne traite que le cas dans cet exemple d'1 ou 2 éléments sélectionnés
										if (inputForm !== "") {

											if (inputForm.length == 3) {
												listid[inputForm[1]].selected = true;
											}
											if (inputForm.length == 6) {
												listid[inputForm[1]].selected = true;
												listid[inputForm[4]].selected = true;
											}

											var formulaire = null; // ciblage du formulaire
											var inputForm = null;
											var listid = null;

											$("#setSelected").val(null);

										}
									}
								</script>
        <script type="text/javascript">
									$(window).load(slectmultiple());
								</script>
    </app:form>
</app:page>