<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page choisirstatistique.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Tableaux et éditions statistiques" menu="true" responsive="${responsive}">

    <app:chemins action='zf3/flux.ex' responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Tableaux et éditions statistiques" responsive="${responsive}">Tableaux et éditions statistiques</app:chemin>
    </app:chemins>


    <app:onglets cleactive="1" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Tableaux et éditions statistiques" ongletclass="btmodel_inv_half" responsive="${responsive}" />
    </app:onglets>


    <app:form method="post" action="zf3/flux.ex" formid="statistiques" formobjectname="statistiquesform" onsubmit="return submit_form(500)"
        formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}" rwdformclass="form-horizontal">

        <fieldset>

            <app:input libelle="Seuil d'agrégation" pctlibelle="10%" maxlength="10" size="10" attribut="seuilAgregation"
                responsive="${responsive}" />

            <p>Exemples de tableaux liés avec des éditions (parfois les mêmes que ci-dessous) ou des graphiques jasper</p>
            <app:radio libelle="Type de tableau statistique" pctlibelle="30%" itemsmap="${rapports}" attribut="rapport" />
            <app:boxboutons>
                <app:submit label="Consulter un tableau" transition="consulter" id="enterButton" />
            </app:boxboutons>

            <p>Exemples d'édition</p>
            <table border="1">
                <thead>
                    <tr>
                        <td><b>Edition</b></td>
                        <td><b>Caractéristiques</b></td>
                        <td>&nbsp;</td>
                    </tr>
                </thead>
                <tr>
                    <td>agregcontriparavis</td>
                    <td>Production asynchrone, totalisation par page, totalisation pour l'édition, affichage conditionnel, stub
                        uniquement</td>
                    <td><app:button title="Créer l'édition agregcontriparavis" action="zf3/flux.ex" transition="agregcontriparavis">Créer l'édition agregcontriparavis</app:button></td>
                </tr>
                <tr>
                    <td>agregavispartypeimpotseuil</td>
                    <td>Production synchrone Passage de paramètres, rupture sur un champ, reutilisation de service metier</td>
                    <td><app:submit label="Créer l'édition agregavispartypeimpotseuil" transition="agregavispartypeimpotseuil" /></td>
                </tr>
                <tr>
                    <td>casinospardepartement</td>
                    <td>Production synchrone. Tableau croisé, stub uniquement</td>
                    <td><app:button title="casinospardepartement" action="zf3/flux.ex" transition="exemplecasinos">Créer l'édition casinospardepartement</app:button></td>
                </tr>
                <tr>
                    <td>casinos test caractères dégradés</td>
                    <td>Production synchrone. Edition d'une lettre contenant des caractères accentués dégradés</td>
                    <td><app:button title="Créer l'édition casinos test caractères dégradés" action="zf3/flux.ex"
                            transition="testeditioncardegrade">Créer l'édition casinos test caractères dégradés</app:button></td>
                </tr>

                <tr>
                    <td>agregcontripartypeimpot</td>
                    <td>Production synchrone. Edition html</td>
                    <td><app:link action="zf3/flux.ex" transition="agregcontripartypeimpot" openwindow="true"
                            label="Créer l'édition agregcontripartypeimpot" /> <app:button title="agregcontripartypeimpot"
                            action="zf3/flux.ex" transition="agregcontripartypeimpot" openwindow="true">Créer l'édition agregcontripartypeimpot</app:button></td>
                </tr>
                <tr>
                    <td>adressescontribuables, sousrapportadresse</td>
                    <td>Production asynchrone, utilisation d'un sous-rapport, stub uniquement</td>
                    <td>Voir menu Contribuables/Gestion des contribuables/Edition liste des contribuables</td>
                </tr>
                <tr>
                    <td>listecontribuables</td>
                    <td>Production asynchrone, via SRVED2</td>
                    <td>Voir menu Contribuables/Gestion des contribuables/Edition liste des contribuables</td>
                </tr>
            </table>
            <br />
            <app:boxboutons>
                <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="annuler" responsive="${responsive}" />
            </app:boxboutons>

        </fieldset>

    </app:form>
</app:page>