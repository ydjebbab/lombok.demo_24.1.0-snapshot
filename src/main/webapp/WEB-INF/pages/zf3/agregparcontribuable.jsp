<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page agregparcontribuable.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Statistiques" menu="true" responsive="${responsive}">

    <app:chemins action='zf3/flux.ex' responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Tableaux et éditions statistiques" responsive="${responsive}" transition="retour">Tableaux et éditions statistiques</app:chemin>
        <app:chemin title="Agrégation par contribuable des avis d'imposition" responsive="${responsive}">Agrégation par contribuable des avis d'imposition</app:chemin>
    </app:chemins>

    <app:onglets cleactive="2" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Tableaux et éditions statistiques" responsive="${responsive}" transition="retour"
            ongletclass="btmodel_inv_half" />
        <app:onglet cle="2" libelle="Avis d'imposition" responsive="${responsive}" ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form method="post" action="zf3/flux.ex" formid="statistiques" formobjectname="statistiquesform" onsubmit="return submit_form(500)"
        formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}">

        <%--
        <ec:table title="Agrégation des avis d'imposition par contribuable" items="statistiques" action="flux.ex" form="statistiques"
            autoIncludeParameters="false" var="row" showPagination="true" showStatusBar="false" filterable="false" sortable="false"
            locale="fr_FR" width="100%" rowsDisplayed="20" theme="eXtremeTable centerTable">
         --%>
        <ec:table title="Agrégation des avis d'imposition par contribuable" items="statistiques" var="row" action="flux.ex"
            autoIncludeParameters="false" form="statistiques" locale="fr_FR" filterable="true" sortable="true" rowsDisplayed="20"
            showPagination="true" showNewPagination="true" showExports="true" newPaginationNbPage="5" showStatusBar="false"
            showPaginationBottom="false" showExportsBottom="false" showNewFilter="true" onInvokeAction="ecupdatectra('tableaunavigation')"
            positionnoresultsfound="body">

            <ec:exportPdf fileName="zf3.agregcontriparavis.edition" text="zf3.agregcontriparavis.parametres" view="jasper"
                viewResolver="jasper" tooltip="Export PDF" headerColor="black" headerBackgroundColor="#b6c2da"
                headerTitle="Agregation des contribuables par avis d'imposition" imageName="pdf" />

            <ec:parameter name="_eventId_tableaunavigation" value="tableaunavigation" />
            <ec:row>
                <%--
                <ec:column property="identifiant" title="Identifiant du contribuable" width="30%" />
                <ec:column property="nombreAvis" title="Nombre d'avis" width="20%" />
                <ec:column property="montantMoyen" title="Montant moyen des avis" cell="currency" format="### ### ###0,00" width="25%" />
                <ec:column property="montantTotal" title="Montant total des avis" cell="currency" format="### ### ###0,00" width="25%" />
                 --%>
                <ec:column property="identifiant" title="Identifiant du contribuable" width="30%" filterable="true" sortable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=persist scope=col" />
                <ec:column property="nombreAvis" title="Nombre d'avis" width="20%" filterable="true" sortable="true"
                    showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=1 scope=col" />
                <ec:column property="montantMoyen" title="Montant moyen des avis" cell="currency" format="### ### ###0,00" width="25%"
                    filterable="true" sortable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=2 scope=col" />
                <ec:column property="montantTotal" title="Montant total des avis" cell="currency" format="### ### ###0,00" width="25%"
                    filterable="true" sortable="true" showResponsive="${responsive}" dataAttributes="data-tablesaw-priority=3 scope=col" />
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à la page précédente" transition="retour" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à la page précédente" transition="retour" responsive="${responsive}"
                         />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>