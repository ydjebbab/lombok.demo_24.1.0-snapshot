<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page recherchecodepostalcriteres.jsp -->

<app:page titreecran="Code postaux" titrecontainer="Recherche >> Recherche d'un code postal" menu="true">

    <app:onglets cleactive="1">
        <app:onglet cle="1" libelle="Saisie des criteres de recherche" />
    </app:onglets>

    <app:form method="post" action="zf2/flux.ex" formid="recherchecodepostal" formobjectname="recherchecodepostalform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets">

        <app:input attribut="critere.code" libelle="Code postal :" maxlength="50" size="50" />
        <app:input attribut="critere.ville" libelle="Commune :" maxlength="50" size="50" />
        <app:input attribut="critere.departement" libelle="Département :" maxlength="50" size="50" />
        <app:input attribut="critere.region" libelle="Région :" maxlength="50" size="50" />
        <p class="commentaire">Le nom de la commune doit être écrit sans accentuation (é,è,ç,à,etc.) ni ponctuation (virgule, tiret,
            apostrophe, etc.). Les noms "SAINT" et "SAINTE" doivent être écrits "ST" et "STE" à l'exception de "SAINTES" et "SAINTS".</p>
        <app:radio libelle="Type de recherche" attribut="critere.cedexOuEtCp" itemsmap="${typesderecherche}" boxheight="60px" />

        <app:boxboutons>
            <app:submit label="Retourner à l'accueil" transition="_eventId_annuler" />
            <app:submit label="Rechercher" transition="_eventId_recherchercodepostal" id="enterButton" class="primary" />
        </app:boxboutons>


    </app:form>
    <div style="position: absolute; top: 70%; left: 20%; width: 80%; height: auto">
        <ec:table items="listecodepostaux" title="Choisissez un code postal dans la liste" autoIncludeParameters="false" var="row"
            showPagination="false" showStatusBar="false" filterable="false" sortable="false" locale="fr_FR" width="80%" theme="eXtremeTable">
            <ec:row>
                <ec:column property="code" title="Code Postal">
                    <a href="flux.ex?_flowExecutionKey=${flowExecutionKey}&_eventId=selection&code=${row.code}&ville=${row.ville}">
                        ${row.code}</a>
                </ec:column>
                <ec:column property="ville" title="Commune" />
            </ec:row>
        </ec:table>
    </div>
</app:page>