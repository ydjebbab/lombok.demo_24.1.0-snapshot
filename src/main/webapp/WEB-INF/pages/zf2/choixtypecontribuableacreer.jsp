<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page choixtypecontribuableacreer.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Ajouter un contribuable" menu="true" responsive="${responsive}">

    <app:chemins action="zf2/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Gestion des contribuables" responsive="${responsive}">Gestion des contribuables</app:chemin>
        <app:chemin title="Ajouter un contribuable" responsive="${responsive}">Ajouter un contribuable</app:chemin>
    </app:chemins>

    <app:onglets cleactive="1" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Ajouter un contribuable" responsive="${responsive}" ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form method="post" action="zf2/flux.ex" formid="creationmodificationcontribuable"
        formobjectname="creationmodificationcontribuableform" onsubmit="return submit_form(500)" formboxid="donnees"
        formboxclass="donnees sousonglets" responsive="${responsive}">


        <app:select attribut="typeDeContribuable" libelle="Type de contribuable" itemsmap="${typescontribuablesmap}"
            responsive="${responsive}" />

        <%--         <app:boxboutons> --%>
        <%--             <app:submit label="Annuler" transition="annuler" responsive="${responsive}" /> --%>
        <%--             <app:submit label="Valider" transition="valider" id="enterButton" class="primary" responsive="${responsive}" /> --%>
        <%--         </app:boxboutons> --%>

        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                    <app:submit label="Valider le type de contribuable" transition="valider" id="enterButton" />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil"
                        responsive="${responsive}" />
                    <app:submit label="Valider le type de contribuable" transition="valider" id="enterButton" responsive="${responsive}"
                        inputclass="primary btn btn-default" />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>

    </app:form>
</app:page>