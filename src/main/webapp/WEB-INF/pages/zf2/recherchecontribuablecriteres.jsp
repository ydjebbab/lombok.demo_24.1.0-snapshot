<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page recherchecontribuablecriteres.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Rechercher un contribuable" titrecontainer="Rechercher un contribuable" menu="true" responsive="${responsive}">

    <app:chemins action='zf2/flux.ex' responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Gestion des contribuables" responsive="${responsive}">Gestion des contribuables</app:chemin>
        <app:chemin title="Rechercher un contribuable" responsive="${responsive}">Rechercher un contribuable</app:chemin>
    </app:chemins>


    <app:onglets cleactive="1" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" libelle="Saisie des criteres de recherche" responsive="${responsive}" ongletclass="btmodel_inv_half" />
    </app:onglets>


    <app:form method="post" action="zf2/flux.ex" formid="recherchecontribuable" formobjectname="recherchecontribuableform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets" responsive="${responsive}"
        autocomplete="on" rwdformclass="form-horizontal">
    
        <fieldset>
            <legend>Critères de recherche</legend>
<p  >Les champs marqués d'un "*" sont obligatoires</p>
            
                <app:checkbox attribut="crit.typesCiviliteId" itemsmap="${typesdecivilite}" readonly="false" libelle="Recherche par civilité :"
                    boxwidth="100%" labelboxwidth="29.9%" theme="H" checkall='true' inputboxwidth="auto" responsive="${responsive}" bootstraplblclass="btmodel_third"
                    bootstrapgrpclass="btmodel_quarter" id="rech_contr_type_civilite"></app:checkbox>
            

            <%--             <app:input attribut="crit.identifiant" libelle="Recherche par code utilisateur :" maxlength="50" size="20" requis="true" --%>
            <%--                 theme="H" boxwidth="55%" compboxwidth="0" labelboxwidth="55%" inputboxwidth="44%" boxheight="20px" --%>
            <%--                 responsive="${responsive}" bootstrapclass="btmodel_half" /> --%>
            <app:input attribut="crit.identifiant" libelle="Recherche par l'identifiant :" maxlength="50" size="20" requis="true" theme="H"
                boxwidth="55%" compboxwidth="0" labelboxwidth="55%" inputboxwidth="44%" boxheight="20px" responsive="${responsive}"
                bootstrapclass="btmodel_half" />

            <app:input attribut="crit.adresseMail" libelle="Adresse de messagerie :" maxlength="50" size="20" requis="false" theme="H"
                boxwidth="45%" compboxwidth="0" labelboxwidth="45%" inputboxwidth="50%" boxheight="20px" responsive="${responsive}"
                bootstrapclass="btmodel_half" />

            <c:if test="${responsive eq 'true'}">
                <div class="clearfix"></div>
            </c:if>

            <app:input attribut="crit.referenceAvis" libelle="Référence d'un avis d'imposition :" maxlength="50" size="20" requis="false"
                theme="H" boxwidth="55%" compboxwidth="0" labelboxwidth="55%" inputboxwidth="44%" boxheight="20px"
                responsive="${responsive}" bootstrapclass="btmodel_half" />

            <app:radio attribut="crit.typeContriPart" itemsmap="${typecontripart}" libelle="Rechercher un contribuable particulier"
                consigne="Rechercher uniquement les contribuable de type particulier" theme="H" boxwidth="45%" compboxwidth="0"
                inputboxwidth="initial" boxheight="40px" responsive="${responsive}" bootstrapclass="btmodel_half" 
                id="rech_contr_part"/>

            <app:radio attribut="crit.typeRechercheId" itemsmap="${typesderecherche}" libelle="Type de recherche"
                consigne="Choisissez un type de recherche" theme="H" boxwidth="100%" compboxwidth="0" labelboxwidth="29.9%"
                inputboxwidth="auto" responsive="${responsive}" bootstraplblclass="btmodel_quarter" bootstrapgrpclass="btmodel_third" 
                id="type_rech" />

        </fieldset>
        <p>
            <br />
        </p>
        <fieldset>
            <legend>Options de recherche</legend>



            <app:select attribut="crit.typeTriId" itemsmap="${typesdetri}" libelle="Ordre de tri :" theme="V" boxheight="25px"
                boxwidth="100%" labelboxwidth="35%" inputboxwidth="60%" requis="false" defautitem="false" responsive="${responsive}" />
            <app:select attribut="crit.typeCritereDeTriId" itemsmap="${typesdecriteredetri}" libelle="Critère de tri :" maxlength="50"
                size="50" requis="false" theme="H" boxheight="25px" boxwidth="60%" labelboxwidth="59%" inputboxwidth="38%"
                defautitem="false" responsive="${responsive}" />
            <app:select attribut="crit.typeNbLignesId" itemsmap="${typesdenblignes}" libelle="Nombre de lignes à retourner :" theme="V"
                boxheight="35px" boxwidth="100%" requis="false" defautitem="false" responsive="${responsive}" />
        </fieldset>
        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="annuler" />
                    <app:submit label="Rechercher" transition="rechercher" id="enterButton" class="primary" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" transition="annuler" responsive="${responsive}" />
                    <app:submit label="Rechercher" transition="rechercher" id="enterButton" responsive="${responsive}"
                        inputclass="btn btn-default primary" />
                </c:otherwise>
            </c:choose>
            <app:reset />
        </app:boxboutons>
        <!--        <script type="text/javascript" src="https://getfirebug.com/firebug-lite-debug.js"></script> -->

    </app:form>
</app:page>