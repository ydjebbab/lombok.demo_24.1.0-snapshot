<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page resultatsrecherchecontribuableavecavis.jsp -->
<!-- PAS ENCORE RESPONSIVE -->

<c:set var="responsive" value="true" />

<app:page titreecran="Résultats de la recherche">

    <app:chemins action="zf2/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Rechercher un contribuable" transition="NouvelleRecherche">Rechercher un contribuable</app:chemin>
        <app:chemin title="Résultats de la recherche">Résultats de la recherche</app:chemin>
    </app:chemins>


    <app:form action="zf2/flux.ex" formid="formulaire" formboxclass="eXtremeTable">

        <ec:table items="listecontribuable" tableId="rechcontavecavis" view="cphtml" var="cont" action="flux.ex" state="default"
            stateAttr="gotofirstpage" autoIncludeParameters="false" form="formulaire" locale="fr_FR" filterable="true" sortable="true"
            sortRowsCallback="cp" filterRowsCallback="cpfiltreinfsup" rowsDisplayed="10" showPagination="true">
            <ec:row highlightRow="true">
                <ech:column group="groupe1" subGroup="sousgroupe11" property="identifiant" title="Identifiant..." width="10%"
                    sortable="true" filterable="true" />
                <ech:column group="groupe1" subGroup="sousgroupe12" property="nom" title="Nom" width="15%" sortable="true" filterable="true" />
                <ech:column group="groupe2" subGroup="sousgroupe22" property="prenom" title="Prénom" width="15%" sortable="true"
                    filterable="true" />
                <ech:column group="groupe3" subGroup="sousgroupe31" property="adresseMail" title="Adresse de messagerie" width="30%"
                    sortable="true" filterable="true" />
                <ech:column group="groupe3" subGroup="sousgroupe31" property="dateDeNaissance" title="Date de naissance" cell="date"
                    width="10%" sortable="true" filterable="true" />
                <ech:column group="groupe3" subGroup="sousgroupe32" property="solde" title="solde (€)" cell="monnaiecelldeuxdecimales"
                    width="15%" style="text-align: right" filterable="true" />
                <ech:column group="groupe4" subGroup="sousgroupe41" property="listeAvisImposition" filterable="false">
                    <table>
                        <c:forEach var="avis" items="${cont.listeAvisImposition}">
                            <tr>
                                <td>${avis.reference}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </ech:column>
            </ec:row>
        </ec:table>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="Annuler" title="Retourner à l'accueil" />
                    <app:submit label="Retourner à la liste des contribuables" title="Retourner à la liste des contribuables"
                        transition="RetourResultats" />
                    <app:submit label="Nouvelle recherche" title="Nouvelle recherche" transition="NouvelleRecherche" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="Annuler" responsive="${responsive}" />
                    <app:submit label="Retourner à la liste des contribuables" title="Retourner à la liste des contribuables"
                        transition="RetourResultats" responsive="${responsive}" />
                    <app:submit label="Nouvelle recherche" title="Nouvelle recherche" transition="NouvelleRecherche"
                        responsive="${responsive}" />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>
    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>