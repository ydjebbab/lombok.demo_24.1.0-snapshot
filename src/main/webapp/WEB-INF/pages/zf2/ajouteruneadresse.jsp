<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page ajouteruneadresse.jsp -->
<app:page titreecran="Gérer les adresses">

    <app:chemins action="zf2/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Résultats de la recherche" active="false" transition="NouvelleRecherche">Résultats de la recherche</app:chemin>
        <app:chemin title="Modifier un contribuable" transition="retour">Modifier un contribuable</app:chemin>
        <app:chemin title="Gérer les adresses">Gérer les adresses</app:chemin>
    </app:chemins>

    <app:form action="zf2/flux.ex" formobjectname="creationmodificationcontribuableform">
        <p>
            <br />
        </p>
        <fieldset>
            <legend>Adresse postale</legend>
            <div>
                <app:textarea attribut="adresse.ligne1" cols="32" rows="2" readonly="${readonly}" requis="false" />
                <app:input attribut="adresse.ligne2" maxlength="32" size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="adresse.ligne3" maxlength="32" size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="adresse.ligne4" maxlength="50" size="50" readonly="${readonly}" requis="false" />
                <app:input attribut="adresse.codePostal" maxlength="8" size="8" readonly="${readonly}" inputboxwidth="30%" />
                <app:input attribut="adresse.ville" maxlength="32" size="32" readonly="${readonly}" />
                <app:select attribut="adresse.pays" itemsmap="${paysmap}" readonly="${readonly}" />
            </div>
        </fieldset>

        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                    <app:submit label="Enregistrer l'adresse" transition="enregistrer" id="enterButton" title="Enregistrer l'adresse" />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" responsive="${responsive}" />
                    <app:submit label="Enregistrer l'adresse" transition="enregistrer" id="enterButton" responsive="${responsive}"
                        inputclass="primary btn btn-default" title="Enregistrer l'adresse" />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>
        <p>
            <br>
        </p>

    </app:form>
</app:page>