<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page confcreatcontribapresnorm.jsp -->
<app:page titreecran="Confirmer la création d'un contribuable après normalisation">

    <app:chemins action="zf2/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Gestion des contribuables" responsive="${responsive}">Gestion des contribuables</app:chemin>
        <app:chemin title="Ajouter un contribuable" transition="choixtypecontribuable">Ajouter un contribuable</app:chemin>
        <app:chemin title="Créer un contribuable" transition="pageprecedente">Créer un contribuable</app:chemin>
        <app:chemin title="Confirmer la création">Confirmer la création</app:chemin>
    </app:chemins>

    <app:onglets cleactive="2">
        <app:onglet cle="1" transition="choixtypecontribuable" libelle="Ajouter un contribuable" />
        <app:onglet cle="2" transition="choixtypecontribuable" libelle="Informations sur le contribuable" />
    </app:onglets>

    <app:form method="post" action="zf2/flux.ex" formid="saisiecontribuable" formobjectname="creationmodificationcontribuableform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets">

        <div style="text-align: center; margin-right: auto; margin-left: auto; width: 80%">
            <p>Des éléments de votre ancienne adresse ont été changés, complétés ou abrégés afin de les mettre aux normes de la Poste.
                Acceptez-vous ces modifications?</p>
        </div>

        <c:set var="readonly" value="true" />

        <fieldset>
            <legend>Identité</legend>
            <app:input attribut="contribuable.identifiant" libelle="Identifiant" maxlength="50" size="50" readonly="${readonly}" />
            <app:select attribut="contribuable.civilite" itemslist="${civiliteliste}" label="libelle" value="code" readonly="${readonly}"
                defautitem="false" />
            <app:input attribut="contribuable.nom" libelle="Nom" maxlength="36" size="36" readonly="${readonly}" />
            <app:input attribut="contribuable.prenom" libelle="Prénom" maxlength="32" size="32" readonly="${readonly}" />
            <app:input attribut="contribuable.adresseMail" libelle="Adresse mail" maxlength="50" size="50" readonly="${readonly}" />
            <app:input attribut="contribuable.dateDeNaissance" libelle="Date de naissance" maxlength="10" size="10" readonly="${readonly}"
                requis="false" inputboxwidth="10%">
                <app:calendar />
            </app:input>
            <app:input attribut="contribuable.solde" maxlength="50" size="50" readonly="${readonly}" />
        </fieldset>
        <br />
        <fieldset>
            <legend>Adresse postale</legend>
            <div>
                <app:input attribut="contribuable.listeAdresses[0].ligne1" libelle="adresse Etage - escalier - appartement" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].ligne2" libelle="Immeuble - bâtiment - résidence" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].ligne3" libelle="adresse N° et libellé de la voie" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].ligne4" libelle="Lieu-dit ou boîte postale" maxlength="50" size="50"
                    readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].codePostal" libelle="Code postal" maxlength="8" size="8"
                    readonly="${readonly}" />
                <app:select attribut="contribuable.listeAdresses[0].ville" libelle="Localité" itemslist="${listevilles}" label="ville"
                    value="ville" />
                <app:select attribut="contribuable.listeAdresses[0].pays" libelle="Pays" itemsmap="${paysmap}" readOnly="true" />
            </div>
        </fieldset>

        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                    <app:submit label="Retourner à la page précédente" transition="pageprecedente" title="Retourner à la page précédente" />
                    <app:submit label="Confirmer l'enregistrement" transition="enregistrer" id="enterButton"
                        title="Confirmer l'enregistrement" />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" responsive="${responsive}" />
                    <app:submit label="Retourner à la page précédente" transition="pageprecedente" title="Retourner à la page précédente"
                        responsive="${responsive}" />
                    <app:submit label="Confirmer l'enregistrement" transition="enregistrer" id="enterButton" responsive="${responsive}"
                        inputclass="primary btn btn-default" title="Confirmer l'enregistrement" />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>
    </app:form>
</app:page>