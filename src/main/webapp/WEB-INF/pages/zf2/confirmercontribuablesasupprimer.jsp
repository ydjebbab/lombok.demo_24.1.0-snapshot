<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page confirmercontribuablesasupprimer.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Confirmer la suppression" menu="true" responsive="${responsive}">

    <app:chemins action="zf2/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Gestion des contribuables" responsive="${responsive}">Gestion des contribuables</app:chemin>
        <app:chemin title="Rechercher un contribuable" active="false" transition="NouvelleRecherche" responsive="${responsive}">Rechercher un contribuable</app:chemin>
        <app:chemin title="Résultats de la recherche" transition="Annuler" responsive="${responsive}">Résultats de la recherche</app:chemin>
        <app:chemin title="Confirmer la suppression" responsive="${responsive}">Confirmer la suppression</app:chemin>
    </app:chemins>

    <p>Les données suivantes vont être supprimées</p>
    <app:form method='POST' formobjectname='suppressioncontribuableform' action='zf2/flux.ex'>
        <spring:bind path="suppressioncontribuableform.listeDesContribuablesASupprimer">
            <ec:table items="status.value" form="formulaire" var="cont" autoIncludeParameters="false" filterable="false" sortable="false"
                showPagination="false" showStatusBar="false" locale="fr_FR">
                <ec:row highlightRow="false">
                    <ec:column property="identifiant" title="Identifiant" width="15%" />
                </ec:row>
            </ec:table>
        </spring:bind>

        <c:choose>
            <c:when test="${responsive != 'true'}">
                <app:boxboutons>
                    <app:submit label="Retourner à la liste des contribuables" transition="Annuler"
                        title="Retourner à la liste des contribuables" />
                    <app:submit label="Supprimer le contribuable" transition="Supprimer" title="Supprimer le contribuable" />
                </app:boxboutons>
            </c:when>
            <c:otherwise>
                <app:boxboutons>
                    <app:submit label="Retourner à la liste des contribuables" transition="Annuler"
                        title="Retourner à la liste des contribuables" responsive="${responsive}" />
                    <app:submit label="Supprimer le contribuable" transition="Supprimer" responsive="${responsive}"
                        inputclass="primary btn btn-default" title="Supprimer le contribuable" />
                </app:boxboutons>
            </c:otherwise>
        </c:choose>

    </app:form>
</app:page>
