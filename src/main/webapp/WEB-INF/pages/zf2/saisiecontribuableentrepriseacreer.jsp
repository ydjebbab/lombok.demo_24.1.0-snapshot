<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page saisiecontribuableentrepriseacreer.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Créer d'un contribuable de type entreprise" menu="true" responsive="${responsive}">

    <app:chemins action="zf2/flux.ex" responsive="${responsive}">
        <app:chemin transition="annuler" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Gestion des contribuables" responsive="${responsive}">Gestion des contribuables</app:chemin>
        <app:chemin title="Ajouter un contribuable" transition="choixtypecontribuable" responsive="${responsive}">Ajouter un contribuable</app:chemin>
        <app:chemin title="Créer un contribuable" responsive="${responsive}">Créer un contribuable</app:chemin>
    </app:chemins>

    <app:onglets cleactive="2" responsive="${responsive}" boxongletsclass="row height_equalizer ongletcontainer">
        <app:onglet cle="1" transition="choixtypecontribuable" libelle="Ajouter un contribuable" responsive="${responsive}"
            ongletclass="btmodel_inv_half" />
        <app:onglet cle="2" transition="choixtypecontribuable" libelle="Informations sur le contribuable" responsive="${responsive}"
            ongletclass="btmodel_inv_half" />
    </app:onglets>

    <app:form method="post" action="zf2/flux.ex" formid="creationmodificationcontribuable"
        formobjectname="creationmodificationcontribuableform" onsubmit="return submit_form(500)" formboxid="donnees"
        formboxclass="donnees sousonglets" responsive="${responsive}">

        <c:set var="readonly" value="false" />

        <fieldset>
            <legend>Identité</legend>
            <app:input attribut="contribuable.identifiant" libelle="Identifiant" maxlength="50" size="50" accesskey="I"
                readonly="${readonly}" responsive="${responsive}" />
        </fieldset>
        <p></p>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="annuler" title="Retourner à l'accueil" />
                    <app:submit label="Retourner à la page précédente" title="Retourner à la page précédente"
                        transition="choixtypecontribuable" />
                    <app:submit label="Valider la création" title="Valider la création" transition="valider" id="enterButton"
                        class="primary" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="annuler" responsive="${responsive}" />
                    <app:submit label="Retourner à la page précédente" title="Retourner à la page précédente"
                        transition="choixtypecontribuable" responsive="${responsive}" />
                    <app:submit label="Valider la création" title="Valider la création" transition="valider" id="enterButton"
                        class="primary" responsive="${responsive}" />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
</app:page>