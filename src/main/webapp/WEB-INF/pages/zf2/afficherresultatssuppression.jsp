<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page afficherresultatssuppression.jsp -->
<c:set var="responsive" value="true" />

<app:page titreecran="Résultat de la suppression" menu="true" responsive="${responsive}">

    <app:chemins action="zf2/flux.ex" responsive="${responsive}">
        <app:chemin action="accueil.ex" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin title="Contribuables" responsive="${responsive}">Contribuables</app:chemin>
        <app:chemin title="Gestion des contribuables" responsive="${responsive}">Gestion des contribuables</app:chemin>
        <app:chemin title="Rechercher un contribuable" active="false" transition="NouvelleRecherche" responsive="${responsive}">Rechercher un contribuable</app:chemin>
        <app:chemin title="Résultats de la recherche" transition="Retour" responsive="${responsive}">Résultats de la recherche</app:chemin>
        <app:chemin title="Confirmer la suppression" responsive="${responsive}">Confirmer la suppression</app:chemin>
        <app:chemin title="Résultat de la suppression" responsive="${responsive}">Résultat de la suppression</app:chemin>
    </app:chemins>

    <!-- TABLE PAS ENCORE RESPONSIVE -->

    <p class="titretableau">Les données suivantes ont été supprimées</p>
    <app:form method="POST" action="zf2/flux.ex" formid="suppressioncontribuableform">

        <spring:bind path="suppressioncontribuableform.listeDesContribuablesASupprimer">
            <ec:table items="status.value" form="suppressioncontribuableform" var="cont" autoIncludeParameters="false" filterable="false"
                sortable="false" showPagination="false" showStatusBar="false" locale="fr_FR" tableId="resultatSuppression"
                theme="eXtremeTable">
                <ec:row>
                    <ec:column property="identifiant" title="Identifiant" width="15%" />
                </ec:row>
            </ec:table>
        </spring:bind>

        <%--         <app:boxboutons> --%>
        <%--             <app:submit label="Retour" transition="Retour" responsive="${responsive}" /> --%>
        <%--         </app:boxboutons> --%>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="retour" title="Retourner à l'accueil" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="retour"
                        responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
</app:page>
