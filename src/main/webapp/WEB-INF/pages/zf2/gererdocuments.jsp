<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page gererdocuments.jsp -->
<app:page titreecran="Gérer les documents">

    <app:chemins action="zf2/flux.ex">
        <app:chemin action="accueil.ex" title="Retour à l'accueil">Accueil</app:chemin>
        <app:chemin title="Rechercher un contribuable" active="false" transition="NouvelleRecherche">Rechercher un contribuable</app:chemin>
        <app:chemin title="Résultats de la recherche" transition="retour">Résultats de la recherche</app:chemin>
        <app:chemin title="Gérer les documents">Gérer les documents</app:chemin>
    </app:chemins>

    <app:form action="zf2/flux.ex" formobjectname="gestiondocumentsform">
        <c:if test="${! empty gestiondocumentsform.contribuable.lesDocsJoints }">
            <ec:table items="gestiondocumentsform.contribuable.lesDocsJoints" action="${pageContext.request.contextPath}/zf2/flux.ex"
                tableId="tabledoc" autoIncludeParameters="false" form="formulaire" locale="fr_FR" view="cphtml" filterable="false"
                sortable="false" showPagination="false" showExports="false" var="row">

                <ec:row highlightRow="true" namePropertyShowIfEqualsLast="leFichierJoint.typeMimeFichier">
                    <ech:column property="chkbx" cell="cpcheckboxcell" rowid="hashcode" viewsAllowed="cphtml" headerCell="cpselectAll"
                        width="5%" filterable="false" sortable="false" />
                    <ec:column property="leFichierJoint.nomFichierOriginal" alias="vide" title="Fichier n°" width="5%" sortable="true">${ROWCOUNT} </ec:column>
                    <ec:column property="leFichierJoint.nomFichierOriginal" alias="nomFichierOriginal" title="Fichier" width="30%"
                        sortable="true">
                        <app:link action="zf2/flux.ex" transition="telecharger" attribute="hashcode" value="${lb:hashcode(row)}"
                            label="${row.leFichierJoint.nomFichierOriginal}" active="true" />
                    </ec:column>
                    <ec:column property="identifiant" title="Identifiant du fichier" width="10%" sortable="true" writeOrBlank="true">
                    </ec:column>
                    <ec:column property="leFichierJoint.tailleFichier" alias="tailleFichier" title="Taille (octets)" width="10%"
                        sortable="true">
                    </ec:column>
                    <ec:column property="leFichierJoint.typeMimeFichier" alias="typeMimeFichier" title="Type" width="25%" sortable="true">
                    </ec:column>
                    <ec:column property="leFichierJoint.dateHeureSoumission" alias="dateHeureSoumission" title="Soumission" width="20%"
                        sortable="true" cell="date" format="HH:mm dd/MM/yyyy">
                    </ec:column>
                </ec:row>
            </ec:table>
        </c:if>
        <p>
            <br />
        </p>
        <app:boxboutons>
            <app:submit label="Retour" transition="retour" />
            <app:submit label="Supprimer" transition="supprimer" />
        </app:boxboutons>
        <p>
            <br />
        </p>
        <fieldset>
            <legend>
                <app:submit label="Ajouter le document" transition="ajouterdocument" />
            </legend>
            <app:input attribut="documentJoint.identifiant" libelle="Identification du document" labelboxwidth="35%" maxlength="10"
                size="10" />
            <app:textformate libelle="Fichier transféré" attribut="documentJoint.leFichierJoint.nomFichierOriginal" inputboxwidth="25%">

                <app:button action="zf2/flux.ex" transition="upload" openwindow="true" label="Transférer un fichier" name="transferer"
                    w_name="Transferer" w_height="500">Transférer</app:button>
            </app:textformate>
        </fieldset>

    </app:form>
    <app:disperror var="${erreurentreflux}" />
</app:page>