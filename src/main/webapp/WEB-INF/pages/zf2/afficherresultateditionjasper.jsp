<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page afficherresultateditionjasper.jsp -->

<c:set var="responsive" value="true" />

<app:page titreecran="Edition produite" menu="true" responsive="${responsive}">
    <app:form action="zf2/flux.ex" responsive="${responsive}">

        <p>
            L'édition demandée est accessible à l'adresse suivante : (
            <app:lienedition jobhistory="${jobHistory}" openwindow="false" />
            ).
        </p>

<%--         <app:boxboutons> --%>
<%--             <app:submit label="Retour à l'accueil" transition="retour" responsive="${responsive}" /> --%>
<%--         </app:boxboutons> --%>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="retour" title="Retourner à l'accueil" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="retour"
                        responsive="${responsive}"  />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
</app:page>