<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page afficherresultatscreation.jsp -->

<c:set var="responsive" value="true" />

<!-- PAS ENCORE RESPONSIVE -->

<app:page titreecran="Nouveau contribuable" menu="true" responsive="${responsive}">

    <app:chemins action="zf2/flux.ex" responsive="${responsive}">
        <app:chemin transition="retour" title="Retour à l'accueil" responsive="${responsive}">Accueil</app:chemin>
        <app:chemin responsive="${responsive}">Création d'un contribuable</app:chemin>
        <app:chemin responsive="${responsive}">Nouveau contribuable</app:chemin>
    </app:chemins>

    <app:onglets cleactive="1">
        <app:onglet cle="1" libelle="Informations sur le contribuable" />
    </app:onglets>

    <app:form method="post" action="zf2/flux.ex" formid="resultatscreation" formobjectname="creationmodificationcontribuableform"
        onsubmit="return submit_form(500)" formboxid="donnees" formboxclass="donnees sousonglets">

        <c:set var="readonly" value="true" />

        <fieldset>
            <legend>Identité</legend>
            <app:input attribut="contribuable.identifiant" libelle="Identifiant" maxlength="50" size="50" readonly="${readonly}"
                responsive="${responsive}" />
            <app:select attribut="contribuable.civilite" itemslist="${civiliteliste}" label="libelle" value="code" readonly="${readonly}"
                defautitem="false" responsive="${responsive}" />
            <app:input attribut="contribuable.nom" libelle="Nom" maxlength="36" size="36" readonly="${readonly}" responsive="${responsive}" />
            <app:input attribut="contribuable.prenom" libelle="Prénom" maxlength="32" size="32" readonly="${readonly}"
                responsive="${responsive}" />
            <app:input attribut="contribuable.adresseMail" libelle="Adresse mail" maxlength="50" size="50" readonly="${readonly}"
                responsive="${responsive}" />
            <app:input attribut="contribuable.dateDeNaissance" maxlength="10" size="10" requis="false" readonly="${readonly}"
                inputboxwidth="10%" consigne="Veuillez saisir une date de la forme jj/mm/aaaa">
                <app:calendar />
            </app:input>
            <app:input attribut="contribuable.solde" maxlength="50" size="50" readonly="${readonly}" requis="false" />
        </fieldset>
        <p></p>
        <fieldset>
            <legend>Adresse postale</legend>
            <div>
                <app:input attribut="contribuable.listeAdresses[0].ligne1" libelle="adresse Etage - escalier - appartement" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].ligne2" libelle="Immeuble - bâtiment - résidence" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].ligne3" libelle="adresse N° et libellé de la voie" maxlength="32"
                    size="32" readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].ligne4" libelle="Lieu-dit ou boîte postale" maxlength="50" size="50"
                    readonly="${readonly}" requis="false" />
                <app:input attribut="contribuable.listeAdresses[0].codePostal" libelle="Code postal" maxlength="8" size="8"
                    readonly="${readonly}" />
                <app:input attribut="contribuable.listeAdresses[0].ville" libelle="Localité" maxlength="32" size="32" readonly="${readonly}" />
                <app:select attribut="contribuable.listeAdresses[0].pays" libelle="Pays" itemsmap="${paysmap}" readonly="${readonly}" />
            </div>
        </fieldset>

        <app:boxboutons>
            <c:choose>
                <c:when test="${responsive != 'true'}">
                    <app:submit label="Retourner à l'accueil" transition="retour" title="Retourner à l'accueil"
                        id="enterButton" />
                </c:when>
                <c:otherwise>
                    <app:submit label="Retourner à l'accueil" title="Retourner à l'accueil" transition="retour"
                        responsive="${responsive}"  id="enterButton" />
                </c:otherwise>
            </c:choose>
        </app:boxboutons>

    </app:form>
</app:page>