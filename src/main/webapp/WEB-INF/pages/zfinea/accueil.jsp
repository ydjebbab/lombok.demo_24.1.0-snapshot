<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/tags/html/includes.tagf"%>

<!--  Page zfinea/accueil.jsp  -->

<spring:eval expression="@environment.getProperty('appli.inea.version')" var="ineaVersion"></spring:eval>
<spring:eval expression="@environment.getProperty('appli.inea.jquery-version')" var="jqueryVersion"></spring:eval>
<spring:eval expression="@environment.getProperty('appli.inea.css-url')" var="ineaAppliCssUrl"></spring:eval>

<c:set var="ineaUrl" value="webjars/inea/${ineaVersion}"></c:set>
<c:set var="jqueryUrl" value="webjars/jquery/${jqueryVersion}"></c:set>


<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
<meta charset="utf-8" />
<title><c:out value="${libellelong}" /></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="${title}" />
<meta name="author" content="DGFiP" />
<meta name="msapplication-config" content="none" />
<meta name="format-detection" content="telephone=no" />

<link rel="icon" sizes="16x16 32x32 48x48 64x64" href="${ineaUrl}/img/favicon.ico" />
<!--[if IE]>
    <link rel="shortcut icon" href="${ineaUrl}/img/favicon.ico" />
<![endif] -->
<!-- Optional: Android & iPhone -->
<link rel="icon apple-touch-icon-precomposed" href="${ineaUrl}/img/favicon-152.png" />
<!-- Optional: IE10 Tile. -->
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="${ineaUrl}/img/favicon-144.png" />
<!-- Optional: ipads, androids, iphones, ... -->
<link rel="apple-touch-icon" sizes="152x152" href="${ineaUrl}/img/favicon-152.png" />
<link rel="apple-touch-icon" sizes="144x144" href="${ineaUrl}/img/favicon-144.png" />
<link rel="apple-touch-icon" sizes="120x120" href="${ineaUrl}/img/favicon-120.png" />
<link rel="apple-touch-icon" sizes="114x114" href="${ineaUrl}/img/favicon-114.png" />
<link rel="apple-touch-icon" sizes="72x72" href="${ineaUrl}/img/favicon-72.png" />
<link rel="apple-touch-icon" href="${ineaUrl}/img/favicon-57.png" />

<link href="${ineaAppliCssUrl}" rel="stylesheet" />
<script src="${jqueryUrl}/dist/jquery.min.js"></script>
<script src="${ineaUrl}/js/inea.min.js" async defer></script>
<script src="${ineaUrl}/js/jquery-ui.min.js" async defer></script>
</head>
<body>

    <noscript>
        <p class="js-off">Javascript est desactivé dans votre navigateur.</p>
    </noscript>

    <!-- inea:browsehappy:debut -->
    <!--[if lt IE 8]>
<div class="browsehappy">
  <div class="container">
    <p>Savez-vous que votre navigateur est obsolète ?</p>
      <p> Pour naviguer de la manière la plus satisfaisante sur le Web, nous vous recommandons de procéder à une
        <a href="http://windows.microsoft.com/fr-fr/internet-explorer/download-ie">mise à jour de votre navigateur</a>.
        <br>Vous pouvez aussi
        <a href="http://browsehappy.com/">essayer d’autres navigateurs web populaires</a>.
      </p>
  </div>
</div>
<![endif]-->
    <!-- inea:browsehappy:fin -->

    <div class="container contenu-bg row-marge-basse">
        <!-- inea:liens-evitement:debut -->
        <div class="row">
            <div class="col-xs-12">
                <p class="sr-only">Accès rapide</p>
                <ul class="skiplinks">
                    <li><a href="#contenu">Contenu</a></li>
                </ul>
            </div>
        </div>
        <!-- inea:liens-evitement:fin -->
        <!-- inea:entete:debut -->
        <header class="public" role="banner">
            <div class="row row-marge-haute row-marge-basse">
                <div class="col-xs-10 col-xs-offset-2 col-sm-6 col-sm-offset-1 col-md-6 col-md-offset-0">
                    <!-- inea:accueil-entete:debut -->
                    <a class="accueil-entete" href="index.html">
                        <div class="marianne-entete logo-marianne"></div>
                        <div class="titre-entete">
                            <div class="libelle-titre-entete">
                                <span class="text-particulier">Gabarits types </span> INEA
                            </div>
                            <div class="logo-titre-entete logo-impots-gouv-fr"></div>
                        </div>
                    </a>
                    <!-- inea:accueil-entete:fin -->
                </div>
                <div class="col-xs-12 col-sm-5 col-md-6">
                    <!-- inea:actions-privees:debut -->
                    <!-- inea:actions-privees:fin -->
                </div>
            </div>
            <!-- inea:details-identification:debut -->
            <!-- inea:details-identification:fin -->


            <!-- Chemins vers les ressources statiques externes -->



            <!-- Documentation Inea outils -->




            <!-- Chemins vers les ressources statiques de l'application -->


            <!-- inea:barre-navigation:debut -->
            <div class="row">
                <div class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"
                            aria-controls="navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Menu</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                                class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar-collapse" class="collapse navbar-collapse">
                        <nav id="menu" role="navigation" aria-label="Menu principal">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="/www/inea-outils/0.0.27/docs/cus/index.html"> <span
                                        class="dgfipicon dgfipicon-accueil-menu icon-marge-droite"></span>Accueil <span class="sr-only">,
                                            élément actif</span>

                                </a></li>
                                <li class="dropdown "><a class="collectivite" href="/www/inea-outils/0.0.27/docs/cus/index.html"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span
                                        class="dgfipicon dgfipicon-table-des-matieres icon-marge-droite"></span>Structure

                                </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/structure_vide.html">Structure vide</a></li>
                                    </ul></li>
                                <li class="dropdown"><a class="particulier" href="/www/inea-outils/0.0.27/docs/cus/index.html"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span
                                        class="dgfipicon dgfipicon-exercer-dom icon-marge-droite"></span>Navigation

                                </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/fil-ariane.html">Fil d'ariane</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/menu_horizontal.html">Menu horizontal</a>
                                        </li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/menu_vertical.html">Menu vertical</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/pagination.html">Pagination</a></li>
                                    </ul></li>
                                <li class="dropdown "><a class="international" href="/www/inea-outils/0.0.27/docs/cus/index.html"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span
                                        class="dgfipicon dgfipicon-visualiser-oui icon-marge-droite"></span>Consultation

                                </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/messages.html">Messages</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/rubrique.html">Rubrique</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/sous-rubriques.html">Sous-rubriques</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/rubrique-aide-droite.html">Rubrique
                                                aide droite</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/recapitulatif.html">Récapitulatif</a></li>
                                    </ul></li>
                                <li class="dropdown "><a class="partenaire" href="/www/inea-outils/0.0.27/docs/cus/index.html"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span
                                        class="dgfipicon dgfipicon-enregistrer icon-marge-droite"></span>Formulaires

                                </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/bouton-radio.html">Bouton radio</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/champ-saisie-obligatoire.html">Champ
                                                de saisie obligatoire</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/champ-saisie-calendrier-obligatoire.html">Champ
                                                de saisie calendrier obligatoire</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/groupe-champs.html">Groupe de champs</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/groupe-champs-obligatoires.html">Groupe
                                                de champs obligatoires</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/case-cocher.html">Case à cocher</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/champ-saisie-informations.html">Champs
                                                de saisie avec informations</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/liste-selection-obligatoire.html">Liste
                                                de sélection obligatoire</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/liste-selection-groupes.html">Liste de
                                                sélection avec des groupes</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/proposition.html">Formulaire
                                                proposition</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/barre-progression.html">Barre de
                                                progression</a></li>
                                    </ul></li>
                                <li class="dropdown "><a class="professionnel" href="/www/inea-outils/0.0.27/docs/cus/index.html"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span
                                        class="dgfipicon dgfipicon-consulter-comptes icon-marge-droite"></span>Tableaux

                                </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/tableaux-vue-ensemble.html">Tableaux -
                                                vue d'ensemble</a></li>
                                    </ul></li>
                                <li class="dropdown"><a class="collectivite" href="/www/inea-outils/0.0.27/docs/cus/index.html"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span
                                        class="dgfipicon dgfipicon-sortie-page icon-marge-droite"></span>Modales

                                </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/dialogue-attente.html">Dialogue
                                                attente</a></li>
                                    </ul></li>
                                <li class="dropdown"><a class="particulier" href="/www/inea-outils/0.0.27/docs/cus/index.html"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span
                                        class="dgfipicon dgfipicon-profil icon-marge-droite"></span>Pictogrammes

                                </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/public/index.html">En attente &lt;- Accueil</a></li>
                                    </ul></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- inea:barre-navigation:fin -->

        </header>
        <!-- inea:entete:fin -->
    </div>


    <!-- Chemins vers les ressources statiques externes -->



    <!-- Documentation Inea outils -->




    <!-- Chemins vers les ressources statiques de l'application -->



    <!-- Début espace de dialogue -->
    <main id="contenu" class="contenu" role="main">
    <div class="container">
        <!-- inea:fil-ariane:debut -->
        <div class="row row-marge-haute row-marge-basse">
            <div class="col-xs-12 col-lg-avec-marge-basse">
                <div class="sr-only">
                    <p>Vous êtes ici</p>
                    <ol class="breadcrumb">
                        <li><span>Accueil</span></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- inea:fil-ariane:fin -->
        <!-- inea:messages-espace-dialogue:debut -->
        <!-- inea:messages-espace-dialogue:fin -->
        <!-- inea:titre-espace-dialogue:debut -->
        <!-- inea:element-plier-deplier:debut -->
        <!-- Debut ligne titre -->
        <div class="row titre-espace-dialogue">
            <div class="col-xs-12 col-lg-sans-gouttiere-gauche col-lg-sans-gouttiere-droite">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-lg-avec-marge-basse">
                                <div class="titre-espace-dialogue">
                                    <button type="button" class="collapse-toggle collapsed" data-toggle="collapse"
                                        data-target="#contenu-titre" aria-controls="contenu-titre" aria-expanded="false">
                                        <span class="h1" role="heading" aria-level="1">Cas d'utilisation INEA</span> <span
                                            class="collapseicon" title="Déplier" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="collapse" id="contenu-titre" role="region" tabindex="-1" style="height: 227.433px;">
                            <div class="row">
                                <div class="col-xs-12">
                                    <!-- Debut ligne 1 -->
                                    <div class="row rubrique">
                                        <!-- Début rubrique -->
                                        <div class="col-xs-12 rubrique">
                                            <div class="row titre-rubrique">
                                                <div class="col-xs-12">
                                                    <div class="titre">
                                                        <h2>
                                                            <span class="glyphicon glyphicon-sunglasses icon-marge-droite"></span>Explorer
                                                            la Démo
                                                        </h2>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <p>
                                                        La maquette <a href="/www/inea-outils/0.0.27/index.html">Démo</a> est un exemple
                                                        d'utilisation de la bibliothèque de composants <a
                                                            href="http://venezia.appli.dgfip/www/inea/">INEA</a>.
                                                    </p>
                                                    <p>
                                                        Afin de ne pas créer des exemples liés aux spécificités de tel ou tel métier DGFiP,
                                                        on a choisi une thématique neutre, celle d'un site de voyage. <br>De cette
                                                        manière chaque projet peut s'en inspirer dans la création de son IHM et les
                                                        personaliser à son métier.
                                                    </p>
                                                    <p>
                                                        Dans ces pages on explore les différents éléments constituants de la <a
                                                            href="/www/inea-outils/0.0.27/index.html">Démo</a>.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Fin rubrique -->
                                    </div>
                                    <!-- Fin ligne 1 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- inea:element-plier-deplier:fin -->
        <!-- Fin ligne titre -->
        <div class="row rubrique row-marge-haute">
            <!-- Début rubrique -->
            <div class="col-xs-12 col-sm-6 col-md-4 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                <div class="contenu rubrique well-lg bg-collectivite">
                    <div class="row">
                        <div class="col-xs-12 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                            <button type="button" class="collapse-toggle" data-toggle="collapse" data-target="#contenu-rubrique-1"
                                aria-controls="contenu-rubrique-1" aria-expanded="false">
                                <div class="font-size-h2 text-center">
                                    <span class="dgfipicon dgfipicon-table-des-matieres icon-marge-droite"></span> Structure
                                </div>
                            </button>
                        </div>
                    </div>
                    <div class="collapse out" id="contenu-rubrique-1" role="region" tabindex="-1">
                        <div class="row row-marge-haute">
                            <div class="col-xs-12">
                                <ul class="liste">
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/structure_vide.html">Structure d'une page
                                            vide</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fin rubrique -->
            </div>
            <!-- Fin rubrique -->
            <!-- Début rubrique -->
            <div class="col-xs-12 col-sm-6 col-md-4 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                <div class="contenu rubrique well-lg bg-particulier">
                    <div class="row">
                        <div class="col-xs-12 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                            <button type="button" class="collapse-toggle" data-toggle="collapse" data-target="#contenu-rubrique-2"
                                aria-controls="contenu-rubrique-2" aria-expanded="false">
                                <div class="font-size-h2 text-center">
                                    <span class="dgfipicon dgfipicon-exercer-dom icon-marge-droite"></span> Navigation
                                </div>
                            </button>
                        </div>
                    </div>
                    <div class="collapse out" id="contenu-rubrique-2" role="region" tabindex="-1">
                        <div class="row row-marge-haute">
                            <div class="col-xs-12">
                                <ul class="liste">
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/fil-ariane.html">Fil d'ariane</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/menu_horizontal.html">Menu horizontal</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/menu_vertical.html">Menu vertical</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/pagination.html">Pagination</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin rubrique -->
            <!-- Début rubrique -->
            <div class="col-xs-12 col-sm-6 col-md-4 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                <div class="contenu rubrique well-lg bg-international">
                    <div class="row">
                        <div class="col-xs-12 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                            <button type="button" class="collapse-toggle" data-toggle="collapse" data-target="#contenu-rubrique-3"
                                aria-controls="contenu-rubrique-3" aria-expanded="false">
                                <div class="font-size-h2 text-center">
                                    <span class="dgfipicon dgfipicon-visualiser-oui icon-marge-droite"></span> Consultation
                                </div>
                            </button>
                        </div>
                    </div>
                    <div class="collapse out" id="contenu-rubrique-3" role="region" tabindex="-1">
                        <div class="row row-marge-haute">
                            <div class="col-xs-12">
                                <ul class="liste">
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/messages.html">Messages</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/rubrique.html">Rubrique</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/sous-rubriques.html">Sous-rubriques</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/rubrique-aide-droite.html">Rubrique aide
                                            droite</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/recapitulatif.html">Récapitulatif</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin rubrique -->
            <!-- Début rubrique -->
            <div class="col-xs-12 col-sm-6 col-md-4 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                <div class="contenu rubrique well-lg bg-partenaire">
                    <div class="row">
                        <div class="col-xs-12 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                            <button type="button" class="collapse-toggle" data-toggle="collapse" data-target="#contenu-rubrique-4"
                                aria-controls="contenu-rubrique-4" aria-expanded="false">
                                <div class="font-size-h2 text-center">
                                    <span class="dgfipicon dgfipicon-enregistrer icon-marge-droite"></span> Formulaires
                                </div>
                            </button>
                        </div>
                    </div>
                    <div class="collapse out" id="contenu-rubrique-4" role="region" tabindex="-1">
                        <div class="row row-marge-haute">
                            <div class="col-xs-12">
                                <ul class="liste">
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/bouton-radio.html">Bouton radio</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/champ-saisie-obligatoire.html">Champ de
                                            saisie obligatoire</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/champ-saisie-calendrier-obligatoire.html">Champ
                                            de saisie calendrier obligatoire </a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/groupe-champs.html">Groupe de champs</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/groupe-champs-obligatoires.html">Groupe de
                                            champs obligatoires</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/case-cocher.html">Case à cocher</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/champ-saisie-informations.html">Champ de
                                            saisie avec informations</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/liste-selection-obligatoire.html">Liste de
                                            sélection obligatoire</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/liste-selection-groupes.html">Liste de
                                            sélection avec des groupes</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/proposition.html">Formulaire proposition</a></li>
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/barre-progression.html">Barre de
                                            progression</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin rubrique -->
            <!-- Début rubrique -->
            <div class="col-xs-12 col-sm-6 col-md-4 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                <div class="contenu rubrique well-lg bg-professionnel">
                    <div class="row">
                        <div class="col-xs-12 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                            <button type="button" class="collapse-toggle" data-toggle="collapse" data-target="#contenu-rubrique-5"
                                aria-controls="contenu-rubrique-5" aria-expanded="false">
                                <div class="font-size-h2 text-center">
                                    <span class="dgfipicon dgfipicon-consulter-comptes icon-marge-droite"></span> Tableaux
                                </div>
                            </button>
                        </div>
                    </div>
                    <div class="collapse out" id="contenu-rubrique-5" role="region" tabindex="-1">
                        <div class="row row-marge-haute">
                            <div class="col-xs-12">
                                <ul class="liste">
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/tableaux-vue-ensemble.html">Tableaux - vue
                                            d'ensemble</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin rubrique -->
            <!-- Début rubrique -->
            <div class="col-xs-12 col-sm-6 col-md-4 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                <div class="contenu rubrique well-lg bg-accueil">
                    <div class="row">
                        <div class="col-xs-12 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                            <button type="button" class="collapse-toggle" data-toggle="collapse" data-target="#contenu-rubrique-6"
                                aria-controls="contenu-rubrique-6" aria-expanded="false">
                                <div class="font-size-h2 text-center">
                                    <span class="dgfipicon dgfipicon-sortie-page icon-marge-droite"></span> Modales
                                </div>
                            </button>
                        </div>
                    </div>
                    <div class="collapse out" id="contenu-rubrique-6" role="region" tabindex="-1">
                        <div class="row row-marge-haute">
                            <div class="col-xs-12">
                                <ul class="liste">
                                    <li><a href="/www/inea-outils/0.0.27/docs/cus/public/dialogue-attente.html">Dialogue attente</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin rubrique -->
            <!-- Début rubrique -->
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                <div class="contenu rubrique well-lg bg-accueil">
                    <div class="row">
                        <div class="col-xs-12 col-xs-avec-marge-haute col-xs-avec-marge-basse">
                            <button type="button" class="collapse-toggle" data-toggle="collapse" data-target="#contenu-rubrique-7"
                                aria-controls="contenu-rubrique-7" aria-expanded="false">
                                <div class="font-size-h2 text-center">
                                    <span class="dgfipicon dgfipicon-profil icon-marge-droite"></span> Pictogrammes
                                </div>
                            </button>
                        </div>
                    </div>
                    <div class="collapse out" id="contenu-rubrique-7" role="region" tabindex="-1">
                        <div class="row row-marge-haute">
                            <div class="col-xs-12">
                                <ul class="liste">
                                    <li>En attente &lt;- Accueil</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin rubrique -->
        </div>
        <!-- Fin container -->
    </div>
    </main>
    <!-- Fin espace de dialogue -->

    <div class="container-fluid">
        <div class="pieddepage row pieddepage-bg">
            <div class="col-xs-12">

                <!-- inea:pied-page:debut -->
                <h2 class="sr-only">Liens divers</h2>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 pieddepage">
                            <div class="row">
                                <div class="col-xs-12 col-md-3">
                                    <h3 class="titre-pieddepage">RGAA et accessibilité</h3>
                                    <ul class="list-unstyled">
                                        <li><a href="http://si1a.intranet.dgfip/developpement-et-recette/rgaa" target="_blank"
                                            rel="noopener"> Présentation <span class="dgfipicon dgfipicon-sortie-page infobulle"
                                                title="nouvelle fenêtre"></span>
                                        </a></li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                    <h3 class="titre-pieddepage">INEA</h3>
                                    <ul class="list-unstyled">
                                        <li><a href="/www/inea-outils/0.0.27/docs/inea/index.html">Documentation</a></li>
                                        <li><a href="http://venezia.appli.dgfip/www/inea/0.0.27/docs/" target="_blank" rel="noopener">
                                                Guide des composants <span class="dgfipicon dgfipicon-sortie-page infobulle"
                                                title="nouvelle fenêtre"></span>
                                        </a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/inea/changelog/index.html">Versions</a></li>
                                        <li><a href="http://venezia.appli.dgfip/scm/browser.php?group_id=870" target="_blank"
                                            rel="noopener"> Sources <span class="dgfipicon dgfipicon-sortie-page infobulle"
                                                title="nouvelle fenêtre"></span>
                                        </a></li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                    <h3 class="titre-pieddepage">Maquette</h3>
                                    <ul class="list-unstyled">
                                        <li><a href="/www/inea-outils/0.0.27/index.html">Voir la démo</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/cus/index.html">Explorer la démo</a></li>
                                        <li><a href="/www/inea-outils/0.0.27/docs/maquette/index.html">Projet Inea Maquette</a></li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                    <h3 class="titre-pieddepage">Développement Java</h3>
                                    <ul class="list-unstyled">
                                        <li><a href="/www/inea-outils/0.0.27/docs/jsp/index.html">Projet Inea JSP</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 pieddepage institutionnel">
                        <p class="text-center"></p>
                        <p class="text-center">
                            <a href="http://si1a.intranet.dgfip/" target="_blank" rel="noopener" title="SI1A (nouvelle fenêtre)">SI1A</a> <span
                                aria-hidden="true"> | </span> <a href="http://venezia.appli.dgfip/" target="_blank" rel="noopener"
                                title="Venezia (nouvelle fenêtre)">Venezia</a>
                        </p>
                        <p></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 pieddepage legal">
                        <footer role="contentinfo">
                            <p class="text-center"></p>
                            <footer class="text-center" role="contentinfo">
                                <span>© Direction générale des Finances publiques</span> <span aria-hidden="true"> - </span> <a
                                    href="mentionlegale">Mentions légales</a>
                            </footer>
                            <p></p>
                        </footer>
                    </div>
                </div>

                <!-- inea:pied-page:fin -->
            </div>
        </div>
    </div>
</body>
</html>