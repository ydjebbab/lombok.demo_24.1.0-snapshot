Vue.use(VueResource)
// important, sinon le cookie de session n'est pas transmis en "mode" CORS !
Vue.http.options.credentials = true;
// pour faciliter les tests en mode CORS
const origineBackend = 'http://localhost:8080';
//const origineBackend = '';
const app = new Vue({
	el:'#app',
	data:{
		dn:'',
		uid:'',
		motdepasse:'',
		nomentite:'',
		tokenCsrf:'',
		cle:'',
		entite:Object,
		entites:Object,
		personneAnnuaire:Object,
		ids:Object,
		autorisation:false,
		noResults:false,
		encours:false,
		selectedentite:''
	},
	methods: {
		// appel REST GET : ramène une entrée en base d'une entité et d'un id donnés 
		fGet:function() {		
			this.encours = true;
			this.$http.get(origineBackend + `/dmo/rest/entite/entite/${this.nomentite}/${this.cle}`)
			.then(response => {
				this.entite = response.data;
				this.encours = false;
				if (this.entite === "") { 
					alert("Pas de " + this.nomentite + " pour l'id " + this.cle);
				}
			}, error => {
				this.encours = false;
				this.noResults = false;
				alert("Erreur : " + error.bodyText);
			});		
		},
		// appel REST PUT : modifie une entrée en base pour une entité et un id donnés 
		fPut:function() {
			this.encours = true;
			this.$http.put(origineBackend + `/dmo/rest/entite/entite/${this.nomentite}/${this.cle}`, this.entite)
			.then(response => {
				this.entite = response.data;
				this.encours = false;
				alert(this.nomentite + " (id: " + this.entite.id + ") mis à jour.");
			}, error => {
				this.encours = false;
				alert("Erreur : " + error.bodyText);
			}) ;		
		},
		// appel REST POST : crée une entrée en base pour une entité donnée 
		fPost:function() {
			this.encours = true;
			this.$http.post(origineBackend + `/dmo/rest/entite/entite/${this.nomentite}`, this.entite)
			.then(response => {
				this.entite = response.data;
				this.encours = false;
				alert(this.nomentite + " (id: " + this.entite.id + ") créé.");
				this.fIds();
			}, error => {
				this.encours = false;
				alert("Erreur : " + error.bodyText);
			}) ;		
		},
		// appel REST DELETE : supprime une entrée en base d'une entité et avec un id donnés
		fDelete:function() {
			this.encours = true;
			this.$http.delete(origineBackend + `/dmo/rest/entite/entite/${this.nomentite}/${this.entite.id}`)
			.then(response => {
				this.entite = response.data;
				this.encours = false;
				alert(this.nomentite + " (id: " + this.cle + ") supprimé.");
				this.fIds();
			}, error => {
				this.encours = false;
				alert("Erreur : " + error.bodyText);
			});		
		},		
		// appel REST GET : ramène la liste des ids des entrées en base pour une entité donnée 
		fIds:function() {
			this.encours = true;
			this.$http.get(origineBackend + `/dmo/rest/entite/ids/${this.nomentite}`)
			.then(response => {
				this.ids = response.data;
				this.cle = '';
				this.entite = '';
				this.encours = false;
			}, error => {
				this.encours = false;
				alert("Erreur : " + error.bodyText);				
			});
		},
		fGetEntites: function() {
			this.encours = true;
			// appel REST GET : ramène la liste des entités JPA de l'application		
			this.$http.get(origineBackend + `/dmo/rest/entite/entites`)
			.then(response => {
				this.entites = response.data;
				this.encours = false;
			}, error => {
				this.encours = false;
				alert("Erreur : " + error.bodyText);
			});	
		},
		// récupère le jeton CSRF dans le cookie de réponse
		fGetTokenCsrf: function() {
			var patternMatch = document.cookie.match(new RegExp(`XSRF-TOKEN=([^;]+)`));
			if (patternMatch && patternMatch.length > 0) {
				return patternMatch[1];
			}
			return '';
		},
		// identification en mode "portail" : l'authentification a été gérée par une autre brique (le DAC)
		// on envoie donc le DN et le backend simule l'authentification et construit un objet avec les informations
		// récupérées dans un annuaire LDAP
		fIdentifiePortail:function() {
			this.encours = true;
			this.$http.get(origineBackend + `/dmo/j_appelportailbackend`, 
					{ headers: { Authorization: 'Basic ' + window.btoa(this.dn + ':' + 'ADMINISTRATEUR') }})
			.then(response => {
				this.tokenCsrf = this.fGetTokenCsrf();
				Vue.http.headers.common['X-XSRF-Token'] = this.tokenCsrf;
				this.autorisation = true;
				this.personneAnnuaire = response.data;
				this.fGetEntites();
			}, error => {
				this.autorisation = false;
				alert("Erreur : " + error.bodyText);				
			});
		},
		// identification en mode "appli" : passage par la couche sécurité configurée dans le projet  
		fIdentifieAppli:function() {
			this.encours = true;
			this.$http.post(origineBackend + `/dmo/j_spring_security_check_backend`,
				{ username: this.uid, password: this.motdepasse },
				{ emulateJSON: true}
			)
			.then(response => {
				this.tokenCsrf = this.fGetTokenCsrf();
				Vue.http.headers.common['X-XSRF-Token'] = this.tokenCsrf;
				this.autorisation = true;
				this.personneAnnuaire = response.data;
				this.fGetEntites();
			}, error => {
				this.autorisation = false;
				alert("Erreur : " + error.bodyText);				
			});
		},
		fClearEntite: function() {
			this.entite = '';			
		},
		isNumber: function(evt) {
			evt = (evt) ? evt : window.event;
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
				evt.preventDefault();
			} else {
				return true;
			}
		}
	},
	mounted() {
		// récupération du token CSRF / XSRF dans les cookies de réponse
		this.tokenCsrf = this.fGetTokenCsrf();
		// injection globale du token pour l'ensemble des requêtes émises
		Vue.http.headers.common['X-XSRF-Token'] = this.tokenCsrf;
	}
});