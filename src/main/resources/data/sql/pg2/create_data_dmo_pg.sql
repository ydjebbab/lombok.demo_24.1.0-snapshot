/* CF: 26/10/2019 
- Script d'ajout de donn�es manquantes dans la base POSTGRESQL lombokdb (IP : 10.153.90.54) 
- Ce script est � appeler manuellement. 
- Il devrait remplacer les donn�es du DUMP qui se trouve dans le fichier data/sql/pg/lombokinitdata.sql
*/

INSERT INTO zpaysiso_zpis (ZPIS_ID, CODEISO, nom)  VALUES ('FR', 'FR', 'FRANCE');
INSERT INTO zpaysiso_zpis (ZPIS_ID, CODEISO, nom)  VALUES ('DE', 'DE', 'ALLEMAGNE');
INSERT INTO zpaysiso_zpis (ZPIS_ID, CODEISO, nom)  VALUES ('AT', 'AT', 'AUTRICHE');
INSERT INTO zpaysiso_zpis (ZPIS_ID, CODEISO, nom)  VALUES ('BR', 'BR', 'BR�SIL');
INSERT INTO zpaysiso_zpis (ZPIS_ID, CODEISO, nom)  VALUES ('CI', 'CI', 'C�TE D''IVOIRE');
INSERT INTO zpaysiso_zpis (ZPIS_ID, CODEISO, nom)  VALUES ('MC', 'MC', 'MONACO');
INSERT INTO zpaysiso_zpis (ZPIS_ID, CODEISO, nom)  VALUES ('PT', 'PT', 'PORTUGAL');
INSERT INTO zpaysiso_zpis (ZPIS_ID, CODEISO, nom)  VALUES ('SZ', 'SZ', 'SWAZILAND');
INSERT INTO zpaysiso_zpis (ZPIS_ID, CODEISO, nom)  VALUES ('TN', 'TN', 'TUNISIE');
INSERT INTO zpaysiso_zpis (ZPIS_ID, CODEISO, nom)  VALUES ('UY', 'UY', 'URUGUAY');


INSERT INTO zcodepostal_zcpo (ZCPO_ID, version, code, ville) VALUES (4120540, 0, '93100', 'MONTREUIL');
INSERT INTO zcodepostal_zcpo (ZCPO_ID, version, code, ville) VALUES (4120541, 0, '93160', 'NOISY-LE-GRAND');
INSERT INTO zcodepostal_zcpo (ZCPO_ID, version, code, ville) VALUES (4120542, 0, '77186', 'NOISIEL');
INSERT INTO zcodepostal_zcpo (ZCPO_ID, version, code, ville) VALUES (4120543, 0, '33000', 'BORDEAUX');


INSERT INTO declaration_revenu (ID, version, numerofiscal, montantrevenu, tauxprelevement, reduction, creditImpot, doneffectue, contribaudiopublic, informations, cont_id_listedeclarationrevenu) 
VALUES (100, 0, 1234123456780, 1000, 10, 20, 10, 60, true, 'Rien � signaler', 20000);
INSERT INTO declaration_revenu (ID, version, numerofiscal, montantrevenu, tauxprelevement, reduction, creditImpot, doneffectue, contribaudiopublic, informations, cont_id_listedeclarationrevenu) 
VALUES (101, 0, 1234123456781, 1001, 11, 21, 11, 61, false, 'Rien � signaler', 20001);
INSERT INTO declaration_revenu (ID, version, numerofiscal, montantrevenu, tauxprelevement, reduction, creditImpot, doneffectue, contribaudiopublic, informations, cont_id_listedeclarationrevenu) 
VALUES (102, 0, 1234123456782, 1002, 12, 22, 12, 62, true, 'Rien � signaler', 20002);
INSERT INTO declaration_revenu (ID, version, numerofiscal, montantrevenu, tauxprelevement, reduction, creditImpot, doneffectue, contribaudiopublic, informations, cont_id_listedeclarationrevenu) 
VALUES (103, 0, 1234123456783, 1003, 13, 23, 13, 63, false, 'Rien � signaler', 20003);
INSERT INTO declaration_revenu (ID, version, numerofiscal, montantrevenu, tauxprelevement, reduction, creditImpot, doneffectue, contribaudiopublic, informations, cont_id_listedeclarationrevenu) 
VALUES (104, 0, 1234123456784, 1004, 14, 24, 14, 64, true, 'Rien � signaler', 20004);
INSERT INTO declaration_revenu (ID, version, numerofiscal, montantrevenu, tauxprelevement, reduction, creditImpot, doneffectue, contribaudiopublic, informations, cont_id_listedeclarationrevenu) 
VALUES (105, 0, 1234123456785, 1005, 15, 25, 15, 65, false, 'Rien � signaler', 20005);
-- D�claration de revenus de chouard-cp
INSERT INTO declaration_revenu (ID, version, numerofiscal, montantrevenu, tauxprelevement, reduction, creditImpot, doneffectue, contribaudiopublic, informations, cont_id_listedeclarationrevenu) 
VALUES (106, 0, 1234123456786, 1006, 16, 26, 16, 66, true, 'Rien � signaler', 20225);

INSERT INTO revenu_foncier (id, version, typelocation, montantrevenu, id_declarationrevenu) VALUES (10, 0, 'Meubl�', 2400, 106);
INSERT INTO revenu_foncier (id, version, typelocation, montantrevenu, id_declarationrevenu) VALUES (11, 0, 'Meubl�', 1100, 106);
INSERT INTO revenu_foncier (id, version, typelocation, montantrevenu, id_declarationrevenu) VALUES (12, 0, 'Vide', 600, 106);
INSERT INTO revenu_foncier (id, version, typelocation, montantrevenu, id_declarationrevenu) VALUES (13, 0, 'Meubl�', 550, 106);

INSERT INTO revenu_mobilier (id, version, montantpel, montantcel, montantpea, montantpee, montantassurancevie, montantlivreta, montantactions, id_declarationrevenu) VALUES (10, 0, 22, 33, 44, 55, 66, 77, 88, 106);
INSERT INTO revenu_mobilier (id, version, montantpel, montantcel, montantpea, montantpee, montantassurancevie, montantlivreta, montantactions, id_declarationrevenu) VALUES (11, 0, 23, 34, 45, 56, 67, 78, 89, 106);
INSERT INTO revenu_mobilier (id, version, montantpel, montantcel, montantpea, montantpee, montantassurancevie, montantlivreta, montantactions, id_declarationrevenu) VALUES (12, 0, 24, 35, 46, 57, 68, 79, 90, 106);
INSERT INTO revenu_mobilier (id, version, montantpel, montantcel, montantpea, montantpee, montantassurancevie, montantlivreta, montantactions, id_declarationrevenu) VALUES (13, 0, 25, 36, 47, 58, 69, 80, 91, 106);

INSERT INTO CENTRE_IMPOTS (ID, VERSION, COMMUNE, NOMBRE_AGENTS, OUVERT) VALUES (1, 0, 'Paris', 20, true);
INSERT INTO CENTRE_IMPOTS (ID, VERSION, COMMUNE, NOMBRE_AGENTS, OUVERT) VALUES (2, 0, 'Noisy', 10, true);
INSERT INTO CENTRE_IMPOTS (ID, VERSION, COMMUNE, NOMBRE_AGENTS, OUVERT) VALUES (3, 0, 'Fontenay', 30, false);
INSERT INTO CENTRE_IMPOTS (ID, VERSION, COMMUNE, NOMBRE_AGENTS, OUVERT) VALUES (4, 0, 'Nanterre', 5, true);
INSERT INTO CENTRE_IMPOTS (ID, VERSION, COMMUNE, NOMBRE_AGENTS, OUVERT) VALUES (5, 0, 'Puteaux', 8, true);
