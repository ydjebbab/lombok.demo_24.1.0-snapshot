/* CF: 20/12/2019 
- Script de cr�ation de la base POSTGRESQL lombokdb (IP : 10.153.90.54) 
- Ce script est � appeler manuellement.
- Il faut le compl�ter avec tous les autres objets de la base 
*/

CREATE TABLE declaration_revenu (
	id bigint NOT NULL,
	version integer,
	numerofiscal bigint,
	montantrevenu bigint,
	tauxprelevement bigint,
	reduction bigint,
	creditImpot bigint,
	doneffectue bigint,
	contribaudiopublic boolean,
	informations character varying not null,
	cont_id_listedeclarationrevenu bigint,
	CONSTRAINT pk_id PRIMARY KEY (id) 
);

CREATE SEQUENCE SEQ_DECLARATION_REVENU
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 10000
  CACHE 1;
ALTER TABLE DECLARATION_REVENU OWNER TO dmo;
ALTER SEQUENCE SEQ_DECLARATION_REVENU OWNER TO dmo;
  
CREATE TABLE revenu_foncier (
id bigint not null,
version integer,
montantRevenu bigint,
typeLocation character varying not null,
id_declarationrevenu bigint NOT NULL,
CONSTRAINT pk_revfonc_id PRIMARY KEY (id)
);

CREATE TABLE revenu_mobilier (
id bigint not null,
version integer,
montantPEL bigint,
montantCEL bigint,
montantPEA bigint,
montantPEE bigint,
montantAssuranceVie bigint,
montantLivretA bigint,
montantActions bigint,
id_declarationrevenu bigint,
CONSTRAINT pk_revmob_id PRIMARY KEY (id)
);

ALTER TABLE revenu_mobilier 
ADD CONSTRAINT fk_id_declarationrevenu_rm
FOREIGN KEY (id_declarationrevenu) 
REFERENCES declaration_revenu;

ALTER TABLE revenu_foncier 
ADD CONSTRAINT fk_id_declarationrevenu_rf
FOREIGN KEY (id_declarationrevenu) 
REFERENCES declaration_revenu;

CREATE SEQUENCE SEQ_REVENU_FONCIER
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 10000
  CACHE 1;
ALTER SEQUENCE SEQ_REVENU_FONCIER OWNER TO dmo;

ALTER TABLE revenu_mobilier OWNER TO dmo;
ALTER TABLE revenu_foncier OWNER TO dmo;
 
CREATE SEQUENCE SEQ_REVENU_MOBILIER
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 10000
  CACHE 1;
ALTER SEQUENCE SEQ_REVENU_MOBILIER OWNER TO dmo;
  
CREATE TABLE CENTRE_IMPOTS (
    id bigint NOT NULL,
    version integer,
    commune character varying not null,
    nombre_Agents bigint,
    ouvert boolean,
    CONSTRAINT pk_id PRIMARY KEY (id) 
);

CREATE SEQUENCE SEQ_CENTRE_IMPOTS
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 10000
  CACHE 1;
ALTER SEQUENCE SEQ_CENTRE_IMPOTS OWNER TO dmo;