 alter table OPERATIONJOURNAL_OPER 
		add  apurementPossible  number(1,0);

alter table OPERATIONJOURNAL_OPER 
		add  codeOperation  varchar2(255 char);

alter table OPERATIONJOURNAL_OPER 
		add  valeurIdentifiantMetier1  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  valeurIdentifiantMetier2  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  valeurIdentifiantMetier3  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  valeurIdentifiantMetier4  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  valeurIdentifiantMetier5  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  libelleIdentifiantMetier1  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  libelleIdentifiantMetier2  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  libelleIdentifiantMetier3  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  libelleIdentifiantMetier4  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  libelleIdentifiantMetier5  varchar2(255 char) ;
 

create index IN_OPER_4 on OPERATIONJOURNAL_OPER (valeurIdentifiantMetier1, valeurIdentifiantMetier2,valeurIdentifiantMetier3,valeurIdentifiantMetier4,valeurIdentifiantMetier5)
                TABLESPACE @appli@_INDX ;   

alter table PARAMETREOPERATION_PAOP 
		add  valeurPrecedente  varchar2(255 char) ;

  
