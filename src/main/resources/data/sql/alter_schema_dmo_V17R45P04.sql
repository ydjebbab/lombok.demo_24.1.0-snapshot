alter table ZSTRUCTURECP_ZSTR 
		add  EmailREC varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  AppliRec varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  LecteurOpt varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  CodeIndicateur varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  LibIndicateur varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add   BICCEP  varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  domiciliationCEP varchar2(255 char) ;



 

 create table  ZLIENSDEP_ZLID (
        ZLID_ID number(19,0) not null,
        ZLIENSDEP_DIS  varchar2(255 char) not null,
        VERSION number(10,0) not null,
        ZLTI_ID_LETYPELIEN number(19,0) not null,
        ZSTR_ID_LESLIENSDEP number(19,0),
     CONSTRAINT PK_ZLID  PRIMARY KEY(ZLID_ID)
            USING INDEX
            TABLESPACE @appli@_INDX                
    )
       TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80    ;


  create table ZTYPELIEN_ZTLI (
        ZTLI_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codetypelien varchar2(255 char) not null,
        libelletypelien varchar2(255 char),
        CONSTRAINT PK_ZTLI primary key (ZTLI_ID)
             USING INDEX
                  TABLESPACE @appli@_INDX ,
           CONSTRAINT UK_ZTLI_1  unique (codetypelien)
			 USING INDEX
            TABLESPACE @appli@_INDX    
    );

  create table ZLID_QSTRUCTLIES (
        ZLID_ID number(19,0) not null,
        STRUCTURELIEEPARID varchar2(255 char)
    )
 TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80 ;

 

 

  
 
 alter table ZLIENSDEP_ZLID 
        add constraint FK_ZLID_ZTLI_1 
        foreign key (ZLTI_ID_LETYPELIEN) 
        references ZTYPELIEN_ZTLI deferrable 
         initially deferred;

    alter table ZLIENSDEP_ZLID
        add constraint FK_ZLID_ZSTR_1 
        foreign key (ZSTR_ID_LESLIENSDEP) 
        references  ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

  



alter table ZLID_QSTRUCTLIES 
        add constraint FK_ZLID_QSTRUCTLIES_ZLID_1 
        foreign key (ZLID_ID) 
      references ZLIENSDEP_ZLID deferrable 
         initially deferred;

   
  

 create index IN_ZLID_1 on  ZLIENSDEP_ZLID (ZLIENSDEP_DIS)
			TABLESPACE @appli@_INDX;
 


create index IN_ZLID_ZSTR_1 on ZLIENSDEP_ZLID (ZSTR_ID_LESLIENSDEP)
                TABLESPACE @appli@_INDX;

create index IN_FK_ZLID_QSTRUCTLIES_ZLID_1 on ZLID_QSTRUCTLIES (ZLID_ID)
                TABLESPACE @appli@_INDX;

  create sequence ZLID_ID_SEQUENCE;

  create sequence ZTLI_ID_SEQUENCE;



----------------------------------------
