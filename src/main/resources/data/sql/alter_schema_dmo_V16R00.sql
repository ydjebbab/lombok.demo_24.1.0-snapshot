---
--- Ajout du schema du composant fichier de rejet
--- VERSION : V16R00
--- 

    create table ZFICHIERDEREJET_ZFDR (
        ZFDR_ID number(19,0) not null,
        VERSION number(10,0) not null,
        IDENTIFIANT varchar2(255 char) not null,
        DTCREATION timestamp not null,
        DTRETRAIT timestamp,
        DTVALIDRETRAIT timestamp,
        TRAIIMMARTPARINS number(1,0) not null,
        VERCORRARTPARINS number(1,0) not null,
        MOTIFDUREJET varchar2(255 char),
        NOMAFFICDEREJ varchar2(255 char),
        NOMAFFTYPDEFIC varchar2(255 char),
        NOMDELACLADEPRODUREJ varchar2(255 char),
        NOMDELACLADERETDUREJ varchar2(255 char),
        NOMINTFICDEREJ varchar2(255 char),
        NOMINTTYPDEFICREJ varchar2(255 char),
        PERDECONDANHISAPRRET number(19,0),
        PERDEVALDUFICDEREJ number(19,0),
        UIDUTIAYAVALIPOURET number(19,0),
        ENCODAGEDUTEXTE integer,
        ETATFICHIER integer,
        CONSTRAINT PK_ZFICHIERDEREJET_ZFDR PRIMARY KEY(ZFDR_ID)
        USING INDEX TABLESPACE dmo_INDX,
        CONSTRAINT UK_ZFICHIERDEREJET_ZFDR UNIQUE(IDENTIFIANT)
        USING INDEX TABLESPACE dmo_INDX
    ) TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80;

    create table ZFILTREFDR_ZFIF (
        ZFIF_ID number(19,0) not null,
        VERSION number(10,0) not null,
        vallow number(1,0),
        vdeny number(1,0),
        nomDuFiltre varchar2(255 char),
        ZHAF_ID_LISTEFILTRES number(19,0),
        CONSTRAINT PK_ZFILTREFDR_ZFIF primary key (ZFIF_ID)
        USING INDEX TABLESPACE dmo_INDX
    ) TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80;

    create table ZHABILITATIONSFDR_ZHAF (
        ZHAF_ID number(19,0) not null,
        VERSION number(10,0) not null,
        dateDebut timestamp,
        dateFin timestamp,
        libelleCourtAppli varchar2(255 char),
        nomProfil varchar2(255 char) not null,
        ZFDR_ID_LISTEHABILIT number(19,0),
        CONSTRAINT PK_ZHABILITATIONSFDR_ZHAF primary key (ZHAF_ID)
        USING INDEX TABLESPACE dmo_INDX
    ) TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80;

    create table ZLISTEARTICLEFDR_ZLFR (
        ZLFR_ID_LISTEARTICLE number(19,0) not null,
        contenuArticle clob,
        estASupprime number(1,0),
        estCorrige number(1,0),
        motifDuRejet varchar2(255 char),
        numeroArticle number(19,0) not null,
        ZLFR_ID_LISTEARTICLE_INDEX number(10,0),
        CONSTRAINT PK_ZLISTEARTICLEFDR_ZLFR primary key (ZLFR_ID_LISTEARTICLE, ZLFR_ID_LISTEARTICLE_INDEX)
        USING INDEX TABLESPACE dmo_INDX
    ) TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80;

    create table ZVALEURFILTREFDR_ZVFF (
        ZVFF_ID number(19,0) not null,
        VERSION number(10,0) not null,
        valeur varchar2(255 char),
        ZFIF_ID_LESVALEURSFILTRES number(19,0),
        CONSTRAINT PK_ZVALEURFILTREFDR_ZVFF primary key (ZVFF_ID)
        USING INDEX TABLESPACE dmo_INDX
    ) TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80;

    create index IN_ZFIF_ZHAF_1 on ZFILTREFDR_ZFIF (ZHAF_ID_LISTEFILTRES);

    alter table ZFILTREFDR_ZFIF 
        add constraint FK_ZFIF_ZHAF_1 
        foreign key (ZHAF_ID_LISTEFILTRES) 
        references ZHABILITATIONSFDR_ZHAF;

    create index IN_ZLHA_ZFDR_1 on ZHABILITATIONSFDR_ZHAF (ZFDR_ID_LISTEHABILIT)
                    TABLESPACE dmo_INDX;

    alter table ZHABILITATIONSFDR_ZHAF 
        add constraint FK_ZLHA_ZFDR_1 
        foreign key (ZFDR_ID_LISTEHABILIT) 
        references ZFICHIERDEREJET_ZFDR;

    alter table ZLISTEARTICLEFDR_ZLFR 
        add constraint FK_ZLFR_ID_LISTEARTICLE_1 
        foreign key (ZLFR_ID_LISTEARTICLE) 
        references ZFICHIERDEREJET_ZFDR;

    create index IN_ZVFF_ZFIF_1 on ZVALEURFILTREFDR_ZVFF (ZFIF_ID_LESVALEURSFILTRES)
                     TABLESPACE dmo_INDX;

    alter table ZVALEURFILTREFDR_ZVFF 
        add constraint FK_ZVFF_ZFIF_1 
        foreign key (ZFIF_ID_LESVALEURSFILTRES) 
        references ZFILTREFDR_ZFIF;

    create sequence ZFDR_ID_SEQUENCE;

    create sequence ZFIF_ID_SEQUENCE;

    create sequence ZHAF_ID_SEQUENCE;

    create sequence ZVFF_ID_SEQUENCE;
