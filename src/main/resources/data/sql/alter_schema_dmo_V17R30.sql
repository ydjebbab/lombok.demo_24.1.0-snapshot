create table ZPROPIMMEUBLE_ZPIM (
        ZPIM_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codePropImmeuble varchar2(255 char) not null,
        libellePropImmeuble varchar2(255 char),
		 CONSTRAINT PK_ZPIM PRIMARY KEY(ZPIM_ID)
            USING INDEX
            TABLESPACE dmo_INDX,
        CONSTRAINT UK_ZPIM_1 UNIQUE(codePropImmeuble)
            USING INDEX
            TABLESPACE dmo_INDX        
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;
        

	create table ZORGAACCUEIL_ZOAC (
        ZOAC_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeOrgAccueil varchar2(255 char) not null,
        libOrgAccueil varchar2(255 char),
        CONSTRAINT PK_ZOAC PRIMARY KEY(ZOAC_ID)
            USING INDEX
            TABLESPACE dmo_INDX,
        CONSTRAINT UK_ZOAC_1 UNIQUE(codeOrgAccueil)
            USING INDEX
            TABLESPACE dmo_INDX        
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;

	create sequence ZPIM_ID_SEQUENCE;

    create sequence ZOAC_ID_SEQUENCE;

    alter table ZCOORDBANCAIRECP_ZCOB 
		add  ibANAutomatise varchar2(255 char) ;

	alter table ZSTRUCTURECP_ZSTR 
		add  ZPIM_ID_LEPROPIMMEUBLE number(19,0);

	alter table ZSTRUCTURECP_ZSTR 
		add  ZOAC_ID_LEORGAACCUEIL number(19,0);

    create index IN_ZCOB_2 on ZCOORDBANCAIRECP_ZCOB (codeIC)
                TABLESPACE dmo_INDX ;

	create table ADRESSETEST_ADRT (
        ADRESSE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        voie varchar2(255 char),
        ville varchar2(255 char),
        CONSTRAINT PK_ADRT PRIMARY KEY(ADRESSE_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;

	create table PERSONNETEST_PRST (
        PERSONNE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        prenom varchar2(255 char),
        nom varchar2(255 char),
        CONSTRAINT PK_PRST PRIMARY KEY(PERSONNE_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;

	create sequence ADRESSE_ID_SEQUENCE;

    create sequence PERSONNE_ID_SEQUENCE;

	create table PERSONNEADRESSETEST_PADT (
        PERSONNE_ID number(19,0) not null,
        ADRESSE_ID number(19,0) not null,
        CONSTRAINT PK_PADT PRIMARY KEY(PERSONNE_ID, ADRESSE_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;

    alter table PERSONNEADRESSETEST_PADT 
        add constraint FK_PADT_ADRT
        foreign key (ADRESSE_ID) 
        references ADRESSETEST_ADRT(ADRESSE_ID);

    alter table PERSONNEADRESSETEST_PADT 
        add constraint FK_PADT_PRST 
        foreign key (PERSONNE_ID) 
        references PERSONNETEST_PRST(PERSONNE_ID);

	create table ORIGINETEST_ORIT (
        ORIGINE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        libelle varchar2(255 char),
        CONSTRAINT PK_ORIT PRIMARY KEY(ORIGINE_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;
    
	create sequence ORIGINE_ID_SEQUENCE;

	create table VILLETEST_VILT (
        VILLE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        libelle varchar2(255 char),
        CONSTRAINT PK_VILT PRIMARY KEY(VILLE_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;
    
	create sequence VILLE_ID_SEQUENCE;

	create table HABITANTTEST_HABT (
        HABITANT_ID number(19,0) not null,
        VERSION number(10,0) not null,
        nom varchar2(255 char),
        prenom varchar2(255 char),
        ORIGINE_ID number(19,0) not null,
        VILLE_ID number(19,0) not null,
        CONSTRAINT PK_HABT PRIMARY KEY(HABITANT_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;
    
	create sequence HABITANT_ID_SEQUENCE;

    alter table HABITANTTEST_HABT 
        add constraint FK_HABT_ORIT
        foreign key (ORIGINE_ID) 
        references ORIGINETEST_ORIT(ORIGINE_ID);

    alter table HABITANTTEST_HABT 
        add constraint FK_HABT_VILT
        foreign key (VILLE_ID) 
        references VILLETEST_VILT(VILLE_ID);


        
