  drop table ZDOCUMENTATLAS_ZDOA cascade constraints;
  
  drop table ZFICHIERJOINT_ZFIJ cascade constraints;

  drop table ZCONTENUFICHIER_ZCOF cascade constraints;
    
  drop table ZTYPEDOCUMENTATLAS_ZTDA cascade constraints;
  
  drop sequence ZDOA_ID_SEQUENCE;

  drop sequence ZFIJ_ID_SEQUENCE;

  drop sequence ZCOF_ID_SEQUENCE;
  
  drop sequence ZTDA_ID_SEQUENCE;