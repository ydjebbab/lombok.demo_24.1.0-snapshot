    create table TJ_ZECL_ZSTR_TJ (
        ZECL_ID number(19,0) not null,
        ZSTR_ID number(19,0) not null,
        CONSTRAINT PK_TJ_ZECL_ZSTR_TJ PRIMARY KEY(ZECL_ID, ZSTR_ID)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

    create table TJ_ZSTR_ZATT_TJ (
        ZSTR_ID number(19,0) not null,
        ZATT_ID number(19,0) not null,
        CONSTRAINT PK_TJ_ZSTR_ZATT_TJ PRIMARY KEY(ZSTR_ID, ZATT_ID)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
    TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

    create table TJ_ZSTR_ZFON_TJ (
        ZSTR_ID number(19,0) not null,
        ZFON_ID number(19,0) not null,
        CONSTRAINT PK_TJ_ZSTR_ZFON_TJ PRIMARY KEY(ZSTR_ID, ZFON_ID)
            USING INDEX
            TABLESPACE @appli@_INDX                
    )
    TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

    create table ZADRESSE_ZADR (
        ZADR_ID number(19,0) not null,
        version number(10,0) not null,
        codePostalEtCedex varchar2(255 char),
        ligne2ServiceAppartEtgEsc varchar2(255 char),
        ligne3BatimentImmeubleResid varchar2(255 char),
        ligne4NumeroLibelleVoie varchar2(255 char),
        ligne5LieuDitMentionSpeciale varchar2(255 char),
        pays varchar2(255 char),
        ville varchar2(255 char),
        CONSTRAINT PK_ZADR PRIMARY KEY(ZADR_ID)
            USING INDEX
            TABLESPACE @appli@_INDX                        
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

    create table ZATTRIBUTIONTGCP_ZATT (
        ZATT_ID number(19,0) not null,
        VERSION number(10,0) not null,
        libelleattributionTG varchar2(255 char) not null,
        codeattributionTG varchar2(255 char),
        CONSTRAINT PK_ZATT PRIMARY KEY(ZATT_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,                       
        CONSTRAINT UK_ZATT_1 UNIQUE(libelleattributionTG)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

    create table ZCATEGORIESTRUCTURECP_ZCAS (
        ZCAS_ID number(19,0) not null,
        VERSION number(10,0) not null,
        CODECATEGORIE varchar2(255 char) not null,
        libellecategorie varchar2(255 char),
        CONSTRAINT PK_ZCAS PRIMARY KEY(ZCAS_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,        
        CONSTRAINT UK_ZCAS_1 UNIQUE(CODECATEGORIE)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80    
    ;

    create table ZCODEPOSTAL_ZCPO (
        ZCPO_ID number(19,0) not null,
        version number(10,0) not null,
        code varchar2(255 char) not null,
        ville varchar2(255 char) not null,
        CONSTRAINT PK_ZCPO PRIMARY KEY(ZCPO_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,                
        CONSTRAINT UK_ZCPO_1 UNIQUE(code, ville)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80        
    ;

    create table ZCOMMUNE_ZCOM (
        ZCOM_ID number(19,0) not null,
        codecommuneINSEE varchar2(255 char),
        libellecommune varchar2(255 char),
        ZDEP_ID_LESCOMMUNES number(19,0),
        CONSTRAINT PK_ZCOM PRIMARY KEY(ZCOM_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,        
        CONSTRAINT UK_ZCOM_1 UNIQUE(codecommuneINSEE)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80        
    ;

    create table ZCOORDBANCAIRECP_ZCOB (
        ZCOB_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeFlux varchar2(255 char),
        codeIC varchar2(255 char),
        biC varchar2(255 char),
        ibAN varchar2(255 char),
        ZRIB_ID_LERIBCLASSIQUE number(19,0) unique,
        ZRIB_ID_LERIBAUTOMATISE number(19,0) unique,
        ZRCB_ID_LEREROUTAGE number(19,0) unique,
        ZSTR_ID_LESCOORDBANCAIRESCP number(19,0),
        CONSTRAINT PK_ZCOB PRIMARY KEY(ZCOB_ID)
            USING INDEX
            TABLESPACE @appli@_INDX                
    )
       TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80        
    ;

    create table ZDEPARTEMENT_ZDEP (
        ZDEP_ID number(19,0) not null,
        codedepartementINSEE varchar2(255 char),
        libelledepartement varchar2(255 char),
        ZREG_ID_LESDEPARTEMENTS number(19,0),
        CONSTRAINT PK_ZDEP PRIMARY KEY(ZDEP_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZDEP_1 UNIQUE(codedepartementINSEE)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
       TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80        
    ;

    create table ZDOMAINECP_ZDOM (
        ZDOM_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codedomaine varchar2(255 char) not null,
        libelledomaine varchar2(255 char),
        CONSTRAINT PK_ZDOM PRIMARY KEY(ZDOM_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZDOM_1 UNIQUE(codedomaine)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
       TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80     
    ;

    create table ZECLATEMENTCP_ZECL (
        ZECL_ID number(19,0) not null,
        ZECLATEMENTCP_DIS varchar2(255 char) not null,
        VERSION number(10,0) not null,
        dateDerniereMaj timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ZSTR_ID_LESECLSTRUBENEF number(19,0),
        CONSTRAINT PK_ZECL PRIMARY KEY(ZECL_ID)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
       TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80         
    ;

    create table ZFONCTIONCP_ZFON (
        ZFON_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codefonction varchar2(255 char) not null,
        libellefonction varchar2(255 char),
        CONSTRAINT PK_ZFON PRIMARY KEY(ZFON_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZFON_1 UNIQUE(codefonction)
            USING INDEX
            TABLESPACE @appli@_INDX            
    )
       TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80             
    ;

    create table ZGUICHETBANC_ZGUI (
        ZGUI_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeEtablissement varchar2(255 char),
        codeGuichet varchar2(255 char),
        nomGuichet varchar2(255 char),
        adresseGuichet varchar2(255 char),
        codePostal varchar2(255 char),
        villeCodePostal varchar2(255 char),
        denominationComplete varchar2(255 char),
        denominationAbregee varchar2(255 char),
        libelleAbrevDeDomiciliation varchar2(255 char),
        codeReglementDesCredits varchar2(255 char),
        moisDeModification varchar2(255 char),
        anneeDeModification varchar2(255 char),
        codeEnregistrement varchar2(255 char),
        nouveauCodeEtablissement varchar2(255 char),
        nouveauCodeGuichet varchar2(255 char),
        estEnVoieDePeremption number(1,0),
        estPerime number(1,0),
        dateDerniereMiseAJour timestamp,
        CONSTRAINT PK_ZGUI PRIMARY KEY(ZGUI_ID)
            USING INDEX
            TABLESPACE @appli@_INDX 
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80             
    ;

    create table ZMODEGESTIONCP_ZMGE (
        ZMGE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codemodegestion varchar2(255 char) not null,
        libellemodegestion varchar2(255 char),
        CONSTRAINT PK_ZMGE PRIMARY KEY(ZMGE_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZMGE_1 UNIQUE(codemodegestion)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80     
    ;

    create table ZPAYSINSEE_ZPIN (
        ZPIN_ID number(19,0) not null,
        codePaysInsee varchar2(255 char),
        nom varchar2(255 char),
        CONSTRAINT PK_ZPIN PRIMARY KEY(ZPIN_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZPIN_1 UNIQUE(codePaysInsee)
            USING INDEX
            TABLESPACE @appli@_INDX
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80         
    ;

    create table ZPAYSISO_ZPIS (
        ZPIS_ID varchar2(255 char) not null,
        CODEISO varchar2(255 char),
        nom varchar2(255 char),
        CONSTRAINT PK_ZPIS PRIMARY KEY(ZPIS_ID)
            USING INDEX
            TABLESPACE @appli@_INDX         
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80         
    ;

    create table ZRATTACHEMENTCP_ZRAT (
        ZRAT_ID number(19,0) not null,
        ZRATTACHEMENTCP_DIS varchar2(255 char) not null,
        VERSION number(10,0) not null,
        dateDerniereMaj timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ZSTR_ID_LESRATSTRUABSORBEES number(19,0),
        CONSTRAINT PK_ZRAT PRIMARY KEY(ZRAT_ID)
            USING INDEX
            TABLESPACE @appli@_INDX                 
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80             
    ;

    create table ZREGION_ZREG (
        ZREG_ID number(19,0) not null,
        coderegionINSEE varchar2(255 char),
        libelleregion varchar2(255 char),
        CONSTRAINT PK_ZREG PRIMARY KEY(ZREG_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZREG_1 UNIQUE(coderegionINSEE)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80     
    ;

    create table ZREROUTAGECOORDBANCAIRECP_ZRCB (
        ZRCB_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeIC varchar2(255 char),
        dateDebutReroutage timestamp,
        dateFinReroutage timestamp,
        identifiantBeneficiaire varchar2(255 char),
        CONSTRAINT PK_ZRCB PRIMARY KEY(ZRCB_ID)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80         
    ;

    create table ZRIB_RIB (
        id number(19,0) not null,
        version number(10,0) not null,
        codeBanque varchar2(255 char) not null,
        codeGuichet varchar2(255 char) not null,
        numeroCompte varchar2(255 char) not null,
        cleRib varchar2(255 char),
        estAutomatise number(1,0),
        CONSTRAINT PK_ZRIB PRIMARY KEY(id)
            USING INDEX
            TABLESPACE @appli@_INDX         
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80             
    ;

    create table ZSTRUCTURECP_ZSTR (
        ZSTR_ID number(19,0) not null,
        ZSTRUCTURECP_DIS varchar2(255 char) not null,
        VERSION number(10,0) not null,
        IDENTITENOMINOE varchar2(255 char) not null,
        dateDerniereMaj timestamp,
        codique varchar2(255 char),
        codeAnnexe varchar2(255 char),
        denominationAbregee varchar2(255 char),
        denominationStandard varchar2(255 char),
        codeLiaison varchar2(255 char),
        libelleLong varchar2(255 char),
        libelleStandard varchar2(255 char),
        libelleAbrege varchar2(255 char),
        siret varchar2(255 char),
        etat varchar2(255 char),
        adresseMelGenerique varchar2(255 char),
        logementFonction varchar2(255 char),
        ZTSC_ID_LETYPESTRUCTURECP number(19,0),
        ZTSD_ID_LETYPESERVICEDIRECTION number(19,0),
        ZCAS_ID_LACATEGORIESTRUCTURECP number(19,0),
        ZPIN_ID_LEPAYS number(19,0),
        adresseAbregee varchar2(255 char),
        ZADR_ID_LADRESSEGEOGRAPHIQUE number(19,0) unique,
        ZADR_ID_LADRESSEPOSTALE number(19,0) unique,
        telStandard varchar2(255 char),
        telPoste1 varchar2(255 char),
        telPoste2 varchar2(255 char),
        telecopie varchar2(255 char),
        horairesAbg varchar2(255 char),
        horairesObsCompl varchar2(255 char),
        remunerationCodeIR varchar2(255 char),
        remunerationEchelleLettre varchar2(255 char),
        codeEtablissmentCEP varchar2(255 char),
        codeGuichetCEP varchar2(255 char),
        codeDICGL varchar2(255 char),
        diCGL varchar2(255 char),
        tenueComptaDateDebut timestamp,
        tenueComptaDateFin timestamp,
        ZSTR_ID_LACENTRALCOMPTABLE number(19,0),
        ZSTR_ID_LEMODEGESTION number(19,0),
        dateCreation timestamp,
        dateSuppression timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ancienCodique varchar2(255 char),
        ancienCodeAnnexe varchar2(255 char),
        ZRAT_ID_LESSTRUABSORBEES number(19,0),
        ZCOM_ID_LACOMMUNE number(19,0),
        codeCategTG varchar2(255 char),
        libCategTG varchar2(255 char),
        codeCategRF varchar2(255 char),
        libCategRF varchar2(255 char),
        codeGrRemEtger varchar2(255 char),
        libGrRemEtger varchar2(255 char),
        CONSTRAINT PK_ZSTR PRIMARY KEY(ZSTR_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZSTR_1 UNIQUE(IDENTITENOMINOE)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80           
    ;

    create index IN_ZSTR_1 on ZSTRUCTURECP_ZSTR (ZSTRUCTURECP_DIS)
                TABLESPACE @appli@_INDX;

    create index IN_ZSTR_2 on ZSTRUCTURECP_ZSTR (CODIQUE)
                TABLESPACE @appli@_INDX;
                
    create index IN_ZSTR_3 on ZSTRUCTURECP_ZSTR (ZCAS_ID_LACATEGORIESTRUCTURECP)
                TABLESPACE @appli@_INDX;                

    create index IN_ZSTR_4 on ZSTRUCTURECP_ZSTR (ZCOM_ID_LACOMMUNE)
                TABLESPACE @appli@_INDX;   

    create table ZSTR_LESHORAIRESCP (
        ZSTR_ID_LESHORAIRESCP number(19,0) not null,
        joursemaine varchar2(255 char),
        matindebut varchar2(255 char),
        matinfin varchar2(255 char),
        apremdebut varchar2(255 char),
        apremfin varchar2(255 char),
        ZSTR_ID_LESHORAIRESCP_INDEX number(10,0),
        CONSTRAINT PK_ZSTR_LESHORAIRESCP PRIMARY KEY(ZSTR_ID_LESHORAIRESCP, ZSTR_ID_LESHORAIRESCP_INDEX)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80     
    ;

    create table ZSTR_LESPOLESACTIV (
        ZSTR_ID_LESPOLESACTIV number(19,0),
        melpoleactivite varchar2(255 char),
        ZTYP_ID_LETYPEPOLEACTIV number(19,0),
        ZSTR_ID_LESPOLESACTIV_INDEX number(10,0),
        CONSTRAINT PK_ZSTR_LESPOLESACTIV PRIMARY KEY(ZSTR_ID_LESPOLESACTIV, ZSTR_ID_LESPOLESACTIV_INDEX)
            USING INDEX
            TABLESPACE @appli@_INDX                
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80         
    ;

    create table ZSTR_LESSERVICESCP (
        ZSTR_ID_LESSERVICESCP number(19,0),
        telService varchar2(255 char),
        ZTYS_ID_LETYPESERVICECP number(19,0),
        ZSTR_ID_LESSERVICESCP_INDEX number(10,0),
        CONSTRAINT PK_ZSTR_LESSERVICESCP PRIMARY KEY(ZSTR_ID_LESSERVICESCP, ZSTR_ID_LESSERVICESCP_INDEX)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80             
    ;

    create table ZTYPEPOLEACTIVITECP_ZTYP (
        ZTYP_ID number(19,0) not null,
        VERSION number(10,0) not null,
        libpoleactivite varchar2(255 char) not null,
        codepoleactivite varchar2(255 char),
        CONSTRAINT PK_ZTYP PRIMARY KEY(ZTYP_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZTYP_1 UNIQUE(libpoleactivite)
            USING INDEX
            TABLESPACE @appli@_INDX                
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80                 
    ;

    create table ZTYPESERVICECP_ZTYS (
        ZTYS_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeservice varchar2(255 char) not null,
        libelleservice varchar2(255 char),
        ZTYD_ID_UNDOMAINECP number(19,0),
        CONSTRAINT PK_ZTYS PRIMARY KEY(ZTYS_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZTYS_1 UNIQUE(codeservice)
            USING INDEX
            TABLESPACE @appli@_INDX         
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80                     
    ;

    create table ZTYPESERVICEDIRECTION_ZTSD (
        ZTSD_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeTypeServiceDirection varchar2(255 char) not null,
        libelleTypeServiceDirection varchar2(255 char),
        CONSTRAINT PK_ZTSD PRIMARY KEY(ZTSD_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZTSD_1 UNIQUE(codeTypeServiceDirection)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80                         
    ;

    create table ZTYPESTRUCTURECP_ZTSC (
        ZTSC_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeTypeStructure varchar2(255 char) not null,
        libelleTypeStructure varchar2(255 char),
        CONSTRAINT PK_ZTSC PRIMARY KEY(ZTSC_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZTSC_1 UNIQUE(codeTypeStructure)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80                             
    ;


    alter table TJ_ZECL_ZSTR_TJ 
        add constraint FK_TJ_ZECL_ZSTR_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table TJ_ZECL_ZSTR_TJ 
        add constraint FK_TJ_ZECL_ZSTR_TJ_ZECL_1 
        foreign key (ZECL_ID) 
        references ZECLATEMENTCP_ZECL deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZATT_TJ 
        add constraint FK_TJ_ZSTR_ZATT_TJ_ZATT_1 
        foreign key (ZATT_ID) 
        references ZATTRIBUTIONTGCP_ZATT deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZATT_TJ 
        add constraint FK_TJ_ZSTR_ZATT_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZFON_TJ 
        add constraint FK_TJ_ZSTR_ZFON_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZFON_TJ 
        add constraint FK_TJ_ZSTR_ZFON_TJ_ZFON_1 
        foreign key (ZFON_ID) 
        references ZFONCTIONCP_ZFON deferrable 
         initially deferred;

    create index IN_ZCOM_ZDEP_1 on ZCOMMUNE_ZCOM (ZDEP_ID_LESCOMMUNES)
                    TABLESPACE @appli@_INDX;

    alter table ZCOMMUNE_ZCOM 
        add constraint FK_ZCOM_ZDEP_1 
        foreign key (ZDEP_ID_LESCOMMUNES) 
        references ZDEPARTEMENT_ZDEP deferrable 
         initially deferred;

    create index IN_ZCOB_ZSTR_1 on ZCOORDBANCAIRECP_ZCOB (ZSTR_ID_LESCOORDBANCAIRESCP)
                TABLESPACE @appli@_INDX;
    

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOB_ZRIB_1 
        foreign key (ZRIB_ID_LERIBCLASSIQUE) 
        references ZRIB_RIB deferrable 
         initially deferred;

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOB_ZSTR_1 
        foreign key (ZSTR_ID_LESCOORDBANCAIRESCP) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOM_ZRCB_1 
        foreign key (ZRCB_ID_LEREROUTAGE) 
        references ZREROUTAGECOORDBANCAIRECP_ZRCB deferrable 
         initially deferred;

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOB_ZRIB_2 
        foreign key (ZRIB_ID_LERIBAUTOMATISE) 
        references ZRIB_RIB deferrable 
         initially deferred;

    create index IN_ZDEP_ZREG_1 on ZDEPARTEMENT_ZDEP (ZREG_ID_LESDEPARTEMENTS)
                TABLESPACE @appli@_INDX;

    alter table ZDEPARTEMENT_ZDEP 
        add constraint FK_ZDEP_ZREG_1 
        foreign key (ZREG_ID_LESDEPARTEMENTS) 
        references ZREGION_ZREG deferrable 
         initially deferred;

    create index IN_ZECL_ZSTR_1 on ZECLATEMENTCP_ZECL (ZSTR_ID_LESECLSTRUBENEF)
                TABLESPACE @appli@_INDX;
    

    create index IN_ZECL_1 on ZECLATEMENTCP_ZECL (ZECLATEMENTCP_DIS)
                TABLESPACE @appli@_INDX;

    alter table ZECLATEMENTCP_ZECL 
        add constraint FK_ZECL_ZSTR_1 
        foreign key (ZSTR_ID_LESECLSTRUBENEF) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    create index IN_ZGUI_2 on ZGUICHETBANC_ZGUI (codeGuichet) 
                TABLESPACE @appli@_INDX;

    create index IN_ZGUI_1 on ZGUICHETBANC_ZGUI (codeEtablissement) 
                TABLESPACE @appli@_INDX;

    create index IN_ZGUI_3 on ZGUICHETBANC_ZGUI (dateDerniereMiseAJour)
                TABLESPACE @appli@_INDX;

    create index IN_ZRAT_ZSTR_1 on ZRATTACHEMENTCP_ZRAT (ZSTR_ID_LESRATSTRUABSORBEES)
                TABLESPACE @appli@_INDX;

    create index IN_ZDIS_1 on ZRATTACHEMENTCP_ZRAT (ZRATTACHEMENTCP_DIS)
                TABLESPACE @appli@_INDX;

    alter table ZRATTACHEMENTCP_ZRAT 
        add constraint FK_ZRAT_ZSTR_1 
        foreign key (ZSTR_ID_LESRATSTRUABSORBEES) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;


    create index IN_ZSTR_ZRAT_1 on ZSTRUCTURECP_ZSTR (ZRAT_ID_LESSTRUABSORBEES)
                TABLESPACE @appli@_INDX
    ;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZRAT_1 
        foreign key (ZRAT_ID_LESSTRUABSORBEES) 
        references ZRATTACHEMENTCP_ZRAT deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZMGE_1 
        foreign key (ZSTR_ID_LEMODEGESTION) 
        references ZMODEGESTIONCP_ZMGE deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZADR_1 
        foreign key (ZADR_ID_LADRESSEGEOGRAPHIQUE) 
        references ZADRESSE_ZADR deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZTSD_1 
        foreign key (ZTSD_ID_LETYPESERVICEDIRECTION) 
        references ZTYPESERVICEDIRECTION_ZTSD deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZPIN_1 
        foreign key (ZPIN_ID_LEPAYS) 
        references ZPAYSINSEE_ZPIN deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZCAS_1 
        foreign key (ZCAS_ID_LACATEGORIESTRUCTURECP) 
        references ZCATEGORIESTRUCTURECP_ZCAS deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZTSC_1 
        foreign key (ZTSC_ID_LETYPESTRUCTURECP) 
        references ZTYPESTRUCTURECP_ZTSC deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZADR_2 
        foreign key (ZADR_ID_LADRESSEPOSTALE) 
        references ZADRESSE_ZADR deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZCOM_1 
        foreign key (ZCOM_ID_LACOMMUNE) 
        references ZCOMMUNE_ZCOM deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZSTR_1 
        foreign key (ZSTR_ID_LACENTRALCOMPTABLE) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESHORAIRESCP 
        add constraint FK_ZSTR_LESHORAIRESCP_ZSTR_1 
        foreign key (ZSTR_ID_LESHORAIRESCP) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESPOLESACTIV 
        add constraint FK_ZSTR_LESPOLESACTIV_ZSTR_1 
        foreign key (ZSTR_ID_LESPOLESACTIV) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESPOLESACTIV 
        add constraint FK_ZSTR_ZTYP_1 
        foreign key (ZTYP_ID_LETYPEPOLEACTIV) 
        references ZTYPEPOLEACTIVITECP_ZTYP deferrable 
         initially deferred;

    alter table ZSTR_LESSERVICESCP 
        add constraint FK_ZSTR_LESSERVICESCP_ZSTR_1 
        foreign key (ZSTR_ID_LESSERVICESCP) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESSERVICESCP 
        add constraint FK_ZSTR_ZTYS_1 
        foreign key (ZTYS_ID_LETYPESERVICECP) 
        references ZTYPESERVICECP_ZTYS deferrable 
         initially deferred;

    alter table ZTYPESERVICECP_ZTYS 
        add constraint FK_ZTYS_ZDOM_1 
        foreign key (ZTYD_ID_UNDOMAINECP) 
        references ZDOMAINECP_ZDOM deferrable 
         initially deferred;

    create sequence ZADR_ID_SEQUENCE;

    create sequence ZATT_ID_SEQUENCE;

    create sequence ZCAS_ID_SEQUENCE;

    create sequence ZCOB_ID_SEQUENCE;

    create sequence ZCOM_ID_SEQUENCE;

    create sequence ZCPO_ID_SEQUENCE;

    create sequence ZDEP_ID_SEQUENCE;

    create sequence ZDOM_ID_SEQUENCE;

    create sequence ZECL_ID_SEQUENCE;

    create sequence ZFON_ID_SEQUENCE;

    create sequence ZGUI_ID_SEQUENCE;

    create sequence ZMGE_ID_SEQUENCE;

    create sequence ZPAY_ID_SEQUENCE;

    create sequence ZPIN_ID_SEQUENCE;

    create sequence ZRAT_ID_SEQUENCE;

    create sequence ZRCB_ID_SEQUENCE;

    create sequence ZRIB_ID_SEQUENCE;

    create sequence ZSTR_ID_SEQUENCE;

    create sequence ZTSC_ID_SEQUENCE;

    create sequence ZTSD_ID_SEQUENCE;

    create sequence ZTYP_ID_SEQUENCE;

    create sequence ZTYS_ID_SEQUENCE;

  -- Modifications à appliquer à la V17R25

	create table ZPROPIMMEUBLE_ZPIM (
        ZPIM_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codePropImmeuble varchar2(255 char) not null,
        libellePropImmeuble varchar2(255 char),
		 CONSTRAINT PK_ZPIM PRIMARY KEY(ZPIM_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZPIM_1 UNIQUE(codePropImmeuble)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80                         
    ;

	create table ZORGAACCUEIL_ZOAC (
        ZOAC_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeOrgAccueil varchar2(255 char) not null,
        libOrgAccueil varchar2(255 char),
        CONSTRAINT PK_ZOAC PRIMARY KEY(ZOAC_ID)
            USING INDEX
            TABLESPACE @appli@_INDX,
        CONSTRAINT UK_ZOAC_1 UNIQUE(codeOrgAccueil)
            USING INDEX
            TABLESPACE @appli@_INDX        
    )
        TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80                         
    ;

  create sequence ZPIM_ID_SEQUENCE;

  create sequence ZOAC_ID_SEQUENCE;
 
  alter table ZCOORDBANCAIRECP_ZCOB 
		add  ibANAutomatise varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  ZPIM_ID_LEPROPIMMEUBLE number(19,0);

alter table ZSTRUCTURECP_ZSTR 
		add  ZOAC_ID_LEORGAACCUEIL number(19,0);


  create index IN_ZCOB_2 on ZCOORDBANCAIRECP_ZCOB (codeIC)
                TABLESPACE @appli@_INDX ; 
--modifications V17R45 (recomandations sql advisor)

CREATE INDEX IN_ ZSTR_LESHORAIRESCP_1 ON ZSTR_LESHORAIRESCP 
(ZSTR_ID_LESHORAIRESCP);

CREATE INDEX IN_ ZSTR_LESSERVICESCP_1 ON ZSTR_LESSERVICESCP 
(ZSTR_ID_LESSERVICESCP);

CREATE INDEX IN_ ZSTR_LESPOLESACTIV_1 ON ZSTR_LESPOLESACTIV 
(ZSTR_ID_LESPOLESACTIV); 


--Modifications pour la V17R45 (ajout de liens de dependance et de typelien ) 
 

alter table ZSTRUCTURECP_ZSTR 
		add  EmailREC varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  AppliRec varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  LecteurOpt varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  CodeIndicateur varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  LibIndicateur varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add   BICCEP  varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  domiciliationCEP varchar2(255 char) ;



 

 create table  ZLIENSDEP_ZLID (
        ZLID_ID number(19,0) not null,
        ZLIENSDEP_DIS  varchar2(255 char) not null,
        VERSION number(10,0) not null,
        ZLTI_ID_LETYPELIEN number(19,0) not null,
        ZSTR_ID_LESLIENSDEP number(19,0),
     CONSTRAINT PK_ZLID  PRIMARY KEY(ZLID_ID)
            USING INDEX
            TABLESPACE @appli@_INDX                
    )
       TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80    ;


  create table ZTYPELIEN_ZTLI (
        ZTLI_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codetypelien varchar2(255 char) not null,
        libelletypelien varchar2(255 char),
        CONSTRAINT PK_ZTLI primary key (ZTLI_ID)
             USING INDEX
                  TABLESPACE @appli@_INDX ,
           CONSTRAINT UK_ZTLI_1  unique (codetypelien)
			 USING INDEX
            TABLESPACE @appli@_INDX    
    );

  create table ZLID_QSTRUCTLIES (
        ZLID_ID number(19,0) not null,
        STRUCTURELIEEPARID varchar2(255 char)
    )
 TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80 ;

 

 

  
 
 alter table ZLIENSDEP_ZLID 
        add constraint FK_ZLID_ZTLI_1 
        foreign key (ZLTI_ID_LETYPELIEN) 
        references ZTYPELIEN_ZTLI deferrable 
         initially deferred;

    alter table ZLIENSDEP_ZLID
        add constraint FK_ZLID_ZSTR_1 
        foreign key (ZSTR_ID_LESLIENSDEP) 
        references  ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

  



alter table ZLID_QSTRUCTLIES 
        add constraint FK_ZLID_QSTRUCTLIES_ZLID_1 
        foreign key (ZLID_ID) 
      references ZLIENSDEP_ZLID deferrable 
         initially deferred;

   
  

 create index IN_ZLID_1 on  ZLIENSDEP_ZLID (ZLIENSDEP_DIS)
			TABLESPACE @appli@_INDX;
 


create index IN_ZLID_ZSTR_1 on ZLIENSDEP_ZLID (ZSTR_ID_LESLIENSDEP)
                TABLESPACE @appli@_INDX;

create index IN_FK_ZLID_QSTRUCTLIES_ZLID_1 on ZLID_QSTRUCTLIES (ZLID_ID)
                TABLESPACE @appli@_INDX;

  create sequence ZLID_ID_SEQUENCE;

  create sequence ZTLI_ID_SEQUENCE;

----------------------------------------
   
--rajout des index sur les champs utilisés pour faire des recherches dans structure


 create index IN_ZSTR_5 on ZSTRUCTURECP_ZSTR (CODEANNEXE)
                TABLESPACE @appli@_INDX;


create index IN_ZSTR_7  on ZSTRUCTURECP_ZSTR (ZPIN_ID_LEPAYS)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_8  on ZSTRUCTURECP_ZSTR (DATEDERNIEREMAJ)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_9  on ZSTRUCTURECP_ZSTR (ETAT)
                TABLESPACE @appli@_INDX;



create index IN_ZCAS_2 on ZCATEGORIESTRUCTURECP_ZCAS (LIBELLECATEGORIE)
                TABLESPACE @appli@_INDX;


create index IN_ZTSC_2 on ZTYPESTRUCTURECP_ZTSC (LIBELLETYPESTRUCTURE)
                TABLESPACE @appli@_INDX;


create index IN_ZRAT_1  on ZRATTACHEMENTCP_ZRAT (DATEDERNIEREMAJ)
                TABLESPACE @appli@_INDX;


create index IN_ZECL_2  on ZECLATEMENTCP_ZECL (DATEDERNIEREMAJ)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_10  on ZSTRUCTURECP_ZSTR (ZSTR_ID_LACENTRALCOMPTABLE)
                TABLESPACE @appli@_INDX;

create index IN_ZSTR_11  on ZSTRUCTURECP_ZSTR (ZTSC_ID_LETYPESTRUCTURECP)
                TABLESPACE @appli@_INDX;
----------------------------------------
   
--prise en compte de l attribut sages


 ALTER TABLE ZSTRUCTURECP_ZSTR  ADD (SAGES VARCHAR2(10 byte));

