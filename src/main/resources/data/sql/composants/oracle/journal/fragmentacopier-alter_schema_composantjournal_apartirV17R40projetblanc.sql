 alter table OPERATIONJOURNAL_OPER 
		add  apurementPossible  number(1,0);

alter table OPERATIONJOURNAL_OPER 
		add  codeOperation  varchar2(255 char);

alter table OPERATIONJOURNAL_OPER 
		add  valeurIdentifiantMetier1  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  valeurIdentifiantMetier2  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  valeurIdentifiantMetier3  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  valeurIdentifiantMetier4  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  valeurIdentifiantMetier5  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  libelleIdentifiantMetier1  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  libelleIdentifiantMetier2  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  libelleIdentifiantMetier3  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  libelleIdentifiantMetier4  varchar2(255 char) ;

alter table OPERATIONJOURNAL_OPER 
		add  libelleIdentifiantMetier5  varchar2(255 char) ;
 

create index IN_OPER_4 on OPERATIONJOURNAL_OPER (libelleIdentifiantMetier1)
                TABLESPACE @appli@_INDX ;   
create index IN_OPER_5 on OPERATIONJOURNAL_OPER (libelleIdentifiantMetier2)
                TABLESPACE @appli@_INDX ;   
create index IN_OPER_6 on OPERATIONJOURNAL_OPER (libelleIdentifiantMetier3)
                TABLESPACE @appli@_INDX ;   
create index IN_OPER_7 on OPERATIONJOURNAL_OPER (libelleIdentifiantMetier4)
                TABLESPACE @appli@_INDX ;   
create index IN_OPER_8 on OPERATIONJOURNAL_OPER (libelleIdentifiantMetier5)
                TABLESPACE @appli@_INDX ;
create index IN_OPER_9 on OPERATIONJOURNAL_OPER (valeurIdentifiantMetier1)
                TABLESPACE @appli@_INDX ;   
create index IN_OPER_10 on OPERATIONJOURNAL_OPER (valeurIdentifiantMetier2)
                TABLESPACE @appli@_INDX ;   
create index IN_OPER_11 on OPERATIONJOURNAL_OPER (valeurIdentifiantMetier3)
                TABLESPACE @appli@_INDX ;   
create index IN_OPER_12 on OPERATIONJOURNAL_OPER (valeurIdentifiantMetier4)
                TABLESPACE @appli@_INDX ;   
create index IN_OPER_13 on OPERATIONJOURNAL_OPER (valeurIdentifiantMetier5)
                TABLESPACE @appli@_INDX ;   

alter table PARAMETREOPERATION_PAOP 
		add  valeurPrecedente  varchar2(255 char) ;