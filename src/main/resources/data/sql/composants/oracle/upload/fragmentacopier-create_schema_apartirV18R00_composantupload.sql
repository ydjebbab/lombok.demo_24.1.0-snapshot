    create table ZCONTENUFICHIER_ZCOF (
        id number(19,0) not null,
        version number(10,0) not null,
        DATA blob,
        CONSTRAINT PK_ZCOF primary key (id) USING INDEX TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

    create table ZFICHIERJOINT_ZFIJ (
        id number(19,0) not null,
        version number(10,0) not null,
        dateHeureSoumission timestamp,
        nomFichierOriginal varchar2(255 char),
        tailleFichier number(19,0),
        typeMimeFichier varchar2(255 char) not null,
        ZCOF_ID_LECONTENUDUFICHIER number(19,0),
        CONSTRAINT PK_ZFIJ primary key (id) USING INDEX TABLESPACE @appli@_INDX
    )
    TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80
    ;

    alter table ZFICHIERJOINT_ZFIJ 
        add constraint FK_ZFIJ_ZCOF_1 
        foreign key (ZCOF_ID_LECONTENUDUFICHIER) 
        references ZCONTENUFICHIER_ZCOF;

	create sequence ZCOF_ID_SEQUENCE;

	create sequence ZFIJ_ID_SEQUENCE;