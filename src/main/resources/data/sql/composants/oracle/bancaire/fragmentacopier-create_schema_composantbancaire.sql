create table ZGUICHETBANC_ZGUI (
        ZGUI_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeEtablissement varchar2(255 char),
        codeGuichet varchar2(255 char),
        nomGuichet varchar2(255 char),
        adresseGuichet varchar2(255 char),
        codePostal varchar2(255 char),
        villeCodePostal varchar2(255 char),
        denominationComplete varchar2(255 char),
        denominationAbregee varchar2(255 char),
        libelleAbrevDeDomiciliation varchar2(255 char),
        codeReglementDesCredits varchar2(255 char),
        moisDeModification varchar2(255 char),
        anneeDeModification varchar2(255 char),
        codeEnregistrement varchar2(255 char),
        nouveauCodeEtablissement varchar2(255 char),
        nouveauCodeGuichet varchar2(255 char),
        estEnVoieDePeremption number(1,0),
        estPerime number(1,0),
        dateDerniereMiseAJour timestamp,
        CONSTRAINT PK_ZGUICHETBANC_ZGUI primary key (ZGUI_ID)
        USING INDEX TABLESPACE @appli@_INDX
    ) TABLESPACE @appli@_DATA PCTFREE 10 PCTUSED 80;
    
create index IN_ZGUI_2 on ZGUICHETBANC_ZGUI (codeGuichet)
            TABLESPACE @appli@_INDX;

create index IN_ZGUI_3 on ZGUICHETBANC_ZGUI (dateDerniereMiseAJour)
            TABLESPACE @appli@_INDX;

create index IN_ZGUI_1 on ZGUICHETBANC_ZGUI (codeEtablissement)
            TABLESPACE @appli@_INDX;
    
create sequence ZGUI_ID_SEQUENCE;
