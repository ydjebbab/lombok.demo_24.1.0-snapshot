--================================
--Suppression des tables et vues preexistantes
--================================

drop table if exists T_DONNEES_OBLIGATOIRES cascade;
drop table if exists T_SA cascade;
drop table if exists T_COL_PAR_TYPE_RECHERCHE cascade;
drop table if exists T_LOC_DONNEES cascade;
drop table if exists T_TRACE_PRINCIPALE cascade;
drop table if exists T_EMETTEUR_TRACE cascade;
drop table if exists T_CRITERE_RECHERCHE cascade;
drop table if exists T_METIER_TRACE cascade;
drop table if exists T_TYPE_DONNEE cascade;   
drop table if exists T_DONNEES_PAR_COL cascade;
drop table if exists T_FAMILLE_TRACES cascade;
drop table if exists T_TYPE_TRACE cascade;
drop table if exists T_COL_RECHERCHE cascade;   
drop table if exists T_TYPE_RECHERCHE cascade;
drop table if exists T_STATISTIQUES cascade;
drop table if exists T_HORO_STATS cascade;

drop view if exists vue_trace_principale;
drop view if exists vue_metier;
drop view if exists vue_emetteur;

drop sequence if exists SEQ_TRACE_PRINCIPALE;
drop sequence if exists seq_stat;
drop sequence if exists seq_horo;