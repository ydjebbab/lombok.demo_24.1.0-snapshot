
    drop table if exists MESSAGETYPE_METY cascade ;

    drop table if exists MESSAGE_MESS cascade ;

    drop table if exists PROFIL_PROF cascade;

    drop sequence  if exists MESSAGETYPE_ID_SEQUENCE;

    drop sequence  if exists MESSAGE_ID_SEQUENCE;

    drop sequence  if exists PROFIL_ID_SEQUENCE;

    drop table IF EXISTS CODIQUE_CODI cascade ;    
    
    drop table IF EXISTS TJ_MESS_CODI_TJ cascade ;

    drop table IF EXISTS TJ_MESS_PROF_TJ cascade ;       

    drop sequence IF EXISTS CODIQUE_ID_SEQUENCE;
    
    create table MESSAGETYPE_METY (
        MESSAGETYPE_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        LIBELLEMESSAGETYPE varchar(2000 ) not null,
        primary key (MESSAGETYPE_ID),
        unique (LIBELLEMESSAGETYPE)
    );

    create table MESSAGE_MESS (
        MESSAGE_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        LIBELLEMESSAGE varchar(2000 ) not null,
        DATECREATIONMESSAGE timestamp not null,
        DATEDEBMESSAGE timestamp,
        DATEFINMESSAGE timestamp,
        DATEDEBINACPROF timestamp,
        DATEFININACPROF timestamp,
        TYPEMESSAGE varchar(255 ),
        primary key (MESSAGE_ID),
        unique (LIBELLEMESSAGE, DATECREATIONMESSAGE)
    );

    create table PROFIL_PROF (
        PROFIL_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        CODEPROFILCONCATCODEAPPLI varchar(255 ) not null,
        LIBELLEPROFIL varchar(255 ) not null,
        CODEPROFIL varchar(255 ) not null,
        MESS_ID_LISTPROFDESACT numeric(19,0),
        primary key (PROFIL_ID),
        unique (CODEPROFILCONCATCODEAPPLI)
    );

    create index IN_PROF_MESS_1 on PROFIL_PROF (MESS_ID_LISTPROFDESACT);

    alter table PROFIL_PROF 
        add constraint FK_PROF_MESS__1 
        foreign key (MESS_ID_LISTPROFDESACT) 
        references MESSAGE_MESS;

    create sequence MESSAGETYPE_ID_SEQUENCE;

    create sequence MESSAGE_ID_SEQUENCE;

    create sequence PROFIL_ID_SEQUENCE;

    create table CODIQUE_CODI (
        CODIQUE_ID numeric(19,0) not null,
        VERSION numeric(10,0) not null,
        LIBELLECODIQUE varchar(255 ) not null,
        primary key (CODIQUE_ID),
        unique (LIBELLECODIQUE)
    ); 
    
    create table  TJ_MESS_CODI_TJ (
        MESS_ID numeric(19,0) not null,
        CODI_ID numeric(19,0) not null,
        primary key (MESS_ID, CODI_ID)
    );

    create table  TJ_MESS_PROF_TJ (
        MESS_ID numeric(19,0) not null,
        PROF_ID numeric(19,0) not null,
        primary key (MESS_ID, PROF_ID)
    );

    

    alter table MESSAGE_MESS 
        add UIDCREATEURMESSAGE varchar(255 ) ;   

   
    alter table TJ_MESS_CODI_TJ 
        add constraint FK_TJ_MESS_CODI_TJ_MESS_1 
        foreign key (MESS_ID) 
        references MESSAGE_MESS;

    alter table TJ_MESS_CODI_TJ 
        add constraint FK_TJ_MESS_CODI_TJ_CODI_1 
        foreign key (CODI_ID) 
        references CODIQUE_CODI;

    alter table TJ_MESS_PROF_TJ 
        add constraint FK_TJ_MESS_PROF_TJ_MESS_1 
        foreign key (MESS_ID) 
        references MESSAGE_MESS;

    alter table TJ_MESS_PROF_TJ 
        add constraint FK_TJ_MESS_PROF_TJ_PROF_1 
        foreign key (PROF_ID) 
        references PROFIL_PROF;

   
    create sequence CODIQUE_ID_SEQUENCE;
    