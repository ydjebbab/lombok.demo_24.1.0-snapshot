---
--- SUPPRESSION DE L'UTILISATEUR DU COMPOSANT EDITION
--- VERSION : V13R05
--- 
--- Modifications 
--- wpetit        03/05/2007    creation      
---
--drop owned by Jdmo;
 revoke all on tablespace Jdmo_DATA from Jdmo;
revoke all on tablespace Jdmo_INDX from Jdmo;
revoke all on tablespace Jdmo_BLOB from Jdmo;

DROP USER Jdmo ;
