SET search_path = jdmo, pg_catalog;


CREATE TABLE qrtz_blob_triggers (
    trigger_name character varying(80) NOT NULL,
    trigger_group character varying(80) NOT NULL,
    blob_data bytea,
    sched_name character varying(120) DEFAULT 'TestScheduler'::character varying NOT NULL
);


ALTER TABLE qrtz_blob_triggers OWNER TO jdmo;

--
-- Name: qrtz_calendars; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE qrtz_calendars (
    calendar_name character varying(80) NOT NULL,
    calendar bytea,
    sched_name character varying(120) DEFAULT 'TestScheduler'::character varying NOT NULL
);


ALTER TABLE qrtz_calendars OWNER TO jdmo;

--
-- Name: qrtz_cron_triggers; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE qrtz_cron_triggers (
    trigger_name character varying(80) NOT NULL,
    trigger_group character varying(80) NOT NULL,
    cron_expression character varying(80) NOT NULL,
    time_zone_id character varying(80),
    sched_name character varying(120) DEFAULT 'TestScheduler'::character varying NOT NULL
);


ALTER TABLE qrtz_cron_triggers OWNER TO jdmo;

--
-- Name: qrtz_fired_triggers; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE qrtz_fired_triggers (
    entry_id character varying(95) NOT NULL,
    trigger_name character varying(80) NOT NULL,
    trigger_group character varying(80) NOT NULL,
    instance_name character varying(80) NOT NULL,
    fired_time numeric(13,0) NOT NULL,
    priority numeric(13,0) NOT NULL,
    state character varying(16) NOT NULL,
    job_name character varying(80),
    job_group character varying(80),
    requests_recovery boolean,
    is_nonconcurrent character varying(1),
    is_update_data character varying(1),
    sched_name character varying(120) DEFAULT 'TestScheduler'::character varying NOT NULL
);


ALTER TABLE qrtz_fired_triggers OWNER TO jdmo;

--
-- Name: qrtz_job_details; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE qrtz_job_details (
    job_name character varying(80) NOT NULL,
    job_group character varying(80) NOT NULL,
    description character varying(120),
    job_class_name character varying(128) NOT NULL,
    is_durable boolean NOT NULL,
    requests_recovery boolean NOT NULL,
    job_data bytea,
    is_nonconcurrent character varying(1),
    is_update_data character varying(1),
    sched_name character varying(120) DEFAULT 'TestScheduler'::character varying NOT NULL
);


ALTER TABLE qrtz_job_details OWNER TO jdmo;

--
-- Name: qrtz_locks; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE qrtz_locks (
    lock_name character varying(40) NOT NULL,
    sched_name character varying(120) DEFAULT 'TestScheduler'::character varying NOT NULL
);


ALTER TABLE qrtz_locks OWNER TO jdmo;

--
-- Name: qrtz_paused_trigger_grps; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE qrtz_paused_trigger_grps (
    trigger_group character varying(80) NOT NULL,
    sched_name character varying(120) DEFAULT 'TestScheduler'::character varying NOT NULL
);


ALTER TABLE qrtz_paused_trigger_grps OWNER TO jdmo;

--
-- Name: qrtz_scheduler_state; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE qrtz_scheduler_state (
    instance_name character varying(80) NOT NULL,
    last_checkin_time numeric(13,0) NOT NULL,
    checkin_interval numeric(13,0) NOT NULL,
    sched_name character varying(120) DEFAULT 'TestScheduler'::character varying NOT NULL
);


ALTER TABLE qrtz_scheduler_state OWNER TO jdmo;

--
-- Name: qrtz_simple_triggers; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE qrtz_simple_triggers (
    trigger_name character varying(80) NOT NULL,
    trigger_group character varying(80) NOT NULL,
    repeat_count numeric(7,0) NOT NULL,
    repeat_interval numeric(12,0) NOT NULL,
    times_triggered numeric(7,0) NOT NULL,
    sched_name character varying(120) DEFAULT 'TestScheduler'::character varying NOT NULL
);


ALTER TABLE qrtz_simple_triggers OWNER TO jdmo;

--
-- Name: qrtz_simprop_triggers; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE qrtz_simprop_triggers (
    sched_name character varying(120) NOT NULL,
    trigger_name character varying(200) NOT NULL,
    trigger_group character varying(200) NOT NULL,
    str_prop_1 character varying(512),
    str_prop_2 character varying(512),
    str_prop_3 character varying(512),
    int_prop_1 numeric(10,0),
    int_prop_2 numeric(10,0),
    long_prop_1 numeric(19,0),
    long_prop_2 numeric(19,0),
    dec_prop_1 numeric(13,4),
    dec_prop_2 numeric(13,4),
    bool_prop_1 character varying(1),
    bool_prop_2 character varying(1)
);


ALTER TABLE qrtz_simprop_triggers OWNER TO jdmo;

--
-- Name: qrtz_triggers; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE qrtz_triggers (
    trigger_name character varying(80) NOT NULL,
    trigger_group character varying(80) NOT NULL,
    job_name character varying(80) NOT NULL,
    job_group character varying(80) NOT NULL,
    description character varying(120),
    next_fire_time numeric(13,0),
    prev_fire_time numeric(13,0),
    priority numeric(13,0),
    trigger_state character varying(16) NOT NULL,
    trigger_type character varying(8) NOT NULL,
    start_time numeric(13,0) NOT NULL,
    end_time numeric(13,0),
    calendar_name character varying(80),
    misfire_instr numeric(2,0),
    job_data bytea,
    sched_name character varying(120) DEFAULT 'TestScheduler'::character varying NOT NULL
);


ALTER TABLE qrtz_triggers OWNER TO jdmo;

--
-- Name: zcontenuedition_id_sequence; Type: SEQUENCE; Schema: jdmo; Owner: jdmo
--

CREATE SEQUENCE zcontenuedition_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zcontenuedition_id_sequence OWNER TO jdmo;

--
-- Name: zcontenuedition_zcoe; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE zcontenuedition_zcoe (
    zcoe_id numeric(19,0) NOT NULL,
    version numeric(10,0) NOT NULL,
    data bytea
);


ALTER TABLE zcontenuedition_zcoe OWNER TO jdmo;

--
-- Name: zfiltreedition_id_sequence; Type: SEQUENCE; Schema: jdmo; Owner: jdmo
--

CREATE SEQUENCE zfiltreedition_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zfiltreedition_id_sequence OWNER TO jdmo;

--
-- Name: zfiltreedition_zfie; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE zfiltreedition_zfie (
    zfie_id numeric(19,0) NOT NULL,
    version numeric(10,0) NOT NULL,
    nomdufiltre character varying(255),
    allow boolean,
    deny boolean,
    zprd_id_listefiltres numeric(19,0)
);


ALTER TABLE zfiltreedition_zfie OWNER TO jdmo;

--
-- Name: zfiltrevalue_id_sequence; Type: SEQUENCE; Schema: jdmo; Owner: jdmo
--

CREATE SEQUENCE zfiltrevalue_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zfiltrevalue_id_sequence OWNER TO jdmo;

--
-- Name: zfiltrevalue_zfiv; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE zfiltrevalue_zfiv (
    zfiv_id numeric(19,0) NOT NULL,
    version numeric(10,0) NOT NULL,
    value character varying(255),
    zfie_id_valsfiltre numeric(19,0)
);


ALTER TABLE zfiltrevalue_zfiv OWNER TO jdmo;

--
-- Name: zjobhistory_id_sequence; Type: SEQUENCE; Schema: jdmo; Owner: jdmo
--

CREATE SEQUENCE zjobhistory_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zjobhistory_id_sequence OWNER TO jdmo;

--
-- Name: zjobhistory_zjoh; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE zjobhistory_zjoh (
    zjoh_id numeric(19,0) NOT NULL,
    version numeric(10,0) NOT NULL,
    editionuuid character varying(255),
    beaneditionid character varying(255),
    datepurge timestamp without time zone,
    nbjstock numeric(10,0),
    description character varying(255),
    urledition character varying(2000),
    typemime character varying(255),
    characterencoding character varying(255),
    extension character varying(255),
    nomficdownload character varying(255),
    stockageenbase boolean,
    applieditionroot character varying(255),
    indexrepertoire numeric(10,0),
    chemincomplet character varying(255),
    nomficstockage character varying(255),
    zcoe_id_lecontenuedition numeric(19,0),
    datedemande timestamp without time zone,
    starttime timestamp without time zone,
    endtime timestamp without time zone,
    status character varying(255),
    modesoumission character varying(255),
    triggername character varying(255),
    triggergroup character varying(255),
    resultat character varying(255),
    message character varying(4000),
    delailancement numeric(10,0),
    notificationmail boolean,
    maila character varying(255),
    mailde character varying(255),
    mailobjet character varying(255),
    mailmessage character varying(4000),
    envoipiecejointe boolean,
    applicationorigine character varying(255),
    uidproprietaire character varying(255),
    monodestinataire boolean,
    nouvelleedition boolean,
    taillefichier numeric(19,0),
    urlpintranet character varying(2000),
    urlpinternet character varying(2000)
);


ALTER TABLE zjobhistory_zjoh OWNER TO jdmo;

--
-- Name: zprofildesti_id_sequence; Type: SEQUENCE; Schema: jdmo; Owner: jdmo
--

CREATE SEQUENCE zprofildesti_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zprofildesti_id_sequence OWNER TO jdmo;

--
-- Name: zprofildesti_zprd; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE zprofildesti_zprd (
    zprd_id numeric(19,0) NOT NULL,
    version numeric(10,0) NOT NULL,
    nomprofil character varying(255),
    zjoh_id_profilsdesti numeric(19,0)
);


ALTER TABLE zprofildesti_zprd OWNER TO jdmo;

--
-- Name: ztrcpurgeedition_id_sequence; Type: SEQUENCE; Schema: jdmo; Owner: jdmo
--

CREATE SEQUENCE ztrcpurgeedition_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ztrcpurgeedition_id_sequence OWNER TO jdmo;

--
-- Name: ztrcpurgeedition_ztpe; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE ztrcpurgeedition_ztpe (
    ztpe_id numeric(19,0) NOT NULL,
    version numeric(10,0) NOT NULL,
    datedebutpurge timestamp without time zone,
    datefinpurge timestamp without time zone,
    etatpurge character varying(255),
    messageerreur character varying(4000),
    nbeditionssupprimees numeric(19,0),
    dureeexecution numeric(19,0),
    applicationorigine character varying(255)
);


ALTER TABLE ztrcpurgeedition_ztpe OWNER TO jdmo;

--
-- Name: zuidconsult_id_sequence; Type: SEQUENCE; Schema: jdmo; Owner: jdmo
--

CREATE SEQUENCE zuidconsult_id_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE zuidconsult_id_sequence OWNER TO jdmo;

--
-- Name: zuidconsult_zuco; Type: TABLE; Schema: jdmo; Owner: jdmo
--

CREATE TABLE zuidconsult_zuco (
    zuco_id numeric(19,0) NOT NULL,
    version numeric(10,0) NOT NULL,
    uidutilisateur character varying(255),
    zjoh_id_zuidconsult numeric(19,0)
);


ALTER TABLE zuidconsult_zuco OWNER TO jdmo;


INSERT INTO qrtz_locks values('TRIGGER_ACCESS');
INSERT INTO qrtz_locks values('JOB_ACCESS');
INSERT INTO qrtz_locks values('CALENDAR_ACCESS');
INSERT INTO qrtz_locks values('STATE_ACCESS');
INSERT INTO qrtz_locks values('MISFIRE_ACCESS');
COMMIT;

