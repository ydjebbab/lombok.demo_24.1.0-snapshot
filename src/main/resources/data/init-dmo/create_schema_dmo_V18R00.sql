 --cree les tablesinitiales de dmo (avec renommage des tables
 --des composants par Z)    
     

create table AVISIMPOSITION_AVIS (
        AVISIMPOSITION_ID number(19,0) not null,
        VERSION number(10,0) not null,
        reference varchar2(255 char) not null,
        montant double precision,
        dateRole timestamp,
        TYIM_ID_TYPEIMPOT number(19,0) not null,
        CONT_ID_LISTEAVISIMPOSITION number(19,0),
        CONSTRAINT PK_AVIS primary key (AVISIMPOSITION_ID) USING INDEX TABLESPACE DMO_INDX
    );

    create table CIVILITE_CIVI (
        id number(19,0) not null,
        version number(10,0) not null,
        code number(19,0) not null,
        libelle varchar2(255 char) not null,
        CONSTRAINT PK_CIVI primary key (id) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_CIVI unique (code)
    );

    
 create table ZRIB_RIB (
        id number(19,0) not null,
        version number(10,0) not null,
        codeBanque varchar2(255 char) not null,
        codeGuichet varchar2(255 char) not null,
        numeroCompte varchar2(255 char) not null,
        cleRib varchar2(255 char),
        estAutomatise number(1,0),
        CONSTRAINT PK_ZRIB PRIMARY KEY(id)
            USING INDEX
            TABLESPACE DMO_INDX         
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80             
    ;

  

    create table CONTRATMENSUALISATION_COME (
        CONTRATMENSU_ID number(19,0) not null,
        VERSION number(10,0) not null,
        NUMEROCONTRAT number(19,0) not null,
        anneePriseEffet number(19,0),
        estTraite number(1,0) not null,
        dateDemande timestamp not null,
        TYPEIMPOT_ID_TYPEIMPOT number(19,0) not null,
        RIB_ID_RIB number(19,0) not null,
        CONT_ID_LISTECONTRATMENSU number(19,0),
        CONSTRAINT PK_COME primary key (CONTRATMENSU_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_COME unique (NUMEROCONTRAT)
    );

    create table CONTRIBUABLE_CONT (
        CONT_ID number(19,0) not null,
        CONTRIBUABLE_DIS varchar2(255 char) not null,
        VERSION number(10,0) not null,
        IDENTIFIANT varchar2(255 char) not null,
        CIVI_ID_CIVILITE number(19,0),
        SIFA_ID_SITUATIONFAMILIALE number(19,0),
        adresseMail varchar2(255 char),
        nom varchar2(255 char),
        prenom varchar2(255 char),
        telephoneFixe varchar2(255 char),
        telephonePortable varchar2(255 char),
        dateDeMiseAJour varchar2(255 char),
        dateDeNaissance timestamp,
        fuseauhorairetel varchar2(255 char),
        solde double precision,
        CONSTRAINT PK_CONT primary key (CONT_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_CONT unique (IDENTIFIANT)
    );

    create table CONT_LISTEADRESSES (
        CONT_ID_LISTEADRESSES number(19,0) not null,
        codePostal varchar2(255 char),
        ligne1 varchar2(255 char),
        ligne2 varchar2(255 char),
        ligne3 varchar2(255 char),
        ligne4 varchar2(255 char),
        pays varchar2(255 char),
        ville varchar2(255 char),
        CONT_ID_LISTEADRESSES_INDEX number(10,0),
        CONSTRAINT PK_LISTEADRESSES primary key (CONT_ID_LISTEADRESSES, CONT_ID_LISTEADRESSES_INDEX) USING INDEX TABLESPACE DMO_INDX
    );

    


   
    create table PAYS_PAYS (
        PAYS_ID varchar2(255 char) not null,
        CODEISO varchar2(255 char),
        NOM varchar2(255 char) ,
        CONSTRAINT PK_PAYS primary key (PAYS_ID) USING INDEX TABLESPACE DMO_INDX
    );

    

    create table SITUATIONFAMI_SIFA (
        id number(19,0) not null,
        version number(10,0) not null,
        code number(19,0) not null,
        libelle varchar2(255 char) not null,
        CONSTRAINT PK_SIFA primary key (id) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_SIFA unique (code)
    );

   

    create table TYPEIMPOT_TYIM (
        id number(19,0) not null,
        version number(10,0) not null,
        codeImpot number(19,0) not null,
        estOuvertALaMensualisation number(1,0),
        estDestineAuxParticuliers number(1,0),
        nomImpot varchar2(255 char) not null,
        jourOuvertureAdhesion timestamp,
        jourLimiteAdhesion timestamp,
        seuil number(19,2),
        taux number(19,10),
        CONSTRAINT PK_TYIM primary key (id) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_TYIM unique (codeImpot)
    );

    create index IN_AVIS_CONT_1 on AVISIMPOSITION_AVIS (CONT_ID_LISTEAVISIMPOSITION)
            TABLESPACE DMO_INDX;

    alter table AVISIMPOSITION_AVIS 
        add constraint FK_AVIS_CONT_1 
        foreign key (CONT_ID_LISTEAVISIMPOSITION) 
        references CONTRIBUABLE_CONT;

    alter table AVISIMPOSITION_AVIS 
        add constraint FK_AVIS_TYIM_1 
        foreign key (TYIM_ID_TYPEIMPOT) 
        references TYPEIMPOT_TYIM;

    create index IN_COME_CONT_1 on CONTRATMENSUALISATION_COME (CONT_ID_LISTECONTRATMENSU)
            TABLESPACE DMO_INDX;

    alter table CONTRATMENSUALISATION_COME 
        add constraint FK_COME_CONT_1 
        foreign key (CONT_ID_LISTECONTRATMENSU) 
        references CONTRIBUABLE_CONT;

    alter table CONTRATMENSUALISATION_COME 
        add constraint FK_COME_TYIM_1 
        foreign key (TYPEIMPOT_ID_TYPEIMPOT) 
        references TYPEIMPOT_TYIM;

    alter table CONTRATMENSUALISATION_COME 
        add constraint FK_COME_RIB_1 
        foreign key (RIB_ID_RIB) 
        references ZRIB_RIB;

    create index IN_CONT_1 on CONTRIBUABLE_CONT (CONTRIBUABLE_DIS)
        TABLESPACE DMO_INDX;

    alter table CONTRIBUABLE_CONT 
        add constraint FK_CONT_SIFA_1 
        foreign key (SIFA_ID_SITUATIONFAMILIALE) 
        references SITUATIONFAMI_SIFA;

    alter table CONTRIBUABLE_CONT 
        add constraint FK_CONT_CIVI_1 
        foreign key (CIVI_ID_CIVILITE) 
        references CIVILITE_CIVI;

    alter table CONT_LISTEADRESSES 
        add constraint FK_CONT_ID_LISTEADRESSES_1 
        foreign key (CONT_ID_LISTEADRESSES) 
        references CONTRIBUABLE_CONT;

  
    create sequence CONTRATMENSU_ID_SEQUENCE
                    START WITH   50000
                    INCREMENT BY 1
                    MINVALUE     1
                    NOMAXVALUE
                    CACHE        20
                    NOCYCLE
                    NOORDER;

    create sequence hibernate_sequence
                       START WITH   50000
                    INCREMENT BY 1
                    MINVALUE     1
                    NOMAXVALUE
                    CACHE        20
                    NOCYCLE
                    NOORDER;
alter table CONTRATMENSUALISATION_COME 
        drop constraint FK_COME_RIB_1;

 alter table CONTRATMENSUALISATION_COME 
        add constraint FK_COME_RIB_1 
        foreign key (RIB_ID_RIB) 
        references ZRIB_RIB;
  
        
        create table ADRESSETEST_ADRT (
        ADRESSE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        voie varchar2(255 char),
        ville varchar2(255 char),
        CONSTRAINT PK_ADRT PRIMARY KEY(ADRESSE_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;

	create table PERSONNETEST_PRST (
        PERSONNE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        prenom varchar2(255 char),
        nom varchar2(255 char),
        CONSTRAINT PK_PRST PRIMARY KEY(PERSONNE_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;

	create sequence ADRESSE_ID_SEQUENCE;

    create sequence PERSONNE_ID_SEQUENCE;

	create table PERSONNEADRESSETEST_PADT (
        PERSONNE_ID number(19,0) not null,
        ADRESSE_ID number(19,0) not null,
        CONSTRAINT PK_PADT PRIMARY KEY(PERSONNE_ID, ADRESSE_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;

    alter table PERSONNEADRESSETEST_PADT 
        add constraint FK_PADT_ADRT
        foreign key (ADRESSE_ID) 
        references ADRESSETEST_ADRT(ADRESSE_ID);

    alter table PERSONNEADRESSETEST_PADT 
        add constraint FK_PADT_PRST 
        foreign key (PERSONNE_ID) 
        references PERSONNETEST_PRST(PERSONNE_ID);

	create table ORIGINETEST_ORIT (
        ORIGINE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        libelle varchar2(255 char),
        CONSTRAINT PK_ORIT PRIMARY KEY(ORIGINE_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;
    
	create sequence ORIGINE_ID_SEQUENCE;

	create table VILLETEST_VILT (
        VILLE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        libelle varchar2(255 char),
        CONSTRAINT PK_VILT PRIMARY KEY(VILLE_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;
    
	create sequence VILLE_ID_SEQUENCE;

	create table HABITANTTEST_HABT (
        HABITANT_ID number(19,0) not null,
        VERSION number(10,0) not null,
        nom varchar2(255 char),
        prenom varchar2(255 char),
        ORIGINE_ID number(19,0) not null,
        VILLE_ID number(19,0) not null,
        CONSTRAINT PK_HABT PRIMARY KEY(HABITANT_ID)
            USING INDEX
            TABLESPACE dmo_INDX
    )
        TABLESPACE dmo_DATA PCTFREE 10 PCTUSED 80 ;
    
	create sequence HABITANT_ID_SEQUENCE;

    alter table HABITANTTEST_HABT 
        add constraint FK_HABT_ORIT
        foreign key (ORIGINE_ID) 
        references ORIGINETEST_ORIT(ORIGINE_ID);

    alter table HABITANTTEST_HABT 
        add constraint FK_HABT_VILT
        foreign key (VILLE_ID) 
        references VILLETEST_VILT(VILLE_ID);
        
  create table ZCONTENUFICHIER_ZCOF (
        id number(19,0) not null,
        version number(10,0) not null,
        DATA blob,
        CONSTRAINT PK_ZCOF primary key (id) USING INDEX TABLESPACE DMO_INDX
    );


   create table ZFICHIERJOINT_ZFIJ (
        id number(19,0) not null,
        version number(10,0) not null,
        dateHeureSoumission timestamp,
        nomFichierOriginal varchar2(255 char),
        tailleFichier number(19,0),
        typeMimeFichier varchar2(255 char) not null,
        ZCOF_ID_LECONTENUDUFICHIER number(19,0),
        CONSTRAINT PK_ZFIJ primary key (id) USING INDEX TABLESPACE DMO_INDX
    );

    alter table ZFICHIERJOINT_ZFIJ 
        add constraint FK_ZFIJ_ZCOF_1 
        foreign key (ZCOF_ID_LECONTENUDUFICHIER) 
        references ZCONTENUFICHIER_ZCOF;

	create sequence ZCOF_ID_SEQUENCE;

	create sequence ZFIJ_ID_SEQUENCE;

     

	create table ZCODEPOSTAL_ZCPO (
        ZCPO_ID number(19,0) not null,
        version number(10,0) not null,
        code varchar2(255 char) not null,
        ville varchar2(255 char) not null,
        CONSTRAINT PK_ZCODEPOSTAL_ZCPO primary key (ZCPO_ID)
            USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZCODEPOSTAL_ZCPO unique (code, ville)
            USING INDEX TABLESPACE DMO_INDX
    ) TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80;
    
create index IN_ZCPO_1 on ZCODEPOSTAL_ZCPO (code)
            TABLESPACE DMO_INDX;
create index IN_ZCPO_2 on ZCODEPOSTAL_ZCPO (ville)
            TABLESPACE DMO_INDX;



  

	create table ZDOCUMENTATLAS_ZDOA (
        ZDOA_ID number(19,0) not null,
        VERSION number(10,0) not null,
        identifiantAtlas varchar2(255 char) not null,
        nom varchar2(255 char) not null,
        nomEnvoi varchar2(255 char),
		ETAT number(10,0) not null,
        dateDePeremption timestamp,
        dateDEnvoiAtlas timestamp,
        dateAccuseReception timestamp,
        datePurgeContenu timestamp,
		ZFIJ_ID_LEFICHIER number(19,0),
        CONSTRAINT PK_ZDOA PRIMARY KEY(ZDOA_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZDOA_1 UNIQUE(identifiantAtlas)
            USING INDEX
            TABLESPACE DMO_INDX
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80 ;

     
	alter table ZDOCUMENTATLAS_ZDOA 
        add constraint FK_ZDOA_ZFIJ_1
        foreign key (ZFIJ_ID_LEFICHIER) 
        references ZFICHIERJOINT_ZFIJ;

	 

	create sequence ZDOA_ID_SEQUENCE;
	
	create table ZGUICHETBANC_ZGUI (
        ZGUI_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeEtablissement varchar2(255 char),
        codeGuichet varchar2(255 char),
        nomGuichet varchar2(255 char),
        adresseGuichet varchar2(255 char),
        codePostal varchar2(255 char),
        villeCodePostal varchar2(255 char),
        denominationComplete varchar2(255 char),
        denominationAbregee varchar2(255 char),
        libelleAbrevDeDomiciliation varchar2(255 char),
        codeReglementDesCredits varchar2(255 char),
        moisDeModification varchar2(255 char),
        anneeDeModification varchar2(255 char),
        codeEnregistrement varchar2(255 char),
        nouveauCodeEtablissement varchar2(255 char),
        nouveauCodeGuichet varchar2(255 char),
        estEnVoieDePeremption number(1,0),
        estPerime number(1,0),
        dateDerniereMiseAJour timestamp,
        CONSTRAINT PK_ZGUICHETBANC_ZGUI primary key (ZGUI_ID)
        USING INDEX TABLESPACE DMO_INDX
    ) TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80;
    
create index IN_ZGUI_2 on ZGUICHETBANC_ZGUI (codeGuichet)
            TABLESPACE DMO_INDX;

create index IN_ZGUI_3 on ZGUICHETBANC_ZGUI (dateDerniereMiseAJour)
            TABLESPACE DMO_INDX;

create index IN_ZGUI_1 on ZGUICHETBANC_ZGUI (codeEtablissement)
            TABLESPACE DMO_INDX;
    



 create table ZFICHIERDEREJET_ZFDR (
        ZFDR_ID number(19,0) not null,
        VERSION number(10,0) not null,
        IDENTIFIANT varchar2(255 char) not null,
        DTCREATION timestamp not null,
        DTRETRAIT timestamp,
        DTVALIDRETRAIT timestamp,
        TRAIIMMARTPARINS number(1,0) not null,
        VERCORRARTPARINS number(1,0) not null,
        MOTIFDUREJET varchar2(255 char),
        NOMAFFICDEREJ varchar2(255 char),
        NOMAFFTYPDEFIC varchar2(255 char),
        NOMDELACLADEPRODUREJ varchar2(255 char),
        NOMDELACLADERETDUREJ varchar2(255 char),
        NOMINTFICDEREJ varchar2(255 char),
        NOMINTTYPDEFICREJ varchar2(255 char),
        PERDECONDANHISAPRRET number(19,0),
        PERDEVALDUFICDEREJ number(19,0),
        UIDUTIAYAVALIPOURET number(19,0),
        ENCODAGEDUTEXTE integer,
        ETATFICHIER integer,
        CONSTRAINT PK_ZFICHIERDEREJET_ZFDR PRIMARY KEY(ZFDR_ID)
        USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZFICHIERDEREJET_ZFDR UNIQUE(IDENTIFIANT)
        USING INDEX TABLESPACE DMO_INDX
    ) TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80;

    create table ZFILTREFDR_ZFIF (
        ZFIF_ID number(19,0) not null,
        VERSION number(10,0) not null,
        vallow number(1,0),
        vdeny number(1,0),
        nomDuFiltre varchar2(255 char),
        ZHAF_ID_LISTEFILTRES number(19,0),
        CONSTRAINT PK_ZFILTREFDR_ZFIF primary key (ZFIF_ID)
        USING INDEX TABLESPACE DMO_INDX
    ) TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80;

    create table ZHABILITATIONSFDR_ZHAF (
        ZHAF_ID number(19,0) not null,
        VERSION number(10,0) not null,
        dateDebut timestamp,
        dateFin timestamp,
        libelleCourtAppli varchar2(255 char),
        nomProfil varchar2(255 char) not null,
        ZFDR_ID_LISTEHABILIT number(19,0),
        CONSTRAINT PK_ZHABILITATIONSFDR_ZHAF primary key (ZHAF_ID)
        USING INDEX TABLESPACE DMO_INDX
    ) TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80;

    create table ZLISTEARTICLEFDR_ZLFR (
        ZLFR_ID_LISTEARTICLE number(19,0) not null,
        contenuArticle clob,
        estASupprime number(1,0),
        estCorrige number(1,0),
        motifDuRejet varchar2(255 char),
        numeroArticle number(19,0) not null,
        ZLFR_ID_LISTEARTICLE_INDEX number(10,0),
        CONSTRAINT PK_ZLISTEARTICLEFDR_ZLFR primary key (ZLFR_ID_LISTEARTICLE, ZLFR_ID_LISTEARTICLE_INDEX)
        USING INDEX TABLESPACE DMO_INDX
    ) TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80;

    create table ZVALEURFILTREFDR_ZVFF (
        ZVFF_ID number(19,0) not null,
        VERSION number(10,0) not null,
        valeur varchar2(255 char),
        ZFIF_ID_LESVALEURSFILTRES number(19,0),
        CONSTRAINT PK_ZVALEURFILTREFDR_ZVFF primary key (ZVFF_ID)
        USING INDEX TABLESPACE DMO_INDX
    ) TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80;

    create index IN_ZFIF_ZHAF_1 on ZFILTREFDR_ZFIF (ZHAF_ID_LISTEFILTRES);

    alter table ZFILTREFDR_ZFIF 
        add constraint FK_ZFIF_ZHAF_1 
        foreign key (ZHAF_ID_LISTEFILTRES) 
        references ZHABILITATIONSFDR_ZHAF;

    create index IN_ZLHA_ZFDR_1 on ZHABILITATIONSFDR_ZHAF (ZFDR_ID_LISTEHABILIT)
                    TABLESPACE DMO_INDX;

    alter table ZHABILITATIONSFDR_ZHAF 
        add constraint FK_ZLHA_ZFDR_1 
        foreign key (ZFDR_ID_LISTEHABILIT) 
        references ZFICHIERDEREJET_ZFDR;

    alter table ZLISTEARTICLEFDR_ZLFR 
        add constraint FK_ZLFR_ID_LISTEARTICLE_1 
        foreign key (ZLFR_ID_LISTEARTICLE) 
        references ZFICHIERDEREJET_ZFDR;

    create index IN_ZVFF_ZFIF_1 on ZVALEURFILTREFDR_ZVFF (ZFIF_ID_LESVALEURSFILTRES)
                     TABLESPACE DMO_INDX;

    alter table ZVALEURFILTREFDR_ZVFF 
        add constraint FK_ZVFF_ZFIF_1 
        foreign key (ZFIF_ID_LESVALEURSFILTRES) 
        references ZFILTREFDR_ZFIF;

    create sequence ZFDR_ID_SEQUENCE;

    create sequence ZFIF_ID_SEQUENCE;

    create sequence ZHAF_ID_SEQUENCE;

    create sequence ZVFF_ID_SEQUENCE;

    create table ZOPERATIONJOURNAL_ZOPE (
       ZOPE_ID number(19,0) not null,
        version number(10,0) not null,
        DATEHEUREOPERATION timestamp not null,
        IDENTIFIANTUTILISATEUROUBATCH varchar2(255 char) not null,
        natureOperation varchar2(255 char),
        identifiantStructure varchar2(255 char),
        typeDureeDeConservation varchar2(255 char),
        CONSTRAINT PK_ZOPERATIONJOURNAL_ZOPE primary key (ZOPE_ID)
                USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZOPERATIONJOURNAL_ZOPE unique (DATEHEUREOPERATION, IDENTIFIANTUTILISATEUROUBATCH)
                USING INDEX TABLESPACE DMO_INDX
    ) TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80;

    create table ZPARAMETREOPERATION_ZPAO (
        ZPAO_ID number(19,0) not null,
        version number(10,0) not null,
        NOM varchar2(255 char) not null,
        VALEUR varchar2(255 char),
        CONSTRAINT PK_ZPARAMETREOPERATION_ZPAO primary key (ZPAO_ID)
               USING INDEX TABLESPACE DMO_INDX
    ) TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80;

    create table TJ_ZOPE_ZPAO_TJ (
        ZOPE_ID number(19,0) not null,
        ZPAO_ID number(19,0) not null,
        CONSTRAINT PK_TJ_ZOPE_ZPAO_TJ primary key (ZOPE_ID,ZPAO_ID)
             USING INDEX TABLESPACE DMO_INDX
    ) TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80;

    alter table TJ_ZOPE_ZPAO_TJ 
        add constraint FK_TJ_ZOPE_ZPAO_TJ_ZPAO_1 
        foreign key (ZPAO_ID)
        references ZPARAMETREOPERATION_ZPAO;

    alter table TJ_ZOPE_ZPAO_TJ 
        add constraint FK_TJ_ZOPE_ZPAO_TJ_ZOPE_1 
        foreign key (ZOPE_ID) 
        references ZOPERATIONJOURNAL_ZOPE;

CREATE SEQUENCE ZOPE_ID_SEQUENCE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  CACHE 20;

CREATE SEQUENCE ZPAO_ID_SEQUENCE
  MINVALUE 1
  MAXVALUE 999999999999999999999999999
  INCREMENT BY 1
  NOCYCLE
  NOORDER
  CACHE 20;

CREATE INDEX IN_ZOPE_1 ON ZOPERATIONJOURNAL_ZOPE (NATUREOPERATION)
        TABLESPACE DMO_INDX;

CREATE INDEX IN_ZOPE_2 ON ZOPERATIONJOURNAL_ZOPE (IDENTIFIANTSTRUCTURE)
        TABLESPACE DMO_INDX;

CREATE INDEX IN_ZOPE_3 ON ZOPERATIONJOURNAL_ZOPE (IDENTIFIANTUTILISATEUROUBATCH)
        TABLESPACE DMO_INDX;        
        
CREATE INDEX IN_TJ_ZOPE_ZPAO_TJ_1  ON TJ_ZOPE_ZPAO_TJ(ZPAO_ID)
        TABLESPACE DMO_INDX;        

--ALTER TABLE Zparametreoperation_Zpao DISABLE UNIQUE (nom, valeur);


 alter table ZOPERATIONJOURNAL_ZOPE
		add  apurementPossible  number(1,0);

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  codeOperation  varchar2(255 char);


alter table  ZOPERATIONJOURNAL_ZOPE
		add  valeurIdentifiantMetier1  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  valeurIdentifiantMetier2  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE
		add  valeurIdentifiantMetier3  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  valeurIdentifiantMetier4  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  valeurIdentifiantMetier5  varchar2(255 char) ;

alter table ZOPERATIONJOURNAL_ZOPE 
		add  libelleIdentifiantMetier1  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE
		add  libelleIdentifiantMetier2  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  libelleIdentifiantMetier3  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE 
		add  libelleIdentifiantMetier4  varchar2(255 char) ;

alter table  ZOPERATIONJOURNAL_ZOPE
		add  libelleIdentifiantMetier5  varchar2(255 char) ;
 

create index IN_ZOPE_4 on  ZOPERATIONJOURNAL_ZOPE (libelleIdentifiantMetier1)
                TABLESPACE DMO_INDX ;   
create index IN_ZOPE_5 on  ZOPERATIONJOURNAL_ZOPE (libelleIdentifiantMetier2)
                TABLESPACE DMO_INDX ;   
create index IN_ZOPE_6 on  ZOPERATIONJOURNAL_ZOPE (libelleIdentifiantMetier3)
                TABLESPACE DMO_INDX ;   
create index IN_ZOPE_7 on  ZOPERATIONJOURNAL_ZOPE (libelleIdentifiantMetier4)
                TABLESPACE DMO_INDX ;   
create index IN_ZOPE_8 on  ZOPERATIONJOURNAL_ZOPE (libelleIdentifiantMetier5)
                TABLESPACE DMO_INDX ;
create index IN_ZOPE_9 on ZOPERATIONJOURNAL_ZOPE (valeurIdentifiantMetier1)
                TABLESPACE DMO_INDX ;   
create index IN_ZOPE_10 on ZOPERATIONJOURNAL_ZOPE (valeurIdentifiantMetier2)
                TABLESPACE DMO_INDX ;   
create index IN_ZOPE_11 on ZOPERATIONJOURNAL_ZOPE (valeurIdentifiantMetier3)
                TABLESPACE DMO_INDX ;   
create index IN_ZOPE_12 on ZOPERATIONJOURNAL_ZOPE (valeurIdentifiantMetier4)
                TABLESPACE DMO_INDX ;   
create index IN_ZOPE_13 on ZOPERATIONJOURNAL_ZOPE (valeurIdentifiantMetier5)
                TABLESPACE DMO_INDX ;   

alter table ZPARAMETREOPERATION_ZPAO 
		add  valeurPrecedente  varchar2(255 char) ;

    create table TJ_ZECL_ZSTR_TJ (
        ZECL_ID number(19,0) not null,
        ZSTR_ID number(19,0) not null,
        CONSTRAINT PK_TJ_ZECL_ZSTR_TJ PRIMARY KEY(ZECL_ID, ZSTR_ID)
            USING INDEX
            TABLESPACE DMO_INDX
    )
    TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80
    ;

    create table TJ_ZSTR_ZATT_TJ (
        ZSTR_ID number(19,0) not null,
        ZATT_ID number(19,0) not null,
        CONSTRAINT PK_TJ_ZSTR_ZATT_TJ PRIMARY KEY(ZSTR_ID, ZATT_ID)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
    TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80
    ;

    create table TJ_ZSTR_ZFON_TJ (
        ZSTR_ID number(19,0) not null,
        ZFON_ID number(19,0) not null,
        CONSTRAINT PK_TJ_ZSTR_ZFON_TJ PRIMARY KEY(ZSTR_ID, ZFON_ID)
            USING INDEX
            TABLESPACE DMO_INDX                
    )
    TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80
    ;

    create table ZADRESSE_ZADR (
        ZADR_ID number(19,0) not null,
        version number(10,0) not null,
        codePostalEtCedex varchar2(255 char),
        ligne2ServiceAppartEtgEsc varchar2(255 char),
        ligne3BatimentImmeubleResid varchar2(255 char),
        ligne4NumeroLibelleVoie varchar2(255 char),
        ligne5LieuDitMentionSpeciale varchar2(255 char),
        pays varchar2(255 char),
        ville varchar2(255 char),
        CONSTRAINT PK_ZADR PRIMARY KEY(ZADR_ID)
            USING INDEX
            TABLESPACE DMO_INDX                        
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80
    ;

    create table ZATTRIBUTIONTGCP_ZATT (
        ZATT_ID number(19,0) not null,
        VERSION number(10,0) not null,
        libelleattributionTG varchar2(255 char) not null,
        codeattributionTG varchar2(255 char),
        CONSTRAINT PK_ZATT PRIMARY KEY(ZATT_ID)
            USING INDEX
            TABLESPACE DMO_INDX,                       
        CONSTRAINT UK_ZATT_1 UNIQUE(libelleattributionTG)
            USING INDEX
            TABLESPACE DMO_INDX
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80
    ;

    create table ZCATEGORIESTRUCTURECP_ZCAS (
        ZCAS_ID number(19,0) not null,
        VERSION number(10,0) not null,
        CODECATEGORIE varchar2(255 char) not null,
        libellecategorie varchar2(255 char),
        CONSTRAINT PK_ZCAS PRIMARY KEY(ZCAS_ID)
            USING INDEX
            TABLESPACE DMO_INDX,        
        CONSTRAINT UK_ZCAS_1 UNIQUE(CODECATEGORIE)
            USING INDEX
            TABLESPACE DMO_INDX
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80    
    ;

     
    create table ZCOMMUNE_ZCOM (
        ZCOM_ID number(19,0) not null,
        codecommuneINSEE varchar2(255 char),
        libellecommune varchar2(255 char),
        ZDEP_ID_LESCOMMUNES number(19,0),
        CONSTRAINT PK_ZCOM PRIMARY KEY(ZCOM_ID)
            USING INDEX
            TABLESPACE DMO_INDX,        
        CONSTRAINT UK_ZCOM_1 UNIQUE(codecommuneINSEE)
            USING INDEX
            TABLESPACE DMO_INDX
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80        
    ;

    create table ZCOORDBANCAIRECP_ZCOB (
        ZCOB_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeFlux varchar2(255 char),
        codeIC varchar2(255 char),
        biC varchar2(255 char),
        ibAN varchar2(255 char),
        ZRIB_ID_LERIBCLASSIQUE number(19,0) unique,
        ZRIB_ID_LERIBAUTOMATISE number(19,0) unique,
        ZRCB_ID_LEREROUTAGE number(19,0) unique,
        ZSTR_ID_LESCOORDBANCAIRESCP number(19,0),
        CONSTRAINT PK_ZCOB PRIMARY KEY(ZCOB_ID)
            USING INDEX
            TABLESPACE DMO_INDX                
    )
       TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80        
    ;

    create table ZDEPARTEMENT_ZDEP (
        ZDEP_ID number(19,0) not null,
        codedepartementINSEE varchar2(255 char),
        libelledepartement varchar2(255 char),
        ZREG_ID_LESDEPARTEMENTS number(19,0),
        CONSTRAINT PK_ZDEP PRIMARY KEY(ZDEP_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZDEP_1 UNIQUE(codedepartementINSEE)
            USING INDEX
            TABLESPACE DMO_INDX
    )
       TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80        
    ;

    create table ZDOMAINECP_ZDOM (
        ZDOM_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codedomaine varchar2(255 char) not null,
        libelledomaine varchar2(255 char),
        CONSTRAINT PK_ZDOM PRIMARY KEY(ZDOM_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZDOM_1 UNIQUE(codedomaine)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
       TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80     
    ;

    create table ZECLATEMENTCP_ZECL (
        ZECL_ID number(19,0) not null,
        ZECLATEMENTCP_DIS varchar2(255 char) not null,
        VERSION number(10,0) not null,
        dateDerniereMaj timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ZSTR_ID_LESECLSTRUBENEF number(19,0),
        CONSTRAINT PK_ZECL PRIMARY KEY(ZECL_ID)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
       TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80         
    ;

    create table ZFONCTIONCP_ZFON (
        ZFON_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codefonction varchar2(255 char) not null,
        libellefonction varchar2(255 char),
        CONSTRAINT PK_ZFON PRIMARY KEY(ZFON_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZFON_1 UNIQUE(codefonction)
            USING INDEX
            TABLESPACE DMO_INDX            
    )
       TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80             
    ;

    
    create table ZMODEGESTIONCP_ZMGE (
        ZMGE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codemodegestion varchar2(255 char) not null,
        libellemodegestion varchar2(255 char),
        CONSTRAINT PK_ZMGE PRIMARY KEY(ZMGE_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZMGE_1 UNIQUE(codemodegestion)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80     
    ;

    create table ZPAYSINSEE_ZPIN (
        ZPIN_ID number(19,0) not null,
        codePaysInsee varchar2(255 char),
        nom varchar2(255 char),
        CONSTRAINT PK_ZPIN PRIMARY KEY(ZPIN_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZPIN_1 UNIQUE(codePaysInsee)
            USING INDEX
            TABLESPACE DMO_INDX
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80         
    ;

    create table ZPAYSISO_ZPIS (
        ZPIS_ID varchar2(255 char) not null,
        CODEISO varchar2(255 char),
        nom varchar2(255 char),
        CONSTRAINT PK_ZPIS PRIMARY KEY(ZPIS_ID)
            USING INDEX
            TABLESPACE DMO_INDX         
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80         
    ;

    create table ZRATTACHEMENTCP_ZRAT (
        ZRAT_ID number(19,0) not null,
        ZRATTACHEMENTCP_DIS varchar2(255 char) not null,
        VERSION number(10,0) not null,
        dateDerniereMaj timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ZSTR_ID_LESRATSTRUABSORBEES number(19,0),
        CONSTRAINT PK_ZRAT PRIMARY KEY(ZRAT_ID)
            USING INDEX
            TABLESPACE DMO_INDX                 
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80             
    ;

    create table ZREGION_ZREG (
        ZREG_ID number(19,0) not null,
        coderegionINSEE varchar2(255 char),
        libelleregion varchar2(255 char),
        CONSTRAINT PK_ZREG PRIMARY KEY(ZREG_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZREG_1 UNIQUE(coderegionINSEE)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80     
    ;

    create table ZREROUTAGECOORDBANCAIRECP_ZRCB (
        ZRCB_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeIC varchar2(255 char),
        dateDebutReroutage timestamp,
        dateFinReroutage timestamp,
        identifiantBeneficiaire varchar2(255 char),
        CONSTRAINT PK_ZRCB PRIMARY KEY(ZRCB_ID)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80         
    ;

    

    create table ZSTRUCTURECP_ZSTR (
        ZSTR_ID number(19,0) not null,
        ZSTRUCTURECP_DIS varchar2(255 char) not null,
        VERSION number(10,0) not null,
        IDENTITENOMINOE varchar2(255 char) not null,
        dateDerniereMaj timestamp,
        codique varchar2(255 char),
        codeAnnexe varchar2(255 char),
        denominationAbregee varchar2(255 char),
        denominationStandard varchar2(255 char),
        codeLiaison varchar2(255 char),
        libelleLong varchar2(255 char),
        libelleStandard varchar2(255 char),
        libelleAbrege varchar2(255 char),
        siret varchar2(255 char),
        etat varchar2(255 char),
        adresseMelGenerique varchar2(255 char),
        logementFonction varchar2(255 char),
        ZTSC_ID_LETYPESTRUCTURECP number(19,0),
        ZTSD_ID_LETYPESERVICEDIRECTION number(19,0),
        ZCAS_ID_LACATEGORIESTRUCTURECP number(19,0),
        ZPIN_ID_LEPAYS number(19,0),
        adresseAbregee varchar2(255 char),
        ZADR_ID_LADRESSEGEOGRAPHIQUE number(19,0) unique,
        ZADR_ID_LADRESSEPOSTALE number(19,0) unique,
        telStandard varchar2(255 char),
        telPoste1 varchar2(255 char),
        telPoste2 varchar2(255 char),
        telecopie varchar2(255 char),
        horairesAbg varchar2(255 char),
        horairesObsCompl varchar2(255 char),
        remunerationCodeIR varchar2(255 char),
        remunerationEchelleLettre varchar2(255 char),
        codeEtablissmentCEP varchar2(255 char),
        codeGuichetCEP varchar2(255 char),
        codeDICGL varchar2(255 char),
        diCGL varchar2(255 char),
        tenueComptaDateDebut timestamp,
        tenueComptaDateFin timestamp,
        ZSTR_ID_LACENTRALCOMPTABLE number(19,0),
        ZSTR_ID_LEMODEGESTION number(19,0),
        dateCreation timestamp,
        dateSuppression timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ancienCodique varchar2(255 char),
        ancienCodeAnnexe varchar2(255 char),
        ZRAT_ID_LESSTRUABSORBEES number(19,0),
        ZCOM_ID_LACOMMUNE number(19,0),
        codeCategTG varchar2(255 char),
        libCategTG varchar2(255 char),
        codeCategRF varchar2(255 char),
        libCategRF varchar2(255 char),
        codeGrRemEtger varchar2(255 char),
        libGrRemEtger varchar2(255 char),
        CONSTRAINT PK_ZSTR PRIMARY KEY(ZSTR_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZSTR_1 UNIQUE(IDENTITENOMINOE)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80           
    ;

    create index IN_ZSTR_1 on ZSTRUCTURECP_ZSTR (ZSTRUCTURECP_DIS)
                TABLESPACE DMO_INDX;

    create index IN_ZSTR_2 on ZSTRUCTURECP_ZSTR (CODIQUE)
                TABLESPACE DMO_INDX;
                
    create index IN_ZSTR_3 on ZSTRUCTURECP_ZSTR (ZCAS_ID_LACATEGORIESTRUCTURECP)
                TABLESPACE DMO_INDX;                

    create index IN_ZSTR_4 on ZSTRUCTURECP_ZSTR (ZCOM_ID_LACOMMUNE)
                TABLESPACE DMO_INDX;   

    create table ZSTR_LESHORAIRESCP (
        ZSTR_ID_LESHORAIRESCP number(19,0) not null,
        joursemaine varchar2(255 char),
        matindebut varchar2(255 char),
        matinfin varchar2(255 char),
        apremdebut varchar2(255 char),
        apremfin varchar2(255 char),
        ZSTR_ID_LESHORAIRESCP_INDEX number(10,0),
        CONSTRAINT PK_ZSTR_LESHORAIRESCP PRIMARY KEY(ZSTR_ID_LESHORAIRESCP, ZSTR_ID_LESHORAIRESCP_INDEX)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80     
    ;

    create table ZSTR_LESPOLESACTIV (
        ZSTR_ID_LESPOLESACTIV number(19,0),
        melpoleactivite varchar2(255 char),
        ZTYP_ID_LETYPEPOLEACTIV number(19,0),
        ZSTR_ID_LESPOLESACTIV_INDEX number(10,0),
        CONSTRAINT PK_ZSTR_LESPOLESACTIV PRIMARY KEY(ZSTR_ID_LESPOLESACTIV, ZSTR_ID_LESPOLESACTIV_INDEX)
            USING INDEX
            TABLESPACE DMO_INDX                
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80         
    ;

    create table ZSTR_LESSERVICESCP (
        ZSTR_ID_LESSERVICESCP number(19,0),
        telService varchar2(255 char),
        ZTYS_ID_LETYPESERVICECP number(19,0),
        ZSTR_ID_LESSERVICESCP_INDEX number(10,0),
        CONSTRAINT PK_ZSTR_LESSERVICESCP PRIMARY KEY(ZSTR_ID_LESSERVICESCP, ZSTR_ID_LESSERVICESCP_INDEX)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80             
    ;

    create table ZTYPEPOLEACTIVITECP_ZTYP (
        ZTYP_ID number(19,0) not null,
        VERSION number(10,0) not null,
        libpoleactivite varchar2(255 char) not null,
        codepoleactivite varchar2(255 char),
        CONSTRAINT PK_ZTYP PRIMARY KEY(ZTYP_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZTYP_1 UNIQUE(libpoleactivite)
            USING INDEX
            TABLESPACE DMO_INDX                
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80                 
    ;

    create table ZTYPESERVICECP_ZTYS (
        ZTYS_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeservice varchar2(255 char) not null,
        libelleservice varchar2(255 char),
        ZTYD_ID_UNDOMAINECP number(19,0),
        CONSTRAINT PK_ZTYS PRIMARY KEY(ZTYS_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZTYS_1 UNIQUE(codeservice)
            USING INDEX
            TABLESPACE DMO_INDX         
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80                     
    ;

    create table ZTYPESERVICEDIRECTION_ZTSD (
        ZTSD_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeTypeServiceDirection varchar2(255 char) not null,
        libelleTypeServiceDirection varchar2(255 char),
        CONSTRAINT PK_ZTSD PRIMARY KEY(ZTSD_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZTSD_1 UNIQUE(codeTypeServiceDirection)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80                         
    ;

    create table ZTYPESTRUCTURECP_ZTSC (
        ZTSC_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeTypeStructure varchar2(255 char) not null,
        libelleTypeStructure varchar2(255 char),
        CONSTRAINT PK_ZTSC PRIMARY KEY(ZTSC_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZTSC_1 UNIQUE(codeTypeStructure)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80                             
    ;


    alter table TJ_ZECL_ZSTR_TJ 
        add constraint FK_TJ_ZECL_ZSTR_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table TJ_ZECL_ZSTR_TJ 
        add constraint FK_TJ_ZECL_ZSTR_TJ_ZECL_1 
        foreign key (ZECL_ID) 
        references ZECLATEMENTCP_ZECL deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZATT_TJ 
        add constraint FK_TJ_ZSTR_ZATT_TJ_ZATT_1 
        foreign key (ZATT_ID) 
        references ZATTRIBUTIONTGCP_ZATT deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZATT_TJ 
        add constraint FK_TJ_ZSTR_ZATT_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZFON_TJ 
        add constraint FK_TJ_ZSTR_ZFON_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table TJ_ZSTR_ZFON_TJ 
        add constraint FK_TJ_ZSTR_ZFON_TJ_ZFON_1 
        foreign key (ZFON_ID) 
        references ZFONCTIONCP_ZFON deferrable 
         initially deferred;

    create index IN_ZCOM_ZDEP_1 on ZCOMMUNE_ZCOM (ZDEP_ID_LESCOMMUNES)
                    TABLESPACE DMO_INDX;

    alter table ZCOMMUNE_ZCOM 
        add constraint FK_ZCOM_ZDEP_1 
        foreign key (ZDEP_ID_LESCOMMUNES) 
        references ZDEPARTEMENT_ZDEP deferrable 
         initially deferred;

    create index IN_ZCOB_ZSTR_1 on ZCOORDBANCAIRECP_ZCOB (ZSTR_ID_LESCOORDBANCAIRESCP)
                TABLESPACE DMO_INDX;
    

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOB_ZRIB_1 
        foreign key (ZRIB_ID_LERIBCLASSIQUE) 
        references ZRIB_RIB deferrable 
         initially deferred;

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOB_ZSTR_1 
        foreign key (ZSTR_ID_LESCOORDBANCAIRESCP) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOM_ZRCB_1 
        foreign key (ZRCB_ID_LEREROUTAGE) 
        references ZREROUTAGECOORDBANCAIRECP_ZRCB deferrable 
         initially deferred;

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOB_ZRIB_2 
        foreign key (ZRIB_ID_LERIBAUTOMATISE) 
        references ZRIB_RIB deferrable 
         initially deferred;

    create index IN_ZDEP_ZREG_1 on ZDEPARTEMENT_ZDEP (ZREG_ID_LESDEPARTEMENTS)
                TABLESPACE DMO_INDX;

    alter table ZDEPARTEMENT_ZDEP 
        add constraint FK_ZDEP_ZREG_1 
        foreign key (ZREG_ID_LESDEPARTEMENTS) 
        references ZREGION_ZREG deferrable 
         initially deferred;

    create index IN_ZECL_ZSTR_1 on ZECLATEMENTCP_ZECL (ZSTR_ID_LESECLSTRUBENEF)
                TABLESPACE DMO_INDX;
    

    create index IN_ZECL_1 on ZECLATEMENTCP_ZECL (ZECLATEMENTCP_DIS)
                TABLESPACE DMO_INDX;

    alter table ZECLATEMENTCP_ZECL 
        add constraint FK_ZECL_ZSTR_1 
        foreign key (ZSTR_ID_LESECLSTRUBENEF) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    

    create index IN_ZRAT_ZSTR_1 on ZRATTACHEMENTCP_ZRAT (ZSTR_ID_LESRATSTRUABSORBEES)
                TABLESPACE DMO_INDX;

    create index IN_ZDIS_1 on ZRATTACHEMENTCP_ZRAT (ZRATTACHEMENTCP_DIS)
                TABLESPACE DMO_INDX;

    alter table ZRATTACHEMENTCP_ZRAT 
        add constraint FK_ZRAT_ZSTR_1 
        foreign key (ZSTR_ID_LESRATSTRUABSORBEES) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;


    create index IN_ZSTR_ZRAT_1 on ZSTRUCTURECP_ZSTR (ZRAT_ID_LESSTRUABSORBEES)
                TABLESPACE DMO_INDX
    ;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZRAT_1 
        foreign key (ZRAT_ID_LESSTRUABSORBEES) 
        references ZRATTACHEMENTCP_ZRAT deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZMGE_1 
        foreign key (ZSTR_ID_LEMODEGESTION) 
        references ZMODEGESTIONCP_ZMGE deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZADR_1 
        foreign key (ZADR_ID_LADRESSEGEOGRAPHIQUE) 
        references ZADRESSE_ZADR deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZTSD_1 
        foreign key (ZTSD_ID_LETYPESERVICEDIRECTION) 
        references ZTYPESERVICEDIRECTION_ZTSD deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZPIN_1 
        foreign key (ZPIN_ID_LEPAYS) 
        references ZPAYSINSEE_ZPIN deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZCAS_1 
        foreign key (ZCAS_ID_LACATEGORIESTRUCTURECP) 
        references ZCATEGORIESTRUCTURECP_ZCAS deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZTSC_1 
        foreign key (ZTSC_ID_LETYPESTRUCTURECP) 
        references ZTYPESTRUCTURECP_ZTSC deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZADR_2 
        foreign key (ZADR_ID_LADRESSEPOSTALE) 
        references ZADRESSE_ZADR deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZCOM_1 
        foreign key (ZCOM_ID_LACOMMUNE) 
        references ZCOMMUNE_ZCOM deferrable 
         initially deferred;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZSTR_1 
        foreign key (ZSTR_ID_LACENTRALCOMPTABLE) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESHORAIRESCP 
        add constraint FK_ZSTR_LESHORAIRESCP_ZSTR_1 
        foreign key (ZSTR_ID_LESHORAIRESCP) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESPOLESACTIV 
        add constraint FK_ZSTR_LESPOLESACTIV_ZSTR_1 
        foreign key (ZSTR_ID_LESPOLESACTIV) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESPOLESACTIV 
        add constraint FK_ZSTR_ZTYP_1 
        foreign key (ZTYP_ID_LETYPEPOLEACTIV) 
        references ZTYPEPOLEACTIVITECP_ZTYP deferrable 
         initially deferred;

    alter table ZSTR_LESSERVICESCP 
        add constraint FK_ZSTR_LESSERVICESCP_ZSTR_1 
        foreign key (ZSTR_ID_LESSERVICESCP) 
        references ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

    alter table ZSTR_LESSERVICESCP 
        add constraint FK_ZSTR_ZTYS_1 
        foreign key (ZTYS_ID_LETYPESERVICECP) 
        references ZTYPESERVICECP_ZTYS deferrable 
         initially deferred;

    alter table ZTYPESERVICECP_ZTYS 
        add constraint FK_ZTYS_ZDOM_1 
        foreign key (ZTYD_ID_UNDOMAINECP) 
        references ZDOMAINECP_ZDOM deferrable 
         initially deferred;

    create sequence ZADR_ID_SEQUENCE;

    create sequence ZATT_ID_SEQUENCE;

    create sequence ZCAS_ID_SEQUENCE;

    create sequence ZCOB_ID_SEQUENCE;

    create sequence ZCOM_ID_SEQUENCE;

    create sequence ZCPO_ID_SEQUENCE;

    create sequence ZDEP_ID_SEQUENCE;

    create sequence ZDOM_ID_SEQUENCE;

    create sequence ZECL_ID_SEQUENCE;

    create sequence ZFON_ID_SEQUENCE;

    create sequence ZGUI_ID_SEQUENCE;

    create sequence ZMGE_ID_SEQUENCE;

    create sequence ZPAY_ID_SEQUENCE;

    create sequence ZPIN_ID_SEQUENCE;

    create sequence ZRAT_ID_SEQUENCE;

    create sequence ZRCB_ID_SEQUENCE;

    create sequence ZRIB_ID_SEQUENCE;

    create sequence ZSTR_ID_SEQUENCE;

    create sequence ZTSC_ID_SEQUENCE;

    create sequence ZTSD_ID_SEQUENCE;

    create sequence ZTYP_ID_SEQUENCE;

    create sequence ZTYS_ID_SEQUENCE;

  -- Modifications à appliquer à la V17R25

	create table ZPROPIMMEUBLE_ZPIM (
        ZPIM_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codePropImmeuble varchar2(255 char) not null,
        libellePropImmeuble varchar2(255 char),
		 CONSTRAINT PK_ZPIM PRIMARY KEY(ZPIM_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZPIM_1 UNIQUE(codePropImmeuble)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80                         
    ;

	create table ZORGAACCUEIL_ZOAC (
        ZOAC_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeOrgAccueil varchar2(255 char) not null,
        libOrgAccueil varchar2(255 char),
        CONSTRAINT PK_ZOAC PRIMARY KEY(ZOAC_ID)
            USING INDEX
            TABLESPACE DMO_INDX,
        CONSTRAINT UK_ZOAC_1 UNIQUE(codeOrgAccueil)
            USING INDEX
            TABLESPACE DMO_INDX        
    )
        TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80                         
    ;

  create sequence ZPIM_ID_SEQUENCE;

  create sequence ZOAC_ID_SEQUENCE;
 
  alter table ZCOORDBANCAIRECP_ZCOB 
		add  ibANAutomatise varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  ZPIM_ID_LEPROPIMMEUBLE number(19,0);

alter table ZSTRUCTURECP_ZSTR 
		add  ZOAC_ID_LEORGAACCUEIL number(19,0);


  create index IN_ZCOB_2 on ZCOORDBANCAIRECP_ZCOB (codeIC)
                TABLESPACE DMO_INDX ; 
--modifications V17R45 (recomandations sql advisor)

--CREATE INDEX IN_ ZSTR_LESHORAIRESCP_1 ON ZSTR_LESHORAIRESCP 
--(ZSTR_ID_LESHORAIRESCP);

--CREATE INDEX IN_ ZSTR_LESSERVICESCP_1 ON ZSTR_LESSERVICESCP 
--(ZSTR_ID_LESSERVICESCP);

--CREATE INDEX IN_ ZSTR_LESPOLESACTIV_1 ON ZSTR_LESPOLESACTIV 
--(ZSTR_ID_LESPOLESACTIV); 


--Modifications pour la V17R45 (ajout de liens de dependance et de typelien ) 
 

alter table ZSTRUCTURECP_ZSTR 
		add  EmailREC varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  AppliRec varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  LecteurOpt varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  CodeIndicateur varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  LibIndicateur varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add   BICCEP  varchar2(255 char) ;

alter table ZSTRUCTURECP_ZSTR 
		add  domiciliationCEP varchar2(255 char) ;



 

 create table  ZLIENSDEP_ZLID (
        ZLID_ID number(19,0) not null,
        ZLIENSDEP_DIS  varchar2(255 char) not null,
        VERSION number(10,0) not null,
        ZLTI_ID_LETYPELIEN number(19,0) not null,
        ZSTR_ID_LESLIENSDEP number(19,0),
     CONSTRAINT PK_ZLID  PRIMARY KEY(ZLID_ID)
            USING INDEX
            TABLESPACE DMO_INDX                
    )
       TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80    ;


  create table ZTYPELIEN_ZTLI (
        ZTLI_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codetypelien varchar2(255 char) not null,
        libelletypelien varchar2(255 char),
        CONSTRAINT PK_ZTLI primary key (ZTLI_ID)
             USING INDEX
                  TABLESPACE DMO_INDX ,
           CONSTRAINT UK_ZTLI_1  unique (codetypelien)
			 USING INDEX
            TABLESPACE DMO_INDX    
    );

  create table ZLID_QSTRUCTLIES (
        ZLID_ID number(19,0) not null,
        STRUCTURELIEEPARID varchar2(255 char)
    )
 TABLESPACE DMO_DATA PCTFREE 10 PCTUSED 80 ;

 

 

  
 
 alter table ZLIENSDEP_ZLID 
        add constraint FK_ZLID_ZTLI_1 
        foreign key (ZLTI_ID_LETYPELIEN) 
        references ZTYPELIEN_ZTLI deferrable 
         initially deferred;

    alter table ZLIENSDEP_ZLID
        add constraint FK_ZLID_ZSTR_1 
        foreign key (ZSTR_ID_LESLIENSDEP) 
        references  ZSTRUCTURECP_ZSTR deferrable 
         initially deferred;

  



alter table ZLID_QSTRUCTLIES 
        add constraint FK_ZLID_QSTRUCTLIES_ZLID_1 
        foreign key (ZLID_ID) 
      references ZLIENSDEP_ZLID deferrable 
         initially deferred;

   
  

 create index IN_ZLID_1 on  ZLIENSDEP_ZLID (ZLIENSDEP_DIS)
			TABLESPACE DMO_INDX;
 


create index IN_ZLID_ZSTR_1 on ZLIENSDEP_ZLID (ZSTR_ID_LESLIENSDEP)
                TABLESPACE DMO_INDX;

create index IN_FK_ZLID_QSTRUCTLIES_ZLID_1 on ZLID_QSTRUCTLIES (ZLID_ID)
                TABLESPACE DMO_INDX;

  create sequence ZLID_ID_SEQUENCE;

  create sequence ZTLI_ID_SEQUENCE;
  
   create index IN_ZSTR_5 on ZSTRUCTURECP_ZSTR (CODEANNEXE)
                TABLESPACE DMO_INDX;


create index IN_ZSTR_7  on ZSTRUCTURECP_ZSTR (ZPIN_ID_LEPAYS)
                TABLESPACE DMO_INDX;

create index IN_ZSTR_8  on ZSTRUCTURECP_ZSTR (DATEDERNIEREMAJ)
                TABLESPACE DMO_INDX;

create index IN_ZSTR_9  on ZSTRUCTURECP_ZSTR (ETAT)
                TABLESPACE DMO_INDX;



create index IN_ZCAS_2 on ZCATEGORIESTRUCTURECP_ZCAS (LIBELLECATEGORIE)
                TABLESPACE DMO_INDX;


create index IN_ZTSC_2 on ZTYPESTRUCTURECP_ZTSC (LIBELLETYPESTRUCTURE)
                TABLESPACE DMO_INDX;


create index IN_ZRAT_1  on ZRATTACHEMENTCP_ZRAT (DATEDERNIEREMAJ)
                TABLESPACE DMO_INDX;


create index IN_ZECL_2  on ZECLATEMENTCP_ZECL (DATEDERNIEREMAJ)
                TABLESPACE DMO_INDX;

create index IN_ZSTR_10  on ZSTRUCTURECP_ZSTR (ZSTR_ID_LACENTRALCOMPTABLE)
                TABLESPACE DMO_INDX;

create index IN_ZSTR_11  on ZSTRUCTURECP_ZSTR (ZTSC_ID_LETYPESTRUCTURECP)
                TABLESPACE DMO_INDX;



 
   

 
 create table DOCUMENTJOINT_DOC (
        id number(19,0) not null,
        version number(10,0) not null,
        identifiant varchar2(255 char),
        ZFIJ_ID_leFichierJoint number(19,0) not null,
        CONT_ID_LESDOCSJOINTS number(19,0),
        CONT_ID_LESDOCSJOINTS_INDEX number(10,0),
        CONSTRAINT PK_DOC primary key (id) USING INDEX TABLESPACE DMO_INDX
    );
    
     alter table DOCUMENTJOINT_DOC 
        add constraint FK6C171108270C3B81 
        foreign key (ZFIJ_ID_leFichierJoint) 
        references ZFICHIERJOINT_ZFIJ;

    alter table DOCUMENTJOINT_DOC 
        add constraint FK_FIC_CONT_1 
        foreign key (CONT_ID_LESDOCSJOINTS) 
        references CONTRIBUABLE_CONT;


----------------------------------------
ALTER TABLE ZSTRUCTURECP_ZSTR  ADD (SAGES VARCHAR2(10 byte));
