    create table TJ_ZECL_ZSTR_TJ (
        ZECL_ID number(19,0) not null,
        ZSTR_ID number(19,0) not null,
        CONSTRAINT PK_ZSTR_TJ primary key (ZECL_ID, ZSTR_ID) USING INDEX TABLESPACE DMO_INDX
    );

    create table TJ_ZSTR_ZATT_TJ (
        ZSTR_ID number(19,0) not null,
        ZATT_ID number(19,0) not null,
        CONSTRAINT PK_ZATT_TJ primary key (ZSTR_ID, ZATT_ID) USING INDEX TABLESPACE DMO_INDX
    );

    create table TJ_ZSTR_ZFON_TJ (
        ZSTR_ID number(19,0) not null,
        ZFON_ID number(19,0) not null,
        CONSTRAINT PK_ZFON_TJ primary key (ZSTR_ID, ZFON_ID) USING INDEX TABLESPACE DMO_INDX
    );

    create table ZADRESSE_ZADR (
        ZADR_ID number(19,0) not null,
        version number(10,0) not null,
        codePostalEtCedex varchar2(255 char),
        ligne2ServiceAppartEtgEsc varchar2(255 char),
        ligne3BatimentImmeubleResid varchar2(255 char),
        ligne4NumeroLibelleVoie varchar2(255 char),
        ligne5LieuDitMentionSpeciale varchar2(255 char),
        pays varchar2(255 char),
        ville varchar2(255 char),
        CONSTRAINT PK_ZADR primary key (ZADR_ID) USING INDEX TABLESPACE DMO_INDX
    );

    create table ZATTRIBUTIONTGCP_ZATT (
        ZATT_ID number(19,0) not null,
        VERSION number(10,0) not null,
        libelleattributionTG varchar2(255 char) not null,
        codeattributionTG varchar2(255 char),
        CONSTRAINT PK_ZATT primary key (ZATT_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_ZATT unique (libelleattributionTG)
    );

    create table ZCATEGORIESTRUCTURECP_ZCAS (
        ZCAS_ID number(19,0) not null,
        VERSION number(10,0) not null,
        CODECATEGORIE varchar2(255 char) not null,
        libellecategorie varchar2(255 char),
        CONSTRAINT PK_ZCAS primary key (ZCAS_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN_ZCAS unique (CODECATEGORIE)
    );

    create table ZCOMMUNE_ZCOM (
        ZCOM_ID number(19,0) not null,
        codecommuneINSEE varchar2(255 char),
        libellecommune varchar2(255 char),
        ZDEP_ID_LESCOMMUNES number(19,0),
        CONSTRAINT PK_ZCOM primary key (ZCOM_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN_ZCOM unique (codecommuneINSEE)
    );

    create table ZCOORDBANCAIRECP_ZCOB (
        ZCOB_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeFlux varchar2(255 char),
        codeIC varchar2(255 char),
        biC varchar2(255 char),
        ZRIB_ID_LERIB number(19,0) unique,
        ZSTR_ID_LESCOORDBANCAIRESCP number(19,0),
        CONSTRAINT PK_ZCOB primary key (ZCOB_ID) USING INDEX TABLESPACE DMO_INDX
    );

    create table ZDEPARTEMENT_ZDEP (
        ZDEP_ID number(19,0) not null,
        codedepartementINSEE varchar2(255 char),
        libelledepartement varchar2(255 char),
        ZREG_ID_LESDEPARTEMENTS number(19,0),
        CONSTRAINT PK_ZDEP primary key (ZDEP_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_ZDEP unique (codedepartementINSEE)
    );

    create table ZDOMAINECP_ZDOM (
        ZDOM_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codedomaine varchar2(255 char) not null,
        libelledomaine varchar2(255 char),
        CONSTRAINT PK_ZDOM primary key (ZDOM_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_ZDOM unique (codedomaine)
    );

    create table ZECLATEMENTCP_ZECL (
        ZECL_ID number(19,0) not null,
        ZECLATEMENTCP_DIS varchar2(255 char) not null,
        VERSION number(10,0) not null,
        dateDerniereMaj timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ZSTR_ID_LESECLSTRUBENEF number(19,0),
        CONSTRAINT PK_ZECL primary key (ZECL_ID) USING INDEX TABLESPACE DMO_INDX
    );

    create table ZFONCTIONCP_ZFON (
        ZFON_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codefonction varchar2(255 char) not null,
        libellefonction varchar2(255 char),
        CONSTRAINT PK_ZFON primary key (ZFON_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN_ZFON unique (codefonction)
    );

    create table ZMODEGESTIONCP_ZMGE (
        ZMGE_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codemodegestion varchar2(255 char) not null,
        libellemodegestion varchar2(255 char),
        CONSTRAINT PK_ZMGE primary key (ZMGE_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN_ZMGE unique (codemodegestion)
    );

    create table ZPAYSINSEE_ZPIN (
        ZPIN_ID number(19,0) not null,
        codePaysInsee varchar2(255 char),
        nom varchar2(255 char),
        CONSTRAINT PK_ZPIN primary key (ZPIN_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN_ZPIN unique (codePaysInsee)
    );

    create table ZRATTACHEMENTCP_ZRAT (
        ZRAT_ID number(19,0) not null,
        ZRATTACHEMENTCP_DIS varchar2(255 char) not null,
        VERSION number(10,0) not null,
        dateDerniereMaj timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ZSTR_ID_LESRATSTRUABSORBEES number(19,0),
        CONSTRAINT PK_ZRAT primary key (ZRAT_ID) USING INDEX TABLESPACE DMO_INDX
    );

    create table ZREGION_ZREG (
        ZREG_ID number(19,0) not null,
        coderegionINSEE varchar2(255 char),
        libelleregion varchar2(255 char),
        CONSTRAINT PK_ZREG primary key (ZREG_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_ZREGA unique (coderegionINSEE)
    );

    create table ZRIB_RIB (
        id number(19,0) not null,
        version number(10,0) not null,
        codeBanque varchar2(255 char) not null,
        codeGuichet varchar2(255 char) not null,
        numeroCompte varchar2(255 char) not null,
        cleRib varchar2(255 char),
        ibAN varchar2(255 char),
        estAutomatise number(1,0),
        CONSTRAINT PK_ZRIB primary key (id) USING INDEX TABLESPACE DMO_INDX
    );

    create table ZSTRUCTURECP_ZSTR (
        ZSTR_ID number(19,0) not null,
        ZSTRUCTURECP_DIS varchar2(255 char) not null,
        VERSION number(10,0) not null,
        IDENTITENOMINOE varchar2(255 char) not null,
        dateDerniereMaj timestamp,
        codique varchar2(255 char),
        codeAnnexe varchar2(255 char),
        denominationAbregee varchar2(255 char),
        denominationStandard varchar2(255 char),
        codeLiaison varchar2(255 char),
        libelleLong varchar2(255 char),
        libelleStandard varchar2(255 char),
        libelleAbrege varchar2(255 char),
        typeStructure varchar2(255 char),
        siret varchar2(255 char),
        etat varchar2(255 char),
        adresseMelGenerique varchar2(255 char),
        logementFonction varchar2(255 char),
        ZCAS_ID_LACATEGORIESTRUCTURECP number(19,0),
        ZPIN_ID_LEPAYS number(19,0),
        adresseAbregee varchar2(255 char),
        ZADR_ID_LADRESSEGEOGRAPHIQUE number(19,0) unique,
        ZADR_ID_LADRESSEPOSTALE number(19,0) unique,
        telStandard varchar2(255 char),
        telPoste1 varchar2(255 char),
        telPoste2 varchar2(255 char),
        telecopie varchar2(255 char),
        horairesAbg varchar2(255 char),
        horairesObsCompl varchar2(255 char),
        remunerationCodeIR varchar2(255 char),
        remunerationEchelleLettre varchar2(255 char),
        codeEtablissmentCEP varchar2(255 char),
        codeGuichetCEP varchar2(255 char),
        codeDICGL varchar2(255 char),
        diCGL varchar2(255 char),
        tenueComptaDateDebut timestamp,
        tenueComptaDateFin timestamp,
        ZSTR_ID_LACENTRALCOMPTABLE number(19,0),
        ZSTR_ID_LEMODEGESTION number(19,0),
        dateCreation timestamp,
        dateSuppression timestamp,
        dateDecision timestamp,
        dateRealisation timestamp,
        ancienCodique varchar2(255 char),
        ancienCodeAnnexe varchar2(255 char),
        ZRAT_ID_LESSTRUABSORBEES number(19,0),
        ZCOM_ID_LACOMMUNE number(19,0),
        codeCategTG varchar2(255 char),
        libCategTG varchar2(255 char),
        codeCategRF varchar2(255 char),
        libCategRF varchar2(255 char),
        codeGrRemEtger varchar2(255 char),
        libGrRemEtger varchar2(255 char),
        ZTSD_ID_LETYPESERVICEDIRECTION number(19,0),
        CONSTRAINT PK_ZSTR primary key (ZSTR_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_ZSTR unique (IDENTITENOMINOE)
    );

    create table ZSTR_LESHORAIRESCP (
        ZSTR_ID_LESHORAIRESCP number(19,0) not null,
        joursemaine varchar2(255 char),
        matindebut varchar2(255 char),
        matinfin varchar2(255 char),
        apremdebut varchar2(255 char),
        apremfin varchar2(255 char),
        ZSTR_ID_LESHORAIRESCP_INDEX number(10,0),
        CONSTRAINT PK_LESHORAIRESCP primary key (ZSTR_ID_LESHORAIRESCP, ZSTR_ID_LESHORAIRESCP_INDEX) USING INDEX TABLESPACE DMO_INDX
    );

    create table ZSTR_LESPOLESACTIV (
        ZSTR_ID_LESPOLESACTIV number(19,0),
        melpoleactivite varchar2(255 char),
        ZTYP_ID_LETYPEPOLEACTIV number(19,0),
        ZSTR_ID_LESPOLESACTIV_INDEX number(10,0),
        CONSTRAINT PK_LESPOLESACTIV primary key (ZSTR_ID_LESPOLESACTIV, ZSTR_ID_LESPOLESACTIV_INDEX) USING INDEX TABLESPACE DMO_INDX
    );

    create table ZSTR_LESSERVICESCP (
        ZSTR_ID_LESSERVICESCP number(19,0),
        telService varchar2(255 char),
        ZTYS_ID_LETYPESERVICECP number(19,0),
        ZSTR_ID_LESSERVICESCP_INDEX number(10,0),
        CONSTRAINT PK_LESSERVICESCP primary key (ZSTR_ID_LESSERVICESCP, ZSTR_ID_LESSERVICESCP_INDEX) USING INDEX TABLESPACE DMO_INDX
    );

    create table ZTYPEPOLEACTIVITECP_ZTYP (
        ZTYP_ID number(19,0) not null,
        VERSION number(10,0) not null,
        libpoleactivite varchar2(255 char) not null,
        codepoleactivite varchar2(255 char),
        CONSTRAINT PK_ZTYP primary key (ZTYP_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_ZTYP unique (libpoleactivite)
    );

    create table ZTYPESERVICECP_ZTYS (
        ZTYS_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeservice varchar2(255 char) not null,
        libelleservice varchar2(255 char),
        ZTYD_ID_UNDOMAINECP number(19,0),
        CONSTRAINT PK_ZTYS primary key (ZTYS_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_ZTYS unique (codeservice)
    );

    create table ZTYPESERVICEDIRECTION_ZTSD (
        ZTSD_ID number(19,0) not null,
        VERSION number(10,0) not null,
        codeTypeServiceDirection varchar2(255 char) not null,
        libelleTypeServiceDirection varchar2(255 char),
        CONSTRAINT PK_ZTSD primary key (ZTSD_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_ZTSD unique (codeTypeServiceDirection)
    );

    alter table TJ_ZECL_ZSTR_TJ 
        add constraint FK_TJ_ZECL_ZSTR_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR;

    alter table TJ_ZECL_ZSTR_TJ 
        add constraint FK_TJ_ZECL_ZSTR_TJ_ZECL_1 
        foreign key (ZECL_ID) 
        references ZECLATEMENTCP_ZECL;

    alter table TJ_ZSTR_ZATT_TJ 
        add constraint FK_TJ_ZSTR_ZATT_TJ_ZATT_1 
        foreign key (ZATT_ID) 
        references ZATTRIBUTIONTGCP_ZATT;

    alter table TJ_ZSTR_ZATT_TJ 
        add constraint FK_TJ_ZSTR_ZATT_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR;

    alter table TJ_ZSTR_ZFON_TJ 
        add constraint FK_TJ_ZSTR_ZFON_TJ_ZSTR_1 
        foreign key (ZSTR_ID) 
        references ZSTRUCTURECP_ZSTR;

    alter table TJ_ZSTR_ZFON_TJ 
        add constraint FK_TJ_ZSTR_ZFON_TJ_ZFON_1 
        foreign key (ZFON_ID) 
        references ZFONCTIONCP_ZFON;

    create index IN_ZCOM_ZDEP_1 on ZCOMMUNE_ZCOM (ZDEP_ID_LESCOMMUNES) 
            TABLESPACE DMO_INDX;

    alter table ZCOMMUNE_ZCOM 
        add constraint FK_ZCOM_ZDEP_1 
        foreign key (ZDEP_ID_LESCOMMUNES) 
        references ZDEPARTEMENT_ZDEP;

    create index IN_ZCOB_ZSTR_1 on ZCOORDBANCAIRECP_ZCOB (ZSTR_ID_LESCOORDBANCAIRESCP)
            TABLESPACE DMO_INDX;

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOB_ZRIB_1 
        foreign key (ZRIB_ID_LERIB) 
        references ZRIB_RIB;

    alter table ZCOORDBANCAIRECP_ZCOB 
        add constraint FK_ZCOB_ZSTR_1 
        foreign key (ZSTR_ID_LESCOORDBANCAIRESCP) 
        references ZSTRUCTURECP_ZSTR;

    create index IN_ZDEP_ZREG_1 on ZDEPARTEMENT_ZDEP (ZREG_ID_LESDEPARTEMENTS)
            TABLESPACE DMO_INDX;

    alter table ZDEPARTEMENT_ZDEP 
        add constraint FK_ZDEP_ZREG_1 
        foreign key (ZREG_ID_LESDEPARTEMENTS) 
        references ZREGION_ZREG;

    create index IN_ZECL_ZSTR_1 on ZECLATEMENTCP_ZECL (ZSTR_ID_LESECLSTRUBENEF)
            TABLESPACE DMO_INDX;

    create index IN_ZECL_1 on ZECLATEMENTCP_ZECL (ZECLATEMENTCP_DIS)
            TABLESPACE DMO_INDX;

    alter table ZECLATEMENTCP_ZECL 
        add constraint FK_ZECL_ZSTR_1 
        foreign key (ZSTR_ID_LESECLSTRUBENEF) 
        references ZSTRUCTURECP_ZSTR;

    create index IN_ZRAT_ZSTR_1 on ZRATTACHEMENTCP_ZRAT (ZSTR_ID_LESRATSTRUABSORBEES)
            TABLESPACE DMO_INDX;

    create index IN_ZDIS_1 on ZRATTACHEMENTCP_ZRAT (ZRATTACHEMENTCP_DIS)
            TABLESPACE DMO_INDX;

    alter table ZRATTACHEMENTCP_ZRAT 
        add constraint FK_ZRAT_ZSTR_1 
        foreign key (ZSTR_ID_LESRATSTRUABSORBEES) 
        references ZSTRUCTURECP_ZSTR;

    create index IN_ZSTR_1 on ZSTRUCTURECP_ZSTR (ZSTRUCTURECP_DIS)
            TABLESPACE DMO_INDX;

    create index IN_ZSTR_ZRAT_1 on ZSTRUCTURECP_ZSTR (ZRAT_ID_LESSTRUABSORBEES)
            TABLESPACE DMO_INDX;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZRAT_1 
        foreign key (ZRAT_ID_LESSTRUABSORBEES) 
        references ZRATTACHEMENTCP_ZRAT;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZMGE_1 
        foreign key (ZSTR_ID_LEMODEGESTION) 
        references ZMODEGESTIONCP_ZMGE;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZADR_1 
        foreign key (ZADR_ID_LADRESSEGEOGRAPHIQUE) 
        references ZADRESSE_ZADR;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZTSD_1 
        foreign key (ZTSD_ID_LETYPESERVICEDIRECTION) 
        references ZTYPESERVICEDIRECTION_ZTSD;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZPIN_1 
        foreign key (ZPIN_ID_LEPAYS) 
        references ZPAYSINSEE_ZPIN;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZCAS_1 
        foreign key (ZCAS_ID_LACATEGORIESTRUCTURECP) 
        references ZCATEGORIESTRUCTURECP_ZCAS;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZADR_2 
        foreign key (ZADR_ID_LADRESSEPOSTALE) 
        references ZADRESSE_ZADR;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZCOM_1 
        foreign key (ZCOM_ID_LACOMMUNE) 
        references ZCOMMUNE_ZCOM;

    alter table ZSTRUCTURECP_ZSTR 
        add constraint FK_ZSTR_ZSTR_1 
        foreign key (ZSTR_ID_LACENTRALCOMPTABLE) 
        references ZSTRUCTURECP_ZSTR;

    alter table ZSTR_LESHORAIRESCP 
        add constraint FK_ZSTR_LESHORAIRESCP_ZSTR_1 
        foreign key (ZSTR_ID_LESHORAIRESCP) 
        references ZSTRUCTURECP_ZSTR;

    alter table ZSTR_LESPOLESACTIV 
        add constraint FK_ZSTR_LESPOLESACTIV_ZSTR_1 
        foreign key (ZSTR_ID_LESPOLESACTIV) 
        references ZSTRUCTURECP_ZSTR;

    alter table ZSTR_LESPOLESACTIV 
        add constraint FK_ZSTR_ZTYP_1 
        foreign key (ZTYP_ID_LETYPEPOLEACTIV) 
        references ZTYPEPOLEACTIVITECP_ZTYP;

    alter table ZSTR_LESSERVICESCP 
        add constraint FK_ZSTR_LESSERVICESCP_ZSTR_1 
        foreign key (ZSTR_ID_LESSERVICESCP) 
        references ZSTRUCTURECP_ZSTR;

    alter table ZSTR_LESSERVICESCP 
        add constraint FK_ZSTR_ZTYS_1 
        foreign key (ZTYS_ID_LETYPESERVICECP) 
        references ZTYPESERVICECP_ZTYS;

    alter table ZTYPESERVICECP_ZTYS 
        add constraint FK_ZTYS_ZDOM_1 
        foreign key (ZTYD_ID_UNDOMAINECP) 
        references ZDOMAINECP_ZDOM;

    create sequence ZADR_ID_SEQUENCE;

    create sequence ZATT_ID_SEQUENCE;

    create sequence ZCAS_ID_SEQUENCE;

    create sequence ZCOB_ID_SEQUENCE;

    create sequence ZCOM_ID_SEQUENCE;

    create sequence ZDEP_ID_SEQUENCE;

    create sequence ZDOM_ID_SEQUENCE;

    create sequence ZECL_ID_SEQUENCE;

    create sequence ZFON_ID_SEQUENCE;

    create sequence ZMGE_ID_SEQUENCE;

    create sequence ZPIN_ID_SEQUENCE;

    create sequence ZRAT_ID_SEQUENCE;

    create sequence ZRIB_ID_SEQUENCE
              START WITH   100000
              INCREMENT BY 1
              MINVALUE     1
              NOMAXVALUE
              CACHE        20
              NOCYCLE
              NOORDER;

    create sequence ZSTR_ID_SEQUENCE;

    create sequence ZTSD_ID_SEQUENCE;

    create sequence ZTYP_ID_SEQUENCE;

    create sequence ZTYS_ID_SEQUENCE;
    
  
