    create table AVISIMPOSITION_AVIS (
        AVISIMPOSITION_ID number(19,0) not null,
        VERSION number(10,0) not null,
        reference varchar2(255 char) not null,
        montant double precision,
        dateRole timestamp,
        TYIM_ID_TYPEIMPOT number(19,0) not null,
        CONT_ID_LISTEAVISIMPOSITION number(19,0),
        CONSTRAINT PK_AVIS primary key (AVISIMPOSITION_ID) USING INDEX TABLESPACE DMO_INDX
    );

    create table CIVILITE_CIVI (
        id number(19,0) not null,
        version number(10,0) not null,
        code number(19,0) not null,
        libelle varchar2(255 char) not null,
        CONSTRAINT PK_CIVI primary key (id) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_CIVI unique (code)
    );

    create table CODEPOSTAL_CPOS (
        id number(19,0) not null,
        version number(10,0) not null,
        code varchar2(255 char) not null,
        ville varchar2(255 char) not null,
        CONSTRAINT PK_CPOS primary key (id) USING INDEX TABLESPACE DMO_INDX
    );

    create table CONTENUFICHIER_COFIC (
        id number(19,0) not null,
        version number(10,0) not null,
        DATA blob,
        CONSTRAINT PK_COFIC primary key (id) USING INDEX TABLESPACE DMO_INDX
    );

    create table CONTRATMENSUALISATION_COME (
        CONTRATMENSU_ID number(19,0) not null,
        VERSION number(10,0) not null,
        NUMEROCONTRAT number(19,0) not null,
        anneePriseEffet number(19,0),
        estTraite number(1,0) not null,
        dateDemande timestamp not null,
        TYPEIMPOT_ID_TYPEIMPOT number(19,0) not null,
        RIB_ID_RIB number(19,0) not null,
        CONT_ID_LISTECONTRATMENSU number(19,0),
        CONSTRAINT PK_COME primary key (CONTRATMENSU_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_COME unique (NUMEROCONTRAT)
    );

    create table CONTRIBUABLE_CONT (
        CONT_ID number(19,0) not null,
        CONTRIBUABLE_DIS varchar2(255 char) not null,
        VERSION number(10,0) not null,
        IDENTIFIANT varchar2(255 char) not null,
        CIVI_ID_CIVILITE number(19,0),
        SIFA_ID_SITUATIONFAMILIALE number(19,0),
        adresseMail varchar2(255 char),
        nom varchar2(255 char),
        prenom varchar2(255 char),
        telephoneFixe varchar2(255 char),
        telephonePortable varchar2(255 char),
        dateDeMiseAJour varchar2(255 char),
        dateDeNaissance timestamp,
        fuseauhorairetel varchar2(255 char),
        solde double precision,
        CONSTRAINT PK_CONT primary key (CONT_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_CONT unique (IDENTIFIANT)
    );

    create table CONT_LISTEADRESSES (
        CONT_ID_LISTEADRESSES number(19,0) not null,
        codePostal varchar2(255 char),
        ligne1 varchar2(255 char),
        ligne2 varchar2(255 char),
        ligne3 varchar2(255 char),
        ligne4 varchar2(255 char),
        pays varchar2(255 char),
        ville varchar2(255 char),
        CONT_ID_LISTEADRESSES_INDEX number(10,0),
        CONSTRAINT PK_LISTEADRESSES primary key (CONT_ID_LISTEADRESSES, CONT_ID_LISTEADRESSES_INDEX) USING INDEX TABLESPACE DMO_INDX
    );

    create table DOCUMENTJOINT_DOC (
        id number(19,0) not null,
        version number(10,0) not null,
        identifiant varchar2(255 char),
        FIC_ID_leFichierJoint number(19,0) not null,
        CONT_ID_LESDOCSJOINTS number(19,0),
        CONT_ID_LESDOCSJOINTS_INDEX number(10,0),
        CONSTRAINT PK_DOC primary key (id) USING INDEX TABLESPACE DMO_INDX
    );

    create table FICHIERJOINT_FIJO (
        id number(19,0) not null,
        version number(10,0) not null,
        dateHeureSoumission timestamp,
        nomFichierOriginal varchar2(255 char),
        tailleFichier number(19,0),
        typeMimeFichier varchar2(255 char) not null,
        COFI_ID_LECONTENUDUFICHIER number(19,0),
        CONSTRAINT PK_FIJO primary key (id) USING INDEX TABLESPACE DMO_INDX
    );


    create table OPERATIONJOURNAL_OPER (
        OPER_ID number(19,0) not null,
        version number(10,0) not null,
        DATEHEUREOPERATION timestamp not null,
        IDENTIFIANTUTILISATEUROUBATCH varchar2(255 char) not null,
        natureOperation varchar2(255 char),
        identifiantStructure varchar2(255 char),
        typeDureeDeConservation varchar2(255 char),
        CONSTRAINT PK_OPER primary key (OPER_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_OPER unique (DATEHEUREOPERATION, IDENTIFIANTUTILISATEUROUBATCH)
    );

    create table PARAMETREOPERATION_PAOP (
        PAOP_ID number(19,0) not null,
        version number(10,0) not null,
        NOM varchar2(255 char) not null,
        VALEUR varchar2(255 char),
        CONSTRAINT PK_PAOP primary key (PAOP_ID) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_PAOP unique (NOM, VALEUR)
    );

    create table PAYS_PAYS (
        PAYS_ID varchar2(255 char) not null,
        CODEISO varchar2(255 char),
        NOM varchar2(255 char) ,
        CONSTRAINT PK_PAYS primary key (PAYS_ID) USING INDEX TABLESPACE DMO_INDX
    );

    create table RIB_RIB (
        id number(19,0) not null,
        version number(10,0) not null,
        cleRib varchar2(255 char),
        ibAN varchar2(255 char),
        estAutomatise number(1,0),
        codeBanque varchar2(255 char) not null,
        codeGuichet varchar2(255 char) not null,
        numeroCompte varchar2(255 char) not null,
        CONSTRAINT PK_RIB primary key (id) USING INDEX TABLESPACE DMO_INDX
    );

    create table SITUATIONFAMI_SIFA (
        id number(19,0) not null,
        version number(10,0) not null,
        code number(19,0) not null,
        libelle varchar2(255 char) not null,
        CONSTRAINT PK_SIFA primary key (id) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_SIFA unique (code)
    );

    create table TJ_OPER_PAOP_TJ (
        OPER_ID number(19,0) not null,
        PAOP_ID number(19,0) not null,
        CONSTRAINT PK_PAOP_TJ primary key (OPER_ID, PAOP_ID) USING INDEX TABLESPACE DMO_INDX
    );

    create table TYPEIMPOT_TYIM (
        id number(19,0) not null,
        version number(10,0) not null,
        codeImpot number(19,0) not null,
        estOuvertALaMensualisation number(1,0),
        estDestineAuxParticuliers number(1,0),
        nomImpot varchar2(255 char) not null,
        jourOuvertureAdhesion timestamp,
        jourLimiteAdhesion timestamp,
        seuil number(19,2),
        taux number(19,10),
        CONSTRAINT PK_TYIM primary key (id) USING INDEX TABLESPACE DMO_INDX,
        CONSTRAINT UN1_TYIM unique (codeImpot)
    );

    create index IN_AVIS_CONT_1 on AVISIMPOSITION_AVIS (CONT_ID_LISTEAVISIMPOSITION)
            TABLESPACE DMO_INDX;

    alter table AVISIMPOSITION_AVIS 
        add constraint FK_AVIS_CONT_1 
        foreign key (CONT_ID_LISTEAVISIMPOSITION) 
        references CONTRIBUABLE_CONT;

    alter table AVISIMPOSITION_AVIS 
        add constraint FK_AVIS_TYIM_1 
        foreign key (TYIM_ID_TYPEIMPOT) 
        references TYPEIMPOT_TYIM;

    create index IN_COME_CONT_1 on CONTRATMENSUALISATION_COME (CONT_ID_LISTECONTRATMENSU)
            TABLESPACE DMO_INDX;

    alter table CONTRATMENSUALISATION_COME 
        add constraint FK_COME_CONT_1 
        foreign key (CONT_ID_LISTECONTRATMENSU) 
        references CONTRIBUABLE_CONT;

    alter table CONTRATMENSUALISATION_COME 
        add constraint FK_COME_TYIM_1 
        foreign key (TYPEIMPOT_ID_TYPEIMPOT) 
        references TYPEIMPOT_TYIM;

    alter table CONTRATMENSUALISATION_COME 
        add constraint FK_COME_RIB_1 
        foreign key (RIB_ID_RIB) 
        references RIB_RIB;

    create index IN_CONT_1 on CONTRIBUABLE_CONT (CONTRIBUABLE_DIS)
        TABLESPACE DMO_INDX;

    alter table CONTRIBUABLE_CONT 
        add constraint FK_CONT_SIFA_1 
        foreign key (SIFA_ID_SITUATIONFAMILIALE) 
        references SITUATIONFAMI_SIFA;

    alter table CONTRIBUABLE_CONT 
        add constraint FK_CONT_CIVI_1 
        foreign key (CIVI_ID_CIVILITE) 
        references CIVILITE_CIVI;

    alter table CONT_LISTEADRESSES 
        add constraint FK_CONT_ID_LISTEADRESSES_1 
        foreign key (CONT_ID_LISTEADRESSES) 
        references CONTRIBUABLE_CONT;

    alter table DOCUMENTJOINT_DOC 
        add constraint FK6C171108270C3B81 
        foreign key (FIC_ID_leFichierJoint) 
        references FICHIERJOINT_FIJO;

    alter table DOCUMENTJOINT_DOC 
        add constraint FK_FIC_CONT_1 
        foreign key (CONT_ID_LESDOCSJOINTS) 
        references CONTRIBUABLE_CONT;

    alter table FICHIERJOINT_FIJO 
        add constraint FK_FIJO_COFI_1 
        foreign key (COFI_ID_LECONTENUDUFICHIER) 
        references CONTENUFICHIER_COFIC;

    alter table TJ_OPER_PAOP_TJ 
        add constraint FK_TJ_OPER_PAOP_TJ_PAOP_1 
        foreign key (PAOP_ID) 
        references PARAMETREOPERATION_PAOP;

    alter table TJ_OPER_PAOP_TJ 
        add constraint FK_TJ_OPER_PAOP_TJ_OPER_1 
        foreign key (OPER_ID) 
        references OPERATIONJOURNAL_OPER;

    create sequence CONTRATMENSU_ID_SEQUENCE
                    START WITH   50000
                    INCREMENT BY 1
                    MINVALUE     1
                    NOMAXVALUE
                    CACHE        20
                    NOCYCLE
                    NOORDER;

    create sequence hibernate_sequence
                       START WITH   50000
                    INCREMENT BY 1
                    MINVALUE     1
                    NOMAXVALUE
                    CACHE        20
                    NOCYCLE
                    NOORDER;
