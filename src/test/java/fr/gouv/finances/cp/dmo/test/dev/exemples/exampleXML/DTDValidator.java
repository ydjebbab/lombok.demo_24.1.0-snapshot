/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : DTDValidator.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exampleXML;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/* Exemple   de validation d'un fichier XML avec DOM et SAX en utilisant un DTD
 *  
 * @amleplatinec-cp  
 */

/**
 * Class DTDValidator DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class DTDValidator
{
    /** Constant : LOAD_EXTERNAL_DTD_FEATURE_ID. */
    protected static final String LOAD_EXTERNAL_DTD_FEATURE_ID =
        "http://apache.org/xml/features/nonvalidating/load-external-dtd";

    /** Constant : FATAL_ERROR. */
    static final String FATAL_ERROR = "http://apache.org/xml/features/continue-after-fatal-error";

    /** Le logger. */
    private final Log log = LogFactory.getLog(this.getClass());

    /** La réponse de la validation. */
    private String response = null;

    /** résultat de validation *. */
    private boolean isValid = false;

    /**
     * Verifie si valid.
     * 
     * @return true, si c'est valid
     */
    public boolean isValid()
    {
        return isValid;
    }

    // -------------------------------------------------------------------------------------------------------------------------
    // Premier cas : la DTD est celle spécifiée dans le fichier xml
    // et est située au même endroit physique que le fichier xml

    /**
     * on passe juste le nom du fichier xml (avec son chemin ) en entree méthode principale qui fait la validation.
     * 
     * @param xmlFile param
     * @return the string
     */
    public String validerXMLParRapportDTD(String xmlFile)
    {

        Validator handler = new Validator();

        try
        {
            // System.setProperty(DOM_FACTORY_NAME, DOM_FACTORY_IMPL);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setExpandEntityReferences(false);

            factory.setValidating(true);
            // factory.setAttribute(CONTINUE_AFTER_FATAL_ERROR, Boolean.TRUE);

            DocumentBuilder builder = factory.newDocumentBuilder();

            builder.setErrorHandler(handler);
            builder.parse(xmlFile);

            isValid = !handler.validationError;
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
                return response;
            }
        }
        catch (ParserConfigurationException e)
        {
            log.debug("ParserConfigurationException détectée");
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuffer sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuffer(response);
            }
            else
            {
                sbu = new StringBuffer("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(e.getMessage());
            return sbu.toString();

        }
        catch (IOException e)
        {
            log.debug("IOException détectée");
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuffer sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuffer(response);
            }
            else
            {
                sbu = new StringBuffer("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(e.getMessage());
            return sbu.toString();

        }
        catch (SAXException e)
        {
            log.debug("SAXException détectée");
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuffer sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuffer(response);
            }
            else
            {
                sbu = new StringBuffer("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(e.getMessage());
            return sbu.toString();

        }

        return response;
    }

    // ------------------------------------------------------------------------------------------------------------------------------------------------

    // Second cas : la DTD n'estpas forcément celle spécifiée dans le fichier xml
    // ou n' est située au même endroit physique que le fichier xml

    /**
     * on passe juste les noms de fichiers en entree méthode principale qui fait la validation.
     * 
     * @param xmlFile param
     * @param dtdFile param
     * @return the string
     */
    public String validerXMLParRapportDTDNouvelle(String xmlFile, String dtdFile)
    {

        Validator handler = new Validator();

        try
        {
            // System.setProperty(DOM_FACTORY_NAME, DOM_FACTORY_IMPL);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setExpandEntityReferences(false);

            factory.setValidating(false);
            // factory.setAttribute(CONTINUE_AFTER_FATAL_ERROR, Boolean.TRUE);

            /* attribut qui permet d'ignorer la dtd externe présente dans le fichier */

            factory.setAttribute(LOAD_EXTERNAL_DTD_FEATURE_ID, Boolean.FALSE);

            DocumentBuilder builder = factory.newDocumentBuilder();

            /* on affecte la nouvelle dtd */

            builder.setEntityResolver(new DTDResolver(dtdFile));

            builder.setErrorHandler(handler);
            builder.parse(xmlFile);

            isValid = !handler.validationError;
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
                return response;
            }
        }
        catch (ParserConfigurationException e)
        {
            log.debug("ParserConfigurationException détectée");
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuffer sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuffer(response);
            }
            else
            {
                sbu = new StringBuffer("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(e.getMessage());
            return sbu.toString();

        }
        catch (IOException e)
        {
            log.debug("IOException détectée");
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuffer sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuffer(response);
            }
            else
            {
                sbu = new StringBuffer("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(e.getMessage());
            return sbu.toString();

        }

        catch (IllegalArgumentException e)
        {
            log.debug("IllegalArgumentException détectée");
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuffer sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuffer(response);
            }
            else
            {
                sbu = new StringBuffer("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(e.getMessage());
            return sbu.toString();

        }
        catch (SAXException e)
        {
            log.debug("SAXException détectée");
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuffer sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuffer(response);
            }
            else
            {
                sbu = new StringBuffer("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(e.getMessage());
            return sbu.toString();

        }

        return response;
    }

    /**
     * on implémente un nouveau Entity resolver pour remplacer la dtd présente dans le fichier initial par une autre
     * valeur.
     * 
     * @author amleplatinec-cp
     */
    private class DTDResolver implements EntityResolver
    {
        // constructor
        /**
         * Instanciation de dTD resolver.
         * 
         * @param DTDfile param
         */
        public DTDResolver(String DTDfile)
        {
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see org.xml.sax.EntityResolver#resolveEntity(java.lang.String, java.lang.String)
         */
        public InputSource resolveEntity(java.lang.String publicId, java.lang.String systemId) throws SAXException,
            java.io.IOException
        {
            InputSource is = null;
            return is;
        }

    }

    // -----------------------------------------------------------------------------------------------------------------------------------------------------

    // Méthodes communes pour récupérer les erreurs

    /**
     * Surcharge validateur pour récuperer les erreurs.
     * 
     * @author amleplatinec-cp
     */
    private class Validator extends DefaultHandler
    {

        /** Memo erreur validation. */
        public boolean validationError = false;

        /** Memo Exception(s) sax. */
        public ArrayList<SAXParseException> saxParseExceptionList = new ArrayList<SAXParseException>();

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see org.xml.sax.helpers.DefaultHandler#error(org.xml.sax.SAXParseException)
         */
        public void error(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see org.xml.sax.helpers.DefaultHandler#fatalError(org.xml.sax.SAXParseException)
         */
        public void fatalError(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see org.xml.sax.helpers.DefaultHandler#warning(org.xml.sax.SAXParseException)
         */
        public void warning(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }
    }

    /**
     * mise sous une forme string de la réponse.
     * 
     * @param saxParseExceptionList param
     * @return the string
     */
    private String calculerReponse(ArrayList<SAXParseException> saxParseExceptionList)
    {
        StringBuffer sbu = new StringBuffer();
        sbu.append("les erreurs suivantes ont ete detectees");
        for (int i = 0; i < saxParseExceptionList.size(); i++)
        {
            SAXParseException xcpt = (SAXParseException) saxParseExceptionList.get(i);
            sbu.append("\n  ").append(xcpt.getLineNumber());
            sbu.append(":").append(xcpt.getColumnNumber());
            sbu.append(" *** ").append(xcpt.getMessage());
        }
        return sbu.toString();
    }

}
