/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementChargementGuichetsEtEtablissementsTabankTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.batch;

import static org.mockito.Mockito.verify;

import org.junit.Test;

import fr.gouv.finances.lombok.util.base.LombokBatchTestSansSpring;

/**
 * Class TraitementChargementGuichetsEtEtablissementsTabankTest.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 14 déc. 2009
 */


public class TraitementChargementGuichetsEtEtablissementsTabankTest extends LombokBatchTestSansSpring
{

    /**
     * methode Test traiter batch : DGFiP.
     */
    @Test
    public void testTraiterBatch()
    {
        testBatch(new String[] {"traitementchargementguichetsetetablissementstabank"});

        // vérification que le test fini sans erreur
        verify(systemExit).exit(0);

      

    }

}