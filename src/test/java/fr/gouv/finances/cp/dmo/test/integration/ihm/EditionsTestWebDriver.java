/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.test.integration.ihm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Classe de test comportant une méthode et une bonne configuration.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class EditionsTestWebDriver
{

    /**
     * méthode de Test : .
     */
    @Test
    public void testUntitled()
    {
        /*driver.get("identification.ex");

        WebElement login = driver.findElement(By.id("username"));
        login.sendKeys("chouard-cp");
        WebElement pwd = driver.findElement(By.id("password"));
        pwd.sendKeys("0");

        pwd.submit();

        (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>()
        {
            public Boolean apply(WebDriver d)
            {
                return (d.getTitle() != null);
            }
        });

        // WebElement menu = driver.findElement(By.id("Editions"));
        // WebElement menu = driver.findElement(By.linkText("Editions"));
        // (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>()
        // {
        // public Boolean apply(WebDriver d) {
        // return (d.findElement(By.cssSelector("nav div.menu > ul")) != null);
        // }
        // });

        // WebElement sousmenu = driver.findElement(By.id("Bannettedeséditions"));

        // Actions builder = new Actions(driver);
        // builder.moveToElement(menu).build().perform();

        driver.quit();*/

    }
}
