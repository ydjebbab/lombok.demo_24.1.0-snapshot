/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : PresetDatabaseDaoTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.cp.dmo.dao.IAdresseContribuableDao;
import fr.gouv.finances.cp.dmo.dao.IContratMensuDao;
import fr.gouv.finances.cp.dmo.dao.IContribuableDao;
import fr.gouv.finances.cp.dmo.dao.IReferenceDao;
import fr.gouv.finances.cp.dmo.entite.AdresseContribuable;
import fr.gouv.finances.cp.dmo.entite.AdresseTest;
import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.Civilite;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuableEnt;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.PersonneTest;
import fr.gouv.finances.cp.dmo.entite.Rib;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.service.ITestAdressesPersonnesService;
import fr.gouv.finances.lombok.util.base.LombokTransactionDAOTest;

/**
 * Class PresetDatabaseDaoTest 
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@LombokTransactionDAOTest
public class PresetDatabaseDaoTest //extends BaseDAOTestCase
{
    
    private static final Logger log = LoggerFactory.getLogger(PresetDatabaseDaoTest.class);

    /** Constant : c200. */
    private static final int c200 = 200;

    /** referencedao. */
    @Autowired
    protected IReferenceDao referencedao = null;

    /** contribuabledao. */
    @Autowired
    protected IContribuableDao contribuabledao = null;

    /** contratmensudao. */
    @Autowired
    protected IContratMensuDao contratmensudao = null;

    /** adressecontribuabledao. */
    @Autowired
    protected IAdresseContribuableDao adressecontribuabledao = null;

    /** testadressespersonnesserviceso. */
    @Autowired
    protected ITestAdressesPersonnesService testadressespersonnesserviceso;

    /** untypeimpot. */
    private TypeImpot untypeimpot;

    /** uncontribuablepar. */
    private ContribuablePar uncontribuablepar;

    /** unrib. */
    private Rib unrib;

    /** uncontratmensu. */
    private ContratMensu uncontratmensu;

    /** unavisimposition. */
    private AvisImposition unavisimposition;

    /** unereference. */
    private String unereference;

    /** uncontribuableent. */
    private ContribuableEnt uncontribuableent;

    /**
     * methode Testchargedonnesbase 
     * 
     * @throws Exception the exception
     */
 // Evite de faire un rollback en fin de test (comportement par défaut)
    @Test
    @Rollback(false)
    public void testchargedonnesbase() throws Exception
    {
        log.debug(">>> Debut  testchargedonnesbase() ");
        // Données de référence : TypeImpot

        //this.endTransaction();
        String datedebuts = "01/10/2005";
        SimpleDateFormat sdfdebut = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
        Date datedebut = sdfdebut.parse(datedebuts);

        String datefins = "01/10/2006";
        SimpleDateFormat sdffin = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
        Date datefin = sdffin.parse(datefins);

        untypeimpot = referencedao.findTypeImpotParCodeImpot(Long.valueOf(1));

        if (untypeimpot == null)
        {
            untypeimpot = new TypeImpot();

            untypeimpot.setCodeImpot(Long.valueOf(1));
            untypeimpot.setNomImpot("Impôt sur le revenu");
            untypeimpot.setEstOuvertALaMensualisation(Boolean.TRUE);
            untypeimpot.setEstDestineAuxParticuliers(Boolean.TRUE);
            untypeimpot.setJourOuvertureAdhesion(datedebut);
            untypeimpot.setJourLimiteAdhesion(datefin);
            untypeimpot.setSeuil(new BigDecimal("300245.12"));
            untypeimpot.setTaux(new BigDecimal("0.1234567895"));
            referencedao.saveTypeImpot(untypeimpot);
        }

        untypeimpot = referencedao.findTypeImpotParCodeImpot(Long.valueOf(2));
        if (untypeimpot == null)
        {
            untypeimpot = new TypeImpot();

            untypeimpot.setCodeImpot(Long.valueOf(2));
            untypeimpot.setNomImpot("Taxe foncières");
            untypeimpot.setEstOuvertALaMensualisation(Boolean.TRUE);
            untypeimpot.setEstDestineAuxParticuliers(Boolean.TRUE);
            untypeimpot.setJourOuvertureAdhesion(datedebut);
            untypeimpot.setJourLimiteAdhesion(datefin);
            untypeimpot.setSeuil(new BigDecimal("999999.99"));
            untypeimpot.setTaux(new BigDecimal("0.8888888888"));
            referencedao.saveTypeImpot(untypeimpot);
        }

        untypeimpot = referencedao.findTypeImpotParCodeImpot(Long.valueOf(3));
        if (untypeimpot == null)
        {
            untypeimpot = new TypeImpot();

            untypeimpot.setCodeImpot(Long.valueOf(3));
            untypeimpot.setNomImpot("Taxe d'habitation");
            untypeimpot.setEstOuvertALaMensualisation(Boolean.TRUE);
            untypeimpot.setEstDestineAuxParticuliers(Boolean.TRUE);
            untypeimpot.setJourOuvertureAdhesion(datedebut);
            untypeimpot.setJourLimiteAdhesion(datefin);
            untypeimpot.setSeuil(new BigDecimal("777777.77"));
            untypeimpot.setTaux(new BigDecimal("0.7777777777"));
            referencedao.saveTypeImpot(untypeimpot);
        }

        untypeimpot = referencedao.findTypeImpotParCodeImpot(Long.valueOf(4));
        if (untypeimpot == null)
        {
            untypeimpot = new TypeImpot();

            untypeimpot.setCodeImpot(Long.valueOf(4));
            untypeimpot.setNomImpot("Taxe professionnelle");
            untypeimpot.setEstOuvertALaMensualisation(Boolean.TRUE);
            untypeimpot.setEstDestineAuxParticuliers(Boolean.TRUE);
            untypeimpot.setJourOuvertureAdhesion(datedebut);
            untypeimpot.setJourLimiteAdhesion(datefin);
            untypeimpot.setSeuil(new BigDecimal("66666666.66"));
            untypeimpot.setTaux(new BigDecimal("0.6666666666"));
            referencedao.saveTypeImpot(untypeimpot);
        }

        /*
         * untypeimpot = referencedao.findTypeImpotParCodeImpot(Long.valueOf(5)); if (untypeimpot == null) { untypeimpot
         * = new TypeImpot(); untypeimpot.setCodeImpot(Long.valueOf(5));
         * untypeimpot.setNomImpot("Taxe locale sur les salaires");
         * untypeimpot.setEstOuvertALaMensualisation(Boolean.FALSE);
         * untypeimpot.setEstDestineAuxParticuliers(Boolean.FALSE); untypeimpot.setJourOuvertureAdhesion(datedebut);
         * untypeimpot.setJourLimiteAdhesion(datefin); untypeimpot.setSeuil(new BigDecimal("555555.55"));
         * untypeimpot.setTaux(new BigDecimal("0.5555555555")); referencedao.saveTypeImpot(untypeimpot); }
         */

        // ContribuablePar
        // AvisImposition
        List<String> listereference = new ArrayList<String>();
        listereference.add("1111111111");
        listereference.add("2222222222");
        listereference.add("3333333333");
        listereference.add("4444444444");

        List<Contribuable> listecontribuable = new ArrayList<Contribuable>();

        for (int i = c200; i < 2000; i++)
        {
            String nom = "userent" + i;
            String uid = nom + "-cp";
            uncontribuableent = contribuabledao.findContribuableEntrepriseParUID(uid);
            if (uncontribuableent == null)
            {
                uncontribuableent = new ContribuableEnt();
                uncontribuableent.setIdentifiant(uid);
                uncontribuableent.setNom(nom);
                uncontribuableent.setSolde(new Double(i * 1000));
                uncontribuableent.setTelephoneFixe("00 11 22 33 44");
            }
            contribuabledao.saveObject(uncontribuableent);
        }

        Civilite civilite1 = referencedao.findCiviliteParCode(Long.valueOf(1));
        Civilite civilite2 = referencedao.findCiviliteParCode(Long.valueOf(2));
        Civilite civilite3 = referencedao.findCiviliteParCode(Long.valueOf(3));

        for (int i = c200; i < 2000; i++)
        {
            String uid = "usertest" + i + "-cp";
            uncontribuablepar = contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID(uid);
            if (uncontribuablepar == null)
            {
                uncontribuablepar = new ContribuablePar();
                uncontribuablepar.setIdentifiant(uid);
                uncontribuablepar.setNom("nom" + i);
                uncontribuablepar.setPrenom("prenom" + i);
                uncontribuablepar.setSolde(new Double("125325483.12"));
                if (i % 3 == 0)
                {
                    uncontribuablepar.setCivilite(civilite1);

                    SimpleTimeZone atz =
                        new SimpleTimeZone(3600000, "Europe/Paris", Calendar.MARCH, -1, Calendar.SUNDAY, 3600000,
                            SimpleTimeZone.UTC_TIME, Calendar.OCTOBER, -1, Calendar.SUNDAY, 3600000,
                            SimpleTimeZone.UTC_TIME, 3600000);

                    uncontribuablepar.setFuseauhorairetel(atz);

                    String datenaisannce = "01/10/1960";
                    SimpleDateFormat sdfdatenaisannce = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
                    Date dateDeNaissance = sdfdatenaisannce.parse(datenaisannce);

                    uncontribuablepar.setDateDeNaissance(dateDeNaissance);

                }
                else if (i % 2 == 0)
                {
                    uncontribuablepar.setCivilite(civilite2);
                    uncontribuablepar.setFuseauhorairetel(TimeZone.getTimeZone("America/Chicago"));
                    String datenaisannce = "01/10/2010";
                    SimpleDateFormat sdfdatenaisannce = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
                    Date dateDeNaissance = sdfdatenaisannce.parse(datenaisannce);

                    uncontribuablepar.setDateDeNaissance(dateDeNaissance);
                }
                else
                {
                    uncontribuablepar.setCivilite(civilite3);
                    uncontribuablepar.setFuseauhorairetel(TimeZone.getTimeZone("Pacific/Honolulu"));
                    String datenaisannce = "01/10/1950";
                    SimpleDateFormat sdfdatenaisannce = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);
                    Date dateDeNaissance = sdfdatenaisannce.parse(datenaisannce);

                    uncontribuablepar.setDateDeNaissance(dateDeNaissance);
                }

                uncontribuablepar.setTelephoneFixe("0000000000");
                uncontribuablepar.setTelephonePortable("0000000000");
                uncontribuablepar.setAdresseMail("usertest" + i + "@cp.finances.gouv.fr");
                AdresseContribuable uneAdressePrincipale = new AdresseContribuable();

                uneAdressePrincipale.setCodePostal("93100");
                uneAdressePrincipale.setLigne1("3eme étage - escalier C");
                uneAdressePrincipale.setLigne2("Immeuble D3");
                uneAdressePrincipale.setLigne3("RUE AUGUSTE BLANQUI");
                uneAdressePrincipale.setLigne4("BP18");
                uneAdressePrincipale.setVille("MONTREUIL"); // a supprimer

                uneAdressePrincipale.setPays("FRANCE");

                uncontribuablepar.rattacherUneNouvelleAdresse(uneAdressePrincipale);
                contribuabledao.saveContribuableParticulier(uncontribuablepar);
            }
            listecontribuable.add(uncontribuablepar);
        }

        uncontribuablepar =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("wpetit-cp");
        if (uncontribuablepar == null)
        {
            uncontribuablepar = new ContribuablePar();
            uncontribuablepar.setIdentifiant("wpetit-cp");
            uncontribuablepar.setAdresseMail("wenceslas.petit@cp.finances.gouv.fr");
            uncontribuablepar.setCivilite(civilite1);
            uncontribuablepar.setFuseauhorairetel(TimeZone.getTimeZone("Europe/Paris"));
            contribuabledao.saveContribuableParticulier(uncontribuablepar);
        }
        listecontribuable.add(uncontribuablepar);

        uncontribuablepar =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("chouard-cp");
        if (uncontribuablepar == null)
        {
            uncontribuablepar = new ContribuablePar();
            uncontribuablepar.setIdentifiant("chouard-cp");
            uncontribuablepar.setCivilite(civilite1);
            uncontribuablepar.setFuseauhorairetel(TimeZone.getTimeZone("Europe/Paris"));
            contribuabledao.saveContribuableParticulier(uncontribuablepar);
        }
        listecontribuable.add(uncontribuablepar);

        uncontribuablepar =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("lcontinsouzas-cp");
        if (uncontribuablepar == null)
        {
            uncontribuablepar = new ContribuablePar();
            uncontribuablepar.setIdentifiant("lcontinsouzas-cp");
            uncontribuablepar.setCivilite(civilite1);
            uncontribuablepar.setFuseauhorairetel(TimeZone.getTimeZone("Europe/Paris"));
            contribuabledao.saveContribuableParticulier(uncontribuablepar);
        }
        listecontribuable.add(uncontribuablepar);

        uncontribuablepar =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("amleplatinec-cp");
        if (uncontribuablepar == null)
        {
            uncontribuablepar = new ContribuablePar();
            uncontribuablepar.setIdentifiant("amleplatinec-cp");
            uncontribuablepar.setCivilite(civilite1);
            uncontribuablepar.setFuseauhorairetel(TimeZone.getTimeZone("Europe/Paris"));
            contribuabledao.saveContribuableParticulier(uncontribuablepar);
        }
        listecontribuable.add(uncontribuablepar);

        uncontribuablepar =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("pbeugniez-cp");
        if (uncontribuablepar == null)
        {
            uncontribuablepar = new ContribuablePar();
            uncontribuablepar.setIdentifiant("pbeugniez-cp");
            uncontribuablepar.setCivilite(civilite1);
            uncontribuablepar.setFuseauhorairetel(TimeZone.getTimeZone("Europe/Paris"));
            contribuabledao.saveContribuableParticulier(uncontribuablepar);
        }
        listecontribuable.add(uncontribuablepar);

        Random rand = new Random();
        int maxmontant = 10000;
        Date daterole;

        for (int j = 0; j < listecontribuable.size(); j++)
        {
            for (int i = 0; i < listereference.size(); i++)
            {
                uncontribuablepar =
                    contribuabledao
                        .findContribuableEtAvisImpositionEtContratsMensualisationParUID(((ContribuablePar) listecontribuable
                            .get(j)).getIdentifiant());
                unereference = j + (listereference.get(i));
                unavisimposition = contribuabledao.findAvisImpositionParReference(unereference);

                if (unavisimposition == null)
                {
                    unavisimposition = new AvisImposition();
                    unavisimposition.setContribuablePar(uncontribuablepar);
                    unavisimposition.setReference(unereference);

                    unavisimposition.setTypeImpot(referencedao.findTypeImpotParCodeImpot(Long.valueOf((i % 4) + 1)));

                    uncontribuablepar.rattacherUnNouveauAvisImposition(unavisimposition);

                    // Génération d'un montant aléatoire
                    double rrr = (rand.nextDouble()) * maxmontant;
                    int decimalPlace = 2;
                    BigDecimal bdd = new BigDecimal(rrr);

                    bdd = bdd.setScale(decimalPlace, BigDecimal.ROUND_UP);
                    rrr = bdd.doubleValue();
                    unavisimposition.setMontant(new Double(rrr));

                    // Génération d'une date aléatoire
                    int jour = rand.nextInt(27) + 1;
                    int mois = rand.nextInt(11) + 1;
                    String s = jour + "/" + mois + "/2005";
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.FRENCH);

                    daterole = sdf.parse(s);

                    unavisimposition.setDateRole(daterole);

                    // contribuabledao.deleteObject(unavisimposition);
                    contribuabledao.saveObject(unavisimposition);
                }
            }
        }

        // ContratMensu
        uncontribuablepar =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("wpetit-cp");
        untypeimpot = referencedao.findTypeImpotParCodeImpot(Long.valueOf(1));

        // Si aucune demande de mensualisation n'existe pour ce type d'impôt, on
        // crée une demande
        uncontratmensu = contratmensudao.findContratpourContribuableEtTypeImpot(uncontribuablepar, untypeimpot);

        if (uncontratmensu == null)
        {
            // Rib de test
            unrib = new Rib();
            unrib.setCodeBanque("30002");
            unrib.setCodeGuichet("00658");
            unrib.setNumeroCompte("11111111111");
            unrib.setCleRib("93");

            uncontratmensu = new ContratMensu();
            uncontratmensu.setRib(unrib);
            uncontratmensu.setAnneePriseEffet(Long.valueOf(2005));
            uncontratmensu.setContribuablePar(uncontribuablepar);
            uncontratmensu.setTypeImpot(untypeimpot);

            Set<AvisImposition> listavis = uncontribuablepar.getListeAvisImposition();
            AvisImposition unavis = (AvisImposition) listavis.toArray()[0];

            uncontratmensu.setNumeroContrat(Long.valueOf(unavis.getReference()));
            uncontribuablepar.rattacherUnNouveauContratMensu(uncontratmensu);

            contribuabledao.saveObject(uncontribuablepar);
        }

        uncontribuablepar =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("chouard-cp");
        untypeimpot = referencedao.findTypeImpotParCodeImpot(Long.valueOf(1));

        // Si aucune demande de mensualisation n'existe pour ce type d'impôt, on
        // crée une demande
        uncontratmensu = contratmensudao.findContratpourContribuableEtTypeImpot(uncontribuablepar, untypeimpot);

        if (uncontratmensu == null)
        {
            // Rib de test
            unrib = new Rib();
            unrib.setCodeBanque("10037");
            unrib.setCodeGuichet("00625");
            unrib.setNumeroCompte("11111111111");
            unrib.setCleRib("45");

            uncontratmensu = new ContratMensu();
            uncontratmensu.setRib(unrib);
            uncontratmensu.setContribuablePar(uncontribuablepar);
            uncontratmensu.setTypeImpot(untypeimpot);
            uncontratmensu.setAnneePriseEffet(Long.valueOf(2005));

            Set<AvisImposition> listavis = uncontribuablepar.getListeAvisImposition();
            AvisImposition unavis = (AvisImposition) listavis.toArray()[0];

            uncontratmensu.setNumeroContrat(Long.valueOf(unavis.getReference()));
            uncontribuablepar.rattacherUnNouveauContratMensu(uncontratmensu);
            contribuabledao.saveObject(uncontribuablepar);
        }

        uncontribuablepar =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("lcontinsouzas-cp");
        untypeimpot = referencedao.findTypeImpotParCodeImpot(Long.valueOf(1));

        uncontratmensu = contratmensudao.findContratpourContribuableEtTypeImpot(uncontribuablepar, untypeimpot);

        if (uncontratmensu == null)
        {
            unrib = new Rib();
            unrib.setCodeBanque("30081");
            unrib.setCodeGuichet("75000");
            unrib.setNumeroCompte("00000007002");
            unrib.setCleRib("40");

            uncontratmensu = new ContratMensu();
            uncontratmensu.setRib(unrib);
            uncontratmensu.setContribuablePar(uncontribuablepar);
            uncontratmensu.setTypeImpot(untypeimpot);
            uncontratmensu.setAnneePriseEffet(Long.valueOf(2005));
            Set<AvisImposition> listavis = uncontribuablepar.getListeAvisImposition();
            AvisImposition unavis = (AvisImposition) listavis.toArray()[0];

            uncontratmensu.setNumeroContrat(Long.valueOf(unavis.getReference()));

            uncontribuablepar.rattacherUnNouveauContratMensu(uncontratmensu);

            contribuabledao.saveObject(uncontribuablepar);
        }
        
        
        ajoutDePersonnesEtAdressesTest();
        
        log.debug(">>> fin  testchargedonnesbase() ");
    }

    private void ajoutDePersonnesEtAdressesTest()
    {
        int nbPersonneACreer = 40;
      
        AdresseTest adressePartagee1 = creerUneAdressePartagee(1);
        AdresseTest adressePartagee2 = creerUneAdressePartagee(2);
        
        /* Création des adresses et rattachement des personnes */
        for (int indicePersonne = 0; indicePersonne < nbPersonneACreer; indicePersonne++)
        {
            PersonneTest personneTest = ajouterUnePersonneTest(indicePersonne);

            /* Deux adresses non partagées par personne */
            for (int indiceNumAdressePourLaPersonne = 0; indiceNumAdressePourLaPersonne < 3; indiceNumAdressePourLaPersonne++)
            {
                AdresseTest adresseTestNonPartagee = creerUneAdresseNonPartagee(indicePersonne, indiceNumAdressePourLaPersonne);
                ajouterUneAdresseAUnePersonne(personneTest, adresseTestNonPartagee);
            }
            
            /* Adresse 1 partagée par la première et la dernière personne crée */
            if (indicePersonne == 0 || indicePersonne == nbPersonneACreer -1)
            {
                testadressespersonnesserviceso.ajouterAdresseALaPersonne(personneTest, adressePartagee1);
            }
            
            /* Adresse 2 partagée par les personnes 10 à 15 */
            if (indicePersonne >=9  && indicePersonne < 15)
            {
                testadressespersonnesserviceso.ajouterAdresseALaPersonne(personneTest, adressePartagee2);
            }           
        }
        /*
         * Création d'adresses tests qui seront affectées à une seule personne 2 adresses par personne
         */        
    }

    
    private String constructionSuffixeAdressePartagee(int indiceAdressePartagee)
    {
        StringBuilder suffixe = new StringBuilder();        
        suffixe.append("_partagee_");     
        suffixe.append(indiceAdressePartagee);
        return suffixe.toString();
    }
    
    private String constructionSuffixeAdresseNonPartagee(int indicePersonne, int indiceNumAdressePourLaPersonne)
    {
        StringBuilder suffixe = new StringBuilder();
        suffixe.append("_");
        suffixe.append(indicePersonne);
        suffixe.append("_");
        suffixe.append(indiceNumAdressePourLaPersonne);

        return suffixe.toString();
    }

    private AdresseTest constructionAdresseTestModele(String suffixe)
    {
        AdresseTest adresseTest = new AdresseTest();
        String prefixvoie = "voie";
        String prefixville = "ville";

        StringBuilder voie = new StringBuilder();
        voie.append(prefixvoie);
        voie.append(suffixe);

        StringBuilder ville = new StringBuilder();
        ville.append(prefixville);
        ville.append(suffixe);
        adresseTest.setVoie(voie.toString());
        adresseTest.setVille(ville.toString());
        
        return adresseTest;
    }

    private void ajouterUneAdresseAUnePersonne(PersonneTest personneTest, AdresseTest adresseTest)
    {
        testadressespersonnesserviceso.ajouterAdresseALaPersonne(personneTest, adresseTest);
    }
    
    private AdresseTest creerUneAdressePartagee( int indiceAdressePartagee)
    {
        String suffixe = constructionSuffixeAdressePartagee(indiceAdressePartagee);
        AdresseTest adresseTestModele = constructionAdresseTestModele(suffixe);        
        AdresseTest adresseTest = creerUneAdresseTest(adresseTestModele);;
        
        return adresseTest;
    }
    
    private AdresseTest creerUneAdresseNonPartagee(int indicePersonne, int indiceNumAdressePourLaPersonne)
    {
        String suffixe = constructionSuffixeAdresseNonPartagee(indicePersonne, indiceNumAdressePourLaPersonne);
        AdresseTest adresseTestModele = constructionAdresseTestModele(suffixe);        
        AdresseTest adresseTest = creerUneAdresseTest(adresseTestModele);;
        
        return adresseTest;
    }

    private AdresseTest creerUneAdresseTest(AdresseTest adresseTestModele)
    {

        AdresseTest adresseTest = testadressespersonnesserviceso
            .rechercherAdresseEtPersonnesParVoieEtVille(adresseTestModele.getVoie().toString(), adresseTestModele.getVille().toString());

        if (adresseTest == null)
        {
            adresseTest = new AdresseTest();
            adresseTest.setVoie(adresseTestModele.getVoie().toString());
            adresseTest.setVille(adresseTestModele.getVille().toString());
            testadressespersonnesserviceso.sauvegarderAdresse(adresseTest);
        }
        return adresseTest;
    }

    private PersonneTest ajouterUnePersonneTest(int indice)
    {
        PersonneTest personneTest = null;

        String prefixnom = "nom";
        String prefixprenom = "prenom";

        StringBuilder nom = new StringBuilder();
        nom.append(prefixnom);
        nom.append(indice);

        StringBuilder prenom = new StringBuilder();
        prenom.append(prefixprenom);
        prenom.append(indice);

        personneTest = testadressespersonnesserviceso.rechercherPersonneEtAdressesParNom(nom.toString(), prenom.toString());

        if (personneTest == null)
        {
            personneTest = new PersonneTest();
            personneTest.setNom(nom.toString());
            personneTest.setPrenom(prenom.toString());
            testadressespersonnesserviceso.sauvegarderPersonne(personneTest);
        }

        return personneTest;
    }

}
