/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ExempleConversionEncoding.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

import junit.framework.TestCase;
import fr.gouv.finances.cp.dmo.test.integration.util.FichierUtil;

/**
 * Class ExempleConversionEncoding DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ExempleConversionEncoding extends TestCase
{

    /** Constant : REPBASE. */
    private static final String REPBASE =
        FichierUtil.recupererClassPath() + "/fr/gouv/finances/cp/dmo/test/dev/exemples/exemplesdecode/";

    /**
     * methode Test encoding : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testEncoding() throws Exception
    {

        OutputStreamWriter outsw1 = new OutputStreamWriter(new ByteArrayOutputStream());
        System.out.println("encoding par defaut :" + outsw1.getEncoding());

        FileInputStream fis = new FileInputStream(REPBASE + "testwin1252.txt");
        InputStreamReader isr = new InputStreamReader(fis, "Cp1252");
        BufferedReader br = new BufferedReader(isr);
        String text = br.readLine();

        FileOutputStream fos = new FileOutputStream(REPBASE + "testutf8.txt");
        Writer outsw2 = new OutputStreamWriter(fos, "UTF8");
        
        outsw2.write(text);
        outsw2.close();

    }
}
