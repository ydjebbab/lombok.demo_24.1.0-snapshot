/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ExemplesFormatageDeMontants.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class ExemplesFormatageDeMontants DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ExemplesFormatageDeMontants extends TestCase
{

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * methode Test replace text in byte table : DGFiP.
     */
    public void testReplaceTextInByteTable()
    {
        byte[] tbyte;
        String textasupprimer = "todo";

        String content = "todoffffffffffffffffffffftodofffffftodofff";
        tbyte = content.getBytes();

        tbyte = (new String(tbyte)).replaceAll(textasupprimer, "").getBytes();

        //System.out.println("a = " + new String(tbyte));
    }

    /**
     * methode Test formater un montant : DGFiP.
     */
    public void testFormaterUnMontant()
    {
        // Print out a number using the localized number, integer, currency,
        // and percent format for each locale
        // Locale[] locales = NumberFormat.getAvailableLocales();
        // double myNumber = -1234.56;
        // NumberFormat form;
        Locale locale;
        // String format ;
        String format1;
        format1 = "#\u00A0##0,00";
        double value = 11125.12;
        // format = "# ##0,00";
        locale = Locale.FRANCE;
        NumberFormat nf = NumberFormat.getNumberInstance(locale);
        DecimalFormat df = (DecimalFormat) nf;
        // df.applyLocalizedPattern(format);
        // System.out.println(df.format(value));
        df.applyLocalizedPattern(format1);
        // log.info("valeur formatée = " + df.format(value));

        DecimalFormatSymbols dformat = new DecimalFormatSymbols(locale);
        //log.info(dformat.getGroupingSeparator());
        char monchar = dformat.getGroupingSeparator();
        //log.info("grouping = " + Integer.toHexString(monchar));

        monchar = dformat.getPercent();
        //log.info("percent  = " + Integer.toHexString(monchar));

        monchar = dformat.getDecimalSeparator();
        //log.info("digital sep  = " + Integer.toHexString(monchar));

        monchar = dformat.getDigit();
        //log.info("digit  = " + Integer.toHexString(monchar));

        monchar = dformat.getZeroDigit();
        //log.info("zerodigit  = " + Integer.toHexString(monchar));

        String mon = dformat.getCurrencySymbol();

        //log.info("Curr 0  = " + Integer.toHexString(mon.charAt(0)));

        /*
         * System.out.println( "Default format pattern: " + ((DecimalFormat)formatter).toPattern() );
         * System.out.println( "Default format pattern: " + ((DecimalFormat)formatter).toLocalizedPattern() ); String b
         * = ((DecimalFormat) formatter).toLocalizedPattern(); System.out.println( "Currency format pattern: " +
         * ((DecimalFormat)formatter).toPattern() );
         */
        NumberFormat formatter;
        formatter = NumberFormat.getInstance(locale);
        formatter = NumberFormat.getCurrencyInstance(locale);

        //log.info("Currency format pattern: " + ((DecimalFormat) formatter).toLocalizedPattern());

        formatter = NumberFormat.getPercentInstance(locale);
        //log.info("Currency format pattern: " + ((DecimalFormat) formatter).toLocalizedPattern());

        monchar = '>';
        //log.info(">  = " + Integer.toHexString(monchar));
        //log.info(">  = " + ((int) monchar));

        monchar = '<';
        // log.info("<  = " + Integer.toHexString(monchar));
        //log.info("<  = " + ((int) monchar));

        monchar = ' ';
        //log.info("  = " + Integer.toHexString(monchar));
        //log.info("  = " + ((int) monchar));

        monchar = '_';
        // log.info("_  = " + Integer.toHexString(monchar));
        //log.info("_  = " + ((int) monchar));

    }

}