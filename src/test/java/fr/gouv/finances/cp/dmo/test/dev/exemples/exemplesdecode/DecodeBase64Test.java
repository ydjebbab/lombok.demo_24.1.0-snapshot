/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : DecodeBase64Test.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import junit.framework.TestCase;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;

/**
 * Class DecodeBase64Test DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class DecodeBase64Test extends TestCase
{

    /**
     * methode Testencodedecode base64 : DGFiP.
     * 
     * @throws DecoderException the decoder exception
     */
    public void testencodedecodeBase64() throws DecoderException
    {

        String messageCode = "dWlkPWNob3VhcmQtY3Asb3U9cGVyc29ubmVzLG91PWRnY3Asb3U9bWVmaSxvPWdvdXYsYz1mcjtwYXNzZWFtbA==";
        String messageDecode = new String(Base64.decodeBase64(messageCode.getBytes()));
        
        assertEquals("uid=chouard-cp,ou=personnes,ou=dgcp,ou=mefi,o=gouv,c=fr;passeaml", messageDecode);
    }

}
