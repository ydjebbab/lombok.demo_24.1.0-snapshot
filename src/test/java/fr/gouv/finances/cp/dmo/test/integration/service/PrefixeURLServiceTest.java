/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.service;

import static org.junit.Assert.assertEquals;

import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.gouv.finances.lombok.mel.service.PrefixeUrlService;

/**
 * Class PrefixeURLServiceTest 
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@AutoConfigureMockMvc
public class PrefixeURLServiceTest
{
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${appli.libelle-court}")
    public String libellecourt;

    @Autowired
    protected PrefixeUrlService prefixeurlserviceso;

    /**
     * methode Test succes verifier nombre deprefixes url 
     */
    @Test
    public void testSuccesVerifierNombreDeprefixesURL()
    {
        log.info(libellecourt);

        String mode = prefixeurlserviceso.getModeauthentification();
        log.info("Mode authentification :" + mode);
        List<String> listeprefixes = prefixeurlserviceso.donnerListeDesPrefixesUrlPossiblesPourAccesALapplication();
        for (Iterator<String> iterator = listeprefixes.iterator(); iterator.hasNext();)
        {
            String string = (String) iterator.next();
            log.info("Prefixe URL trouvé :" + string);

        }
        int nbprefixesattendues = 0;
        if (mode.equalsIgnoreCase("appli"))
        {
            nbprefixesattendues = 1;
        }
        else if (mode.equalsIgnoreCase("appliportail"))
        {
            nbprefixesattendues = 2;
        }

        assertEquals(listeprefixes.size(), nbprefixesattendues);

    }

}
