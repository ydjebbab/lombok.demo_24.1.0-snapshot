/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementChargementDonneesReferencesTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.batch;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;

import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.batch.TraitementChargementDonneesDeReferenceImpl;
import fr.gouv.finances.lombok.batch.ServiceBatchCommun;

/**
 * Class TraitementChargementDonneesReferencesTest
 * 
 * @author chouard-cp
 * @author CF : MAJ suite à la suppression du fichier XML de configuration
 *         traitementchargementdonneesdereferenceContext-service.xml
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = {"embedded", "jpa", "jdbc", "upload", "journal", "batch"})
@ContextConfiguration(locations = {
        // "classpath:conf/testpropertysource.xml",
        "classpath:conf/batch/batchContext-resources.xml",
        "classpath:conf/app-config.xml",
        "classpath:conf/batch-config-test.xml"})
@Transactional
public class TraitementChargementDonneesReferencesTest
{
    @Autowired
    @Qualifier("datasource") // Base HSQL
    DataSource datasource;

    @Autowired()
    @Qualifier("traitementchargementdonneesdereference")
    private TraitementChargementDonneesDeReferenceImpl traitementchargementdonneesdereferenceimpl;

    /**
     * Méthode de bouche trou en attendant de faire mieux. Quelques lignes pour vider les tables préremplies et éviter
     * les erreurs de contrainte unique dans le test.
     */
    @Before
    public void viderBase()
    {
        Statement stmt;
        try
        {
            stmt = datasource.getConnection().createStatement();
            stmt.execute("TRUNCATE TABLE CONTRATMENSUALISATION_COME");
            stmt.execute("TRUNCATE TABLE avisimposition_avis");
            stmt.execute("TRUNCATE TABLE typeimpot_tyim;");
        }
        catch (SQLException exc)
        {
            fail();
        }
    }

    @Test
    public void testTraiterBatch()
    {
        ServiceBatchCommun sbc = (ServiceBatchCommun) traitementchargementdonneesdereferenceimpl;
        int statuscode = sbc.demarrer();
        assertEquals(0, statuscode);
    }

}
