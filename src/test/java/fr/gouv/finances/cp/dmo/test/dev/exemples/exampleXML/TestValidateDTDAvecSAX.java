/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TestValidateDTDAvecSAX.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exampleXML;

import junit.framework.TestCase;
import fr.gouv.finances.cp.dmo.test.integration.util.FichierUtil;

/**
 * Class TestValidateDTDAvecSAX DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class TestValidateDTDAvecSAX extends TestCase
{

    /** Constant : REPBASE. */
    private static final String REPBASE =
        FichierUtil.recupererClassPath() + "/fr/gouv/finances/cp/dmo/test/dev/exemples/exampleXML/";

    /**
     * methode Test dtd succes a partir noms : DGFiP.
     */
    public void testDTDSuccesAPartirNoms()
    {
        DTDValidatorAvecSAX dtdValidator = new DTDValidatorAvecSAX();
        String lesResponses = dtdValidator.validerXMLParRapportDTD(REPBASE + "ARISAMLExport.xml");
        
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertEquals(null, lesResponses);

    }

    /**
     * methode Test dtd echec a partir noms : DGFiP.
     */
    public void testDTDEchecAPartirNoms()
    {
        DTDValidatorAvecSAX dtdValidator = new DTDValidatorAvecSAX();
        String lesResponses = dtdValidator.validerXMLParRapportDTD(REPBASE + "ARISAMLExportFalse.xml");
        
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertNotNull(lesResponses);

    }

    /**
     * methode Test dtda completer succes a partir noms : DGFiP.
     */
    public void testDTDACompleterSuccesAPartirNoms()
    {
        DTDValidatorAvecSAX dtdValidator = new DTDValidatorAvecSAX();
        String lesResponses =
            dtdValidator.validerXMLParRapportDTDNouvelle(REPBASE + "ARISAMLExport.xml", REPBASE + "NEWARIS-Export.dtd");
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertEquals(null, lesResponses);

    }

    /**
     * methode Test dtda completer echec a partir noms : DGFiP.
     */
    public void testDTDACompleterEchecAPartirNoms()
    {
        DTDValidatorAvecSAX dtdValidator = new DTDValidatorAvecSAX();
        String lesResponses =
            dtdValidator.validerXMLParRapportDTDNouvelle(REPBASE + "ARISAMLExportFalse.xml", REPBASE
                + "NEWARIS-Export.dtd");
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertNotNull(lesResponses);

    }

}
