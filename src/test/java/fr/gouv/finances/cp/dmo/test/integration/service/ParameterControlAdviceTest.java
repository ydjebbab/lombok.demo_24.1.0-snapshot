/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ParameterControlAdviceTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.service;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.lombok.util.base.LombokServiceTest;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;


/**
 * Class ParameterControlAdviceTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringRunner.class)
@LombokServiceTest
public class ParameterControlAdviceTest //extends BaseServiceTestCase
{

    /** contribuableserviceso. */
    @Autowired
    protected IContribuableService contribuableserviceso;

    
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    
    /**
     * methode Test null parameter : DGFiP.
     */
    @Test
    public void testNullParameter()
    {
        thrown.expect(ProgrammationException.class);
        
        String test = null;
       
            contribuableserviceso
                .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID(test);
            Assert.fail("Une exception de type ProgrammationException est attendue par l'advice");
       
       
    }
    @Test
    public void testNotNullParameter()
    {
        thrown.expect(RegleGestionException.class);
        String test = "notnull";
        
        contribuableserviceso
            .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID(test);
       
        
    }
}
