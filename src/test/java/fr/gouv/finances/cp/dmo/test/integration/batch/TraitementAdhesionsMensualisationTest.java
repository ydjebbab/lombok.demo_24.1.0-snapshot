/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 */
package fr.gouv.finances.cp.dmo.test.integration.batch;

import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.gouv.finances.lombok.util.base.LombokBatchTestSansSpring;

/**
 * Class TraitementAdhesionsMensualisationTest 
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class TraitementAdhesionsMensualisationTest extends LombokBatchTestSansSpring
{
    static final Logger logger = LoggerFactory.getLogger(TraitementAdhesionsMensualisationTest.class.getName());

    @Test
    public void testTraiterBatch()
    {
        logger.info("info");
        logger.error("error");
        System.out.println(ClassLoader.getSystemResource("log4j2.properties"));
        
        testBatch(new String[] {"traitementadhesionsmensualisation"});
        // vérification que le test fini sans erreur
        verify(systemExit).exit(0);

    }

}
