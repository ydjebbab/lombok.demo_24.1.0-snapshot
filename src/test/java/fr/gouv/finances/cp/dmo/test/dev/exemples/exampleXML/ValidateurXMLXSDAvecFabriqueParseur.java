/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ValidateurXMLXSDAvecFabriqueParseur.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exampleXML;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Class ValidateurXMLXSDAvecFabriqueParseur.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 14 déc. 2009
 */
public class ValidateurXMLXSDAvecFabriqueParseur implements ErrorHandler
{

    /** Memo erreur validation. */
    public boolean validationError = false;

    /** sax parse exception list. */
    public ArrayList<SAXParseException> saxParseExceptionList = new ArrayList<SAXParseException>();

    /** résultat de validation *. */
    private boolean isValid = false;

    /** La réponse de la validation. */
    private String response = null;

    /**
     * Verifie si valid.
     * 
     * @return true, si c'est valid
     */
    public boolean isValid()
    {
        return isValid;
    }

    /**
     * on passe juste les adresses du xml et du xsd.
     * 
     * @param xmlFile param
     * @param xsdFile param
     * @param recuperertoutesleserreurs param
     * @return the string
     */
    public String validerXMLParRapportXSD(String xmlFile, String xsdFile, String recuperertoutesleserreurs)
    {
        // changement de code pour utiliser javax.xml.apis plutôt que xerces en direct.
        // on prend le parseur du jdk (com.sun....) sinon on tombe sur le parseur embarqué par jopendocument
        // qui est lui-même tiré par jopendocument qui déclare un fichier
        // META-INF/services/javax.xml.validation.SchemaFactory
        // qui pointe vers une implémentation de parseur xsd de type NG.
        try
        {
            SchemaFactory sf =
                SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI,
                    "com.sun.org.apache.xerces.internal.jaxp.validation.XMLSchemaFactory",
                    null);
            Schema schema = sf.newSchema(new File(xsdFile));
            Validator validator = schema.newValidator();
            Source source = new StreamSource(new File(xmlFile));
            validator.validate(source);

            if (validationError == true)
            {
                response = calculerReponse(saxParseExceptionList);
                return response;
            }
            else
            {
                return null;
            }
        }

        catch (SAXException e)
        {
            System.out.println("SAXException détectée");
            if (this.validationError)
            {
                response = calculerReponse(this.saxParseExceptionList);
            }
            StringBuffer sb;
            if (response != null && response.compareTo("") != 0)
            {
                sb = new StringBuffer(response);
            }
            else
            {
                sb = new StringBuffer("");
            }
            sb.append("\n  ");
            sb.append(" *** ").append(e.getMessage());
            return sb.toString();

        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            System.out.println("IOException détectée");
            if (this.validationError)
            {
                response = calculerReponse(this.saxParseExceptionList);
            }
            StringBuffer sb;
            if (response != null && response.compareTo("") != 0)
            {
                sb = new StringBuffer(response);
            }
            else
            {
                sb = new StringBuffer("");
            }
            sb.append("\n  ");
            sb.append(" *** ").append(e.getMessage());
            return sb.toString();
        }

    }

    /**
     * renvoie toutes les erreurs sous forme de string.
     * 
     * @param saxParseExceptionList param
     * @return the string
     */
    private String calculerReponse(ArrayList<SAXParseException> saxParseExceptionList)
    {
        StringBuffer sb = new StringBuffer();
        sb.append("les erreurs suivantes ont ete detectees");
        for (int i = 0; i < saxParseExceptionList.size(); i++)
        {
            SAXParseException xcpt = (SAXParseException) saxParseExceptionList.get(i);
            sb.append("\n  ").append(xcpt.getLineNumber());
            sb.append(":").append(xcpt.getColumnNumber());
            sb.append(" *** ").append(xcpt.getMessage());
        }
        return sb.toString();
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.xml.sax.ErrorHandler#error(org.xml.sax.SAXParseException)
     */
    public void error(SAXParseException arg0) throws SAXException
    {
        validationError = true;
        saxParseExceptionList.add(arg0);

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.xml.sax.ErrorHandler#fatalError(org.xml.sax.SAXParseException)
     */
    public void fatalError(SAXParseException arg0) throws SAXException
    {
        validationError = true;
        saxParseExceptionList.add(arg0);

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.xml.sax.ErrorHandler#warning(org.xml.sax.SAXParseException)
     */
    public void warning(SAXParseException arg0) throws SAXException
    {
        validationError = true;
        saxParseExceptionList.add(arg0);

    }

}
