/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ContribuableServiceTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.gouv.finances.cp.dmo.WebLauncher;
import fr.gouv.finances.cp.dmo.dao.IReferenceDao;
import fr.gouv.finances.cp.dmo.entite.AdresseContribuable;
import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.Rib;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.service.IContribuableService;
import fr.gouv.finances.cp.dmo.techbean.CriteresRecherches;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;

/**
 * Class ContribuableServiceTest
 * 
 * @author chouard-cp
 * @author cf Passage en Java 8
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WebLauncher.class})
@WebAppConfiguration
public class ContribuableServiceTest // extends BaseServiceTestCase
{

    private static final String adressemel1 = "yussouf.goulamhoussen@dgfip.finances.gouv.fr";

    @Autowired
    protected IContribuableService contribuableserviceso;

    @Autowired
    protected IReferenceDao referencedao;

    /** identifiantutilisateuroubatch. */
    String identifiantutilisateuroubatch = "admintest-cp";

    /**
     * Test success normaliseradressecontribuable
     */
    @Test
    public void testSuccessNormaliseradressecontribuable()
    {
        ContribuablePar contr = new ContribuablePar();
        List<AdresseContribuable> Adresses = new ArrayList<AdresseContribuable>();
        AdresseContribuable uneAdresse = new AdresseContribuable();

        uneAdresse.setLigne3("   CHAINE, #  AVEC BLANCS'   CONSECUTIFS   ");
        Adresses.add(uneAdresse);
        contr.setListeAdresses(Adresses);

        contribuableserviceso.normaliserAdresseContribuable(contr);
        System.out.println("ligne = " + contr.getAdressePrincipale().getLigne3());
        Assert.assertEquals("CHAINE AVEC BLANCS CONSECUTIFS", contr.getAdressePrincipale().getLigne3());
    }

    /**
     * Test succes rechercher un contribuable et contrats mensualisation et avis imposition et type impots par uid
     */
    @Test
    public void testSuccesRechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID()
    {
        ContribuablePar contribuable = null;
        contribuable =
            contribuableserviceso
                .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID("wpetit-cp");
        Assert.assertEquals("wpetit-cp", contribuable.getIdentifiant());
    }

    /**
     * Test echec rechercher un contribuable et contrats mensualisation et avis imposition et type impots par uid
     */
    @Test
    public void testEchecRechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID()
    {
        String uid = "dsqd";
        RegleGestionException e =
            new RegleGestionException(
                "Caractéristiques contribuable non trouvées dans la base de données - contactez le service recouvrement");
        try
        {
            contribuableserviceso
                .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID(uid);
            Assert.fail("Echec du controle : le contribuable n existe pas ");
        }
        catch (RegleGestionException regleGestionException)
        {
            Assert.assertEquals(e.getMessage(), regleGestionException.getMessage());
        }
        catch (Exception nonexpected)
        {
            e.printStackTrace();
            Assert.fail("Exception non prévue");
        }
    }

    /**
     * Test echec rechercher un contribuable et contrats mensualisation et avis imposition et type impots par reference
     * avis
     */
    @Test
    public void testEchecRechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParReferenceAvis()
    {
        String refAvis = "5435444";

        ContribuablePar contribuable =
            contribuableserviceso
                .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParReferenceAvis(refAvis);
        Assert.assertNull(contribuable);
    }

    /**
     * Test succes rechercher liste contribuable
     */
    @Test
    public void testSuccesRechercherListeContribuable()
    {
        CriteresRecherches crit = new CriteresRecherches();
        crit.setIdentifiant("wpetit-cp");

        Optional<List<? extends Contribuable>> contribuableApresTEst = contribuableserviceso.rechercherListeContribuable(crit);
        Assert.assertEquals(contribuableApresTEst.get().size(), 1);
    }

    /**
     * Test succes adherer ala mensualisation
     */
    @Test
    public void testSuccesAdhererAlaMensualisation()
    {
        ContribuablePar contribuable = null;

        contribuable =
            contribuableserviceso
                .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID("amleplatinec-cp");
        Set<AvisImposition> listavis = contribuable.getListeAvisImposition();
        contribuable.setAdresseMail(adressemel1);
        AvisImposition unavis = (AvisImposition) listavis.toArray()[2];
        TypeImpot untypeimpot = unavis.getTypeImpot();

        /* Rib de test */
        Rib unrib = new Rib();
        unrib.setCodeBanque("30002");
        unrib.setCodeGuichet("00658");
        unrib.setNumeroCompte("11111111111");
        unrib.setCleRib("93");

        ContratMensu uncontratmensu = new ContratMensu();
        uncontratmensu.setRib(unrib);
        uncontratmensu.setContribuablePar(contribuable);
        uncontratmensu.setTypeImpot(untypeimpot);
        uncontratmensu.setAnneePriseEffet(Long.valueOf(2005));
        uncontratmensu.setNumeroContrat(Long.valueOf(unavis.getReference()));

        contribuableserviceso.adhererAlaMensualisation(contribuable, untypeimpot, uncontratmensu,
            unavis.getReference(), identifiantutilisateuroubatch);

        ContribuablePar contribuableApresTEst =
            contribuableserviceso
                .rechercherUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID("amleplatinec-cp");
        Iterator<ContratMensu> itermensu = contribuableApresTEst.getListeContratMensu().iterator();
        ContratMensu uncontratlu;
        boolean resulttest = false;

        while (itermensu.hasNext())
        {
            uncontratlu = (ContratMensu) itermensu.next();
            if (uncontratlu.equals(uncontratmensu))
            {
                resulttest = true;
            }
        }

        Assert.assertTrue(resulttest);

    }

    /**
     * Test succes ajouter un contribuable
     */
    @Test
    public void testSuccesAjouterUnContribuable()
    {

        ContribuablePar uncontribuablepar = new ContribuablePar();
        uncontribuablepar.setIdentifiant("testuserjunit1-cp");
        uncontribuablepar.setNom("testnom");
        uncontribuablepar.setPrenom("testprenom");
        uncontribuablepar.setTelephoneFixe("0000000000");
        uncontribuablepar.setTelephonePortable("0000000000");
        uncontribuablepar.setAdresseMail("testuserjunit1@cp.finances.gouv.fr");
        AdresseContribuable uneAdressePrincipale = new AdresseContribuable();
        uneAdressePrincipale.setCodePostal("93100");
        uneAdressePrincipale.setLigne1("3eme étage - escalier C");
        uneAdressePrincipale.setLigne2("Immeuble D3");
        uneAdressePrincipale.setLigne3("RUE AUGUSTE BLANQUI");
        uneAdressePrincipale.setLigne4("BP18");
        uneAdressePrincipale.setVille("MONTREUIL");
        uneAdressePrincipale.setPays("FRANCE");

        uncontribuablepar.rattacherUneNouvelleAdresse(uneAdressePrincipale);
        contribuableserviceso.ajouterUnContribuable(uncontribuablepar, identifiantutilisateuroubatch);
        CriteresRecherches crit = new CriteresRecherches();
        crit.setIdentifiant(uncontribuablepar.getIdentifiant());

        Optional<List<? extends Contribuable>> contribuableApresTEst = contribuableserviceso.rechercherListeContribuable(crit);
        Assert.assertEquals(uncontribuablepar.getIdentifiant(), ((ContribuablePar) contribuableApresTEst.get().get(0))
            .getIdentifiant());

    }

    /**
     * Test succes modifier un contribuable
     */
    @Test
    public void testSuccesModifierUnContribuable()
    {

        ContribuablePar uncontribuablepar = new ContribuablePar();
        uncontribuablepar.setIdentifiant("testuserjunit2-cp");
        uncontribuablepar.setNom("testnom");
        uncontribuablepar.setPrenom("testprenom");
        uncontribuablepar.setTelephoneFixe("0000000000");
        uncontribuablepar.setTelephonePortable("0000000000");
        uncontribuablepar.setAdresseMail("testuserjunit2@cp.finances.gouv.fr");
        AdresseContribuable uneAdressePrincipale = new AdresseContribuable();
        uneAdressePrincipale.setCodePostal("93100");
        uneAdressePrincipale.setLigne1("3eme étage - escalier C");
        uneAdressePrincipale.setLigne2("Immeuble D3");
        uneAdressePrincipale.setLigne3("RUE AUGUSTE BLANQUI");
        uneAdressePrincipale.setLigne4("BP18");
        uneAdressePrincipale.setVille("MONTREUIL");
        uneAdressePrincipale.setPays("FRANCE");
        uncontribuablepar.rattacherUneNouvelleAdresse(uneAdressePrincipale);

        // a déjà été

        contribuableserviceso.ajouterUnContribuable(uncontribuablepar, identifiantutilisateuroubatch);
        // testée donc on
        // s'esn sert
        uncontribuablepar.setTelephonePortable("9999999999999");
        contribuableserviceso.modifierUnContribuable(uncontribuablepar, identifiantutilisateuroubatch);
        CriteresRecherches crit = new CriteresRecherches();
        crit.setIdentifiant("testuserjunit2-cp");

        Optional<List<? extends Contribuable>> contribuableApresTEst = contribuableserviceso.rechercherListeContribuable(crit);
        Assert.assertEquals("9999999999999", ((ContribuablePar) contribuableApresTEst.get().get(0)).getTelephonePortable());
    }

    /**
     * Test succes supprimer un contribuable
     */
    @Test
    public void testSuccesSupprimerUnContribuable()
    {
        ContribuablePar uncontribuablepar = new ContribuablePar();
        uncontribuablepar.setIdentifiant("testuserjunit3-cp");
        uncontribuablepar.setNom("testnom");
        uncontribuablepar.setPrenom("testprenom");
        uncontribuablepar.setTelephoneFixe("0000000000");
        uncontribuablepar.setTelephonePortable("0000000000");
        uncontribuablepar.setAdresseMail("testuserjunit3@cp.finances.gouv.fr");
        AdresseContribuable uneAdressePrincipale = new AdresseContribuable();
        uneAdressePrincipale.setCodePostal("93100");
        uneAdressePrincipale.setLigne1("3eme étage - escalier C");
        uneAdressePrincipale.setLigne2("Immeuble D3");
        uneAdressePrincipale.setLigne3("RUE AUGUSTE BLANQUI");
        uneAdressePrincipale.setLigne4("BP18");
        uneAdressePrincipale.setVille("MONTREUIL");
        uneAdressePrincipale.setPays("FRANCE");
        uncontribuablepar.rattacherUneNouvelleAdresse(uneAdressePrincipale);
        contribuableserviceso.ajouterUnContribuable(uncontribuablepar, identifiantutilisateuroubatch);
        List<Contribuable> contASupprimer = new ArrayList<Contribuable>();
        contASupprimer.add(uncontribuablepar);
        contribuableserviceso.supprimerDesContribuablesParticuliers(contASupprimer, identifiantutilisateuroubatch);
        CriteresRecherches crit = new CriteresRecherches();
        crit.setIdentifiant(uncontribuablepar.getIdentifiant());

        Optional<List<? extends Contribuable>> contribuableApresTEst = contribuableserviceso.rechercherListeContribuable(crit);
        List<? extends Contribuable> listeContribuablevide = new ArrayList<Contribuable>();

        Assert.assertEquals(listeContribuablevide, contribuableApresTEst.get());
    }

    /**
     * Test rechercher liste types impots disponibles a la mensualisation pour un contribuable
     */
    @Test
    public void testRechercherListeTypesImpotsDisponiblesALaMensualisationPourUnContribuable()
    {
        CriteresRecherches crit = new CriteresRecherches();
        crit.setIdentifiant("wpetit-cp");

        Optional<List<? extends Contribuable>> contribuableApresTEst = contribuableserviceso.rechercherListeContribuable(crit);
        @SuppressWarnings("rawtypes")
        List uneListeTypeImpot =
            contribuableserviceso
                .rechercherListeTypesImpotsDisponiblesALaMensualisationPourUnContribuable((ContribuablePar) contribuableApresTEst
                    .get().get(0));

        Assert.assertEquals(uneListeTypeImpot.size(), 3);

    }

}
