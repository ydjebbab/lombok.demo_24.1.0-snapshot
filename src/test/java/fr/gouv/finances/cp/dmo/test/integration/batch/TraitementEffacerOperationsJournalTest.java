/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementEffacerOperationsJournalTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.batch;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.batch.ServiceBatchCommun;
import org.junit.Assert;

/**
 * Class TraitementEffacerOperationsJournalTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = {"embedded", "hibernate", "jdbc", "upload", "journal"})
@ContextConfiguration(locations = {
        "classpath:conf/testpropertysource.xml",
        "classpath:conf/app-config.xml",
        "classpath:conf/batch/batchContext-resources.xml",
        "classpath:conf/batch/effaceroperationsjournal/traitementeffaceroperationsjournalContext-service.xml"})
@Transactional
public class TraitementEffacerOperationsJournalTest
{
    @Autowired
    ApplicationContext batchContext;

    @Test
    public void testTraiterBatch()
    {
        ServiceBatchCommun sbc =
            (ServiceBatchCommun) batchContext.getBean("traitementeffaceroperationsjournalimpl");
        int statuscode = sbc.demarrer();
        Assert.assertEquals(0, statuscode);
    }

}