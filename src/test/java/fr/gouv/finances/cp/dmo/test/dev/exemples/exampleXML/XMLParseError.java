/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : XMLParseError.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exampleXML;

/**
 * Class XMLParseError DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class XMLParseError
{

    /** num ligne erreur. */
    private int numLigneErreur;

    /** num colonne erreur. */
    private int numColonneErreur;

    /** text erreur. */
    private String textErreur;

    /**
     * Instanciation de xML parse error.
     */
    public XMLParseError()
    {

    }

    /**
     * Accesseur de l attribut num colonne erreur.
     * 
     * @return num colonne erreur
     */
    public int getNumColonneErreur()
    {
        return numColonneErreur;
    }

    /**
     * Modificateur de l attribut num colonne erreur.
     * 
     * @param numColonneErreur le nouveau num colonne erreur
     */
    public void setNumColonneErreur(int numColonneErreur)
    {
        this.numColonneErreur = numColonneErreur;
    }

    /**
     * Accesseur de l attribut num ligne erreur.
     * 
     * @return num ligne erreur
     */
    public int getNumLigneErreur()
    {
        return numLigneErreur;
    }

    /**
     * Modificateur de l attribut num ligne erreur.
     * 
     * @param numLigneErreur le nouveau num ligne erreur
     */
    public void setNumLigneErreur(int numLigneErreur)
    {
        this.numLigneErreur = numLigneErreur;
    }

    /**
     * Accesseur de l attribut text erreur.
     * 
     * @return text erreur
     */
    public String getTextErreur()
    {
        return textErreur;
    }

    /**
     * Modificateur de l attribut text erreur.
     * 
     * @param textErreur le nouveau text erreur
     */
    public void setTextErreur(String textErreur)
    {
        this.textErreur = textErreur;
    }

}
