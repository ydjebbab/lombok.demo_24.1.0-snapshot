/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementProductionListeContribuablesTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.batch;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.batch.TraitementProductionEditionsListeContribuablesImpl;
import fr.gouv.finances.lombok.batch.ServiceBatchCommun;

/**
 * On essaie de tester des "grosses" éditions afin de déterminer la limite.
 * 
 * @author chouard-cp
 * @author CF : MAJ suite à la suppression du fichier XML de configuration
 *         traitementproductioneditionslistecontribuablesContext-service.xml
 * @version $Revision: 1.2 $ Date: 14 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = {"embedded", "jpa", "jdbc", "upload", "journal", "edition", "batch"})
@ContextConfiguration(locations = {
        "classpath:conf/testpropertysource.xml",
        "classpath:conf/app-config.xml",
        "classpath:conf/batch/batchContext-resources.xml",
        "classpath:conf/batch-config-test.xml"})
// "classpath:conf/batch/editionslistecontribuable/traitementproductioneditionslistecontribuablesContext-service.xml"})
@Transactional
public class TraitementProductionListeContribuablesTest
{

    /*
     * protected String[] ajouterConfigsLocationsSpecifiques() { return new String[] {
     * "file:$localisations{appli.batch.root}/editionslistecontribuable/traitementproductioneditionslistecontribuablesContext-service.xml",
     * "file:$localisations{appli.web.root}/WEB-INF/conf/commun/applicationContext-commun-edition.xml",
     * "file:$localisations{appli.web.root}/WEB-INF/conf/application/applicationContext-edition-zf2.xml"}; }
     */

    @Autowired()
    @Qualifier("traitementproductioneditionslistecontribuables")
    private TraitementProductionEditionsListeContribuablesImpl traitementProductionEditionsListeContribuablesImpl;

    /**
     * methode Test traiter batch : DGFiP.
     */
    @Test
    public void testTraiterBatch()
    {
        ServiceBatchCommun sbc = (ServiceBatchCommun) traitementProductionEditionsListeContribuablesImpl;
        int statuscode = sbc.demarrer();
        Assert.assertEquals(0, statuscode);
    }

}