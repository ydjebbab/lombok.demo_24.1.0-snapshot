/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : PersonneAnnuaireTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

/**
 * Class PersonneAnnuaireTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class PersonneAnnuaireTest
{
    
    /** affectation. */
    public String affectation;

    /** annexe. */
    public String annexe;

    /** business category. */
    public String businessCategory;

    /** cn. */
    public String cn;

    /** code fonction. */
    public String codeFonction;

    /** code grade. */
    public String codeGrade;

    /** departement. */
    public String departement;

    /** department. */
    public String department;

    /** fonction. */
    public String fonction;

    /** given name. */
    public String givenName;
}