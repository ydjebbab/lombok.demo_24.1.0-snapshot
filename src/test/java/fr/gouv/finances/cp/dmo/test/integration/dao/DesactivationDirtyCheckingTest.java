/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : DesactivationDirtyCheckingTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.PlatformTransactionManager;

import fr.gouv.finances.cp.dmo.dao.IContribuableDao;
import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.lombok.util.base.LombokTransactionDAOTest;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;

/**
 * Class DesactivationDirtyCheckingTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@LombokTransactionDAOTest(supprimerDirtyCheck=true)
public class DesactivationDirtyCheckingTest
{
    @Autowired
    protected PlatformTransactionManager transactionManager;
    
    @BeforeTransaction
    public void setUpBeforeTransaction() throws Exception {
      
      // lire la propriété de l'annotation
      boolean supprimerDirtyCheck= DesactivationDirtyCheckingTest.class.getAnnotation(LombokTransactionDAOTest.class).supprimerDirtyCheck(); 

      if( HibernateTransactionManager.class == transactionManager.getClass() && supprimerDirtyCheck ){
          // FIXME : CF TODO
          //(( HibernateTransactionManager ) transactionManager ).setEntityInterceptor( new InterceptorSansDirtyChecking() );
      }
      else if(HibernateTransactionManager.class != transactionManager.getClass()){
          throw new ProgrammationException("Utilisation d'un transactionManager"
                  + transactionManager.getClass().toString()
                  + " non pris en charge pour la suppression du dirtyCheck hibernate");
      }
    }
   /* @BeforeTransaction
    public void dirtyCheck() throws Exception {
        
        
        
         * Modification de la factory hibernate pour desactivation du dirty checking
         
        SessionFactoryImpl sessionFactory =
            (SessionFactoryImpl) context.getBean("hibernatesessionfactory");
        Interceptor i = new InterceptorSansDirtyChecking();
        
       
        Field f = sessionFactory.getClass().getDeclaredField("interceptor");

        f.setAccessible(true);
        f.set(sessionFactory, i);
    }*/
    /*@BeforeTransaction
    public void supprimerDirtyCheck() throws Exception {
        HibernateTransactionManager mngr = (HibernateTransactionManager)transactionManager;
        mngr.setEntityInterceptor(new InterceptorSansDirtyChecking());
       
    }*/

    /** contribuabledao. */
    @Autowired
    protected IContribuableDao contribuabledao;

    /** contribuable. */
    ContribuablePar contribuable = new ContribuablePar();

    // Desactivation du dirty checking
    /** 
     *(methode de remplacement)
     * {@inheritDoc}
     * @see fr.gouv.finances.cp.dmo.test.util.base.BaseDAOTestCase#neRienFaireSiDirtyCheck()
     */
   
    protected boolean neRienFaireSiDirtyCheck()
    {
        return true;
    }

    /**
     * methode Test success desactivation dirty checking : DGFiP.
     */
    @Test
    public void testSuccessDesactivationDirtyChecking()
    {

        String reference = "843333333333";
        AvisImposition avis = contribuabledao.findAvisImpositionParReference(reference);
        double memo = avis.getMontant();
        
        avis.setMontant(memo + 1);

        contribuabledao.findAvisImpositionParReference(reference);

        // Supprime la session pour forcer un nouveau mapping a partir de la relecture en base
        contribuabledao.clearPersistenceContext();

        AvisImposition troisiemelectureavis = contribuabledao.findAvisImpositionParReference(reference);
        // Si le Dirty Checking est bien désactivé on doi retrouver le montant initial
        // Sinon le montant incérémenté
        assertEquals(troisiemelectureavis.getMontant().doubleValue(), memo,0.01);

    }

}
