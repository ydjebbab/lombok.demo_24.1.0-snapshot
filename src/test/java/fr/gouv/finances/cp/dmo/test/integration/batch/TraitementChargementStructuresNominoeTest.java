/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementChargementStructuresNominoeTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.batch;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.structure.batch.TraitementChargementStructuresCPImpl;

/**
 * Class TraitementChargementStructuresNominoeTest. le profile edition est nécessaire car structure effectue
 * certainement des manips avec quartz (et le profile edition charge les tables quartz en mode embedded
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 14 déc. 2009
 */
@Ignore("Test déconnecté car il s'appuie sur le composant structure non encore passé en 24")
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = {"embedded", "hibernate", "jdbc", "upload", "journal", "structure", "edition"})
@ContextConfiguration(locations = {
        "classpath:conf/testpropertysource.xml",
        "classpath:conf/app-config.xml",
        "classpath:conf/batch/batchContext-resources.xml",
        "classpath:conf/batch/chargementstructurescp/traitementchargementstructurescpContext-service.xml"})
@Transactional
public class TraitementChargementStructuresNominoeTest
{
    @Autowired
    ApplicationContext batchContext;

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.lombok.util.base.BaseTestAvecContexteSpring#rollBackEnFinDeTest()
     */
    protected boolean rollBackEnFinDeTest()
    {
        return false;
    }

    /**
     * methode Test traiter batch : DGFiP.
     */
    @Test
    public void testTraiterBatch()
    {
        // this.endTransaction();
        TraitementChargementStructuresCPImpl sbc =
            (TraitementChargementStructuresCPImpl) batchContext.getBean("traitementchargementstructurescpimpl");
        sbc.demarrer();
        int statuscode = sbc.demarrer();
        assertEquals(0, statuscode);

    }

}