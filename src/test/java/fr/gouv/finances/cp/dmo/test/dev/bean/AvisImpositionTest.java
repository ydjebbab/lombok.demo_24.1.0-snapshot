/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : AvisImpositionTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.bean;

import org.junit.Test;

import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Class de test 
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class AvisImpositionTest extends AbstractCorePojoTest<AvisImposition>
{
    /**
     * test de la méthode de vérification du rattachement d'un avis d'imposition à un type d'impôt donné.
     */
    @Test
    public void testVerifierAvisDImpositionEstAttacheAUnTypeImpotRG15()
    {
        // l'avis d'imposition à tester et son type d'impôt correspondant
        TypeImpot typeImpot = new TypeImpot(new Long(4));
        AvisImposition avisImposition = new AvisImposition(null, null, null, null, "1111111111", typeImpot);

        // test de la méthode de vérification
        avisImposition.verifierAvisDImpositionEstAttacheAUnTypeImpotRG15(new Long(4));
    }
}
