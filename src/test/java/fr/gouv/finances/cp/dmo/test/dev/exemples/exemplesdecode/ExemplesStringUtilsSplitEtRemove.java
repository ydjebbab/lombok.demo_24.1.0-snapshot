/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ExemplesStringUtilsSplitEtRemove.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import junit.framework.TestCase;

import org.apache.commons.lang.StringUtils;

/**
 * Class ExemplesStringUtilsSplitEtRemove DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ExemplesStringUtilsSplitEtRemove extends TestCase
{

    /**
     * methode Test coupe fin : DGFiP.
     */
    public void testCoupeFin()
    {
        String chaineACouper = "dnenlong;passeaml";
        String separateur = "";
        
        separateur = separateur.concat(";");
        separateur = separateur.concat("passeaml");
        chaineACouper = StringUtils.remove(chaineACouper, separateur);
        assertEquals(chaineACouper, "dnenlong");
    }

    /**
     * methode Test coupe debut : DGFiP.
     */
    public void testCoupeDebut()
    {
        String chaineACouper = "dnenlong;passeaml";
        String separateur = "";
        
        separateur = separateur.concat(";");
        // separateur = separateur.concat("passeaml");
        String[] tab = StringUtils.split(chaineACouper, separateur);
        assertEquals(tab[0], "dnenlong");
        assertEquals(tab[1], "passeaml");
    }

}
