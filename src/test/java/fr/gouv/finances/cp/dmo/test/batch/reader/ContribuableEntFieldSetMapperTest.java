/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.test.batch.reader;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

import org.junit.Test;
import org.springframework.batch.item.file.transform.FieldSet;

import fr.gouv.finances.cp.dmo.batch.reader.ContribuableEntFieldSetMapper;

/**
 * Class ContribuableEntFieldSetMapperTest
 */
public class ContribuableEntFieldSetMapperTest
{

    /**
     * methode Test contribuable ent field set mapper
     *
     * @throws Exception the exception
     */
    @Test
    public void testContribuableEntFieldSetMapper() throws Exception
    {
        ContribuableEntFieldSetMapper cefsm = new ContribuableEntFieldSetMapper();
        assertNotNull(cefsm);
    }

    /**
     * methode Test map field set :
     *
     * @throws Exception the exception
     */
    @Test
    public void testMapFieldSet() throws Exception
    {
        FieldSet fieldSet = mock(FieldSet.class);
        new ContribuableEntFieldSetMapper().mapFieldSet(fieldSet);
    }

}
