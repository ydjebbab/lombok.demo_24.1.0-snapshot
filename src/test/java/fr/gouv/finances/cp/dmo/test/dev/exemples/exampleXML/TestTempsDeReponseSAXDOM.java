/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TestTempsDeReponseSAXDOM.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exampleXML;

import fr.gouv.finances.cp.dmo.test.integration.util.FichierUtil;
import junit.framework.TestCase;

/**
 * Class TestTempsDeReponseSAXDOM DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class TestTempsDeReponseSAXDOM extends TestCase
{

    /** Constant : REPBASE. */
    private static final String REPBASE =
        FichierUtil.recupererClassPath() + "/fr/gouv/finances/cp/dmo/test/dev/exemples/exampleXML/";

    /**
     * TODO retester avec des gros fichiers sepa.
     */

    public void testSAX()
    {
        ValidateurXMLXSDAvecFabriqueParseur xsdValidator = new ValidateurXMLXSDAvecFabriqueParseur();
        //System.out.println("date de début avec sax");
        //System.out.println(new Date().getTime());
        String lesResponses =
            xsdValidator.validerXMLParRapportXSD(REPBASE + "NominoeDemoV18.xml", REPBASE + "Nominoe_V18R0.xsd",
                "recuperertoutesleserreurs");
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        System.out.println("date de fin avec sax");
        System.out.println(new Date().getTime());
        */
        assertNull(lesResponses);

    }

    @Override
    protected void setUp() throws Exception
    {
        System.setProperty("jaxp.debug", "1");
    }

    /**
     * methode Test dom : DGFiP.
     */
    public void testDOM()
    {
        ValidateurXMLXSDAvecDom xsdValidator = new ValidateurXMLXSDAvecDom();
        //System.out.println("date de début avec dom");
        //System.out.println(new Date().getTime());
        String lesResponses =
            xsdValidator.validerXMLParRapportXSD(REPBASE + "NominoeDemoV18.xml", REPBASE + "Nominoe_V18R0.xsd",
                "recuperertoutesleserreurs");
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        System.out.println("date de fin avec dom");
        System.out.println(new Date().getTime());
        */
        assertNull(lesResponses);

    }

    /**
     * Bilan : sax est plus rapide que dom
     */

}
