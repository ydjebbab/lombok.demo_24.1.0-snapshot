/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ExempleEcritureLectureFichiers.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import fr.gouv.finances.cp.dmo.test.integration.util.FichierUtil;
import fr.gouv.finances.lombok.util.exception.ApplicationExceptionTransformateur;
import fr.gouv.finances.lombok.util.exception.ExploitationException;

/**
 * Class ExempleEcritureLectureFichiers DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ExempleEcritureLectureFichiers extends TestCase
{

    /** Constant : REPBASE. */
    private static final String REPBASE =
        "file:" + FichierUtil.recupererClassPath() + "/fr/gouv/finances/cp/dmo/test/dev/exemples/exemplesdecode/";

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    // en principe à injecter par spring
    /** cheminfichierexemple. */
    private String cheminfichierexemple = REPBASE + "testfichierexemple";

    /**
     * methode Test ecriture fichier : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testEcritureFichier() throws Exception
    {
        File leFichier;

        try
        {
            leFichier = new File(new URL(this.cheminfichierexemple + ".txt").getFile());
        }
        catch (MalformedURLException e)
        {
            throw ApplicationExceptionTransformateur.transformer("la syntaxe de l'url spécifiée "
                + "pour le fichier exemple est erronée", e);
        }

        PrintWriter leWriter = null;
        try
        {

            leWriter = new PrintWriter(new BufferedWriter(new FileWriter(leFichier)));

            for (int i = 0; i <= 10; i++)
            {
               // leWriter.println("une ligne d'exemple accentuée n°" + i);

            }
        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer("probleme lors du traitement du fichier Exemple"
                + leFichier.getAbsolutePath(), exception);
        }
        finally
        {
            if (leWriter != null)
            {
                leWriter.close();
            }
        }

    }

    /**
     * methode Test lecture fichier : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testLectureFichier() throws Exception
    {
        File leFichier;
       // log.info("Debut testLectureFichier");
        try
        {
            leFichier = new File(new URL(this.cheminfichierexemple + ".txt").getFile());
        }
        catch (MalformedURLException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "la syntaxe de l'url spécifiée pour le fichier exemple est erronée", e);
        }

        BufferedReader leReader = null;
        String ligneFichierExemple;
        
        try
        {
            leReader = new BufferedReader(new FileReader(leFichier));
            while ((ligneFichierExemple = leReader.readLine()) != null)
            {
              //  log.info(ligneFichierExemple);
            }
        }
        catch (FileNotFoundException exception)
        {
            throw new ExploitationException("Attention le fichier exemple" + "n'a pas été trouvé sur ce chemin "
                + leFichier.getAbsolutePath() + " copiez le sur ce chemin ou corrigez "
                + " sa localisation dans le fichier " + " application.properties et relancez", exception);
        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer("probleme lors du traitement du fichier Exemple"
                + leFichier.getAbsolutePath(), exception);
        }
        finally
        {
            if (leReader != null)
            {
                leReader.close();
            }

        }

    }

    /**
     * methode Test ecriture fichier avec maitrise encodage : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testEcritureFichierAvecMaitriseEncodage() throws Exception
    {
        File leFichier;
        String encodage = "UTF8";
        String leNomDeFichier = this.cheminfichierexemple + encodage + ".txt";
        try
        {
            leFichier = new File(new URL(leNomDeFichier).getFile());
        }
        catch (MalformedURLException e)
        {
            throw ApplicationExceptionTransformateur.transformer("la syntaxe de l'url spécifiée "
                + "pour le fichier code postaux est erronée", e);
        }

        PrintWriter leWriter = null;
        try
        {
            leWriter =
                new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(leFichier), encodage)));

            for (int i = 0; i <= 10; i++)
            {
                //leWriter.println("une ligne d'exemple utf8 avec des caractères accentués <éçùàè€> <\u6e50> " + i);

            }
        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer("probleme lors du traitement du fichier Exemple"
                + leFichier.getAbsolutePath(), exception);
        }
        finally
        {
            if (leWriter != null)
            {
                leWriter.close();
            }
        }

    }

    /**
     * methode Test lecture fichier avec maitrise encodage : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testLectureFichierAvecMaitriseEncodage() throws Exception
    {

        File leFichier;
        String encodage = "UTF8";
        String leNomDeFichier = this.cheminfichierexemple + encodage + ".txt";
        
        //log.info("Debut testLectureFichierAvecMaitriseEncodage : " + leNomDeFichier);
        try
        {
            leFichier = new File(new URL(leNomDeFichier).getFile());
        }
        catch (MalformedURLException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "la syntaxe de l'url spécifiée pour le fichier code postaux est erronée", e);
        }

        BufferedReader leReader = null;
        String ligneFichierExemple;
        
        try
        {
            leReader = new BufferedReader(new InputStreamReader(new FileInputStream(leFichier), encodage));

            while ((ligneFichierExemple = leReader.readLine()) != null)
            {
                log.info(ligneFichierExemple);
            }
        }
        catch (FileNotFoundException exception)
        {
            throw new ExploitationException("Attention le fichier exemple" + "n'a pas été trouvé sur ce chemin "
                + leFichier.getAbsolutePath() + " copiez le sur ce chemin ou corrigez "
                + " sa localisation dans le fichier " + " application.properties et relancez", exception);
        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer("probleme lors du traitement du fichier Exemple"
                + leFichier.getAbsolutePath(), exception);
        }
        finally
        {
            if (leReader != null)
            {
                leReader.close();
            }
        }

    }

    /**
     * methode Test ecriture fichier avec maitrise encodage et fin de ligne windows : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testEcritureFichierAvecMaitriseEncodageEtFinDeLigneWindows() throws Exception
    {

        String encodage = "Cp1252";
        String findeligne = "\r\n";
        String typefindeligne = "Windows";
        String leNomDefichier = this.cheminfichierexemple + encodage + typefindeligne + ".txt";

        ecrireFichierAvecMaitriseEncodageEtFinDeLigne(encodage, findeligne, typefindeligne, leNomDefichier);

    }

    /**
     * methode Test ecriture fichier avec maitrise encodage et fin de ligne unix : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testEcritureFichierAvecMaitriseEncodageEtFinDeLigneUnix() throws Exception
    {

        String encodage = "ISO8859_15";
        String findeligne = "\n";
        String typefindeligne = "Unix";
        String leNomDeFichier = this.cheminfichierexemple + encodage + typefindeligne + ".txt";

        ecrireFichierAvecMaitriseEncodageEtFinDeLigne(encodage, findeligne, typefindeligne, leNomDeFichier);

    }

    /**
     * methode Ecrire fichier avec maitrise encodage et fin de ligne : DGFiP.
     * 
     * @param encodage param
     * @param findeligne param
     * @param typefindeligne param
     * @param leNomDeFichier param
     * @throws IOException Signal qu'une execption de type I/O s'est produite.
     */
    private void ecrireFichierAvecMaitriseEncodageEtFinDeLigne(String encodage, String findeligne,
        String typefindeligne, String leNomDeFichier) throws IOException
    {
        File leFichier;
        try
        {
            leFichier = new File(new URL(leNomDeFichier).getFile());
        }
        catch (MalformedURLException e)
        {
            throw ApplicationExceptionTransformateur.transformer("la syntaxe de l'url spécifiée "
                + "pour le fichier code postaux est erronée", e);
        }

        OutputStreamWriter leWriter = null;
        try
        {
            leWriter = new OutputStreamWriter(new BufferedOutputStream(new FileOutputStream(leFichier)), encodage);

            for (int i = 0; i <= 10; i++)
            {
                leWriter.write("une ligne d'exemple " + encodage
                    + " avec des caractères accentués et séparateur pour editeur " + typefindeligne
                    + "uniquement <éçùàè€> " + i);
                leWriter.write(findeligne);
            }

        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer("probleme lors du traitement du fichier Exemple"
                + leFichier.getAbsolutePath(), exception);
        }
        finally
        {
            if (leWriter != null)
            {
                leWriter.close();
            }
        }
    }

    /**
     * methode Test lecture fichier avec maitrise encodage et fin de ligne windows : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testLectureFichierAvecMaitriseEncodageEtFinDeLigneWindows() throws Exception
    {
        String encodage = "Cp1252";
        String typefindeligne = "Windows";
        String leNomDefichier = this.cheminfichierexemple + encodage + typefindeligne + ".txt";

        //log.info("Debut testEcritureFichierAvecMaitriseEncodageEtFinDeLigneWindows : " + leNomDefichier);
        File leFichier;
        try
        {
            leFichier = new File(new URL(leNomDefichier).getFile());
            if(!leFichier.exists()) {
                ecrireFichierAvecMaitriseEncodageEtFinDeLigne(encodage, "\r\n", typefindeligne, leNomDefichier);
            }
        }
        catch (MalformedURLException e)
        {
            throw ApplicationExceptionTransformateur.transformer(
                "la syntaxe de l'url spécifiée pour le fichier code postaux est erronée", e);
        }

        BufferedReader leReader = null;
        String ligneFichierExemple;
        
        try
        {
            leReader = new BufferedReader(new InputStreamReader(new FileInputStream(leFichier), encodage));

            while ((ligneFichierExemple = leReader.readLine()) != null)
            {
             //   log.toString();
             //   log.info(ligneFichierExemple);
            }
        }
        catch (FileNotFoundException exception)
        {
            throw new ExploitationException("Attention le fichier exemple" + "n'a pas été trouvé sur ce chemin "
                + leFichier.getAbsolutePath() + " copiez le sur ce chemin ou corrigez "
                + " sa localisation dans le fichier " + " application.properties et relancez", exception);
        }
        catch (IOException exception)
        {
            throw ApplicationExceptionTransformateur.transformer("probleme lors du traitement du fichier Exemple"
                + leFichier.getAbsolutePath(), exception);
        }
        finally
        {
            if (leReader != null)
            {
                leReader.close();
            }
        }

    }

}
