/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ValidateurXMLXSDAvecDom.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exampleXML;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Class ValidateurXMLXSDAvecDom Exemple de validation d'un fichier XML avec JAXP en utilisant un XSD pourquoi a t on
 * utilse JAXP au lieu de sax et XMLReader : en cas de xsd incorrect , jaxp renvoit les erreurs du xsd alors que le
 * parser de xmlreader renvoit quil n y a aucune erreur de parsage.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 16 déc. 2009
 */
public class ValidateurXMLXSDAvecDom
{

    /** Constant : JAXP_SCHEMA_LANGUAGE. */
    private static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";

    /** Constant : W3C_XML_SCHEMA. */
    private static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";

    /** Constant : JAXP_SCHEMA_SOURCE. */
    private static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";

    /** Memo erreur validation. */
    public boolean validationError = false;

    /** résultat de validation *. */
    private boolean isValid = false;

    /** Le logger. */
    private final Log log = LogFactory.getLog(this.getClass());

    /** La réponse de la validation. */
    private String response = null;

    /**
     * Verifie si valid.
     * 
     * @return true, si c'est valid
     */
    public boolean isValid()
    {
        return isValid;
    }

    /**
     * on passe juste les noms de fichiers en entree méthode principale qui fait la validation.
     * 
     * @param xmlFile (nom du fichier)
     * @param xsdFile (nom du fichier)
     * @param recuperertoutesleserreurs param
     * @return XMLParseError[]
     */
    public String validerXMLParRapportXSD(String xmlFile, String xsdFile, String recuperertoutesleserreurs)
    {

        Validator handler = new Validator();

        try
        {
            // System.setProperty(DOM_FACTORY_NAME, DOM_FACTORY_IMPL);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setExpandEntityReferences(false);
            factory.setNamespaceAware(true);
            factory.setValidating(true);

            factory.setAttribute(JAXP_SCHEMA_LANGUAGE, W3C_XML_SCHEMA);
            // factory.setAttribute( "http://apache.org/xml/properties/schema/external-noNamespaceSchemaLocation",
            // xsdFile);
            factory.setAttribute(JAXP_SCHEMA_SOURCE, xsdFile);
            if (recuperertoutesleserreurs != null
                && recuperertoutesleserreurs.compareTo("recuperertoutesleserreurs") == 0)
            {
                // factory.setAttribute(CONTINUE_AFTER_FATAL_ERROR, Boolean.TRUE);
            }

            DocumentBuilder builder = factory.newDocumentBuilder();

            builder.setErrorHandler(handler);
            builder.parse(xmlFile);

            isValid = !handler.validationError;
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
                return response;
            }
        }
        catch (ParserConfigurationException e)
        {
            log.debug("ParserConfigurationException détectée");
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuffer sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuffer(response);
            }
            else
            {
                sbu = new StringBuffer("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(e.getMessage());
            return sbu.toString();

        }
        catch (IOException e)
        {
            log.debug("IOException détectée");
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuffer sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuffer(response);
            }
            else
            {
                sbu = new StringBuffer("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ");
            sbu.append(e.getMessage());
            return sbu.toString();

        }
        catch (SAXException e)
        {
            log.debug("SAXException détectée");
            if (handler.validationError)
            {
                response = calculerReponse(handler.saxParseExceptionList);
            }
            StringBuffer sbu;
            if (response != null && response.compareTo("") != 0)
            {
                sbu = new StringBuffer(response);
            }
            else
            {
                sbu = new StringBuffer("");
            }
            sbu.append("\n  ");
            sbu.append(" *** ").append(e.getMessage());
            return sbu.toString();

        }

        return response;
    }

    /**
     * methode Calculer reponse : DGFiP.
     * 
     * @param saxParseExceptionList param
     * @return string
     */
    private String calculerReponse(ArrayList<SAXParseException> saxParseExceptionList)
    {
        StringBuffer sbu = new StringBuffer();
        sbu.append("les erreurs suivantes ont ete detectees");
        for (int i = 0; i < saxParseExceptionList.size(); i++)
        {
            SAXParseException xcpt = (SAXParseException) saxParseExceptionList.get(i);
            sbu.append("\n  ").append(xcpt.getLineNumber());
            sbu.append(":").append(xcpt.getColumnNumber());
            sbu.append(" *** ").append(xcpt.getMessage());
        }
        return sbu.toString();
    }

    /**
     * Class Validator DGFiP.
     * 
     * @author chouard-cp
     * @version $Revision: 1.3 $ Date: 11 déc. 2009
     */
    private class Validator extends DefaultHandler
    {

        /** Memo erreur validation. */
        public boolean validationError = false;

        /** Memo Exception(s) sax. */
        public ArrayList<SAXParseException> saxParseExceptionList = new ArrayList<SAXParseException>();

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see org.xml.sax.helpers.DefaultHandler#error(org.xml.sax.SAXParseException)
         */
        public void error(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see org.xml.sax.helpers.DefaultHandler#fatalError(org.xml.sax.SAXParseException)
         */
        public void fatalError(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }

        /**
         * (methode de remplacement) {@inheritDoc}
         * 
         * @see org.xml.sax.helpers.DefaultHandler#warning(org.xml.sax.SAXParseException)
         */
        public void warning(final SAXParseException exception)
        {
            validationError = true;
            saxParseExceptionList.add(exception);
        }
    }

}
