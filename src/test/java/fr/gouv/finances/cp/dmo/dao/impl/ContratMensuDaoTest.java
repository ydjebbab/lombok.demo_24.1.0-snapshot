package fr.gouv.finances.cp.dmo.dao.impl;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IContratMensuDao;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.Rib;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.test.config.PersistenceJPAConfigTest;

/**
 * Classe de test du DAO ContratMensuDao
 * 
 * @author celfer Date: 14 janv. 2019
 */
/*
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = {"embedded", "jpa", "edition", "atlas", "journal-ws", "itm", "bancaire", "sireme", "clamav-simulation",
        "springbatch", "jdbc", "nowebflowcsrf", "adresse", "journal"})
@ContextConfiguration(locations = {
        "classpath:conf/testpropertysource.xml",
        "classpath:conf/app-config.xml",
        "classpath:conf/batch/batchContext-resources.xml"})
@Transactional
*/

@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = {"embedded", "jpa"})
@Rollback(true)
@Transactional
@SpringBootApplication
@SpringBootTest(classes = {PersistenceJPAConfigTest.class})
public class ContratMensuDaoTest
{
    private static final Logger log = LoggerFactory.getLogger(ContratMensuDaoTest.class);

    @Resource
    protected IContratMensuDao contratmensudao;

    @Test
    @Rollback(true)
    public void testEnregistrerCommeTraiteContratsVerifiantRG70()
    {
        int nbContratsTraites = contratmensudao.saveCommeTraiteContratsVerifiantRG70();
        assertEquals(4, nbContratsTraites);
    }

    @Test
    public void testFindContratsNeVerifiantPasRG70()
    {
        List<ContratMensu> contratsNeVerifiantPasRG70Liste = contratmensudao.findContratsNeVerifiantPasRG70(0, 10);
        log.debug("contratsNeVerifiantPasRG70Liste : " + contratsNeVerifiantPasRG70Liste);
        assertEquals(4, contratsNeVerifiantPasRG70Liste.size());
    }

    @Test
    public void testFindContratsNonTraiteVerifiantRG70()
    {
        List<ContratMensu> contratsVerifiantPasRG70Liste = contratmensudao.findContratsNonTraiteVerifiantRG70(0, 10);
        log.debug("contratsVerifiantPasRG70Liste : " + contratsVerifiantPasRG70Liste);
        assertEquals(0, contratsVerifiantPasRG70Liste.size());
    }

    @Test
    public void testGetContratpourContribuableEtTypeImpot()
    {
        ContribuablePar contribuablePar = new ContribuablePar();
        contribuablePar.setId(Long.valueOf(20226));

        TypeImpot typeImpot = new TypeImpot();
        typeImpot.setId(Long.valueOf(20000));

        ContratMensu contratMensu = contratmensudao.findContratpourContribuableEtTypeImpot(contribuablePar, typeImpot);
        log.debug("contratMensu : " + contratMensu);

        assertEquals(Long.valueOf(20002), contratMensu.getId());
    }

    @Test
    public void testGetNombreContratsNonTraiteNeVerifiantPasRG70()
    {
        int nbContrats = contratmensudao.countNombreContratsNonTraiteNeVerifiantPasRG70();
        log.debug("nbContrats : " + nbContrats);
        assertEquals(4, nbContrats);
    }

    @Test
    public void testGetNombreContratsNonTraiteVerifiantRG70()
    {
        int nbContrats = contratmensudao.countNombreContratsNonTraiteVerifiantRG70();
        log.debug("nbContrats : " + nbContrats);
        assertEquals(0, nbContrats);
    }

    @Test
    public void testReporterAnneePriseEffetPourContratsNonTraitesNeVerifiantPasRG71()
    {
        int nbContrats = contratmensudao.reporterAnneePriseEffetPourContratsNonTraitesNeVerifiantPasRG71();
        log.debug("nbContrats : " + nbContrats);
        assertEquals(0, nbContrats);
    }

    @Test
    @Rollback(false)
    public void testSaveContratmensualisation()
    {
        ContratMensu contratMensu = new ContratMensu();
        ContribuablePar contribuablepar = new ContribuablePar();
        contribuablepar.setId(Long.valueOf(20225));
        contratMensu.setContribuablePar(contribuablepar);

        TypeImpot typeImpot = new TypeImpot();
        typeImpot.setId(Long.valueOf(20000));
        contratMensu.setTypeImpot(typeImpot);

        Rib rib = new Rib();
        rib.setCodeBanque("30001");
        rib.setCodeGuichet("00200");
        rib.setNumeroCompte("0000Y055052");
        contratMensu.setRib(rib);
        
        contratMensu.setNumeroContrat(Long.valueOf(9852144));
        contratMensu.setEstTraite(false);
        contratMensu.setDateDemande(new Date());

        contratmensudao.saveContratMensualisation(contratMensu);
        // TODO : assertion à faire
    }
    
    @Test
    @Rollback(false)
    public void testSupprimerContratsNeVerifiantPasRG70()
    {
        int nbContratsSupprimes = contratmensudao.deleteContratsNeVerifiantPasRG70();
        log.debug("nbContratsSupprimes : " + nbContratsSupprimes);
        assertEquals(4, nbContratsSupprimes);
    }
   
}
