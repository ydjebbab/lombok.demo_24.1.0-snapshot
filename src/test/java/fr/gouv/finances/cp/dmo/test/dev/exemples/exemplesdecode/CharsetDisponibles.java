/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : CharsetDisponibles.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.SortedMap;

import junit.framework.TestCase;

/**
 * Class CharsetDisponibles DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class CharsetDisponibles extends TestCase
{
    /**
     * methode Test encoding : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testEncoding() throws Exception
    {
        SortedMap sort = Charset.availableCharsets();

        Set keys = sort.keySet();
        for (Iterator iter = keys.iterator(); iter.hasNext();)
        {
            Object key = iter.next();

            // CF :mise en commentaire
            // System.out.println(((Charset) sort.get(key)).displayName(Locale.FRANCE) + ";"
            // + ((Charset) sort.get(key)).name());
        }
    }
}
