/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementPurgeEditionsTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.batch;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.edition.batch.TraitementPurgeEditionsImpl;
import org.junit.Assert;

/**
 * Class TraitementPurgeEditionsTest.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 14 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext
@ActiveProfiles(profiles = {"embedded", "hibernate", "jdbc", "upload", "journal", "edition"})
@ContextConfiguration(locations = {
        "classpath:conf/testpropertysource.xml",
        "classpath:conf/app-config.xml",
        "classpath:conf/batch/batchContext-resources.xml",
        "classpath:conf/batch/purgeeditions/traitementpurgeeditionsContext-service.xml"})
@Transactional(transactionManager = "editionTransactionManager")
@Rollback(false)
public class TraitementPurgeEditionsTest
{
    @Autowired
    ApplicationContext batchContext;

    /*
     * protected String[] ajouterConfigsLocationsSpecifiques() { return new String[] {
     * "file:$localisations{appli.batch.root}/purgeeditions/traitementpurgeeditionsContext-service.xml",
     * "file:$localisations{appli.web.root}/WEB-INF/conf/commun/applicationContext-commun-edition.xml"}; }
     */

    /**
     * methode Test traiter batch : DGFiP.
     */
    @Test
    public void testTraiterBatch()
    {
        // this.endTransaction();

        TraitementPurgeEditionsImpl sbc =
            (TraitementPurgeEditionsImpl) batchContext.getBean("traitementpurgeeditionsimpl");
        sbc.demarrer();
        int statuscode = sbc.demarrer();
        Assert.assertEquals(0, statuscode);

    }

}