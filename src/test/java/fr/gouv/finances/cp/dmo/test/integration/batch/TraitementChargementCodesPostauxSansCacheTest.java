/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementChargementCodesPostauxSansCacheTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.batch;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.batch.ServiceBatchCommun;

/**
 * Class TraitementChargementCodesPostauxSansCacheTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = {"embedded", "hibernate", "jdbc", "upload", "journal", "adresse"})
@ContextConfiguration(locations = {
        "classpath:conf/testpropertysource.xml",
        "classpath:conf/app-config.xml",
        "classpath:conf/batch/batchContext-resources.xml",
        "classpath:conf/batch/chargementcodespostauxsanscache/traitementchargementcodespostauxsanscacheContext-service.xml"})
@Rollback(false)
@Transactional
public class TraitementChargementCodesPostauxSansCacheTest
{
    @Autowired
    private ApplicationContext appContext;

    /**
     * methode Test traiter batch : DGFiP.
     */
    @Test
    public void testTraiterBatch()
    {
        ServiceBatchCommun sbc =
            (ServiceBatchCommun) appContext.getBean("traitementchargementcodespostauxsanscacheimpl");

        int statuscode = sbc.demarrer();
        Assert.assertEquals(0, statuscode);
    }

}
