/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.test.integration.ihm;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;

/**
 * Classe de test comportant une méthode et une bonne configuration.
 */
@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LogonTestWebDriver
{
    @Autowired
    WebApplicationContext context;

    private WebClient webClient;

    @LocalServerPort
    int randomServerPort;

    @Value("${server.context-path}")
    String contextPath;

    @Before
    public void setup()
    {
        webClient = new WebClient(BrowserVersion.EDGE);
        webClient.getOptions().setJavaScriptEnabled(true);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
    }

    @After
    public void close() throws Exception
    {
        webClient.close();
    }

    /**
     * méthode de Test : .
     * 
     * @throws IOException
     * @throws MalformedURLException
     * @throws FailingHttpStatusCodeException
     */
    @Test
    public void testUntitled() throws FailingHttpStatusCodeException, MalformedURLException, IOException
    {

        String url = "http://localhost:" + randomServerPort + contextPath;
        HtmlPage logonPage = webClient.getPage(url);

        HtmlForm logonForm = (HtmlForm) logonPage.getElementById("formulaire");
        logonForm.getInputByName("username").setValueAttribute("chouard-cp");
        logonForm.<HtmlPasswordInput> getInputByName("password").setValueAttribute("0");

        // ScriptResult
        // scriptResult=logonPage.executeJavaScript("stocketransition('_eventId_ok');removeInputEventId();initecevefn();");
        // HtmlPage accueilPage = (HtmlPage) scriptResult.getNewPage();

        HtmlPage accueilPage = logonPage.<HtmlSubmitInput> getHtmlElementById("enterButton").click();

        webClient.waitForBackgroundJavaScript(3);

        System.out.println(accueilPage.asXml());
        System.out.println(accueilPage.getTitleText());
        /*
         * driver.get("identification.ex"); WebElement login = driver.findElement(By.id("username"));
         * login.sendKeys("chouard-cp"); WebElement pwd = driver.findElement(By.id("password")); pwd.sendKeys("0");
         * pwd.submit(); (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() { public Boolean
         * apply(WebDriver d) { return (d.getTitle() != null); } }); Assert.assertEquals("DMO", driver.getTitle());
         * driver.quit();
         */

    }
}
