/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.dao;

import java.math.BigDecimal;
import java.text.NumberFormat;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IReferenceDao;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.test.config.PersistenceJPAConfigTest;
import fr.gouv.finances.lombok.util.format.FormaterNombre;

/**
 * Classe de test du DAO ReferenceDao
 * 
 * @author chouard-cp
 * @author CF
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = {"embedded", "jpa"})
@Rollback(true)
@Transactional
@SpringBootApplication
@SpringBootTest(classes = {PersistenceJPAConfigTest.class})
public class ReferenceDaoTest
{
    @Autowired
    protected IReferenceDao referencedao;

    private TypeImpot untypeimpot;

    /**
     * methode Testpertedeprecisionenbase
     * 
     * @throws Exception the exception
     */
    @Test
    public void testpertedeprecisionenbase() throws Exception
    {
        NumberFormat unNumberFormat = FormaterNombre.getFormatNombreNDecimales(10, true);

        Long code = Long.valueOf(1999);
        untypeimpot = referencedao.findTypeImpotParCodeImpot(code);
        if (untypeimpot != null)
        {
            referencedao.deleteObject(untypeimpot);
        }

        untypeimpot = new TypeImpot();
        untypeimpot.setCodeImpot(code);
        untypeimpot.setNomImpot("Impôt n° " + code);
        untypeimpot.setEstOuvertALaMensualisation(Boolean.TRUE);

        // NUMBER(19,10) : Max précision = 9 chiffres + 10 après la virgule
        BigDecimal taux = new BigDecimal("779879879.2525252525");

        String memoavantbase = unNumberFormat.format(taux);

        untypeimpot.setTaux(taux);
        referencedao.saveTypeImpot(untypeimpot);
        untypeimpot = referencedao.findTypeImpotParCodeImpot(code);

        String memoapresbase = unNumberFormat.format(untypeimpot.getTaux());
        Assert.assertEquals(memoavantbase, memoapresbase);
    }

}
