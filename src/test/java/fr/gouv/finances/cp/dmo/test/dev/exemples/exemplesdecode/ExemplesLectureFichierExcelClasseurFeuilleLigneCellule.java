/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ExemplesLectureFichierExcelClasseurFeuilleLigneCellule.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import java.io.FileInputStream;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import fr.gouv.finances.cp.dmo.test.integration.util.FichierUtil;

/**
 * Class ExemplesLectureFichierExcelClasseurFeuilleLigneCellule DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ExemplesLectureFichierExcelClasseurFeuilleLigneCellule extends TestCase
{

    /** Constant : fichierxls. */
    static final String fichierxls =
        FichierUtil.recupererClassPath() + "/fr/gouv/finances/cp/dmo/test/dev/exemples/exemplesdecode/test1.xls";

    // static final String fichierxls="C:/testsxls/fichietestsfichierechangeORCHIDEEV4.xls";

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * Instanciation de exemples lecture fichier excel classeur feuille ligne cellule.
     */
    public ExemplesLectureFichierExcelClasseurFeuilleLigneCellule()
    {
    };

    /**
     * methode Testlecture : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testlecture() throws Exception
    {
        log.info("Debut");

        HSSFWorkbook classeur;

        FileInputStream fin = new FileInputStream(fichierxls);
        POIFSFileSystem poifs = new POIFSFileSystem(fin);
        // false = on ne prend pas les macros
        classeur = new HSSFWorkbook(poifs, false);

        for (int i = 0; i < classeur.getNumberOfSheets(); i++)
        {
           // log.info("Lecture Feuille : " + classeur.getSheetName(i));
            if (classeur.getSheetAt(i) != null)
            {
                this.traiterFeuille(classeur.getSheetAt(i));
            }
        }
        fin.close();
        log.info("Fin");

    }

    /**
     * methode Traiter feuille : DGFiP.
     * 
     * @param feuille param
     */
    private void traiterFeuille(HSSFSheet feuille)
    {
        //log.info("Premiere Ligne de la feuille :" + feuille.getFirstRowNum());
        //log.info("Derniere Ligne de la feuille :" + feuille.getLastRowNum());
        for (int i = feuille.getFirstRowNum(); i <= feuille.getLastRowNum(); i++)
        {
            // log.info("Lecture Ligne : " + i);
            if (feuille.getRow(i) != null)
            {
                this.traiterLigne(feuille.getRow(i));
            }
        }

    }

    /**
     * methode Traiter ligne : DGFiP.
     * 
     * @param ligne param
     */
    private void traiterLigne(HSSFRow ligne)
    {
        // log.info("Premiere Cellule de la ligne :" + ligne.getFirstCellNum());
        // log.info("Derniere Cellule de la ligne :" + ligne.getLastCellNum());
        for (short i = ligne.getFirstCellNum(); i <= ligne.getLastCellNum(); i++)
        {
            // log.info("Lecture Cellule : " + i);
            if (ligne.getCell(i) != null)
            {
                switch (ligne.getCell(i).getCellType())
                {
                    case HSSFCell.CELL_TYPE_STRING:
                        // log.info("Valeur string : " + ligne.getCell(i).getStringCellValue());
                        break;
                    case HSSFCell.CELL_TYPE_NUMERIC:
                        // log.info("Valeur numerique : " + ligne.getCell(i).getNumericCellValue());
                        // log.info("Valeur numerique lue comme une date : " + ligne.getCell(i).getDateCellValue());
                        break;
                    case HSSFCell.CELL_TYPE_FORMULA:
                        
                        /*
                         * log.info("Formule : " + ligne.getCell(i).getCellFormula()); try {
                         * log.info("Formule lue comme valeur numerique " + ligne.getCell(i).getCellFormula()); } catch
                         * (Exception e) { log.info("Formule lue comme valeur string " +
                         * ligne.getCell(i).getCellFormula()); }
                         */
                        break;
                    case HSSFCell.CELL_TYPE_BOOLEAN:
                        // log.info("Valeur booleenne : " + ligne.getCell(i).getBooleanCellValue());
                    default:
                        break;
                }
            }
            else
            {
                // log.info("Cellule nulle");
            }
        }

    }

}