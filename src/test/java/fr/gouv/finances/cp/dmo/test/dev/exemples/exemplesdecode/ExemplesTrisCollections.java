/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ExemplesTrisCollections.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparableComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.commons.collections.comparators.ReverseComparator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.bean.PaysIso;

/**
 * Class ExemplesTrisCollections DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ExemplesTrisCollections extends TestCase
{

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /**
     * methode Test tri de collections : DGFiP.
     */
    public void testTriDeCollections()
    {
        List<PaysIso> liste = new ArrayList<PaysIso>();
        PaysIso p1 = new PaysIso();
        PaysIso p2 = new PaysIso();
        PaysIso p3 = new PaysIso();
        
        p1.setNom("Bulgarie");
        p2.setNom("Russie");
        p3.setNom("Algerie");
        liste.add(p1);
        liste.add(p2);
        liste.add(p3);

        // log.info("Debut test tri de collections");
        Comparator<PaysIso> c = new CritereParNom();
        Collections.sort(liste, c);

        Iterator iter = liste.iterator();
        while (iter.hasNext())
        {
            PaysIso unpays = (PaysIso) (iter.next());
            //log.info(unpays.getNom());
        }
    }
    
    
    /**
     * Class CritereParNom DGFiP.
     * 
     * @author chouard-cp
     * @version $Revision: 1.3 $ Date: 11 déc. 2009
     */
    public class CritereParNom implements Comparator<PaysIso>
    {
        
        /** 
         *(methode de remplacement)
         * {@inheritDoc}
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        public int compare(PaysIso o1, PaysIso o2)
        {
            return o1.getNom().compareTo(o2.getNom());
        }

    }

    /**
     * methode Test tri de collections avec bean utils : DGFiP.
     */
    public void testTriDeCollectionsAvecBeanUtils()
    {
        List<PaysIso> liste = new ArrayList<PaysIso>();
        PaysIso p1 = new PaysIso();
        PaysIso p2 = new PaysIso();
        PaysIso p3 = new PaysIso();
        
        p1.setNom("Bulgarie");
        p2.setNom("Russie");
        p3.setNom("Algerie");
        liste.add(p1);
        liste.add(p2);
        liste.add(p3);

        //log.info("Debut test tri de collections avec utilitaire beanutils");
        Comparator bc = new BeanComparator("nom");
        Collections.sort(liste, (Comparator< ? super PaysIso>) bc);

        Iterator iter = liste.iterator();
        while (iter.hasNext())
        {
            PaysIso unpays = (PaysIso) (iter.next());
            // log.info(unpays.getNom());
        }

        List<CodePostal> listeCodePostaux = new ArrayList<CodePostal>();
        CodePostal c1 = new CodePostal();
        
        c1.setCode("93360");
        c1.setVille("AAA");
        listeCodePostaux.add(c1);
        CodePostal c2 = new CodePostal();
        c2.setCode("93360");
        c2.setVille("CCC");
        listeCodePostaux.add(c2);
        CodePostal c3 = new CodePostal();
        c3.setCode("93360");
        c3.setVille("BBB");
        listeCodePostaux.add(c3);
        CodePostal c4 = new CodePostal();
        c4.setCode("92360");
        c4.setVille("AAA");
        listeCodePostaux.add(c4);
        CodePostal c5 = new CodePostal();
        c5.setCode("92360");
        c5.setVille("CCC");
        listeCodePostaux.add(c5);
        CodePostal c6 = new CodePostal();
        c6.setCode("92360");
        c6.setVille("BBB");
        listeCodePostaux.add(c6);
        CodePostal c7 = new CodePostal();
        c7.setCode("92360");
        c7.setVille("bbb");
        listeCodePostaux.add(c7);
        CodePostal c8 = new CodePostal();
        c8.setCode("92360");
        c8.setVille("ccc");
        listeCodePostaux.add(c8);
        // Tri ascendant sur les propriétés code et ville
        List<Comparator> sortFields = new ArrayList<Comparator>();
        sortFields.add(new BeanComparator("code"));
        sortFields.add(new BeanComparator("ville"));
        ComparatorChain multiSort = new ComparatorChain(sortFields);
        Collections.sort(listeCodePostaux, (Comparator< ? super CodePostal>) multiSort);

        // log.info("Résutat du tri ascendant sur les propriétés code puis ville");
        iter = listeCodePostaux.iterator();
        while (iter.hasNext())
        {
            CodePostal uncodepostal = (CodePostal) (iter.next());
            // log.info(uncodepostal.getCode() + " - " + uncodepostal.getVille());
        }

        // Tri ascendant sur la propriété code, puis descendant sur ville

        sortFields = new ArrayList<Comparator>();
        sortFields.add(new BeanComparator("code"));
        sortFields.add(new BeanComparator("ville", new ReverseComparator(new ComparableComparator())));
        multiSort = new ComparatorChain(sortFields);
        Collections.sort(listeCodePostaux, (Comparator< ? super CodePostal>) multiSort);

        // log.info("Résutat du tri ascendant sur la propriété code, puis descendant sur  ville");
        iter = listeCodePostaux.iterator();
        while (iter.hasNext())
        {
            CodePostal uncodepostal = (CodePostal) (iter.next());
            //log.info(uncodepostal.getCode() + " - " + uncodepostal.getVille());
        }

        // Tri descendant insensible à la casse
        List<Comparator> sortFields1 = new ArrayList<Comparator>();

        BeanComparator reversedOrderCaseInsensitive =
            new BeanComparator("ville", new ReverseComparator(String.CASE_INSENSITIVE_ORDER));

        sortFields1.add(reversedOrderCaseInsensitive);

        ComparatorChain multiSort1 = new ComparatorChain(sortFields1);

        Collections.sort(listeCodePostaux, (Comparator< ? super CodePostal>) multiSort1);
        //log.info("Résutat du tri descendant insensible à la casse sur la propriété code, puis descendant sur  ville");
        iter = listeCodePostaux.iterator();
        while (iter.hasNext())
        {
            CodePostal uncodepostal = (CodePostal) (iter.next());
            // log.info(uncodepostal.getCode() + " - " + uncodepostal.getVille());
        }

    }

    /**
     * methode Test lecture date from string : DGFiP.
     */
    public void testLectureDateFromString()
    {
        DateTime uneDate;
        DateTimeFormatter unFormat = DateTimeFormat.forPattern("dd/MM/yyyy");
        String texteDateOK = "12/01/2006";
        String texteDateKO = "12/13/2006";
        String texteDateFormatErrone = "12012006";

        log.info("Debut test lecture date from string");
        uneDate = unFormat.parseDateTime(texteDateOK);
        //log.info(uneDate);

        try
        {
            unFormat.parseDateTime(texteDateKO);
            fail("Pas d'exception alors que Date erronee");
        }
        catch (IllegalArgumentException e)
        {
            log.info(e.getMessage());
        }

        try
        {
            unFormat.parseDateTime(texteDateFormatErrone);
            fail("Pas d'exception alors que Format errone");
        }
        catch (IllegalArgumentException e)
        {
            log.info(e.getMessage());
        }

    }

}