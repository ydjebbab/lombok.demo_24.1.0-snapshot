/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : JoursFeriestest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.bean;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import fr.gouv.finances.lombok.util.date.JoursFeries;

/**
 * Class de test
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class JoursFeriesTest
{
    /**
     * methode Test nombre jours ouvres
     */
    @Test
    public void testNombreJoursOuvres()
    {
        Calendar cal = Calendar.getInstance();

        // Date du 27/04/07
        cal.set(2007, 3, 27);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        Date date27042007 = cal.getTime();

        // Date du 04/05/07
        cal.set(2007, 4, 4);
        Date date04052007 = cal.getTime();

        int nbJoursSansSamETDim =
            JoursFeries.calculeNombreJoursSansSamedisEtDimanchesEntreDeuxDates(date27042007, date04052007);
        // System.out.println("nb jours hors samedis et dimanches " + nbJoursSansSamETDim);

        int nbJoursFeriesSamETDim =
            JoursFeries.calculeNombreJoursFeriesHorsSamediEtDimancheEntreDeuxDates(date27042007, date04052007);
        // System.out.println("nb jours fériés hors samedis et dimanches" + nbJoursFeriesSamETDim);

        int nbJoursbornesincluses = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(date27042007, date04052007);

        // System.out.println("nb jours bornes incluses " + nbJoursbornesincluses);

        // Date du 31/03/07
        cal.set(2007, 2, 31);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        Date date31032007 = cal.getTime();

        // Date du 03/04/07
        cal.set(2007, 3, 3);
        Date date03042007 = cal.getTime();

        nbJoursSansSamETDim =
            JoursFeries.calculeNombreJoursSansSamedisEtDimanchesEntreDeuxDates(date31032007, date03042007);
        // System.out.println("nb jours hors samedis et dimanches " + nbJoursSansSamETDim);

        nbJoursFeriesSamETDim =
            JoursFeries.calculeNombreJoursFeriesHorsSamediEtDimancheEntreDeuxDates(date31032007, date03042007);
        // System.out.println("nb jours fériés hors samedis et dimanches" + nbJoursFeriesSamETDim);

        nbJoursbornesincluses = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(date31032007, date03042007);

        // System.out.println("nb jours bornes incluses " + nbJoursbornesincluses);
    }
}
