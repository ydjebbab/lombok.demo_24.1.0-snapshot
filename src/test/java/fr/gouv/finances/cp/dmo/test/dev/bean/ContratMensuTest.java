/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ContratMensuTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.bean;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Class ContratMensuTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ContratMensuTest extends AbstractCorePojoTest<ContratMensuTest>
{
    /**
     * Test de la méthode de vérification de la péremption du contrat de mensualisation.
     * 
     * @throws ParseException erreur de conversion de date
     */
    @Test
    public void testVerifierNonPerimeRG70() throws ParseException
    {        
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        // on crée un premier contrat de mensualisation cm1 qui est périmé
        String strdate1 = "01/01/2006";
        Date date1 = sdf.parse(strdate1);
        ContratMensu cm1 = new ContratMensu(null, null, date1, null, null, null, null, null);

        // on crée un second contrat cm2 non périmé
        Date date2 = new Date();
        ContratMensu cm2 = new ContratMensu(null, null, date2, null, null, null, null, null);

        // on vérifie que le test de péremption sur les 2 contrats est correct
        Assert.assertTrue(cm1.verifierNonPerimeRG70());
        Assert.assertFalse(cm2.verifierNonPerimeRG70());
    }
}
