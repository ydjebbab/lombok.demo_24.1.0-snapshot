/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ExemplesExpressionsRegulieres.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import junit.framework.TestCase;

/**
 * Class ExemplesExpressionsRegulieres DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ExemplesExpressionsRegulieres extends TestCase
{

    /**
     * methode Test split : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testSplit() throws Exception
    {
        String unechaine = "Agence Régionale d'Hospitalisation";

        String[] result = unechaine.split("[\\s']");

        /*
        for (int x = 0; x < result.length; x++)
        {
            System.out.println(result[x]);
        }
        */
    }
}
