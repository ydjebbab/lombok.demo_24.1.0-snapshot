/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TrcPurgeEditiondaoTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.edition.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.transaction.PlatformTransactionManager;

import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;
import fr.gouv.finances.lombok.edition.dao.TrcPurgeEditionDao;
import fr.gouv.finances.lombok.util.base.LombokTransactionDAOTest;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * Class TrcPurgeEditiondaoTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringRunner.class)
@LombokTransactionDAOTest

public class TrcPurgeEditiondaoTest // extends BaseDAOEditionTestCase
{

    /** logger, le fichier log4j.properties sera trouvé grâce à LombokJunit4ClassRunner. */
    protected final Log log = LogFactory.getLog(getClass());

    @Autowired
    protected PlatformTransactionManager editionTransactionManager;

    /** trcpurgeeditiondao. */
    @Autowired
    protected TrcPurgeEditionDao trcpurgeeditiondao;

    @Autowired
    protected ApplicationContext context;

    /** application origine. */
    private String applicationOrigine = "DMO";

    @AfterTransaction
    public void onTearDownInTransaction() throws Exception
    {
        SessionFactory sessionFactory = (SessionFactory) context.getBean("editionhibernatesessionfactory");
        HibernateTemplate hibernateTemplate = new HibernateTemplate(sessionFactory);

        hibernateTemplate.flush();
    }

    /**
     * methode Test controle deux purges date debut identiques : DGFiP.
     * 
     * @throws Exception the exception
     */
    @Test
    public void testControleDeuxPurgesDateDebutIdentiques() throws Exception
    {
        TrcPurgeEdition trcPurgeEdition1 = new TrcPurgeEdition();
        // trcPurgeEdition1.demarreExecution(applicationOrigine);
        trcPurgeEdition1.setDateDebutPurge(new Date(1));
        trcPurgeEdition1.setDateFinPurge(new Date(1000));
        trcPurgeEdition1.setEtatPurge(TrcPurgeEdition.EN_ATTENTE_DEMARRAGE);
        trcPurgeEdition1.setApplicationOrigine(applicationOrigine);
        trcPurgeEdition1.demarreExecution();
        trcpurgeeditiondao.saveTrcPurgeEdition(trcPurgeEdition1);

        TrcPurgeEdition trcPurgeEdition2 = new TrcPurgeEdition();
        trcPurgeEdition2.setApplicationOrigine(applicationOrigine);
        trcPurgeEdition2.setDateDebutPurge(trcPurgeEdition1.getDateDebutPurge());
        boolean existedeja = trcpurgeeditiondao.checkExistencePurgeDateIdentique(trcPurgeEdition2);

        log.info("existedeja =" + existedeja);
        log.info("trc1 : "
            + FormaterDate.formatDateHeureMinuteSecondeMillisecondeCourt(trcPurgeEdition1.getDateDebutPurge()));
        log.info("trc2 : "
            + FormaterDate.formatDateHeureMinuteSecondeMillisecondeCourt(trcPurgeEdition2.getDateDebutPurge()));

        Assert.assertEquals(existedeja, true);
    }

    /**
     * methode Test controle deux purges date debut non identiques : DGFiP.
     * 
     * @throws Exception the exception
     */
    @Test
    public void testControleDeuxPurgesDateDebutNonIdentiques() throws Exception
    {
        TrcPurgeEdition trcPurgeEdition1 = new TrcPurgeEdition();
        trcPurgeEdition1.setDateDebutPurge(Calendar.getInstance().getTime());
        trcPurgeEdition1.setEtatPurge(TrcPurgeEdition.EN_ATTENTE_DEMARRAGE);
        trcPurgeEdition1.setDateDebutPurge(new Date(1));
        trcPurgeEdition1.setDateFinPurge(new Date(1000));
        trcPurgeEdition1.setApplicationOrigine(applicationOrigine);
        trcPurgeEdition1.demarreExecution();
        trcpurgeeditiondao.saveTrcPurgeEdition(trcPurgeEdition1);

        Calendar cal = Calendar.getInstance(Locale.FRANCE);
        cal.setTime(trcPurgeEdition1.getDateDebutPurge());
        cal.add(Calendar.MILLISECOND, 1);

        TrcPurgeEdition trcPurgeEdition2 = new TrcPurgeEdition();
        trcPurgeEdition2.setApplicationOrigine(applicationOrigine);
        trcPurgeEdition2.setDateDebutPurge(cal.getTime());

        boolean existedeja = trcpurgeeditiondao.checkExistencePurgeDateIdentique(trcPurgeEdition2);

        log.info("existedeja =" + existedeja);
        log.info("trc1 : "
            + FormaterDate.formatDateHeureMinuteSecondeMillisecondeCourt(trcPurgeEdition1.getDateDebutPurge()));
        log.info("trc2 : "
            + FormaterDate.formatDateHeureMinuteSecondeMillisecondeCourt(trcPurgeEdition2.getDateDebutPurge()));

        Assert.assertEquals(existedeja, false);
    }

    /**
     * methode Test enregistrement purge en cours : DGFiP.
     * 
     * @throws Exception the exception
     */
    @Test
    public void testEnregistrementPurgeEnCours() throws Exception
    {
        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.setApplicationOrigine(applicationOrigine);
        trcPurgeEdition.setDateDebutPurge(new Date(1));
        trcPurgeEdition.setDateDebutPurge(new Date(1000));
        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.EN_ATTENTE_DEMARRAGE);
        trcPurgeEdition.demarreExecution();

        trcpurgeeditiondao.saveTrcPurgeEdition(trcPurgeEdition);

        List<TrcPurgeEdition> lesPurgesPourUnInstant =
            trcpurgeeditiondao.findLesTrcPurgeEditionPourUnInstantDemarrage(trcPurgeEdition.getDateDebutPurge(),
                applicationOrigine);

        for (Iterator<TrcPurgeEdition> iter = lesPurgesPourUnInstant.iterator(); iter.hasNext();)
        {
            TrcPurgeEdition eltrc = iter.next();
            log.info("Date début purge : " + eltrc.getDateDebutPurge());
        }

        Assert.assertEquals(lesPurgesPourUnInstant.size(), 1);
    }

}
