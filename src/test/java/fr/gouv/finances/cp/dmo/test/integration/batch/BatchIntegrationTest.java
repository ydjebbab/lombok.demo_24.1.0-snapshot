/*
 * Copyright (c) 2018 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.test.integration.batch;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.gouv.finances.cp.dmo.batch.BatchLauncher;

/**
 * Test d'intégration des batchs du projet.
 */
@RunWith(Parameterized.class)
public class BatchIntegrationTest
{

    /** map. */
    private String[] args;

    /** exit code. */
    private int exitCode;

    /**
     * Parametres pour chaque iteration correspondant à l'exécution d'un batch.
     *
     * @return collection
     */
    @Parameters
    public static Collection<Object[]> configs()
    {

        return Arrays.asList(new Object[][] {
              /*  {new String[0], 7701}, // pas de nom de batch passé*/
                {new String[] {"batchquinexistepas"}, 1}, // nom de batch non présent dans le contexte
                {new String[] {"traitementadhesionsmensualisation"}, 0},
                {new String[] {"traitementchangementdelacassedetouslesnomsdecontribuables"}, 0},
        });
    }

    /**
     * Instanciation de batch integration test.
     *
     * @param map DOCUMENTEZ_MOI
     * @param exitCode DOCUMENTEZ_MOI
     */
    public BatchIntegrationTest(String[] args, int exitCode)
    {
        this.args = args;
        this.exitCode = exitCode;
    }

    /** exit. */
    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    /**
     * Test le batch en s'attendant à un code retour précis.
     */
    @Test
    public void traiterBatch()
    {
        exit.expectSystemExitWithStatus(exitCode);
        BatchLauncher.main(args);
    }
}
