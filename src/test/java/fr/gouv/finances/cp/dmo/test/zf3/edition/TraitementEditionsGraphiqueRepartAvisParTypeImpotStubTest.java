/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.test.zf3.edition;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import org.junit.Test;

import fr.gouv.finances.cp.dmo.techbean.StatistiquesContribuables;
import fr.gouv.finances.cp.dmo.zf3.edition.TraitementEditionsGraphiqueRepartAvisParTypeImpotStub;

public class TraitementEditionsGraphiqueRepartAvisParTypeImpotStubTest
{
    @Test
    public void testTraitementEditionsGraphiqueRepartAvisParTypeImpotStub() throws Exception
    {
        new TraitementEditionsGraphiqueRepartAvisParTypeImpotStub();
    }

    @Test
    public void testCreateBeanCollection() throws Exception
    {
        Collection<StatistiquesContribuables> beanCollection =
            TraitementEditionsGraphiqueRepartAvisParTypeImpotStub
                .createBeanCollection();
        assertNotNull(beanCollection);
        assertEquals(4, beanCollection.size());
    }
}
