/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ProductionEditionCsvTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.edition.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.PlatformTransactionManager;

import fr.gouv.finances.cp.dmo.test.integration.util.FichierUtil;
import fr.gouv.finances.lombok.edition.bean.EditionSynchroneNonNotifieeParMail;
import fr.gouv.finances.lombok.edition.service.EditionDemandeService;
import fr.gouv.finances.lombok.securite.techbean.PersonneAnnuaire;
import fr.gouv.finances.lombok.upload.bean.FichierJoint;

/**
 * Class ProductionEditionCsvTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductionEditionTest // extends BaseServiceEditionTestCase
{

    @Rule
    public final TemporaryFolder folderTemp = new TemporaryFolder();

    @Autowired
    protected PlatformTransactionManager editionTransactionManager;

    /** Constant : REPBASE. */
    private static final String REPBASE =
        FichierUtil.recupererClassPath() + "/fr/gouv/finances/cp/dmo/test/dev/edition/service/";

    /** editiondemandeserviceso. */
    @Autowired
    protected EditionDemandeService editiondemandeserviceso;

    /**
     * methode Test creation fichier csv : DGFiP.
     */
    @Test
    public void testEditionPdfSynchroneCompresseeNonStockeee()
    {

        FichierJoint fichierJoint =
            editiondemandeserviceso.declencherProductionEditionCompresseeNonStockee("zf3.agregcontriparavis.edition",
                new HashMap<>());

        File pdfZip = new File(folderTemp.getRoot(), fichierJoint.getNomFichierOriginal());

        try (FileOutputStream fileOuputStream = new FileOutputStream(pdfZip))
        {
            fileOuputStream.write(fichierJoint.getLeContenuDuFichier().getData());
        }
        catch (IOException ioExc)
        {
            ioExc.printStackTrace();
        }

        // on se fait plaisir avec les streams et les lambdas même si le test ne veut rien dire
        Function<ZipEntry, String> getName = ZipEntry::getName;
        Function<String, Boolean> endsWithPdf = (String s) -> s.endsWith("pdf");
        Predicate<ZipEntry> zipE = zipEntry -> getName.andThen(endsWithPdf).apply(zipEntry);

        try (ZipFile zipFile = new ZipFile(pdfZip))
        {
            zipFile.stream().allMatch(zipE);
        }
        catch (IOException exc)
        {
            Assert.fail("Exception:" + exc);
        }
    }

    @Test
    public void testCreationFichierPdfCompresse()
    {

        PersonneAnnuaire personne = new PersonneAnnuaire();
        personne.setUid("test-cp");

        EditionSynchroneNonNotifieeParMail editionSynchroneNonNotifieeParMail = editiondemandeserviceso
            .initEditionSynchroneNonNotifieeParMail("zf2.listecontribuablescsv.edition", personne.getUid());
        editionSynchroneNonNotifieeParMail = editiondemandeserviceso.declencherEdition(editionSynchroneNonNotifieeParMail, new HashMap<>());

        // System.out.println(editionSynchroneNonNotifieeParMail.getStatutEdition());

        // List<JobHistory> jobs =
        // editiondemandeserviceso.rechercherLaDerniereEditionParBeanIdEtParAppliOrigine(beanEditionId,
        // appliOrigine)(personne);

        // assertThat(!jobs.isEmpty());

        /*
         * EditionSynchroneNonNotifieeParMail editionAsynchroneNonNotifieeParMail = editiondemandeserviceso
         * .initEditionSynchroneNonNotifieeParMail("zf2.adressescsv.edition", personne.getUid());
         */
        /*
         * EditionSynchroneNonNotifieeParMail editionAsynchroneNonNotifieeParMail =
         * editiondemandeserviceso.initEditionSynchroneNonNotifieeParMail("zf3.agregcontriparavis.edition", personne
         * .getUid()); editionAsynchroneNonNotifieeParMail =
         * editiondemandeserviceso.declencherEditionCompressee(editionAsynchroneNonNotifieeParMail, new HashMap());
         * FichierJoint fichierJoint = editiondemandeserviceso.rechercherEdition((JobHistory)
         * editionAsynchroneNonNotifieeParMail);
         */

        // FichierJoint fichierJoint =
        // editiondemandeserviceso.declencherProductionEditionCompresseeNonStockee("zf2.adressescsv.edition", new
        // HashMap());

        /*
         * EditionSynchroneNonNotifieeParMail editionAsynchroneNonNotifieeParMail = editiondemandeserviceso
         * .initEditionSynchroneNonNotifieeParMail("zf2.adressescsv.edition", personne.getUid());
         * editiondemandeserviceso.declencherEditionCompressee(editionAsynchroneNonNotifieeParMail, new HashMap());
         */
        /*
         * String nomFichier = fichierJoint.getNomFichierOriginal(); FileOutputStream fos; try { File edi = new
         * File(REPBASE + nomFichier); edi.getParentFile().mkdirs(); edi.createNewFile(); fos = new
         * FileOutputStream(REPBASE + nomFichier, false); if (fichierJoint.isDataInByteArray()) {
         * fos.write(fichierJoint.getLeContenuDuFichier().getData()); fos.close(); } else { File fichierTempo =
         * fichierJoint.getLeContenuDuFichier().getFichierTemporaire(); InputStream inputStream = new
         * FileInputStream(fichierTempo); BufferedInputStream buffInput = new BufferedInputStream(inputStream);
         * BufferedOutputStream buffOutput = new BufferedOutputStream(fos); byte[] buff = new byte[8192]; int bytesRead;
         * while (-1 != (bytesRead = buffInput.read(buff, 0, buff.length))) { buffOutput.write(buff, 0, bytesRead); }
         * buffOutput.flush(); buffOutput.close(); buffInput.close(); } } catch (FileNotFoundException e) { // TODO
         * Auto-generated catch block e.printStackTrace(); } catch (IOException e) { // TODO Auto-generated catch block
         * e.printStackTrace(); }
         */

    }

    public static boolean is_pdf(byte[] data)
    {
        if (data != null && data.length > 4 &&
            data[0] == 0x25 && // %
            data[1] == 0x50 && // P
            data[2] == 0x44 && // D
            data[3] == 0x46 && // F
            data[4] == 0x2D)
        { // -

            // version 1.3 file terminator
            if (data[5] == 0x31 && data[6] == 0x2E && data[7] == 0x33 &&
                data[data.length - 7] == 0x25 && // %
                data[data.length - 6] == 0x25 && // %
                data[data.length - 5] == 0x45 && // E
                data[data.length - 4] == 0x4F && // O
                data[data.length - 3] == 0x46 && // F
                data[data.length - 2] == 0x20 && // SPACE
                data[data.length - 1] == 0x0A)
            { // EOL
                return true;
            }

            // version 1.3 file terminator
            if (data[5] == 0x31 && data[6] == 0x2E && data[7] == 0x34 &&
                data[data.length - 6] == 0x25 && // %
                data[data.length - 5] == 0x25 && // %
                data[data.length - 4] == 0x45 && // E
                data[data.length - 3] == 0x4F && // O
                data[data.length - 2] == 0x46 && // F
                data[data.length - 1] == 0x0A)
            { // EOL
                return true;
            }
        }
        return false;
    }
}
