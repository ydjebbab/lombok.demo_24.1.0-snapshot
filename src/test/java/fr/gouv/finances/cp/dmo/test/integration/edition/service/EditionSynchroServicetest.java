/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : EditionSynchroServicetest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.edition.service;

import static org.junit.Assert.assertEquals;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.WebLauncher;
import fr.gouv.finances.lombok.edition.bean.TrcPurgeEdition;
import fr.gouv.finances.lombok.edition.service.EditionSynchroService;
import fr.gouv.finances.lombok.util.exception.ExploitationException;

/**
 * Class EditionSynchroServicetest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WebLauncher.class})
@WebAppConfiguration
// ajoute le côté transactionnel en utilisant le transaction manager pour les editions

@Transactional(transactionManager = "editionTransactionManager")
@Rollback(false)
public class EditionSynchroServicetest // extends BaseServiceEditionTestCase
{

    /** editionsynchroserviceso. */
    @Autowired
    protected EditionSynchroService editionsynchroserviceso;

    /** application origine. */
    private String applicationOrigine = "DMO";

    /**
     * methode Test supprime toutes les traces de purges : DGFiP.
     */
    @Test
    @Rollback
    public void testSupprimeToutesLesTracesDePurges()
    {
        editionsynchroserviceso.supprimeToutesLesTracesDePurge();
    }

    /**
     * methode Test enregistre demarrage purge ok : DGFiP.
     * 
     * @throws Exception the exception
     */
    @Test
    @Rollback
    public void testEnregistreDemarragePurgeOk() throws Exception
    {

        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.EN_ATTENTE_DEMARRAGE);
        trcPurgeEdition.setDateDebutPurge(new Date(1));
        trcPurgeEdition.setDateFinPurge(Calendar.getInstance().getTime());
        trcPurgeEdition.setApplicationOrigine(applicationOrigine);

        editionsynchroserviceso.enregistreDemarragePurge(applicationOrigine, trcPurgeEdition);

        TrcPurgeEdition trcPurgeEditionLu =
            editionsynchroserviceso.findUnTrcPurgeEditionPourUnInstantDemarrage(trcPurgeEdition.getDateDebutPurge(),
                applicationOrigine);

        boolean rc = trcPurgeEdition.equals(trcPurgeEditionLu);

        assertEquals(rc, true);

    }

    /**
     * methode Test enregistre arret purge : DGFiP.
     * 
     * @throws Exception the exception
     */
    @Test(expected = ExploitationException.class)
    @Rollback
    public void testEnregistreDemarragePurgeOKo() throws Exception
    {
        TrcPurgeEdition trcPurgeEdition = new TrcPurgeEdition();
        trcPurgeEdition.setEtatPurge(TrcPurgeEdition.EN_ATTENTE_DEMARRAGE);
        trcPurgeEdition.setDateDebutPurge(new Date(1));
        trcPurgeEdition.setDateFinPurge(Calendar.getInstance().getTime());
        trcPurgeEdition.setApplicationOrigine(applicationOrigine);
        editionsynchroserviceso.enregistreDemarragePurge(applicationOrigine, trcPurgeEdition);

        // relance tâche dans la même milliseconde
        TrcPurgeEdition trcPurgeEdition2 = new TrcPurgeEdition();
        trcPurgeEdition2.setEtatPurge(TrcPurgeEdition.EN_ATTENTE_DEMARRAGE);
        trcPurgeEdition2.setDateDebutPurge(new Date(1));
        trcPurgeEdition2.setDateFinPurge(Calendar.getInstance().getTime());
        trcPurgeEdition2.setApplicationOrigine(applicationOrigine);
        editionsynchroserviceso.enregistreDemarragePurge(applicationOrigine, trcPurgeEdition2);

    }

}
