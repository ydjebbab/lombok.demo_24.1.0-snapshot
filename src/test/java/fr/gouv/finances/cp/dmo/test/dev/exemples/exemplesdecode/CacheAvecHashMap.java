/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : CacheAvecHashMap.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import fr.gouv.finances.lombok.structure.bean.StructureCP;

/**
 * Class CacheAvecHashMap DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */

public class CacheAvecHashMap
{

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /** cache. */
    protected Map<Integer, StructureCP> cache = new HashMap<Integer, StructureCP>();

    // test lecture du même objet avec utilisation du cache
    /**
     * methode Test lecture depuis un client batch : DGFiP.
     */
    @Test
    public void testLectureDepuisUnClientBatch()
    {
        StructureCP structure1 = rechercherStructureApartirDuCodeIcAvecCache(232);
        StructureCP structure2 = rechercherStructureApartirDuCodeIcAvecCache(232);
        
        Assert.assertEquals(structure1, structure2);
    }

    /**
     * methode Rechercher structure apartir du code ic avec cache : DGFiP.
     * 
     * @param ic param
     * @return structure cp
     */
    protected StructureCP rechercherStructureApartirDuCodeIcAvecCache(Integer ic)
    {
        StructureCP uneStructureCP = null;
        uneStructureCP = cache.get(ic);
        if (uneStructureCP == null)
        {
            log.info("Objet pas encore dans le cache : Lecture");
            uneStructureCP = rechercherStructureApartirDuCodeIc(ic);
            cache.put(ic, uneStructureCP);
        }
        else
        {
            log.info("Objet lu dans le cache");
        }
        return uneStructureCP;
    }

    /**
     * methode Rechercher structure apartir du code ic : DGFiP.
     * 
     * @param ic param
     * @return structure cp
     */
    protected StructureCP rechercherStructureApartirDuCodeIc(Integer ic)
    {
        // Simule la lecture via un service métier d'une structure correspondant au codeic
        StructureCP uneStructureCP = new StructureCP();
        uneStructureCP.setLibelleLong("Trésorerie générale du Doubs");
        return uneStructureCP;
    }
}
