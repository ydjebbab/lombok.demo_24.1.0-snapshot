package fr.gouv.finances.cp.dmo.test.integration;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import fr.gouv.finances.lombok.edition.bean.ParametrageVueJasperCsv;
import fr.gouv.finances.lombok.edition.service.EditionProducerService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PropertiesTest
{
    @Autowired
    @Qualifier("parametragevue.csv")
    private ParametrageVueJasperCsv parametrageVueJasperCsv;

    @Autowired
    @Qualifier("editionproducerserviceimpl")
    private EditionProducerService editionProducerServiceImpl;

    @Test
    public void testResolvingProperties()
    {

        System.out.println(parametrageVueJasperCsv.getCharacterEncoding());
        assertThat("ISO-8859-15".compareToIgnoreCase(parametrageVueJasperCsv.getCharacterEncoding()), is(0));

    }

}
