/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TestValidateXMLAvecFabriqueParseur.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exampleXML;

import junit.framework.TestCase;
import fr.gouv.finances.cp.dmo.test.integration.util.FichierUtil;

/**
 * Class TestValidateXMLAvecFabriqueParseur DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.4 $ Date: 11 déc. 2009
 */
public class TestValidateXMLAvecFabriqueParseur extends TestCase
{

    /** Constant : REPBASE. */
    private static final String REPBASE = FichierUtil.recupererClassPath()
        + "/fr/gouv/finances/cp/dmo/test/dev/exemples/exampleXML/";

    /**
     * test ok de validation xml xsd.
     */

    public void testHeliosOK()
    {

        ValidateurXMLXSDAvecFabriqueParseur xsdValidator = new ValidateurXMLXSDAvecFabriqueParseur();
        String lesResponses =
            xsdValidator.validerXMLParRapportXSD(REPBASE + "budcolltot.xml", REPBASE + "budcolltot.xsd",
                "recuperertoutesleserreurs");
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertNull(lesResponses);

    }

    /**
     * test ou fichier xsd n'existe pas.
     */
    public void testHeliosFichierXSDInexistant()
    {

        ValidateurXMLXSDAvecFabriqueParseur xsdValidator = new ValidateurXMLXSDAvecFabriqueParseur();
        String lesResponses;
        try
        {
            lesResponses =
                xsdValidator.validerXMLParRapportXSD(REPBASE + "budcolltot.xml", REPBASE + "fichierinexistant.xsd",
                    "recuperertoutesleserreurs");
        }
        catch (NullPointerException e)
        {
            lesResponses = "erreur : fichier XSD non trouvé";
        }
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertNotNull(lesResponses);

    }

    /**
     * test ou le fichier xsd comporte des erreurs de syntaxe.
     */

    public void testHeliosFichierXSDAvecErreurs()
    {

        ValidateurXMLXSDAvecFabriqueParseur xsdValidator = new ValidateurXMLXSDAvecFabriqueParseur();
        String lesResponses =
            xsdValidator.validerXMLParRapportXSD(REPBASE + "budcolltot.xml", REPBASE + "budcolltotfaux.xsd",
                "recuperertoutesleserreurs");
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertNotNull(lesResponses);

    }

    /**
     * test erreurs manque balise + manque un champs obligatoire xsd.
     */

    public void testHeliosFichierXMLAvec2Erreurs()
    {

        ValidateurXMLXSDAvecFabriqueParseur xsdValidator = new ValidateurXMLXSDAvecFabriqueParseur();
        String lesResponses =
            xsdValidator.validerXMLParRapportXSD(REPBASE + "budcolltotfaux.xml", REPBASE + "budcolltot.xsd",
                "recuperertoutesleserreurs");
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertNotNull(lesResponses);

    }

    /**
     * test erreurs manque balise + manque un champs obligatoire xsd.
     */

    public void testHeliosFichierXMLAvec1Erreur()
    {

        ValidateurXMLXSDAvecFabriqueParseur xsdValidator = new ValidateurXMLXSDAvecFabriqueParseur();
        String lesResponses =
            xsdValidator.validerXMLParRapportXSD(REPBASE + "budcolltotfaux1.xml", REPBASE + "budcolltot.xsd",
                "recuperertoutesleserreurs");
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertNotNull(lesResponses);

    }

    /**
     * test erreurs xml avec erreurs et xsd avec erreurs.
     */

    public void testHeliosFichierXMLAvecErreursxmlxsd()
    {

        ValidateurXMLXSDAvecFabriqueParseur xsdValidator = new ValidateurXMLXSDAvecFabriqueParseur();
        String lesResponses =
            xsdValidator.validerXMLParRapportXSD(REPBASE + "budcolltotfaux.xml", REPBASE + "budcolltotfaux.xsd",
                "recuperertoutesleserreurs");
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertNotNull(lesResponses);

    }

    /**
     * methode Test erreur nominoe : DGFiP.
     */
    public void testErreurNominoe()
    {

        ValidateurXMLXSDAvecFabriqueParseur xsdValidator = new ValidateurXMLXSDAvecFabriqueParseur();
        String lesResponses = null;

        lesResponses =
            xsdValidator.validerXMLParRapportXSD(REPBASE + "Nominoe_false.xml", REPBASE + "Nominoe_V18R0.xsd",
                "recuperertoutesleserreurs");
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertNotNull(lesResponses);

    }

    /**
     * methode Test nominoe : DGFiP.
     */
    public void testNominoe()
    {

        ValidateurXMLXSDAvecFabriqueParseur xsdValidator = new ValidateurXMLXSDAvecFabriqueParseur();
        String lesResponses = null;

        lesResponses =
            xsdValidator.validerXMLParRapportXSD(REPBASE + "Nominoe_false.xml", REPBASE + "Nominoe_V18R0.xsd",
                "recuperertoutesleserreurs");
        /*
        if (lesResponses != null)
        {
            System.out.println("erreur détectée");
            System.out.println(lesResponses);
        }
        else
        {
            System.out.println("aucune erreur");
        }
        */
        assertNotNull(lesResponses);

    }

}
