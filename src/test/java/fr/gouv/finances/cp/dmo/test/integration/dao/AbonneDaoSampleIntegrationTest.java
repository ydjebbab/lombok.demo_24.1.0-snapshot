/*
 * Copyright (c) 2016 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.test.integration.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import javax.naming.Name;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.NameNotFoundException;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.cp.dmo.ldap.Abonne;
import fr.gouv.finances.cp.dmo.ldap.AbonneDao;

/**
 * Abstract base class for PersonDao integration tests.
 * 
 * @author anne-marie
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = {"embedded", "hibernate", "edition", "atlas", "journal", "structure", "bancaire", "jdbc"})
@ContextConfiguration(name = "application", locations = "classpath:conf/app-config.xml")
public class AbonneDaoSampleIntegrationTest
{

    private static final String adressemel1 = "yussouf.goulamhoussen@dgfip.finances.gouv.fr";

    @Rule
    public final ExpectedException exc = ExpectedException.none();

    /**
     * Bind dn.
     *
     * @param _x the _x
     * @return the javax.naming. name
     */
    private final static Name bindDN(String _x)
    {
        return LdapNameBuilder.newInstance().add("c", "fr").add("o", "gouv")
            .add("ou", "MEFI").add("ou", "abonnes-dgcp-xt")
            .add("ou", "divers-xt").add("uid", _x).build();

    }

    /** The abonne. */
    protected Abonne abonne;

    /** abonne1. */
    protected Abonne abonne1;

    /** The abonne dao. */
    @Autowired
    private AbonneDao abonneDao;

    /**
     * Prepare abonne.
     *
     * @throws Exception the exception
     */
    @Before
    public void prepareAbonne()
    {
        abonne = new Abonne();
        abonne.setPostalAddress("impasse de la baleine");
        abonne.setUid("amleplatinec-xt");
        abonne.setAffectation("093034");
        abonne.setC("fr");
        abonne.setCreate("toto-xt");

        abonne.setGivenName("anne-marie");
        abonne.setL("noisy");

        abonne.setPersonalTitle("Mme");
        abonne.setPopulation("SSLSPL");
        abonne.setPostalCode("93160");
        abonne.setSn("leplatinec");

        abonne.setMail(adressemel1);

        abonneDao.delete(abonne);

    }

    /**
     * Testcreate person names.
     */
    @Test
    public void testCreateAbonne()
    {
        abonneDao.create(abonne);
        Abonne result = abonneDao.findByDn(bindDN("amleplatinec-xt"));
        assertEquals(result, abonne);

    }

    // }

    /**
     * Test create update delete.
     */
    @Test
    public void testCreateUpdateDeleteAbonne()
    {

        abonneDao.create(abonne);

        Abonne result = abonneDao.findByDn(bindDN("amleplatinec-xt"));

        abonne.setPostalAddress("DIRECTION DES FINANCES 33 RUE VALADE");

        abonneDao.update(abonne);
        result = abonneDao.findByDn(bindDN("amleplatinec-xt"));
        assertEquals(result.getPostalAddress(), "DIRECTION DES FINANCES 33 RUE VALADE");

    }

    /**
     * Testdelete person names.
     */
    @Test
    public void testdeleteAbonne()
    {

        abonneDao.create(abonne);
        Abonne amleplatinec = abonneDao.findByDn(bindDN("amleplatinec-xt"));
        assertNotNull(amleplatinec);
        abonneDao.delete(abonne);
        exc.expect(NameNotFoundException.class);
        amleplatinec = abonneDao.findByDn(bindDN("amleplatinec-xt"));

    }

    /**
     * Test find all.
     */
    @Test
    public void testFindAll()
    {
        List<Abonne> result = abonneDao.findAll();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        // assertEquals(89096, result.size());
        Abonne first = result.get(0);
        assertEquals("amacakanja-xt", first.getUid());
    }

    /**
     * methode Test find by sn :.
     */
    @Test
    public void testFindBySn()
    {
        Abonne result = abonneDao.findBySn("macakanja");
        String uid = result.getUid();
        assertEquals("amacakanja-xt", uid);
    }

}