/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : CalculeNombreJoursOuvresTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import org.junit.Test;

import fr.gouv.finances.lombok.util.date.JoursFeries;
import fr.gouv.finances.lombok.util.format.FormaterDate;

/**
 * Class CalculeNombreJoursOuvresTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class CalculeNombreJoursOuvresTest
{

    /**
     * methode Test calcul jours ouvres : DGFiP.
     * verification en ligne:http://www.joursouvres.fr/joursouvres_joursferies_2007.htm#aepd
     */
    @Test
    public void testCalculJoursOuvres()
    {
        DateFormat dateFormat = FormaterDate.getFormatDate("dd/MM/yyyy");

        Date dateDebut;
        Date dateFin;

        try
        {
            dateDebut = dateFormat.parse("29/10/2007");
            dateFin = dateFormat.parse("22/01/2008");

            int nombreJoursHorsSamedisEtDimanches =
                JoursFeries.calculeNombreJoursSansSamedisEtDimanchesEntreDeuxDates(dateDebut, dateFin);
            assertEquals(nombreJoursHorsSamedisEtDimanches, 62);
            //System.out.println("Nombre de jours hors samedis et dimanches = " + nombreJoursHorsSamedisEtDimanches);

            int nombreJoursFeriesHorsSAmedisEtDimanches =
                JoursFeries.calculeNombreJoursFeriesHorsSamediEtDimancheEntreDeuxDates(dateDebut, dateFin);
            assertEquals(nombreJoursFeriesHorsSAmedisEtDimanches, 3);
            //System.out
            //    .println("Nombre de jours hors samedis et dimanches = " + nombreJoursFeriesHorsSAmedisEtDimanches);

            int nombreJoursOuvres = JoursFeries.calculeNombreJoursOuvresEntreDeuxDates(dateDebut, dateFin);
            assertEquals(nombreJoursOuvres, 59);
            //System.out.println("Nombre de jours ouvrés = " + nombreJoursOuvres);
        }
        catch (ParseException exc)
        {
            exc.printStackTrace();
        }
    }
}
