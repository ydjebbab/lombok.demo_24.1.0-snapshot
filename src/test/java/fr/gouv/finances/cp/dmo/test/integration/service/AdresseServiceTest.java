/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : AdresseServiceTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.gouv.finances.cp.dmo.WebLauncher;
import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.service.AdresseService;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Class AdresseServiceTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WebLauncher.class})
@WebAppConfiguration
public class AdresseServiceTest //extends BaseServiceTestCase
{

    /** adresseserviceso. */
    @Autowired
    protected AdresseService adresseserviceso;

    /**
     * methode Test succes verifier existence code postal r g22 : DGFiP.
     */
    @Test
    public void testSuccesVerifierExistenceCodePostalRG22()
    {

        adresseserviceso.verifierExistenceCodePostalRG22("93100");
    }

    /**
     * methode Test echec verifier existence code postal r g22 : DGFiP.
     */
    @Test
    public void testEchecVerifierExistenceCodePostalRG22()
    {
        RegleGestionException e =
            new RegleGestionException(Messages.getString("exception.codepostal.nexistepas") + "sqfqs");

        try
        {
            adresseserviceso.verifierExistenceCodePostalRG22("sqfqs");
            Assert.fail("Echec du controle : le code postal n existe pas ");
        }
        catch (RegleGestionException expected)
        {
            Assert.assertEquals(e.getMessage(), expected.getMessage());
        }
        catch (Exception nonexpected)
        {
            Assert.fail("Exception non prévue");
        }
    }

    /**
     * methode Test succes verifier code postal correspond a un libelle commune r g23 : DGFiP.
     */
    @Test
    public void testSuccesVerifierCodePostalCorrespondAUnLibelleCommuneRG23()
    {

        CodePostal cp = new CodePostal();
        cp.setCode("99999");
        cp.setVille("TEST");
        adresseserviceso.ajouterUnCodePostal(cp);
        adresseserviceso.verifierCodePostalCorrespondAUnLibelleCommuneRG23("99999", "TEST");

    }

    /**
     * methode Test echec verifier code postal correspond a un libelle commune r g23 : DGFiP.
     */
    @Test
    public void testEchecVerifierCodePostalCorrespondAUnLibelleCommuneRG23()
    {
        RegleGestionException e = new RegleGestionException("Le code postal  sqfqsne correpond pas à cette ville  : vjwvv");

        try
        {
            adresseserviceso.verifierCodePostalCorrespondAUnLibelleCommuneRG23("sqfqs", "vjwvv");
            Assert.fail("Echec du controle : le code postal n existe pas ");
        }
        catch (RegleGestionException expected)
        {
            Assert.assertEquals(e.getMessage(), expected.getMessage());
        }
        catch (Exception nonexpected)
        {
            Assert.fail("Exception non prévue");
        }
    }

    /**
     * methode Test succes rechercher villes pour un code postal : DGFiP.
     */
    @Test
    public void testSuccesRechercherVillesPourUnCodePostal()
    {

        @SuppressWarnings("rawtypes")
        List listAdresse = adresseserviceso.rechercherVillesPourUnCodePostal("93100");
        Assert.assertEquals(1, listAdresse.size());
    }

    /**
     * methode Test echec rechercher villes pour un code postal : DGFiP.
     */
    @Test
    public void testEchecRechercherVillesPourUnCodePostal()
    {

        RegleGestionException e = new RegleGestionException("Code postal inexistant AFCTG");

        try
        {
            adresseserviceso.rechercherVillesPourUnCodePostal("AFCTG");
            Assert.fail("Echec du controle : le code postal na pas de villes associess ");
        }
        catch (RegleGestionException expected)
        {
            Assert.assertEquals(e.getMessage(), expected.getMessage());
        }
        catch (Exception nonexpected)
        {
            Assert.fail("Exception non prévue");
        }
    }

    /**
     * methode Test succes rechercher code postal par code ville : DGFiP.
     */
    @Test
    public void testSuccesRechercherCodePostalParCodeVille()
    {

        @SuppressWarnings("rawtypes")
        List listAdresse = adresseserviceso.rechercherCodePostalParCodeVille("93100", "MONTREUIL");
        Assert.assertEquals(1, listAdresse.size());
    }

 
}
