/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.cp.dmo.test.integration.ihm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.gouv.finances.cp.dmo.WebLauncher;

/**
 * DemarrageEmbeddedTest.java Démarrage de l'application en mode embedded
 * 
 * @author chouard Date: 14 déc. 2017
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes=WebLauncher.class)
public class DemarrageEmbeddedTest
{
    @Test
    public void demarrageEmbeddedAvecLesContextes()
    {

    }

}
