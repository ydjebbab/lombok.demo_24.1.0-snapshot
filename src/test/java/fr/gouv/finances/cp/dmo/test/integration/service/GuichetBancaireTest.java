/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : GuichetBancaireTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import fr.gouv.finances.cp.dmo.WebLauncher;
import fr.gouv.finances.lombok.bancaire.service.GuichetBancaireService;

/**
 * Class GuichetBancaireTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WebLauncher.class})
@WebAppConfiguration
public class GuichetBancaireTest // extends BaseServiceTestCase
{

    /** guichetbancaireserviceso. */
    @Autowired
    protected GuichetBancaireService guichetbancaireserviceso;

    /**
     * methode Test rechercher guichets par code guichet code etablissement : DGFiP.
     */
    @Test
    public void testRechercherGuichetsParCodeGuichetCodeEtablissement()
    {
        @SuppressWarnings("rawtypes")
        List lesGuichets =
            guichetbancaireserviceso.rechercherGuichetsBancairesParCodeGuichetEtCodeEtablissement("10057", "13400");
        Assert.assertNotNull(lesGuichets);

    }

}
