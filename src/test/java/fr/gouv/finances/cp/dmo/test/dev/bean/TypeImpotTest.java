/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TypeImpotTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.bean;

import java.math.BigDecimal;

import org.junit.Test;

import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;

/**
 * Class TypeImpotTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class TypeImpotTest extends AbstractCorePojoTest<TypeImpot>
{
    /**
     * Constructeur de la classe TypeImpotTest.java
     */
    public TypeImpotTest()
    {
        super();
        addPrefabValues(BigDecimal.class, BigDecimal.valueOf(0));
    }

    /**
     * test de la méthode qui vérifie si type d'impôt est ouvert à la mensualisation.
     */
    @Test
    public void testVerifierEstOuvertALaMensualisationRG10()
    {
        TypeImpot typeImpot = new TypeImpot();
        typeImpot.setEstOuvertALaMensualisation(Boolean.TRUE);
        typeImpot.verifierEstOuvertALaMensualisationRG10();
    }

    /**
     * test de la méthode qui vérifie si type d'impôt est destiné aux particuliers.
     */
    @Test
    public void testVerifierEstDestineAuxparticuliersRG11()
    {
        TypeImpot typeImpot = new TypeImpot();
        typeImpot.setEstDestineAuxParticuliers(Boolean.TRUE);
        typeImpot.verifierEstDestineAuxparticuliersRG11();
    }

}
