/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ConsommeDeLaMemoireTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import java.util.UUID;

/**
 * Class ConsommeDeLaMemoireTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ConsommeDeLaMemoireTest
{
    
    /** Go1. */
    private static int Go1 = 100000;

    /**
     * Methode main DGFiP.
     * 
     * @param args param
     */
    public static void main(String[] args)
    {
        int val = Go1;

        // Set bigset=new HashSet();
        PersonneAnnuaireTest[] bigtab = new PersonneAnnuaireTest[val + 1];
        for (int i = 0; i < val; i++)
        {
            PersonneAnnuaireTest p10Ko = new PersonneAnnuaireTest();
            p10Ko.affectation = construitUneChaineAleatoire();
            p10Ko.annexe = construitUneChaineAleatoire();
            p10Ko.businessCategory = construitUneChaineAleatoire();
            p10Ko.cn = construitUneChaineAleatoire();
            p10Ko.codeFonction = construitUneChaineAleatoire();
            p10Ko.codeGrade = construitUneChaineAleatoire();
            p10Ko.departement = construitUneChaineAleatoire();
            p10Ko.department = construitUneChaineAleatoire();
            p10Ko.fonction = construitUneChaineAleatoire();
            p10Ko.givenName = construitUneChaineAleatoire();
            // bigset.add(p10Ko);
            bigtab[i] = p10Ko;
            int t = 10 * i;
            int a = i % 1000;
            if (a == 0)
            {
                System.out.println("taille utile allouee en ko : " + t);

            }

        }
    }

    /**
     * methode Construit une chaine aleatoire : DGFiP.
     * 
     * @return string
     */
    private static String construitUneChaineAleatoire()
    {
        StringBuffer t = new StringBuffer("");
        for (int i = 0; i < 36; i++)
        {
            t.append(UUID.randomUUID().toString());
        }
        return t.toString();
    }

}
