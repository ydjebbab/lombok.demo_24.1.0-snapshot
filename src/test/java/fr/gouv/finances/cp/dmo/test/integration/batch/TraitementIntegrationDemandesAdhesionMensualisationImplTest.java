/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : TraitementIntegrationDemandesAdhesionMensualisationImplTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.batch;

import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.batch.TraitementIntegrationDemandesAdhesionMensualisationImpl;
import fr.gouv.finances.cp.dmo.dao.IContratMensuDao;
import fr.gouv.finances.cp.dmo.dao.IContribuableDao;
import fr.gouv.finances.cp.dmo.dao.IReferenceDao;
import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.lombok.batch.ServiceBatchCommun;

/**
 * Class TraitementIntegrationDemandesAdhesionMensualisationImplTest
 * 
 * @author chouard-cp
 * @author CF : MAJ suite à la suppression du fichier XML de configuration
 *         traitementintegrationdemandesadhesionmensualisationContext-service.xml
 * @version $Revision: 1.3 $ Date: 14 déc. 2009
 */
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = {"embedded", "jpa", "jdbc", "upload", "journal", "batch"})
@ContextConfiguration(locations = {
        "classpath:conf/testpropertysource.xml",
        "classpath:conf/app-config.xml",
        "classpath:conf/batch/batchContext-resources.xml",
        "classpath:conf/batch-config-test.xml"})
@Transactional
public class TraitementIntegrationDemandesAdhesionMensualisationImplTest
{
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected IContribuableDao contribuabledao;

    @Autowired
    protected IReferenceDao referencedao;

    @Autowired
    protected IContratMensuDao contratmensudao;

    @Autowired()
    @Qualifier("traitementintegrationdemandesadhesionmensualisation")
    private TraitementIntegrationDemandesAdhesionMensualisationImpl traitementIntegrationDemandesAdhesionMensualisationImpl;

    @Test
    public void testTraiterBatch()
    {
        ServiceBatchCommun sbc = (ServiceBatchCommun) traitementIntegrationDemandesAdhesionMensualisationImpl;
        int statuscode = sbc.demarrer();
        Assert.assertEquals(0, statuscode);
    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see org.springframework.test.AbstractTransactionalSpringContextTests#onSetUpInTransaction()
     */
    @BeforeTransaction
    public void onSetUpInTransaction() throws Exception
    {
        logger.info("Préparation des données de test");
        String refAvis2 = "11111111111";
        ContribuablePar unContribuable =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParReferenceAvis(refAvis2);
        if (unContribuable != null)
        {
            unContribuable =
                contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID(unContribuable
                    .getIdentifiant());
            Set<ContratMensu> listeContrats = unContribuable.getListeContratMensu();
            Set<AvisImposition> listeAvis = unContribuable.getListeAvisImposition();
            Iterator<ContratMensu> iterContrats = listeContrats.iterator();
            Iterator<AvisImposition> iterAvis = listeAvis.iterator();

            // si le contrat existe déjà, nous le supprimons.

            while (iterContrats.hasNext())
            {
                ContratMensu element = (ContratMensu) iterContrats.next();
                if (element.getNumeroContrat().compareTo(Long.valueOf(refAvis2)) == 0)
                {
                    // unContribuable.detacherContratMensu(element);
                    contratmensudao.deleteObject(element);
                    break;
                }
            }
            // on place le bon type impot sur l'avis

            TypeImpot unTypeImpot = referencedao.findTypeImpotParCodeImpot(new Long(2));
            while (iterAvis.hasNext())
            {
                AvisImposition element = (AvisImposition) iterAvis.next();
                if (element.getReference().compareTo(refAvis2) == 0)
                {
                    element.setTypeImpot(unTypeImpot);
                    break;
                }
            }
            // on sauvegarde nos modifications.

            contribuabledao.saveContribuableParticulier(unContribuable);
        }
        else
        {
            logger.info("Pb lors du chargement des données de test: contribuable avec avis" + refAvis2
                + " n'existe pas!");
        }
    }

}
