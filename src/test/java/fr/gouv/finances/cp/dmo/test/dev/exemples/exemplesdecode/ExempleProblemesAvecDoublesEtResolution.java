/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ExempleProblemesAvecDoublesEtResolution.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import java.math.BigDecimal;

import junit.framework.TestCase;

/**
 * Class ExempleProblemesAvecDoublesEtResolution DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ExempleProblemesAvecDoublesEtResolution extends TestCase
{

    /** intvaleur1. */
    Integer intvaleur1 = new Integer(1);

    /** doublevaleur1. */
    Double doublevaleur1 = new Double(1);

    /** doublevaleur0v1. */
    Double doublevaleur0v1 = new Double(0.1);

    /** doublevaleur9. */
    Double doublevaleur9 = new Double(9);

    /**
     * methode Test multiplication avecdouble fausse : DGFiP.
     */
    public void testMultiplicationAvecdoubleFausse()
    {

        //System.out.println("1 - 0.1 * 9 avec type simple double :");
        //System.out.println(doublevaleur1.doubleValue() - doublevaleur0v1.doubleValue() * doublevaleur9.doubleValue());

    }

    /**
     * methode Test operation complete avec big decimal a partir de double fausse : DGFiP.
     */
    public void testOperationCompleteAvecBigDecimalAPartirDeDoubleFausse()
    {

        BigDecimal bigDvaleur0v1 = new BigDecimal(doublevaleur0v1.doubleValue());
        BigDecimal bigDvaleur9 = new BigDecimal(doublevaleur9.doubleValue());
        BigDecimal bigDvaleur1 = new BigDecimal(doublevaleur1.doubleValue());

        BigDecimal bigDResultatMult0v1Par9 = bigDvaleur0v1.multiply(bigDvaleur9);
        BigDecimal bigDResultat = bigDvaleur1.subtract(bigDResultatMult0v1Par9);

       // System.out.println("1 - 0.1 * 9 tout en BigDecimal initialise a partir de double:");
       // System.out.println(bigDResultat);
    }

    /**
     * methode Test operation complete avec big decimal initialise avec des strings : DGFiP.
     */
    public void testOperationCompleteAvecBigDecimalInitialiseAvecDesStrings()
    {

        BigDecimal bigDvaleur0v1 = new BigDecimal("0.1");
        BigDecimal bigDvaleur9 = new BigDecimal("9");
        BigDecimal bigDvaleur1 = new BigDecimal("1");

        BigDecimal bigDResultatMult0v1Par9 = bigDvaleur0v1.multiply(bigDvaleur9);
        BigDecimal bigDResultat = bigDvaleur1.subtract(bigDResultatMult0v1Par9);

        //System.out.println("1 - 0.1 * 9 tout en BigDecimal initialise  partir de string");
        //System.out.println(bigDResultat);
    }

}
