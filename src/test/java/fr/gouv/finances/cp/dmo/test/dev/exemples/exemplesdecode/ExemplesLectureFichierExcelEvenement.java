/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ExemplesLectureFichierExcelEvenement.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import java.io.FileInputStream;
import java.io.InputStream;

import junit.framework.TestCase;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.eventusermodel.HSSFEventFactory;
import org.apache.poi.hssf.eventusermodel.HSSFListener;
import org.apache.poi.hssf.eventusermodel.HSSFRequest;
import org.apache.poi.hssf.record.BOFRecord;
import org.apache.poi.hssf.record.BoundSheetRecord;
import org.apache.poi.hssf.record.LabelSSTRecord;
import org.apache.poi.hssf.record.NumberRecord;
import org.apache.poi.hssf.record.Record;
import org.apache.poi.hssf.record.RowRecord;
import org.apache.poi.hssf.record.SSTRecord;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import fr.gouv.finances.cp.dmo.test.integration.util.FichierUtil;

/**
 * Class ExemplesLectureFichierExcelEvenement DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ExemplesLectureFichierExcelEvenement extends TestCase implements HSSFListener
{

    /** Constant : fichierxls. */
    static final String fichierxls =
        FichierUtil.recupererClassPath() + "/fr/gouv/finances/cp/dmo/test/dev/exemples/exemplesdecode/test1.xls";

    /** log. */
    protected final Log log = LogFactory.getLog(getClass());

    /** sstrec. */
    private SSTRecord sstrec = null;

    /**
     * Instanciation de exemples lecture fichier excel evenement.
     */
    public ExemplesLectureFichierExcelEvenement()
    {
    };

    /** 
     *(methode de remplacement)
     * {@inheritDoc}
     * @see org.apache.poi.hssf.eventusermodel.HSSFListener#processRecord(org.apache.poi.hssf.record.Record)
     */
    public void processRecord(Record record)
    {

        switch (record.getSid())
        {
            // the BOFRecord can represent either the beginning of a sheet or the workbook
            case BOFRecord.sid:
                BOFRecord bof = (BOFRecord) record;
                /*
                if (bof.getType() == BOFRecord.TYPE_WORKBOOK)
                {
                    System.out.println("Encountered workbook");
                    // assigned to the class level member
                }
                else if (bof.getType() == BOFRecord.TYPE_WORKSHEET)
                {
                    System.out.println("Encountered sheet reference");
                }
                */
                break;
            case BoundSheetRecord.sid:
                BoundSheetRecord bsr = (BoundSheetRecord) record;
                //System.out.println("Nouvelle feuille : " + bsr.getSheetname());
                break;
            case RowRecord.sid:
                RowRecord rowrec = (RowRecord) record;
                //System.out.println("Nouvelle ligne, premiere colonne a " + rowrec.getFirstCol()
                //    + " derniere colonne a " + rowrec.getLastCol());
                break;
            case NumberRecord.sid:
                NumberRecord numrec = (NumberRecord) record;
                //System.out.println("Cellule avec valeur " + numrec.getValue() + " à la ligne " + numrec.getRow()
                //    + " à la colonne " + numrec.getColumn());
                break;
            // SSTRecords store a array of unique strings used in Excel.
            case SSTRecord.sid:
                sstrec = (SSTRecord) record;
                /*
                for (int k = 0; k < sstrec.getNumUniqueStrings(); k++)
                {
                    System.out.println("String table value " + k + " = " + sstrec.getString(k));
                }
                */
                break;
            case LabelSSTRecord.sid:
                LabelSSTRecord lrec = (LabelSSTRecord) record;
                //System.out.println("String cell found with value " + sstrec.getString(lrec.getSSTIndex()));
                break;
        }

    }

    /**
     * methode Testlecture : DGFiP.
     * 
     * @throws Exception the exception
     */
    public void testlecture() throws Exception
    {
        log.info("Debut");

        FileInputStream fin = new FileInputStream(fichierxls);
        POIFSFileSystem poifs = new POIFSFileSystem(fin);
        InputStream din = poifs.createDocumentInputStream("Workbook");
        HSSFRequest req = new HSSFRequest();
        
        req.addListenerForAllRecords(this);
        HSSFEventFactory factory = new HSSFEventFactory();
        factory.processEvents(req, din);
        fin.close();
        din.close();
        log.info("Fin");

    }

}