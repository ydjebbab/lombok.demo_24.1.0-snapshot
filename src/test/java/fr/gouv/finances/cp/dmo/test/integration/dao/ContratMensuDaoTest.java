/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.dao;

import java.util.Iterator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IContratMensuDao;
import fr.gouv.finances.cp.dmo.dao.IContribuableDao;
import fr.gouv.finances.cp.dmo.dao.IReferenceDao;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.Rib;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.cp.dmo.test.config.PersistenceJPAConfigTest;

/**
 * Class de test du DAO IContratMensuDao
 * 
 * @author chouard-cp
 * @author CF
 * @version $Revision: 1.5 $ Date: 11 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = {"embedded", "jpa"})
@Rollback(true)
@Transactional
@SpringBootApplication
@SpringBootTest(classes = {PersistenceJPAConfigTest.class})
public class ContratMensuDaoTest
{
    @Autowired
    protected IContratMensuDao contratmensudao = null;

    @Autowired
    protected IContribuableDao contribuabledao = null;

    @Autowired
    protected IReferenceDao referencedao = null;

    /** rollback en fin de test. */
    protected boolean rollbackEnFinDeTest = false;

    private ContratMensu uncontratmensu = null;

    private Rib unrib = null;

    private ContribuablePar uncontribuablepar;

    private TypeImpot untypeimpot;

    /**
     * Test d'insertion d'un contrat
     * 
     * @throws Exception the exception
     */
    @Test
    @Rollback(false)
    public void testInsertContratMensu() throws Exception
    {
        untypeimpot = referencedao.findTypeImpotParCodeImpot(new Long(1));
        uncontribuablepar =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("wpetit-cp");

        uncontribuablepar.setAdresseMail("tre@laposte.net");

        /* Rib de test */
        unrib = new Rib();
        unrib.setCodeBanque("30002");
        unrib.setCodeGuichet("00658");
        unrib.setNumeroCompte("11111111111");
        unrib.setCleRib("93");

        uncontratmensu = new ContratMensu();
        uncontratmensu.setRib(unrib);
        uncontratmensu.setContribuablePar(uncontribuablepar);
        uncontratmensu.setTypeImpot(untypeimpot);
        uncontratmensu.setNumeroContrat(new Long(122211));

        uncontribuablepar.getListeContratMensu().add(uncontratmensu);
        contribuabledao.saveObject(uncontribuablepar);
    }

    /**
     * methode Test de suppression d'un contrat
     * 
     * @throws Exception the exception
     */
    @Test
    @Rollback(false)
    public void testDeleteContratMensu() throws Exception
    {
        uncontribuablepar =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("wpetit-cp");
        for (Iterator<ContratMensu> iter = uncontribuablepar.getListeContratMensu().iterator(); iter.hasNext();)
        {
            ContratMensu element = (ContratMensu) iter.next();
            if (element.getNumeroContrat().compareTo(new Long(122211)) == 0)
            {
                uncontribuablepar.getListeContratMensu().remove(element);
                // contratmensudao.deleteObject(element);
                contribuabledao.saveObject(uncontribuablepar);
                break;
            }
        }
    }

}
