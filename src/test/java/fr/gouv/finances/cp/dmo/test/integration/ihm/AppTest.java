/*
 * Copyright (c) 2011 DGFiP - Tous droits réservés
 */
package fr.gouv.finances.cp.dmo.test.integration.ihm;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

/**
 * Classe de test exemple utilisant le framework.
 */
@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
// @WebAppConfiguration
// @ContextConfiguration(classes=LombokApplication.class,initializers=ConfigFileApplicationContextInitializer.class)
public class AppTest
{
    @Autowired
    WebApplicationContext context;

    private WebClient webClient;

    @LocalServerPort
    int randomServerPort;

    @Value("${server.context-path}")
    String contextPath;

    @Before
    public void setup()
    {
        webClient = new WebClient();
    }

    @After
    public void close() throws Exception
    {
        webClient.close();
    }

    /**
     * methode Test dmo title : .
     * 
     * @throws IOException
     * @throws MalformedURLException
     * @throws FailingHttpStatusCodeException
     */
    @Test
    public void testDMOTitle() throws FailingHttpStatusCodeException, MalformedURLException, IOException
    {
        String url = "http://localhost:" + randomServerPort + contextPath + "/identification.ex";
        HtmlPage logonPage = webClient.getPage(url);
        assertThat(logonPage.getTitleText(), is("DMO - Identification"));

    }
}
