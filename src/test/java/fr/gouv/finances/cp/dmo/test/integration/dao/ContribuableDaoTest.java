/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ContribuableDaoTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.dao;

import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IContribuableDao;
import fr.gouv.finances.cp.dmo.dao.impl.ContribuableDaoImpl;
import fr.gouv.finances.cp.dmo.entite.AdresseContribuable;
import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.Contribuable;
import fr.gouv.finances.cp.dmo.entite.ContribuableEnt;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.techbean.CriteresRecherches;
import fr.gouv.finances.cp.dmo.test.config.PersistenceJPAConfigTest;

/**
 * Class de test du DAO IContribuableDao
 * 
 * @author chouard-cp
 * @author CF
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = {"embedded", "jpa"})
@Rollback(true)
@Transactional
@SpringBootApplication
@SpringBootTest(classes = {PersistenceJPAConfigTest.class})
public class ContribuableDaoTest
{
    private static final Logger log = LoggerFactory.getLogger(ContribuableDaoImpl.class);

    @Autowired
    protected IContribuableDao contribuabledao;

    ContribuablePar contribuable = new ContribuablePar();

    @Test
    public void testSuccessFindNomPrenomAdresseMailExisteDeja()
    {
        ContribuablePar contribuablexistant;
        boolean bool;

        contribuablexistant =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("usertest500-cp");
        if (contribuablexistant == null)
        {
            bool = false;
        }
        else
        {
            contribuable.setIdentifiant(contribuablexistant.getIdentifiant());
            contribuable.setNom(contribuablexistant.getNom());
            contribuable.setPrenom(contribuablexistant.getPrenom());
            contribuable.setAdresseMail(contribuablexistant.getAdresseMail());

            bool = contribuabledao.findNomPrenomAdresseMailExisteDeja(contribuable);
        }
        Assert.assertTrue(bool);

    }

    /**
     * methode Test echec find nom prenom adresse mail existe deja
     */
    @Test
    public void testEchecFindNomPrenomAdresseMailExisteDeja()
    {

        ContribuablePar contribuablexistant;
        boolean bool = false;

        contribuablexistant =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("chouard-cp");
        if (contribuablexistant == null)
        {
            bool = false;
        }
        else
        {
            contribuable.setIdentifiant(contribuablexistant.getIdentifiant());
            contribuable.setNom(contribuablexistant.getNom());
            contribuable.setPrenom(contribuablexistant.getPrenom());
            contribuable.setAdresseMail("hello@test.com");

            bool = contribuabledao.findNomPrenomAdresseMailExisteDeja(contribuable);
        }
        Assert.assertFalse(bool);
    }

    /**
     * methode Test succes find avis imposition par reference
     */
    @Test
    public void testSuccesFindAvisImpositionParReference()
    {
        String reference = "843333333333";
        AvisImposition avis = contribuabledao.findAvisImpositionParReference(reference);

        Assert.assertEquals(avis.getReference(), reference);
    }

    /**
     * methode Test echec find avis imposition par reference
     */
    @Test
    public void testEchecFindAvisImpositionParReference()
    {
        String reference = "aaa";
        AvisImposition avis = contribuabledao.findAvisImpositionParReference(reference);

        Assert.assertNull(avis);
    }

    /**
     * methode Test succes find un contribuable et contrats mensualisation et avis imposition et type impots par uid
     */
    @Test
    public void testSuccesFindUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID()
    {
        log.debug(">>> Debut testSuccesFindUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID");
        contribuable = contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID("wpetit-cp");
        Assert.assertEquals("wpetit-cp", contribuable.getIdentifiant());
    }

    /**
     * methode Test echec find un contribuable et contrats mensualisation et avis imposition et type impots par uid
     */
    @Test
    public void testEchecFindUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParUID()
    {
        String uid = "dsqd";
        contribuable = contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParUID(uid);
        Assert.assertNull(contribuable);
    }

    /**
     * methode Test echec find un contribuable et contrats mensualisation et avis imposition et type impots par
     * reference avis
     */
    @Test
    public void testEchecFindUnContribuableEtContratsMensualisationEtAvisImpositionEtTypeImpotsParReferenceAvis()
    {
        String refAvis = "54354";
        contribuable =
            contribuabledao.findContribuableEtAvisImpositionEtContratsMensualisationParReferenceAvis(refAvis);
        Assert.assertNull(contribuable);
    }

    /**
     * methode Test succes find liste contribuable via criteres recherches
     */
    @Test
    public void testSuccesFindListeContribuableViaCriteresRecherches()
    {
        CriteresRecherches crit = new CriteresRecherches();
        crit.setIdentifiant("wpetit-cp");
        List<Contribuable> contribuableApresTEst = contribuabledao.findListContribuableViaCriteresRecherches(crit);
        Assert.assertEquals(contribuableApresTEst.size(), 1);
    }

    /**
     * methode Test echec find liste contribuable via criteres recherches
     */
    @Test
    public void testEchecFindListeContribuableViaCriteresRecherches()
    {
        CriteresRecherches crit = new CriteresRecherches();
        crit.setIdentifiant("wpetitAZS-cp");
        List<Contribuable> contribuableApresTEst = contribuabledao.findListContribuableViaCriteresRecherches(crit);
        Assert.assertEquals(contribuableApresTEst.size(), 0);

    }

    /**
     * methode Test save contribuable particulier
     */
    @Test
    public void testSaveContribuableParticulier()
    {

        ContribuablePar uncontribuablepar = new ContribuablePar();
        uncontribuablepar.setIdentifiant("junittest1-cp");
        uncontribuablepar.setNom("testnom1");
        uncontribuablepar.setPrenom("testprenom1");
        uncontribuablepar.setTelephoneFixe("0000000000");
        uncontribuablepar.setTelephonePortable("0000000000");
        uncontribuablepar.setAdresseMail("testuser@cp.finances.gouv.fr");
        AdresseContribuable uneAdressePrincipale = new AdresseContribuable();
        uneAdressePrincipale.setCodePostal("93100");
        uneAdressePrincipale.setLigne1("3eme étage - escalier C");
        uneAdressePrincipale.setLigne2("Immeuble D3");
        uneAdressePrincipale.setLigne3("RUE AUGUSTE BLANQUI");
        uneAdressePrincipale.setLigne4("BP18");
        uneAdressePrincipale.setVille("MONTREUIL");
        uneAdressePrincipale.setPays("FRANCE");

        uncontribuablepar.rattacherUneNouvelleAdresse(uneAdressePrincipale);
        contribuabledao.saveContribuableParticulier(uncontribuablepar);
        CriteresRecherches crit = new CriteresRecherches();
        crit.setIdentifiant(uncontribuablepar.getIdentifiant());
        List<Contribuable> contribuableApresTEst = contribuabledao.findListContribuableViaCriteresRecherches(crit);
        Assert.assertEquals(uncontribuablepar.getIdentifiant(), ((ContribuablePar) contribuableApresTEst.toArray()[0])
            .getIdentifiant());

    }

    /**
     * methode Test succes delete contribuable particulier
     */
    @Test
    public void testSuccesDeleteContribuableParticulier()
    {
        ContribuablePar contribuablePar = new ContribuablePar();
        contribuablePar.setIdentifiant("testtest2-cp");
        contribuablePar.setNom("testnom2");
        contribuablePar.setPrenom("testprenom2");
        contribuablePar.setTelephoneFixe("0000000000");
        contribuablePar.setTelephonePortable("0000000000");
        contribuablePar.setAdresseMail("testuser@cp.finances.gouv.fr");
        AdresseContribuable uneAdressePrincipale = new AdresseContribuable();
        uneAdressePrincipale.setCodePostal("93100");
        uneAdressePrincipale.setLigne1("3eme étage - escalier C");
        uneAdressePrincipale.setLigne2("Immeuble D3");
        uneAdressePrincipale.setLigne3("RUE AUGUSTE BLANQUI");
        uneAdressePrincipale.setLigne4("BP18");
        uneAdressePrincipale.setVille("MONTREUIL");
        uneAdressePrincipale.setPays("FRANCE");
        contribuablePar.rattacherUneNouvelleAdresse(uneAdressePrincipale);

        // suppression contribuable
        contribuabledao.deleteContribuable(contribuablePar);
        contribuabledao.flush();

        CriteresRecherches crit = new CriteresRecherches();
        crit.setIdentifiant(contribuablePar.getIdentifiant());
        List<Contribuable> contribuableApresTEst = contribuabledao.findListContribuableViaCriteresRecherches(crit);
        Assert.assertEquals(0, contribuableApresTEst.size());
    }

    /**
     * methode Test succes find un contribuable par sous type et identifiant
     */
    @Test
    public void testSuccesFindUnContribuableParSousTypeEtIdentifiant()
    {
        String identifiant = "%";
        List<Contribuable> liste = contribuabledao.findContribuableParSousClasseEtIdentifiant(identifiant);
        for (Iterator<Contribuable> iterator = liste.iterator(); iterator.hasNext();)
        {
            Contribuable contribuable = (Contribuable) iterator.next();
            log.info(contribuable.getIdentifiant() + " type:" + contribuable.getClass().getName());
            if (contribuable instanceof ContribuableEnt)
            {
                Assert.fail("un contribuableEnt ramené par erreur");
            }
        }
    }

}
