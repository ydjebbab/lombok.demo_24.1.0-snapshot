/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : FichierUtil.java
 *
 */
package fr.gouv.finances.cp.dmo.test.integration.util;

import java.util.StringTokenizer;

/**
 * Cette classe fournit des méthodes facilitant l'utilisation de fichiers utilisés lors de la réalisation de tests
 * unitaires.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 14 déc. 2009
 */
public class FichierUtil
{
    /**
     * Constructeur
     */
    private FichierUtil()
    {
    }

    /**
     * Cette méthode permet de retourner le répertoire contenant de compilation du projet.
     * 
     * @return Le chemin de compilation du projet
     */
    public static String recupererClassPath()
    {
        String classpath = System.getProperty("java.class.path");
        String separator = System.getProperty("path.separator");
        StringTokenizer st = new StringTokenizer(classpath, separator);
        return st.nextToken();
    }
}
