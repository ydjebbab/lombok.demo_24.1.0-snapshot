/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ContribuableTest.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.bean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TimeZone;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.gouv.finances.cp.dmo.entite.AdresseContribuable;
import fr.gouv.finances.cp.dmo.entite.AvisImposition;
import fr.gouv.finances.cp.dmo.entite.ContratMensu;
import fr.gouv.finances.cp.dmo.entite.ContribuablePar;
import fr.gouv.finances.cp.dmo.entite.TypeImpot;
import fr.gouv.finances.lombok.test.pojo.AbstractCorePojoTest;
import fr.gouv.finances.lombok.util.exception.ProgrammationException;
import fr.gouv.finances.lombok.util.exception.RegleGestionException;
import fr.gouv.finances.lombok.util.messages.Messages;

/**
 * Class ContribuableTest DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ContribuableTest extends AbstractCorePojoTest<ContribuablePar>
{

    /** typeimpot1. */
    private TypeImpot typeimpot1;

    /** typeimpot2. */
    private TypeImpot typeimpot2;

    /** typeimpot3. */
    private TypeImpot typeimpot3;

    /** typeimpot4. */
    private TypeImpot typeimpot4;

    /**
     * Constructeur de la classe ContribuableTest.java
     */
    public ContribuableTest()
    {
        super();
        addPrefabValues(TimeZone.class, TimeZone.getDefault());

    }

    /**
     * (methode de remplacement) {@inheritDoc}
     * 
     * @see fr.gouv.finances.cp.dmo.test.util.base.BaseBeanTestCase#setUp()
     */
    @Before
    public void setUp() throws Exception
    {

        // Initialisation de types impot
        String datedebuts = "01/10/2005";
        SimpleDateFormat sdfdebut = new SimpleDateFormat("dd/MM/yyyy");
        Date datedebut = sdfdebut.parse(datedebuts);

        String datefins = "01/10/2006";
        SimpleDateFormat sdffin = new SimpleDateFormat("dd/MM/yyyy");
        Date datefin = sdffin.parse(datefins);

        typeimpot1 = new TypeImpot();
        typeimpot1.setCodeImpot(Long.valueOf(1));
        typeimpot1.setNomImpot("Impôt sur le revenu");
        typeimpot1.setEstOuvertALaMensualisation(Boolean.TRUE);
        typeimpot1.setEstDestineAuxParticuliers(Boolean.TRUE);
        typeimpot1.setJourOuvertureAdhesion(datedebut);
        typeimpot1.setJourLimiteAdhesion(datefin);

        typeimpot2 = new TypeImpot();
        typeimpot2.setCodeImpot(Long.valueOf(2));
        typeimpot2.setNomImpot("Taxe foncières");
        typeimpot2.setEstOuvertALaMensualisation(Boolean.TRUE);
        typeimpot2.setEstDestineAuxParticuliers(Boolean.TRUE);
        typeimpot2.setJourOuvertureAdhesion(datedebut);
        typeimpot2.setJourLimiteAdhesion(datefin);

        typeimpot3 = new TypeImpot();
        typeimpot3.setCodeImpot(Long.valueOf(3));
        typeimpot3.setNomImpot("Taxe d'habitation");
        typeimpot3.setEstOuvertALaMensualisation(Boolean.TRUE);
        typeimpot3.setEstDestineAuxParticuliers(Boolean.TRUE);
        typeimpot3.setJourOuvertureAdhesion(datedebut);
        typeimpot3.setJourLimiteAdhesion(datefin);

        typeimpot4 = new TypeImpot();
        typeimpot4.setCodeImpot(Long.valueOf(4));
        typeimpot4.setNomImpot("Taxe professionnelle");
        typeimpot4.setEstOuvertALaMensualisation(Boolean.TRUE);
        typeimpot4.setEstDestineAuxParticuliers(Boolean.TRUE);
        typeimpot4.setJourOuvertureAdhesion(datedebut);
        typeimpot4.setJourLimiteAdhesion(datefin);

    }

    /**
     * methode Test succesnullparam_rattacher un nouveau avis imposition : DGFiP.
     */
    @Test
    public void testSuccesnullparam_rattacherUnNouveauAvisImposition()
    {
        ContribuablePar contribuablepar = new ContribuablePar();
        contribuablepar.setNom("user");

        Set<AvisImposition> sourceset = new HashSet<AvisImposition>();
        AvisImposition avisimposition;
        avisimposition = new AvisImposition();
        avisimposition.setReference("11111");
        sourceset.add(avisimposition);
        avisimposition = new AvisImposition();
        avisimposition.setReference("22222");
        sourceset.add(avisimposition);
        avisimposition = new AvisImposition();
        avisimposition.setReference("33333");
        sourceset.add(avisimposition);

        for (Iterator<AvisImposition> iter = sourceset.iterator(); iter.hasNext();)
        {
            AvisImposition element = (AvisImposition) iter.next();
            contribuablepar.rattacherUnNouveauAvisImposition(element);
        }
        contribuablepar.rattacherUnNouveauAvisImposition(avisimposition);
        Assert.assertEquals(contribuablepar.getListeAvisImposition(), sourceset);
    }

    /**
     * methode Test echecnullparam_rattacher un nouveau avis imposition : DGFiP.
     */
    public void testEchecnullparam_rattacherUnNouveauAvisImposition()
    {
        ContribuablePar contribuablepar = new ContribuablePar();
        contribuablepar.setNom("user");
        AvisImposition avisimposition = null;

        ProgrammationException e = new ProgrammationException("erreur.programmation");

        try
        {
            contribuablepar.rattacherUnNouveauAvisImposition(avisimposition);
            Assert.fail("Echec du controle : l'avis d'imposition à attaché est null");
        }
        catch (ProgrammationException expected)
        {
            Assert.assertEquals(e.getMessage(), expected.getMessage());
        }
        catch (Exception nonexpected)
        {
            Assert.fail("Exception non prévue");
        }
    }

    /**
     * methode Test echec listevide_verifier avis imposition est attache au contribuable r g14 : DGFiP.
     */
    @Test
    public void testEchecListevide_verifierAvisImpositionEstAttacheAuContribuableRG14()
    {
        ContribuablePar contribuablepar = new ContribuablePar();
        contribuablepar.setNom("user");

        RegleGestionException e =
            new RegleGestionException(Messages.getString("exception.avisnonrattache") + contribuablepar.getNom());

        try
        {
            contribuablepar.verifierAvisImpositionEstAttacheAuContribuableRG14("11111");
            Assert.fail("Echec du controle : aucun avis d'imposition n'est rattaché au contribuable");
        }
        catch (RegleGestionException expected)
        {
            Assert.assertEquals(e.getMessage(), expected.getMessage());
        }
        catch (Exception nonexpected)
        {
            Assert.fail("Exception non prévue");
        }
    }

    /**
     * methode Test echec non appartenance_verifier avis imposition est attache au contribuable r g14 : DGFiP.
     */
    @Test
    public void testEchecNonAppartenance_verifierAvisImpositionEstAttacheAuContribuableRG14()
    {
        ContribuablePar contribuablepar = new ContribuablePar();
        contribuablepar.setNom("user");
        AvisImposition avisimposition = new AvisImposition();
        avisimposition.setReference("777777777");
        contribuablepar.rattacherUnNouveauAvisImposition(avisimposition);
        avisimposition = new AvisImposition();
        avisimposition.setReference("888888888");
        contribuablepar.rattacherUnNouveauAvisImposition(avisimposition);
        avisimposition = new AvisImposition();
        avisimposition.setReference("999999999");
        contribuablepar.rattacherUnNouveauAvisImposition(avisimposition);

        RegleGestionException e =
            new RegleGestionException(Messages.getString("exception.avisnonrattache") + contribuablepar.getNom());

        try
        {
            contribuablepar.verifierAvisImpositionEstAttacheAuContribuableRG14("999999998");
            Assert.fail("Echec du controle : aucun avis d'imposition de référence 999999998 n'est rattaché au contribuable");
        }
        catch (RegleGestionException expected)
        {
            Assert.assertEquals(e.getMessage(), expected.getMessage());
        }
        catch (Exception nonexpected)
        {
            Assert.fail("Exception non prévue");
        }
    }

    /**
     * methode Test succes appartenance_verifier avis imposition est attache au contribuable r g14 : DGFiP.
     */
    @Test
    public void testSuccesAppartenance_verifierAvisImpositionEstAttacheAuContribuableRG14()
    {
        ContribuablePar contribuablepar = new ContribuablePar();
        contribuablepar.setNom("user");
        AvisImposition avisimposition = new AvisImposition();
        avisimposition.setReference("777777777");
        contribuablepar.rattacherUnNouveauAvisImposition(avisimposition);
        avisimposition = new AvisImposition();
        avisimposition.setReference("888888888");
        contribuablepar.rattacherUnNouveauAvisImposition(avisimposition);
        avisimposition = new AvisImposition();
        avisimposition.setReference("999999999");
        contribuablepar.rattacherUnNouveauAvisImposition(avisimposition);
        contribuablepar.verifierAvisImpositionEstAttacheAuContribuableRG14("888888888");

    }

    /**
     * methode Test succes verifier pas d adhesion en cours sur ce type d impot pour ce contribuable r g12 : DGFiP.
     */
    @Test
    public void testSuccesVerifierPasDAdhesionEnCoursSurCeTypeDImpotPourCeContribuableRG12()
    {
        ContribuablePar contribuablepar = new ContribuablePar();
        contribuablepar.setNom("user");
        ContratMensu contratmensu = new ContratMensu();
        contratmensu.setNumeroContrat(Long.valueOf("1111111111"));
        contratmensu.setTypeImpot(typeimpot1);
        contribuablepar.rattacherUnNouveauContratMensu(contratmensu);
        contratmensu = new ContratMensu();
        contratmensu.setNumeroContrat(Long.valueOf("2222222222"));
        contratmensu.setTypeImpot(typeimpot2);
        contribuablepar.rattacherUnNouveauContratMensu(contratmensu);
        contratmensu = new ContratMensu();
        contratmensu.setNumeroContrat(Long.valueOf("3333333333"));
        contratmensu.setTypeImpot(typeimpot3);
        contribuablepar.rattacherUnNouveauContratMensu(contratmensu);
        contribuablepar.verifierPasDAdhesionEnCoursSurCeTypeDImpotPourCeContribuableRG12(Long.valueOf(4));

    }

    /**
     * methode Test echec verifier pas d adhesion en cours sur ce type d impot pour ce contribuable r g12 : DGFiP.
     */
    @Test
    public void testEchecVerifierPasDAdhesionEnCoursSurCeTypeDImpotPourCeContribuableRG12()
    {
        ContribuablePar contribuablepar = new ContribuablePar();
        contribuablepar.setNom("user");
        ContratMensu contratmensu = new ContratMensu();
        contratmensu.setNumeroContrat(Long.valueOf("1111111111"));
        contratmensu.setTypeImpot(typeimpot1);
        contribuablepar.rattacherUnNouveauContratMensu(contratmensu);
        contratmensu = new ContratMensu();
        contratmensu.setNumeroContrat(Long.valueOf("2222222222"));
        contratmensu.setTypeImpot(typeimpot2);
        contribuablepar.rattacherUnNouveauContratMensu(contratmensu);
        contratmensu = new ContratMensu();
        contratmensu.setNumeroContrat(Long.valueOf("3333333333"));
        contratmensu.setTypeImpot(typeimpot3);
        contribuablepar.rattacherUnNouveauContratMensu(contratmensu);

        RegleGestionException e =
            new RegleGestionException(Messages.getString("exception.adhesionencours.part1") + contribuablepar.getNom()
                + Messages.getString("exception.adhesionencours.part2") + typeimpot2.getNomImpot());
        try
        {
            contribuablepar.verifierPasDAdhesionEnCoursSurCeTypeDImpotPourCeContribuableRG12(Long.valueOf(2));
            Assert.fail("Echec du controle : Adhésion en cours pour le type impôt 2 ");
        }
        catch (RegleGestionException expected)
        {
            Assert.assertEquals(e.getMessage(), expected.getMessage());
        }
        catch (Exception nonexpected)
        {
            Assert.fail("Exception non prévue");
        }
    }

    /**
     * methode Test succes rechercher avis imposition attache : DGFiP.
     */
    @Test
    public void testSuccesRechercherAvisImpositionAttache()
    {
        ContribuablePar contribuablepar = new ContribuablePar();
        contribuablepar.setNom("user");
        AvisImposition avisimposition1 = new AvisImposition();
        avisimposition1.setReference("777777777");
        contribuablepar.rattacherUnNouveauAvisImposition(avisimposition1);
        AvisImposition avisimposition2 = new AvisImposition();
        avisimposition2.setReference("888888888");
        contribuablepar.rattacherUnNouveauAvisImposition(avisimposition2);
        AvisImposition avisimposition3 = new AvisImposition();
        avisimposition3.setReference("999999999");
        contribuablepar.rattacherUnNouveauAvisImposition(avisimposition3);

        AvisImposition avistrouve = contribuablepar.rechercherAvisImpositionAttache("888888888");
        Assert.assertEquals(avisimposition2, avistrouve);

        AvisImposition avisnontrouve = contribuablepar.rechercherAvisImpositionAttache("8888888889");
        Assert.assertEquals(null, avisnontrouve);

    }

    /**
     * methode Test get adresse principale : DGFiP.
     */
    @Test
    public void testGetAdressePrincipale()
    {

        ContribuablePar d = new ContribuablePar("1111111111");
        AdresseContribuable uneAdresse = new AdresseContribuable();

        // CF 18/12/2018
        // uneAdresse.setId(Long.valueOf(1));
        uneAdresse.setCodePostal("93000");
        uneAdresse.setLigne1("adresse ligne1");
        uneAdresse.setLigne2("adresse ligne2");
        uneAdresse.setLigne3("adresse ligne3");
        uneAdresse.setLigne4("adresse ligne4");
        uneAdresse.setVille("Montreuil");
        uneAdresse.setPays("France");
        d.rattacherUneNouvelleAdresse(uneAdresse);
        AdresseContribuable uneAdresse1 = new AdresseContribuable();
        uneAdresse1 = d.getAdressePrincipale();
        // CF 18/12/2018
        // Assert.assertEquals(uneAdresse.getId(), uneAdresse1.getId());
        Assert.assertEquals(uneAdresse.getLigne1(), uneAdresse1.getLigne1());
    }

    /**
     * test des méthodes de rattachement/détachement des contrats de mensualisation.
     */
    @Test
    public void testRattacherUnNouveauContratMensu()
    {

        // création d'un contribuable auquel on rattache deux contrats de mensualisation
        ContribuablePar cp = new ContribuablePar("1111111111");
        ContratMensu cm1 = new ContratMensu();

        cm1.setNumeroContrat(new Long("12345"));
        ContratMensu cm2 = new ContratMensu();
        cm2.setNumeroContrat(new Long("67890"));
        cp.rattacherUnNouveauContratMensu(cm1);
        cp.rattacherUnNouveauContratMensu(cm2);

        // on vérifie que les 2 contrats sont bien rattachés au contribuable
        Assert.assertTrue(cp.getListeContratMensu().contains(cm1));
        Assert.assertTrue(cp.getListeContratMensu().contains(cm2));

        // on détache le second contrat et on vérifie que seul le premier est rattaché au contribuable
        cp.detacherContratMensu(cm2);
        Assert.assertTrue(cp.getListeContratMensu().contains(cm1));
        Assert.assertFalse(cp.getListeContratMensu().contains(cm2));

    }

    /**
     * test de la méthode de rattachement des adresses.
     */
    @Test
    public void testRattacherUneNouvelleAdresse()
    {

        // création d'un contribuable auquel on rattache une adresse
        ContribuablePar cp = new ContribuablePar("1111111111");
        AdresseContribuable uneAdresse = new AdresseContribuable();

        uneAdresse.setCodePostal("93000");
        uneAdresse.setLigne1("adresse ligne1");
        uneAdresse.setLigne2("adresse ligne2");
        uneAdresse.setLigne3("adresse ligne3");
        uneAdresse.setLigne4("adresse ligne4");
        uneAdresse.setVille("Montreuil");
        uneAdresse.setPays("France");
        cp.rattacherUneNouvelleAdresse(uneAdresse);

        // on vérifie que l'adresse est bien rattachée au contribuable
        Assert.assertTrue(cp.getListeAdresses().contains(uneAdresse));

    }
}
