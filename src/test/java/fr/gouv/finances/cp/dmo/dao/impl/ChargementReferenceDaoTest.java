package fr.gouv.finances.cp.dmo.dao.impl;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;

import javax.annotation.Resource;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.dom.DOMDocumentFactory;
import org.dom4j.io.SAXReader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.cp.dmo.dao.IChargementReferenceDao;

/**
 * Classe de test du DAO ChargementReferenceDao
 * 
 * @author celfer Date: 22 janv. 2019
 */
@RunWith(SpringJUnit4ClassRunner.class)
// @ActiveProfiles(profiles = {"embedded", "jpa", "jdbc", "upload", "journal"})
@ActiveProfiles(profiles = {"embedded", "jpa", "edition", "atlas", "journal-ws", "itm", "bancaire", "sireme", "clamav-simulation",
        "springbatch", "jdbc", "nowebflowcsrf", "adresse", "journal"})
@ContextConfiguration(locations = {
        //"classpath:conf/batch/chargementdonneesdereference/traitementchargementdonneesdereferenceContext-service.xml",
        "classpath:conf/testpropertysource.xml",
        "classpath:conf/app-config.xml",
        "classpath:conf/batch/batchContext-resources.xml",
        "classpath:conf/batch-config-test.xml"})
@Transactional
public class ChargementReferenceDaoTest
{
    private static final Logger log = LoggerFactory.getLogger(ContratMensuDaoTest.class);

    private final String cheminFichiers = "src/main/resources/conf/batch/chargementdonneesdereference/fichiersentree/";

    private final String nomFichierCivilite = "civilite_input.xml";

    private final String nomFichierPays = "iso_3166-1_list_fr.xml";

    private final String nomFichierSituationFamiliale = "situationfami_input.xml";

    private final String nomFichierTypeImpot = "typeimpot_input.xml";

    @Resource(name = "chargementreferencedaoimpl")
    protected IChargementReferenceDao chargementReferenceDao;

    // @Test
    @Transactional
    @Rollback(false)
    public void tesLoadCiviliteFromXml()
    {
        File fichierCivilite = new File(cheminFichiers + nomFichierCivilite);
        Document document = null;
        try
        {
            document = createDocument(fichierCivilite);
        }
        catch (FileNotFoundException | DocumentException e)
        {
        }
        chargementReferenceDao.loadCiviliteFromXml(document);
        // assertEquals(4, );
    }

    // @Test
    @Transactional
    @Rollback(false)
    public void testLoadPaysFromXml()
    {
        File fichierPays = new File(cheminFichiers + nomFichierPays);
        Document document = null;
        try
        {
            document = createDocument(fichierPays);
        }
        catch (FileNotFoundException | DocumentException e)
        {
        }
        chargementReferenceDao.loadPaysFromXml(document);
        // assertEquals(4, );
    }

    // @Test
    @Transactional
    @Rollback(false)
    public void testLoadSituationFamiFromXml()
    {
        File fichierSituationFami = new File(cheminFichiers + nomFichierSituationFamiliale);
        Document document = null;
        try
        {
            document = createDocument(fichierSituationFami);
        }
        catch (FileNotFoundException | DocumentException e)
        {
        }
        chargementReferenceDao.loadSituationFamiFromXml(document);
        // assertEquals(4, );
    }

    // @Test
    @Transactional
    @Rollback(false)
    public void testLoadTypeImpotFromXml()
    {
        File fichierTypeImpot = new File(cheminFichiers + nomFichierTypeImpot);
        Document document = null;
        try
        {
            document = createDocument(fichierTypeImpot);
        }
        catch (FileNotFoundException | DocumentException e)
        {
        }
        chargementReferenceDao.loadTypeImpotFromXml(document);
        // assertEquals(4, );
    }

    // @Test
    @Transactional
    @Rollback(false)
    public void testUnloadPaysToXml()
    {
        Document document = chargementReferenceDao.unloadPaysToXml();
        log.debug("document.asXML() : " + document.asXML());
        Element rootElement = document.getRootElement();

        int compteurElement = 0;
        for (Iterator i = rootElement.elementIterator(); i.hasNext();)
        {
            Element element = (Element) i.next();
            compteurElement++;
        }
        assertEquals(10, compteurElement);
        /*
         * List list = document.selectNodes("//ISO_3166-1_Entry/ISO_3166-1_Country_name"); log.debug("list.size() : " +
         * list.size()); assertEquals(10, list.size());
         */
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testUnloadTypeImpotToXml()
    {
        Document document = chargementReferenceDao.unloadTypeImpotToXml();
        log.debug("document.asXML() : " + document.asXML());
        Element rootElement = document.getRootElement();

        int compteurElement = 0;
        for (Iterator i = rootElement.elementIterator(); i.hasNext();)
        {
            Element element = (Element) i.next();
            compteurElement++;
        }
        assertEquals(4, compteurElement);
    }

    private static Document createDocument(File file)
        throws FileNotFoundException, DocumentException
    {
        DOMDocumentFactory factory = new DOMDocumentFactory();
        SAXReader reader = new SAXReader();
        reader.setDocumentFactory(factory);
        return reader.read(new FileInputStream(file));
    }
}
