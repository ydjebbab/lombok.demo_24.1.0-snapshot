/*
 * Copyright : Ministère du budget, des comptes publics et de la fonction publique – France
 * Contributeur(s) : 
 * - chouard-cp
 *
*
 *
 * fichier : ListePolicesAwtDispo.java
 *
 */
package fr.gouv.finances.cp.dmo.test.dev.exemples.exemplesdecode;

import java.awt.Font;
import java.awt.GraphicsEnvironment;

/**
 * Class ListePolicesAwtDispo DGFiP.
 * 
 * @author chouard-cp
 * @version $Revision: 1.3 $ Date: 11 déc. 2009
 */
public class ListePolicesAwtDispo
{

    /**
     * The main method.
     * 
     * @param args param
     */
    public static void main(String[] args)
    {
        // get the local graphics environment
        GraphicsEnvironment graphicsEvn = GraphicsEnvironment.getLocalGraphicsEnvironment();
        // get all the available fonts
        String availFonts[] = graphicsEvn.getAvailableFontFamilyNames();
        for (int i = 0; i < availFonts.length; i++)
        {
            // get the font name
            String fontName = availFonts[i];
            System.out.println(fontName);
        }

        Font allFonts[] = graphicsEvn.getAllFonts();
        for (int i = 0; i < availFonts.length; i++)
        {
            // get the font name
            String fontName = (allFonts[i]).getFontName();
            System.out.println(fontName);
        }

    }

}
