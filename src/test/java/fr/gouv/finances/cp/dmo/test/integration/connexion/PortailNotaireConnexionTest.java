/*
 * Copyright (c) 2017 DGFiP - Tous droits réservés
 * 
 */
package fr.gouv.finances.cp.dmo.test.integration.connexion;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.gouv.finances.cp.dmo.config.LombokWebSecurityConfiguration;
import fr.gouv.finances.lombok.config.LombokMenuConfiguration;
import fr.gouv.finances.lombok.config.LombokRootApplicationConfiguration;
import fr.gouv.finances.lombok.config.LombokWebApplicationConfiguration;
import fr.gouv.finances.lombok.config.LombokWebFlowConfiguration;
import fr.gouv.finances.lombok.config.LombokWebFlowSharedConfiguration;
import fr.gouv.finances.lombok.config.LombokWebSharedConfiguration;
import fr.gouv.finances.lombok.util.exception.ExploitationException;

/**
 * PortailNotaireConnexionTest.java classe de test portail notaire avec clé real
 * 
 * @author chouard Date: 30 mai 2017
 */
@Ignore("test déconnecté le temps de réparer les controleurs en 24")
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ActiveProfiles(profiles = {"embedded", "hibernate", "edition", "atlas", "journal", "structure", "bancaire", "jdbc"})
@ContextConfiguration(classes = {
        LombokWebApplicationConfiguration.class,
        LombokMenuConfiguration.class,
        LombokWebSharedConfiguration.class,
        LombokWebFlowConfiguration.class,
        LombokWebFlowSharedConfiguration.class,
        LombokWebSecurityConfiguration.class,
        LombokRootApplicationConfiguration.class})
public class PortailNotaireConnexionTest
{
    /** spring security filter chain. */
    @Resource
    FilterChainProxy springSecurityFilterChain;

    /** wac. */
    @Autowired
    private WebApplicationContext wac;

    /** mock mvc. */
    private MockMvc mockMvc;

    /** thrown. */
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * methode Setup .
     */
    @Before
    public void setup()
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
            .addFilter(springSecurityFilterChain, "/*.ex", "/j_spring_security_check", "/j_spring_security_logout", "/j_appelportail")
            .build();
    }

    @Test
    public void webpassremoteuser() throws Exception
    {
        thrown.expect(ExploitationException.class);

        this.mockMvc.perform(get("/j_appelportail"))
            .andDo(print());

    }

}
