package fr.gouv.finances.dgfip.orc.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import fr.gouv.finances.lombok.util.tar.ArchiveTar;


public class ArchiveTarTest
{

    public static void testArchiveTarFile()
    {
  
        File fichierUn = new File("/tmp/factureHotel.pdf");
        String fichierArchive = "/tmp/TARGZtest.tar.gz";
        
        ArchiveTar archiveTar;
        try
        {
            archiveTar = new ArchiveTar(fichierArchive, 256);

            InputStream stream = new FileInputStream(fichierUn);
            byte [] data = org.apache.commons.io.IOUtils.toByteArray(stream);
            archiveTar.creerEntree("factureHotel.pdf", data);

            stream.close();
            archiveTar.fermerArchive();
        }
        catch (IOException e)
        {
            System.err.println(e);
        }
    }
    
    public static void main(String[] args){
        testArchiveTarFile();
    }
}
