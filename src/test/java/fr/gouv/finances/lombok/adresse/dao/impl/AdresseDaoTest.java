package fr.gouv.finances.lombok.adresse.dao.impl;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.gouv.finances.lombok.adresse.bean.CodePostal;
import fr.gouv.finances.lombok.adresse.dao.AdresseDao;

/**
 * Classe de test du DAO AdresseDao
 * 
 * @author celfer
 * Date: 5 févr. 2019
 */
@RunWith(SpringJUnit4ClassRunner.class)
// @ActiveProfiles(profiles = {"embedded", "jpa", "jdbc", "upload", "journal"})
@ActiveProfiles(profiles = {"embedded", "jpa", "edition", "atlas", "journal-ws", "itm", "bancaire", "sireme", "clamav-simulation",
        "springbatch", "jdbc", "nowebflowcsrf", "adresse", "journal"})
@ContextConfiguration(locations = {
        "classpath:conf/testpropertysource.xml",
        "classpath:conf/app-config.xml"})
        //"classpath:conf/batch/batchContext-resources.xml"})
@Transactional(transactionManager="transactionManager")
public class AdresseDaoTest
{
    private static final Logger log = LoggerFactory.getLogger(AdresseDaoTest.class);

    @Resource(name = "adressedaoimpl")    
    protected AdresseDao adresseDao;

    @Test
    @Rollback(true)
    public void testCheckExistCodePostal()
    {
        boolean isCodePostalPresent = adresseDao.checkExistCodePostal("93100");
        assertEquals(true, isCodePostalPresent);
    }    
    
    //@Test
    @Rollback(true)
    public void testCheckExistCodePostalXVille()
    {
        boolean isCodePostalEtVillePresent = adresseDao.checkExistCodePostalXVille("93100", "Montreuil");
        assertEquals(true, isCodePostalEtVillePresent);
    }    
    
    @Test    
    public void testFindCodePostaux()
    {
        List<CodePostal> listeCodePostaux = adresseDao.findCodePostaux();
        int total = listeCodePostaux.size();
        assertEquals(4, total);
    } 
    
    
    @Test
    @Rollback(true)
    public void testDeleteTousLesCodesPostaux()
    {
        log.debug("Debut test");
       // int total = adresseDao.deleteTousLesCodesPostaux();
        adresseDao.deleteTousLesCodesPostaux();
        //assertEquals(1, total);
        log.debug("Fin test");
    }

}
